# ***dsltool***
## About
***dsltool*** is a tool to read out data from a DSL modem and output them
as text or graph.

The ***dsltool*** daemon records data from a DSL modem by the help of the
[`collectd`](https://collectd.org/) daemon and stores it
in [`rrd`](http://oss.oetiker.ch/rrdtool/) databases.

## Author
***dsltool*** is written by Carsten Spie&szlig;  
([`mailto:dsltool@carsten-spiess.de`](mailto:dsltool@carsten-spiess.de)).

## License
***dsltool*** is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

## Install
At the moment no distribution packages or binaries are available,
so you need to compile it yourself.

### Prerequisites
***dsltool*** is an `cmake` project and needs the several packages to be installed.

Some packages are required only when the corresponding build options are set  
(e.g. WITH_GUI=ON requires Qt5).

#### Linux
On an Debian/Ubuntu system use `apt-get`
```
build-essential
cmake
libcairo2-dev
libcollectdclient-dev
libpango1.0-dev
libpcap-dev
libssl-dev
libtelnet-dev
libxml2-dev
qtbase5-dev
```

#### Windows
Use the installers for (other versions untested)
```
Visual Studio (e.g. VS 2013)
CMake (e.g CMake 3.5.2)
Qt 5.5 (eg. qt-opensource-windows-x86-msvc2013_64-5.5.1)
```

### Build

#### Linux
Change to the ***dsltool*** directory and run the following commands
on a linux shell

```
$ mkdir -p build
$ cd build
$ cmake [options] ../source
$ cmake --build .
$ sudo make install
$ cd ..
```

#### Windows
Open a command window, change to the ***dsltool*** directory and run

```
$ mkdir build
$ pushd build
$ cmake [options] -G "Visual Studio 12 2013 Win64" ../source
$ cmake --build . --config Release
$ popd
```
For a windows build a CMake generator must be defined,  
e.g. ```-G "Visual Studio 12 2013 Win64"```.

#### ***cmake*** [options]
must be preceeded by ```-D``` (e.g. ```-DWITH_GUI=OFF```)

##### ```WITH_CMD```
ccompile command line tool (default ```ON```)

##### ```WITH_DAEMON```
compile daemon (default ```ON```)  
set ```WITH_DAEMON=OFF``` when compiling for Windows

##### ```WITH_GUI```
compile Qt5 gui (defaul ```ON```)

##### ```WITH_PCAP```
compile pcap protocol (default ```OFF```)  
set ```WITH_PCAP=OFF``` when compiling for Windows

##### ```WITH_LIBXML```
compile with libmxl2 (default ```ON```)  
set ```WITH_LIBXML=OFF``` when compiling for Windows

##### ```WITH_PANGO```
compile with pango/cairo (default ```ON```)  
set ```WITH_PANGO=OFF``` when compiling for Windows

##### ```WITH_COLLECTD```
compile with collectd client (default ```ON```)  
set ```WITH_COLLECTD=OFF``` when compiling for Windows

##### ```WITH_LOCALE```
with locale (default ```ON```)

#####  ```WITH_LONGOPT```
with long options (defaul ```ON```)

## Run

Get help
```
dsltool -h
dsltoold -h
```
Start gui
```
dsltool-gui
```

### Supported DSL modems

#### `amazon`

  Modems/router based on Infineon/Lantiq Amazon SE chipset

  + Allnet ALL 0333 CJ


#### `ar7`

  Modems/router based on Texas Instruments AR7 chipset family

  + Funkwerk M22
  + Sphairon AR860
  + D-Link DSL-T380


#### `avm-tr064`

  AVM Fritz!Box Router (Firmware >= 5.50)

  + Fritz!Box 3272 FW 6.30


#### `bc63`

  Modems/router based on Broadcom bc63xx chipset

  + D-Link DSL-321B (HW Revision D_x_)
  + Zyxel VMG1312-B30A
  + Zyxel VMG1312-B10A


#### `conexant`

  Modems/router based on Conexant chipset

  + Sphairon AR800


#### `speedtouch`

  ALCATEL/Thomson 5x6 and 7x6 modems/router with firmware version 5.x and 6.x

  + ALCATEL/Thomson Speedtouch 516i V6 FW 5.4.0.14
  + ALCATEL/Thomson Speedtouch 585i V6 FW 6.1.0.5
  + ALCATEL/Thomson Speedtouch 536i V6 FW 6.2.15.5


#### `trendchip`

  Modems based on Trendchip chipset

  + D-Link DSL-321B (HW Revision Z_x_)


#### `vigor`

  DrayTek Vigor modems

  + Vigor 130


#### `vinax`

  Modems/router based on Infineon/Lantiq Vinax chipset

  + Sphairon Speedlink 1113


#### `demo-adsl` `demo-vdsl`

 Demo modem (gives sample values)
