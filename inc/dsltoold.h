/*!
 * @file        dsltoold.h
 * @brief       application class definitions
 * @details     dsltoold daemon
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DSLTOOLD_H__
#define __DSLTOOL_DSLTOOLD_H__

#include "daemon.h"

/*!
* @class       CDslToolDaemon
* @brief       main application class
*/
class CDslToolDaemon  : public CRunDaemon
{
public:
    CDslToolDaemon (void);

private:
    //! @cond
    CDslToolDaemon (const CDslToolDaemon &cDslToolDaemon);
    CDslToolDaemon &operator = (const CDslToolDaemon &cDslToolDaemon);
    //! @endcond

public:
    int Main (int nArgc, char **astrArgv);
}; // class CDslTool

#endif // __DSLTOOL_DSLTOOLD_H__
//_oOo_
