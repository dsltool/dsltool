/*!
 * @file        xml-context.h
 * @brief       XML parser context class definitions
 * @details     libxml2 DOM and SAX2 parser wrapper
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        05.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_XML_CONTEXT_H__
#define __DSLTOOL_XML_CONTEXT_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include <libxml/HTMLtree.h>
#include <libxml/xpath.h>

/*!
 * @class       CXmlContext
 * @brief       pure virtual XML parser base class
 */
class LIBDSLTOOL_EXPORT CXmlContext
{
protected:
    CXmlContext (bool bHTML);

private:
    //! @cond
    CXmlContext (CXmlContext const &cXmlContext);
    CXmlContext &operator = (CXmlContext const &cXmlContext);
    //! @endcond

public:
    virtual ~CXmlContext (void);

public:
    void Parse (const char *pChunk, size_t nSize);
    void XPathNamespace (const xmlChar *pPrefix, const xmlChar *pURI);
    bool XPathQuery (long &nResult, const xmlChar *pXPath);
    bool XPathQuery (double &fResult, const xmlChar *pXPath);
    bool XPathQuery (CString &strResult, const xmlChar *pXPath);

private:
    xmlXPathObjectPtr XPathObject (const xmlChar *pXPath);


protected:
    bool m_bHTML;                       //!< HTML or XML flag
    xmlParserCtxtPtr m_hParser;         //!< XML parser context pointer
    xmlXPathContextPtr m_hXPath;        //!< XPath context pointer
}; // class CXmlContext

/*!
 * @class       CDomContext
 * @brief       Dom tree parser
 */
class LIBDSLTOOL_EXPORT CDomContext : public CXmlContext
{
public:
    CDomContext (CDomParser *pParser, bool bHTML);

private:
    //! @cond
    CDomContext (CDomContext const &cDomContext);
    CDomContext &operator = (CDomContext const &cDomContext);
    //! @endcond

private:
    CDomParser *m_pParser;      //!< DOM parser
}; // class CDomContext

/*!
 * @class       CSaxContext
 * @brief       SAX2 parser
 */
class LIBDSLTOOL_EXPORT CSaxContext : public CXmlContext
{
public:
    CSaxContext (CSaxParser *pParser, bool bHTML);

private:
    //! @cond
    CSaxContext (CSaxContext const &cSaxContext);
    CSaxContext &operator = (CSaxContext const &cSaxContext);
    //! @endcond

private:
    static void _StartDocument (void *pUser);
    static void _EndDocument (void *pUser);
    static void _StartElement (void *pUser, const xmlChar *pElement, const xmlChar **ppAttrs);
    static void _EndElement (void *pUser, const xmlChar *pElement);
    static void _Characters (void *pUser, const xmlChar *pCharacters, int nSize);

private:
    void StartDocument (void);
    void EndDocument (void);
    void StartElement (const xmlChar *pElement, const xmlChar **ppAttrs);
    void EndElement (const xmlChar *pElement);
    void Characters (const xmlChar *pCharacters, int nSize);

private:
    CSaxParser *m_pParser;      //!< SAX parser
    xmlSAXHandler m_tHandler;   //!< SAX2 callback handler
}; // class CSaxContext

#endif // __DSLTOOL_XML_PARSER_H__
//_oOo_
