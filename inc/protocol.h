/*!
 * @file        protocol.h
 * @brief       protocol base class definitions
 * @details     abstract protocol and helper classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_PROTOCOL_H__
#define __DSLTOOL_PROTOCOL_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include <stddef.h>

#include "basis.h"
#ifdef ENABLE_CAPTURE
#include "capture.h"
#endif //ENABLE_CAPTURE
#include "dynlib.h"
#include "parser.h"
#include "auth.h"

/*!
 * @class       CProtocol
 * @brief       protocol base class
 * @details     abstract base class for protocol interface
 */
class LIBDSLTOOL_EXPORT CProtocol
{
public:
/*!
* @enum        EProtocol
* @brief       ethernet type (ethtype)
*/
    enum EProtocol {
            eDemo = 0,                  //!< demo protocol dummy
#ifdef WITH_TELNET
            eTelnet,                    //!< telnet protocol
#endif // WITH_TELNET
#ifdef WITH_SSH
            eSSH,                       //!< SSH protocol
#endif // WITH_SSH
#ifdef WITH_HTTP
            eHTTP,                      //!< HTTP protocol
#endif // WITH_HTTP
#ifdef WITH_PCAP
#ifdef WITH_TELNET
            ePcapTelnet,                //!< telnet capture file
#endif // WITH_TELNET
#ifdef WITH_SSH
            ePcapSSH,                   //!< SSH capture file
#endif // WITH_SSH
#ifdef WITH_HTTP
            ePcapHTTP,                  //!< HTTP capture file
#endif // WITH_HTTP
#endif // WITH_PCAP
        };

/*!
* @enum        EEthType
* @brief       ethernet type (ethtype)
*/
    enum EEthType
    {
        eAuto = 0,                      //!< automatic selection
        eIPv4,                          //!< IPv4
        eIPv6                           //!< IPv6
    };

protected:
    CProtocol (const char *strHost,
            int nPort,
            EEthType eEthType,
            CAuth *pAuth);

private:
    //! @cond
    CProtocol (CProtocol const &cProtocol);
    CProtocol &operator = (CProtocol const &cProtocol);
    //! @endcond

public:
    virtual ~CProtocol (void);

    static const char *GetProtocol (EEthType eProtocol);
    static EEthType GetProtocol (const CString &strCommand);
    static bool ProtocolValid (const CString &strCommand);

    const char *Host (void);
    int Port (void);
    virtual CAuth *Auth (void);

/*!
 * @fn          virtual bool Open (void)
 * @brief       open protocol
 * @details     pure virtual function, see derived classes
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              open failed
 */
    virtual bool Open (void) = 0;
/*!
 * @fn          virtual void Close (void)
 * @brief       close protocol
 * @details     pure virtual function, see derived classes
 */
    virtual void Close (void) = 0;
/*!
 * @fn          virtual bool Process (void)
 * @brief       protocol process loop
 * @details     pure virtual function, see derived classes
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              process failed
 */
    virtual bool Process (void) = 0;

    virtual bool Send (const char *strText, const char *strDump=NULL);

protected:
/*!
 * @fn          virtual bool SendBuf (const char *pBuffer, size_t nSize)
 * @brief       send data
 * @details     pure virtual function, see derived classes
 * @param[in]   pBuffer
 *              data to send
 * @param[in]   nSize
 *              data length
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              send failure
 */
    virtual bool SendBuf (const char *pBuffer, size_t nSize) = 0;

public:
#ifdef ENABLE_CAPTURE
/*!
 * @fn          virtual void Capture (const char *strFile)
 * @brief       create capture file
 * @details     pure virtual function, see derived classes
 * @param[in]   strFile
 *              capture file name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              create capture failure
 */
    virtual bool Capture (const char *strFile) = 0;
#endif // ENABLE_CAPTURE

public:
    void ParserOpen (CParser::EContent eContent);
    void ParserClose (void);
    void Parse (const char *strData, size_t nRecv);
    void Next (CParser *pNext);
/*!
 * @fn          void Stop (void)
 * @brief       stop process loop
 */
    void Stop (void) {m_bLoop = false;}

protected:
    const char *m_strHost;              //!< host name or IP
    int m_nPort;                        //!< port number
    CProtocol::EEthType m_eEthType;     //!< protocol type
    CAuth              *m_pAuth;        //!< authentication pointer
    CParser            *m_pParser;      //!< parser pointer
    bool                m_bLoop;        //!< loop flag
#ifdef ENABLE_CAPTURE
public:
    CCapture            m_cCapture;     //!< capture
#endif // ENABLE_CAPTURE
}; // class CProtocol

/*!
 * @class       CProtocolFactory
 * @brief       protocol factory class
 * @details     singleton to create protocol class
 */
class LIBDSLTOOL_EXPORT CProtocolFactory
{
private:
    CProtocolFactory (void);

public:
    virtual ~CProtocolFactory (void);

private:
    //! @cond
    CProtocolFactory (const CProtocolFactory &cProtocolFactory);
    CProtocolFactory &operator = (const CProtocolFactory &cProtocolFactory);
    //! @endcond

public:
    static CProtocolFactory g_cProtocolFactory;

    bool Load (const char *strProtocol);
    void Unload (void);

    bool TestVersion (const char *strProtocol);
    CProtocol *Create (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth,
            const char *strExtra);

private:
/*!
 * @typedef     TpfnVersion
 * @brief       get version function pointer
 * @param[out]  nMajor
 *              major version number
 * @param[out]  nMinor
 *              minor version number
 * @param[out]  nSub
 *              sub version number
 */
    typedef void (*TpfnVersion)(int &nMajor, int &nMinor, int &nSub);
/*!
 * @typedef     TpfnCreate
 * @brief       create protocol object function pointer
 * @param[in]   strHost
 *              host name
 * @param[in]   nPort
 *              port number
 * @param[in]   eEthType
 *              protocol type
 * @param[in]   pAuth
 *              authenticator pointer
 * @param[in]   strExtra
 *              extra parameter (or NULL)
 * @return      protocol pointer
 */
    typedef CProtocol *(*TpfnCreate)(const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth,
            const char *strExtra);

    CDynLib             m_cProtocol;    //!< protocol library
    TpfnVersion         m_pfnVersion;   //!< get version function pointer
    TpfnCreate          m_pfnCreate;    //!< create protocol function pointer
}; // class CProtocolFactory

extern "C"
{
#ifndef LIBDSLTOOL_PROTOCOL_EXPORT
#define LIBDSLTOOL_PROTOCOL_EXPORT
#endif // LIBDSLTOOL_PROTOCOL_EXPORT
/*!
 * @fn          void ProtocolVersion (int &nMajor, int &nMinor, int &nSub)
 * @brief       get version
 * @param[out]  nMajor
 *              major version number
 * @param[out]  nMinor
 *              minor version number
 * @param[out]  nSub
 *              sub version number
 */
    void LIBDSLTOOL_PROTOCOL_EXPORT ProtocolVersion (int &nMajor, int &nMinor, int &nSub);
/*!
 * @fn          CProtocol *ProtocolCreate (const char *strHost,
 *                      int nPort,
 *                      CProtocol::EEthType eEthType,
 *                      CAuth *pAuth,
 *                      const char *strExtra)
 * @details     create protocol object
 * @param[in]   strHost
 *              host name
 * @param[in]   nPort
 *              port number
 * @param[in]   eEthType
 *              protocol type
 * @param[in]   pAuth
 *              authenticator pointer
 * @param[in]   strExtra
 *              extra parameter (or NULL)
 * @return      protocol pointer
 */
    CProtocol LIBDSLTOOL_PROTOCOL_EXPORT *ProtocolCreate (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth,
            const char *strExtra);
} // extern "C"

#endif // _DSLTOOL_PROTOCOL_H_
//_oOo_
