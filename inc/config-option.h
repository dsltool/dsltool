/*!
 * @file        config-option.h
 * @brief       application class definitions
 * @details     config option class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        29.04.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_CONFIG_OPTION_H__
#define __DSLTOOL_CONFIG_OPTION_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "modem.h"
#include "protocol.h"
#include "log.h"

#include <regex.h>

/*!
 * @class       CConfigOption
 * @brief       configuration list entry
 * @details     short and long options and config file line
 */
class LIBDSLTOOL_EXPORT CConfigOption
{
public:
/*!
 * @enum        EFlags
 * @brief       configuration flags
 */
    enum EFlags {
        eFlagsOptional = 0,         //!< option is optional
        eFlagsRequired = 1,         //!< option is required
        eFlagsDisabled = 2,         //!< option is disabled
        eFlagsNoShort = 4,          //!< no short option name
        eFlagsNoLong = 8,           //!< no long option name
        eFlagsNoConfig = 16,        //!< not written to config
        eFlagsInc = 32              //!< value is incremented
    };

/*!
 * @enum        EArg
 * @brief       argument type
 */
    enum EArg {
        eArgNone = 0,               //!< option has no argument
        eArgRequired = 1,           //!< option has required argument
        eArgOptional = 2            //!< option has optional argument
    };

/*!
 * @enum        EDataType;
 * @brief       option data type
 */
    enum EDataType {
        eTypeString,                //!< string
        eTypeBool,                  //!< bool (may increment)
        eTypeInt,                   //!< integer
        eTypeUInt,                  //!< unsigned integer
        eTypeLogLevel,              //!< log level enum
        eTypeEthType,               //!< eth type enum
        eTypeCommand                //!< command enum
    };

public:
    CConfigOption (char cShort,
            const char* strLong,
            const char* strHelp,
            const char* strArg,
            unsigned long ulFlags,
            EArg eArg,
            EDataType eType);
    ~CConfigOption (void);

private:
    //! @cond
    CConfigOption (CConfigOption const &cConfigOption);
    CConfigOption &operator = (CConfigOption const &cConfigOption);
    //! @endcond

public:
    bool Disable (void);
    unsigned long GetFlags (void);
    int GetShort (void);
    const char *GetLong (void);
    EArg GetOptArg ();

private:
    bool TestArg (const char* strValue);

public:
    void Reset (void);
    bool SetOption (const char* strValue);
    bool SetConfig (const char* strValue);
    void WriteConfig (CFile &cFile);

    bool Parse (const char* strTest, CString& strValue);
    bool TestRequired (bool bSilent);

    void GetValue (bool &bValue);
    void GetValue (CString& cValue);
    void GetValue (int &nValue);
    void GetValue (unsigned long &ulValue);
    void GetValue (CLog::ELevel &eValue);
    void GetValue (CProtocol::EEthType &eValue);
    void GetValue (CModem::ECommand &eValue);

    void PrintHelp (void);

private:
    char m_cShort;                      //!< short option
    CString m_strLong;                  //!< long option
    const char* m_strHelp;              //!< help text
    const char* m_strArg;               //!< argument name
    unsigned long m_ulFlags;            //!< flags
    EArg m_eArg;                        //!< arguments
    EDataType m_eType;                  //!< data type

    CString m_strRegEx;                 //!< regular expressions
    regex_t m_tRegex;                   //!< compiled regular expression

    int m_nInc;                         //!< increment value
    bool m_bOption;                     //!< option argument value valid
    CString m_strOption;                //!< option argument value
    bool m_bConfig;                     //!< config file value valid
    CString m_strConfig;                //!< config file value
}; // class CConfigOption

#endif // __DSLTOOL_CONFIG_OPTION_H__
//_oOo_
