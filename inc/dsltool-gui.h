/*!
 * @file        dsltool-gui.h
 * @brief       application class definitions
 * @details     dsltool Qt gui application
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DSLTOOL_GUI_H__
#define __DSLTOOL_DSLTOOL_GUI_H__

#include "oneshot.h"

#include <QApplication>

#include "gui-main.h"

/*!
* @class       CDslToolGui
* @brief       main application class
*/
class CDslToolGui : public CRunOneShot
{
    friend class CGuiMain;

public:
    CDslToolGui (int &nArgc, char **astrArgv);

private:
    //! @cond
    CDslToolGui (const CDslToolGui &cDslToolGui);
    CDslToolGui &operator = (const CDslToolGui &cDslToolGui);
    //! @endcond

public:
    int Main (int nArgc, char **astrArgv);
    void Refresh (CModem::ECommand eCommand = CModem::eInfo);

    void ConfigChanged (bool bSave);

private:
    QApplication m_cApp;    //!< Qt application
    CGuiMain m_cGuiMain;    //!< Qt main window
    bool m_bConfigValid;    //!< configuration valid flag
    bool m_bConfigDirty;    //!< configuration changed flag
    bool m_bConfigSaved;    //!< configuration unsaved flag
}; // class CDslToolGui

#endif // __DSLTOOL_DSLTOOL_GUI_H__
//_oOo_
