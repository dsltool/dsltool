/*!
 * @file        parser.h
 * @brief       parser base class definitions
 * @details     pure virtual parser classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_PARSER_H__
#define __DSLTOOL_PARSER_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

class CModem;
/*!
 * @class       CParser
 * @brief       parser base class
 */
class LIBDSLTOOL_EXPORT CParser
{
protected:
    CParser (CModem *pModem);

private:
    //! @cond
    CParser (CParser const &cParser);
    CParser &operator = (CParser const &cParser);
    //! @endcond

public:
/*!
* @enum        EContent
* @brief       content type
*/
    enum EContent
    {
        eContentUnknown,        //!< unknown content
        eTextXml,               //!< text/xml
        eTextHtml               //!< text/html
    };

public:
    virtual ~CParser (void);
    virtual void Open (EContent eContent);
    virtual CParser *Close (void);

/*!
 * @fn          virtual CParser *Parse (const char *strRecv, size_t nRecv)
 * @brief       parse received data
 * @details     pure virtual function, see derived classes
 * @param[in]   strRecv
 *              received data
 * @param[in]   nRecv
 *              receive data length
 * @return      next parser
 */
    virtual CParser *Parse (const char *strRecv, size_t nRecv) = 0;

protected:
    static bool Find (const char *strTest, size_t nTest, const char *strFind);

protected:
    CModem *m_pModem;                   //!< modem pointer
}; // class CParser

/*!
 * @class       CLineParser
 * @brief       line parser base class
 * @details     this parser strips CR and LF and breaks there
 */
class LIBDSLTOOL_EXPORT CLineParser : public CParser
{
protected:
    CLineParser (CModem *pModem);

private:
    //! @cond
    CLineParser (CLineParser const &cLineParser);
    CLineParser &operator = (CLineParser const &cLineParser);
    //! @endcond

public:
    virtual ~CLineParser (void);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);
/*!
 * @fn          virtual CParser *ParseLine (const char *strLine)
 * @brief       parse received line
 * @details     pure virtual function, see derived classes
 * @param[in]   strLine
 *              received line
 * @return      next parser
 */
    virtual CParser *ParseLine (const char *strLine) = 0;

private:
    char *m_strLine;                    //!< line buffer
    size_t m_nSize;                     //!< line buffer size
    size_t m_nLine;                     //!< actual line length
}; // class CLineParser

#endif // __DSLTOOL_PARSER_H__
//_oOo_
