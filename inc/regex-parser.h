/*!
 * @file        regex-parser.h
 * @brief       regular expression parser class definitions
 * @details     regular expression classes and template parser class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_REGEX_PARSER_H__
#define __DSLTOOL_REGEX_PARSER_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include <regex.h>

#include "protocol.h"
#include "modem.h"

/*!
 * @class       CRegExBase
 * @brief       regular expression base class
 */
class LIBDSLTOOL_EXPORT CRegExBase
{
protected:
    CRegExBase (void);
public:
    virtual ~CRegExBase (void);

private:
    //! @cond
    CRegExBase (CRegExBase const &cRegExBase);
    CRegExBase &operator = (CRegExBase const &cRegExBase);
    //! @endcond

public:
/*!
 * @fn          CRegExBase *Next (void)
 * @brief       get next list element
 * @return      next pointer
 */
    CRegExBase *Next (void) {return m_pNext;}
/*!
 * @fn          void Next (CRegExBase *pNext)
 * @brief       set next list element
 * @param[in]   pNext
 *              next pointer
 */
    void Next (CRegExBase *pNext) {m_pNext = pNext;}

    const char *RegEx (void);
    size_t RegExLength (void);

/*!
 * @fn          virtual bool Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              string to parse
 * @param[in]   nLength
 *              length of string
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              parse failed
 */
    virtual bool Parse (const char *strText, size_t nLength) = 0;

protected:
    void RegExInt (int nBase, size_t nLength);

protected:
    CRegExBase *m_pNext;                //!< next list element
    CString m_strRegEx;                 //!< regular expression
}; //class CRegExBase

/*!
 * @class       CRegExSep
 * @brief       separator expression
 */
class LIBDSLTOOL_EXPORT CRegExSep: public CRegExBase
{
public:
    CRegExSep (bool bForce = false);
    virtual bool Parse (const char *strText, size_t nLength);

private:
    //! @cond
    CRegExSep (const CRegExSep &cRegExSep);
    CRegExSep &operator = (const CRegExSep &cRegExSep);
    //! @endcond
}; // class CRegExSep

/*!
 * @class       CRegExConst
 * @brief       const expression
 */
class LIBDSLTOOL_EXPORT CRegExConst : public CRegExBase
{
public:
    CRegExConst (const char *strRegEx);
    virtual bool Parse (const char *strText, size_t nLength);

private:
    //! @cond
    CRegExConst (const CRegExConst &cRegExConst);
    CRegExConst &operator = (const CRegExConst &cRegExConst);
    //! @endcond
}; // class CRegExConst

/*!
 * @class       CRegExTerm
 * @brief       terminal escape const expression
 */
class LIBDSLTOOL_EXPORT CRegExTerm : public CRegExBase
{
public:
    CRegExTerm (const char *strTerm);
    virtual bool Parse (const char *strText, size_t nLength);

private:
    //! @cond
    CRegExTerm (const CRegExTerm &cRegExTerm);
    CRegExTerm &operator = (const CRegExTerm &cRegExTerm);
    //! @endcond
}; // class CRegExTerm

/*!
 * @class       CRegExInt
 * @brief       integer expression
 */
class LIBDSLTOOL_EXPORT CRegExInt : public CRegExBase
{
public:
    CRegExInt (long &nData, int nBase = 0, size_t nLength = 0);
    virtual bool Parse (const char *strText, size_t nLength);

private:
    //! @cond
    CRegExInt (const CRegExInt &cRegExInt);
    CRegExInt &operator = (const CRegExInt &cRegExInt);
    //! @endcond

private:
    long &m_nData;                      //!< data reference
    int m_nBase;                        //!< integer base (0, 8, 10, 16)
}; // class CRegExInt

/*!
 * @class       CRegExDouble
 * @brief       double expression
 */
class LIBDSLTOOL_EXPORT CRegExDouble : public CRegExBase
{
public:
    CRegExDouble (double &fData);
    virtual bool Parse (const char *strText, size_t nLength);

private:
    //! @cond
    CRegExDouble (const CRegExDouble &cRegExDouble);
    CRegExDouble &operator = (const CRegExDouble &cRegExDouble);
    //! @endcond

private:
    double &m_fData;                    //!< data reference
}; // class CRegExDouble

/*!
 * @class       CRegExString
 * @brief       string expression
 */
class LIBDSLTOOL_EXPORT CRegExString : public CRegExBase
{
public:
    CRegExString (CString &strData, const char *strRegEx = NULL);
    virtual bool Parse (const char *strText, size_t nLength);

private:
    //! @cond
    CRegExString (const CRegExString &cRegExString);
    CRegExString &operator = (const CRegExString &cRegExString);
    //! @endcond

private:
    CString &m_strData;                 //!< date reference
}; // class CRegExString

/*!
 * @class       CRegExWord
 * @brief       word expression
 */
class LIBDSLTOOL_EXPORT CRegExWord: public CRegExString
{
public:
    CRegExWord (CString &strData);

private:
    //! @cond
    CRegExWord (const CRegExWord &cRegExWord);
    CRegExWord &operator = (const CRegExWord &cRegExWord);
    //! @endcond
}; // class CRegExWord

/*!
 * @class       CRegExTone
 * @brief       tone expression
 */
class LIBDSLTOOL_EXPORT CRegExTone: public CRegExBase
{
public:
    CRegExTone (CTone &cTone, int nBase = 0, size_t nLength = 0);
    virtual bool Parse (const char *strText, size_t nLength);

private:
    //! @cond
    CRegExTone (const CRegExTone &cRegExTone);
    CRegExTone &operator = (const CRegExTone &cRegExTone);
    //! @endcond

protected:
    CTone &m_cTone;                     //!< tone reference
    int m_nBase;                        //!< integer base
    size_t m_nLength;                   //!< string length
}; // class CRegExTone

/*!
 * @class       CRegExToneIndex
 * @brief       tone index expression
 */
class LIBDSLTOOL_EXPORT CRegExToneIndex: public CRegExTone
{
public:
    CRegExToneIndex (CTone &cTone, int nBase = 0, size_t nLength = 0);
    virtual bool Parse (const char *strText, size_t nLength);

private:
    //! @cond
    CRegExToneIndex (const CRegExToneIndex &cRegExToneIndex);
    CRegExToneIndex &operator = (const CRegExToneIndex &cRegExToneIndex);
    //! @endcond
}; // class CRegExToneIndex

/*!
 * @class       CRegExList
 * @brief       regular expression list
 *
 */
class LIBDSLTOOL_EXPORT CRegExList
{
public:
    CRegExList (void);
    ~CRegExList (void);

private:
    //! @cond
    CRegExList (const CRegExList &cRegExList);
    CRegExList &operator = (const CRegExList &cRegExList);
    //! @endcond

private:
    bool Add (CRegExBase *pRegEx);

public:
    bool AddSep (bool bForce = false);
    bool AddConst (const char *strRegEx);
    bool AddTerm (const char *strTerm);
    bool AddInt (long &nData, int nBase = 0, size_t nLength = 0);
    bool AddDouble (double &fData);
    bool AddString (CString &strData, const char *strRegEx = NULL);
    bool AddWord (CString &strData);
    bool AddTone (CTone &cTone, int nBase = 0, size_t nLength = 0);
    bool AddToneIndex (CTone &cTone, int nBase = 0, size_t nLength = 0);
    bool Init (void);
    bool Parse (const char *strTest);

private:
    CRegExBase *m_pHead;                //!< regular expression list head
    CRegExBase *m_pTail;                //!< regular expression list tail
    size_t m_nCount;                    //!< regular expression list length
    char *m_strRegEx;                   //!< concated regular expressions
    regex_t m_tRegex;                   //!< compiled regular expression
};

/*!
 * @class       CRegExCallback
 * @brief       regular expression callback list
 * @tparam      T
 *              CCmdParser derived class
 */
template <class T> class CRegExCallback
{
public:
/*!
 * @fn          CRegExCallback (CRegExList &cRegExParser,
 *                      T *pParser,
 *                      CParser *(T::*pfnCallback)(void),
 *                      int nTest)
 * @brief       constructor
 * @param[in]   cRegExParser
 *              regular expression parser reference
 * @param[in]   pParser
 * @param[in]   pfnCallback
 * @param[in]   nTest
 *              state test condition
 */
    CRegExCallback (CRegExList &cRegExParser,
            T *pParser,
            CParser *(T::*pfnCallback)(void),
            int nTest)
    : m_pNext (NULL)
    , cRegExParser (cRegExParser)
    , m_pParser (pParser)
    , m_pfnCallback (pfnCallback)
    , m_nTest (nTest)
    {
    }

/*!
 * @fn          CRegExCallback *Next (void)
 * @brief       get next list element
 * @return      next list element
 */
    CRegExCallback *Next (void) {return m_pNext;}
/*!
 * @fn          void Next (CRegExCallback *pNext)
 * @brief       set next list element
 * @param[in]   pNext
 *              next list element
 */
    void Next (CRegExCallback *pNext) {m_pNext = pNext;}
/*!
 * @fn          int Test (void)
 * @brief       get state test condition
 * @return      state test condition
 */
    int Test (void) {return m_nTest;}

/*!
 * @fn          bool Parse (const char *strTest)
 * @brief       match string to regular expression
 * @details     assign all found values to variables
 * @param[in]   strTest
 *              string to test
 * @return      success
 * @retval      true
 *              match
 * @retval      false
 *              mismatch
 */
    bool Parse (const char *strTest) {return cRegExParser.Parse (strTest);}

/*!
 * @fn          Parser *Callback (void)
 * @brief       callback parser
 * @details     step to next parser
 * @return      next parser
 */
    CParser *Callback (void) {return (m_pParser->*m_pfnCallback) ();}

private:
    CRegExCallback *m_pNext;            //!< next list element
    CRegExList &cRegExParser;         //!< regular expression parser
    T *m_pParser;                       //!< @ref CRegExCmdParser derived parser pointer
    CParser *(T::*m_pfnCallback)(void); //!< parser callback function
    int m_nTest;                        //!< state test condition
}; // class CRegExCallback

/*!
 * @class       CRegExCmdParser
 * @brief       command parser
 * @details     send command in constructor
 * @tparam      T
 *              CRegExCmdParser derived class
 * @tparam      B
 *              CLineParser derived base class
 */
template <class T, class B> class CRegExCmdParser: public B
{
protected:
/*!
 * @fn          CRegExCmdParser (CModem *pModem, const char *strCmd)
 * @brief       constructor
 * @param[in]   pModem
 *              modem pointer
 * @param[in]   strCmd
 *              command string
 */
    CRegExCmdParser (CModem *pModem, const char *strCmd)
    : B (pModem)
    , m_pHead (NULL)
    , m_pTail (NULL)
    , m_nState (0)
    {
        pModem->Protocol ()->Send (strCmd);
        pModem->Protocol ()->Send (pModem->LF ());
    }
/*!
 * @fn          ~CRegExCmdParser (void)
 * @brief       destructor
 * @details     clean up regular expression list
 */
    ~CRegExCmdParser (void)
    {
        CRegExCallback<T> *pList;
        CRegExCallback<T> *pNext;

        for (pList = m_pHead; pList; pList = pNext)
        {
            pNext = pList->Next ();
            delete pList;
        }
    }
private:
    //! @cond
    CRegExCmdParser (CRegExCmdParser const &cRegExCmdParser);
    CRegExCmdParser &operator =(CRegExCmdParser const &cRegExCmdParser);
    //! @endcond

protected:
/*!
 * @fn          bool AddCallback (CRegExList &cRegExParser,
 *                      CParser *(T::*pfnCallback)(void),
 *                      int nTest = -1)
 * @brief       cRegExParser
 *              parser reference
 */
    bool AddCallback (CRegExList &cRegExParser,
            CParser *(T::*pfnCallback)(void),
            int nTest = -1)
    {
        bool bRet = false;
        CRegExCallback<T> *pList = new CRegExCallback<T> (cRegExParser, (T*)this, pfnCallback, nTest);

        if (pList)
        {
            if (!m_pHead)
            {
                m_pHead = pList;
            }
            if (m_pTail)
            {
                m_pTail->Next (pList);
            }
            m_pTail = pList;
            bRet = true;
        }

        return bRet;
    }

public:
/*!
 * @fn          virtual CParser *ParseLine (const char *strLine)
 * @brief       parse received line
 * @details     call callback function on match
 * @param[in]   strLine
 *              received line
 * @return      next parser
 */
    virtual CParser *ParseLine (const char *strLine)
    {
        CParser *pNext = this;
        CRegExCallback<T> *pList;

        for (pList = m_pHead; pList; pList = pList->Next ())
        {
            if ((-1 == pList->Test ()) || (m_nState == pList->Test ()))
            {
                if (pList->Parse (strLine))
                {
                    pNext = pList->Callback ();
                    break;
                }
            }
        }

        return pNext;
    }


private:
    CRegExCallback<T> *m_pHead;         //!< regular expression list head
    CRegExCallback<T> *m_pTail;         //!< regular expression list tail

protected:
    int m_nState;                       //!< parser state
}; // class CCmdParser


#endif // __DSLTOOL_REGEX_PARSER_H__
//_oOo_
