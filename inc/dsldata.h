/*!
 * @file        dsldata.h
 * @brief       DSL data definitions
 * @details     parser data storage
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DSLDATA_H__
#define __DSLTOOL_DSLDATA_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include <time.h>

#include "bandplan.h"

/*!
* @class       CData
* @brief       abstracte data base class
*/
class LIBDSLTOOL_EXPORT CData
{
protected:
    CData (bool bDiff);

private:
    //! @cond
    CData (const CData &cData);
    CData &operator = (const CData &cData);
    //! @endcond

protected:
/*!
* @fn          virtual ~CData (void)
* @brief       destructor
*/
    virtual ~CData (void) {}

/*!
* @fn          Init (void)
* @brief       initialize data
* @details     pure virtual function, see derived classes
*/
    virtual void Init (void) = 0;

public:
/*!
 * @fn          bool IsDiff (void) const
 * @brief       test if diff data type
 * @return      diff type
 */
    bool IsDiff (void) const {return m_bDiff;}

protected:
    const bool m_bDiff;                 //!< diff flag
}; // class CData

/*!
* @class       CDataInt
* @brief       integer data
*/
class LIBDSLTOOL_EXPORT CDataInt : public CData
{
public:
    CDataInt (bool bDiff = false);

private:
    //! @cond
    CDataInt (const CDataInt &cDataInt);
    CDataInt &operator = (const CDataInt &cDataInt);
    //! @endcond

public:
    virtual void Init (void);

    void Set (long nData);
    bool Get (long &nData) const;
    bool Diff (long &nData) const;

private:
    long m_nData;                       //!< data
    bool m_bValid;                      //!< data valid flag
    long m_nLastData;                   //!< last data
    bool m_bLastValid;                  //!< last data valid flag
}; // class CDataInt

/*!
* @class       CDataIntUpDown
* @brief       up/down integer data
* @details     pair of integer data
*/
class LIBDSLTOOL_EXPORT CDataIntUpDown : public CData
{
public:
    CDataIntUpDown (bool bDiff = false);

private:
    //! @cond
    CDataIntUpDown (const CDataIntUpDown &cDataIntUpDown);
    CDataIntUpDown &operator = (const CDataIntUpDown &cDataIntUpDown);
    //! @endcond

public:
    virtual void Init (void);

    void Set (long nUp,
            long nDown);
    void SetUp (long nUp);
    void SetDown (long nDown);
    bool GetUp (long &nUp) const;
    bool GetDown (long &nDown) const;
    bool DiffUp (long &nData) const;
    bool DiffDown (long &nData) const;

private:
    long m_nUp;                         //!< upstream (Tx) data
    long m_nDown;                       //!< downstream (Rx) data
    bool m_bUpValid;                    //!< upstream (Tx) data valid flag
    bool m_bDownValid;                  //!< downstream (Rx) data valid flag
    long m_nLastUp;                     //!< last upstream (Tx) data
    long m_nLastDown;                   //!< last downstream (Rx) data
    bool m_bLastUpValid;                //!< last upstream (Tx) data valid flag
    bool m_bLastDownValid;              //!< last upstream (Tx) data valid flag
}; // class CDataInt

/*!
* @class       CDataDouble
* @brief       double data
*/
class LIBDSLTOOL_EXPORT CDataDouble: public CData
{
public:
    CDataDouble (bool bDiff = false);

private:
    //! @cond
    CDataDouble (const CDataDouble &cDataDouble);
    CDataDouble &operator = (const CDataDouble &cDataDouble);
    //! @endcond

public:
    virtual void Init (void);

    void Set (double fData);
    bool Get (double &fData) const;
    bool Diff (double &fData) const;

private:
    double m_fData;                     //!< data
    bool m_bValid;                      //!< data valid flag
    double m_fLastData;                 //!< last data
    bool m_bLastValid;                  //!< last data valid flag
}; // class CDataDouble

/*!
* @class       CDataDoubleUpDown
* @brief       up/down double data
* @details     pair of double data
*/
class LIBDSLTOOL_EXPORT CDataDoubleUpDown : public CData
{
public:
    CDataDoubleUpDown (bool bDiff = false);

private:
    //! @cond
    CDataDoubleUpDown (const CDataDoubleUpDown &cDataDoubleUpDown);
    CDataDoubleUpDown &operator = (const CDataDoubleUpDown &cDataDoubleUpDown);
    //! @endcond

public:
    virtual void Init (void);

    void Set (double fUp, double fDown);
    void SetUp (double fUp);
    void SetDown (double fDown);
    bool GetUp (double &fUp) const;
    bool GetDown (double &fDown) const;
    bool DiffUp (double &fUp) const;
    bool DiffDown (double &fDown) const;

private:
    double m_fUp;                       //!< upstream (Tx) data
    double m_fDown;                     //!< downstream (Rx) data
    bool m_bUpValid;                    //!< upstream data valid flag
    bool m_bDownValid;                  //!< downstream data valid flag
    double m_fLastUp;                   //!< last upstream data
    double m_fLastDown;                 //!< last downstream data
    bool m_bLastUpValid;                //!< last upstream data valid flag
    bool m_bLastDownValid;              //!< last downstream data valid flag
}; // class CDataDoubleUpDown

/*!
* @class       CArrayDoubleUpDown
* @brief       up/down double data array
* @details     pair of double data arrays
*/
class LIBDSLTOOL_EXPORT CArrayDoubleUpDown : public CData
{
public:
    CArrayDoubleUpDown (size_t nSize,
            bool bDiff = false);
    virtual ~CArrayDoubleUpDown (void);

private:
    //! @cond
    CArrayDoubleUpDown (CArrayDoubleUpDown const &cArrayDoubleUpDown);
    CArrayDoubleUpDown &operator = (CArrayDoubleUpDown const &cArrayDoubleUpDown);
    //! @endcond

public:
    virtual void Init (void);

/*!
 * @fn          int Size (void) const
 * @brief       get array size
 * @return      array size
 */
    size_t Size (void) const {return m_nSize;}
    void Set (size_t nIndex, double fUp, double fDown);
    void SetUp (size_t nIndex, double fUp);
    void SetDown (size_t nIndex, double fDown);
    bool GetUp (size_t nIndex, double &fUp) const;
    bool GetDown (size_t nIndex, double &fDown) const;
    bool DiffUp (size_t nIndex, double &fUp) const;
    bool DiffDown (size_t nIndex, double &fDown) const;

private:
    size_t m_nSize;                     //!< array size
    double *m_afUp;                     //!< upstream (Tx) data
    double *m_afDown;                   //!< downstream (Rx) data
    bool *m_abUpValid;                  //!< upstream data valid flag
    bool *m_abDownValid;                //!< downstream data valid flag
    double *m_afLastUp;                 //!< last upstream data
    double *m_afLastDown;               //!< last downstream data
    bool *m_abLastUpValid;              //!< last upstream data valid flag
    bool *m_abLastDownValid;            //!< last downstream data valid flag
}; // class CArrayDoubleUpDown

/*!
* @class       CDataString
* @brief       string data
*/
class LIBDSLTOOL_EXPORT CDataString : public CData
{
public:
    CDataString (void);

private:
    //! @cond
    CDataString (const CDataString &cDataString);
    CDataString &operator = (const CDataString &cDataString);
    //! @endcond

public:
    virtual void Init (void);

    void Set (const char *strData);
    bool Get (CString &strData) const;

private:
    CString m_strData;                  //!< data
    bool m_bValid;                      //!< data valid flag
}; // class CDataString

/*!
* @class       CDataATU
* @brief       ATU data
*/
class LIBDSLTOOL_EXPORT CDataATU : public CData
{
public:
    CDataATU (void);

private:
    //! @cond
    CDataATU (const CDataATU &cDataATU);
    CDataATU &operator = (const CDataATU &cDataATU);
    //! @endcond

public:
    virtual void Init (void);

    void Set (const char *strVendor, int nSpec0, int nSpec1, int nRevision);
    void SetVendor (const char *strVendor);
    void SetSpec (int nSpec0, int nSpec1);
    void SetRevision (int nRevision);
    bool GetVendor (CString &strVendor) const;
    bool GetSpec (int &nSpec0, int &nSpec1) const;
    bool GetRevision (int &nRevision) const;

private:
    char m_strVendor[5];                //!< vendor code
    bool m_bVendorValid;                //!< vendor code valid flag
    int m_nSpec[2];                     //!< vendor sepcific
    bool m_bSpecValid;                  //!< vendor sepcific valid flag
    int m_nRevision;                    //!< revision
    bool m_bRevisionValid;              //!< revision valid flag

}; // class CDataATU

/*!
* @class       CDataTone
* @brief       tone data
*/
class LIBDSLTOOL_EXPORT CDataTone : public CData
{
public:
    CDataTone (void);

private:
    //! @cond
    CDataTone (const CDataTone &cDataTone);
    CDataTone &operator = (const CDataTone &cDataTone);
    //! @endcond

public:
    virtual void Init (void);

    void Size (size_t nSize);

    void Set (CTone &cData, size_t nMin = 0, size_t nMax = 0);
    void Merge (CTone &cData, size_t nMin = 0, size_t nMax = 0);
    void Set (int *pnData, size_t nSize);
    void Set (size_t nIndex, int nData);

/*!
* @fn          bool Valid (void) const
* @brief       get valid flag
* @return      valid flag
*/
    bool Valid (void) const {return m_bValid;}
/*!
* @fn          CTone &Data (void)
* @brief       get tone data
* @return      tone date reference
*/
    CTone &Data (void) {return m_cData;}
/*!
* @fn          const CTone &Data (void) const
* @brief       get tone data
* @return      tone date reference
*/
    const CTone &Data (void) const {return m_cData;}

private:
    CTone m_cData;                      //!< data
    bool m_bValid;                      //!< data valid flag
}; // class CDataTone

/*!
* @class       CDataGroup
* @brief       abstract data group base class
*/
class LIBDSLTOOL_EXPORT CDataGroup
{
protected:
    CDataGroup (void) {}

public:
    virtual ~CDataGroup (void) {}

private:
    //! @cond
    CDataGroup (const CDataGroup &cDataGroup);
    CDataGroup &operator = (const CDataGroup &cDataGroup);
    //! @endcond

public:
/*!
* @fn          Init (void)
* @brief       initialize data
* @details     pure virtual function, see derived classes
*/
    virtual void Init (void) = 0;

}; // class CDataGroup

/*!
* @class       CBandwidthGroup
* @brief       bandwidth data group
*/
class LIBDSLTOOL_EXPORT CBandwidthGroup : public CDataGroup
{
public:
    CBandwidthGroup (void);
    virtual ~CBandwidthGroup (void) {}

private:
    //! @cond
    CBandwidthGroup (const CBandwidthGroup &cBandwidthGroup);
    CBandwidthGroup &operator = (const CBandwidthGroup &cBandwidthGroup);
    //! @endcond

public:
    virtual void Init (void);

public:
    CDataIntUpDown m_cCells;            //!< bandwidth [cells/s]
    CDataIntUpDown m_cKbits;            //!< bandwidth [kBits/s]
    CDataIntUpDown m_cMaxKbits;         //!< max. bandwidth [kBits/s]
}; // class CBandWidth

/*!
* @class       CErrorGroup
* @brief       error data group
*/
class LIBDSLTOOL_EXPORT CErrorGroup : public CDataGroup
{
public:
    CErrorGroup (void);
    virtual ~CErrorGroup (void) {}

private:
    //! @cond
    CErrorGroup (const CErrorGroup &cErrorGroup);
    CErrorGroup &operator = (const CErrorGroup &cErrorGroup);
    //! @endcond

public:
    virtual void Init (void);

public:
    CDataIntUpDown m_cFEC;              //!< FEC errors
    CDataIntUpDown m_cCRC;              //!< CRC errors
    CDataIntUpDown m_cHEC;              //!< HEC errors
}; // class CError

/*!
* @class       CFailureGroup
* @brief       failure data group
*/
class LIBDSLTOOL_EXPORT CFailureGroup : public CDataGroup
{
public:
    CFailureGroup (void);
    virtual ~CFailureGroup (void) {}

private:
    //! @cond
    CFailureGroup (const CFailureGroup &cFailureGroup);
    CFailureGroup &operator = (const CFailureGroup &cFailureGroup);
    //! @endcond

public:
    virtual void Init (void);

public:
    CDataInt m_cErrorSecs15min;         //!< errored seconds per 15min
    CDataInt m_cErrorSecsDay;           //!< errored seconds per day
}; // class CFailure

/*!
* @class       CStatisticsGroup
* @brief       statistics data group
*/
class LIBDSLTOOL_EXPORT CStatisticsGroup : public CDataGroup
{
public:
    CStatisticsGroup (void);
    virtual ~CStatisticsGroup (void) {}

private:
    //! @cond
    CStatisticsGroup (const CStatisticsGroup &cStatisticsGroup);
    CStatisticsGroup &operator = (const CStatisticsGroup &cStatisticsGroup);
    //! @endcond

public:
    virtual void Init (void);

public:
    CErrorGroup m_cError;               //!< error group
    CFailureGroup m_cFailure;           //!< failure group
}; // class CStatistics

/*!
* @class       CAtmGroup
* @brief       ATM data group
*/
class LIBDSLTOOL_EXPORT CAtmGroup : public CDataGroup
{
public:
    CAtmGroup (void);
    virtual ~CAtmGroup (void) {}

private:
    //! @cond
    CAtmGroup (const CAtmGroup &cAtmGroup);
    CAtmGroup &operator = (const CAtmGroup &cAtmGroup);
    //! @endcond

public:
    virtual void Init (void);

public:
    CDataIntUpDown m_cVpiVci;           //!< VPI/VCI
    CDataATU m_cATU_C;                  //!< ATU-C data
    CDataATU m_cATU_R;                  //!< ATU-R data
}; // class CAtm

/*!
* @class       CTonesGroup
* @brief       tones data group
*/
class LIBDSLTOOL_EXPORT CTonesGroup : public CDataGroup
{
public:
    CTonesGroup (void);
    virtual ~CTonesGroup (void);

private:
    //! @cond
    CTonesGroup (const CTonesGroup &cTonesGroup);
    CTonesGroup &operator = (const CTonesGroup &cTonesGroup);
    //! @endcond

public:
    void Bandplan (CBandplan *pBandplan);
/*!
* @fn          CBandplan *Bandplan (void) const
* @brief       get bandplan
* @return      bandplan pointer
*/
    CBandplan *Bandplan (void) const {return m_pBandplan;}

    virtual void Init (void);

public:
    CDataTone m_cBitallocUp;            //!< upstream bit allocation
    CDataTone m_cBitallocDown;          //!< downstream bit allocation
    CDataTone m_cSNR;                   //!< signal noise ratio
    CDataTone m_cGainQ2;                //!< Gain Q2
    CDataTone m_cNoiseMargin;           //!< noise margin
    CDataTone m_cChanCharLog;           //!< channel char log

private:
    CBandplan *m_pBandplan;             //!< bandplan
}; //class CTones

/*!
* @class       CDslGroup
* @brief       DSL data group
*/
class LIBDSLTOOL_EXPORT CDslGroup : public CDataGroup
{
public:
    CDslGroup (void);
    virtual ~CDslGroup (void) {}

private:
    //! @cond
    CDslGroup (const CDslGroup &cDslGroup);
    CDslGroup &operator = (const CDslGroup &cDslGroup);
    //! @endcond

public:
    virtual void Init (void);

public:
    CDataInt m_cModemState;             //!< modem state
    CDataString m_cModemStateStr;       //!< modem state as string
    CDataString m_cOperationMode;       //!< operation mode
    CDataString m_cProfile;             //!< VDSL profile
    CDataString m_cChannelMode;         //!< channel mode
    CArrayDoubleUpDown m_cNoiseMargin;  //!< noise margin
    CArrayDoubleUpDown m_cAttenuation;  //!< attenuation
    CDataDoubleUpDown m_cTxPower;       //!< transmit power
    CBandwidthGroup m_cBandwidth;       //!< bandwidth group
    CAtmGroup m_cAtm;                   //!< ATM group
    CStatisticsGroup m_cStatistics;     //!< statistics group
    CTonesGroup m_cTones;               //!< tones group
};

/*!
* @class       CDslData
* @brief       DSL data container
*/
class LIBDSLTOOL_EXPORT CDslData
{
public:
    CDslData (void);

private:
    //! @cond
    CDslData (const CDslData &cDslData);
    CDslData &operator = (const CDslData &cDslData);
    //! @endcond

public:
    void Init (void);

    void Bandplan (CBandplan::EBandplan eBandplan);
/*!
* @fn          CBandplan *Bandplan (void)
* @brief       get bandplan
* @return      bandplan pointer
*/
    CBandplan *Bandplan (void) {return m_cData.m_cTones.Bandplan ();}
/*!
* @fn          time_t TimeStamp (void) const
* @brief       get timestamp
* @return      timestamp
*/
    time_t TimeStamp (void) const {return m_tTimeStamp;}

private:
    time_t m_tTimeStamp;                //!< timestamp

public:
    CDslGroup m_cData;                  //!< DSL group
}; // class CDslData

#endif // __DSLTOOL_DSLDATA_H__
//_oOo_
