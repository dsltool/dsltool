/*!
 * @file        capture.h
 * @brief       capture class definitions
 * @details     pcap file creation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        08.04.2019
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_CAPTURE_H__
#define __DSLTOOL_CAPTURE_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include <stdint.h>
#include <stddef.h>
#include <pcap.h>

#define CAPTURE_FRAME   1518        //!< Frame size
#define CAPTURE_MTU     1500        //!< MTU size (Frame size
                                    //!< - 14 byte ETH header
                                    //!< - 4 byte FCS)
#define CAPTURE_DATA    1460        //!< Data size (MTU size
                                    //!< - 20 byte IP header
                                    //!< - 20 byte TCP header)
/*!
 * @class       CCapture
 * @brief       protocol capture file
 */
class LIBDSLTOOL_EXPORT CCapture
{
public:
    CCapture (void);
    virtual ~CCapture (void);

private:
    //! @cond
    CCapture (CCapture const &cDump);
    CCapture &operator = (CCapture const &cCapture);
    //! @endcond

public:
    bool Open (const char *strFile, int nPort);
    void Close (void);

private:
    /*!
    * @enum        EState
    * @brief       last dumped data
    */
        enum EState {
            eStateInit,             //!< unknown state
            eStateSend,             //!< send date
            eStateReceive,          //!< receive date
        };

private:
    void Dump (EState eState, uint8_t u8Flags, const void *pData = NULL, size_t nData = 0);
    void Flush (void);
    static uint16_t IPCheckSum (const struct iphdr *pIP);
    static uint16_t TCPCheckSum (const struct iphdr *pIP,
            const struct tcphdr *pTCP, const void *pBuffer, size_t nBuffer);

public:
    void Send (const void *pData, size_t nData);
    void Send (const char *strData);
    void Receive (const void *pData, size_t nData);
    void Receive (const char *strData);

private:
    pcap_t         *m_pPcap;        //!< pcap pointer
    pcap_dumper_t  *m_pDumper;      //!< pcap dumper pointer
    int             m_nPort;        //!< TCP port
    uint32_t        m_nTxSeq;       //!< transmit sequence
    uint32_t        m_nTxNext;      //!< transmit next
    uint32_t        m_nRxSeq;       //!< receive sequence
    uint32_t        m_nRxNext;      //!< receive next
    EState          m_eState;       //!< dump data state
    uint8_t         m_u8Flags;      //!< TCP flags
    size_t          m_nData;        //!< dump data size
    u_char          m_pData[CAPTURE_DATA];  //!< dump data
}; // class CCapture

#endif // __DSLTOOL_CAPTURE_H__
//_oOo_
