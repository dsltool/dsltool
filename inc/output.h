/*!
 * @file        output.h
 * @brief       output base class definitions
 * @details     abstract output and helper class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_OUTPUT_H__
#define __DSLTOOL_OUTPUT_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "config-data.h"

/*!
 * @class       COutput
 * @brief       output base class
 * @details     abstract base class for output interface
 */
class LIBDSLTOOL_EXPORT COutput
{
protected:
    COutput (void);

public:
    virtual ~COutput (void);

/*!
 * @fn          virtual bool Write (const CConfigData &cConfig,
 *                      const CDslData &cData,
 *                      bool bTones) = 0;
 * @brief       write data
 * @details     pure virtual function, see derived classes
 * @param[in]   cConfig
 *              configuration data
 * @param[in]   cData
 *              dsl data
 * @param[in]   bTones
 *              write tones
 */
    virtual void Write (const CConfigData &cConfig,
            const CDslData &cData,
            bool bTones) = 0;
}; // class COutput

/*!
* @class       COutputLib
* @brief       output library container
*/
class LIBDSLTOOL_EXPORT COutputLib
{
public:
/*!
 * @enum        EIndex
 * @brief       array index values
 */
    enum EIndex {
        eDat,       //!< .dat file
        eText,      //!< text output
        ePng,       //!< .png files
        eRrd,       //!< rrd/collectd data
        eApp,       //!< application specific
        eIndexMax   // no entry behind this
    };

public:
    COutputLib (EIndex eIndex, const char* m_strName, bool bTones);
    COutputLib (EIndex eIndex, COutput *pOutput, bool bTones);
    ~COutputLib (void);

private:
    //! @cond
    COutputLib (COutputLib const &cOutputLib);
    COutputLib operator = (COutputLib const &cOutputLib);
    //! @endcond

public:
    void Init (bool bLoad, bool bTones);
    bool Load (void);
    void Unload (void);

    void Write (const CConfigData &cConfig,
            const CDslData &cData);

public:
    static const char *s_astrName[eIndexMax];// library names
private:
    const EIndex m_eIndex;      //!< index
    const char* m_strName;      //!< output library name
    COutput *m_pOutput;         //!< output class
    bool m_bLoad;               //!< load output class
    bool m_bTones;              //!< write tones data
};

/*!
 * @class       COutputFactory
 * @brief       output factory class
 * @details     singleton to create output class
 */
class LIBDSLTOOL_EXPORT COutputFactory
{
public:

private:
    COutputFactory (void);

public:
    virtual ~COutputFactory (void);

private:
    //! @cond
    COutputFactory (const COutputFactory &cOutputFactory);
    COutputFactory &operator = (const COutputFactory &cOutputFactory);
    //! @endcond

public:
    static COutputFactory g_cOutputFactory;

    bool Load (int nIndex, const char *strOutput);
    void Unload (int nIndex);

    bool TestVersion (int nIndex, const char *strOutput);
    COutput *Create (int nIndex);

private:
/*!
 * @typedef     TpfnVersion
 * @brief       get version function pointer
 * @param[out]  nMajor
 *              major version number
 * @param[out]  nMinor
 *              minor version number
 * @param[out]  nSub
 *              sub version number
 */
    typedef void (*TpfnVersion)(int &nMajor, int &nMinor, int &nSub);
/*!
 * @typedef     TpfnCreate
 * @brief       create output function pointer
 * @return      output pointer
 */
    typedef COutput *(*TpfnCreate)(void);

    CDynLib         m_acOutput[COutputLib::eIndexMax];      //!< output library array
    TpfnVersion     m_apfnVersion[COutputLib::eIndexMax];   //!< get version function pointer array
    TpfnCreate      m_apfnCreate[COutputLib::eIndexMax];    //!< create output function pointer array
}; // class COutputFactory

extern "C"
{
#ifndef LIBDSLTOOL_OUTPUT_EXPORT
#define LIBDSLTOOL_OUTPUT_EXPORT
#endif // LIBDSLTOOL_OUTPUT_EXPORT
/*!
 * @fn          void OutputVersion (int &nMajor, int &nMinor, int &nSub)
 * @brief       get version
 * @param[out]  nMajor
 *              major version number
 * @param[out]  nMinor
 *              minor version number
 * @param[out]  nSub
 *              sub version number
 */
    void LIBDSLTOOL_OUTPUT_EXPORT OutputVersion (int &nMajor, int &nMinor, int &nSub);
/*!
 * @fn          COutput *OutputCreate (void)
 * @brief       create output object
 * @return      output pointer
 */
    COutput LIBDSLTOOL_OUTPUT_EXPORT *OutputCreate (void);
} // extern "C"

#endif // __DSLTOOL_OUTPUT_H__
//_oOo_
