/*!
 * @file        log.h
 * @brief       log class definitions
 * @details     log helper class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_LOG_H__
#define __DSLTOOL_LOG_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "basis.h"
#include "file.h"

#define LogError CLog::g_cLog.Error     //!< log error message
#define LogWarning CLog::g_cLog.Warning //!< log warning message
#define LogInfo CLog::g_cLog.Info       //!< log info message
#define LogDebug CLog::g_cLog.Debug     //!< log debug message
#if defined (DEBUG)
#define LogTrace CLog::g_cLog.Trace     //!< log trace messages
#define LogExtra CLog::g_cLog.Extra     //!< log extra message
#ifdef _WIN32
#define LogExtraEnter CLog::g_cLog.Extra("%s:%d enter %s\n", __FILE__, __LINE__, __FUNCSIG__);\
                      CLog::g_cLog.Extra
                                        //!< log extra message function entry
#define LogExtraLeave CLog::g_cLog.Extra("%s:%d leave %s\n", __FILE__, __LINE__, __FUNCSIG__);\
                      CLog::g_cLog.Extra
                                        //!< log extra message function entry
#else // _WIN32
#define LogExtraEnter CLog::g_cLog.Extra("%s:%d enter %s\n", __FILE__, __LINE__, __PRETTY_FUNCTION__);\
                      CLog::g_cLog.Extra
                                        //!< log extra message function entry
#define LogExtraLeave CLog::g_cLog.Extra("%s:%d leave %s\n", __FILE__, __LINE__, __PRETTY_FUNCTION__);\
                      CLog::g_cLog.Extra
                                        //!< log extra message function entry
#endif // _WIN32
#else // defined (DEBUG)
#define LogTrace(...) (void)(0)         //!< suppress trace message
#define LogExtra(...) (void)(0)         //!< suppress extra message
#define LogExtraEnter(...) (void)(0)    //!< suppress extra message
#define LogExtraLeave(...) (void)(0)    //!< suppress extra message
#endif // defined (DEBUG)

/*!
 * @class       CLog
 * @brief       log class
 * @details     singleton class
 */
class LIBDSLTOOL_EXPORT CLog
{
private:
    CLog (void);

private:
    //! @cond
    CLog (const CLog &cLog);
    CLog &operator = (const CLog &cLog);
    //! @endcond

public:
    static CLog g_cLog;

/*!
 * @enum        ELevel
 * @brief       log level
 * @details     increasing number include lower levels
 */
    enum ELevel
    {
        eError = 0,                     //!< log errors only
        eWarning,                       //!< log also warning messages
        eInfo,                          //!< log also info messages
        eDebug,                         //!< log also debug messages
        eTrace,                         //!< log trace messages
        eExtra                          //!< log all messages
    };

    static const char *GetLevel (ELevel eLevel);
    static ELevel GetLevel (const CString &strLevel);
    static bool LevelValid (const CString &strLevel);

    bool Open (CString &strFile);
    void Close (void);
/*!
 * @fn          void Level (ELevel eLevel)
 * @brief       set log level
 * @param[in]   eLevel
 *              log level
 */
    void Level (ELevel eLevel) {m_eLevel = eLevel;}

private:
    void Message (ELevel eLevel, const char *strFormat, va_list vaArgs);

public:
    void Error (const char *strFormat, ...);
    void Warning (const char *strFormat, ...);
    void Info (const char *strFormat, ...);
    void Debug (const char *strFormat, ...);
    void Trace (const char *strFormat, ...);
    void Extra (const char *strFormat, ...);

private:
    ELevel      m_eLevel;               //!< log level
    bool        m_bSyslog;              //!< flag to log to syslog
    CFile       m_cFile;                //!< log file
}; // class CLog

#endif // __DSLTOOL_LOG_H__
//_oOo_
