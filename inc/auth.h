/*!
 * @file        auth.h
 * @brief       authentication class definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        13.04.2019
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_AUTH_H__
#define __DSLTOOL_AUTH_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "basis.h"

class CAuthLogin;
class CAuthKey;

/*!
 * @class       CAuth
 * @brief       authentication base class
 * @details     abstract base class for authentication
 */
class LIBDSLTOOL_EXPORT CAuth
{
protected:
    CAuth (void);

private:
    //! @cond
    CAuth (CAuth const &cAuth);
    CAuth &operator = (CAuth const &cAuth);
    //! @endcond

public:
/*!
 * @fn          virtual ~CAuth (void)
 * @brief       destructor
 */
    virtual ~CAuth (void) {}
}; // class CAuth

/*!
 * @class       CAuthLogin
 * @brief       authentication base class
 * @details     abstract base class for authentication
 */
class LIBDSLTOOL_EXPORT CAuthLogin : public CAuth
{
public:
    CAuthLogin (const char *strUser, const char *strPass);

private:
    //! @cond
    CAuthLogin (CAuthLogin const &cAuthLogin);
    CAuthLogin &operator = (CAuthLogin const &cAuthLogin);
    //! @endcond

public:
/*!
 * @fn          virtual ~CAuthLogin (void)
 * @brief       destructor
 */
    virtual ~CAuthLogin (void) {}

    const char *User (void) const;
    const char *Pass (void) const;

private:
    CString             m_strUser;      //!< user name
    CString             m_strPass;      //!< password
}; // class CAuthLogin

#endif // __DSLTOOL_AUTH_H__
//_oOo_
