/*!
 * @file        config-list.h
 * @brief       application class definitions
 * @details     config list class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        29.04.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_CONFIG_LIST_H__
#define __DSLTOOL_CONFIG_LIST_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "config-data.h"
#include "config-option.h"

/*!
 * @class       CConfigList
 * @brief       configuration data list
 */
class LIBDSLTOOL_EXPORT CConfigList
{
public:
    CConfigList (void);
    ~CConfigList (void);

private:
    //! @cond
    CConfigList (CConfigList const &cConfigList);
    CConfigList &operator = (CConfigList const &cConfigList);
    //! @endcond

public:
/*!
 * @enum        EIndex
 * @brief       array index values
 */
    enum EIndex {
        eHelp,
        eVersion,
        eConfig,
        eVerbose,
        eModemType,
        eProtocolType,
        eExtra,
        eModemHost,
        ePort,
        eEthType,
        eEthType4,
        eEthType6,
        eUser,
        ePass,
        ePath,
        ePrefix,
        eLogLevel,
        eLogFile,
#ifdef ENABLE_CAPTURE
        eCapture,
#endif // ENABLE_CAPTURE
        eCollectHost,
        eCollectSock,
        eInterval,
        ePidFile,
        eBackground,
        eOutput,
        eCommand,
        eIndexMax // no entry behind this
    };
    static const size_t s_nBufSize = 1024;  //!< file read buffer size

public:
    void Reset (void);
    bool Disable (int nIndex);

private:
    unsigned long GetFlags (int nIndex);
    int GetShort (int nIndex);
    const char *GetLong (int nIndex);
    CConfigOption::EArg GetOptArg (int nIndex);
    bool SetOption (int nIndex, const char *strValue);

public:
    bool WriteData (CConfigData &cConfig);
    bool ReadData (const CConfigData &cConfig);

    bool ReadArgs (int nArgc, char **astrArgv);
    bool TestRequired (bool bSilent);

private :
    bool ParseConfig (char *strBuffer, size_t nSize);
    bool ParseConfig (const char *strLine);

public:
    bool ReadConfig (const char *strFile);
    bool WriteConfig (const char *strFile);

public:
    void PrintHelp (void);

private:
    class CConfigOption *m_apOptions[eIndexMax]; //!< option array

}; // class CConfigList

#endif // __DSLTOOL_CONFIG_LIST_H__
//_oOo_
