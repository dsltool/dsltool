/*!
 * @file        bandplan.h
 * @brief       DSL bandplan definitions
 * @details     frequency / tone ranges
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_BANDPLAN_H__
#define __DSLTOOL_BANDPLAN_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "basis.h"


/*!
 * @class       CBand
 * @brief       band data
 * @details     minimum and maximum tone number for downstream and upstream band
 */
class LIBDSLTOOL_EXPORT CBand
{
    friend class CBandplan;
private:
    static const CBand g_acBandNone[];
//! @cond
    static const CBand g_acBandADSL_A[];
    static const CBand g_acBandADSL_B[];
    static const CBand g_acBandADSL2_A[];
    static const CBand g_acBandADSL2_B[];
    static const CBand g_acBandADSL2_J[];
    static const CBand g_acBandADSL2_M[];
    static const CBand g_acBandADSL2p_A[];
    static const CBand g_acBandADSL2p_B[];
    static const CBand g_acBandADSL2p_J[];
    static const CBand g_acBandADSL2p_M[];
    static const CBand g_acBandVDSL2_B7_1[];
    static const CBand g_acBandVDSL2_B7_2_8[];
    static const CBand g_acBandVDSL2_B7_2_12[];
    static const CBand g_acBandVDSL2_B7_3_8[];
    static const CBand g_acBandVDSL2_B7_3_12[];
    static const CBand g_acBandVDSL2_B7_4_8[];
    static const CBand g_acBandVDSL2_B7_4_12[];
    static const CBand g_acBandVDSL2_B7_5_8[];
    static const CBand g_acBandVDSL2_B7_5_12[];
    static const CBand g_acBandVDSL2_B7_6_8[];
    static const CBand g_acBandVDSL2_B7_6_12[];
    static const CBand g_acBandVDSL2_B7_7[];
    static const CBand g_acBandVDSL2_B7_8[];
    static const CBand g_acBandVDSL2_B7_9[];
    static const CBand g_acBandVDSL2_B7_10[];
    static const CBand g_acBandVDSL2_B8_1_8[];
    static const CBand g_acBandVDSL2_B8_1_12[];
    static const CBand g_acBandVDSL2_B8_2_8[];
    static const CBand g_acBandVDSL2_B8_2_12[];
    static const CBand g_acBandVDSL2_B8_3_8[];
    static const CBand g_acBandVDSL2_B8_3_12[];
    static const CBand g_acBandVDSL2_B8_4_8[];
    static const CBand g_acBandVDSL2_B8_4_12[];
    static const CBand g_acBandVDSL2_B8_5_8[];
    static const CBand g_acBandVDSL2_B8_5_12[];
    static const CBand g_acBandVDSL2_B8_6_8[];
    static const CBand g_acBandVDSL2_B8_6_12[];
    static const CBand g_acBandVDSL2_B8_7_8[];
    static const CBand g_acBandVDSL2_B8_7_12[];
    static const CBand g_acBandVDSL2_B8_8[];
    static const CBand g_acBandVDSL2_B8_9[];
    static const CBand g_acBandVDSL2_B8_10[];
    static const CBand g_acBandVDSL2_B8_11[];
    static const CBand g_acBandVDSL2_B8_12[];
    static const CBand g_acBandVDSL2_B8_13[];
    static const CBand g_acBandVDSL2_B8_14[];
    static const CBand g_acBandVDSL2_B8_15[];
    static const CBand g_acBandVDSL2_B8_16[];
    static const CBand g_acBandVDSL2_B8_17[];
    static const CBand g_acBandVDSL2_B8_18[];
    static const CBand g_acBandVDSL2_B8_19[];
    static const CBand g_acBandVDSL2_B8_20[];
    static const CBand g_acBandVDSL2_B8_21[];
    static const CBand g_acBandVDSL2_B8_22[];
//! @endcond

    CBand (int nMin, int nMid, int nMax);

public:
    static const int s_nMaxBand = 5;    //!< maximum number of bands
/*!
* @fn          int MinUp (void) const
* @brief       get upstream limit
* @return      min tone number
*/
    int MinUp (void) const {return m_nMinUp;}
/*!
* @fn          int MaxUp (void) const
* @brief       get upstream limit
* @return      max tone number
*/
    int MaxUp (void) const {return m_nMaxUp;}
/*!
* @fn          int MinDown (void) const
* @brief       get downstream limit
* @return      min tone number
*/
    int MinDown (void) const {return m_nMinDown;}
/*!
* @fn          int MaxDown (void) const
* @brief       get downstream limit
* @return      max tone number
*/
    int MaxDown (void) const {return m_nMaxDown;}
private:
    int         m_nMinUp;               //!< min tone number for upstream
    int         m_nMaxUp;               //!< max tone number for upstream
    int         m_nMinDown;             //!< min tone number for downstream
    int         m_nMaxDown;             //!< max tone number for downstream
}; // class CBand

/*!
* @class       CBandplan
* @brief       bandplan data
* @details     bandplan information and band limits
*/
class LIBDSLTOOL_EXPORT CBandplan
{
public:
/*!
* @enum        EBandplan
* @brief       bandplan index
*/
    enum EBandplan
    {
        eNone,                  //!< empty bandplan
        eADSL_A,                //!< ADSL annex A
        eADSL_B,                //!< ADSL annex B
        eADSL2_A,               //!< ADSL2 annex A
        eADSL2_B,               //!< ADSL2 annex B
        eADSL2_J,               //!< ADSL2 annex J
        eADSL2_M,               //!< ADSL2 annex M
        eADSL2p_A,              //!< ADSL2+ annex A
        eADSL2p_B,              //!< ADSL2+ annex B
        eADSL2p_J,              //!< ADSL2+ annex J
        eADSL2p_M,              //!< ADSL2+ annex M
        eVDSL2_B7_1_8a,         //!< VDSL2 annex B7-1 profile 8a
        eVDSL2_B7_1_8b,         //!< VDSL2 annex B7-1 profile 8b
        eVDSL2_B7_1_8c,         //!< VDSL2 annex B7-1 profile 8c
        eVDSL2_B7_1_8d,         //!< VDSL2 annex B7-1 profile 8d
        eVDSL2_B7_2_8a,         //!< VDSL2 annex B7-2 profile 8a
        eVDSL2_B7_2_8b,         //!< VDSL2 annex B7-2 profile 8b
        eVDSL2_B7_2_8c,         //!< VDSL2 annex B7-2 profile 8c
        eVDSL2_B7_2_8d,         //!< VDSL2 annex B7-2 profile 8d
        eVDSL2_B7_2_12a,        //!< VDSL2 annex B7-2 profile 12a
        eVDSL2_B7_2_12b,        //!< VDSL2 annex B7-2 profile 12b
        eVDSL2_B7_3_8a,         //!< VDSL2 annex B7-3 profile 8a
        eVDSL2_B7_3_8b,         //!< VDSL2 annex B7-3 profile 8b
        eVDSL2_B7_3_8c,         //!< VDSL2 annex B7-3 profile 8c
        eVDSL2_B7_3_8d,         //!< VDSL2 annex B7-3 profile 8d
        eVDSL2_B7_3_12a,        //!< VDSL2 annex B7-3 profile 12a
        eVDSL2_B7_3_12b,        //!< VDSL2 annex B7-3 profile 12b
        eVDSL2_B7_4_8a,         //!< VDSL2 annex B7-4 profile 8a
        eVDSL2_B7_4_8b,         //!< VDSL2 annex B7-4 profile 8b
        eVDSL2_B7_4_8c,         //!< VDSL2 annex B7-4 profile 8c
        eVDSL2_B7_4_8d,         //!< VDSL2 annex B7-4 profile 8d
        eVDSL2_B7_4_12a,        //!< VDSL2 annex B7-4 profile 12a
        eVDSL2_B7_4_12b,        //!< VDSL2 annex B7-4 profile 12b
        eVDSL2_B7_5_8a,         //!< VDSL2 annex B7-5 profile 8a
        eVDSL2_B7_5_8b,         //!< VDSL2 annex B7-5 profile 8b
        eVDSL2_B7_5_8c,         //!< VDSL2 annex B7-5 profile 8c
        eVDSL2_B7_5_8d,         //!< VDSL2 annex B7-5 profile 8d
        eVDSL2_B7_5_12a,        //!< VDSL2 annex B7-5 profile 12a
        eVDSL2_B7_5_12b,        //!< VDSL2 annex B7-5 profile 12b
        eVDSL2_B7_6_8a,         //!< VDSL2 annex B7-6 profile 8a
        eVDSL2_B7_6_8b,         //!< VDSL2 annex B7-6 profile 8b
        eVDSL2_B7_6_8c,         //!< VDSL2 annex B7-6 profile 8c
        eVDSL2_B7_6_8d,         //!< VDSL2 annex B7-6 profile 8d
        eVDSL2_B7_6_12a,        //!< VDSL2 annex B7-6 profile 12a
        eVDSL2_B7_6_12b,        //!< VDSL2 annex B7-6 profile 12b
        eVDSL2_B7_7_17a,        //!< VDSL2 annex B7-7 profile 17a
        eVDSL2_B7_8_30a,        //!< VDSL2 annex B7-8 profile 30a
        eVDSL2_B7_9_17a,        //!< VDSL2 annex B7-9 profile 17a
        eVDSL2_B7_10_30a,       //!< VDSL2 annex B7-10 profile 30a
        eVDSL2_B8_1_8a,         //!< VDSL2 annex B8-1 profile 8a
        eVDSL2_B8_1_8b,         //!< VDSL2 annex B8-1 profile 8b
        eVDSL2_B8_1_8c,         //!< VDSL2 annex B8-1 profile 8c
        eVDSL2_B8_1_8d,         //!< VDSL2 annex B8-1 profile 8d
        eVDSL2_B8_1_12a,        //!< VDSL2 annex B8-1 profile 12a
        eVDSL2_B8_1_12b,        //!< VDSL2 annex B8-1 profile 12b
        eVDSL2_B8_2_8a,         //!< VDSL2 annex B8-2 profile 8a
        eVDSL2_B8_2_8b,         //!< VDSL2 annex B8-2 profile 8b
        eVDSL2_B8_2_8c,         //!< VDSL2 annex B8-2 profile 8c
        eVDSL2_B8_2_8d,         //!< VDSL2 annex B8-1 profile 8d
        eVDSL2_B8_2_12a,        //!< VDSL2 annex B8-2 profile 12a
        eVDSL2_B8_2_12b,        //!< VDSL2 annex B8-2 profile 12b
        eVDSL2_B8_3_8a,         //!< VDSL2 annex B8-3 profile 8a
        eVDSL2_B8_3_8b,         //!< VDSL2 annex B8-3 profile 8b
        eVDSL2_B8_3_8c,         //!< VDSL2 annex B8-3 profile 8c
        eVDSL2_B8_3_8d,         //!< VDSL2 annex B8-3 profile 8d
        eVDSL2_B8_3_12a,        //!< VDSL2 annex B8-3 profile 12a
        eVDSL2_B8_3_12b,        //!< VDSL2 annex B8-3 profile 12b
        eVDSL2_B8_4_8a,         //!< VDSL2 annex B8-4 profile 8a
        eVDSL2_B8_4_8b,         //!< VDSL2 annex B8-4 profile 8b
        eVDSL2_B8_4_8c,         //!< VDSL2 annex B8-4 profile 8c
        eVDSL2_B8_4_8d,         //!< VDSL2 annex B8-4 profile 8d
        eVDSL2_B8_4_12a,        //!< VDSL2 annex B8-4 profile 12a
        eVDSL2_B8_4_12b,        //!< VDSL2 annex B8-4 profile 12b
        eVDSL2_B8_5_8a,         //!< VDSL2 annex B8-5 profile 8a
        eVDSL2_B8_5_8b,         //!< VDSL2 annex B8-5 profile 8b
        eVDSL2_B8_5_8c,         //!< VDSL2 annex B8-5 profile 8c
        eVDSL2_B8_5_8d,         //!< VDSL2 annex B8-5 profile 8d
        eVDSL2_B8_5_12a,        //!< VDSL2 annex B8-5 profile 12a
        eVDSL2_B8_5_12b,        //!< VDSL2 annex B8-5 profile 12b
        eVDSL2_B8_6_8a,         //!< VDSL2 annex B8-6 profile 8a
        eVDSL2_B8_6_8b,         //!< VDSL2 annex B8-6 profile 8b
        eVDSL2_B8_6_8c,         //!< VDSL2 annex B8-6 profile 8c
        eVDSL2_B8_6_8d,         //!< VDSL2 annex B8-6 profile 8d
        eVDSL2_B8_6_12a,        //!< VDSL2 annex B8-6 profile 12a
        eVDSL2_B8_6_12b,        //!< VDSL2 annex B8-6 profile 12b
        eVDSL2_B8_7_8a,         //!< VDSL2 annex B8-7 profile 8a
        eVDSL2_B8_7_8b,         //!< VDSL2 annex B8-7 profile 8b
        eVDSL2_B8_7_8c,         //!< VDSL2 annex B8-7 profile 8c
        eVDSL2_B8_7_8d,         //!< VDSL2 annex B8-7 profile 8d
        eVDSL2_B8_7_12a,        //!< VDSL2 annex B8-7 profile 12a
        eVDSL2_B8_7_12b,        //!< VDSL2 annex B8-7 profile 12b
        eVDSL2_B8_8_17a,        //!< VDSL2 annex B8-8 profile 17a
        eVDSL2_B8_9_17a,        //!< VDSL2 annex B8-9 profile 17a
        eVDSL2_B8_10_17a,       //!< VDSL2 annex B8-10 profile 17a
        eVDSL2_B8_11_17a,       //!< VDSL2 annex B8-11 profile 17a
        eVDSL2_B8_12_17a,       //!< VDSL2 annex B8-12 profile 17a
        eVDSL2_B8_13_30a,       //!< VDSL2 annex B8-13 profile 30a
        eVDSL2_B8_14_30a,       //!< VDSL2 annex B8-14 profile 30a
        eVDSL2_B8_15_30a,       //!< VDSL2 annex B8-15 profile 30a
        eVDSL2_B8_16_30a,       //!< VDSL2 annex B8-16 profile 30a
        eVDSL2_B8_17_17a,       //!< VDSL2 annex B8-17 profile 17a
        eVDSL2_B8_18_17a,       //!< VDSL2 annex B8-18 profile 17a
        eVDSL2_B8_19_35b,       //!< VDSL2 annex B8-19 profile 35b
        eVDSL2_B8_20_35b,       //!< VDSL2 annex B8-20 profile 35b
        eVDSL2_B8_21_35b,       //!< VDSL2 annex B8-21 profile 35b
        eVDSL2_B8_22_35b        //!< VDSL2 annex B8-22 profile 35b
    };

private:
    CBandplan (const char *strType,
            const char *strITU_TVersion,
            const char *strITU_TAnnex,
            const char *strITU_TProfile,
            const char *strITU_TBandplan,
            size_t nTones,
            double fSpacing,
            const CBand *pBand,
            size_t nBands);

private:
    //! @cond
    CBandplan (const CBandplan &cBandplan);
    //! @endcond
    CBandplan &operator = (const CBandplan &cBandplan);


private:
/*!
 * @class       CBandplanData
 * @brief       bandplan data entry
 * @details     for bandplan data array
 */
    class CBandplanData
    {
    public:
        CBandplanData (CBandplan::EBandplan eBandplan, CBandplan &cBandplan);
        static CBandplan *Get (EBandplan eBandplan);

    private:
        //! @cond
        CBandplanData (const CBandplanData &cBandplanData);
        CBandplanData &operator = (const CBandplanData &cBandplanData);
        //! @endcond

    private:
        CBandplan::EBandplan        m_eBandplan;    //!< bandplan index
        CBandplan                  &m_cBandplan;    //!< bandplan reference

    private:
        static const CBandplanData  g_acList[];     //!< bandplan data array
    };

private:
    static CBandplan g_cBandplanNone;
//! @cond
    static CBandplan g_cBandplanADSL_A;
    static CBandplan g_cBandplanADSL_B;
    static CBandplan g_cBandplanADSL2_A;
    static CBandplan g_cBandplanADSL2_B;
    static CBandplan g_cBandplanADSL2_J;
    static CBandplan g_cBandplanADSL2_M;
    static CBandplan g_cBandplanADSL2p_A;
    static CBandplan g_cBandplanADSL2p_B;
    static CBandplan g_cBandplanADSL2p_J;
    static CBandplan g_cBandplanADSL2p_M;
    static CBandplan g_cBandplanVDSL2_B7_1_8a;
    static CBandplan g_cBandplanVDSL2_B7_1_8b;
    static CBandplan g_cBandplanVDSL2_B7_1_8c;
    static CBandplan g_cBandplanVDSL2_B7_1_8d;
    static CBandplan g_cBandplanVDSL2_B7_2_8a;
    static CBandplan g_cBandplanVDSL2_B7_2_8b;
    static CBandplan g_cBandplanVDSL2_B7_2_8c;
    static CBandplan g_cBandplanVDSL2_B7_2_8d;
    static CBandplan g_cBandplanVDSL2_B7_2_12a;
    static CBandplan g_cBandplanVDSL2_B7_2_12b;
    static CBandplan g_cBandplanVDSL2_B7_3_8a;
    static CBandplan g_cBandplanVDSL2_B7_3_8b;
    static CBandplan g_cBandplanVDSL2_B7_3_8c;
    static CBandplan g_cBandplanVDSL2_B7_3_8d;
    static CBandplan g_cBandplanVDSL2_B7_3_12a;
    static CBandplan g_cBandplanVDSL2_B7_3_12b;
    static CBandplan g_cBandplanVDSL2_B7_4_8a;
    static CBandplan g_cBandplanVDSL2_B7_4_8b;
    static CBandplan g_cBandplanVDSL2_B7_4_8c;
    static CBandplan g_cBandplanVDSL2_B7_4_8d;
    static CBandplan g_cBandplanVDSL2_B7_4_12a;
    static CBandplan g_cBandplanVDSL2_B7_4_12b;
    static CBandplan g_cBandplanVDSL2_B7_5_8a;
    static CBandplan g_cBandplanVDSL2_B7_5_8b;
    static CBandplan g_cBandplanVDSL2_B7_5_8c;
    static CBandplan g_cBandplanVDSL2_B7_5_8d;
    static CBandplan g_cBandplanVDSL2_B7_5_12a;
    static CBandplan g_cBandplanVDSL2_B7_5_12b;
    static CBandplan g_cBandplanVDSL2_B7_6_8a;
    static CBandplan g_cBandplanVDSL2_B7_6_8b;
    static CBandplan g_cBandplanVDSL2_B7_6_8c;
    static CBandplan g_cBandplanVDSL2_B7_6_8d;
    static CBandplan g_cBandplanVDSL2_B7_6_12a;
    static CBandplan g_cBandplanVDSL2_B7_6_12b;
    static CBandplan g_cBandplanVDSL2_B7_7_17a;
    static CBandplan g_cBandplanVDSL2_B7_8_30a;
    static CBandplan g_cBandplanVDSL2_B7_9_17a;
    static CBandplan g_cBandplanVDSL2_B7_10_30a;
    static CBandplan g_cBandplanVDSL2_B8_1_8a;
    static CBandplan g_cBandplanVDSL2_B8_1_8b;
    static CBandplan g_cBandplanVDSL2_B8_1_8c;
    static CBandplan g_cBandplanVDSL2_B8_1_8d;
    static CBandplan g_cBandplanVDSL2_B8_1_12a;
    static CBandplan g_cBandplanVDSL2_B8_1_12b;
    static CBandplan g_cBandplanVDSL2_B8_2_8a;
    static CBandplan g_cBandplanVDSL2_B8_2_8b;
    static CBandplan g_cBandplanVDSL2_B8_2_8c;
    static CBandplan g_cBandplanVDSL2_B8_2_8d;
    static CBandplan g_cBandplanVDSL2_B8_2_12a;
    static CBandplan g_cBandplanVDSL2_B8_2_12b;
    static CBandplan g_cBandplanVDSL2_B8_3_8a;
    static CBandplan g_cBandplanVDSL2_B8_3_8b;
    static CBandplan g_cBandplanVDSL2_B8_3_8c;
    static CBandplan g_cBandplanVDSL2_B8_3_8d;
    static CBandplan g_cBandplanVDSL2_B8_3_12a;
    static CBandplan g_cBandplanVDSL2_B8_3_12b;
    static CBandplan g_cBandplanVDSL2_B8_4_8a;
    static CBandplan g_cBandplanVDSL2_B8_4_8b;
    static CBandplan g_cBandplanVDSL2_B8_4_8c;
    static CBandplan g_cBandplanVDSL2_B8_4_8d;
    static CBandplan g_cBandplanVDSL2_B8_4_12a;
    static CBandplan g_cBandplanVDSL2_B8_4_12b;
    static CBandplan g_cBandplanVDSL2_B8_5_8a;
    static CBandplan g_cBandplanVDSL2_B8_5_8b;
    static CBandplan g_cBandplanVDSL2_B8_5_8c;
    static CBandplan g_cBandplanVDSL2_B8_5_8d;
    static CBandplan g_cBandplanVDSL2_B8_5_12a;
    static CBandplan g_cBandplanVDSL2_B8_5_12b;
    static CBandplan g_cBandplanVDSL2_B8_6_8a;
    static CBandplan g_cBandplanVDSL2_B8_6_8b;
    static CBandplan g_cBandplanVDSL2_B8_6_8c;
    static CBandplan g_cBandplanVDSL2_B8_6_8d;
    static CBandplan g_cBandplanVDSL2_B8_6_12a;
    static CBandplan g_cBandplanVDSL2_B8_6_12b;
    static CBandplan g_cBandplanVDSL2_B8_7_8a;
    static CBandplan g_cBandplanVDSL2_B8_7_8b;
    static CBandplan g_cBandplanVDSL2_B8_7_8c;
    static CBandplan g_cBandplanVDSL2_B8_7_8d;
    static CBandplan g_cBandplanVDSL2_B8_7_12a;
    static CBandplan g_cBandplanVDSL2_B8_7_12b;
    static CBandplan g_cBandplanVDSL2_B8_8_17a;
    static CBandplan g_cBandplanVDSL2_B8_9_17a;
    static CBandplan g_cBandplanVDSL2_B8_10_17a;
    static CBandplan g_cBandplanVDSL2_B8_11_17a;
    static CBandplan g_cBandplanVDSL2_B8_12_17a;
    static CBandplan g_cBandplanVDSL2_B8_13_30a;
    static CBandplan g_cBandplanVDSL2_B8_14_30a;
    static CBandplan g_cBandplanVDSL2_B8_15_30a;
    static CBandplan g_cBandplanVDSL2_B8_16_30a;
    static CBandplan g_cBandplanVDSL2_B8_17_17a;
    static CBandplan g_cBandplanVDSL2_B8_18_17a;
    static CBandplan g_cBandplanVDSL2_B8_19_35b;
    static CBandplan g_cBandplanVDSL2_B8_20_35b;
    static CBandplan g_cBandplanVDSL2_B8_21_35b;
    static CBandplan g_cBandplanVDSL2_B8_22_35b;
//! @endcond

public:
/*!
* @fn          virtual ~CBandplan (void)
* @brief       destructor
*/
    virtual ~CBandplan (void) {}
    static CBandplan *Get (EBandplan eBandplan);

public:
/*!
* @fn          const char *Type (void)
* @brief       get band type
* @return      band type string
*/
    const char *Type (void) const {return m_strType;}
/*!
* @fn          size_t Tones (void)
* @brief       get number of tones
* @return      number of tones
*/
    size_t Tones (void) const {return m_nTones;}
/*!
* @fn          double Spacing (void)
* @brief       get frequency spacing
* @return      frequency spacing
*/
    double Spacing (void) const {return m_fSpacing;}
/*!
* @fn          size_t Bands (void)
* @brief       get number of bands
* @return      number of bands
*/
    size_t Bands (void) const {return m_nBands;}
/*!
* @fn          const CBand &Band (size_t nBand)
* @brief       get band limits
* @param[in]   nBand
*              band number
* @return      band data
*/
    const CBand &Band (size_t nBand) const {return m_pBand[nBand];}
    void OperationMode (CString &strOperationMode);
    void Profile (CString &strProfile);

protected:
    const char         *m_strType;              //!< dsl type
    const char         *m_strITU_TVersion;      //!< ITU-T version
    const char         *m_strITU_TAnnex;        //!< ITU-T annex
    const char         *m_strITU_TProfile;      //!< ITU-T profile
    const char         *m_strITU_TBandplan;     //!< ITU-T frequency allocation
    size_t              m_nTones;               //!< number of tones
    double              m_fSpacing;             //!< spacing between tones [kHz]
    const CBand        *m_pBand;                //!< band margins
    size_t              m_nBands;               //!< number of bands
}; // class CBandplan

#endif // __DSLTOOL_BANDPLAN_H__
//_oOo_
