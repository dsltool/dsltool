/*!
 * @file        gui-config.h
 * @brief       config dialog class definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_GUI_CONFIG_H__
#define __DSLTOOL_GUI_CONFIG_H__

#include <QDialog>

#include "ui_config.h"
#include "config-list.h"
#include "gui-main.h"

/*!
 * @class       CGuiConfig
 * @brief       configuration class
 */
class CGuiConfig: public QDialog
{
    Q_OBJECT

public:
    explicit CGuiConfig (CGuiMain *pParent);

private:
    //! @cond
    CGuiConfig (const CGuiConfig &cGuiConfig);
    CGuiConfig &operator = (const CGuiConfig &cGuiConfig);
    //! @endcond

public:
    void ResetFocus (void);
    void SetConfig (const CConfigData &cConfig);
    bool GetConfig (CConfigData &cConfig, CConfigList &cConfigList);

private:
    bool Get (bool &bDirty, CString &strConfig, const QLineEdit *pEdit,
            const QCheckBox *pCheck = NULL, const char *strDefault = NULL);
    bool Get (bool &bDirty, CString &strConfig, const QComboBox *pCombo,
            const QCheckBox *pCheck = NULL, const char *strDefault = NULL);
    bool Get (bool &bDirty, int  &nConfig, const QComboBox *pCombo,
            const QCheckBox *pCheck = NULL, int nDefault = -1);
    bool Get (bool &bDirty, int  &nConfig, const QSpinBox *pSpin,
            const QCheckBox *pCheck = NULL, int nDefault = -1);

private:
/*!
 * @enum        ELogType
 * @brief       logging type
 */
    enum ELogType {
        eLogNone,       //!< no logging
        eLogStderr,     //!< log to stderr
        eLogSyslog,     //!< log to syslog
        eLogDefault,    //!< log to default file
        eLogFile        //!< log to file
    };

    ELogType LogType (bool bLog, const CString &strLogFile);

public slots:
    // check boxes
    void OnCheckExtra (int nState);
    void OnCheckModemPort (int nState);
    void OnCheckEthType (int nState);
    void OnCheckFilePath (int nState);
    void OnCheckFilePrefix (int nState);
    void OnCheckCollectHost (int nState);
    void OnCheckCollectSock (int nState);
    void OnCheckDaemonInt (int nState);
    void OnCheckDaemonPid (int nState);

    // combo boxes
    void OnComboLog (int nIndex);

    // tool buttons
    void OnToolExtra (void);
    void OnToolFilePath (void);
    void OnToolLogFile (void);
    void OnToolCollectSock (void);
    void OnToolDaemonPid (void);

private:
    Ui::ConfigWindow m_uiConfig;    //!< configuration dialog UI
};


#endif // __DSLTOOL_GUI_CONFIG_H__
//_oOo_
