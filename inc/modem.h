/*!
 * @file        modem.h
 * @brief       modem base class definitions
 * @details     abstract modem and helper class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_MODEM_H__
#define __DSLTOOL_MODEM_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "dynlib.h"
#include "dsldata.h"
#include "auth.h"

class CProtocol;
/*!
 * @class       CModem
 * @brief       modem base class
 * @details     abstract base class for modem interface
 */
class LIBDSLTOOL_EXPORT CModem
{
protected:
    CModem (CDslData &cData,
            CProtocol *pProtocol);

private:
    //! @cond
    CModem (CModem const &cModem);
    CModem &operator = (CModem const &cModem);
    //! @endcond

public:
/*!
 * @fn          virtual ~CModem (void) {}
 * @brief       destructor
 */
    virtual ~CModem (void) {}

/*!
 * @enum        ECommand
 * @brief       modem commands
 */
    enum ECommand
    {
        eInfo = 1,                      //!< read all
        eStatus,                        //!< read status only
        eResync,                        //!< resync modem
        eReboot                         //!< reboot modem
    };

    static const char *GetCommand (ECommand eCommand);
    static ECommand GetCommand (const CString &strCommand);
    static bool CommandValid (const CString &strCommand);

    virtual void Init (ECommand eCommand);
    virtual bool Connect (void);

    static long BuildVersion (long nMajor, long nMinor = 0,
            long nSub = 0, long nSubSub = 0);

    const char *User (void);
    const char *Pass (void);

public:
/*!
 * @fn          CProtocol *Protocol (void)
 * @brief       get protocol pointer
 * @return      protocol pointer
 */
    CProtocol *Protocol (void) {return m_pProtocol;}
/*!
 * @fn          long Version (void)
 * @brief       get version number
 * @return      version number
 */
    long Version (void) {return m_nVersion;}
/*!
 * @fn          void Version (long nVersion)
 * @brief       set version number
 * @param[in]   nVersion
 *              version number
 */
    void Version (long nVersion) {m_nVersion = nVersion;}
/*!
 * @fn          ECommand Command (void)
 * @brief       get command
 * @return      command
 */
    ECommand Command (void) {return m_eCommand;}

/*!
 * @fn          const char *Prompt (void)
 * @brief       get prompt
 * @return      prompt
 */
    const char *Prompt (void) {return m_strPrompt;}
/*!
 * @fn          const char *LF (void)
 * @brief       get linefeed
 * @return      linefeed
 */
    const char *LF (void) {return m_strLF;}
/*!
 * @fn          NDslData::CDslData &Data (void)
 * @brief       get data strorage
 * @return      data strorage
 */
    CDslData &Data (void) {return m_cData;}

protected:
    CDslData           &m_cData;        //!< dsl data storage
    ECommand            m_eCommand;     //!< actual command
    CProtocol          *m_pProtocol;    //!< protocol pointer
    CAuthLogin         *m_pLogin;       //!< authentication pointer
    CString             m_strPrompt;    //!< prompt
    CString             m_strLF;        //!< linefeed
    long                m_nVersion;     //!< version number
}; // class CModem

/*!
 * @class       CModemFactory
 * @brief       modem factory class
 * @details     singleton to create modem class
 */
class LIBDSLTOOL_EXPORT CModemFactory
{
private:
    CModemFactory (void);

public:
    virtual ~CModemFactory (void);

private:
    //! @cond
    CModemFactory (const CModemFactory &cModemFactory);
    CModemFactory &operator = (const CModemFactory &cModemFactory);
    //! @endcond

public:
    static CModemFactory g_cModemFactory;

    bool Load (const char *strModem);
    void Unload (void);

    bool TestVersion (const char *strModem);
    CModem *Create (CDslData &cData,
            CProtocol *pProtocol);

private:
/*!
 * @typedef     TpfnVersion
 * @brief       get version function pointer
 * @param[out]  nMajor
 *              major version number
 * @param[out]  nMinor
 *              minor version number
 * @param[out]  nSub
 *              sub version number
 */
    typedef void (*TpfnVersion)(int &nMajor, int &nMinor, int &nSub);
/*!
 * @typedef     TpfnCreate
 * @brief       create modem function pointer
 * @param[in]   cData
 *              DSL data reference
 * @param[in]   pProtocol
 *              protocol pointer
 * @return      modem pointer
 */
    typedef CModem *(*TpfnCreate)(CDslData &cData,
            CProtocol *pProtocol);

    CDynLib             m_cModem;       //!< modem library
    TpfnVersion         m_pfnVersion;   //!< get version function pointer
    TpfnCreate          m_pfnCreate;    //!< create modem function pointer
}; // class CModemFactory

extern "C"
{
#ifndef LIBDSLTOOL_MODEM_EXPORT
#define LIBDSLTOOL_MODEM_EXPORT
#endif // LIBDSLTOOL_MODEM_EXPORT
/*!
 * @fn          void ModemVersion (int &nMajor, int &nMinor, int &nSub)
 * @brief       get version
 * @param[out]  nMajor
 *              major version number
 * @param[out]  nMinor
 *              minor version number
 * @param[out]  nSub
 *              sub version number
 */
    void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub);
/*!
 * @fn          CModem* ModemCreate (CDslData &cData,
 *                      CProtocol *pProtocol)
 * @brief       create modem object
 * @param[in]   cData
 *              DSL data reference
 * @param[in]   pProtocol
 *              protocol pointer
 * @return      modem pointer
 */
    CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData,
            CProtocol *pProtocol);
} // extern "C"

#endif // __DSLTOOL_MODEM_H__
//_oOo_
