/*!
 * @file        gui-info.h
 * @brief       info dialog class definitions
 *
 * @author      Carsten Spie&szlig; fli4l@carsten-spiess.de
 * @date        13.03.2017
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_GUI_INFO_H__
#define __DSLTOOL_GUI_INFO_H__

#include <QDialog>

#include "ui_info.h"
#include "gui-main.h"

/*!
 * @class       CGuiInfo
 * @brief       info dialog class
 */
class CGuiInfo: public QDialog
{
    Q_OBJECT

public:
    explicit CGuiInfo (CGuiMain *pParent);

private:
    //! @cond
    CGuiInfo (const CGuiInfo &cGuiInfo);
    CGuiInfo &operator = (const CGuiInfo &cGuiInfo);
    //! @endcond

private:
    Ui::InfoWindow m_uiInfo;            //!< info dialog UI
};


#endif // __DSLTOOL_GUI_INFO_H__
//_oOo_
