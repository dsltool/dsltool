/*!
 * @file        gui-main.h
 * @brief       main window class definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_GUI_MAIN_H__
#define __DSLTOOL_GUI_MAIN_H__

#include <QMainWindow>
#include <QPicture>

#include <QTranslator>

#include "ui_main.h"
#include "output.h"

/*!
 * @class       CGuiMain
 * @brief       main window class
 */
class CGuiMain: public QMainWindow
{
    Q_OBJECT

public:
    explicit CGuiMain (class CDslToolGui &cDslToolGui);

private:
    //! @cond
    CGuiMain (const CGuiMain &cGuiMain);
    CGuiMain &operator = (const CGuiMain &cGuiMain);
    //! @endcond

protected:
    virtual void closeEvent (QCloseEvent *pEvent);

public slots:
    void OnNew (void);
    void OnOpen (void);
    void OnSave (void);
    void OnSaveAs (void);
    void OnQuit (void);
    void OnRefresh (void);
    void OnResync (void);
    void OnReboot (void);
    void OnLanguage (QAction *pAction);
    void OnConfig (void);
    void OnHelp (void);
    void OnInfo (void);

public:
/*!
 * @enum        EStatus
 * @brief       status values
 */
    enum EStatus {
        eNotConfigured, //!< invalid configuration
        eReady,         //!< finished getting modem data
        eRunning,       //!< getting modem data
        eError          //!< error on loading modem or protocol handler
    };
    void ResetFocus (void);
    void SetStatus (EStatus eStatus);
    void EnableMenu (void);

private:
    void CreateLanguageMenu (void);
    void Translate (const QString &strLocale);
    void TranslateStatus (void);

public:
    void Write (const CDslData &cData);

private:
    void Write (CBandplan *pBandplan);
    void Write (const CDataInt &cData,
            QLineEdit *pEdit);
    void Write (const CDataIntUpDown &cData,
            QLineEdit *pEditUp,
            QLineEdit *pEditDown);
    void Write (const CDataDouble &cData,
            QLineEdit *pEdit);
    void Write (const CDataDoubleUpDown &cData,
            QLineEdit *pEditUp,
            QLineEdit *pEditDown);
    void Write (const CArrayDoubleUpDown &cData,
            QLineEdit *pEditUp,
            QLineEdit *pEditDown);
    void Write (const CDataString &cData,
            QLineEdit *pEditFmt);
    void Write (const CDataATU &cData,
            QLineEdit *pEditVendor,
            QLineEdit *pEditSpec,
            QLineEdit *pEditRevision);

    bool WriteBits (const CTonesGroup &cTones);
    bool WriteSNR (const CTonesGroup &cTones);
    bool WriteChar (const CTonesGroup &cTones);

private:
    class CDslToolGui &m_cDslToolGui;   //!< applicatio
    QPicture m_cImgBitalloc;            //!< bitalloc image
    QPicture m_cImgSNR;                 //!< SNR image
    QPicture m_cImgChar;                //!< char image
    QTranslator m_cTranslator;          //!< gui translator
    QTranslator m_cTranslatorQt;        //!< Qt translator
    Ui::MainWindow m_uiMain;            //!< main window UI
    EStatus m_eStatus;                  //!< status
    QLabel m_cStatus;                   //!< status widget
}; // class CGuiMain

/*!
 * @class       CGuiOutput
 * @brief       gui output handler
 * @details     call write function on CGuiMain
 */
class CGuiOutput: public COutput
{
public:
    CGuiOutput (CGuiMain &cGuiMain);

private:
    //! @cond
    CGuiOutput (const CGuiOutput &cGuiOutput);
    CGuiOutput &operator = (const CGuiOutput &cGuiOutput);
    //! @endcond

public:
    virtual void Write (const CConfigData &cConfig,
            const CDslData &cData,
            bool bTones);

private:
    CGuiMain &m_cGuiMain;       //!< main window
}; // class CGuiOutput

#endif // __DSLTOOL_GUI_MAIN_H__
//_oOo_
