/*!
 * @file        txt.h
 * @brief       text definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DSLTOOL_STRING_H__
#define __DSLTOOL_DSLTOOL_STRING_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

/*!
 * @def         STRING
 * @brief       define string constant
 */
#define STRING extern LIBDSLTOOL_EXPORT const char *

/*!
 * @namespace   NStrDef
 * @brief       definition strings
 */
namespace NStrDef
{
    STRING FilePrefix;                  //!< file name prefix format string
    STRING FilePrefixDaemon;            //!< file name prefix format string

    STRING ConfigSys;                   //!< system config file
    STRING ConfigUser;                  //!< user config file

    STRING LogPath;                     //!< default log path
    STRING LogFile;                     //!< log file name format string
    STRING CaptureFile;                 //!< capture file name format string
    STRING DatFile;                     //!< dat file name format string
    STRING PngBitsFile;                 //!< bits.png file name format string
    STRING PngSNRFile;                  //!< snr.png file name format string
    STRING PngCharFile;                 //!< char.png file name format string

    STRING Protocol;                    //!< default protocol
    STRING Path;                        //!< default path

    STRING CollectHost;                 //!< default collectd host name
    STRING CollectSock;                 //!< default collectd socket

    STRING DaemonPid;                   //!< default daemon pid file
} // namespace NStrDef

/*!
 * @namespace   NStrErr
 * @brief       error strings
 */
namespace NStrErr
{
    STRING ArgParseError;               //!< parse error
    STRING ArgMultiOption;              //!< multiple options
    STRING ArgWrongModemType;           //!< wrong modem type
    STRING ArgWrongProtocolType;        //!< wrong protocol type

    STRING ArgInvalidOption;            //!< invalid option

    STRING ArgMissingOptionS;           //!< missing option (short only)
#ifdef WITH_LONGOPT
    STRING ArgMissingOptionL;           //!< missing option (long only)
    STRING ArgMissingOptionSL;          //!< missing option (short & long)
#endif // WITH_LONGOPT
    STRING ArgMissingArgS;              //!< missing argument (short only)
#ifdef WITH_LONGOPT
    STRING ArgMissingArgL;              //!< missing argument (long only)
    STRING ArgMissingArgSL;             //!< missing argument (short & long)
#endif // WITH_LONGOPT
    STRING ArgInvalidArgS;              //!< invalid argument (short only)
#ifdef WITH_LONGOPT
    STRING ArgInvalidArgL;              //!< invalid argument (long only)
    STRING ArgInvalidArgSL;             //!< invalid argument (short & long)
#endif // WITH_LONGOPT
    STRING ArgWrongArg;                 //!< wrong argument value

    STRING OutputType;                  //!< wrong output type

    STRING Daemon;                      //!< daemon start failure

    STRING FileStat;                    //!< file stat failure
    STRING FileOpen;                    //!< file open failure

    STRING LogOpen;                     //!< log open failure

    STRING Version;                     //!< dyn.lib version error

    STRING CollectOpen;                 //!< collectd open failure
    STRING CollectPut;                  //!< collectd put value error

    STRING ParserRegComp;               //!< regular expression compile error
    STRING ParserRegExec;               //!< regular expression exec error
} // namespace NStrErr

/*!
 * @namespace   NStrInfo
 * @brief       info strings
 */
namespace NStrInfo
{
    STRING LoadProtocol;                //!< protocol loaded
    STRING LoadModem;                   //!< modem loaded
    STRING LoadOutput;                  //!< output loaded
} // namespace NStrInfo

/*!
 * @namespace   NStrHelp
 * @brief       help strings
 */
namespace NStrHelp
{
    STRING Version;                     //!< version information
    STRING Help;                        //!< help message
} // namespace NStrHelp

/*!
 * @def         STRING
 * @brief       implement string constant
 */
#undef STRING
#define STRING const char *

#endif // __DSLTOOL_DSLTOOL_STRING_H__
//_oOo_
