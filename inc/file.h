/*!
 * @file        file.h
 * @brief       file class definitions
 * @details     file access helper class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_FILE_H__
#define __DSLTOOL_FILE_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include <stdio.h>
#include <stdarg.h>

/*!
 * @class       CFile
 * @brief       file access
 */
class LIBDSLTOOL_EXPORT CFile
{
public:
    CFile (FILE* pFile = NULL);
    virtual ~CFile (void);

private:
    //! @cond
    CFile (CFile const &cFile);
    CFile &operator = (CFile const &cFile);
    //! @endcond

public:
    bool Open (FILE* pFile);
    bool Open (const char *strFile, const char *strMode);
    void Close (void);
    static bool Exist (const char *strFile, bool bCreate = false);
    static void Delete (const char *strFile);

    bool Read (void *pBuffer, size_t nBuffer, size_t &nRead);
    bool Write (const void *pBuffer, size_t nBuffer, size_t &nWrite);

    void VPrintf (const char *strFormat, va_list vaArgs);
    void Printf (const char *strFormat, ...);

private:
    FILE       *m_pFile;                //!< file handle
}; // class CFile

#endif // __DSLTOOL_FILE_H__
//_oOo_
