/*!
 * @file        config-data.h
 * @brief       application class definitions
 * @details     config data class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        29.04.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_CONFIG_DATA_H__
#define __DSLTOOL_CONFIG_DATA_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "modem.h"
#include "protocol.h"
#include "log.h"

/*!
 * @class       CConfigData
 * @brief       configuration data
 */
class LIBDSLTOOL_EXPORT CConfigData
{
    friend class CRun;
    friend class CRunDaemon;
    friend class CConfigList;
    friend class CDslToolGui;
    friend class CGuiMain;
    friend class CGuiConfig;

public:
    CConfigData (void);

private:
    //! @cond
    CConfigData (const CConfigData &cConfigData);
    CConfigData &operator = (const CConfigData &cConfigData);
    //! @endcond

public:
    void Reset (void);
    void SetConfig (void);
    void SetDefaults (void);

public:
    const char *ModemHost (void) const;
    const char *Path (void) const;
    const char *Prefix (void) const;
    const char *CollectHost (void) const;
    const char *CollectSock (void) const;
    int Interval (void) const;
    const char *Command(void) const;

public:
    static const unsigned long s_ulOutDat       = 0x01; //!< output to .dat
    static const unsigned long s_ulOutStd       = 0x02; //!< output to stdout
    static const unsigned long s_ulOutTones     = 0x04; //!< tones to stdout
    static const unsigned long s_ulOutGraph     = 0x08; //!< tones to .png
    static const unsigned long s_ulOutCollect   = 0x10; //!< tones to collectd


private:
    bool m_bHelp;                       //!< print help
    bool m_bVersion;                    //!< print version info
    CString m_strConfig;                //!< config file name
    CString m_strModemType;             //!< modem type
    CString m_strProtocol;              //!< protocol type
    CString m_strExtra;                 //!< extra argument to protocol handler
    CString m_strModemHost;             //!< host name
    int m_nPort;                        //!< port number
    CProtocol::EEthType m_eEthType;     //!< protocol type
    CString m_strUser;                  //!< user name
    CString m_strPass;                  //!< password
    CString m_strPath;                  //!< output path
    CString m_strPrefix;                //!< output file prefix
    CLog::ELevel m_eLogLevel;           //!< log level
    bool m_bLog;                        //!< log flag
    CString m_strLogFile;               //!< log file name
#ifdef ENABLE_CAPTURE
    bool m_bCapture;                    //!< capture flag
    CString m_strCaptureFile;           //!< capture file name
#endif // ENABLE_CAPTURE
    CString m_strCollectHost;           //!< rrd host name
    CString m_strCollectSock;           //!< collect socket name
    bool m_bDaemon;                     //!< daemonize
    int m_nInterval;                    //!< update interval
    CString m_strPidFile;               //!< pid file name
    bool m_bBackground;                 //!< put into background
    unsigned long m_ulOutput;           //!< output bitmask
    CModem::ECommand m_eCommand;        //!< command number
}; // class CConfigData

#endif // __DSLTOOL_CONFIG_DATA_H__
//_oOo_
