/*!
 * @file        dynlib.h
 * @brief       dynamic library definitions
 * @details     dynamic library functions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        03.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DYNLIB_H__
#define __DSLTOOL_DYNLIB_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

/*!
 * @class       CDynLib
 * @brief       dynamic library class
 */
class LIBDSLTOOL_EXPORT CDynLib
{
public:
    CDynLib (void);
    ~CDynLib (void);

private:
    //! @cond
    CDynLib (CDynLib const &cDynLib);
    CDynLib &operator = (CDynLib const &cDynLib);
    //! @endcond

public:
    bool Open (const char *strName);
    void Close (void);
    void *Symbol (const char *strSymbol);

private:
#ifdef _WIN32
    HMODULE m_pHandle;        //!< library handle
#else // _WIN32
    void *m_pHandle;        //!< library handle
#endif // _WIN32
}; // class CDynLib

#endif // __DSLTOOL_DYNLIB_H__
//_oOo_
