/*!
 * @file        xml-parser.h
 * @brief       XML parser class definitions
 * @details     DOM and SAX2 parser
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        05.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_XML_PARSER_H__
#define __DSLTOOL_XML_PARSER_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "regex-parser.h"

class CXmlContext;
/*!
 * @class       CXmlParser
 * @brief       pure virtual XML parser base class
 */
class LIBDSLTOOL_EXPORT CXmlParser : public CParser
{
protected:
    CXmlParser (CModem* pModem);

private:
    //! @cond
    CXmlParser (CXmlParser const &cXmlParser);
    CXmlParser &operator = (CXmlParser const &cXmlParser);
    //! @endcond

public:
    virtual ~CXmlParser (void);

public:
    virtual CParser *Close (void);
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

protected:
    bool m_bHTML;               //!< HTML or XML flag
    CXmlContext *m_pContext;    //!< XML parser context pointer
    CParser* m_pNext;           //!< next parser
}; // class CXmlParser

/*!
 * @class       CSaxParser
 * @brief       XML SAX2 parser
 */
class LIBDSLTOOL_EXPORT CSaxParser : public CXmlParser
{
public:
    CSaxParser (CModem *pModem);

public:
/*!
 * @fn          virtual CParser *ParseLine (const char *strLine)
 * @brief       parse received line
 * @details     pure virtual function, see derived classes
 * @param[in]   strLine
 *              received line
 * @return      next parser
 */
    virtual CParser *ParseLine (const char *strLine) = 0;

public:
    virtual void Open (EContent eContent);

public:
    void StartDocument (void);
    void EndDocument (void);
    void StartElement (const char *strElement);
    void EndElement (const char *strElement);
    void Attribute (const char *strAttribute, const char *strValue);
    void Data (const char *strData);
}; // class CSaxParser

/*!
 * @class       CDomParser
 * @brief       XML DOM tree parser
 */
class LIBDSLTOOL_EXPORT CDomParser : public CXmlParser
{
public:
    CDomParser (CModem *pModem);

public:
    virtual void Open (EContent eContent);
    virtual CParser *Close (void);
/*!
 * @fn          virtual CParser* Finished (void)
 * @brief       parse has finished
 * @details     puer virtual function
 * @return      next parser
 */
    virtual CParser* Finished (void) = 0;

protected:
    void XPathNamespace (const char *strPrefix, const char *strURI);
    bool XPathQuery (long &nResult, const char *strXPath);
    bool XPathQuery (double &fResult, const char *strXPath);
    bool XPathQuery (CString &strResult, const char *strXPath);
}; // class CSaxParser

/*!
 * @class       CDomCmdParser
 * @brief       XML DOM tree command parser
 * @details     send command in constructor
 */
class LIBDSLTOOL_EXPORT CDomCmdParser : public CDomParser
{
protected:
    CDomCmdParser (CModem *pModem, const char *strCmd);
}; // class CDomCmdParser

#endif // __DSLTOOL_XML_PARSER_H__
//_oOo_
