/*!
 * @file        daemon.h
 * @brief       application class definitions
 * @details     daemon execution classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DAEMON_H__
#define __DSLTOOL_DAEMON_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include <semaphore.h>
#include "run.h"

/*!
* @class       CRunDaemon
* @brief       daemon run class
*/
class LIBDSLTOOL_EXPORT CRunDaemon : public CRun
{
protected:
    CRunDaemon (void);

public:
    virtual ~CRunDaemon (void);

public:
    virtual bool Run (void);

private:
    void InitSignal (void);
    void InitTimer (void);
    void SigHandler (int nSignal);

private:
    void WritePid (void);
    void DeletePid (void);
    bool Daemonize (void);
    bool Loop (void);

private:
    bool m_bLoop;                       //!< loop flag
    sem_t m_tSem;                       //!< trigger semaphore

}; // class CRunDaemon

#endif // __DSLTOOL_DAEMON_H__
//_oOo_
