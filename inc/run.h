/*!
 * @file        run.h
 * @brief       application class definitions
 * @details     run execution base class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_RUN_H__
#define __DSLTOOL_RUN_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include "config-list.h"

#include "output.h"

/*!
* @class       CRun
* @brief       abstract run base class
*/
class LIBDSLTOOL_EXPORT CRun
{
protected:
    CRun (void);

private:
    //! @cond
    CRun (CRun const &cRun);
    CRun &operator = (CRun const &cRun);
    //! @endcond

protected:
    static CRun *g_pRun;

public:
    virtual ~CRun (void);

public:
    bool Disable (int nEntry);
    bool ReadConfigData (int nArgc, char **astrArgv);
    bool ReadConfigData (void);
    bool CheckConfigData (bool bSilent=false);
    bool WriteConfigData (void);
    void InitOutput (COutputLib::EIndex eIndex, bool bLoad, bool bTones);
    void InitOutput (COutputLib::EIndex eIndex, COutput *pOutput, bool bTones);
    bool LoadOutput (void);
    bool LoadProtocol (void);
    bool LoadModem (void);
    bool PrintInfo (const char *strArg0);

public:
/*!
* @fn          virtual bool Run (void)
* @brief       execute protocol and parser
* @details     pure virtual function, see derived classes
*              @ref CRunOneShot and @ref CRunDaemon
* @return      success
* @retval      true
*              o.k.
* @retval      false
*              run failed
*/
    virtual bool Run (void) = 0;
    static void _SigHandler (int nSignal);
/*!
* @fn          virtual void SigHandler (int nSignal)
* @brief       signal handler
* @details     pure virtual function, see derived classes
*              @ref CRunOneShot and @ref CRunDaemon
* @param[in]   nSignal
*              signal number
*/
    virtual void SigHandler (int nSignal) = 0;

    bool Init (void);
    bool Process (void);
    void Output (void);
    void Finish (void);

protected:
    CConfigData m_cConfig;              //!< configuration data
    CConfigList m_cConfigList;          //!< configuration list
    CDslData m_cData;                   //!< dsl data storage
    COutputLib *m_apOutput[COutputLib::eIndexMax]; //!< output array
    CAuth *m_pAuth;                     //!< authenticator pointer
    CProtocol *m_pProtocol;             //!< protocol pointer
    CModem *m_pModem;                   //!< modem pointer
}; // class CRun

#endif // __DSLTOOL_RUN_H__
//_oOo_
