/*!
 * @file        basis.h
 * @brief       basic class definitions
 * @details     string and tone classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_BASIS_H__
#define __DSLTOOL_BASIS_H__

//! @cond
#include "libdsltool-export.h"
//! @endcond

#include <stddef.h>

#ifdef WITH_LOCALE
#include <locale.h>

#ifdef _WIN32
    #define locale_t _locale_t
    #define freelocale _free_locale
    #define strtol_l _strtol_l
    #define strtoul_l _strtoul_l
    #define strtod_l _strtod_l
    #define strncasecmp _strnicmp
    #define snprintf _snprintf
#endif // _WIN32
#endif // WITH_LOCALE

#ifndef ARRAY_SIZE
/*!
 * @def         ARRAY_SIZE(a)
 * @brief       get array dimension
 * @param[in]   a
 *              array
 */
    #define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))
#endif // ARRAY_SIZE

/*!
 * @class       CString
 * @brief       simple string class
 */
class LIBDSLTOOL_EXPORT CString
{
public:
    CString (size_t nSize = 0);
    CString (const char *strString);
    CString (const char *strString, size_t nLength);
    ~CString (void);

private:
    //! @cond
    CString (const CString& cString);
    //! @endcond

#ifdef WITH_LOCALE
public:
    static void InitLocale (void);
    static void FreeLocale (void);
#endif // WITH_LOCALE

public:
    CString& operator = (const CString& cString);
    CString& operator = (const char *strString);
    CString& operator += (const CString& cString);
    CString& operator += (const char *strString);
    operator const char *(void) const;

    CString& Format (const char *strFormat, ...);
    CString& Timestamp (void);
    CString& Printable (void);
    CString& Upper (void);
    CString& Lower (void);

public:
    long Int (int nBase = 0) const;
    unsigned long UInt (int nBase = 0) const;
    double Double (void) const;
    size_t Length (void) const;
    bool Compare (const char *strCompare) const;
    bool CaseCompare (const char *strCompare) const;

private:
#ifdef WITH_LOCALE
    static locale_t g_tLocale;
#endif // WITH_LOCALE
    char *m_strString;          //!< string pointer
    size_t m_nSize;             //!< string size
}; // class CString

/*!
 * @class       CTone
 * @brief       tone array class
 */
class LIBDSLTOOL_EXPORT CTone
{
public:
    CTone (void);
    CTone (size_t nSize);
    virtual ~CTone (void);

private:
    //! @cond
    CTone (CTone const &cTone);
    CTone operator = (CTone const &cTone);
    //! @endcond

public:
    int operator [] (size_t nIndex) const;

public:
    void Init (void);
    void Scale (int nAdd, int nMul, int nDiv, int nBits = 0);

/*!
 * @fn          size_t Size (void) const
 * @brief       get array size
 * @return      array size
 */
    size_t Size (void) const {return m_nSize;}
    void Size (size_t nSize);

/*!
 * @fn          size_t Index (void)
 * @brief       get array index
 * @return      array index
 */
    size_t Index (void) {return m_nIndex;}
    void Index (size_t nIndex);

public:
    bool Set (CTone &cSource, size_t nMin = 0, size_t nMax = 0);
    bool Merge (CTone &cSource, size_t nMin = 0, size_t nMax = 0);
    bool Set (int *pnSource, size_t nSize);
    bool Set (size_t nIndex, int nTone);
    bool SetInc (int nTone);

private:
    int *m_pnTones;             //!< tone array
    size_t m_nSize;             //!< tone array size
    size_t m_nIndex;            //!< current index
}; // class CTone

#endif // __DSLTOOL_BASIS_H__
//_oOo_
