/*!
 * @file        oneshot.cpp
 * @brief       oneshot application class implementation
 * @details     oneshot execution classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <signal.h>

#include "oneshot.h"
#include "txt.h"

/*!
 * @fn          CRunOneShot::CRunOneShot (void)
 * @brief       constructor
 */
CRunOneShot::CRunOneShot (void)
: CRun ()
{
}

/*!
 * @fn          CRunOneShot::~CRunOneShot(void)
 * @brief       destructor
 */
CRunOneShot::~CRunOneShot (void)
{
}


/*!
 * @fn          bool CRunOneShot::Run (void)
 * @brief       execute protocol and parser
 * @details     initialize signal handler to allow abort processing
 * @n           call @ref Process once
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              run failed
 */
bool CRunOneShot::Run (void)
{
    bool bRet = false;

    InitSignal ();

    if (Init ())
    {
        bRet = Process ();
        if (bRet)
        {
            Output ();
        }
    }
    Finish ();
    return bRet;
}

/*!
 * @fn          void CRunOneShot::InitSignal (void)
 * @brief       initialize signal handler
 */
void CRunOneShot::InitSignal (void)
{
    signal (SIGINT, _SigHandler);
#ifndef _WIN32
    signal (SIGKILL, _SigHandler);
#endif // _WIN32
}

/*!
 * @fn          void CRunOneShot::SigHandler (int nSignal)
 * @brief       signal handler
 * @details     abort protocol parser on SIGINT and SIGKILL
 * @param[in]   nSignal
 *              signal number
 */
void CRunOneShot::SigHandler (int nSignal)
{
    LogDebug ("signal %d received\n", nSignal);

    switch (nSignal)
    {
    case SIGINT:
#ifndef _WIN32
    case SIGKILL:
#endif // _WIN32
        if (m_pProtocol)
        {
            m_pProtocol->Stop ();
        }
        break;
    }
}

//_oOo_
