/*!
 * @file        config-list.cpp
 * @brief       application class implementation
 * @details     config list class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        29.04.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 *
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#define __STDC__ 1
#define __GNU_LIBRARY__
#endif
#include <getopt.h>

#include "config-list.h"
#include "txt.h"

//! @cond
namespace NStrConfig
{
    STRING Help =
            "      print this help\n";
    STRING Version =
            "      show version info\n";
    STRING Config =
            "      read config from <file>\n"
            "      (default '~/.dsltool.conf' or '/etc/dsltool.conf'\n";
    STRING LogLevel =
            "      set loglevel to <level>\n"
            "      <level> is one of 'error'(default), 'warning' 'info'\n"
            "      'debug', 'trace' or 'extra'\n";
    STRING Verbose =
            "      increase verbosity (may be used more than once)\n";
    STRING Log =
            "      log to <file> (default '/var/log/<prefix>.log')\n"
            "      log to stderr when <file> is '-'\n"
            "      log to syslog when <file> is '#'\n"
            "      log to <path>/<prefix>.log when <file> is '+'\n";
#ifdef ENABLE_CAPTURE
    STRING Capture =
            "      capture to <file>\n"
            "      (default './dsltool-<host>-<timestamp>-<command>.pcap'\n";
#endif // ENABLE_CAPTURE
    STRING Path =
            "      write outputs to <path> (default '/var/run')\n";
    STRING Prefix =
            "      output file name prefix (default 'dsltool-<host>')\n";
    STRING ModemType =
            "      specify modem type (see documentation)\n";
    STRING ProtocolType =
            "      use protocol <prot> to connect (default: telnet)\n";
    STRING Extra =
            "      extra arguments to protocol handler\n"
            "      (e.g. read from capture file instead of modem\n"
            "      when <prot>= 'pcap-telnet')\n";
    STRING ModemHost =
            "      connect to modem <host> (name or IP)\n";
    STRING ModemPort =
            "      use alternative port number (default telnet:23, http:80)\n";
    STRING EthType =
            "      use eth type <type>\n"
            "      <type> is one of 'auto'(default), 'IPv4' or 'IPv6'\n";
    STRING IPv4 =
            "      use IPv4\n";
    STRING IPv6 =
            "      use IPv6\n";
    STRING User =
            "      modem user name \n";
    STRING Pass =
            "      modem password\n";
    STRING Background =
            "      put into background (daemonize)\n";
    STRING Interval =
            "      daemon update interval <int> seconds (default 10s)\n";
    STRING Pid =
            "      pidfile <pid> (default '/var/run/dsltoold-<host>.pid')\n";
    STRING CollectdHost =
            "      send collectd data to <host> (name or IP)\n"
            "           (default 'localhost')\n";
    STRING CollectdSock =
            "      send collectd data to <sock>\n"
            "      (default '/var/run/collectd.sock')\n";
    STRING Output =
            "      output to be written, flags ored of:\n"
            "       1: .dat file\n"
            "       2: stdout (short)\n"
            "       4: stdout (long)\n"
            "       8: .png graph\n"
            "       16: collectd/rrd data\n";
    STRING Command =
            "      command, <command> one of 'info', 'status',\n"
            "      'resync', 'reboot' (default 'info')\n";
} // namespace NStrConfig
//! @endcond

/*!
 * @fn          CConfigList::CConfigList (void)
 * @brief       constructor
 * @details     fill option array
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
CConfigList::CConfigList (void)
{
    for (int nIndex = 0; nIndex < eIndexMax; nIndex++)
    {
        m_apOptions[nIndex] = 0;
    }

    // used shorts: 46bcCeEfFhHilLmNpPorsUvVx
    m_apOptions[eHelp] = new CConfigOption (
            'h', "help", NStrConfig::Help, "",
            CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgNone,
            CConfigOption::eTypeBool);
    m_apOptions[eVersion] = new CConfigOption (
            'V', "version", NStrConfig::Version, "",
            CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgNone,
            CConfigOption::eTypeBool);
    m_apOptions[eConfig] = new CConfigOption (
            'c', "config", NStrConfig::Config, "file",
            CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeBool);
    m_apOptions[eModemType] = new CConfigOption (
            'm', "modem-type", NStrConfig::ModemType, "modem",
            CConfigOption::eFlagsRequired,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[eProtocolType] = new CConfigOption (
            'C', "protocol-type", NStrConfig::ProtocolType, "protocol",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[eExtra] = new CConfigOption (
            'e', "extra", NStrConfig::Extra, "extra",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[eModemHost] = new CConfigOption (
            'H', "modem-host", NStrConfig::ModemHost, "host",
            CConfigOption::eFlagsRequired,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[ePort] = new CConfigOption (
            'N', "modem-port", NStrConfig::ModemPort, "port",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeInt);
    m_apOptions[eEthType] = new CConfigOption (
            'E', "eth-type", NStrConfig::EthType, "type",
            CConfigOption::eFlagsNoShort,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeEthType);
    m_apOptions[eEthType4] = new CConfigOption (
            '4', "", NStrConfig::IPv4, "",
            CConfigOption::eFlagsNoLong | CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgNone,
            CConfigOption::eTypeBool);
    m_apOptions[eEthType6] = new CConfigOption (
            '6', "", NStrConfig::IPv6, "",
            CConfigOption::eFlagsNoLong | CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgNone,
            CConfigOption::eTypeBool);
    m_apOptions[eUser] = new CConfigOption (
            'U', "user", NStrConfig::User, "user",
            CConfigOption::eFlagsRequired,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[ePass] = new CConfigOption (
            'P', "pass", NStrConfig::Pass, "pass",
            CConfigOption::eFlagsRequired,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[ePath] = new CConfigOption (
            'f', "path", NStrConfig::Path, "path",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[ePrefix] = new CConfigOption (
            'F', "prefix", NStrConfig::Prefix, "prefix",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[eLogLevel] = new CConfigOption (
            'L', "log-level", NStrConfig::LogLevel, "level",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeLogLevel);
    m_apOptions[eVerbose] = new CConfigOption (
            'v', "verbose", NStrConfig::Verbose, "",
            CConfigOption::eFlagsInc | CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgNone,
            CConfigOption::eTypeBool);
    m_apOptions[eLogFile] = new CConfigOption (
            'l', "log", NStrConfig::Log, "file",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgOptional,
            CConfigOption::eTypeString);
#ifdef ENABLE_CAPTURE
    m_apOptions[eCapture] = new CConfigOption (
            'd', "capture", NStrConfig::Capture, "file",
            CConfigOption::eFlagsOptional | CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgNone,
            CConfigOption::eTypeBool);
#endif // ENABLE_CAPTURE
    m_apOptions[eCollectHost] = new CConfigOption (
            'r', "collectd-host", NStrConfig::CollectdHost, "host",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[eCollectSock] = new CConfigOption (
            's', "collectd-sock", NStrConfig::CollectdSock, "sock",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[eInterval] = new CConfigOption (
            'i', "interval", NStrConfig::Interval, "interval",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeInt);
    m_apOptions[ePidFile] = new CConfigOption (
            'p', "pid", NStrConfig::Pid, "file",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeString);
    m_apOptions[eBackground] = new CConfigOption (
            'b', "background", NStrConfig::Background, "",
            CConfigOption::eFlagsOptional,
            CConfigOption::eArgNone,
            CConfigOption::eTypeBool);
    m_apOptions[eOutput] = new CConfigOption (
            'o', "output", NStrConfig::Output, "output",
            CConfigOption::eFlagsOptional | CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeUInt);
    m_apOptions[eCommand] = new CConfigOption (
            'x', "command", NStrConfig::Command, "command",
            CConfigOption::eFlagsOptional | CConfigOption::eFlagsNoConfig,
            CConfigOption::eArgRequired,
            CConfigOption::eTypeCommand);
}

/*!
 * @fn          CConfigList::~CConfigList (void)
 * @brief       destructor
 * @details     cleanup option array
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
CConfigList::~CConfigList (void)
{
    for (int nIndex = 0; nIndex < eIndexMax; nIndex++)
    {
        if (m_apOptions[nIndex])
        {
            delete m_apOptions[nIndex];
            m_apOptions[nIndex] = NULL;
        }
    }
}

/*!
 * @fn          void CConfigList::Reset (void)
 * @brief       reset config options
 */
void CConfigList::Reset (void)
{
    int nIndex;
    for (nIndex = 0; nIndex < eIndexMax; nIndex++)
    {
        m_apOptions[nIndex]->Reset ();
    }
}

/*!
 * @fn          bool CConfigList::Disable (int nIndex)
 * @brief       disable entry
 * @param[in]   nIndex
 *              option index
 * @details     disabled entries are not added to short and long options
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CConfigList::Disable (int nIndex)
{
    bool bRet = false;
    if ((nIndex >= 0) && (nIndex < eIndexMax))
    {
        if (m_apOptions[nIndex])
        {
            bRet = m_apOptions[nIndex]->Disable ();
        }
    }
    return bRet;
}

/*!
 * @fn          unsigned long CConfigList::GetFlags (int nIndex)
 * @brief       get flags
 * @param[in]   nIndex
 *              option index
 * @return      flags
 */
unsigned long CConfigList::GetFlags (int nIndex)
{
    unsigned long ulRet = 0;
    if ((nIndex >= 0) && (nIndex < eIndexMax))
    {
        if (m_apOptions[nIndex])
        {
            ulRet = m_apOptions[nIndex]->GetFlags ();
        }
    }
    return ulRet;
}

/*!
 * @fn          int CConfigList::GetShort (int nIndex)
 * @brief       get short option character
 * @param[in]   nIndex
 *              option index
 * @return      option character
 * @retval      -1
 *              disabled
 */
int CConfigList::GetShort (int nIndex)
{
    int nRet = -1;
    if ((nIndex >= 0) && (nIndex < eIndexMax))
    {
        if (m_apOptions[nIndex])
        {
            nRet = m_apOptions[nIndex]->GetShort ();
        }
    }
    return nRet;
}

/*!
 * @fn          const char *CConfigList::GetLong (int nIndex)
 * @brief       get long option
 * @param[in]   nIndex
 *              option index
 * @return      long option
 * @retval      NULL
 *              disabled
 */
const char *CConfigList::GetLong (int nIndex)
{
    const char *strRet = NULL;
    if ((nIndex >= 0) && (nIndex < eIndexMax))
    {
        if (m_apOptions[nIndex])
        {
            strRet = m_apOptions[nIndex]->GetLong ();
        }
    }
    return strRet;
}

/*!
 * @fn          CConfigOption::EArg CConfigList::GetOptArg (int nIndex)
 * @brief       get argument type
 * @param[in]   nIndex
 *              option index
 * @return      argument type
 */
CConfigOption::EArg CConfigList::GetOptArg (int nIndex)
{
    CConfigOption::EArg eArg = CConfigOption::eArgNone;

    if ((nIndex >= 0) && (nIndex < eIndexMax))
    {
        if (m_apOptions[nIndex])
        {
            eArg = m_apOptions[nIndex]->GetOptArg ();
        }
    }
    return eArg;
}

/*!
 * @fn          bool CConfigList::SetOption (int nIndex, const char *strValue)
 * @brief       set argument value
 * @details     tested with TestArg before set
 * @param[in]   nIndex
 *              option index
 * @param[in]   strValue
 *              option argument
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigList::SetOption (int nIndex, const char *strValue)
{
    bool bRet = false;
    if ((nIndex >= 0) && (nIndex < eIndexMax))
    {
        if (m_apOptions[nIndex])
        {
            bRet = m_apOptions[nIndex]->SetOption (strValue);
        }
    }
    return bRet;
}

/*!
 * @fn          bool CConfigList::WriteData (CConfigData &cConfig)
 * @brief       write config data
 * @details     read option list and write to config data
 * @param[in]   cConfig
 *              config data
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigList::WriteData (CConfigData &cConfig)
{
    bool bRet = true;
    bool bEthType, bEthType4, bEthType6;
    int nVerbose = 0;

    m_apOptions[eHelp]->GetValue (cConfig.m_bHelp);
    m_apOptions[eVersion]->GetValue (cConfig.m_bVersion);

    m_apOptions[eConfig]->GetValue (cConfig.m_strConfig );

    m_apOptions[eModemType]->GetValue (cConfig.m_strModemType);
    m_apOptions[eProtocolType]->GetValue (cConfig.m_strProtocol);
    m_apOptions[eExtra]->GetValue (cConfig.m_strExtra);
    m_apOptions[eModemHost]->GetValue (cConfig.m_strModemHost);
    m_apOptions[ePort]->GetValue (cConfig.m_nPort);

    m_apOptions[eEthType]->GetValue (bEthType);
    m_apOptions[eEthType4]->GetValue (bEthType4);
    m_apOptions[eEthType6]->GetValue (bEthType6);
    if ((bEthType && bEthType4) ||
        (bEthType && bEthType6) ||
        (bEthType4 && bEthType6))
    {
#ifdef WITH_LONGOPT
        LogError (NStrErr::ArgMultiOption, "-4, -6 or --ethtype=<arg>" );
#else
        LogError (NStrErr::ArgMultiOption, "-4 or -6" );
#endif // WITH_LONGOPT
    }
    else if (bEthType4)
    {
        cConfig.m_eEthType = CProtocol::eIPv4;
    }
    else if (bEthType4)
    {
        cConfig.m_eEthType = CProtocol::eIPv6;
    }
    else if (bEthType)
    {
        m_apOptions[eEthType]->GetValue (cConfig.m_eEthType);
    }

    m_apOptions[eUser]->GetValue (cConfig.m_strUser);
    m_apOptions[ePass]->GetValue (cConfig.m_strPass);

    m_apOptions[ePath]->GetValue (cConfig.m_strPath);
    m_apOptions[ePrefix]->GetValue (cConfig.m_strPrefix);

    // reset log level to avoid increase applied twice
    cConfig.m_eLogLevel = CLog::eError;
    m_apOptions[eLogLevel]->GetValue (cConfig.m_eLogLevel);
    m_apOptions[eVerbose]->GetValue (nVerbose);
    for (;nVerbose > 0;nVerbose--)
    {
        switch (cConfig.m_eLogLevel)
        {
        case CLog::eError:
            cConfig.m_eLogLevel = CLog::eWarning;
            break;
        case CLog::eWarning:
            cConfig.m_eLogLevel = CLog::eInfo;
            break;
        case CLog::eInfo:
            cConfig.m_eLogLevel = CLog::eDebug;
            break;
        case CLog::eDebug:
            cConfig.m_eLogLevel = CLog::eTrace;
            break;
        case CLog::eTrace:
            cConfig.m_eLogLevel = CLog::eExtra;
            break;
        case CLog::eExtra:
            break;
        }
    }
    m_apOptions[eLogFile]->GetValue (cConfig.m_bLog);
    m_apOptions[eLogFile]->GetValue (cConfig.m_strLogFile);

#ifdef ENABLE_CAPTURE
    m_apOptions[eCapture]->GetValue (cConfig.m_bCapture);
    m_apOptions[eCapture]->GetValue (cConfig.m_strCaptureFile);
#endif // ENABLE_CAPTURE

    m_apOptions[eCollectHost]->GetValue (cConfig.m_strCollectHost);
    m_apOptions[eCollectSock]->GetValue (cConfig.m_strCollectSock);

    m_apOptions[eInterval]->GetValue (cConfig.m_nInterval);
    m_apOptions[ePidFile]->GetValue (cConfig.m_strPidFile);
    m_apOptions[eBackground]->GetValue (cConfig.m_bBackground);

    m_apOptions[eOutput]->GetValue (cConfig.m_ulOutput);
    m_apOptions[eCommand]->GetValue (cConfig.m_eCommand);

    return bRet;
}

/*!
 * @fn          bool CConfigList::ReadData (const CConfigData &cConfig)
 * @brief       read config data
 * @details     read config data and write to option list
 * @param[in]   cConfig
 *              config data
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigList::ReadData (const CConfigData &cConfig)
{
    CString strTemp;

    Reset ();

    m_apOptions[eConfig]->SetConfig (cConfig.m_strConfig );
    m_apOptions[eModemType]->SetConfig (cConfig.m_strModemType);
    m_apOptions[eProtocolType]->SetConfig (cConfig.m_strProtocol);
    m_apOptions[eExtra]->SetConfig (cConfig.m_strExtra);
    m_apOptions[eModemHost]->SetConfig (cConfig.m_strModemHost);

    strTemp.Format ("%d", cConfig.m_nPort);
    m_apOptions[ePort]->SetConfig (strTemp);
    m_apOptions[eEthType]->SetConfig (CProtocol::GetProtocol(cConfig.m_eEthType));

    m_apOptions[eUser]->SetConfig (cConfig.m_strUser);
    m_apOptions[ePass]->SetConfig (cConfig.m_strPass);

    m_apOptions[ePath]->SetConfig (cConfig.m_strPath);
    m_apOptions[ePrefix]->SetConfig (cConfig.m_strPrefix);

    m_apOptions[eLogLevel]->SetConfig (CLog::GetLevel(cConfig.m_eLogLevel));

    if (cConfig.m_bLog)
    {
        m_apOptions[eLogFile]->SetConfig (cConfig.m_strLogFile);
    }

    m_apOptions[eCollectHost]->SetConfig (cConfig.m_strCollectHost);
    m_apOptions[eCollectSock]->SetConfig (cConfig.m_strCollectSock);

    strTemp.Format ("%d", cConfig.m_nInterval);
    m_apOptions[eInterval]->SetConfig (strTemp);
    m_apOptions[ePidFile]->SetConfig (cConfig.m_strPidFile);
    if (cConfig.m_bBackground)
    {
        m_apOptions[eBackground]->SetConfig (NULL);
    }

    strTemp.Format ("0x%x", cConfig.m_ulOutput);
    m_apOptions[eOutput]->SetConfig (strTemp);
    m_apOptions[eCommand]->SetConfig (cConfig.Command());

    return true;
}

/*!
 * @fn          bool CConfigList::ReadArgs (int nArgc, char **astrArgv)
 * @brief       parse arguments
 * @param       nArgc
 *              number of arguments
 * @param       astrArgv
 *              argument array
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              read failure
 */
bool CConfigList::ReadArgs (int nArgc, char **astrArgv)
{
    bool bRet = true;
    int nIndex = 0;
    CString strShortOpts ("");
#ifdef WITH_LONGOPT
    int nLongEntry = 0;
    struct option asLongOpts[eIndexMax+1];
#endif // WITH_LONGOPT

    for (nIndex = 0; nIndex < eIndexMax; nIndex++)
    {
        unsigned long ulFlags = GetFlags (nIndex);

        if (0 == (ulFlags & CConfigOption::eFlagsDisabled))
        {
            if (0 == (ulFlags & CConfigOption::eFlagsNoShort))
            {
                char strOpt[2] = {(char)GetShort (nIndex), '\0'};
                strShortOpts += strOpt;
                switch (GetOptArg (nIndex))
                {
                case CConfigOption::eArgNone:
                    break;
                case CConfigOption::eArgRequired:
                    strShortOpts += ":";
                    break;
                case CConfigOption::eArgOptional:
                    strShortOpts += "::";
                    break;
                }
            }

#ifdef WITH_LONGOPT
            if (0 == (ulFlags & CConfigOption::eFlagsNoLong))
            {
                asLongOpts[nLongEntry].name = GetLong (nIndex);
                asLongOpts[nLongEntry].has_arg = GetOptArg (nIndex);
                asLongOpts[nLongEntry].flag = NULL;
                asLongOpts[nLongEntry].val = GetShort (nIndex);
                nLongEntry++;
            }
#endif // WITH_LONGOPT
        }
    }
#ifdef WITH_LONGOPT
    asLongOpts[nLongEntry].name = NULL;
    asLongOpts[nLongEntry].has_arg = 0;
    asLongOpts[nLongEntry].flag = NULL;
    asLongOpts[nLongEntry].val = 0;
#endif // WITH_LONGOPT


    while (1)
    {
        bool bFound = false;
        int nOpt;
#ifdef WITH_LONGOPT
         nOpt = getopt_long (nArgc, astrArgv, strShortOpts, asLongOpts, NULL);
#else
         nOpt = getopt (nArgc, astrArgv, strShortOpts);
#endif // WITH_LONGOPT

        if (-1 == nOpt)
        {
            break;
        }

        for (nIndex = 0; nIndex < eIndexMax; nIndex++)
        {
            if (nOpt == GetShort (nIndex))
            {
                if (SetOption (nIndex, optarg))
                {
                    bFound = true;
                }
                else
                {
#ifdef WITH_LONGOPT
                    unsigned long ulFlags = GetFlags (nIndex);

                    if (ulFlags & CConfigOption::eFlagsNoLong)
                    {
#endif // WITH_LONGOPT
                        LogError (NStrErr::ArgInvalidArgS, GetShort (nIndex), optarg);
#ifdef WITH_LONGOPT
                    }
                    else if (ulFlags & CConfigOption::eFlagsNoShort)
                    {
                        LogError (NStrErr::ArgInvalidArgL, (const char*)GetLong (nIndex), optarg);
                    }
                    else
                    {
                        LogError (NStrErr::ArgInvalidArgSL, GetShort (nIndex), (const char*)GetLong (nIndex), optarg);
                    }
#endif // WITH_LONGOPT
                    bRet = false;
                }
                break;
            }
        }
        if (!bFound)
        {
            LogError (NStrErr::ArgInvalidOption, nOpt);
            bRet = false;
        }
    }

    if (optind < nArgc)
    {
        LogError (NStrErr::ArgParseError);
        bRet = false;
    }

    return bRet;
}

/*!
 * @fn          bool CConfigList::TestRequired (void)
 * @brief       test required options
 * @param[in]   bSilent
 *              don't log message
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigList::TestRequired (bool bSilent)
{
    bool bRet = true;

    for (int nIndex = 0; nIndex < eIndexMax; nIndex++)
    {
        if (m_apOptions[nIndex])
        {
            if (!m_apOptions[nIndex]->TestRequired (bSilent))
            {
                bRet = false;
            }
        }
    }

    return bRet;
}

/*!
 * @fn          bool CConfigList::ParseConfig (char *strBuffer, size_t nSize)
 * @brief       parse config file buffer
 * @param[in]   strBuffer
 *              config file buffer
 * @param[in]   nSize
 *              buffer size
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigList::ParseConfig (char *strBuffer, size_t nSize)
{
    bool bRet = true;
    size_t nIndex;
    char *strLine = strBuffer;

    for (nIndex = 0; nIndex < nSize; nIndex++)
    {
        switch (strBuffer[nIndex])
        {
            case '\r':
            case '\n':
                strBuffer[nIndex] = '\0';
                bRet = ParseConfig (strLine);
                strLine = strBuffer + nIndex +1;
                break;
            default:
                break;
        }
        if (!bRet)
        {
            break;
        }
    }

    if (bRet)
    {
        bRet = ParseConfig (strLine);
    }
    return bRet;
}

/*!
 * @fn          bool CConfigList::ParseConfig (const char *strLine)
 * @brief       parse config file line
 * @param[in]   strLine
 *              config file line
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigList::ParseConfig (const char *strLine)
{
    bool bRet = true;

    // check for line comment starting with '#'
    if (strLine[0] != '#')
    {
        for (int nIndex = 0; nIndex < eIndexMax; nIndex++)
        {
            if (m_apOptions[nIndex])
            {
                CString strValue;
                if (m_apOptions[nIndex]->Parse (strLine, strValue))
                {
                    bRet = m_apOptions[nIndex]->SetConfig (strValue);
                }
            }
            if (!bRet)
            {
                break;
            }
        }
    }

    return bRet;
}

/*!
 * @fn          bool CConfigList::ReadConfig (const char *strFile)
 * @brief       read config file
 * @details     read to growing buffer an parse buffer
 * @param[in]   strFile
 *              config file name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigList::ReadConfig (const char *strFile)
{
    bool bRet = false;
    char *pBuffer = 0;
    size_t nBuffer = s_nBufSize;
    size_t nSize = 0;
    size_t nRead = 0;
    CFile cFile;

    if (cFile.Open (strFile, "r"))
    {
        pBuffer = new char[nBuffer];
        while (cFile.Read (&pBuffer[nSize], s_nBufSize, nRead))
        {
            char *pTemp = pBuffer;

            nBuffer += s_nBufSize;
            pBuffer = new char[nBuffer];
            memcpy (pBuffer, pTemp, nSize);
            delete [] pTemp;

            nSize += nRead;
        }
        nSize += nRead;

        bRet = ParseConfig (pBuffer, nSize);
    }

    if (pBuffer)
    {
        delete [] pBuffer;
    }
    return bRet;
}

/*!
 * @fn          bool CConfigList::WriteConfig (const char *strFile)
 * @brief       write config file
 * @param[in]   strFile
 *              config file name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigList::WriteConfig (const char *strFile)
{
    bool bRet = false;
    CFile cFile;

    if (cFile.Open (strFile, "w"))
    {
        for (int nIndex = 0; nIndex < eIndexMax; nIndex++)
        {
            if (m_apOptions[nIndex])
            {
                m_apOptions[nIndex]->WriteConfig (cFile);
            }
        }
        bRet = true;
    }
    return bRet;
}

/*!
 * @fn          void CConfigList::PrintHelp (void)
 * @brief       print help for all options
 */
void CConfigList::PrintHelp (void)
{
    for (int nIndex = 0; nIndex < eIndexMax; nIndex++)
    {
        if (m_apOptions[nIndex])
        {
            m_apOptions[nIndex]->PrintHelp ();
        }
    }
}

//_oOo_
