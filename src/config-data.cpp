/*!
 * @file        config-data.cpp
 * @brief       application class implementation
 * @details     config data class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        29.04.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 *
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdlib.h>
#ifdef _WIN32
#include <Shlobj.h>
#else // _WIN32
#include <unistd.h>
#include <pwd.h>
#endif // _WIN32

#include "config-data.h"
#include "txt.h"

/*!
 * @fn          CConfigData::CConfigData (void)
 * @brief       constructor
 * @details     initializes default values
 */
CConfigData::CConfigData (void)
: m_bHelp (false)
, m_bVersion (false)
, m_strConfig ()
, m_strModemType ()
, m_strProtocol (NStrDef::Protocol)
, m_strExtra ()
, m_strModemHost ()
, m_nPort (-1)
, m_eEthType (CProtocol::eAuto)
, m_strUser ()
, m_strPass ()
, m_strPath (NStrDef::Path)
, m_strPrefix ()
, m_eLogLevel (CLog::eError)
, m_bLog (false)
, m_strLogFile ()
#ifdef ENABLE_CAPTURE
, m_bCapture (false)
, m_strCaptureFile ()
#endif // ENABLE_CAPTURE
, m_strCollectHost (NStrDef::CollectHost)
, m_strCollectSock (NStrDef::CollectSock)
, m_bDaemon (false)
, m_nInterval (10)
, m_strPidFile (NStrDef::DaemonPid)
, m_bBackground (false)
, m_ulOutput (CConfigData::s_ulOutDat)
, m_eCommand (CModem::eInfo)
{
}

/*!
 * @fn          void CConfigData::Reset (void)
 * @brief       reset config data
 */
void CConfigData::Reset (void)
{
    m_bHelp  = false;
    m_bVersion = false;
    m_strConfig = "";
    m_strModemType = "";
    m_strProtocol = NStrDef::Protocol;
    m_strExtra = "";
    m_strModemHost = "";
    m_nPort = -1;
    m_eEthType = CProtocol::eAuto;
    m_strUser = "";
    m_strPass = "";
    m_strPath = NStrDef::Path;
    m_strPrefix = "";
    m_eLogLevel = CLog::eError;
    m_bLog = false;
    m_strLogFile = "";
#ifdef ENABLE_CAPTURE
    m_bCapture = false;
    m_strCaptureFile = "";
#endif // ENABLE_CAPTURE
    m_strCollectHost = NStrDef::CollectHost;
    m_strCollectSock = NStrDef::CollectSock;
    m_bDaemon = false;
    m_nInterval = 10;
    m_strPidFile = NStrDef::DaemonPid;
    m_bBackground = false;
    m_ulOutput = CConfigData::s_ulOutDat;
    m_eCommand = CModem::eInfo;
}

/*!
 * @fn          void CConfigData::SetConfig (void)
 * @brief       set config file
 * @details     call after ReadArgs and before ReadConfig
 */
void CConfigData::SetConfig (void)
{
    if (0 == m_strConfig.Length ())
    {
        if (m_bDaemon)
        {
            if (CFile::Exist (NStrDef::ConfigSys))
            {
                m_strConfig = NStrDef::ConfigSys;
            }
        }
        else
        {
            CString strConfig;
#ifdef _WIN32
            LPWSTR strHome = NULL;
            if (SUCCEEDED (SHGetKnownFolderPath (FOLDERID_LocalAppData,
                    KF_FLAG_NOT_PARENT_RELATIVE | KF_FLAG_DEFAULT_PATH,
                    NULL, &strHome)))
            {
#else // _WIN32
            const char* strHome = getenv ("$HOME");

            if (!strHome)
            {
                strHome = getpwuid (getuid ())->pw_dir;
            }

            if (strHome)
            {
#endif // _WIN32
                strConfig.Format (NStrDef::ConfigUser, strHome);

                if (CFile::Exist (strConfig))
                {
                    m_strConfig = strConfig;
                }
#ifdef _WIN32
                CoTaskMemFree (strHome);
#endif // _WIN32

            }
        }
    }
}

/*!
 * @fn          void CConfigData::SetDefaults (void)
 * @brief       set default values
 * @details     call after ReadArgs and ReadConfig
 */
void CConfigData::SetDefaults (void)
{
    if (0 == m_strPrefix.Length ())
    {
        if (m_bDaemon)
        {
            m_strPrefix.Format (NStrDef::FilePrefixDaemon, ModemHost());
        }
        else
        {
            m_strPrefix.Format (NStrDef::FilePrefix, ModemHost());
        }
    }

    if (0 == m_strLogFile.Length ())
    {
        // default log path
        m_strLogFile.Format (NStrDef::LogFile,
                NStrDef::LogPath, Prefix());
    }

#ifdef ENABLE_CAPTURE
    if (0 == m_strCaptureFile.Length ())
    {
        // default log path
        CString strTimestamp;
        strTimestamp.Timestamp();

        m_strCaptureFile.Format (NStrDef::CaptureFile, Path(), Prefix(),
                (const char*) strTimestamp, Command());
    }
#endif // ENABLE_CAPTURE
}

/*!
 * @fn          const char *CConfigData::ModemHost (void) const
 * @brief       get modem host
 * @return      modem host
 */
const char *CConfigData::ModemHost (void) const
{
    return m_strModemHost;
}


/*!
 * @fn          const char *CConfigData::Path (void) const
 * @brief       get path
 * @return      path
 */
const char *CConfigData::Path (void) const
{
    return m_strPath;
}

/*!
 * @fn          const char *CConfigData::Prefix (void) const
 * @brief       get prefix
 * @return      prefix
 */
const char *CConfigData::Prefix (void) const
{
    return m_strPrefix;
}

/*!
 * @fn          const char *CConfigData::CollectHost (void) const
 * @brief       get collect host
 * @return      collect host
 */
const char *CConfigData::CollectHost (void) const
{
    return m_strCollectHost;
}

/*!
 * @fn          const char *CConfigData::CollectSock (void) const
 * @brief       get collect socket
 * @return      collect socket
 */
const char *CConfigData::CollectSock (void) const
{
    return m_strCollectSock;
}

/*!
 * @fn          int CConfigData::Interval (void) const
 * @brief       get interval
 * @return      interval
 */
int CConfigData::Interval (void) const
{
    return m_nInterval;
}

/*!
 * @fn          const char *CConfigData::Command(void) const
 * @brief       get command
 * @return      command
 */
const char *CConfigData::Command(void) const
{
    return CModem::GetCommand(m_eCommand);
}

//_oOo_
