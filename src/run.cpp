/*!
 * @file        run.cpp
 * @brief       application class implementation
 * @details     config and run execution classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 *
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "run.h"
#include "txt.h"

/*!
 * @var         CRun *CRun::g_pRun
 * @brief       global execution pointer
 * @details     required for signal handler
 */
CRun *CRun::g_pRun = NULL;

/*!
 * @fn          CRun::CRun (void)
 * @brief       constructor
 * @details     initializes global execution pointer
 */
CRun::CRun (void)
: m_cConfig ()
, m_cConfigList ()
, m_cData ()
, m_pAuth (NULL)
, m_pProtocol (NULL)
, m_pModem (NULL)
{
    for (int nIndex = 0 ; nIndex < COutputLib::eIndexMax; nIndex++)
    {
        m_apOutput[nIndex] = NULL;
    }
    g_pRun = this;

#ifdef WITH_LOCALE
    CString::InitLocale ();
#endif // WITH_LOCALE
}

/*!
 * @fn          CRun::~CRun (void)
 * @brief       destructor
 * @details     releases resources
 */
CRun::~CRun (void)
{
    Finish ();

    for (int nIndex = 0 ; nIndex < COutputLib::eIndexMax; nIndex++)
    {
        if (m_apOutput[nIndex])
        {
            delete m_apOutput[nIndex];
            m_apOutput[nIndex] = NULL;
        }
    }

#ifdef WITH_LOCALE
    CString::FreeLocale ();
#endif // WITH_LOCALE

    g_pRun = NULL;
}

/*!
 * @fn          bool CRun::Disable (int nEntry)
 * @brief       disable config list entry
 * @param[in]   nEntry
 *              list entry index
 * @return      succes
 * @retval      true
 *              o.k.
 * @retval      false
 *              disable failed (wrong index?)
 */
bool CRun::Disable (int nEntry)
{
    return m_cConfigList.Disable (nEntry);
}

/*!
 * @fn          bool CRun::ReadConfigData (int nArgc, char **astrArgv)
 * @brief       read config file and arguments
 * @param       nArgc
 *              number of arguments
 * @param       astrArgv
 *              argument array
 * @return      succes
 * @retval      true
 *              o.k.
 * @retval      false
 *              read or test failed
 */
bool CRun::ReadConfigData (int nArgc, char **astrArgv)
{
    bool bRet = false;

    // read command line arguments
    if (m_cConfigList.ReadArgs (nArgc, astrArgv))
    {
        // pass arguments to config
        if (m_cConfigList.WriteData (m_cConfig))
        {
            bRet = ReadConfigData ();
        }
    }
    else
    {
        m_cConfig.m_bHelp = true;;
    }

    return bRet;
}

/*!
 * @fn          bool CRun::ReadConfigData (void)
 * @brief       read config file
 * @return      succes
 * @retval      true
 *              o.k.
 * @retval      false
 *              read or test failed
 */
bool CRun::ReadConfigData (void)
{
    bool bRet = false;

    do
    {
        // read config file
        m_cConfig.SetConfig ();
        if (m_cConfig.m_strConfig)
        {
            if (!m_cConfigList.ReadConfig (m_cConfig.m_strConfig))
            {
                break;
            }
        }

        if (!m_cConfigList.WriteData (m_cConfig))
        {
            break;
        }

        if (m_cConfig.m_bHelp || m_cConfig.m_bVersion)
        {
            break;
        }

        // set outputs
        InitOutput (COutputLib::eDat,
                0 != (m_cConfig.m_ulOutput & CConfigData::s_ulOutDat),
                false);
        InitOutput (COutputLib::eText,
                0 != (m_cConfig.m_ulOutput & (CConfigData::s_ulOutStd | CConfigData::s_ulOutTones)),
                0 != (m_cConfig.m_ulOutput & CConfigData::s_ulOutTones));
        InitOutput (COutputLib::ePng,
                0 != (m_cConfig.m_ulOutput & CConfigData::s_ulOutGraph),
                true);
        InitOutput (COutputLib::eRrd,
                0 != (m_cConfig.m_ulOutput & CConfigData::s_ulOutCollect),
                false);

        bRet = true;
    }
    while (false);

    return bRet;
}

/*!
 * @fn          bool CRun::CheckConfigData (bool bSilent)
 * @brief       check config data
 * @details     open log file
 * @param[in]   bSilent
 *              don't log message
 * @return      succes
 * @retval      true
 *              o.k.
 * @retval      false
 *              read or test failed
 */
bool CRun::CheckConfigData (bool bSilent)
{
    bool bRet = false;

    do
    {
        if (!m_cConfigList.TestRequired (bSilent))
        {
            m_cConfig.m_bHelp = true;;
            break;
        }

        if (!m_cConfigList.WriteData (m_cConfig))
        {
            break;
        }

        m_cConfig.SetDefaults ();

        CLog::g_cLog.Level (m_cConfig.m_eLogLevel);
        if (m_cConfig.m_bLog)
        {
            if (m_cConfig.m_strLogFile.Compare ("+"))
            {
                // standard output path
                CString strLogFile;
                strLogFile.Format (NStrDef::LogFile,
                        (const char*)m_cConfig.m_strPath,
                        (const char*)m_cConfig.m_strPrefix);
                CLog::g_cLog.Open (strLogFile);
            }
            else
            {
                CLog::g_cLog.Open (m_cConfig.m_strLogFile);
            }
        }

        bRet = true;
    }
    while (false);

    return bRet;
}

/*!
 * @fn          bool CRun::WriteConfigData (void)
 * @brief       write config data to file
 * @return      succes
 * @retval      true
 *              o.k.
 * @retval      false
 *              write failed
 */
bool CRun::WriteConfigData (void)
{
    bool bRet = false;

    if (m_cConfigList.ReadData (m_cConfig))
    {
        if (m_cConfigList.WriteConfig (m_cConfig.m_strConfig))
        {
            bRet = true;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CRun::PrintInfo (const char *strArg0)
 * @brief       print version info and or help text
 * @param[in]   strArg0
 *              programm name (argv[0])
 * @return      version info requested
 * @retval      true
 *              version info
 * @retval      false
 *              no version info
 */
bool CRun::PrintInfo (const char *strArg0)
{
    bool bRet = false;
    const char *strProg = strrchr (strArg0, '/');
    if (strProg)
    {
        strProg++;
    }
    else
    {
        strProg = strArg0;
    }
    if (m_cConfig.m_bVersion || m_cConfig.m_bHelp)
    {
        fprintf (stdout, NStrHelp::Version, strProg, VERSION);
        bRet = true;
    }

    if (m_cConfig.m_bHelp)
    {
        fprintf (stdout, NStrHelp::Help, strProg);
        m_cConfigList.PrintHelp ();
    }

    return bRet;
}

/*!
 * @fn          void CRun::InitOutput (COutputLib::EIndex eIndex,
 *                      bool bLoad, bool bTones)
 * @brief       initialize output as external library
 * @param[in]   eIndex
 *              index
 * @param[in]   bLoad
 *              library to be loaded
 * @param[in]   bTones
 *              write tones
 */
void CRun::InitOutput (COutputLib::EIndex eIndex, bool bLoad, bool bTones)
{
    if (m_apOutput[eIndex])
    {
        delete m_apOutput[eIndex];
        m_apOutput[eIndex] = NULL;
    }

    if (bLoad)
    {
        m_apOutput[eIndex] = new COutputLib (eIndex,
                COutputLib::s_astrName[eIndex], bTones);
    }
}

/*!
 * @fn          void CRun::InitOutput (COutputLib::EIndex eIndex,
 *                      COutput *pOutput, bool bTones)
 * @brief       initialize output as internal object
 * @param[in]   eIndex
 *              index
 * @param[in]   pOutput
 *              output object
 * @param[in]   bTones
 *              write tones
 */
void CRun::InitOutput (COutputLib::EIndex eIndex, COutput *pOutput, bool bTones)
{
    if (m_apOutput[eIndex])
    {
        delete m_apOutput[eIndex];
        m_apOutput[eIndex] = NULL;
    }

    m_apOutput[eIndex] = new COutputLib (eIndex, pOutput, bTones);
}

/*!
 * @fn          bool CRun::LoadOutput (void)
 * @brief       load output libraries
 * @return      succes
 * @retval      true
 *              o.k.
 * @retval      false
 *              load or version failure
 */
bool CRun::LoadOutput (void)
{
    bool bRet = true;

    for (int nIndex = 0; nIndex < COutputLib::eIndexMax; nIndex++)
    {
        if (m_apOutput[nIndex])
        {
            if (!m_apOutput[nIndex]->Load ())
            {
                bRet = false;
            }
        }
    }

    return bRet;
}

/*!
 * @fn          bool CRun::LoadProtocol (void)
 * @brief       load protocol library
 * @return      succes
 * @retval      true
 *              o.k.
 * @retval      false
 *              load or version failure
 */
bool CRun::LoadProtocol (void)
{
    bool bRet = false;

    do
    {
        if (!CProtocolFactory::g_cProtocolFactory.Load (m_cConfig.m_strProtocol))
        {
            LogError (NStrErr::ArgWrongProtocolType, (const char*)m_cConfig.m_strProtocol);
            m_cConfig.m_bHelp = true;
            break;
        }

        if (!CProtocolFactory::g_cProtocolFactory.TestVersion (m_cConfig.m_strProtocol))
        {
            break;
        }

        bRet = true;
    }
    while (false);

    return bRet;
}

/*!
 * @fn          bool CRun::LoadModem (void)
 * @brief       load modem library
 * @return      succes
 * @retval      true
 *              o.k.
 * @retval      false
 *              load or version failure
 */
bool CRun::LoadModem (void)
{
    bool bRet = false;

    do
    {
        if (!CModemFactory::g_cModemFactory.Load (m_cConfig.m_strModemType))
        {
            LogError (NStrErr::ArgWrongModemType, (const char*)m_cConfig.m_strModemType);
            m_cConfig.m_bHelp = true;
            break;
        }

        if (!CModemFactory::g_cModemFactory.TestVersion (m_cConfig.m_strModemType))
        {
            break;
        }

        bRet = true;
    }
    while (false);

    return bRet;
}

/*!
 * @fn          void CRun::_SigHandler (int nSignal)
 * @brief       signal handler
 * @details     static function, calls virtual signal handler
 * @param[in]   nSignal
 *              signal number
 */
void CRun::_SigHandler (int nSignal)
{
    if (g_pRun)
    {
        g_pRun->SigHandler (nSignal);
    }
}

/*!
 * @fn          bool CRun::Init (void)
 * @brief       initialization of protocol and modem
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              initialization failed
 */
bool CRun::Init (void)
{
    bool bRet = false;

    m_pAuth = new CAuthLogin (m_cConfig.m_strUser, m_cConfig.m_strPass);

    if (m_pAuth)
    {
        m_pProtocol = CProtocolFactory::g_cProtocolFactory.Create (
                m_cConfig.m_strModemHost, m_cConfig.m_nPort,
                m_cConfig.m_eEthType, m_pAuth,
                m_cConfig.m_strExtra);

        if (m_pProtocol)
        {
#ifdef ENABLE_CAPTURE
            if (m_cConfig.m_bCapture)
            {
                m_pProtocol->Capture (m_cConfig.m_strCaptureFile);
            }
#endif // ENABLE_CAPTURE

            m_pModem = CModemFactory::g_cModemFactory.Create (m_cData, m_pProtocol);

            if (m_pModem)
            {
                bRet = true;
            }
            else
            {
                delete m_pProtocol;
                m_pProtocol = NULL;
                delete m_pAuth;
                m_pAuth = NULL;
            }
        }
        else
        {
            delete m_pAuth;
            m_pAuth = NULL;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CRun::Process (void)
 * @brief       process modem commands once
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              process failed
 */
bool CRun::Process (void)
{
    bool bRet = false;

    if (m_pModem && m_pProtocol)
    {
        if (m_pProtocol->Open ())
        {
            m_pModem->Init (m_cConfig.m_eCommand);

            if (m_pModem->Connect ())
            {
                bRet = m_pProtocol->Process ();
            }

            m_pProtocol->Close ();
        }

#if defined (ENABLE_TEST)
        // in Test mode do a second run
        if ( !m_cConfig.m_bDaemon && m_cConfig.m_strExtra)
        {
            if (m_pProtocol->Open ())
            {
                m_pModem->Init (m_cConfig.m_eCommand);

                if (m_pModem->Connect ())
                {
                    bRet = m_pProtocol->Process ();
                }

                m_pProtocol->Close ();
            }
        }
#endif // defined (ENABLE_TEST)
    }

    return bRet;
}

/*!
 * @fn          void CRun::Output (void)
 * @brief       output process results
 */
void CRun::Output (void)
{
    for (int nIndex = 0; nIndex < COutputLib::eIndexMax; nIndex++)
    {
        if (m_apOutput[nIndex])
        {
            m_apOutput[nIndex]->Write (m_cConfig, m_cData);
        }
    }
}

/*!
 * @fn          void CRun::Finish (void)
 * @brief       releases resources
 */
void CRun::Finish (void)
{
    if (m_pModem)
    {
        delete m_pModem;
        m_pModem = NULL;
    }

    if (m_pProtocol)
    {
        delete m_pProtocol;
        m_pProtocol = NULL;
    }

    if (m_pAuth)
    {
        delete m_pAuth;
        m_pAuth = NULL;
    }
}

//_oOo_
