/*!
 * @file        bandplan.cpp
 * @brief       DSL bandplan implementation
 * @details     frequency / tone ranges
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "bandplan.h"
#include "txt.h"

//! @cond
namespace NStrBand
{
    STRING Unknown              = "?";
    STRING ADSL                 = "ADSL";
    STRING ADSL2                = "ADSL2";
    STRING ADSL2plus            = "ADSL2+";
    STRING VDSL                 = "VDSL";
    STRING VDSL2                = "VDSL2";

    STRING VersionG992_1        = "ITU-T G.992.1";
    STRING VersionG992_3        = "ITU-T G.992.3";
    STRING VersionG992_5        = "ITU-T G.992.5";
    STRING VersionG993_1        = "ITU-T G.993.1";
    STRING VersionG993_2        = "ITU-T G.993.2";

    STRING AnnexA               = "Annex A";
    STRING AnnexB               = "Annex B";
    STRING AnnexC               = "Annex C";
    STRING AnnexJ               = "Annex J";
    STRING AnnexM               = "Annex M";

    STRING Profile8a            = "8a";
    STRING Profile8b            = "8b";
    STRING Profile8c            = "8c";
    STRING Profile8d            = "8d";
    STRING Profile12a           = "12a";
    STRING Profile12b           = "12b";
    STRING Profile17a           = "17a";
    STRING Profile30a           = "30a";
    STRING Profile35b           = "35b";

    STRING BandplanB7_1         = "997-M1c-A-7";
    STRING BandplanB7_2         = "997-M1x-M-8";
    STRING BandplanB7_3         = "997-M1x-M";
    STRING BandplanB7_4         = "997-M2x-M-8";
    STRING BandplanB7_5         = "997-M2x-A";
    STRING BandplanB7_6         = "997-M2x-M";
    STRING BandplanB7_7         = "HPE17-M1-NUS0";
    STRING BandplanB7_8         = "HPE30-M1-NUS0";
    STRING BandplanB7_9         = "997E17-M2x-A";
    STRING BandplanB7_10        = "997E30-M2x-NUS0";

    STRING BandplanB8_1         = "998-M1x-A";
    STRING BandplanB8_2         = "998-M1x-B";
    STRING BandplanB8_3         = "998-M1x-NUS0";
    STRING BandplanB8_4         = "998-M2x-A";
    STRING BandplanB8_5         = "998-M2x-M";
    STRING BandplanB8_6         = "998-M2x-B";
    STRING BandplanB8_7         = "998-M2x-NUS0";
    STRING BandplanB8_8         = "998E17-M2x-NUS0";
    STRING BandplanB8_9         = "998E17-M2x-NUS0-M";
    STRING BandplanB8_10        = "998ADE17-M2x-NUS0-M";
    STRING BandplanB8_11        = "998ADE17-M2x-A";
    STRING BandplanB8_12        = "998ADE17-M2x-B";
    STRING BandplanB8_13        = "998E30-M2x-NUS0";
    STRING BandplanB8_14        = "998E30-M2x-NUS0-M";
    STRING BandplanB8_15        = "998ADE30-M2x-NUS0-M";
    STRING BandplanB8_16        = "998ADE30-M2x-NUS0-A";
    STRING BandplanB8_17        = "998ADE17-M2x-M";
    STRING BandplanB8_18        = "998E17-M2x-A";
    STRING BandplanB8_19        = "998E35-M2x-A";
    STRING BandplanB8_20        = "998ADE35-M2x-A";
    STRING BandplanB8_21        = "998ADE35-M2x-B";
    STRING BandplanB8_22        = "998ADE35-M2x-M";
} // namespace NStrBand
//! @endcond

/*!
 * @def         BAND(x)
 * @brief       band definition
 * @details     band reference and number of bands
 * @param       x
 *              band reference (prefixed by g_acBand)
 */
#define BAND(x) CBand::g_acBand##x, ARRAY_SIZE(CBand::g_acBand##x)

/*!
 * @fn          CBand::CBand (int nMin, int nMid, int nMax)
 * @brief       constructor
 * @details     sets limits
 * @param[in]   nMin
 *              upstream lower limit
 * @param[in]   nMid
 *              upstream upper limit, downstream lower limit
 * @param[in]   nMax
 *              downstream upper limit
 */
CBand::CBand (int nMin, int nMid, int nMax)
: m_nMinUp (-1)
, m_nMaxUp (-1)
, m_nMinDown (-1)
, m_nMaxDown (-1)
{
    if (-1 != nMin)
    {
        m_nMinUp = nMin;
        m_nMaxUp = nMid;
    }
    if (-1 != nMax)
    {
        m_nMinDown = nMid;
        m_nMaxDown = nMax;
    }
}

/*!
 * @fn          CBandplan::CBandplan (const char *strType,
 *                      const char *strITU_TVersion,
 *                      const char *strITU_TAnnex,
 *                      const char *strITU_TProfile,
 *                      const char *strITU_TBandplan,
 *                      size_t nTones,
 *                      double fSpacing,
 *                      const CBand *pBand,
 *                      size_t nBands)
 * @brief       constructor
 * @details     initializes bandplan data
 * @param[in]   strType
 *              band type name
 * @param[in]   strITU_TVersion
 *              ITU version
 * @param[in]   strITU_TAnnex
 *              annex name
 * @param[in]   strITU_TProfile
 *              profile name
 * @param[in]   strITU_TBandplan
 *              bandplan name
 * @param[in]   nTones
 *              number of tones
 * @param[in]   fSpacing
 *              frequency spacing
 * @param[in]   pBand
 *              band pointer
 * @param[in]   nBands
 *              number of bands
 */
CBandplan::CBandplan (const char *strType,
        const char *strITU_TVersion,
        const char *strITU_TAnnex,
        const char *strITU_TProfile,
        const char *strITU_TBandplan,
        size_t nTones,
        double fSpacing,
        const CBand *pBand,
        size_t nBands)
: m_strType (strType)
, m_strITU_TVersion (strITU_TVersion)
, m_strITU_TAnnex (strITU_TAnnex)
, m_strITU_TProfile (strITU_TProfile)
, m_strITU_TBandplan (strITU_TBandplan)
, m_nTones (nTones)
, m_fSpacing (fSpacing)
, m_pBand (pBand)
, m_nBands (nBands)
{
}

/*!
 * @fn          CBandplan &CBandplan::operator = (const CBandplan &cBandplan)
 * @brief       assignment operator
 * @param[in]   cBandplan
 *              source reference
 * @return
 */
CBandplan &CBandplan::operator = (const CBandplan &cBandplan)
{
    m_strType = cBandplan.m_strType;
    m_strITU_TVersion = cBandplan.m_strITU_TVersion;
    m_strITU_TAnnex = cBandplan.m_strITU_TAnnex;
    m_strITU_TProfile = cBandplan.m_strITU_TProfile;
    m_strITU_TBandplan = cBandplan.m_strITU_TBandplan;
    m_nTones = cBandplan.m_nTones;
    m_fSpacing = cBandplan.m_fSpacing;
    m_pBand = cBandplan.m_pBand;
    m_nBands = cBandplan.m_nBands;

    return *this;
}

/*!
 * @var         CBand::g_acBandNone
 * @brief       empty band
 */
const CBand CBand::g_acBandNone[] = {
    CBand (0, 0, 0)};

/*!
 * @var         CBandplan::g_cBandplanNone
 * @brief       empty bandplan
 */
CBandplan CBandplan::g_cBandplanNone (
        NStrBand::Unknown, "", "", "", "",
        256,
        4.3125,
        BAND (None));

//! @cond
// ADSL
const CBand CBand::g_acBandADSL_A[] = {
    CBand (6, 32, 256)};
CBandplan CBandplan::g_cBandplanADSL_A  (
    NStrBand::ADSL,
    NStrBand::VersionG992_1,
    NStrBand::AnnexA, "", "",
    256, 4.3125, BAND (ADSL_A));

const CBand CBand::g_acBandADSL_B[] = {
    CBand (28, 64, 256)};
CBandplan CBandplan::g_cBandplanADSL_B (
    NStrBand::ADSL,
    NStrBand::VersionG992_1,
    NStrBand::AnnexB, "", "",
    256, 4.3125, BAND (ADSL_B));

// ADSL2
const CBand CBand::g_acBandADSL2_A[] = {
    CBand (6, 32, 256)};
CBandplan CBandplan::g_cBandplanADSL2_A (
    NStrBand::ADSL2,
    NStrBand::VersionG992_3,
    NStrBand::AnnexA, "", "",
    256, 4.3125, BAND (ADSL2_A));

const CBand CBand::g_acBandADSL2_B[] = {
    CBand (28, 64, 256)};
CBandplan CBandplan::g_cBandplanADSL2_B (
    NStrBand::ADSL2,
    NStrBand::VersionG992_3,
    NStrBand::AnnexB, "", "",
    256, 4.3125, BAND (ADSL2_B));

const CBand CBand::g_acBandADSL2_J[] = {
    CBand (0, 64, 256)};
CBandplan CBandplan::g_cBandplanADSL2_J (
    NStrBand::ADSL2,
    NStrBand::VersionG992_3,
    NStrBand::AnnexJ, "", "",
    256, 4.3125, BAND (ADSL2_J));

const CBand CBand::g_acBandADSL2_M[] = {
    CBand (6, 64, 256)};
CBandplan CBandplan::g_cBandplanADSL2_M (
    NStrBand::ADSL2,
    NStrBand::VersionG992_3,
    NStrBand::AnnexM, "", "",
    256, 4.3125, BAND (ADSL2_M));

// ADSL2+
const CBand CBand::g_acBandADSL2p_A[] = {
    CBand (6, 32, 256)};
CBandplan CBandplan::g_cBandplanADSL2p_A (
    NStrBand::ADSL2plus,
    NStrBand::VersionG992_5,
    NStrBand::AnnexA, "", "",
    512, 4.3125, BAND (ADSL2p_A));

const CBand CBand::g_acBandADSL2p_B[] = {
    CBand (28, 64, 512)};
CBandplan CBandplan::g_cBandplanADSL2p_B (
    NStrBand::ADSL2plus,
    NStrBand::VersionG992_5,
    NStrBand::AnnexB, "", "",
    512, 4.3125, BAND (ADSL2p_B));

const CBand CBand::g_acBandADSL2p_J[] = {
    CBand (0, 64, 512)};
CBandplan CBandplan::g_cBandplanADSL2p_J (
    NStrBand::ADSL2plus,
    NStrBand::VersionG992_5,
    NStrBand::AnnexJ, "", "",
    512, 4.3125, BAND (ADSL2p_J));

const CBand CBand::g_acBandADSL2p_M[] = {
    CBand (6, 64, 512)};
CBandplan CBandplan::g_cBandplanADSL2p_M (
    NStrBand::ADSL2plus,
    NStrBand::VersionG992_5,
    NStrBand::AnnexM, "", "",
    512, 4.3125, BAND (ADSL2p_M));

// VDSL2 Bandplan 997
const CBand CBand::g_acBandVDSL2_B7_1[] = {
    CBand (6, 32, 696),
    CBand (696, 1183, 1635)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_1_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB7_1,
    2048, 4.3125, BAND (VDSL2_B7_1));
CBandplan CBandplan::g_cBandplanVDSL2_B7_1_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB7_1,
    2048, 4.3125, BAND (VDSL2_B7_1));
CBandplan CBandplan::g_cBandplanVDSL2_B7_1_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB7_1,
    2048, 4.3125, BAND (VDSL2_B7_1));
CBandplan CBandplan::g_cBandplanVDSL2_B7_1_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB7_1,
    2048, 4.3125, BAND (VDSL2_B7_1));

const CBand CBand::g_acBandVDSL2_B7_2_8[] = {
    CBand (6, 64, 696),
    CBand (696, 1183, 1635)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_2_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB7_2,
    2048, 4.3125, BAND (VDSL2_B7_2_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_2_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB7_2,
    2048, 4.3125, BAND (VDSL2_B7_2_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_2_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB7_2,
    2048, 4.3125, BAND (VDSL2_B7_2_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_2_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB7_2,
    2048, 4.3125, BAND (VDSL2_B7_2_8));

const CBand CBand::g_acBandVDSL2_B7_2_12[] = {
    CBand (6, 64, 696),
    CBand (696, 1183, 1635),
    CBand (1635, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_2_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB7_2,
    4096, 4.3125, BAND (VDSL2_B7_2_12));
CBandplan CBandplan::g_cBandplanVDSL2_B7_2_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB7_2,
    4096, 4.3125, BAND (VDSL2_B7_2_12));

const CBand CBand::g_acBandVDSL2_B7_3_8[] = {
    CBand (6, 64, 696),
    CBand (696, 1183, 1635)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_3_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB7_3,
    2048, 4.3125, BAND (VDSL2_B7_3_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_3_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB7_3,
    2048, 4.3125, BAND (VDSL2_B7_3_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_3_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB7_3,
    2048, 4.3125, BAND (VDSL2_B7_3_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_3_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB7_3,
    2048, 4.3125, BAND (VDSL2_B7_3_8));

const CBand CBand::g_acBandVDSL2_B7_3_12[] = {
    CBand (6, 64, 696),
    CBand (696, 1183, 1635),
    CBand (1635, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_3_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB7_3,
    4096, 4.3125, BAND (VDSL2_B7_3_12));
CBandplan CBandplan::g_cBandplanVDSL2_B7_3_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB7_3,
    4096, 4.3125, BAND (VDSL2_B7_3_12));

const CBand CBand::g_acBandVDSL2_B7_4_8[] = {
    CBand (6, 64, 696),
    CBand (696, 1183, 1635)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_4_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB7_4,
    2048, 4.3125, BAND (VDSL2_B7_4_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_4_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB7_4,
    2048, 4.3125, BAND (VDSL2_B7_4_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_4_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB7_4,
    2048, 4.3125, BAND (VDSL2_B7_4_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_4_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB7_4,
    2048, 4.3125, BAND (VDSL2_B7_4_8));

const CBand CBand::g_acBandVDSL2_B7_4_12[] = {
    CBand (6, 64, 696),
    CBand (696, 1183, 1635),
    CBand (1635, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_4_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB7_4,
    4096, 4.3125, BAND (VDSL2_B7_4_12));
CBandplan CBandplan::g_cBandplanVDSL2_B7_4_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB7_4,
    4096, 4.3125, BAND (VDSL2_B7_4_12));

const CBand CBand::g_acBandVDSL2_B7_5_8[] = {
    CBand (6, 32, 696),
    CBand (696, 1183, 1635)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_5_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB7_5,
    2048, 4.3125, BAND (VDSL2_B7_5_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_5_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB7_5,
    2048, 4.3125, BAND (VDSL2_B7_5_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_5_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB7_5,
    2048, 4.3125, BAND (VDSL2_B7_5_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_5_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB7_5,
    2048, 4.3125, BAND (VDSL2_B7_5_8));

const CBand CBand::g_acBandVDSL2_B7_5_12[] = {
    CBand (6, 32, 696),
    CBand (696, 1183, 1635),
    CBand (1635, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_5_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB7_5,
    4096, 4.3125, BAND (VDSL2_B7_5_12));
CBandplan CBandplan::g_cBandplanVDSL2_B7_5_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB7_5,
    4096, 4.3125, BAND (VDSL2_B7_5_12));

const CBand CBand::g_acBandVDSL2_B7_6_8[] = {
    CBand (6, 64, 696),
    CBand (696, 1183, 1635)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_6_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB7_6,
    2048, 4.3125, BAND (VDSL2_B7_6_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_6_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB7_6,
    2048, 4.3125, BAND (VDSL2_B7_6_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_6_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB7_6,
    2048, 4.3125, BAND (VDSL2_B7_6_8));
CBandplan CBandplan::g_cBandplanVDSL2_B7_6_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB7_6,
    2048, 4.3125, BAND (VDSL2_B7_6_8));

const CBand CBand::g_acBandVDSL2_B7_6_12[] = {
    CBand (6, 64, 696),
    CBand (696, 1183, 1635),
    CBand (1635, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_6_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB7_6,
    4096, 4.3125, BAND (VDSL2_B7_6_12));
CBandplan CBandplan::g_cBandplanVDSL2_B7_6_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB7_6,
    4096, 4.3125, BAND (VDSL2_B7_6_12));

const CBand CBand::g_acBandVDSL2_B7_7[] = {
    CBand (-1, 1635, 2348),
    CBand (2348, 3246, 4096)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_7_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB7_7,
    4096, 4.3125, BAND (VDSL2_B7_7));

const CBand CBand::g_acBandVDSL2_B7_8[] = {
    CBand (-1, 817, 1174),
    CBand (1174, 1623, 2487),
    CBand (2487, 2886, 3479)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_8_30a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile30a,
    NStrBand::BandplanB7_8,
    4096, 8.625, BAND (VDSL2_B7_8));

const CBand CBand::g_acBandVDSL2_B7_9[] = {
    CBand (6, 32, 696),
    CBand (696, 1183, 1635),
    CBand (1635, 2783, 3246),
    CBand (3246, 4096, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_9_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB7_9,
    4096, 4.3125, BAND (VDSL2_B7_9));

const CBand CBand::g_acBandVDSL2_B7_10[] = {
    CBand (-1, 16, 348),
    CBand (348, 591, 817),
    CBand (817, 1391, 1623),
    CBand (1623, 2261, 3130),
    CBand (3130, 3479, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B7_10_30a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile30a,
    NStrBand::BandplanB7_10,
    4096, 8.625, BAND (VDSL2_B7_10));

// VDSL2 Banplan 998
const CBand CBand::g_acBandVDSL2_B8_1_8[] = {
    CBand (6, 32, 870),
    CBand (870, 1206, 1971)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_1_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB8_1,
    2048, 4.3125, BAND (VDSL2_B8_1_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_1_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB8_1,
    2048, 4.3125, BAND (VDSL2_B8_1_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_1_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB8_1,
    2048, 4.3125, BAND (VDSL2_B8_1_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_1_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB8_1,
    2048, 4.3125, BAND (VDSL2_B8_1_8));

const CBand CBand::g_acBandVDSL2_B8_1_12[] = {
    CBand (6, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_1_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB8_1,
    4096, 4.3125, BAND (VDSL2_B8_1_12));
CBandplan CBandplan::g_cBandplanVDSL2_B8_1_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB8_1,
    4096, 4.3125, BAND (VDSL2_B8_1_12));

const CBand CBand::g_acBandVDSL2_B8_2_8[] = {
    CBand (28, 64, 870),
    CBand (870, 1206, 1971)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_2_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB8_2,
    2048, 4.3125, BAND (VDSL2_B8_2_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_2_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB8_2,
    2048, 4.3125, BAND (VDSL2_B8_2_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_2_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB8_2,
    2048, 4.3125, BAND (VDSL2_B8_2_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_2_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB8_2,
    2048, 4.3125, BAND (VDSL2_B8_2_8));

const CBand CBand::g_acBandVDSL2_B8_2_12[] = {
    CBand (28, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_2_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB8_2,
    4096, 4.3125, BAND (VDSL2_B8_2_12));
CBandplan CBandplan::g_cBandplanVDSL2_B8_2_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB8_2,
    4096, 4.3125, BAND (VDSL2_B8_2_12));

const CBand CBand::g_acBandVDSL2_B8_3_8[] = {
    CBand (-1, 32, 870),
    CBand (870, 1206, 1971)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_3_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB8_3,
    2048, 4.3125, BAND (VDSL2_B8_3_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_3_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB8_3,
    2048, 4.3125, BAND (VDSL2_B8_3_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_3_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB8_3,
    2048, 4.3125, BAND (VDSL2_B8_3_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_3_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB8_3,
    2048, 4.3125, BAND (VDSL2_B8_3_8));

const CBand CBand::g_acBandVDSL2_B8_3_12[] = {
    CBand (-1, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_3_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB8_3,
    4096, 4.3125, BAND (VDSL2_B8_3_12));
CBandplan CBandplan::g_cBandplanVDSL2_B8_3_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB8_3,
    4096, 4.3125, BAND (VDSL2_B8_3_12));

const CBand CBand::g_acBandVDSL2_B8_4_8[] = {
    CBand (6, 32, 870),
    CBand (870, 1206, 1971)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_4_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB8_4,
    2048, 4.3125, BAND (VDSL2_B8_4_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_4_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB8_4,
    2048, 4.3125, BAND (VDSL2_B8_4_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_4_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB8_4,
    2048, 4.3125, BAND (VDSL2_B8_4_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_4_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB8_4,
    2048, 4.3125, BAND (VDSL2_B8_4_8));

const CBand CBand::g_acBandVDSL2_B8_4_12[] = {
    CBand (6, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_4_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB8_4,
    4096, 4.3125, BAND (VDSL2_B8_4_12));
CBandplan CBandplan::g_cBandplanVDSL2_B8_4_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB8_4,
    4096, 4.3125, BAND (VDSL2_B8_4_12));

const CBand CBand::g_acBandVDSL2_B8_5_8[] = {
    CBand (6, 64, 870),
    CBand (870, 1206, 1971)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_5_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB8_5,
    2048, 4.3125, BAND (VDSL2_B8_5_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_5_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB8_5,
    2048, 4.3125, BAND (VDSL2_B8_5_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_5_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB8_5,
    2048, 4.3125, BAND (VDSL2_B8_5_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_5_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB8_5,
    2048, 4.3125, BAND (VDSL2_B8_5_8));

const CBand CBand::g_acBandVDSL2_B8_5_12[] = {
    CBand (6, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_5_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB8_5,
    4096, 4.3125, BAND (VDSL2_B8_5_12));
CBandplan CBandplan::g_cBandplanVDSL2_B8_5_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB8_5,
    4096, 4.3125, BAND (VDSL2_B8_5_12));

const CBand CBand::g_acBandVDSL2_B8_6_8[] = {
    CBand (28, 64, 870),
    CBand (870, 1206, 1971)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_6_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB8_6,
    2048, 4.3125, BAND (VDSL2_B8_6_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_6_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB8_6,
    2048, 4.3125, BAND (VDSL2_B8_6_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_6_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB8_6,
    2048, 4.3125, BAND (VDSL2_B8_6_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_6_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB8_6,
    2048, 4.3125, BAND (VDSL2_B8_6_8));

const CBand CBand::g_acBandVDSL2_B8_6_12[] = {
    CBand (28, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_6_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB8_6,
    4096, 4.3125, BAND (VDSL2_B8_6_12));
CBandplan CBandplan::g_cBandplanVDSL2_B8_6_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB8_6,
    4096, 4.3125, BAND (VDSL2_B8_6_12));

const CBand CBand::g_acBandVDSL2_B8_7_8[] = {
    CBand (-1, 32, 870),
    CBand (870, 1206, 1971)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_7_8a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8a,
    NStrBand::BandplanB8_7,
    2048, 4.3125, BAND (VDSL2_B8_7_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_7_8b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8b,
    NStrBand::BandplanB8_7,
    2048, 4.3125, BAND (VDSL2_B8_7_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_7_8c (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8c,
    NStrBand::BandplanB8_7,
    2048, 4.3125, BAND (VDSL2_B8_7_8));
CBandplan CBandplan::g_cBandplanVDSL2_B8_7_8d (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile8d,
    NStrBand::BandplanB8_7,
    2048, 4.3125, BAND (VDSL2_B8_7_8));

const CBand CBand::g_acBandVDSL2_B8_7_12[] = {
    CBand (-1, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_7_12a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12a,
    NStrBand::BandplanB8_7,
    4096, 4.3125, BAND (VDSL2_B8_7_12));
CBandplan CBandplan::g_cBandplanVDSL2_B8_7_12b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile12b,
    NStrBand::BandplanB8_7,
    4096, 4.3125, BAND (VDSL2_B8_7_12));

const CBand CBand::g_acBandVDSL2_B8_8[] = {
    CBand (-1, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 3246),
    CBand (3246, 4096, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_8_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB8_8,
    4096, 4.3125, BAND (VDSL2_B8_8));

const CBand CBand::g_acBandVDSL2_B8_9[] = {
    CBand (-1, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 3246),
    CBand (3246, 4096, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_9_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB8_9,
    4096, 4.3125, BAND (VDSL2_B8_9));

const CBand CBand::g_acBandVDSL2_B8_10[] = {
    CBand (-1, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 4096)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_10_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB8_10,
    4096, 4.3125, BAND (VDSL2_B8_10));

const CBand CBand::g_acBandVDSL2_B8_11[] = {
    CBand (6, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 4096)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_11_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB8_11,
    4096, 4.3125, BAND (VDSL2_B8_11));

const CBand CBand::g_acBandVDSL2_B8_12[] = {
    CBand (28, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 4096)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_12_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB8_12,
    4096, 4.3125, BAND (VDSL2_B8_12));

const CBand CBand::g_acBandVDSL2_B8_13[] = {
    CBand (-1, 16, 435),
    CBand (435, 603, 986),
    CBand (986, 1391, 1623),
    CBand (1623, 2487, 2886),
    CBand (2886, 3479, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_13_30a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile30a,
    NStrBand::BandplanB8_13,
    4096, 8.625, BAND (VDSL2_B8_13));

const CBand CBand::g_acBandVDSL2_B8_14[] = {
    CBand (-1, 32, 435),
    CBand (435, 603, 986),
    CBand (986, 1391, 1623),
    CBand (1623, 2487, 2886),
    CBand (2886, 3479, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_14_30a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile30a,
    NStrBand::BandplanB8_14,
    4096, 8.625, BAND (VDSL2_B8_14));

const CBand CBand::g_acBandVDSL2_B8_15[] = {
    CBand (-1, 32, 435),
    CBand (435, 603, 986),
    CBand (986, 1391, 2886),
    CBand (2886, 3479, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_15_30a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile30a,
    NStrBand::BandplanB8_15,
    4096, 8.625, BAND (VDSL2_B8_15));

const CBand CBand::g_acBandVDSL2_B8_16[] = {
    CBand (-1, 16, 435),
    CBand (435, 603, 986),
    CBand (986, 1391, 2886),
    CBand (2886, 3479, -1)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_16_30a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile30a,
    NStrBand::BandplanB8_16,
    4096, 8.625, BAND (VDSL2_B8_16));

const CBand CBand::g_acBandVDSL2_B8_17[] = {
    CBand (6, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 4096)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_17_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB8_17,
    4096, 4.3125, BAND (VDSL2_B8_17));

const CBand CBand::g_acBandVDSL2_B8_18[] = {
    CBand (6, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 4096)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_18_17a (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile17a,
    NStrBand::BandplanB8_18,
    4096, 4.3125, BAND (VDSL2_B8_18));

const CBand CBand::g_acBandVDSL2_B8_19[] = {
    CBand (6, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 8192)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_19_35b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile35b,
    NStrBand::BandplanB8_19,
    8192, 4.3125, BAND (VDSL2_B8_19));

const CBand CBand::g_acBandVDSL2_B8_20[] = {
    CBand (6, 32, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 8192)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_20_35b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile35b,
    NStrBand::BandplanB8_20,
    8192, 4.3125, BAND (VDSL2_B8_20));

const CBand CBand::g_acBandVDSL2_B8_21[] = {
    CBand (32, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 8192)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_21_35b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile35b,
    NStrBand::BandplanB8_21,
    8192, 4.3125, BAND (VDSL2_B8_21));

const CBand CBand::g_acBandVDSL2_B8_22[] = {
    CBand (6, 64, 870),
    CBand (870, 1206, 1971),
    CBand (1971, 2783, 8192)};
CBandplan CBandplan::g_cBandplanVDSL2_B8_22_35b (
    NStrBand::VDSL2,
    NStrBand::VersionG993_2,
    NStrBand::AnnexB,
    NStrBand::Profile35b,
    NStrBand::BandplanB8_22,
    8192, 4.3125, BAND (VDSL2_B8_22));
//! @endcond

/*!
 * @def         PLAN(x)
 * @brief       bandplan definition
 * @details     bandplan enum and global struct
 * @param       x
 *              band reference (prefixed by e and g_cBandplan)
 */
#define PLAN(x) CBandplanData(e##x, g_cBandplan##x)

const CBandplan::CBandplanData CBandplan::CBandplanData::g_acList[] = {
        PLAN (ADSL_A), PLAN (ADSL_B),
        PLAN (ADSL2_A), PLAN (ADSL2_B), PLAN (ADSL2_J), PLAN (ADSL2_M),
        PLAN (ADSL2p_A), PLAN (ADSL2p_B), PLAN (ADSL2p_J), PLAN (ADSL2p_M),
        PLAN (VDSL2_B7_1_8a), PLAN (VDSL2_B7_1_8b), PLAN (VDSL2_B7_1_8c), PLAN (VDSL2_B7_1_8d),
        PLAN (VDSL2_B7_2_8a), PLAN (VDSL2_B7_2_8b), PLAN (VDSL2_B7_2_8c), PLAN (VDSL2_B7_2_8d),
        PLAN (VDSL2_B7_2_12a), PLAN (VDSL2_B7_2_12b),
        PLAN (VDSL2_B7_3_8a), PLAN (VDSL2_B7_3_8b), PLAN (VDSL2_B7_3_8c), PLAN (VDSL2_B7_3_8d),
        PLAN (VDSL2_B7_3_12a), PLAN (VDSL2_B7_3_12b),
        PLAN (VDSL2_B7_4_8a), PLAN (VDSL2_B7_4_8b), PLAN (VDSL2_B7_4_8c), PLAN (VDSL2_B7_4_8d),
        PLAN (VDSL2_B7_4_12a), PLAN (VDSL2_B7_4_12b),
        PLAN (VDSL2_B7_5_8a), PLAN (VDSL2_B7_5_8b), PLAN (VDSL2_B7_5_8c), PLAN (VDSL2_B7_5_8d),
        PLAN (VDSL2_B7_5_12a), PLAN (VDSL2_B7_5_12b),
        PLAN (VDSL2_B7_6_8a), PLAN (VDSL2_B7_6_8b), PLAN (VDSL2_B7_6_8c), PLAN (VDSL2_B7_6_8d),
        PLAN (VDSL2_B7_6_12a), PLAN (VDSL2_B7_6_12b),
        PLAN (VDSL2_B7_7_17a),
        PLAN (VDSL2_B7_8_30a),
        PLAN (VDSL2_B7_9_17a),
        PLAN (VDSL2_B7_10_30a),
        PLAN (VDSL2_B8_1_8a), PLAN (VDSL2_B8_1_8b), PLAN (VDSL2_B8_1_8c), PLAN (VDSL2_B8_1_8d),
        PLAN (VDSL2_B8_1_12a), PLAN (VDSL2_B8_1_12b),
        PLAN (VDSL2_B8_2_8a), PLAN (VDSL2_B8_2_8b), PLAN (VDSL2_B8_2_8c), PLAN (VDSL2_B8_2_8d),
        PLAN (VDSL2_B8_2_12a), PLAN (VDSL2_B8_2_12b),
        PLAN (VDSL2_B8_3_8a), PLAN (VDSL2_B8_3_8b), PLAN (VDSL2_B8_3_8c), PLAN (VDSL2_B8_3_8d),
        PLAN (VDSL2_B8_3_12a), PLAN (VDSL2_B8_3_12b),
        PLAN (VDSL2_B8_4_8a), PLAN (VDSL2_B8_4_8b), PLAN (VDSL2_B8_4_8c), PLAN (VDSL2_B8_4_8d),
        PLAN (VDSL2_B8_4_12a), PLAN (VDSL2_B8_4_12b),
        PLAN (VDSL2_B8_5_8a), PLAN (VDSL2_B8_5_8b), PLAN (VDSL2_B8_5_8c), PLAN (VDSL2_B8_5_8d),
        PLAN (VDSL2_B8_5_12a), PLAN (VDSL2_B8_5_12b),
        PLAN (VDSL2_B8_6_8a), PLAN (VDSL2_B8_6_8b), PLAN (VDSL2_B8_6_8c), PLAN (VDSL2_B8_6_8d),
        PLAN (VDSL2_B8_6_12a), PLAN (VDSL2_B8_6_12b),
        PLAN (VDSL2_B8_7_8a), PLAN (VDSL2_B8_7_8b), PLAN (VDSL2_B8_7_8c), PLAN (VDSL2_B8_7_8d),
        PLAN (VDSL2_B8_7_12a), PLAN (VDSL2_B8_7_12b),
        PLAN (VDSL2_B8_8_17a),
        PLAN (VDSL2_B8_9_17a),
        PLAN (VDSL2_B8_10_17a),
        PLAN (VDSL2_B8_11_17a),
        PLAN (VDSL2_B8_12_17a),
        PLAN (VDSL2_B8_13_30a),
        PLAN (VDSL2_B8_14_30a),
        PLAN (VDSL2_B8_15_30a),
        PLAN (VDSL2_B8_16_30a),
        PLAN (VDSL2_B8_17_17a),
        PLAN (VDSL2_B8_18_17a),
        PLAN (VDSL2_B8_19_35b),
        PLAN (VDSL2_B8_20_35b),
        PLAN (VDSL2_B8_21_35b),
        PLAN (VDSL2_B8_22_35b)
};

/*!
 * @fn          CBandplan::CBandplanData::CBandplanData (CBandplan::EBandplan eBandplan, CBandplan &cBandplan)
 * @brief       constructor
 * @param[in]   eBandplan
 *              bandplan index
 * @param[in]   cBandplan
 *              bandplan reference
 */
CBandplan::CBandplanData::CBandplanData (CBandplan::EBandplan eBandplan,
        CBandplan &cBandplan)
: m_eBandplan (eBandplan)
, m_cBandplan (cBandplan)
{
}

/*
 * @fn          CBandplan::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
 * @brief       copy constructor
 * @param[in]   cBandplanData
 *              bandplan data reference
 */
CBandplan::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
: m_eBandplan (cBandplanData.m_eBandplan)
, m_cBandplan (cBandplanData.m_cBandplan)
{
}


/*!
 * @fn          CBandplan *CBandplan::CBandplanData::Get (EBandplan eBandplan)
 * @brief       get bandplan
 * @param[in]   eBandplan
 *              bandplan index
 * @return      bandplan pointer
 */
CBandplan *CBandplan::CBandplanData::Get (EBandplan eBandplan)
{
    CBandplan &cBandplan = g_cBandplanNone;

    for (size_t nPlan = 0; nPlan < ARRAY_SIZE (g_acList); nPlan++)
    {
        if (g_acList[nPlan].m_eBandplan == eBandplan)
        {
            cBandplan = g_acList[nPlan].m_cBandplan;
            break;
        }
    }

    return &cBandplan;
}

/*!
 * @fn          CBandplan *CBandplan::Get (EBandplan eBandplan)
 * @brief       get bandplan
 * @param[in]   eBandplan
 *              bandplan index
 * @return      bandplan pointer
 */
CBandplan *CBandplan::Get (EBandplan eBandplan)
{
    return CBandplanData::Get (eBandplan);
}

/*!
 * @fn          void CBandplan::OperationMode (CString &strOperationMode)
 * @brief       get operation mode string
 * @param[out]  strOperationMode
 *              output string
 */
void CBandplan::OperationMode (CString &strOperationMode)
{
    strOperationMode.Format ("%s %s %s",
            m_strType, m_strITU_TVersion, m_strITU_TAnnex);
}

/*!
 * @fn          void CBandplan::Profile (CString &strProfile)
 * @brief       get profile string
 * @param[out]  strProfile
 *              output string
 */
void CBandplan::Profile (CString &strProfile)
{
    strProfile.Format ("%s %s",
            m_strITU_TProfile, m_strITU_TBandplan);
}

//_oOo_
