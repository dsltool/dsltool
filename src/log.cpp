/*!
 * @file        log.cpp
 * @brief       log class implemenation
 * @details     log helper class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#ifndef _WIN32
#include <syslog.h>
#endif // _WIN32

#include "log.h"
#include "txt.h"

/*!
 * @var         CLog CLog::g_cLog
 * @brief       global log instance
 * @details     singleton instance
 */
CLog CLog::g_cLog;

/*!
 * @fn          CLog::CLog (void)
 * @brief       constructor
 * @details     log to stderr
 */
CLog::CLog (void)
: m_eLevel (eInfo)
, m_bSyslog (false)
, m_cFile (stderr)
{
}

/*!
 * @fn          const char *CLog::GetLevel(ELevel eLevel)
 * @brief       get log level name
 * @param[in]   eLevel
 *              log level enum
 * @return      level name
 * @retval      NULL
 *              invalid log level
 */
const char *CLog::GetLevel (ELevel eLevel)
{
    switch (eLevel)
    {
    case eError:
        return "error";
    case eWarning:
        return "warning";
    case eInfo:
        return "info";
    case eDebug:
        return "debug";
    case eTrace:
        return "trace";
    case eExtra:
        return "extra";
    }
    return NULL;
}

/*!
 * @fn          CLog::ELevel CLog::GetLevel (const CString &strLevel)
 * @brief       get log level enum
 * @param[in]   strLevel
 *              log level command name
 * @return      level name
 */
CLog::ELevel CLog::GetLevel (const CString &strLevel)
{
    if (strLevel.CaseCompare (GetLevel (eExtra)))
    {
        return eExtra;
    }
    if (strLevel.CaseCompare (GetLevel (eError)))
    {
        return eError;
    }
    if (strLevel.CaseCompare (GetLevel (eWarning)))
    {
        return eWarning;
    }
    if (strLevel.CaseCompare (GetLevel (eDebug)))
    {
        return eDebug;
    }
    if (strLevel.CaseCompare (GetLevel (eTrace)))
    {
        return eTrace;
    }
    if (strLevel.CaseCompare (GetLevel (eExtra)))
    {
        return eExtra;
    }
    return eExtra;
}

/*!
 * @fn          bool CLog::LevelValid (const CString &strLevel)
 * @brief       test for valid log level
 * @param[in]   strLevel
 *              log level command name
 * @return      valid
 * @retval      true
 *              valid log level
 * @retval      true
 *              invalid log level
 */
bool CLog::LevelValid (const CString &strLevel)
{
    if (strLevel.CaseCompare (GetLevel (eExtra)))
    {
        return true;
    }
    if (strLevel.CaseCompare (GetLevel (eError)))
    {
        return true;
    }
    if (strLevel.CaseCompare (GetLevel (eWarning)))
    {
        return true;
    }
    if (strLevel.CaseCompare (GetLevel (eDebug)))
    {
        return true;
    }
    if (strLevel.CaseCompare (GetLevel (eTrace)))
    {
        return true;
    }
    if (strLevel.CaseCompare (GetLevel (eExtra)))
    {
        return true;
    }
    return false;
}

/*!
 * @fn          bool CLog::Open (const char *strFile)
 * @brief       open log file
 * @details     stderr is opened when open failed
 * @param[in]   strFile
 *              file name (# > syslog)
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to open log file
 */
bool CLog::Open (CString &strFile)
{
    bool bRet = false;

    if (strFile)
    {
        if (strFile.Compare ("#"))
        {
#ifdef _WIN32
            m_bSyslog = false;
            m_cFile.Open (stderr);
#else // _WIN32
            m_bSyslog = true;
#endif // _WIN32
            bRet = true;
        }
        else if (strFile.Compare ("-"))
        {
            m_bSyslog = false;
            m_cFile.Open (stderr);
            bRet = true;
        }
        else
        {
            m_bSyslog = false;
            bRet = m_cFile.Open (strFile, "a");
        }
    }

    if (!bRet)
    {
        m_bSyslog = false;
        m_cFile.Open (stderr);
        m_cFile.Printf (NStrErr::LogOpen, (const char*)strFile);
    }
    return bRet;
}

/*!
 * @fn          void CLog::Close (void)
 * @brief       close log file
 */
void CLog::Close (void)
{
    m_cFile.Close ();
}

/*!
 * @fn          void CLog::Message (ELevel eLevel,
 *                      const char *strFormat,
 *                      va_list vaArgs)
 * @brief       log message
 * @details     logs when eLevel satisfies log level
 * @param[in]   eLevel
 *              log level
 * @param[in]   strFormat
 *              format string
 * @param[in]   vaArgs
 *              arguments
 */
void CLog::Message (ELevel eLevel,
        const char *strFormat,
        va_list vaArgs)
{
    if (eLevel <= m_eLevel)
    {
#ifndef _WIN32
        if (m_bSyslog)
        {
            vsyslog (LOG_ERR + (int)eLevel, strFormat, vaArgs);
        }
        else
#endif // _WIN32
        {
            m_cFile.VPrintf (strFormat, vaArgs);
        }
    }
}

/*!
 * @fn          void CLog::Error (const char *strFormat, ...)
 * @brief       log error message
 * @param[in]   strFormat
 *              format string
 * @param[in]   ...
 *              arguments
 */
void CLog::Error (const char *strFormat, ...)
{
    va_list vaArgs;

    va_start (vaArgs, strFormat);
    Message (eError, strFormat, vaArgs);
    va_end (vaArgs);
}

/*!
 * @fn          void CLog::Warning (const char *strFormat, ...)
 * @brief       log warning message
 * @param[in]   strFormat
 *              format string
 * @param[in]   ...
 *              arguments
 */
void CLog::Warning (const char *strFormat, ...)
{
    va_list vaArgs;

    va_start (vaArgs, strFormat);
    Message (eWarning, strFormat, vaArgs);
    va_end (vaArgs);
}

/*!
 * @fn          void CLog::Info (const char *strFormat, ...)
 * @brief       log info message
 * @param[in]   strFormat
 *              format string
 * @param[in]   ...
 *              arguments
 */
void CLog::Info (const char *strFormat, ...)
{
    va_list vaArgs;

    va_start (vaArgs, strFormat);
    Message (eInfo, strFormat, vaArgs);
    va_end (vaArgs);
}

/*!
 * @fn          void CLog::Debug (const char *strFormat, ...)
 * @brief       log debug message
 * @param[in]   strFormat
 *              format string
 * @param[in]   ...
 *              arguments
 */
void CLog::Debug (const char *strFormat, ...)
{
    va_list vaArgs;

    va_start (vaArgs, strFormat);
    Message (eDebug, strFormat, vaArgs);
    va_end (vaArgs);
}

/*!
 * @fn          void CLog::Trace (const char *strFormat, ...)
 * @brief       log trace message
 * @param[in]   strFormat
 *              format string
 * @param[in]   ...
 *              arguments
 */
void CLog::Trace (const char *strFormat, ...)
{
    va_list vaArgs;

    va_start (vaArgs, strFormat);
    Message (eTrace, strFormat, vaArgs);
    va_end (vaArgs);
}

/*!
 * @fn          void CLog::Extra (const char *strFormat, ...)
 * @brief       log extra message
 * @param[in]   strFormat
 *              format string
 * @param[in]   ...
 *              arguments
 */
void CLog::Extra (const char *strFormat, ...)
{
    va_list vaArgs;

    va_start (vaArgs, strFormat);
    Message (eExtra, strFormat, vaArgs);
    va_end (vaArgs);
}
//_oOo_
