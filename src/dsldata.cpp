/*!
 * @file        dsldata.cpp
 * @brief       DSL data implementation
 * @details     parser data storage
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <math.h>

#include "dsldata.h"
#include "log.h"
#include "txt.h"

/*!
 * @fn          CData::CData (bool bDiff)
 * @brief       constructor
 * @param[in]   bDiff
 *              diff flag
 */
CData::CData (bool bDiff)
: m_bDiff (bDiff)
{
}

/*!
 * @fn          CDataInt::CDataInt (bool bDiff)
 * @brief       constructor
 * @param[in]   bDiff
 *              diff flag
 */
CDataInt::CDataInt (bool bDiff)
: CData (bDiff)
, m_nData (0)
, m_bValid (false)
, m_nLastData (0)
, m_bLastValid (false)
{
}

/*!
 * @fn          void CDataInt::Init (void)
 * @brief       initialize data
 * @details     copy data to last data
 *              invalidate data
 */
void CDataInt::Init (void)
{
    if (m_bDiff)
    {
        m_nLastData = m_nData;
        m_bLastValid = m_bValid;
    }
    m_bValid = false;
}

/*!
 * @fn          void CDataInt::Set (long nData)
 * @brief       set data value
 * @details     validate data
 * @param[in]   nData
 *              data value
 */
void CDataInt::Set (long nData)
{
    m_nData = nData;
    m_bValid = true;
}

/*!
 * @fn          void CDataInt::Get (long &nData) const
 * @brief       get data value
 * @param[out]  nData
 *              data value
 * @return      valid
 */
bool CDataInt::Get (long &nData) const
{
    if (m_bValid)
    {
        nData = m_nData;
    }
    return m_bValid;
}

/*!
 * @fn          void CDataInt::Diff (long &nData) const
 * @brief       get diff value
 * @param[out]  nData
 *              diff value
 * @return      valid
 */
bool CDataInt::Diff (long &nData) const
{
    if (m_bValid && m_bLastValid)
    {
        nData = m_nData - m_nLastData;
    }
    return m_bValid && m_bLastValid;
}

/*!
 * @fn          CDataIntUpDown::CDataIntUpDown (bool bDiff)
 * @brief       constructor
 * @param[in]   bDiff
 *              diff flag
 */
CDataIntUpDown::CDataIntUpDown (bool bDiff)
: CData (bDiff)
, m_nUp (0)
, m_nDown (0)
, m_bUpValid (false)
, m_bDownValid (false)
, m_nLastUp (0)
, m_nLastDown (0)
, m_bLastUpValid (false)
, m_bLastDownValid (false)
{
}

/*!
 * @fn          void CDataIntUpDown::Init (void)
 * @brief       initialize data
 * @details     copy data to last data
 *              invalidate data
 */
void CDataIntUpDown::Init (void)
{
    if (m_bDiff)
    {
        m_nLastUp = m_nUp;
        m_nLastDown = m_nDown;
        m_bLastUpValid = m_bUpValid;
        m_bLastDownValid = m_bDownValid;
    }

    m_bUpValid = false;
    m_bDownValid = false;
}

/*!
 * @fn          void CDataIntUpDown::Set (long nUp, long nDown)
 * @brief       set data value
 * @details     validate data
 * @param[in]   nUp
 *              upstream data value
 * @param[in]   nDown
 *              downstream data value
 */
void CDataIntUpDown::Set (long nUp, long nDown)
{
    m_nUp = nUp;
    m_nDown = nDown;
    m_bUpValid = true;
    m_bDownValid = true;
}

/*!
 * @fn          void CDataIntUpDown::SetUp (long nUp)
 * @brief       set upstream data value
 * @details     validate upstream data
 * @param[in]   nUp
 *              upstream data value
 */
void CDataIntUpDown::SetUp (long nUp)
{
    m_nUp = nUp;
    m_bUpValid = true;
}

/*!
 * @fn          void CDataIntUpDown::SetDown (long nDown)
 * @brief       set downstream data value
 * @details     validate downstream data
 * @param[in]   nDown
 *              downstream data value
 */
void CDataIntUpDown::SetDown (long nDown)
{
    m_nDown = nDown;
    m_bDownValid = true;
}

/*!
 * @fn          bool CDataIntUpDown::GetUp (long &nUp)const
 * @brief       get upstream data value
 * @param[out]  nUp
 *              upstream data value
 * @return      valid
 */
bool CDataIntUpDown::GetUp (long &nUp) const
{
    if (m_bUpValid)
    {
        nUp = m_nUp;
    }
    return m_bUpValid;
}

/*!
 * @fn          bool CDataIntUpDown::GetDown (long &nDown)const
 * @brief       get upstream data value
 * @param[out]  nDown
 *              downstream data value
 * @return      valid
 */
bool CDataIntUpDown::GetDown (long &nDown) const
{
    if (m_bDownValid)
    {
        nDown = m_nDown;
    }
    return m_bDownValid;
}

/*!
 * @fn          void CDataIntUpDown::DiffUp (long &nUp) const
 * @brief       get diff value
 * @param[out]  nUp
 *              diff value
 * @return      valid
 */
bool CDataIntUpDown::DiffUp (long &nUp) const
{
    if (m_bUpValid && m_bLastUpValid)
    {
        nUp = m_nUp - m_nLastUp;
    }
    return m_bUpValid && m_bLastUpValid;
}

/*!
 * @fn          bool CDataIntUpDown::DiffDown (long &nDown) const
 * @brief       get diff value
 * @param[out]  nDown
 *              diff value
 * @return      valid
 */
bool CDataIntUpDown::DiffDown (long &nDown) const
{
    if (m_bDownValid && m_bLastDownValid)
    {
        nDown = m_nDown - m_nLastDown;
    }
    return m_bDownValid && m_bLastDownValid;
}

/*!
 * @fn          CDataDouble::CDataDouble (bool bDiff)
 * @brief       constructor
 * @param[in]   bDiff
 *              diff flag
 */
CDataDouble::CDataDouble (bool bDiff)
: CData (bDiff)
, m_fData (0)
, m_bValid (false)
, m_fLastData (0)
, m_bLastValid (false)
{
}

/*!
 * @fn          void CDataDouble::Init (void)
 * @brief       initialize data
 * @details     copy data to last data
 *              invalidate data
 */
void CDataDouble::Init (void)
{
    if (m_bDiff)
    {
        m_fLastData = m_fData;
        m_bLastValid = m_bValid;
    }

    m_bValid = false;
}

/*!
 * @fn          void CDataDouble::Set (double fData)
 * @brief       set data value
 * @details     validate data
 * @param[in]   fData
 *              data value
 */
void CDataDouble::Set (double fData)
{
    m_fData = fData;
    m_bValid = true;
}

/*!
 * @fn          bool CDataDouble::Get (double &fData) const
 * @brief       get data value
 * @param[out]  fData
 *              data value
 * @return      valid
 */
bool CDataDouble::Get (double &fData) const
{
    if (m_bValid)
    {
        fData = m_fData;
    }
    return m_bValid;
}

/*!
 * @fn          void CDataDouble::Diff (double &fData) const
 * @brief       get diff value
 * @param[out]  fData
 *              diff value
 * @return      valid
 */
bool CDataDouble::Diff (double &fData) const
{
    if (m_bValid && m_bLastValid)
    {
        fData = m_fData - m_fLastData;
    }
    return m_bValid && m_bLastValid;
}

/*!
 * @fn          CDataDoubleUpDown::CDataDoubleUpDown (bool bDiff)
 * @brief       constructor
 * @param[in]   bDiff
 *              diff flag
 */
CDataDoubleUpDown::CDataDoubleUpDown (bool bDiff)
: CData (bDiff)
, m_fUp (0)
, m_fDown (0)
, m_bUpValid (false)
, m_bDownValid (false)
, m_fLastUp (0)
, m_fLastDown (0)
, m_bLastUpValid (false)
, m_bLastDownValid (false)
{
}

/*!
 * @fn          void CDataDoubleUpDown::Init (void)
 * @brief       initialize data
 * @details     copy data to last data
 *              invalidate data
 */
void CDataDoubleUpDown::Init (void)
{
    if (m_bDiff)
    {
        m_fLastUp = m_fUp;
        m_fLastDown = m_fDown;
        m_bLastUpValid = m_bUpValid;
        m_bLastDownValid = m_bDownValid;
    }

    m_bUpValid = false;
    m_bDownValid = false;
}

/*!
 * @fn          void CDataDoubleUpDown::Set (double fUp, double fDown)
 * @brief       set data value
 * @details     validate data
 * @param[in]   fUp
 *              upstream data value
 * @param[in]   fDown
 *              downstream data value
 */
void CDataDoubleUpDown::Set (double fUp, double fDown)
{
    m_fUp = fUp;
    m_fDown = fDown;
    m_bUpValid = true;
    m_bDownValid = true;
}

/*!
 * @fn          void CDataDoubleUpDown::SetUp (double fUp)
 * @brief       set upstream data value
 * @details     validate upstream data
 * @param[in]   fUp
 *              upstream data value
 */
void CDataDoubleUpDown::SetUp (double fUp)
{
    m_fUp = fUp;
    m_bUpValid = true;
}

/*!
 * @fn          void CDataDoubleUpDown::SetDown (double fDown)
 * @brief       set downstream data value
 * @details     validate downstream data
 * @param[in]   fDown
 *              downstream data value
 */
void CDataDoubleUpDown::SetDown (double fDown)
{
    m_fDown = fDown;
    m_bDownValid = true;
}

/*!
 * @fn          bool CDataDoubleUpDown::GetUp (double &fUp) const
 * @brief       get upstream data value
 * @param[out]  fUp
 *              upstream data value
 * @return      valid
 */
bool CDataDoubleUpDown::GetUp (double &fUp) const
{
    if (m_bUpValid)
    {
        fUp = m_fUp;
    }
    return m_bUpValid;
}

/*!
 * @fn          bool CDataDoubleUpDown::GetDown (double &fDown) const
 * @brief       get downstream data value
 * @param[out]  fDown
 *              downstream data value
 * @return      valid
 */
bool CDataDoubleUpDown::GetDown (double &fDown) const
{
    if (m_bDownValid)
    {
        fDown = m_fDown;
    }
    return m_bDownValid;
}

/*!
 * @fn          void CDataDoubleUpDown::DiffUp (double &fUp) const
 * @brief       get diff value
 * @param[out]  fUp
 *              diff value
 * @return      valid
 */
bool CDataDoubleUpDown::DiffUp (double &fUp) const
{
    if (m_bUpValid && m_bLastUpValid)
    {
        fUp = m_fUp - m_fLastUp;
    }
    return m_bUpValid && m_bLastUpValid;
}

/*!
 * @fn          bool CDataDoubleUpDown::DiffDown (double &fDown) const
 * @brief       get diff value
 * @param[out]  fDown
 *              diff value
 * @return      valid
 */
bool CDataDoubleUpDown::DiffDown (double &fDown) const
{
    if (m_bDownValid && m_bLastDownValid)
    {
        fDown = m_fDown - m_fLastDown;
    }
    return m_bDownValid && m_bLastDownValid;
}

/*!
 * @fn          CArrayDoubleUpDown::CArrayDoubleUpDown (int nIndex nSize,
 *                      const bool bDiff)
 * @brief       constructor
 * @param[in]   nSize
 *              array size
 * @param[in]   bDiff
 *              diff flag
 */
CArrayDoubleUpDown::CArrayDoubleUpDown (size_t nSize,
        bool bDiff)
: CData (bDiff)
, m_nSize (nSize)
, m_afUp (NULL)
, m_afDown (NULL)
, m_abUpValid (NULL)
, m_abDownValid (NULL)
, m_afLastUp (NULL)
, m_afLastDown (NULL)
, m_abLastUpValid (NULL)
, m_abLastDownValid (NULL)
{
    size_t i;

    m_afUp = new double[nSize];
    m_afDown = new double[nSize];
    m_abUpValid = new bool[nSize];
    m_abDownValid = new bool[nSize];
    m_afLastUp = new double[nSize];
    m_afLastDown = new double[nSize];
    m_abLastUpValid = new bool[nSize];
    m_abLastDownValid = new bool[nSize];

    if (m_abUpValid)
    {
        for (i = 0; i < nSize; i++)
        {
            m_abUpValid[i] = false;
        }
    }
    if (m_abDownValid)
    {
        for (i = 0; i < nSize; i++)
        {
            m_abDownValid[i] = false;
        }
    }
    if (m_abLastUpValid)
    {
        for (i = 0; i < nSize; i++)
        {
            m_abLastUpValid[i] = false;
        }
    }
    if (m_abLastDownValid)
    {
        for (i = 0; i < nSize; i++)
        {
            m_abLastDownValid[i] = false;
        }
    }
}

/*!
 * @fn          CArrayDoubleUpDown::~CArrayDoubleUpDown (void)
 * @brief       destructor
 * @details     free memory
 */
CArrayDoubleUpDown::~CArrayDoubleUpDown (void)
{
    if (m_afUp)
    {
        delete[] m_afUp;
    }
    if (m_afDown)
    {
        delete[] m_afDown;
    }
    if (m_abUpValid)
    {
        delete[] m_abUpValid;
    }
    if (m_abDownValid)
    {
        delete[] m_abDownValid;
    }
    if (m_afLastUp)
    {
        delete[] m_afLastUp;
    }
    if (m_afLastDown)
    {
        delete[] m_afLastDown;
    }
    if (m_abLastUpValid)
    {
        delete[] m_abLastUpValid;
    }
    if (m_abLastDownValid)
    {
        delete[] m_abLastDownValid;
    }
}

/*!
 * @fn          void CArrayDoubleUpDown::Init (void)
 * @brief       initialize data
 * @details     copy data to last data
 *              invalidate data
 */
void CArrayDoubleUpDown::Init (void)
{
    size_t i;

    if (m_bDiff)
    {
        if (m_afUp && m_abUpValid && m_afLastUp && m_abLastUpValid)
        {
            for (i = 0; i < m_nSize; i++)
            {
                m_afLastUp[i] = m_afUp[i];
                m_abLastUpValid[i] = m_abUpValid[i];
            }
        }
        if (m_afDown && m_abDownValid && m_afLastDown && m_abLastDownValid)
        {
            for (i = 0; i < m_nSize; i++)
            {
                m_afLastDown[i] = m_afDown[i];
                m_abLastDownValid[i] = m_abDownValid[i];
            }
        }
    }

    if (m_abUpValid)
    {
        for (i = 0; i < m_nSize; i++)
        {
            m_abUpValid[i] = false;
        }
    }
    if (m_abDownValid)
    {
        for (i = 0; i < m_nSize; i++)
        {
            m_abDownValid[i] = false;
        }
    }
}

/*!
 * @fn          void CArrayDoubleUpDown::Set (size_t nIndex,
 *                      double fUp, double fDown)
 * @brief       set data value
 * @details     validate data
 * @param[in]   nIndex
 *              array index
 * @param[in]   fUp
 *              upstream data value
 * @param[in]   fDown
 *              downstream data value
 */
void CArrayDoubleUpDown::Set (size_t nIndex,
        double fUp, double fDown)
{
    if (nIndex < m_nSize)
    {
        if (m_afUp)
        {
            m_afUp[nIndex] = fUp;
            if (m_abUpValid)
            {
                m_abUpValid[nIndex] = true;
            }
        }
        if (m_afDown)
        {
            m_afDown[nIndex] = fDown;
            if (m_abDownValid)
            {
                m_abDownValid[nIndex] = true;
            }
        }
    }
}

/*!
 * @fn          void CArrayDoubleUpDown::SetUp (size_t nIndex, double fUp)
 * @brief       set upstream data value
 * @details     validate upstream data
 * @param[in]   nIndex
 *              array index
 * @param[in]   fUp
 *              upstream data value
 */
void CArrayDoubleUpDown::SetUp (size_t nIndex, double fUp)
{
    if (nIndex < m_nSize)
    {
        if (m_afUp)
        {
            m_afUp[nIndex] = fUp;
            if (m_abUpValid)
            {
                m_abUpValid[nIndex] = true;
            }
        }
    }
}

/*!
 * @fn          void CArrayDoubleUpDown::SetDown (size_t nIndex, double fDown)
 * @brief       set downstream data value
 * @details     validate downstream data
 * @param[in]   nIndex
 *              array index
 * @param[in]   fDown
 *              downstream data value
 */
void CArrayDoubleUpDown::SetDown (size_t nIndex, double fDown)
{
    if (nIndex < m_nSize)
    {
        if (m_afDown)
        {
            m_afDown[nIndex] = fDown;
            if (m_abDownValid)
            {
                m_abDownValid[nIndex] = true;
            }
        }
    }
}

/*!
 * @fn          bool CArrayDoubleUpDown::GetUp (size_t nIndex, double &fUp)
 * @brief       get upstream data value
 * @param[in]   nIndex
 *              array index
 * @param[out]  fUp
 *              upstream data value
 * @return      valid
 */
bool CArrayDoubleUpDown::GetUp (size_t nIndex, double &fUp) const
{
    bool bValid = false;
    if (nIndex < m_nSize)
    {
        if (m_afUp)
        {
            if (m_abUpValid)
            {
                bValid = m_abUpValid[nIndex];
                if (bValid)
                {
                    fUp = m_afUp[nIndex];
                }
            }
        }
    }
    return bValid;
}

/*!
 * @fn          bool CArrayDoubleUpDown::GetDown (size_t nIndex, double &fDown)
 * @brief       get Downstream data value
 * @param[in]   nIndex
 *              array index
 * @param[out]  fDown
 *              Downstream data value
 * @return      valid
 */
bool CArrayDoubleUpDown::GetDown (size_t nIndex, double &fDown) const
{
    bool bValid = false;
    if (nIndex < m_nSize)
    {
        if (m_afDown)
        {
            if (m_abDownValid)
            {
                bValid = m_abDownValid[nIndex];
                if (bValid)
                {
                    fDown = m_afDown[nIndex];
                }
            }
        }
    }
    return bValid;
}

/*!
 * @fn          bool CArrayDoubleUpDown::DiffUp (size_t nIndex, double &fUp)
 * @brief       get upstream diff value
 * @param[in]   nIndex
 *              array index
 * @param[out]  fUp
 *              upstream diff value
 * @return      valid
 */
bool CArrayDoubleUpDown::DiffUp (size_t nIndex, double &fUp) const
{
    bool bValid = false;
    if (nIndex < m_nSize)
    {
        if (m_afUp && m_afLastUp)
        {
            if (m_abUpValid && m_abLastUpValid)
            {
                bValid = m_abUpValid[nIndex] && m_abLastUpValid[nIndex];
                if (bValid)
                {
                    fUp = m_afUp[nIndex] - m_afLastUp[nIndex];;
                }
            }
        }
    }
    return bValid;
}

/*!
 * @fn          bool CArrayDoubleUpDown::DiffDown (size_t nIndex, double &fDown) const
 * @brief       get Downstream data value
 * @param[in]   nIndex
 *              array index
 * @param[out]  fDown
 *              Downstream data value
 * @return      valid
 */
bool CArrayDoubleUpDown::DiffDown (size_t nIndex, double &fDown) const
{
    bool bValid = false;
    if (nIndex < m_nSize)
    {
        if (m_afDown && m_afLastDown)
        {
            if (m_abDownValid && m_abLastDownValid)
            {
                bValid = m_abDownValid[nIndex] && m_abLastDownValid[nIndex];
                if (bValid)
                {
                    fDown = m_afDown[nIndex] - m_afLastDown[nIndex];;
                }
            }
        }
    }
    return bValid;
}

/*!
 * @fn          CDataString::CDataString (void)
 * @brief       constructor
 */
CDataString::CDataString (void)
: CData (false)
, m_strData ()
, m_bValid (false)
{
}

/*!
 * @fn          void CDataString::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CDataString::Init (void)
{
    m_bValid = false;
}

/*!
 * @fn          void CDataString::Set (const char *strData)
 * @brief       set data value
 * @details     validate data
 * @param[in]   strData
 *              data value
 */
void CDataString::Set (const char *strData)
{
    m_strData = strData;
    m_bValid = true;
}

/*!
 * @fn          bool CDataString::Get (CString &strData) const
 * @brief       get data value
 * @param[in]   strData
 *              data value
 * @return      valid
 */
bool CDataString::Get (CString &strData) const
{
    if (m_bValid)
    {
        strData = m_strData;
    }
    return m_bValid;
}

/*!
 * @fn          CDataATU::CDataATU (void)
 * @brief       constructor
 */
CDataATU::CDataATU (void)
: CData (false)
, m_bVendorValid (false)
, m_bSpecValid (false)
, m_nRevision (0)
, m_bRevisionValid (false)
{
}

/*!
 * @fn          void CDataATU::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CDataATU::Init (void)
{
    m_bVendorValid = false;
    m_bSpecValid = false;
    m_bRevisionValid = false;
}

/*!
 * @fn          void CDataATU::Set (const char *strVendor,
 *                      int nSpec0, int nSpec1,
 *                      int nRevision)
 * @brief       set data value
 * @details     validate data
 * @param[in]   strVendor
 *              vendor code
 * @param[in]   nSpec0
 *              vendor specific value 0
 * @param[in]   nSpec1
 *              vendor specific value 1
 * @param[in]   nRevision
 *              revision
 */
void CDataATU::Set (const char *strVendor,
        int nSpec0, int nSpec1,
        int nRevision)
{
    SetVendor (strVendor);
    SetSpec (nSpec0, nSpec1);
    SetRevision (nRevision);
}

/*!
 * @fn          void CDataATU::SetVendor (const char *strVendor)
 * @brief       set vendor code
 * @details     validate vendor code
 * @param[in]   strVendor
 *              vendor code
 */
void CDataATU::SetVendor (const char *strVendor)
{
    size_t i;
    for(i = 0; i < 4; i++)
    {
        if (strVendor[i])
        {
            m_strVendor[i] = strVendor[i];
        }
        else
        {
            break;
        }
    }
    m_strVendor[i] = 0;
    m_bVendorValid = true;
}

/*!
 * @fn          void CDataATU::SetSpec (int nSpec0, int nSpec1)
 * @brief       set vendor specific value
 * @details     validate vendor specific value
 * @param[in]   nSpec0
 *              vendor specific value 0
 * @param[in]   nSpec1
 *              vendor specific value 1
 */
void CDataATU::SetSpec (int nSpec0, int nSpec1)
{
    m_nSpec[0] = nSpec0;
    m_nSpec[1] = nSpec1;
    m_bSpecValid = true;
}

/*!
 * @fn          void CDataATU::SetRevision (int nRevision)
 * @brief       set revision
 * @details     validate revision
 * @param[in]   nRevision
 *              revision
 */
void CDataATU::SetRevision (int nRevision)
{
    m_nRevision = nRevision;
    m_bRevisionValid = true;
}

/*!
 * @fn          bool CDataATU::GetVendor (CString &strVendor) const
 * @brief       get vendor code
 * @param[out]  strVendor
 *              vendor code
 * @return      valid
 */
bool CDataATU::GetVendor (CString &strVendor) const
{
    if (m_bVendorValid)
    {
        strVendor = m_strVendor;
    }
    return m_bVendorValid;
}

/*!
 * @fn          bool CDataATU::GetSpec (int &nSpec0, int &nSpec1) const
 * @brief       get vendor specific value
 * @param[out]  nSpec0
 *              vendor specific value 0
 * @param[out]  nSpec1
 *              vendor specific value 1
 * @return      valid
 */
bool CDataATU::GetSpec (int &nSpec0, int &nSpec1) const
{
    if (m_bSpecValid)
    {
        nSpec0 = m_nSpec[0];
        nSpec1 = m_nSpec[1];
    }
    return m_bSpecValid;
}

/*!
 * @fn          bool CDataATU::GetRevision (int &nRevision) const
 * @brief       get revision
 * @param[in]   nRevision
 *              revision
 * @return      valid
 */
bool CDataATU::GetRevision (int &nRevision) const
{
    if (m_bRevisionValid)
    {
        nRevision = m_nRevision;
    }
    return m_bRevisionValid;
}

/*!
 * @fn          CDataTone::CDataTone (void)
 * @brief       constructor
 */
CDataTone::CDataTone (void)
: CData (false)
, m_cData ()
, m_bValid (false)
{
}

/*!
 * @fn          void CDataTone::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CDataTone::Init (void)
{
    m_bValid = false;
    m_cData.Init ();
}

/*!
 * @fn          void CDataTone::Size (size_t nSize)
 * @brief       set tone array size
 * @details     invalidate data
 * @param[in]   nSize
 *              array size
 */
void CDataTone::Size (size_t nSize)
{
    m_bValid = false;
    m_cData.Size (nSize);
}

/*!
 * @fn          void CDataTone::Set (CTone &cData, size_t nMin, size_t nMax)
 * @brief       set tone value
 * @details     validate tones
 * @param[in]   cData
 *              source tone data
 * @param[in]   nMin
 *              min index
 * @param[in]   nMax
 *              max index
 */
void CDataTone::Set (CTone &cData, size_t nMin, size_t nMax)
{
    if (m_cData.Set (cData, nMin, nMax))
    {
        m_bValid = true;
    }
}

/*!
 * @fn          void CDataTone::Merge (CTone &cData, size_t nMin, size_t nMax)
 * @brief       merge tone value
 * @param[in]   cData
 *              source tone data
 * @param[in]   nMin
 *              min index
 * @param[in]   nMax
 *              max index
 */
void CDataTone::Merge (CTone &cData, size_t nMin, size_t nMax)
{
    m_cData.Merge (cData, nMin, nMax);
}

/*!
 * @fn          void CDataTone::Set (int *pnData, size_t nSize)
 * @brief       set tone value
 * @details     validate tones
 * @param[in]   pnData
 *              source tone data
 * @param[in]   nSize
 *              source tone array size
 */
void CDataTone::Set (int *pnData, size_t nSize)
{
    if (m_cData.Set (pnData, nSize))
    {
        m_bValid = true;
    }
}


/*!
 * @fn          void CDataTone::Set (size_t nIndex, int nData)
 * @brief       set tone value
 * @details     validate tones
 * @param[in]   nIndex
 *              array index
 * @param[in]   nData
 *              tone value
 */
void CDataTone::Set (size_t nIndex, int nData)
{
    if (m_cData.Set (nIndex, nData))
    {
        m_bValid = true;
    }
}

/*!
 * @fn          CBandwidthGroup::CBandwidthGroup (void)
 * @brief       constructor
 */
CBandwidthGroup::CBandwidthGroup (void)
: m_cCells ()
, m_cKbits ()
, m_cMaxKbits ()
{
}

/*!
 * @fn          void CBandwidthGroup::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CBandwidthGroup::Init (void)
{
    m_cCells.Init ();
    m_cKbits.Init ();
    m_cMaxKbits.Init ();
}

/*!
 * @fn          CAtmGroup::CAtmGroup (void)
 * @brief       constructor
 */
CAtmGroup::CAtmGroup (void)
: m_cVpiVci ()
, m_cATU_C ()
, m_cATU_R ()
{
}

/*!
 * @fn          void CAtmGroup::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CAtmGroup::Init (void)
{
    m_cVpiVci.Init ();
    m_cATU_C.Init ();
    m_cATU_R.Init ();
}

/*!
 * @fn          CErrorGroup::CErrorGroup (void)
 * @brief       constructor
 */
CErrorGroup::CErrorGroup (void)
: m_cFEC (true)
, m_cCRC (true)
, m_cHEC (true)
{
}

/*!
 * @fn          void CErrorGroup::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CErrorGroup::Init (void)
{
    m_cFEC.Init ();
    m_cCRC.Init ();
    m_cHEC.Init ();
}

/*!
 * @fn          CFailureGroup::CFailureGroup (void)
 * @brief       constructor
 */
CFailureGroup::CFailureGroup (void)
: m_cErrorSecs15min ()
, m_cErrorSecsDay ()
{
}

/*!
 * @fn          void CFailureGroup::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CFailureGroup::Init (void)
{
    m_cErrorSecs15min.Init ();
    m_cErrorSecsDay.Init ();
}

/*!
 * @fn          CStatisticsGroup::CStatisticsGroup (void)
 * @brief       constructor
 */
CStatisticsGroup::CStatisticsGroup (void)
: m_cError ()
, m_cFailure ()
{
}

/*!
 * @fn          void CStatisticsGroup::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CStatisticsGroup::Init (void)
{
    m_cError.Init ();
    m_cFailure.Init ();
}

/*!
 * @fn          CTonesGroup::CTonesGroup (void)
 * @brief       constructor
 */
CTonesGroup::CTonesGroup (void)
: m_cBitallocUp ()
, m_cBitallocDown ()
, m_cSNR ()
, m_cGainQ2 ()
, m_cNoiseMargin ()
, m_cChanCharLog ()
, m_pBandplan (NULL)
{
}

/*!
 * @fn          CTonesGroup::~CTonesGroup ()
 * @brief       destructor
 * @details     free memory
 */
CTonesGroup::~CTonesGroup ()
{
}

/*!
 * @fn          void CTonesGroup::Bandplan (CBandplan *pBandplan)
 * @brief       set bandplan
 * @details     resize tone data
 * @param[in]   pBandplan
 *              bandplan pointer
 */
void CTonesGroup::Bandplan (CBandplan *pBandplan)
{
    m_pBandplan = pBandplan;

    m_cBitallocUp.Size (m_pBandplan->Tones ());
    m_cBitallocDown.Size (m_pBandplan->Tones ());
    m_cSNR.Size (m_pBandplan->Tones ());
    m_cGainQ2.Size (m_pBandplan->Tones ());
    m_cNoiseMargin.Size (m_pBandplan->Tones ());
    m_cChanCharLog.Size (m_pBandplan->Tones ());
}

/*!
 * @fn          void CTonesGroup::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CTonesGroup::Init (void)
{
    m_cBitallocUp.Init ();
    m_cBitallocDown.Init ();
    m_cSNR.Init ();
    m_cGainQ2.Init ();
    m_cNoiseMargin.Init ();
    m_cChanCharLog.Init ();
}

/*!
 * @fn          CDslGroup::CDslGroup (void)
 * @brief       constructor
 */
CDslGroup::CDslGroup (void)
: m_cModemState ()
, m_cModemStateStr ()
, m_cOperationMode ()
, m_cProfile ()
, m_cChannelMode ()
, m_cNoiseMargin (CBand::s_nMaxBand)
, m_cAttenuation (CBand::s_nMaxBand)
, m_cTxPower ()
, m_cBandwidth ()
, m_cAtm ()
, m_cStatistics ()
, m_cTones ()
{
}

/*!
 * @fn          void CDslGroup::Init (void)
 * @brief       initialize data
 * @details     invalidate data
 */
void CDslGroup::Init (void)
{
    m_cModemState.Init ();
    m_cModemStateStr.Init ();
    m_cOperationMode.Init ();
    m_cProfile.Init ();
    m_cChannelMode.Init ();
    m_cNoiseMargin.Init ();
    m_cAttenuation.Init ();
    m_cTxPower.Init ();

    m_cBandwidth.Init ();
    m_cAtm.Init ();
    m_cStatistics.Init ();
    m_cTones.Init ();
}

/*!
 * @fn          CDslData::CDslData (void)
 * @brief       constructor
 */
CDslData::CDslData (void)
: m_tTimeStamp (0)
, m_cData ()
{
    Bandplan (CBandplan::eNone);
}

/*!
 * @fn          void CDslData::Init (void)
 * @brief       initialize data
 * @details     get timestamp, invalidate data
 */
void CDslData::Init (void)
{
    m_tTimeStamp = time (NULL);

    Bandplan (CBandplan::eNone);
    m_cData.Init ();
}

/*!
 * @fn          void CDslData::Bandplan (CBandplan::EBandplan eBandplan)
 * @brief       set bandplan
 * @param[in]   eBandplan
 *              bandplan index
 */
void CDslData::Bandplan (CBandplan::EBandplan eBandplan)
{
    m_cData.m_cTones.Bandplan (CBandplan::Get (eBandplan));
}

//_oOo_
