/*!
 * @file        parser.cpp
 * @brief       parser base class implementation
 * @details     parser base classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>
#include <stdlib.h>

#include "parser.h"
#include "modem.h"
#include "log.h"
#include "txt.h"

/*!
 * @fn          CParser::CParser (CModem *pModem)
 * @brief       constructor
 * @param[in]   pModem
 *              modem pointer
 */
CParser::CParser (CModem *pModem)
: m_pModem (pModem)
{
}

/*!
 * @fn          CParser::~CParser (void)
 * @brief       destructor
 */
CParser::~CParser (void)
{
}

/*!
 * @fn          void CParser::Open (EContent eContent)
 * @brief       open parser
 * @param[in]   eContent
 *              unused
 * @details     does nothing
 */
void CParser::Open (EContent eContent)
{
    (void)eContent;
}

/*!
 * @fn          CParser *CParser::Close (void)
 * @brief       close parser
 * @details     does nothing
 * @return      next parser
 */
CParser *CParser::Close (void)
{
    return this;
}

/*!
 * @fn          bool CParser::Find (const char *strTest, size_t nTest, const char *strFind)
 * @brief       find string
 * @details     compare at end of string
 * @param[in]   strTest
 *              string to test
 * @param[in]   nTest
 *              length of string to test
 * @param[in]   strFind
 *              string to find
 * @return      success
 * @retval      true
 *              match
 * @retval      false
 *              mismatch
 */
bool CParser::Find (const char *strTest, size_t nTest, const char *strFind)
{
    size_t nFind = strlen (strFind);

    if (nTest < nFind)
    {
        return false;
    }

    if (0 == strncasecmp (&strTest[nTest-nFind], strFind, nFind))
    {
        return true;
    }
    return false;
}

/*!
 * @fn          CLineParser::CLineParser (CModem *pModem)
 * @brief       constructor
 * @param[in]   pModem
 *              modem pointer
 */
CLineParser::CLineParser (CModem *pModem)
: CParser (pModem)
, m_strLine (NULL)
, m_nSize (256)
, m_nLine (0)
{
    m_strLine = new char[m_nSize];
}

/*!
 * @fn          CLineParser::~CLineParser (void)
 * @brief       destructor
 * @details     free line buffer
 */
CLineParser::~CLineParser (void)
{
    if (m_strLine)
    {
        delete[] m_strLine;
    }
}

/*!
 * @fn          CParser *CLineParser::Parse (const char *strRecv, size_t nRecv)
 * @brief       parse received data
 * @details     check for line end or prompt, call @ref ParseLine
 * @param[in]   strRecv
 *              receive data
 * @param[in]   nRecv
 *              receive data length
 * @return      next parser
 */
CParser *CLineParser::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;
    size_t nIdx;

    if (!m_strLine)
    {
        m_strLine = new char[m_nSize];
    }

    if (m_strLine)
    {
        for (nIdx = 0; nIdx < nRecv; nIdx++)
        {
            switch (strRecv[nIdx])
            {
                case '\r':
                case '\n':
                    m_strLine[m_nLine] = '\0';
                    if (m_nLine > 0)
                    {
                        LogExtra ("ParseLine: \"%s\"\n", m_strLine);
                        pNext = ParseLine (m_strLine);
                        m_nLine = 0;
                    }
                    break;
                case '\x1b': // <ESC>
                    m_strLine[m_nLine] = '\0';
                    if (m_nLine > 0)
                    {
                        LogExtra ("ParseLine: \"%s\"\n", m_strLine);
                        pNext = ParseLine (m_strLine);
                        m_nLine = 0;
                    }
                    // insert <ESC> at start of next line
                    m_strLine[m_nLine] = strRecv[nIdx];
                    m_nLine++;
                    break;
                default:
                    m_strLine[m_nLine] = strRecv[nIdx];
                    m_nLine++;
                    if (m_nLine >= m_nSize)
                    {
                        char *strTemp = m_strLine;

                        m_nSize *= 2;
                        m_strLine = new char[m_nSize];
                        memcpy (m_strLine, strTemp, m_nSize / 2);
                        delete[] strTemp;
                    }
                    break;
            }
            if (pNext != this)
            {
                break;
            }
        }

        if (pNext == this)
        {
            if ((strlen (m_pModem->Prompt ()) <= m_nLine) &&
                    (0 == strncmp (&m_strLine[m_nLine - strlen (m_pModem->Prompt ())],
                            m_pModem->Prompt (), strlen (m_pModem->Prompt ()))))
            {
                m_strLine[m_nLine] = '\0';
                LogExtra ("ParseLine: \"%s\"\n", m_strLine);
                pNext = ParseLine (m_strLine);
                m_nLine = 0;
            }
        }
    }

    return pNext;
}

//_oOo_
