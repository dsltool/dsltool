/*!
 * @file        xml-context.cpp
 * @brief       XML parser context class implementation
 * @details     libxml2 DOM and SAX2 parser wrapper
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        05.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>

#include "xml-parser.h"
#include "xml-context.h"

#include <libxml/xpathInternals.h>

#include "log.h"

/*!
 * @fn          CXmlContext::CXmlContext (bool bHTML)
 * @brief       constructor
 * @param[in]   bHTML
 *              HTML or XML flag
 */
CXmlContext::CXmlContext (bool bHTML)
: m_bHTML (bHTML)
, m_hParser (NULL)
, m_hXPath (NULL)
{
}

/*!
 * @fn          CXmlContext::~CXmlContext (void)
 * @brief       destructor
 * @details     close parser context
 */
CXmlContext::~CXmlContext (void)
{
    if (m_hParser)
    {
        if (m_bHTML)
        {
            htmlParseChunk (m_hParser, NULL, 0, 1);
        }
        else
        {
            xmlParseChunk (m_hParser, NULL, 0, 1);
        }

        if (m_hParser->myDoc)
        {
            xmlFreeDoc (m_hParser->myDoc);
            m_hParser->myDoc = NULL;
        }

        if (m_hXPath)
        {
            xmlXPathFreeContext (m_hXPath);
        }

        if (m_bHTML)
        {
            htmlFreeParserCtxt (m_hParser);
        }
        else
        {
            xmlFreeParserCtxt (m_hParser);
        }

        m_hParser = NULL;
    }
    xmlCleanupParser ();
}

/*!
 * @fn          void CXmlContext::Parse (const char *strRecv, size_t nRecv)
 * @brief       parse received data
 * @details     pass to libxml
 * @param[in]   strRecv
 *              receive data
 * @param[in]   nRecv
 *              receive data length
 * @return      next parser
 */
void CXmlContext::Parse (const char *strRecv, size_t nRecv)
{
    if (m_hParser)
    {
        if (m_bHTML)
        {
            htmlParseChunk (m_hParser, strRecv, (int)nRecv, 0);
        }
        else
        {
            xmlParseChunk (m_hParser, strRecv, (int)nRecv, 0);
        }
    }
}

/*!
 * @fn          void CXmlContext::XPathNamespace (const xmlChar *pPrefix,
 *                      const xmlChar *pURI)
 * @brief       add XPathQuery namespace
 * @param[in]   pPrefix
 *              namespace prefix
 * @param[in]   pURI
 *              namespace URI
 */
void CXmlContext::XPathNamespace (const xmlChar *pPrefix,
        const xmlChar *pURI)
{
    if (m_hParser)
    {
        if (!m_hXPath)
        {
            if (m_hParser->myDoc)
            {
                m_hXPath = xmlXPathNewContext (m_hParser->myDoc);
            }
        }

        if (m_hXPath)
        {
            xmlXPathRegisterNs (m_hXPath, pPrefix, pURI);
        }
    }
}

/*!
 * @fn          bool CXmlContext::XPathQuery (long  &nResult,
 *                      const xmlChar *pXPath)
 * @brief       XPath query long value
 * @param[out]  nResult
 *              value
 * @param[in]   pXPath
 *              query
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      true
 *              query failed
 */
bool CXmlContext::XPathQuery (long  &nResult,
        const xmlChar *pXPath)
{
    bool bRet = false;
    xmlXPathObjectPtr hObject = XPathObject (pXPath);

    if (hObject)
    {
        nResult = (long)xmlXPathCastToNumber (hObject);
        xmlXPathFreeObject (hObject);
        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          bool CXmlContext::XPathQuery (double &fResult,
 *                      const xmlChar *pXPath)
 * @brief       XPath query double value
 * @param[out]  fResult
 *              value
 * @param[in]   pXPath
 *              query
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      true
 *              query failed
 */
bool CXmlContext::XPathQuery (double &fResult,
        const xmlChar *pXPath)
{
    bool bRet = false;
    xmlXPathObjectPtr hObject = XPathObject (pXPath);

    if (hObject)
    {
        fResult = xmlXPathCastToNumber (hObject);
        xmlXPathFreeObject (hObject);
        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          bool CXmlContext::XPathQuery (CString &strResult,
 *                      const xmlChar *pXPath)
 * @brief       XPath query string value
 * @param[out]  strResult
 *              value
 * @param[in]   pXPath
 *              query
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      true
 *              query failed
 */
bool CXmlContext::XPathQuery (CString &strResult, const xmlChar *pXPath)
{
    bool bRet = false;
    xmlXPathObjectPtr hObject = XPathObject (pXPath);

    if (hObject)
    {
        xmlChar* pResult = xmlXPathCastToString (hObject);
        if (pResult)
        {
            strResult = (const char*)pResult;
            xmlFree(pResult);
        }
        xmlXPathFreeObject (hObject);
        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          xmlXPathObjectPtr CXmlContext::XPathObject (const xmlChar *pXPath)
 * @brief       get XPath object
 * @param[in]   pXPath
 *              query
 * @return      XPath object
 * @retval      NULL
 *              query failed
 */
xmlXPathObjectPtr CXmlContext::XPathObject (const xmlChar *pXPath)
{
    xmlXPathObjectPtr hObject = NULL;
    if (m_hParser)
    {
        if (!m_hXPath)
        {
            if (m_hParser->myDoc)
            {
                m_hXPath = xmlXPathNewContext (m_hParser->myDoc);
            }
        }

        if (m_hXPath)
        {
            hObject = xmlXPathEvalExpression (pXPath, m_hXPath);
        }
    }

    return hObject;
}

/*!
 * @fn          CDomContext::CDomContext (CDomParser *pParser, bool bHTML)
 * @brief       constructor
 * @details     create XML parser context
 * @param[in]   pParser
 *              DOM parser
 * @param[in]   bHTML
 *              HTML or XML flag
 */
CDomContext::CDomContext (CDomParser *pParser, bool bHTML)
: CXmlContext (bHTML)
, m_pParser (pParser)
{
    if (m_bHTML)
    {
        m_hParser = htmlCreatePushParserCtxt (NULL, NULL, NULL, 0, NULL,
                XML_CHAR_ENCODING_UTF8);
        htmlCtxtUseOptions (m_hParser, HTML_PARSE_NOBLANKS | HTML_PARSE_NONET);
    }
    else
    {
        m_hParser = xmlCreatePushParserCtxt (NULL, NULL, NULL, 0, NULL);
    }
}

/*!
 * @fn          CSaxContext::CSaxContext (CSaxParser *pParser, bool bHTML)
 * @brief       constructor
 * @details     create XML parser context
 * @param[in]   pParser
 *              SAX parser
 * @param[in]   bHTML
 *              HTML or XML flag
 */
CSaxContext::CSaxContext (CSaxParser *pParser, bool bHTML)
: CXmlContext (bHTML)
, m_pParser (pParser)
, m_tHandler ()
{
    memset (&m_tHandler, 0, sizeof (m_tHandler));

    m_tHandler.startDocument = _StartDocument;
    m_tHandler.endDocument = _EndDocument;
    m_tHandler.startElement = _StartElement;
    m_tHandler.endElement = _EndElement;
    m_tHandler.characters = _Characters;

    m_tHandler.initialized = XML_SAX2_MAGIC;

    if (m_bHTML)
    {
        m_hParser = htmlCreatePushParserCtxt (&m_tHandler, this, NULL, 0, NULL,
                XML_CHAR_ENCODING_UTF8);
        htmlCtxtUseOptions (m_hParser, HTML_PARSE_NOBLANKS | HTML_PARSE_NONET);
    }
    else
    {
        m_hParser = xmlCreatePushParserCtxt (&m_tHandler, this, NULL, 0, NULL);
    }
}

/*!
 * @fn          void CSaxContext::_StartDocument (void *pUser)
 * @brief       static start document callback
 * @param[in]   pUser
 *              this pointer (cast needed)
 */
void CSaxContext::_StartDocument (void *pUser)
{
    CSaxContext *pThis = (CSaxContext*)pUser;
    pThis->StartDocument ();
}

/*!
 * @fn          void CSaxContext::_EndDocument (void *pUser)
 * @brief       static end document callback
 * @param[in]   pUser
 *              this pointer (cast needed)
 */
void CSaxContext::_EndDocument (void *pUser)
{
    CSaxContext *pThis = (CSaxContext*)pUser;
    pThis->EndDocument ();
}

/*!
 * @fn          void CSaxContext::_StartElement (void *pUser,
 *                      const xmlChar *pElement,
 *                      const xmlChar **ppAttrs)
 * @brief       static start element callback
 * @param[in]   pUser
 *              this pointer (cast needed)
 * @param[in]   pElement
 *              element name
 * @param[in]   ppAttrs
 *              array of attribute/value pairs
 */
void CSaxContext::_StartElement (void *pUser,
        const xmlChar *pElement,
        const xmlChar **ppAttrs)
{
    CSaxContext *pThis = (CSaxContext*)pUser;
    pThis->StartElement (pElement, ppAttrs);
}

/*!
 * @fn          void CSaxContext::_EndElement (void *pUser,
 *                      const xmlChar *pElement)
 * @brief       static end element callback
 * @param[in]   pUser
 *              this pointer (cast needed)
 * @param[in]   pElement
 *              element name
 */
void CSaxContext::_EndElement (void *pUser,
        const xmlChar *pElement)
{
    CSaxContext *pThis = (CSaxContext*)pUser;
    pThis->EndElement (pElement);
}

/*!
 * @fn          void CSaxContext::_Characters (void *pUser,
 *                      const xmlChar *pCharacters,
 *                      int nSize)
 * @brief       static characters callback
 * @param[in]   pUser
 *              this pointer (cast needed)
 * @param[in]   pCharacters
 *              characters
 * @param[in]   nSize
 *              size of characters
 */
void CSaxContext::_Characters (void *pUser,
        const xmlChar *pCharacters,
        int nSize)
{
    CSaxContext *pThis = (CSaxContext*)pUser;
    pThis->Characters (pCharacters, nSize);
}

/*!
 * @fn          void CSaxContext::StartDocument (void)
 * @brief       static start document callback
 */
void CSaxContext::StartDocument (void)
{
    LogExtraEnter ("");

    m_pParser->StartDocument ();

    LogExtraLeave ("");
}

/*!
 * @fn          void CSaxContext::EndDocument (void)
 * @brief       static end document callback
 */
void CSaxContext::EndDocument (void)
{
    LogExtraEnter ("");

    m_pParser->EndDocument ();

    LogExtraLeave ("");
}

/*!
 * @fn          void CSaxContext::StartElement (const xmlChar *pElement,
 *                      const xmlChar **ppAttrs)
 * @brief       start element callback
 * @details     call parser
 * @param[in]   pElement
 *              element name
 * @param[in]   ppAttrs
 *              array of attribute/value pairs
 */
void CSaxContext::StartElement (const xmlChar *pElement,
        const xmlChar **ppAttrs)
{
    LogExtraEnter ("<%s>\n", pElement);

    m_pParser->StartElement ((const char*)pElement);

    if (ppAttrs != NULL)
    {
        int nAttr;

        for (nAttr = 0; ppAttrs[nAttr]; nAttr += 2)
        {
            LogExtra ("%s=%s\n", ppAttrs[nAttr], ppAttrs[nAttr + 1]);

            if (ppAttrs[nAttr + 1])
            {
                m_pParser->Attribute ((const char*)ppAttrs[nAttr], (const char*)ppAttrs[nAttr + 1]);
            }
            else
            {
                m_pParser->Attribute ((const char*)ppAttrs[nAttr], "");
            }
        }
    }

    LogExtraLeave ("");
}

/*!
 * @fn          void CSaxContext::EndElement (const xmlChar *pElement)
 * @brief       end element callback
 * @details     call parser
 * @param[in]   pElement
 *              element name
 */
void CSaxContext::EndElement (const xmlChar *pElement)
{
    CString strLine;

    LogExtraEnter ("</%s>\n", __PRETTY_FUNCTION__, pElement);

    m_pParser->EndElement ((const char*)pElement);

    LogExtraLeave ("");
}

/*!
 * @fn          void CSaxContext::Characters (const xmlChar *pCharacters,
 *                      int nSize)
 * @brief       characters callback
 * @details     call parser
 * @param[in]   pCharacters
 *              characters
 * @param[in]   nSize
 *              size of characters
 */
void CSaxContext::Characters (const xmlChar *pCharacters,
        int nSize)
{
    CString strLine;
    int nStart, nEnd;

    LogExtraEnter ("\"%.*s\"\n", nSize, pCharacters);

    for (nStart = 0, nEnd = 0; nEnd < nSize; nEnd++)
    {
        switch (pCharacters[nEnd])
        {
            case '\r':
            case '\n':
                if (1 < (nEnd - nStart))
                {
                    strLine.Format ("%.*s", nEnd - nStart, &pCharacters[nStart]);
                    m_pParser->Data ((const char*)strLine);
                }
                nEnd++;
                nStart=nEnd;
                break;
        }
    }

    if (1 < (nEnd - nStart))
    {
        strLine.Format ("%.*s", nEnd - nStart, &pCharacters[nStart]);
        m_pParser->Data ((const char*)strLine);
    }

    LogExtraLeave ("");
}

//_oOo_
