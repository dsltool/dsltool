/*!
 * @file        parser.cpp
 * @brief       parser base class implementation
 * @details     parser base classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "regex-parser.h"
#include "modem.h"
#include "log.h"
#include "txt.h"

/*!
 * @fn          CRegExBase::CRegExBase (void)
 * @brief       constructor
 */
CRegExBase::CRegExBase (void)
: m_pNext (NULL)
, m_strRegEx ()
{
}

/*!
 * @fn          CRegExBase::~CRegExBase (void)
 * @brief       destructor
 */
CRegExBase::~CRegExBase (void)
{
}

/*!
 * @fn          void CRegExBase::RegExInt (int nBase, size_t nLength)
 * @brief       create integer regular expression
 * @param[in]   nBase
 *              integer base
 * @param[in]   nLength
 *              string length
 */
void CRegExBase::RegExInt (int nBase, size_t nLength)
{
    if (nLength)
    {
        switch (nBase)
        {
        case 8:
            m_strRegEx.Format ("([0-7]{%d})", nLength);
            break;
        case 16:
            m_strRegEx.Format ("([0-9a-f]{%d})", nLength);
            break;
        default:
            m_strRegEx.Format ("([-+]?[0-9]{%d})", nLength);
            break;
        }
    }
    else
    {
        switch (nBase)
        {
        case 8:
            m_strRegEx = "([0-7]+)";
            break;
        case 16:
            m_strRegEx = "([0-9a-f]+)";
            break;
        default:
            m_strRegEx = "([-+]?[0-9]+)";
            break;
        }
    }
}

/*!
 * @fn          const char *CRegExBase::RegEx (void)
 * @brief       get regular expression
 * @return      regular expression
 */
const char *CRegExBase::RegEx (void)
{
    return m_strRegEx;
}

/*!
 * @fn          size_t CRegExBase::RegExLength (void)
 * @brief       get regular expression length
 * @return      regular expression length
 */
size_t CRegExBase::RegExLength (void)
{
    return m_strRegEx.Length ();
}

/*!
 * @fn          CRegExSep::CRegExSep (bool bForce)
 * @brief       constructor
 * @param[in]   bForce
 *              force space
 */
CRegExSep::CRegExSep (bool bForce)
{
    if (bForce)
    {
        m_strRegEx = "([[:space:]]+)";
    }
    else
    {
        m_strRegEx = "([[:space:]]*)";
    }
}

/*!
 * @fn          bool CRegExSep::Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              unused
 * @param[in]   nLength
 *              unused
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CRegExSep::Parse (const char *strText, size_t nLength)
{
    (void)strText;
    (void)nLength;

    return true;
}

/*!
 * @fn          CRegExConst::CRegExConst (const char *strRegEx)
 * @brief       constructor
 * @param[in]   strRegEx
 *              regular expression
 */
CRegExConst::CRegExConst (const char *strRegEx)
{
    m_strRegEx.Format ("(%s)", strRegEx);
}

/*!
 * @fn          bool CRegExConst::Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              unused
 * @param[in]   nLength
 *              unused
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CRegExConst::Parse (const char *strText, size_t nLength)
{
    (void)strText;
    (void)nLength;

    return true;
}

/*!
 * @fn          CRegExTerm::CRegExTerm (const char *strTerm)
 * @brief       constructor
 * @param[in]   strTerm
 *              terminal escape string, will be preceeded by \<ESC\>[
 */
CRegExTerm::CRegExTerm (const char *strTerm)
{
    m_strRegEx.Format ("(\x1b\\[%s)", strTerm);
}

/*!
 * @fn          bool CRegExTerm::Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              unused
 * @param[in]   nLength
 *              unused
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CRegExTerm::Parse (const char *strText, size_t nLength)
{
    (void)strText;
    (void)nLength;

    return true;
}

/*!
 * @fn          CRegExInt::CRegExInt (long &nData, int nBase, size_t nLength)
 * @brief       constructor
 * @param[in]   nData
 *              integer reference
 * @param[in]   nBase
 *              integer base
 * @param[in]   nLength
 *              string length
 */
CRegExInt::CRegExInt (long &nData, int nBase, size_t nLength)
: m_nData (nData)
, m_nBase (nBase)
{
    RegExInt (nBase, nLength);
}

/*!
 * @fn          bool CRegExInt::Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              string to parse
 * @param[in]   nLength
 *              length of string
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CRegExInt::Parse (const char *strText, size_t nLength)
{
    CString strTemp (strText, nLength);
    m_nData = strTemp.Int (m_nBase);
    return true;
}

/*!
 * @fn          CRegExDouble::CRegExDouble (double &fData)
 * @brief       constructor
 * @param[in]   fData
 *              double reference
 */
CRegExDouble::CRegExDouble (double &fData)
: m_fData (fData)
{
    m_strRegEx = "([-+]?[0-9]*\\.?[0-9]*)";
}

/*!
 * @fn          bool CRegExDouble::Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              string to parse
 * @param[in]   nLength
 *              length of string
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CRegExDouble::Parse (const char *strText, size_t nLength)
{
    CString strTemp (strText, nLength);
    m_fData = strTemp.Double ();
    return true;
}

/*!
 * @fn          CRegExString::CRegExString (CString &strData, const char *strRegEx)
 * @brief       constructor
 * @param[in]   strData
 *              string reference
 * @param[in]   strRegEx
 *              regular expression (or NULL)
 */
CRegExString::CRegExString (CString &strData, const char *strRegEx)
: m_strData (strData)
{
    if (strRegEx)
    {
        m_strRegEx.Format ("(%s)", strRegEx);
    }
    else
    {
        m_strRegEx = "(.*)";
    }
}

/*!
 * @fn          bool CRegExString::Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              string to parse
 * @param[in]   nLength
 *              length of string
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              parse failed
 */
bool CRegExString::Parse (const char *strText, size_t nLength)
{
    bool bRet = false;

    if (strText)
    {
        while ((nLength > 0) && (' ' == strText[nLength-1]))
        {
            nLength--;
        }
        m_strData = CString (strText, nLength);
        bRet = true;
    }
    return bRet;
}

/*!
 * @fn          CRegExWord::CRegExWord (CString &strData)
 * @brief       constructor
 * @param[in]   strData
 *              string reference
 */
CRegExWord::CRegExWord (CString &strData)
: CRegExString (strData)
{
    m_strRegEx = "([^ ]*)";
}

/*!
 * @fn          CRegExTone::CRegExTone (CTone &cTone,
 *                      int nBase, size_t nLength)
 * @brief       constructor
 * @param[in]   cTone
 *              tone reference
 * @param[in]   nBase
 *              integer base
 * @param[in]   nLength
 *              string length
 */
CRegExTone::CRegExTone (CTone &cTone,
        int nBase, size_t nLength)
: m_cTone (cTone)
, m_nBase (nBase)
, m_nLength (nLength)
{
    RegExInt (nBase, nLength);
}

/*!
 * @fn          bool CRegExTone::Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              string to parse
 * @param[in]   nLength
 *              length of string
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              parse failed
 */
bool CRegExTone::Parse (const char *strText, size_t nLength)
{
    bool bRet = false;
    if (strText && nLength)
    {
        CString strTemp (strText, nLength);
        m_cTone.SetInc ((int)strTemp.Int (m_nBase));
        bRet = true;
    }
    return bRet;
}

/*!
 * @fn          CRegExToneIndex::CRegExToneIndex (CTone &cTone,
 *                      int nBase, size_t nLength)
 * @brief       constructor
 * @param[in]   cTone
 *              tone reference
 * @param[in]   nBase
 *              integer base
 * @param[in]   nLength
 *              string length
 */
CRegExToneIndex::CRegExToneIndex (CTone &cTone,
        int nBase, size_t nLength)
: CRegExTone (cTone, nBase, nLength)
{
}

/*!
 * @fn          bool CRegExToneIndex::Parse (const char *strText, size_t nLength)
 * @brief       parse string
 * @param[in]   strText
 *              string to parse
 * @param[in]   nLength
 *              length of string
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CRegExToneIndex::Parse (const char *strText, size_t nLength)
{
    CString strTemp (strText, nLength);
    m_cTone.Index ((size_t)strTemp.Int (m_nBase));
    return true;
}

/*!
 * @fn          CRegExList::CRegExList (void)
 * @brief       constructor
 */
CRegExList::CRegExList (void)
: m_pHead (NULL)
, m_pTail (NULL)
, m_nCount (0)
, m_strRegEx (NULL)
, m_tRegex ()
{
}

/*!
 * @fn          CRegExList::~CRegExList (void)
 * @brief       constructor
 * @details     delete list
 */
CRegExList::~CRegExList (void)
{
    CRegExBase *pList = m_pHead;

    while (pList)
    {
		CRegExBase *pNext = pList->Next();
        delete pList;
        pList = pNext;
    }
    if (m_strRegEx)
    {
        delete [] m_strRegEx;
        regfree (&m_tRegex);
    }
}

/*!
 * @fn          bool CRegExList::Add (CRegExBase *pRegEx)
 * @brief       add object to list
 * @param[in]   pRegEx
 *              regular expression object
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::Add (CRegExBase *pRegEx)
{
    bool bRet = false;

    if (pRegEx)
    {
        if (!m_pHead)
        {
            m_pHead = pRegEx;
        }
        if (m_pTail)
        {
            m_pTail->Next (pRegEx);
        }
        m_pTail = pRegEx;
        m_nCount++;
        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          bool CRegExList::AddSep (bool bForce)
 * @brief       create separator regex and add to list
 * @param[in]   bForce
 *              force space
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddSep (bool bForce)
{
    return Add (new CRegExSep (bForce));
}

/*!
 * @fn          bool CRegExList::AddConst (const char *strRegEx)
 * @brief       create const regex and add to list
 * @param[in]   strRegEx
 *              regular expression
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddConst (const char *strRegEx)
{
    return Add (new CRegExConst (strRegEx));
}

/*!
 * @fn          bool CRegExList::AddTerm (const char *strTerm)
 * @brief       create terminal escape regex and add to list
 * @param[in]   strTerm
 *              terminal escape string, will be preceeded by \<ESC\>[
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddTerm (const char *strTerm)
{
    return Add (new CRegExTerm (strTerm));
}

/*!
 * @fn          bool CRegExList::AddInt (long &nData, int nBase, size_t nLength)
 * @brief       create integer regex and add to list
 * @param[in]   nData
 *              integer reference
 * @param[in]   nBase
 *              integer base
 * @param[in]   nLength
 *              string length
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddInt (long &nData, int nBase, size_t nLength)
{
    return Add (new CRegExInt (nData, nBase, nLength));
}

/*!
 * @fn          bool CRegExList::AddDouble (double &fData)
 * @brief       create double regex and add to list
 * @param[in]   fData
 *              double reference
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddDouble (double &fData)
{
    return Add (new CRegExDouble (fData));
}

/*!
 * @fn          bool CRegExList::AddString (CString &strData, const char *strRegEx)
 * @brief       create string regex and add to list
 * @param[in]   strData
 *              string reference
 * @param[in]   strRegEx
 *              regular expression (or NULL)
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddString (CString &strData, const char *strRegEx)
{
    return Add (new CRegExString (strData, strRegEx));
}

/*!
 * @fn          bool CRegExList::AddWord (CString &strData)
 * @brief       create word regex and add to list
 * @param[in]   strData
 *              string reference
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddWord (CString &strData)
{
    return Add (new CRegExWord (strData));
}

/*!
 * @fn          bool CRegExList::AddTone (CTone &cTone, int nBase, size_t nLength)
 * @brief       create tone regex and add to list
 * @param[in]   cTone
 *              tone reference
 * @param[in]   nBase
 *              integer base
 * @param[in]   nLength
 *              string length
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddTone (CTone &cTone, int nBase, size_t nLength)
{
    return Add (new CRegExTone (cTone, nBase, nLength));
}

/*!
 * @fn          bool CRegExList::AddToneIndex (CTone &cTone, int nBase, size_t nLength)
 * @brief       create tone index regex and add to list
 * @param[in]   cTone
 *              tone reference
 * @param[in]   nBase
 *              integer base
 * @param[in]   nLength
 *              string length
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to add object
 */
bool CRegExList::AddToneIndex (CTone &cTone, int nBase, size_t nLength)
{
    return Add (new CRegExToneIndex (cTone, nBase, nLength));
}

/*!
 * @fn          bool CRegExList::Init (void)
 * @brief       initialize regular expression parser
 * @details     must be called after all Add* calls
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to initialize parser
 */
bool CRegExList::Init (void)
{
    bool bRet = false;
    size_t nLength = 1;
    CRegExBase *pList;


    for (pList = m_pHead; pList; pList = pList->Next ())
    {
        nLength += pList->RegExLength ();
    }

    if (m_strRegEx)
    {
        delete [] m_strRegEx;
        regfree (&m_tRegex);
    }

    m_strRegEx = new char[nLength];
    m_strRegEx[0] = '\0';

    if (m_strRegEx)
    {
        int nRet;

        for (pList = m_pHead; pList; pList = pList->Next ())
        {
            strncat (m_strRegEx, pList->RegEx (), nLength);
        }

        nRet = regcomp (&m_tRegex, m_strRegEx, REG_EXTENDED | REG_ICASE);
        if (0 == nRet)
        {
            bRet = true;
        }
        else
        {
            char strError[256];
            regerror (nRet, &m_tRegex, strError, sizeof (strError));
            LogError (NStrErr::ParserRegComp, strError, m_strRegEx);

            delete [] m_strRegEx;
            m_strRegEx = NULL;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CRegExList::Parse (const char *strTest)
 * @brief       match string to regular expression
 * @details     assign all found values to variables
 * @param[in]   strTest
 *              string to test
 * @return      success
 * @retval      true
 *              match
 * @retval      false
 *              mismatch
 */
bool CRegExList::Parse (const char *strTest)
{
    bool bRet = false;

    if (m_strRegEx)
    {
        regmatch_t *ptMatch = new regmatch_t[m_nCount + 1];

        if (ptMatch)
        {
            if (0 == regexec (&m_tRegex, strTest, m_nCount + 1, ptMatch, 0))
            {
                int i;
                CRegExBase *pList = m_pHead;
                bRet = true; // matched

                for (i = 1, pList = m_pHead;
                     pList;
                     pList = pList->Next (), i++)
                {
                    LogExtra ("match[%d] \"%.*s\"\n",
                            i, ptMatch[i].rm_eo - ptMatch[i].rm_so,
                            strTest + ptMatch[i].rm_so);
                    LogExtra ("regex:\"%s\"\ntest: \"%s\"\n", m_strRegEx, strTest);

                    if (!pList->Parse (strTest + ptMatch[i].rm_so,
                            (size_t)(ptMatch[i].rm_eo - ptMatch[i].rm_so)))
                    {
                        bRet = false; // parse failed
                        break;
                    }
                }
            }
            delete [] ptMatch;
        }
    }

    return bRet;
}

//_oOo_
