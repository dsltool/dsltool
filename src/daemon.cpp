/*!
 * @file        daemon.cpp
 * @brief       daemon application class implementation
 * @details     daemon execution classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>

#include "daemon.h"
#include "txt.h"


/*!
 * @fn          CRunDaemon::CRunDaemon (void)
 * @brief       constructor
 */
CRunDaemon::CRunDaemon (void)
: CRun ()
, m_bLoop (true)
, m_tSem ()
{
    m_cConfig.m_bDaemon = true;
    m_cConfig.m_ulOutput = CConfigData::s_ulOutCollect;
    m_cConfig.m_eCommand = CModem::eStatus;

    sem_init (&m_tSem, 0, 0);
}

/*!
 * @fn          CRunDaemon::~CRunDaemon(void)
 * @brief       destructor
 */
CRunDaemon::~CRunDaemon (void)
{
}

/*!
 * @fn          bool CRunDaemon::Run (void)
 * @brief       execute protocol and parser
 * @details     initialize signal handler to allow abort processing
 * @n           call @ref Process in a loop
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              run failed
 */
bool CRunDaemon::Run (void)
{
    bool bRet = false;

    InitSignal ();

    if (Daemonize ())
    {
        WritePid ();
        InitTimer ();

        if (Init ())
        {
            if (Loop ())
            {
                bRet = true;
            }
        }
        Finish ();
        DeletePid ();
    }

    return bRet;
}

/*!
 * @fn          void CRunDaemon::InitSignal (void)
 * @brief       initialize signal handler
 */
void CRunDaemon::InitSignal (void)
{
    signal (SIGINT, _SigHandler);
#ifndef _WIN32
    signal (SIGKILL, _SigHandler);
#endif // _WIN32
}

/*!
 * @fn          void CRunDaemon::InitTimer (void)
 * @brief       initialize timer signal handler
 */
void CRunDaemon::InitTimer (void)
{
    struct itimerval tTimer;

    tTimer.it_interval.tv_sec = m_cConfig.m_nInterval;
    tTimer.it_interval.tv_usec = 0;
    tTimer.it_value.tv_sec = 0;
    tTimer.it_value.tv_usec = 1000;

    signal (SIGALRM, _SigHandler);
    setitimer (ITIMER_REAL, &tTimer, NULL);
}

/*!
 * @fn          void CRunDaemon::WritePid (void)
 * @brief       write process id to pid file
 */
void CRunDaemon::WritePid (void)
{
    CFile cPidFile;

    if (cPidFile.Open (m_cConfig.m_strPidFile, "a"))
    {
        cPidFile.Printf ("%d", getpid ());
    }
}

/*!
 * @fn          void CRunDaemon::DeletePid (void)
 * @brief       delete pid file
 */
void CRunDaemon::DeletePid (void)
{
    CFile::Delete (m_cConfig.m_strPidFile);
}

/*!
 * @fn          void CRunDaemon::SigHandler (int nSignal)
 * @brief       signal handler
 * @details     release semaphore to run loop
 * @n           abort protocol parser on SIGINT and SIGKILL
 * @param[in]   nSignal
 *              signal number
 */
void CRunDaemon::SigHandler (int nSignal)
{
    LogDebug ("signal %d received\n", nSignal);

    switch (nSignal)
    {
    case SIGALRM:
        sem_post (&m_tSem);
        break;
    case SIGINT:
#ifndef _WIN32
    case SIGKILL:
#endif // _WIN32
        m_bLoop = false;
        if (m_pProtocol)
        {
            m_pProtocol->Stop ();
        }
        sem_post (&m_tSem);
        break;
    }
}

/*!
 * @fn          bool CRunDaemon::Daemonize (void)
 * @brief       put process in daemon mode
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              daemonize failed
 */
bool CRunDaemon::Daemonize (void)
{
    bool bRet = false;

    if (m_cConfig.m_bBackground)
    {
        if (-1 == daemon (1, 1))
        {
            LogError (NStrErr::Daemon, strerror (errno));
        }
        else
        {
            bRet = true;
        }
    }
    else
    {
        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          bool CRunDaemon::Loop (void)
 * @brief       execute protocol and parser
 * @n           call @ref Process in a loop
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              run failed
 */
bool CRunDaemon::Loop (void)
{
    bool bRet = true;

    if (m_pModem)
    {
        int nCount = 0;
        while (m_bLoop)
        {
            sem_wait (&m_tSem);
            if (m_bLoop)
            {
                nCount++;

                LogTrace ("Process Loop %d\n", nCount);

                bRet = Process ();
                if (bRet)
                {
                    Output ();
                }
                else
                {
                    m_bLoop = false;
                }
            }
        }
    }

    return bRet;
}

//_oOo_
