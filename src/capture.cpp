/*!
 * @file        capture.cpp
 * @brief       capture class implementation
 * @details     pcap file creation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        08.04.2019
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <capture.h>
#include <string.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/ip6.h>
#include <netinet/ether.h>

/*!
 * @fn          CCapture::CCapture (void)
 * @brief       constructor
 */
CCapture::CCapture (void)
: m_pPcap(NULL)
, m_pDumper(NULL)
, m_nPort (0)
, m_nTxSeq (0)
, m_nTxNext (0)
, m_nRxSeq (0)
, m_nRxNext (0)
, m_eState(eStateInit)
, m_u8Flags(0)
, m_nData(0)
{
}

/*!
 * @fn          CCapture::~CCapture (void)
 * @brief       destructor
 */
CCapture::~CCapture (void)
{
    Close();
}

/*!
 * @fn          bool CCapture::Open (const char *strFile, int nPort)
 * @brief       open dump
 * @details     create pcap file
 * @param[in]   strFile
 *              pcap file name
 * @param[in]   nPort
 *              TCP Port number used in pcap file
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CCapture::Open (const char *strFile, int nPort)
{
    Close();

    m_nPort = nPort;
    m_nTxSeq = 0;
    m_nRxSeq = 0;

    m_pPcap = pcap_open_dead(DLT_EN10MB, CAPTURE_FRAME);

    if (m_pPcap)
    {
        m_pDumper = pcap_dump_open(m_pPcap, strFile);
    }

    if(m_pDumper)
    {
        Dump (eStateSend, TH_SYN);
        Dump (eStateReceive, TH_SYN|TH_ACK);
        Flush ();
    }
    else
    {
        Close();
    }

    return (NULL != m_pDumper);
}

/*!
 * @fn          void CCapture::Close (void)
 * @brief       close pcap file
 */
void CCapture::Close (void)
{
    if (m_pDumper)
    {
        Flush ();
        Dump (eStateReceive, TH_FIN|TH_ACK);
        Dump (eStateSend, TH_FIN|TH_ACK);
        Dump (eStateReceive, TH_ACK);
        Flush ();

        pcap_dump_close (m_pDumper);
        m_pDumper = NULL;
    }

    if (m_pPcap)
    {
        pcap_close (m_pPcap);
        m_pPcap = NULL;
    }
}

/*!
 * @fn          void CCapture::Dump (bool bSend, uint8_t u8Flags,
 *                      const void *pData, size_t nData)
 * @brief       write packet
 * @details     create TCP packet with data
 * @param[in]   eState
 *              direction, create send or receive packet
 * @param[in]   u8Flags
 *              TCP flags
 * @param[in]   pData
 *              data pointer
 * @param[in]   nData
 *              data size
 */
void CCapture::Dump (EState eState, uint8_t u8Flags,
        const void *pData, size_t nData)
{
    size_t nTemp;

    if (m_eState != eState)
    {
        Flush ();
    }

    while (nData > 0)
    {
        nTemp = CAPTURE_DATA - m_nData;
        if (nData < nTemp)
        {
            nTemp = nData;
        }
        memcpy (&m_pData[m_nData], pData, nTemp);
        m_nData += nTemp;
        nData -= nTemp;

        if(m_nData >= CAPTURE_DATA)
        {
            Flush ();
        }
    };
    m_u8Flags = u8Flags;
    m_eState = eState;
}

void CCapture::Flush (void)
{
    if ((m_pDumper) && (eStateInit != m_eState))
    {
        u_char pPacket[CAPTURE_FRAME];
        struct ether_header *pEth = (struct ether_header *)((void*)pPacket);
        struct iphdr *pIP = (struct iphdr*)(pEth + 1);
        struct tcphdr *pTCP = (struct tcphdr*)(pIP + 1);
        void *pTemp = (void*)(pTCP + 1);
        pcap_pkthdr tHeader;

        memset(pPacket, 0, sizeof(pPacket));

        gettimeofday(&tHeader.ts, NULL);
        tHeader.caplen = tHeader.len = (uint32_t)(sizeof(*pEth) + sizeof(*pIP) + sizeof (*pTCP) + m_nData);

        // build ethernet header
        pEth->ether_type = htons(ETHERTYPE_IP);
        pEth->ether_shost[0] = 0x02;
        pEth->ether_dhost[0] = 0x02;

        if (eStateSend == m_eState)
        {
            pEth->ether_shost[5] = 0x01;
            pEth->ether_dhost[5] = 0x02;
        }
        else
        {
            pEth->ether_shost[5] = 0x02;
            pEth->ether_dhost[5] = 0x01;
        }

        // build IP header
        pIP->version = 4;
        pIP->ihl = sizeof(*pIP) >> 2;
        pIP->tos = 0;
        pIP->tot_len = htons((uint16_t)(sizeof(*pIP) + sizeof (*pTCP) + m_nData));
        pIP->id = 0;
        pIP->frag_off = 0;
        pIP->ttl = 64;
        pIP->protocol = IPPROTO_TCP;

        if (eStateSend == m_eState)
        {
            m_nTxSeq = m_nTxNext;

            pIP->saddr = htonl(0xC0A80001); // 192.168.0.1
            pIP->daddr = htonl(0xC0A80002); // 192.168.0.2
            pTCP->seq = htonl(m_nTxSeq);
            if (m_u8Flags & TH_ACK)
            {
                pTCP->ack_seq = htonl(m_nRxNext);
            }
            if(m_nData)
            {
                m_nTxNext += (uint32_t)m_nData;
            }
            else
            {
                m_nTxNext++;
            }

        }
        else
        {
            m_nRxSeq = m_nRxNext;

            pIP->saddr = htonl(0xC0A80002); // 192.168.0.2
            pIP->daddr = htonl(0xC0A80001); // 192.168.0.1
            pTCP->seq = htonl(m_nRxSeq);
            if (m_u8Flags & TH_ACK)
            {
                pTCP->ack_seq = htonl(m_nTxNext);
            }
            if(m_nData)
            {
                m_nRxNext += (uint32_t)m_nData;
            }
            else
            {
                m_nRxNext ++;
            }
        }
        pIP->check = htons(IPCheckSum(pIP));

        // build TCP header
        if (eStateSend == m_eState)
        {
            pTCP->source = htons(0xA55A);
            pTCP->dest = htons((uint16_t)m_nPort);
        }
        else
        {
            pTCP->source = htons((uint16_t)m_nPort);
            pTCP->dest = htons(0xA55A);
        }
        pTCP->doff = sizeof(*pTCP) >> 2;
        pTCP->fin = (m_u8Flags & TH_FIN) ? 1 : 0;
        pTCP->syn = (m_u8Flags & TH_SYN) ? 1 : 0;
        pTCP->rst = (m_u8Flags & TH_RST) ? 1 : 0;
        pTCP->psh = (m_u8Flags & TH_PUSH) ? 1 : 0;
        pTCP->ack = (m_u8Flags & TH_ACK) ? 1 : 0;
        pTCP->urg = (m_u8Flags & TH_URG) ? 1 : 0;
        pTCP->window = htons(CAPTURE_MTU);
        pTCP->urg_ptr = htons(0);

        if(m_nData)
        {
            memcpy(pTemp, m_pData, m_nData);
        }
        pTCP->check = htons(TCPCheckSum(pIP, pTCP, pTemp, m_nData));

        pcap_dump((u_char *)m_pDumper, &tHeader, pPacket);
    }
    m_eState = eStateInit;
    m_nData = 0;
}

/*!
 * @fn          uint16_t CCapture::IPCheckSum (const struct iphdr *pIP)
 * @brief       calculate IP header checksum
 * @param[in]   pIP
 *              pointer to IP header
 * @return      checksum
 */
uint16_t CCapture::IPCheckSum (const struct iphdr *pIP)
{
    uint16_t *pTemp = (uint16_t*)(void*)pIP;
    uint32_t u32CheckSum = 0;
    size_t i;

    for (i = 0; i < (sizeof(*pIP) >> 1); i++)
    {
        if (i != (offsetof(iphdr, check) >> 1))
        {
            u32CheckSum += ntohs (pTemp[i]);
        }
    }

    return (uint16_t)~((u32CheckSum & 0xFFFF) + (u32CheckSum >> 16));
}

/*!
 * @fn          uint16_t CCapture::TCPCheckSum (const struct iphdr *pIP,
 *                      const struct tcphdr *pTCP,
 *                       const void *pData, size_t nData)
 * @brief       calculate TCP header checksum
 * @details     calculate checksum over TCP pseudo header and data
 * @param[in]   pIP
 *              pointer to IP header
 * @param[in]   pTCP
 *              pointer to TCP header
 * @param[in]   pData
 *              data pointer
 * @param[in]   nData
 *              data size
 * @return      checksum
 */
uint16_t CCapture::TCPCheckSum (const struct iphdr *pIP, const struct tcphdr *pTCP,
        const void *pData, size_t nData)
{
    uint16_t *pTemp = (uint16_t*)(void*)pIP;
    uint32_t u32CheckSum = 0;
    size_t i;

    for (i = (offsetof(iphdr, saddr) >> 1);
            i < ((offsetof(iphdr, daddr) +sizeof(pIP->daddr)) >> 1); i++)
    {
        u32CheckSum += ntohs (pTemp[i]);
    }

    u32CheckSum += pIP->protocol;
    u32CheckSum += (uint32_t)(sizeof(*pTCP) + nData);

    pTemp = (uint16_t*)(void*)pTCP;
    for (i = 0; i < (sizeof(*pTCP) >> 1); i++)
    {
        u32CheckSum += ntohs (pTemp[i]);
    }

    pTemp = (uint16_t*)pData;
    if (nData & 1) nData++;
    for (i = 0; i < (nData >> 1); i++)
    {
        if (i != (offsetof(tcphdr, check) >> 1))
        {
            u32CheckSum += ntohs (pTemp[i]);
        }
    }

    return (uint16_t)~((u32CheckSum & 0xFFFF) + (u32CheckSum >> 16));
}

/*!
 * @fn          void CCapture::Send (const void *pData, size_t nData)
 * @brief       write send packet
 * @param[in]   pData
 *              data pointer
 * @param[in]   nData
 *              data size
 */
void CCapture::Send (const void *pData, size_t nData)
{
    Dump(eStateSend, TH_PUSH|TH_ACK, pData, nData);
}

/*!
 * @fn          void CCapture::Send (const char *strData)
 * @brief       write send packet
 * @param[in]   strData
 *              package data (null teminated string)
 */
void CCapture::Send (const char *strData)
{
    Dump(eStateSend, TH_PUSH|TH_ACK, strData, strlen(strData));
    (void)strData;
}

/*!
 * @fn          void CCapture::Receive (const void *pData, size_t nData)
 * @brief       write receive packet
 * @param[in]   pData
 *              data pointer
 * @param[in]   nData
 *              data size
 */
void CCapture::Receive (const void *pData, size_t nData)
{
    Dump(eStateReceive, TH_PUSH|TH_ACK, pData, nData);
}

/*!
 * @fn          void CCapture::Receive (const char *strData)
 * @brief       write receive packet
 * @param[in]   strData
 *              package data (null teminated string)
 */
void CCapture::Receive (const char *strData)
{
    Dump(eStateReceive, TH_PUSH|TH_ACK, strData, strlen(strData));
}

//_oOo_
