/*!
 * @file        basis.cpp
 * @brief       basic class implementation
 * @details     string and tone classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

#include "basis.h"

#ifdef WITH_LOCALE
/*!
 * @var         CString::g_tLocale
 * @brief       global locale for numeric conversion
 */
locale_t CString::g_tLocale = NULL;

/*!
 * @fn          void CString::InitLocale (void)
 * @brief       init locale for numeric conversion
 * @details     to be called from CRun
 */
void CString::InitLocale (void)
{
    if (!g_tLocale)
    {
#ifdef _WIN32
        g_tLocale = _create_locale (LC_NUMERIC, "C");
#else // _WIN32
        g_tLocale = newlocale (LC_NUMERIC_MASK, "C", NULL);
#endif // _WIN32
    }
}

/*!
 * @fn          void CString::FreeLocale (void)
 * @brief       free locale for numeric conversion
 * @details     to be called from CRun
 */
void CString::FreeLocale (void)
{
    if (g_tLocale)
    {
        freelocale (g_tLocale);
        g_tLocale = NULL;
    }
}
#endif // WITH_LOCALE

/*!
 * @fn          CString::CString (size_t nSize)
 * @brief       constructor
 * @details     creates string of length
 * @param[in]   nSize
 *              string size
 */
CString::CString (size_t nSize)
: m_strString (NULL)
, m_nSize (nSize)
{
    if (m_nSize)
    {
        m_strString = new char[nSize];
    }
    if (!m_strString)
    {
        m_nSize = 0;
    }
}

/*!
 * @fn          CString::CString (const char* strString)
 * @brief       constructor
 * @details     creates string from char pointer
 * @param[in]   strString
 *              source string
 */
CString::CString (const char* strString)
: m_strString (NULL)
, m_nSize (0)
{
    if (strString)
    {
        m_nSize = strlen (strString) + 1;
        m_strString = new char[m_nSize];

        if (m_strString)
        {
            strncpy (m_strString, strString, m_nSize);
        }
        else
        {
            m_nSize = 0;
        }
    }
}

/*!
 * @fn          CString::CString (const char* strString, size_t nLength)
 * @brief       constructor
 * @details     creates string from char pointer with length
 * @param[in]   strString
 *              source string
 * @param[in]   nLength
 *              string length
 */
CString::CString (const char* strString, size_t nLength)
: m_strString (NULL)
, m_nSize (nLength + 1)
{
    if (m_nSize)
    {
        m_strString = new char[m_nSize];
    }
    if (m_strString)
    {
        strncpy (m_strString, strString, nLength);
        m_strString[nLength] = '\0';
    }
    else
    {
        m_nSize = 0;
    }
}

/*!
 * @fn          CString::~CString (void)
 * @brief       destructor
 * @details     frees memory
 */
CString::~CString (void)
{
    if (m_strString)
    {
        delete [] m_strString;
    }
}

/*!
 * @fn          CString& CString::operator = (const CString& cString)
 * @brief       asignmemt operator
 * @param[in]   cString
 *              source string
 * @return      assigned string
 */
CString& CString::operator = (const CString& cString)
{
    if (&cString != this)
    {
        if (m_strString)
        {
            delete [] m_strString;
        }

        if (cString.m_strString)
        {
            m_nSize = cString.m_nSize;
            m_strString = new char [m_nSize];

            if (m_strString)
            {
                strncpy (m_strString, cString.m_strString, m_nSize);
            }
            else
            {
                m_nSize = 0;
            }
        }
        else
        {
            m_nSize = 0;
            m_strString = NULL;
        }
    }
    return *this;
}

/*!
 * @fn          CString& CString::operator = (const char *strString)
 * @brief       asignmemt operator
 * @param[in]   strString
 *              source string
 * @return      assigned string
 */
CString& CString::operator = (const char *strString)
{
    if (m_strString)
    {
        delete [] m_strString;
    }

    if (strString)
    {
        m_nSize = strlen (strString) + 1;
        m_strString = new char [m_nSize];

        if (m_strString)
        {
            strncpy (m_strString, strString, m_nSize);
        }
        else
        {
            m_nSize = 0;
        }
    }
    else
    {
        m_nSize = 0;
        m_strString = NULL;
    }

    return *this;
}

/*!
 * @fn          CString& CString::operator += (const CString& cString)
 * @brief       concat operator
 * @param[in]   cString
 *              string to append
 * @return      concatenated string
 */
CString& CString::operator += (const CString& cString)
{
    char *strTemp = m_strString;
    size_t nTemp = m_nSize;

    if (cString.m_strString)
    {
        m_nSize =  (m_nSize) ? m_nSize : 1;
        if (cString.m_nSize > 0)
            m_nSize += cString.m_nSize - 1;
        m_strString = new char [m_nSize];

        if (m_strString)
        {
            m_strString[0] = '\0';
            if (strTemp)
            {
                strncpy (m_strString, strTemp, nTemp);
            }
            strncat (m_strString, cString.m_strString, m_nSize);
        }
        else
        {
            m_nSize = 0;
        }
    }
    else
    {
        strTemp = NULL;
    }

    if (strTemp)
    {
        delete [] strTemp;
    }
    return *this;
}

/*!
 * @fn          CString& CString::operator += (const char *strString)
 * @brief       concat operator
 * @param[in]   strString
 *              string to append
 * @return      concatenated string
 */
CString& CString::operator += (const char *strString)
{
    char *strTemp = m_strString;
    size_t nTemp = m_nSize;

    if (strString)
    {
        m_nSize =  (m_nSize) ? m_nSize : 1;
        m_nSize += strlen (strString);
        m_strString = new char [m_nSize];
        if (m_strString)
        {
            m_strString[0] = '\0';
            if (strTemp)
            {
                strncpy (m_strString, strTemp, nTemp);
            }
            strncat (m_strString, strString, m_nSize);
        }
        else
        {
            m_nSize = 0;
        }
    }
    else
    {
        strTemp = NULL;
    }

    if (strTemp)
    {
        delete [] strTemp;
    }
    return *this;
}

/*!
 * @fn          CString::operator const char *(void)
 * @brief       cast operator
 * @return      char pointer
 */
CString::operator const char *(void) const
{
    return m_strString;
}

/*!
 * @fn          CString& CString::Format (const char *strFormat, ...)
 * @brief       format string with variable arguments
 * @details     like printf
 * @param[in]   strFormat
 *              format string
 * @return      formated string
 */
CString& CString::Format (const char *strFormat, ...)
{
    va_list vaArgs;

    if (m_strString)
    {
        delete [] m_strString;
        m_strString = NULL;
    }

    va_start (vaArgs, strFormat);
    m_nSize = (size_t)vsnprintf (NULL, 0, strFormat, vaArgs) + 1;
    va_end (vaArgs);

    if (m_nSize)
    {
        m_strString = new char [m_nSize];
    }
    if (m_strString)
    {
        va_start (vaArgs, strFormat);
        vsnprintf (m_strString, m_nSize, strFormat, vaArgs);
        va_end (vaArgs);
    }
    else
    {
        m_nSize = 0;
    }

    return *this;
}

/*!
 * @fn          CString& CString::Timestamp (void)
 * @brief       format string timestamp
 * @return      string with formated timestamp
 */
CString& CString::Timestamp (void)
{
    time_t tTime;
    struct tm * tInfo;

    if (m_strString)
    {
        delete [] m_strString;
        m_strString = NULL;
    }
    m_nSize = 16;
    m_strString = new char [m_nSize];

    time (&tTime);
    tInfo = localtime (&tTime);

    strftime (m_strString, m_nSize, "%Y%m%d-%H%M%S", tInfo);

    return *this;
}

/*!
 * @fn          CString& CString::Printable (void)
 * @brief       make string printable
 * @details     replaces all unprintable chars by '.'
 * @return      replaced string
 */
CString& CString::Printable (void)
{
    size_t nIndex;

    for (nIndex = 0; nIndex < m_nSize; nIndex++)
    {
        if (!isprint (m_strString[nIndex]))
        {
            m_strString[nIndex] = '.';
        }
    }

    return *this;
}

/*!
 * @fn          CString& CString::Lower (void)
 * @brief       convert to lower case
 * @return      upper case string
 */
CString& CString::Lower (void)
{
    size_t nIndex;

    for (nIndex = 0; nIndex < m_nSize; nIndex++)
    {
        m_strString[nIndex] = (char)tolower (m_strString[nIndex]);
    }

    return *this;
}

/*!
 * @fn          CString& CString::Upper (void)
 * @brief       convert to upper case
 * @return      upper case string
 */
CString& CString::Upper (void)
{
    size_t nIndex;

    for (nIndex = 0; nIndex < m_nSize; nIndex++)
    {
        m_strString[nIndex] = (char)toupper (m_strString[nIndex]);
    }

    return *this;
}

/*!
 * @fn          long CString::Int (int nBase) const
 * @brief       convert string to integer
 * @param[in]   nBase
 *              treat string as based (8: octal, 10: dezimal, 16: hexadezimal)
 * @return      integer value
 */
long CString::Int (int nBase) const
{
    long nValue = 0;

    if (m_strString)
    {
#ifdef WITH_LOCALE
        nValue = strtol_l (m_strString, NULL, nBase, g_tLocale);
#else // WITH_LOCALE
        nValue = strtol (m_strString, NULL, nBase);
#endif // WITH_LOCALE

    }
    return nValue;
}

/*!
 * @fn          unsigned long CString::UInt (int nBase) const
 * @brief       convert string to unsigned integer
 * @param[in]   nBase
 *              treat string as based (8: octal, 10: dezimal, 16: hexadezimal)
 * @return      integer value
 */
unsigned long CString::UInt (int nBase) const
{
    unsigned long nValue = 0;

    if (m_strString)
    {
#ifdef WITH_LOCALE
        nValue = strtoul_l (m_strString, NULL, nBase, g_tLocale);
#else // WITH_LOCALE
        nValue = strtoul (m_strString, NULL, nBase);
#endif // WITH_LOCALE
    }
    return nValue;
}

/*!
 * @fn          double CString::Double (void) const
 * @brief       convert string to double
 * @return      double value
 */
double CString::Double (void) const
{
    double fValue = 0.0;
    if (m_strString)
    {
#ifdef WITH_LOCALE
        fValue = strtod_l (m_strString, NULL, g_tLocale);
#else // WITH_LOCALE
        fValue = strtod (m_strString, NULL);
#endif // WITH_LOCALE
    }
    return fValue;
}

/*!
 * @fn          size_t CString::Length (void) const
 * @brief       get string length
 * @return      string length
 */
size_t CString::Length (void) const
{
    size_t nLength = 0;

    if (this)
    {
        if (m_nSize)
        {
            nLength = m_nSize - 1;
        }
    }
    return nLength;
}

/*!
 * @fn          bool CString::Compare (const char *strCompare) const
 * @brief       compare string
 * @param[in]   strCompare
 *              string to compare with
 * @return      result
 * @retval      true
 *              equal
 * @retval      false
 *              not equal
 */
bool CString::Compare (const char *strCompare) const
{
    bool bRet = false;

    if (this)
    {
        if (m_nSize)
        {
            bRet = (0 == strncmp (m_strString, strCompare, m_nSize -1));
        }
    }

    return bRet;
}

/*!
 * @fn          bool CString::CaseCompare (const char *strCompare) const
 * @brief       compare string
 * @param[in]   strCompare
 *              string to compare with
 * @return      result
 * @retval      true
 *              equal
 * @retval      false
 *              not equal
 */
bool CString::CaseCompare (const char *strCompare) const
{
    bool bRet = false;

    if (this)
    {
        if (m_nSize)
        {
            bRet = (0 == strncasecmp (m_strString, strCompare, m_nSize -1));
        }
    }

    return bRet;
}

/*!
 * @fn          CTone::CTone (void)
 * @brief       default constructor
 * @details     empty array
 */
CTone::CTone (void)
: m_pnTones (NULL)
, m_nSize (0)
, m_nIndex (0)
{
}

/*!
 * @fn          CTone::CTone (size_t nSize)
 * @brief       constructor
 * @details     allocate and initialize array
 * @param[in]   nSize
 *              array size
 */
CTone::CTone (size_t nSize)
: m_pnTones (NULL)
, m_nSize (nSize)
, m_nIndex (0)
{
    if (m_nSize)
    {
        m_pnTones = new int [m_nSize];
    }
    Init ();
}

/*!
 * @fn          CTone::~CTone (void)
 * @brief       destructor
 * @details     frees memory
 */
CTone::~CTone (void)
{
    if (m_pnTones)
    {
        delete [] m_pnTones;
    }
}

/*!
 * @fn          int CTone::operator [] (size_t nIndex) const
 * @brief       index operator
 * @param[in]   nIndex
 *              index
 * @return      array value
 */
int CTone::operator [] (size_t nIndex) const
{
    return (m_pnTones && (nIndex < m_nSize)) ? m_pnTones [nIndex] : 0;
}

/*!
 * @fn          void CTone::Init (void)
 * @brief       initialize array
 * @details     sets array values to 0
 */
void CTone::Init (void)
{
    if (m_pnTones)
    {
        for (size_t i = 0; i < m_nSize; i++)
        {
            m_pnTones[i] = 0;
        }
    }
    else
    {
        m_nSize = 0;
    }
    m_nIndex = 0;
}

/*!
 * @fn          void CTone::Scale (int nAdd, int nMul, int nDiv, int nBits)
 * @brief       scale array
 * @details     tone = ((tone + nAdd) * nMul) / nDiv
 * @param[in]   nAdd
 *              offset
 * @param[in]   nMul
 *              multiplier
 * @param[in]   nDiv
 *              divisor
 * @param[in]   nBits
 *              sign expand from bit n (not used when 0)
 */
void CTone::Scale (int nAdd, int nMul, int nDiv, int nBits)
{
    if (m_pnTones)
    {
        if (!nDiv)
        {
            nDiv = 1;
        }

        for (size_t i = 0; i < m_nSize; i++)
        {
            if (nBits)
            {
                if (m_pnTones[i] & (0x1 << (nBits - 1)))
                {
                    m_pnTones[i] |= (~0x1 << (nBits - 1));
                }
            }

            m_pnTones[i] = ((abs (m_pnTones[i]) + nAdd) * nMul) / nDiv;
        }
    }
}

/*!
 * @fn          void CTone::Size (size_t nSize)
 * @brief       set array size
 * @details     reallocate and initialize array
 * @param[in]   nSize
 *              array size
 */
void CTone::Size (size_t nSize)
{
    if (m_nSize != nSize)
    {
        m_nSize = nSize;

        if (m_pnTones)
        {
            delete[] m_pnTones;
        }

        if (m_nSize)
        {
            m_pnTones = new int[m_nSize];
        }
    }
    Init ();
}

/*!
 * @fn          void CTone::Index (size_t nIndex)
 * @brief       set index
 * @note        this function is not thread safe
 * @param[in]   nIndex
 */
void CTone::Index (size_t nIndex)
{
    m_nIndex = (nIndex < m_nSize) ? nIndex : 0;
}

/*!
 * @fn          bool CTone::Set (CTone &cSource, size_t nMin, size_t nMax)
 * @brief       set array values
 * @details     copy partially
 * @param[in]   cSource
 *              source array
 * @param[in]   nMin
 *              min index
 * @param[in]   nMax
 *              max index
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
bool CTone::Set (CTone &cSource, size_t nMin, size_t nMax)
{
    bool bRet = false;

    if (m_nSize == cSource.m_nSize)
    {
        if (m_pnTones && cSource.m_pnTones)
        {
            if ((0 == nMax) || (nMax > m_nSize))
            {
                nMax = m_nSize;
            }

            for (size_t i = nMin; i < nMax; i++)
            {
                m_pnTones[i] = cSource.m_pnTones[i];
            }
            bRet = true;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CTone::Merge (CTone &cSource, size_t nMin, size_t nMax)
 * @brief       merge array values
 * @details     overwrite when value != 0
 * @param[in]   cSource
 *              source array
 * @param[in]   nMin
 *              min index
 * @param[in]   nMax
 *              max index
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
bool CTone::Merge (CTone &cSource, size_t nMin, size_t nMax)
{
    bool bRet = false;

    if (m_nSize == cSource.m_nSize)
    {
        if (m_pnTones && cSource.m_pnTones)
        {
            if ((0 == nMax) || (nMax > m_nSize))
            {
                nMax = m_nSize;
            }

            for (size_t i = nMin; i < nMax; i++)
            {
                if (0 != cSource.m_pnTones[i])
                {
                    m_pnTones[i] = cSource.m_pnTones[i];
                }
            }
            bRet = true;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CTone::Set (int *pnSource, size_t nSize)
 * @brief       set values
 * @details     copy values
 * @param[in]   pnSource
 *              source array
 * @param[in]   nSize
 *              array size
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
bool CTone::Set (int *pnSource, size_t nSize)
{
    bool bRet = false;

    if (!pnSource)
    {
        nSize = 0;
    }

    if (m_nSize == nSize)
    {

        if (m_pnTones && pnSource)
        {
            for (size_t i = 0; i < m_nSize; i++)
            {
                m_pnTones[i] = pnSource[i];
            }
            bRet = true;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CTone::Set (size_t nIndex, int nTone)
 * @brief       set single array value
 * @param[in]   nIndex
 *              array index
 * @param[in]   nTone
 *              new value
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
bool CTone::Set (size_t nIndex, int nTone)
{
    Index (nIndex);
    return SetInc (nTone);
}

/*!
 * @fn          bool CTone::SetInc (int nTone)
 * @brief       set with auto increment
 * @param[in]   nTone
 *              new value
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
bool CTone::SetInc (int nTone)
{
    bool bRet = false;
    if (m_pnTones && (m_nIndex < m_nSize))
    {
        m_pnTones[m_nIndex] = nTone;
        m_nIndex++;
        bRet = true;
    }
    return bRet;
}

//_oOo_
