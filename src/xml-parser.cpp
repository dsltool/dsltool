/*!
 * @file        xml-parser.cpp
 * @brief       XML parser class implementation
 * @details     DOM and SAX2 parser
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        05.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>
#include "xml-parser.h"
#include "xml-context.h"
#include "log.h"

/*!
 * @fn          CXmlParser::CXmlParser (CModem *pModem)
 * @brief       constructor
 * @param[in]   pModem
 *              modem pointer
 */
CXmlParser::CXmlParser (CModem *pModem)
: CParser (pModem)
, m_bHTML (false)
, m_pContext (NULL)
, m_pNext (this)
{
}

/*!
 * @fn          CXmlParser::~CXmlParser (void)
 * @brief       destructor
 * @details     close parser
 */
CXmlParser::~CXmlParser (void)
{
    Close ();
}

/*!
 * @fn          CParser *CXmlParser::Close (void)
 * @brief       close parser context
 * @return      next parser
 */
CParser *CXmlParser::Close (void)
{
    if (m_pContext)
    {
        delete m_pContext;
        m_pContext = NULL;
    }

    return m_pNext;
}

/*!
 * @fn          CParser *CXmlParser::Parse (const char *strRecv, size_t nRecv)
 * @brief       parse received data
 * @details     pass to XML context
 * @param[in]   strRecv
 *              receive data
 * @param[in]   nRecv
 *              receive data length
 * @return      next parser
 */
CParser *CXmlParser::Parse (const char *strRecv, size_t nRecv)
{
    if (m_pContext)
    {
        m_pContext->Parse (strRecv, nRecv);
    }

    return m_pNext;
}

/*!
 * @fn          CSaxParser::CSaxParser (CModem *pModem)
 * @brief       constructor
 * @param[in]   pModem
 *              modem pointer
 */
CSaxParser::CSaxParser (CModem *pModem)
: CXmlParser (pModem)
{
}

/*!
 * @fn          void CSaxParser::Open (EContent eContent)
 * @brief       open parser
 * @param[in]   eContent
 *              content type
 * @details     create XML context
 */
void CSaxParser::Open (EContent eContent)
{
    Close ();
    switch (eContent)
    {
        case eTextXml:
            m_pContext = new CSaxContext (this, false);
            break;
        case eTextHtml:
            m_pContext = new CSaxContext (this, true);
            break;
        default:
            break;
    }
}

/*!
 * @fn          void CSaxParser::StartDocument (void)
 * @brief       start document callback
 * @details     call line parser
 */
void CSaxParser::StartDocument (void)
{
    m_pNext = ParseLine ("X:");
}

/*!
 * @fn          void CSaxParser::EndDocument (void)
 * @brief       start document callback
 * @details     call line parser
 */
void CSaxParser::EndDocument (void)
{
    m_pNext = ParseLine ("/X:");
}

/*!
 * @fn          void CSaxParser::StartElement (const char *strElement)
 * @brief       start element callback
 * @details     call line parser
 * @param[in]   strElement
 *              element name
 */
void CSaxParser::StartElement (const char *strElement)
{
    CString strLine;
    strLine.Format ("E:%s", strElement);
    m_pNext = ParseLine (strLine);
}

/*!
 * @fn          void CSaxParser::EndElement (const char *strElement)
 * @brief       end element callback
 * @details     call line parser
 * @param[in]   strElement
 *              element name
 */
void CSaxParser::EndElement (const char *strElement)
{
    CString strLine;
    strLine.Format ("/E:%s", strElement);
    m_pNext = ParseLine (strLine);
}

/*!
 * @fn          void CSaxParser::Attribute (const char *strAttribute,
 *                      const char *strValue)
 * @brief       attribute callback
 * @details     call line parser
 * @param[in]   strAttribute
 *              attribute name
 * @param[in]   strValue
 *              attribute value
 */
void CSaxParser::Attribute (const char *strAttribute,
        const char *strValue)
{
    CString strLine;
    strLine.Format ("A:%s=%s", strAttribute, strValue);
    m_pNext = ParseLine (strLine);
}

/*!
 * @fn          void CSaxParser::Data (const char *strData)
 * @brief       data callback
 * @details     call line parser
 * @param[in]   strData
 *              data
 */
void CSaxParser::Data (const char *strData)
{
    CString strLine;
    strLine.Format ("D:%s", strData);
    m_pNext = ParseLine (strLine);
}

/*!
 * @fn          CDomParser::CDomParser (CModem *pModem)
 * @brief       constructor
 * @param[in]   pModem
 *              modem pointer
 */
CDomParser::CDomParser (CModem *pModem)
: CXmlParser (pModem)
{
}

/*!
 * @fn          void CDomParser::Open (EContent eContent)
 * @brief       open parser
 * @param[in]   eContent
 *              content type
 * @details     create XML context
 */
void CDomParser::Open (EContent eContent)
{
    Close ();
    switch (eContent)
    {
        case eTextXml:
            m_pContext = new CDomContext (this, false);
            break;
        case eTextHtml:
            m_pContext = new CDomContext (this, true);
            break;
        default:
            break;
    }
}

/*!
 * @fn          CParser *CDomParser::Close (void)
 * @brief       close parser context
 * @details     call @ref Finished function
 * @return      next parser
 */
CParser *CDomParser::Close (void)
{
    if (m_pContext)
    {
        m_pNext = Finished ();
    }

    return CXmlParser::Close ();
}

/*!
 * @fn          bool CDomParser::XPathQuery (long &nResult, const char *strXPath)
 * @brief       XPathQuery query long value
 * @param[out]  nResult
 *              value
 * @param[in]   strXPath
 *              query
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      true
 *              query failed
 */
bool CDomParser::XPathQuery (long &nResult, const char *strXPath)
{
    bool bRet = false;

    if (m_pContext)
    {
        bRet = m_pContext->XPathQuery (nResult, (const xmlChar*)strXPath);
    }

    return bRet;
}

/*!
 * @fn          bool CDomParser::XPathQuery (double &fResult, const char *strXPath)
 * @brief       XPathQuery query double value
 * @param[out]  fResult
 *              value
 * @param[in]   strXPath
 *              query
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      true
 *              query failed
 */
bool CDomParser::XPathQuery (double &fResult, const char *strXPath)
{
    bool bRet = false;

    if (m_pContext)
    {
        bRet = m_pContext->XPathQuery (fResult, (const xmlChar*)strXPath);
    }

    return bRet;
}

/*!
 * @fn          bool CDomParser::XPathQuery (CString &strResult, const char *strXPath)
 * @brief       XPath query string value
 * @param[out]  strResult
 *              value
 * @param[in]   strXPath
 *              query
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      true
 *              query failed
 */
bool CDomParser::XPathQuery (CString &strResult, const char *strXPath)
{
    bool bRet = false;

    if (m_pContext)
    {
        bRet = m_pContext->XPathQuery (strResult, (const xmlChar*)strXPath);
    }

    return bRet;
}

/*!
 * @fn          void CDomParser::XPathNamespace (const char *strPrefix,
 *                      const char *strURI)
 * @brief       add XPathQuery namespace
 * @param[in]   strPrefix
 *              namespace prefix
 * @param[in]   strURI
 *              namespace URI
 */
void CDomParser::XPathNamespace (const char *strPrefix,
        const char *strURI)
{
    if (m_pContext)
    {
        m_pContext->XPathNamespace ((const xmlChar*)strPrefix, (const xmlChar*)strURI);
    }
}

/*!
 * @fn          CDomCmdParser::CDomCmdParser (CModem *pModem, const char *strCmd)
 * @brief       constructor
 * @param[in]   pModem
 *              modem pointer
 * @param[in]   strCmd
 *              command string
 */
CDomCmdParser::CDomCmdParser (CModem *pModem, const char *strCmd)
: CDomParser (pModem)
{
    pModem->Protocol ()->Send (strCmd);
    pModem->Protocol ()->Send (pModem->LF ());
}

//_oOo_
