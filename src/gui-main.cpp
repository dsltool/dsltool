/*!
 * @file        gui-main.cpp
 * @brief       main window class implementation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gui-main.h"
#include "gui-config.h"
#include "gui-info.h"
#include "dsltool-gui.h"
#include "image-qt.h"
#include "graph.h"

#include <QStatusBar>
#include <QShowEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QLibraryInfo>

/*!
 * @fn          CGuiMain::CGuiMain (CDslToolGui &cDslToolGui)
 * @brief       constructor
 * @details     init UI
 * @param[in]   cDslToolGui
 *              application
 */
CGuiMain::CGuiMain (CDslToolGui &cDslToolGui)
: QMainWindow ()
, m_cDslToolGui (cDslToolGui)
, m_cImgBitalloc ()
, m_cImgSNR ()
, m_cImgChar ()
, m_cTranslator ()
, m_cTranslatorQt ()
, m_uiMain ()
, m_eStatus (eNotConfigured)
, m_cStatus ()
{
    m_uiMain.setupUi (this);

    connect (m_uiMain.actionNew, SIGNAL(triggered ()), this, SLOT (OnNew ()));
    connect (m_uiMain.actionOpen, SIGNAL(triggered ()), this, SLOT (OnOpen ()));
    connect (m_uiMain.actionSave, SIGNAL(triggered ()), this, SLOT (OnSave ()));
    connect (m_uiMain.actionSaveAs, SIGNAL(triggered ()), this, SLOT (OnSaveAs ()));
    connect (m_uiMain.actionQuit, SIGNAL(triggered ()), this, SLOT (OnQuit ()));

    connect (m_uiMain.actionRefresh, SIGNAL(triggered ()), this, SLOT (OnRefresh ()));
    connect (m_uiMain.actionResync, SIGNAL(triggered ()), this, SLOT (OnResync ()));
    connect (m_uiMain.actionReboot, SIGNAL(triggered ()), this, SLOT (OnReboot ()));

    connect (m_uiMain.actionConfig, SIGNAL(triggered ()), this, SLOT (OnConfig()));

    connect (m_uiMain.actionHelp, SIGNAL(triggered ()), this, SLOT (OnHelp()));
    connect (m_uiMain.actionInfo , SIGNAL(triggered ()), this, SLOT (OnInfo()));

    statusBar()->addPermanentWidget(&m_cStatus, 200);

    m_uiMain.imgBitalloc->setPicture (m_cImgBitalloc);
    m_uiMain.imgSNR->setPicture (m_cImgSNR);
    m_uiMain.imgChar->setPicture (m_cImgChar);

    m_uiMain.tabMain->setTabEnabled (2, false);

    TranslateStatus ();
    Translate (QLocale::system().name());

    CreateLanguageMenu ();

    ResetFocus ();
    EnableMenu ();
}

/*!
 * @fn          void CGuiMain::closeEvent (QCloseEvent *pEvent)
 * @brief       close event handler
 * @details     ask to save changed config
 * @param[in,out] pEvent
 *              event pointer
 */
void CGuiMain::closeEvent (QCloseEvent *pEvent)
{
    bool bClose = true;

    if (!m_cDslToolGui.m_bConfigSaved)
    {
        switch  (QMessageBox::warning( this,
                tr ("dsltool - close"),
                tr("The configuration has been modified.\n"
                   "Do you want to save your changes?"),
                QMessageBox::Cancel | QMessageBox::Save | QMessageBox::Discard,
                QMessageBox::Save))
        {
        case QMessageBox::Cancel:
            bClose = false;
            break;
        case QMessageBox::Save:
            if (0 == m_cDslToolGui.m_cConfig.m_strConfig.Length ())
            {
                OnSaveAs ();
            }
            else
            {
                OnSave ();
            }
            break;
        default:
            break;
        }
    }

    if (bClose)
    {
        pEvent->accept ();
    }
    else
    {
        pEvent->ignore ();
    }
}

/*!
 * @fn          void CGuiMain::OnNew (void)
 * @brief       "new" menu entry selected
 * @details     reset config and show config dialog
 */
void CGuiMain::OnNew (void)
{
    m_cDslToolGui.m_cConfig.Reset ();
    m_cDslToolGui.m_cConfigList.Reset ();

    m_cDslToolGui.ConfigChanged (true);

    OnConfig ();
}

/*!
 * @fn          void CGuiMain::OnOpen (void)
 * @brief       "open" menu entry selected
 * @details     show open file dialog and load config file
 */
void CGuiMain::OnOpen (void)
{
    QString strPath = QFileDialog::getOpenFileName (this,
            tr ("Open"),
            QString (m_cDslToolGui.m_cConfig.m_strConfig),
            tr ("Config files (*.conf);;All files (*)"));

    if (!strPath.isEmpty ())
    {
        m_cDslToolGui.m_cConfig.Reset ();
        m_cDslToolGui.m_cConfigList.Reset ();

        m_cDslToolGui.m_cConfig.m_strConfig = strPath.toUtf8();

        m_cDslToolGui.ReadConfigData ();

        m_cDslToolGui.ConfigChanged (false);
    }
}

/*!
 * @fn          void CGuiMain::OnSave (void)
 * @brief       "save" menu entry selected
 * @details     save config file
 */
void CGuiMain::OnSave (void)
{
    if (m_cDslToolGui.m_cConfig.m_strConfig.Length ())
    {
        if (m_cDslToolGui.WriteConfigData ())
        {
            m_cDslToolGui.m_bConfigSaved = true;
        }
    }
    EnableMenu ();
}

/*!
 * @fn          void CGuiMain::OnSaveAs (void)
 * @brief       "save as" menu entry selected
 * @details     show save file dialog and save config file
 */
void CGuiMain::OnSaveAs (void)
{
    QString strPath = QFileDialog::getSaveFileName (this,
            tr ("Save"),
            QString (m_cDslToolGui.m_cConfig.m_strConfig),
            tr ("Config files (*.conf);;All files (*)"));

    if (!strPath.isEmpty ())
    {
        m_cDslToolGui.m_cConfig.m_strConfig = strPath.toUtf8();
        m_cDslToolGui.WriteConfigData ();
        if (m_cDslToolGui.WriteConfigData ())
        {
            m_cDslToolGui.m_bConfigSaved = true;
        }
    }

    EnableMenu ();
}

/*!
 * @fn          void CGuiMain::OnQuit (void)
 * @brief       "quit" menu entry selected
 * @details     close window
 */
void CGuiMain::OnQuit (void)
{
    close ();
}

/*!
 * @fn          void CGuiMain::OnRefresh (void)
 * @brief       "refresh" menu entry selected
 * @details     run modem info command
 */
void CGuiMain::OnRefresh (void)
{
    m_cDslToolGui.Refresh (CModem::eInfo);
}

/*!
 * @fn          void CGuiMain::OnResync (void)
 * @brief       "resync" menu entry selected
 * @details     run modem resync command
 */
void CGuiMain::OnResync (void)
{
    m_cDslToolGui.Refresh (CModem::eResync);
}

/*!
 * @fn          void CGuiMain::OnReboot (void)
 * @brief       "reboot" menu entry selected
 * @details     run modem reboot command
 */
void CGuiMain::OnReboot (void)
{
    m_cDslToolGui.Refresh (CModem::eReboot);
}

/*!
 * @fn          void CGuiMain::OnLanguage (QAction *pAction)
 * @brief       "language" menu entry selected
 * @details     retranslate UI
 * @param[in]   pAction
 *              selected language in data member
 */
void CGuiMain::OnLanguage (QAction *pAction)
{
    if (pAction)
    {
        Translate (pAction->data ().toString ());
    }
}

/*!
 * @fn          void CGuiMain::OnConfig (void)
 * @brief       "config" menu entry selected
 * @details     show config dialog
 */
void CGuiMain::OnConfig (void)
{
    CGuiConfig cConfig (this);

    cConfig.SetConfig (m_cDslToolGui.m_cConfig);
    if (QDialog::Accepted == cConfig.exec ())
    {
        if (cConfig.GetConfig (m_cDslToolGui.m_cConfig, m_cDslToolGui.m_cConfigList))
        {
            m_cDslToolGui.ConfigChanged (true);
        }

    }
}

/*!
 * @fn          void CGuiMain::OnHelp (void)
 * @brief       "help" menu entry selected
 * @details     show help text
 */
void CGuiMain::OnHelp (void)
{
    //! @todo
}

/*!
 * @fn          void CGuiMain::OnInfo (void)
 * @brief       "about" menu entry selected
 * @details     show info dialog
 */
void CGuiMain::OnInfo (void)
{
    CGuiInfo cInfo (this);

    cInfo.exec ();
}

/*!
 * @fn          void CGuiMain::ResetFocus (void)
 * @brief       reset focus
 * @details     set focus to first element on first tab
 */
void CGuiMain::ResetFocus (void)
{
    m_uiMain.tabSpectrumMain->setCurrentIndex (0);
    m_uiMain.tabMain->setCurrentIndex (0);
    m_uiMain.textDslType->setFocus ();
}

/*!
 * @fn          void CGuiMain::SetStatus (EStatus eStatus)
 * @brief       set status
 * @details     write to status line widget
 * @param[in]   eStatus
 *              new status value
 */
void CGuiMain::SetStatus (EStatus eStatus)
{
    m_eStatus = eStatus;

    TranslateStatus ();

    EnableMenu ();
}

/*!
 * @fn          void CGuiMain::EnableMenu (void)
 * @brief       enable/disable save menu entries
 */
void CGuiMain::EnableMenu (void)
{
    bool bEnable = ((eReady == m_eStatus)||(eRunning == m_eStatus));

    if ((0 < m_cDslToolGui.m_cConfig.m_strConfig.Length())
            && !m_cDslToolGui.m_bConfigSaved)
    {
        m_uiMain.actionSave->setEnabled (bEnable);
    }
    else
    {
        m_uiMain.actionSave->setEnabled (false);
    }

    m_uiMain.actionSaveAs->setEnabled (bEnable);

    m_uiMain.actionConfig->setEnabled (bEnable);
    m_uiMain.actionRefresh->setEnabled (bEnable);
    m_uiMain.actionResync->setEnabled (bEnable);
    m_uiMain.actionReboot->setEnabled (bEnable);
}

/*!
 * @fn          void CGuiMain::CreateLanguageMenu (void)
 * @brief       create language selection menu
 * @details     check for *.qm files and add menu entry for each found language
 */
void CGuiMain::CreateLanguageMenu (void)
{
    QString strPath = QApplication::applicationDirPath();
    QDir dirLang (strPath);
    QStringList astrLang = dirLang.entryList( QStringList("dsltool-*.qm"));
    QString strLang, strLocale;
    QAction *pAction;
    QActionGroup* pActionGroup = new QActionGroup(m_uiMain.menuLanguage);

    pActionGroup->setExclusive(true);
    connect(pActionGroup, SIGNAL (triggered(QAction *)), this, SLOT (OnLanguage (QAction *)));

    for (int nEntry = 0; nEntry < astrLang.size (); nEntry++)
    {
        strLocale = astrLang[nEntry];
        strLocale.truncate (strLocale.lastIndexOf ('.'));
        strLocale.remove(0, strLocale.indexOf('-') + 1);
        QLocale cLocale (strLocale);

        pAction = new QAction(cLocale.nativeLanguageName (), this);
        pAction->setCheckable(true);
        pAction->setChecked (0 == cLocale.name().compare(QLocale::system().name()));
        pAction->setData(strLocale);

        m_uiMain.menuLanguage->addAction(pAction);
        pActionGroup->addAction(pAction);
    }
}

/*!
 * @fn          void CGuiMain::Translate (const QString &strLocale)
 * @brief       translate UI
 * @details     load .qm files and start translation
 * @param[in]   strLocale
 */
void CGuiMain::Translate (const QString &strLocale)
{
    if (m_cTranslator.load (QString ("dsltool-%1").arg(strLocale),
            QApplication::applicationDirPath()))
    {
        qApp->installTranslator(&m_cTranslator);
    }

    if (m_cTranslatorQt.load (QString ("qt_%1.qm").arg(strLocale),
        QLibraryInfo::location (QLibraryInfo::TranslationsPath)))
    {
        qApp->installTranslator(&m_cTranslatorQt);
    }

    m_uiMain.retranslateUi (this);
    TranslateStatus ();
    m_cDslToolGui.Output ();
}

/*!
 * @fn          void CGuiMain::TranslateStatus (void)
 * @brief       set status line text to current language
 */
void CGuiMain::TranslateStatus (void)
{
    switch (m_eStatus)
    {
    case eNotConfigured:
        m_cStatus.setText (tr("not configured"));
        break;
    case eReady:
        m_cStatus.setText (tr("ready"));
        break;
    case eRunning:
        m_cStatus.setText (tr("running"));
        break;
    case eError:
        m_cStatus.setText (tr("error"));
        break;
    }
}

/*!
 * @fn          void CGuiMain::Write (const CDslData &cData)
 * @brief       write DSL data to window elements
 * @param[in]   cData
 *              DSL data
 */
void CGuiMain::Write (const CDslData &cData)
{
    bool bSpectrum = false;

    // DSL group
    Write (cData.m_cData.m_cTones.Bandplan ());
    Write (cData.m_cData.m_cModemStateStr,
            m_uiMain.textModemState);
    Write (cData.m_cData.m_cOperationMode,
            m_uiMain.textOperationMode);
    Write (cData.m_cData.m_cProfile,
            m_uiMain.textDslProfile);
    Write (cData.m_cData.m_cChannelMode,
            m_uiMain.textChannelMode);

    // ATM group
    Write (cData.m_cData.m_cAtm.m_cVpiVci,
            m_uiMain.textAtmVpi, m_uiMain.textAtmVci);

    // ATU group
    Write (cData.m_cData.m_cAtm.m_cATU_C,
            m_uiMain.textAtuCVendor, m_uiMain.textAtuCVendorSpec,
            m_uiMain.textAtuCRevision);
    Write (cData.m_cData.m_cAtm.m_cATU_R,
            m_uiMain.textAtuRVendor, m_uiMain.textAtuRVendorSpec,
            m_uiMain.textAtuRRevision);

    // failure group
    Write (cData.m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min,
            m_uiMain.textErrorSec15min);
    Write (cData.m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay,
            m_uiMain.textErrorSecDay);

    // bandwidth group
    Write (cData.m_cData.m_cBandwidth.m_cCells,
            m_uiMain.textBandCellsUp , m_uiMain.textBandCellsDown);
    Write (cData.m_cData.m_cBandwidth.m_cKbits,
            m_uiMain.textBandKBitUp , m_uiMain.textBandKBitDown);
    Write (cData.m_cData.m_cBandwidth.m_cMaxKbits,
            m_uiMain.textBandMaxKBitUp, m_uiMain.textBandMaxKBitDown);

    // line group
    Write (cData.m_cData.m_cNoiseMargin,
            m_uiMain.textNoisemarginUp, m_uiMain.textNoisemarginDown);
    Write (cData.m_cData.m_cAttenuation,
            m_uiMain.textAttenuationUp, m_uiMain.textAttenuationDown);
    Write (cData.m_cData.m_cTxPower,
            m_uiMain.textTxPowerUp, m_uiMain.textTxPowerDown);


    // error group
    Write (cData.m_cData.m_cStatistics.m_cError.m_cFEC,
            m_uiMain.textErrorFECTx, m_uiMain.textErrorFECRx);
    Write (cData.m_cData.m_cStatistics.m_cError.m_cCRC,
            m_uiMain.textErrorCRCTx, m_uiMain.textErrorCRCRx);
    Write (cData.m_cData.m_cStatistics.m_cError.m_cHEC,
            m_uiMain.textErrorHECTx, m_uiMain.textErrorHECRx);

    bSpectrum |= WriteBits (cData.m_cData.m_cTones);
    bSpectrum |= WriteSNR (cData.m_cData.m_cTones);
    bSpectrum |= WriteChar (cData.m_cData.m_cTones);

    m_uiMain.tabMain->setTabEnabled(1, bSpectrum);
}

/*!
 * @fn          void CGuiMain::Write (CBandplan *pBandplan)
 * @brief       write bandplan data
 * @param[in]   pBandplan
 *              bandplan data
 */
void CGuiMain::Write (CBandplan *pBandplan)
{
    m_uiMain.textDslType->setText(QString (pBandplan->Type ()));
}

/*!
 * @fn          void CGuiMain::Write (const CDataInt &cData,
 *                      QLineEdit *pEdit)
 * @brief       write integer data
 * @param[in]   cData
 *              integer data
 * @param[out]  pEdit
 *              output edit box
 */
void CGuiMain::Write (const CDataInt &cData,
        QLineEdit *pEdit)
{
    long nData;

    if (pEdit)
    {
        if (cData.Get (nData))
        {
            pEdit->setText (QString ("%1").arg (nData));
        }
        else
        {
            pEdit->setText (QString ());
        }
    }
}

/*!
 * @fn          void CGuiMain::Write (const CDataIntUpDown &cData,
 *                      QLineEdit *pEditUp,
 *                      QLineEdit *pEditDown)
 * @brief       write integer up/down data
 * @param[in]   cData
 *              integer up/down data
 * @param[out]  pEditUp
 *              upstream output edit box
 * @param[out]  pEditDown
 *              downstream output edit box
 */
void CGuiMain::Write (const CDataIntUpDown &cData,
        QLineEdit *pEditUp,
        QLineEdit *pEditDown)
{
    long nData;

    if (pEditUp)
    {
        if (cData.GetUp (nData))
        {
            pEditUp->setText (QString ("%1").arg (nData));
        }
        else
        {
            pEditUp->setText (QString ());
        }
    }
    if (pEditDown)
    {
        if (cData.GetDown (nData))
        {
            pEditDown->setText (QString ("%1").arg (nData));
        }
        else
        {
            pEditDown->setText (QString ());
        }
    }
}

/*!
 * @fn          void CGuiMain::Write (const CDataDouble &cData,
 *                      QLineEdit *pEdit)
 * @brief       write double data
 * @param[in]   cData
 *              double data
 * @param[out]  pEdit
 *              output edit box
 */
void CGuiMain::Write (const CDataDouble &cData,
        QLineEdit *pEdit)
{
    double fData;

    if (pEdit)
    {
        if (cData.Get (fData))
        {
            pEdit->setText (QString ("%1").arg (fData, 0, 'f', 1));
        }
        else
        {
            pEdit->setText (QString ());
        }
    }
}

/*!
 * @fn          void CGuiMain::Write (const CDataDoubleUpDown &cData,
 *                      QLineEdit *pEditUp,
 *                      QLineEdit *pEditDown)
 * @brief       write double up/down data
 * @param[in]   cData
 *              double up/down data
 * @param[out]  pEditUp
 *              upstream output edit box
 * @param[out]  pEditDown
 *              downstream output edit box
 */
void CGuiMain::Write (const CDataDoubleUpDown &cData,
        QLineEdit *pEditUp,
        QLineEdit *pEditDown)
{
    double fData;

    if (pEditUp)
    {
        if (cData.GetUp (fData))
        {
            pEditUp->setText (QString ("%1").arg (fData, 0, 'f', 1));
        }
        else
        {
            pEditUp->setText (QString ());
        }
    }
    if (pEditDown)
    {
        if (cData.GetDown (fData))
        {
            pEditDown->setText (QString ("%1").arg (fData, 0, 'f', 1));
        }
        else
        {
            pEditDown->setText (QString ());
        }
    }
}

/*!
 * @fn          void CGuiMain::Write (const CArrayDoubleUpDown &cData,
 *                      QLineEdit *pEditUp,
 *                      QLineEdit *pEditDown)
 * @brief       write double up/down array data
 * @param[in]   cData
 *              double up/down array data
 * @param[out]  pEditUp
 *              upstream output edit box for first index
 * @param[out]  pEditDown
 *              downstream output edit box for first index
 */
void CGuiMain::Write (const CArrayDoubleUpDown &cData,
        QLineEdit *pEditUp,
        QLineEdit *pEditDown)
{
    double fData;

    if (pEditUp)
    {
        if (cData.GetUp (0, fData))
        {
            pEditUp->setText ( QString ("%1").arg (fData, 0, 'f', 1));
        }
        else
        {
            pEditUp->setText (QString ());
        }
    }
    if (pEditDown)
    {
        if (cData.GetDown (0, fData))
        {
            pEditDown->setText ( QString ("%1").arg (fData, 0, 'f', 1));
        }
        else
        {
            pEditDown->setText (QString ());
        }
    }
}

/*!
 * @fn          void CGuiMain::Write (const CDataString &cData,
 *                      QLineEdit *pEdit)
 * @brief       write string data
 * @param[in]   cData
 *              string data
 * @param[out]  pEdit
 *              output edit box
 */
void CGuiMain::Write (const CDataString &cData,
        QLineEdit *pEdit)
{
    CString strData;

    if (pEdit)
    {
        if (cData.Get (strData))
        {
            pEdit->setText (QString (strData));
        }
        else
        {
            pEdit->setText (QString ());
        }
    }
}

/*!
 * @fn          void CGuiMain::Write (const CDataATU &cData,
 *                      QLineEdit *pEditVendor,
 *                      QLineEdit *pEditSpec,
 *                      QLineEdit *pEditRevision)
 * @brief       write ATU-X data
 * @param[in]   cData
 *              ATU-X data
 * @param[out]  pEditVendor
 *              vendor output edit box
 * @param[out]  pEditSpec
 *              spec output edit box
 * @param[out]  pEditRevision
 *              revision output edit box
 */
void CGuiMain::Write (const CDataATU &cData,
        QLineEdit *pEditVendor,
        QLineEdit *pEditSpec,
        QLineEdit *pEditRevision)
{
    CString strVendor;
    int nSpec0, nSpec1, nRevision;

    if (pEditVendor)
    {
        if (cData.GetVendor (strVendor))
        {
            pEditVendor->setText (QString (strVendor));
        }
        else
        {
            pEditVendor->setText (QString ());
        }
    }
    if (pEditSpec)
    {
        if (cData.GetSpec (nSpec0, nSpec1))
        {
            pEditSpec->setText (QString ("%1.%2").arg(nSpec0).arg(nSpec1));
        }
        else
        {
            pEditSpec->setText (QString ());
        }
    }
    if (pEditRevision)
    {
        if (cData.GetRevision (nRevision))
        {
            pEditRevision->setText (QString ("%1").arg(nRevision));
        }
        else
        {
            pEditRevision->setText (QString ());
        }
    }
}

/*!
 * @fn          bool CGuiMain::WriteBits (const CTonesGroup &cTones)
 * @brief       write bit alloc graph
 * @param[in]   cTones
 *              tones data group
 * @return      enabled
 * @retval      true
 *              graph enabled
 * @retval      false
 *              graph disabled
 */
bool CGuiMain::WriteBits (const CTonesGroup &cTones)
{
    bool bEnabled = false;
    CImageQt cImage (&m_cImgBitalloc);

    if (cTones.m_cBitallocUp.Valid () ||
        cTones.m_cBitallocDown.Valid () ||
        cTones.m_cGainQ2.Valid ())
    {
        CGraphBitalloc cGraph (cImage, cTones);
        cGraph.Draw ();
        bEnabled = true;
    }

    m_uiMain.tabSpectrumMain->setTabEnabled(0, bEnabled);

    return bEnabled;
}

/*!
 * @fn          bool CGuiMain::WriteSNR (const CTonesGroup &cTones)
 * @brief       write SNR graph
 * @param[in]   cTones
 *              tones data group
 * @return      enabled
 * @retval      true
 *              graph enabled
 * @retval      false
 *              graph disabled
 */
bool CGuiMain::WriteSNR (const CTonesGroup &cTones)
{
    bool bEnabled = false;
    CImageQt cImage (&m_cImgSNR);
    if (cTones.m_cSNR.Valid () ||
            cTones.m_cNoiseMargin.Valid ())
    {
        CGraphSNR cGraph (cImage, cTones);
        cGraph.Draw ();
        bEnabled = true;
    }

    m_uiMain.tabSpectrumMain->setTabEnabled(1, bEnabled);

    return bEnabled;
}

/*!
 * @fn          bool CGuiMain::WriteChar (const CTonesGroup &cTones)
 * @brief       write char graph
 * @param[in]   cTones
 *              tones data group
 * @return      enabled
 * @retval      true
 *              graph enabled
 * @retval      false
 *              graph disabled
 */
bool CGuiMain::WriteChar (const CTonesGroup &cTones)
{
    bool bEnabled = false;
    CImageQt cImage (&m_cImgChar);

    if (cTones.m_cChanCharLog.Valid ())
    {
        CGraphChar cGraph (cImage, cTones);
        cGraph.Draw ();
        bEnabled = true;
    }

    m_uiMain.tabSpectrumMain->setTabEnabled(2, bEnabled);

    return bEnabled;
}


/*!
 * @fn          CGuiOutput::CGuiOutput (CGuiMain &cGuiMain)
 * @brief       constructor
 * @param[in]   cGuiMain
 *              main window
 */
CGuiOutput::CGuiOutput (CGuiMain &cGuiMain)
: COutput ()
, m_cGuiMain (cGuiMain)
{
}

/*!
 * @fn          void CGuiOutput::Write (const CConfigData &cConfig,
 *                      const CDslData &cData,
 *                      bool bTones)
 * @brief       write DSL data to window
 * @details     call main window write function @ref CGuiMain::Write
 * @param[in]   cConfig
 *              unused
 * @param[in]   cData
 *              DSL data
 * @param[in]   bTones
 *              unused
 */
void CGuiOutput::Write (const CConfigData &cConfig,
        const CDslData &cData,
        bool bTones)
{
    (void)cConfig;
    (void)bTones;

    m_cGuiMain.Write (cData);
}

//_oOo_
