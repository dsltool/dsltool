/*!
 * @file        dynlib.cpp
 * @brief       dynamic library implementation
 * @details     dynamic library class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        03.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>
#include <dlfcn.h>
#include <limits.h>

#include "dynlib.h"
#include "log.h"

/*!
 * @fn          CDynLib::CDynLib (void)
 * @brief       constructor
 */
CDynLib::CDynLib (void)
: m_pHandle (NULL)
{
}

/*!
 * @fn          CDynLib::CDynLib (void)
 * @brief       destructor
 * @details     close library
 */
CDynLib::~CDynLib (void)
{
    Close ();
}

/*!
 * @fn          bool CDynLib::Open (const char *strName)
 * @brief       open library
 * @param[in]   strName
 *              library name
 * @return      success
 * @retval      true
 *              o.k.
 * @return      false
 *              failure
 */
bool CDynLib::Open (const char *strName)
{
    char strLib[PATH_MAX];

    snprintf (strLib, sizeof (strLib), "libdsltool-%s.so.%d.%d.%d",
            strName, VERSION_MAJOR, VERSION_MINOR, VERSION_SUB);

    m_pHandle = dlopen (strLib, RTLD_LAZY);

    if (!m_pHandle)
    {
        LogError ("dlopen (%s) failed: %s\n", strLib, dlerror ());
    }

    return (NULL != m_pHandle);
}

/*!
 * @fn          void CDynLib::Close (void)
 * @brief       close library
 */
void CDynLib::Close (void)
{
    if (m_pHandle)
    {
        dlclose (m_pHandle);
        m_pHandle = NULL;
    }
}

/*!
 * @fn          void *CDynLib::Symbol (const char *strSymbol)
 * @brief       find symbol
 * @param[in]   strSymbol
 *              symbol name
 * @return      symbol address
 * @return      NULL
 *              failure
 */
void *CDynLib::Symbol (const char *strSymbol)
{
    void *pSymbol = NULL;

    if (m_pHandle)
    {
        pSymbol = dlsym (m_pHandle, strSymbol);

        if (!pSymbol)
        {
            LogError ("dlsym (%s) failed: %s\n", strSymbol, dlerror ());
        }
    }

    return pSymbol;
}

//_oOo_
