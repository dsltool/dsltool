/*!
 * @file        dsltoold.cpp
 * @brief       application class implementation
 * @details     daemon execution classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "dsltoold.h"
#include "txt.h"

/*!
 * @fn          int main (int argc, char **argv)
 * @brief       program entry function
 * @details     creates and executes application object
 * @param[in]   argc
 *              number of arguments
 * @param[in]   argv
 *              arguments
 * @return      exit code
 */
int main (int argc, char **argv)
{
    CDslToolDaemon cDslTool;
    return cDslTool.Main (argc, argv);
}

/*!
 * @fn          CDslToolDaemon::CDslToolDaemon (void)
 * @brief       constructor
 */
CDslToolDaemon::CDslToolDaemon (void)
{
}

/*!
 * @fn          int CDslToolDaemon::Main (int nArgc, char **astrArgv)
 * @brief       application function
 * @details     check arguments and start @ref CRunOneShot or @ref CRunDaemon
 * @param       nArgc
 *              number of arguments
 * @param       astrArgv
 * @return      exit code
 * @retval      EXIT_SUCCESS
 *              success
 * @retval      EXIT_FAILURE
 *              failure
 */
int CDslToolDaemon::Main (int nArgc, char **astrArgv)
{
    int nRet = EXIT_FAILURE;

    Disable (CConfigList::eOutput);
    Disable (CConfigList::eCommand);

    do
    {
        if (!ReadConfigData (nArgc, astrArgv))
        {
            break;
        }

        if (!CheckConfigData ())
        {
            break;
        }

        if (!LoadOutput ())
        {
            break;
        }

        if (!LoadProtocol ())
        {
            break;
        }

        if (!LoadModem ())
        {
            break;
        }

        if (Run ())
        {
            nRet = EXIT_SUCCESS;
        }
    }
    while (false);

    if (PrintInfo (astrArgv[0]))
    {
        nRet = EXIT_SUCCESS;
    }

    CLog::g_cLog.Close ();

    return nRet;
}

//_oOo_
