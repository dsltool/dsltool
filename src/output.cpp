/*!
 * @file        output.cpp
 * @brief       output base class implementation
 * @details     abstract output and helper class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "output.h"
#include "txt.h"

/*!
 * @var         COutputFactory COutputFactory::g_cOutputFactory
 * @brief       singleton output factory object
 */
COutputFactory COutputFactory::g_cOutputFactory;

/*!
 * @fn          COutputFactory::COutputFactory (void)
 * @brief       constructor
 */
COutputFactory::COutputFactory (void)
{
    for (int nIndex = 0; nIndex < COutputLib::eIndexMax; nIndex++)
    {
        m_apfnVersion[nIndex] = NULL;
        m_apfnCreate[nIndex] = NULL;
    }
}

/*!
 * @fn          COutputFactory::~COutputFactory (void)
 * @brief       destructor
 * @details     unload output dynamic libraries
 */
COutputFactory::~COutputFactory (void)
{
    for (int nIndex = 0; nIndex < COutputLib::eIndexMax; nIndex++)
    {
        Unload (nIndex);
    }
}

/*!
 * @fn          bool COutputFactory::Load (int nIndex, const char *strOutput)
 * @brief       load output dynamic library
 * @param[in]   nIndex
 *              output library index
 * @param[in]   strOutput
 *              output name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to load library
 */
bool COutputFactory::Load (int nIndex, const char *strOutput)
{
    bool bRet = false;

    if ((0 <= nIndex) && (nIndex < COutputLib::eIndexMax))
    {
        CDynLib &cOutput = m_acOutput[nIndex];

        if (cOutput.Open (strOutput))
        {
            m_apfnVersion[nIndex] = (TpfnVersion) cOutput.Symbol ("OutputVersion");
            m_apfnCreate[nIndex] = (TpfnCreate) cOutput.Symbol ("OutputCreate");

            if (m_apfnVersion[nIndex] && m_apfnCreate[nIndex])
            {
                LogInfo (NStrInfo::LoadOutput, strOutput);

                bRet = true;
            }
            else
            {
                Unload (nIndex);
            }
        }
    }
    return bRet;
}

/*!
 * @fn          void COutputFactory::Unload (void)
 * @brief       unload output dynamic library
 * @param[in]   nIndex
 *              output library index
 */
void COutputFactory::Unload (int nIndex)
{
    if ((0 <= nIndex) && (nIndex < COutputLib::eIndexMax))
    {
        m_acOutput[nIndex].Close ();
        m_apfnVersion[nIndex] = NULL;
        m_apfnCreate[nIndex] = NULL;
    }
}

/*!
 * @fn          bool COutputFactory::TestVersion (const char *strOutput)
 * @brief       test version number
 * @details     calls @ref OutputVersion function in dynamic library
 * @param[in]   nIndex
 *              output library index
 * @param[in]   strOutput
 *              output name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              version missmatch
 */
bool COutputFactory::TestVersion (int nIndex, const char *strOutput)
{
    bool bRet = false;
    int nMajor = 0;
    int nMinor = 0;
    int nSub = 0;

    if ((0 <= nIndex) && (nIndex < COutputLib::eIndexMax))
    {
        if (m_apfnVersion[nIndex])
        {
            (m_apfnVersion[nIndex]) (nMajor, nMinor, nSub);

            if ((VERSION_MAJOR == nMajor) &&
                (VERSION_MINOR == nMinor) &&
                (VERSION_SUB == nSub))
            {
                bRet = true;
            }
        }
    }
    if (!bRet)
    {
        LogError (NStrErr::Version, strOutput, nMajor, nMinor, nSub,
                  VERSION_MAJOR, VERSION_MINOR, VERSION_SUB);
    }
    return bRet;
}

/*!
 * @fn          COutput *COutputFactory::Create (COutputLib::EIndex eIndex)
 * @brief       create output instance
 * @details     calls @ref OutputCreate function in dynamic library
 * @param[in]   nIndex
 *              output library index
 * @return      output pointer
 * @retval      NULL
 *              failed to create output
 */
COutput *COutputFactory::Create (int nIndex)
{
    COutput *pOutput = NULL;

    if ((0 <= nIndex) && (nIndex < COutputLib::eIndexMax))
    {
        if (m_apfnCreate[nIndex])
        {
            pOutput = m_apfnCreate[nIndex] ();
        }
    }

    return pOutput;
}

/*!
 * @fn          COutput::COutput (void)
 * @brief       constructor
 */
COutput::COutput (void)
{
}

/*!
 * @fn          COutput::~COutput (void)
 * @brief       desstructor
 */
COutput::~COutput (void)
{
}

/*!
 * @var         const char *COutputLib::s_astrName[eIndexMax]
 * @brief       library names
 */
const char *COutputLib::s_astrName[eIndexMax] = {
#ifdef _WIN32
        "dat",
        "text",
        "png",
        "rrd"
#else // _WIN32
        [eDat] = "dat",
        [eText] = "text",
        [ePng] = "png",
        [eRrd] = "rrd"
#endif // _WIN32
};

/*!
 * @fn          COutputLib::COutputLib (EIndex eIndex,
 *                      const char* strName,
 *                      bool bTones)
 * @brief       constructor
 * @param[in]   eIndex
 *              index
 * @param[in]   strName
 *              library name part
 * @param[in]   bTones
 *              write tones
 */
COutputLib::COutputLib (EIndex eIndex, const char* strName, bool bTones)
: m_eIndex (eIndex)
, m_strName (strName)
, m_pOutput (NULL)
, m_bLoad (true)
, m_bTones (bTones)
{
}

/*!
 * @fn          COutputLib::COutputLib (EIndex eIndex, COutput* pOutput, bool bTones)
 * @brief       constructor
 * @param[in]   eIndex
 *              index
 * @param[in]   pOutput
 *              output class
 * @param[in]   bTones
 *              write tones
 */
COutputLib::COutputLib (EIndex eIndex, COutput* pOutput, bool bTones)
: m_eIndex (eIndex)
, m_strName ()
, m_pOutput (pOutput)
, m_bLoad (false)
, m_bTones (bTones)
{
}

/*!
 * @fn          COutputLib::~COutputLib (void)
 * @brief       destructor
 */
COutputLib::~COutputLib (void)
{
    Unload ();
}

/*!
 * @fn          void COutputLib::Init (bool bLoad, bool bTones)
 * @brief       init load and tones flags
 * @param[in]   bLoad
 *              load library
 * @param[in]   bTones
 *              write tones
 */
void COutputLib::Init (bool bLoad, bool bTones)
{
    m_bLoad = bLoad;
    m_bTones = bTones;
}

/*!
 * @fn          bool COutputLib::Load (load)
 * @brief       load library
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      true
 *              failure
 */
bool COutputLib::Load (void)
{
    bool bRet = true;

    if (m_bLoad)
    {
        if (m_strName)
        {
            if (!COutputFactory::g_cOutputFactory.Load (m_eIndex, m_strName))
            {
                LogError (NStrErr::OutputType, m_strName);
                bRet = false;
            }

            if (bRet)
            {
                if (!COutputFactory::g_cOutputFactory.TestVersion (m_eIndex, m_strName))
                {
                    bRet = false;
                }
            }

            if (bRet)
            {
                m_pOutput = COutputFactory::g_cOutputFactory.Create (m_eIndex);

                bRet = (0 != m_pOutput);
            }
        }
    }

    return bRet;
}

/*!
 * @fn          void COutputLib::Unload (void)
 * @brief       unload library
 */
void COutputLib::Unload (void)
{
    if (m_pOutput)
    {
        delete m_pOutput;
        m_pOutput = NULL;
    }
    if (m_bLoad)
    {
        COutputFactory::g_cOutputFactory.Unload (m_eIndex);
    }
}

/*!
 * @fn          void COutputLib::Write (const CConfigData &cConfig,
 *                      const CDslData &cData)
 * @brief       write data
 * @param[in]   cConfig
 *              configuration data
 * @param[in]   cData
 *              dsl data
 */
void COutputLib::Write (const CConfigData &cConfig,
        const CDslData &cData)
{
    if (m_pOutput)
    {
        m_pOutput->Write (cConfig, cData, m_bTones);
    }
}

//_oOo_
