/*!
 * @file        gui-config.cpp
 * @brief       config dialog class implementationn
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gui-config.h"
#include "txt.h"

#include <QShowEvent>
#include <QFileDialog>

//! @cond
#define CHECKBOX(name) connect (m_uiConfig.check##name,\
    SIGNAL (stateChanged (int)), this, SLOT (OnCheck##name (int)))
#define COMBOBOX(name) connect (m_uiConfig.combo##name,\
    SIGNAL (currentIndexChanged (int)), this, SLOT (OnCombo##name (int)))
#define TOOLBTN(name) connect (m_uiConfig.tool##name,\
    SIGNAL (clicked ()), this, SLOT (OnTool##name ()))
//! @endcond

/*!
 * @fn          CGuiConfig::CGuiConfig (CGuiMain *pParent)
 * @brief       constructor
 * @details     init UI
 * @param[in]   pParent
 *              parent window
 */
CGuiConfig::CGuiConfig (CGuiMain *pParent)
: QDialog (pParent)
, m_uiConfig ()
{
    m_uiConfig.setupUi (this);

    CHECKBOX (ModemPort);
    CHECKBOX (Extra);
    CHECKBOX (ModemPort);
    CHECKBOX (EthType);
    CHECKBOX (FilePath);
    CHECKBOX (FilePrefix);
    CHECKBOX (CollectHost);
    CHECKBOX (CollectSock);
    CHECKBOX (DaemonInt);
    CHECKBOX (DaemonPid);


    COMBOBOX (Log);

    // tool buttons
    TOOLBTN (Extra);
    TOOLBTN (FilePath);
    TOOLBTN (LogFile);
    TOOLBTN (CollectSock);
    TOOLBTN (DaemonPid);

    // add modem type item's
    m_uiConfig.comboModemType->addItem ("demo-adsl");
    m_uiConfig.comboModemType->addItem ("demo-vdsl");
    m_uiConfig.comboModemType->addItem ("amazon");
    m_uiConfig.comboModemType->addItem ("ar7");
    m_uiConfig.comboModemType->addItem ("avm-tr064");
    m_uiConfig.comboModemType->addItem ("bc63");
    m_uiConfig.comboModemType->addItem ("conexant");
    m_uiConfig.comboModemType->addItem ("speedtouch");
    m_uiConfig.comboModemType->addItem ("trendchip");
    m_uiConfig.comboModemType->addItem ("vigor");
    m_uiConfig.comboModemType->addItem ("vinax");

    // add protocol item's
    m_uiConfig.comboProtocolType->addItem ("demo");
#ifdef WITH_TELNET
    m_uiConfig.comboProtocolType->addItem ("telnet");
#endif // WITH_TELNET
#ifdef WITH_SSH
    m_uiConfig.comboProtocolType->addItem ("ssh");
#endif // WITH_SSH
#ifdef WITH_HTTP
    m_uiConfig.comboProtocolType->addItem ("http");
#endif // WITH_HTTP
#ifdef WITH_PCAP
#ifdef WITH_TELNET
    m_uiConfig.comboProtocolType->addItem ("pcap-telnet");
#endif // WITH_TELNET
#ifdef WITH_SSH
    m_uiConfig.comboProtocolType->addItem ("pcap-ssh");
#endif // WITH_SSH
#ifdef WITH_HTTP
    m_uiConfig.comboProtocolType->addItem ("pcap-http");
#endif // WITH_HTTP
#endif // WITH_PCAP

    // add ethtype item's
    m_uiConfig.comboEthType->addItem (tr ("auto"));
    m_uiConfig.comboEthType->addItem (tr ("IPv4"));
    m_uiConfig.comboEthType->addItem (tr ("IPv6"));

    // add log level item's
    m_uiConfig.comboLogLevel->addItem (tr ("error"));
    m_uiConfig.comboLogLevel->addItem (tr ("warning"));
    m_uiConfig.comboLogLevel->addItem (tr ("info"));
    m_uiConfig.comboLogLevel->addItem (tr ("debug"));
    m_uiConfig.comboLogLevel->addItem (tr ("trace"));
    m_uiConfig.comboLogLevel->addItem (tr ("extra"));

    // add log item's
    m_uiConfig.comboLog->addItem (tr ("none"));
    m_uiConfig.comboLog->addItem (tr ("stderr"));
    m_uiConfig.comboLog->addItem (tr ("syslog"));
    m_uiConfig.comboLog->addItem (tr ("default.log"));
    m_uiConfig.comboLog->addItem (tr ("file"));

    ResetFocus ();
}

/*!
 * @fn          void CGuiConfig::ResetFocus (void)
 * @brief       reset focus
 * @details     set focus to first element on first tab
 */
void CGuiConfig::ResetFocus (void)
{
    m_uiConfig.tabConfig->setCurrentIndex (0);
    m_uiConfig.comboModemType->setFocus ();
}

/*!
 * @fn          void CGuiConfig::SetConfig (const CConfigData &cConfig)
 * @brief       fill dialog with configuration data
 * @param[in]   cConfig
 *              configuration data
 */
void CGuiConfig::SetConfig (const CConfigData &cConfig)
{
    bool bEnable;
    CString strTemp;
    ELogType eLogType;

    // modem group
    m_uiConfig.comboModemType->setCurrentText (QString (cConfig.m_strModemType));

    m_uiConfig.comboProtocolType->setCurrentText (QString (cConfig.m_strProtocol));

    bEnable = (0 != cConfig.m_strExtra.Length ());
    m_uiConfig.checkExtra->setChecked(bEnable);
    m_uiConfig.editExtra->setEnabled (bEnable);
    m_uiConfig.toolExtra->setEnabled (bEnable);
    m_uiConfig.editExtra->setText (QString (cConfig.m_strExtra));

    // host group
    m_uiConfig.editModemHost->setText (QString (cConfig.m_strModemHost));

    bEnable = (cConfig.m_nPort > 0);
    m_uiConfig.checkModemPort->setChecked (bEnable);
    m_uiConfig.spinModemPort->setEnabled (bEnable);
    m_uiConfig.spinModemPort->setValue (bEnable ? cConfig.m_nPort : 0);

    bEnable = (CProtocol::eAuto != cConfig.m_eEthType);
    m_uiConfig.checkEthType->setChecked (bEnable);
    m_uiConfig.comboEthType->setEnabled (bEnable);
    m_uiConfig.comboEthType->setCurrentIndex (cConfig.m_eEthType);

    // login group
    m_uiConfig.editModemUser->setText (QString (cConfig.m_strUser));

    m_uiConfig.editModemPass->setText (QString (cConfig.m_strPass));

    // file group
    bEnable = (!cConfig.m_strPath.Compare (NStrDef::Path));
    m_uiConfig.checkFilePath->setChecked (bEnable);
    m_uiConfig.editFilePath->setEnabled (bEnable);
    m_uiConfig.toolFilePath->setEnabled (bEnable);
    m_uiConfig.editFilePath->setText (QString (cConfig.m_strPath));

    strTemp.Format (NStrDef::FilePrefix, (const char*)cConfig.m_strPrefix);
    bEnable = (!cConfig.m_strPath.Compare (strTemp));
    m_uiConfig.checkFilePrefix->setChecked (bEnable);
    m_uiConfig.editFilePrefix->setEnabled (bEnable);
    m_uiConfig.editFilePrefix->setText (QString (cConfig.m_strPrefix));

    // log group
    m_uiConfig.comboLogLevel->setCurrentIndex (cConfig.m_eLogLevel);

    eLogType = LogType (cConfig.m_bLog, cConfig.m_strLogFile);
    m_uiConfig.comboLog->setCurrentIndex (eLogType);
    m_uiConfig.editLogFile->setEnabled (eLogType == eLogFile);
    m_uiConfig.toolLogFile->setEnabled (eLogType == eLogFile);
    switch (eLogType)
    {
    case eLogNone:
        m_uiConfig.editLogFile->setText ("");
        break;
    case eLogStderr:
        m_uiConfig.editLogFile->setText ("-");
        break;
    case eLogSyslog:
        m_uiConfig.editLogFile->setText ("#");
        break;
    case eLogDefault:
        m_uiConfig.editLogFile->setText ("+");
        break;
    case eLogFile:
        m_uiConfig.editLogFile->setText (QString (cConfig.m_strLogFile));
        break;
    }

    // collect group
    bEnable = (!cConfig.m_strCollectHost.Compare (NStrDef::CollectHost));
    m_uiConfig.checkCollectHost->setChecked (bEnable);
    m_uiConfig.editCollectHost->setEnabled (bEnable);
    m_uiConfig.editCollectHost->setText (QString (cConfig.m_strCollectHost));

    bEnable = (!cConfig.m_strCollectSock.Compare (NStrDef::CollectSock));
    m_uiConfig.checkCollectSock->setChecked (bEnable);
    m_uiConfig.editCollectSock->setEnabled (bEnable);
    m_uiConfig.toolCollectSock->setEnabled (bEnable);
    m_uiConfig.editCollectSock->setText (QString (cConfig.m_strCollectSock));

    // daemon group
    bEnable = (10 != cConfig.m_nInterval);
    m_uiConfig.checkDaemonInt->setChecked (bEnable);
    m_uiConfig.spinDaemonInt->setEnabled (bEnable);
    m_uiConfig.spinDaemonInt->setValue (cConfig.m_nInterval);

    bEnable = (!cConfig.m_strPidFile.Compare (NStrDef::DaemonPid));
    m_uiConfig.checkDaemonPid->setChecked (bEnable);
    m_uiConfig.editDaemonPid->setEnabled (bEnable);
    m_uiConfig.toolDaemonPid->setEnabled (bEnable);
    m_uiConfig.editDaemonPid->setText (QString (cConfig.m_strPidFile));

    m_uiConfig.checkDaemonBack->setChecked (cConfig.m_bBackground);
}

/*!
 * @fn          bool CGuiConfig::GetConfig (CConfigData &cConfig, CConfigList &cConfigList)
 * @brief       get configuration data from dialog
 * @param[out]  cConfig
 *              configuration data
 * @param[out]  cConfigList
 *              configuration list
 * @return      dirty flag
 * @retval      true
 *              configuration changed
 * @retval      false
 *              configuration not changed
 */
bool CGuiConfig::GetConfig (CConfigData &cConfig, CConfigList &cConfigList)
{
    bool bDirty = false;
    int nEthType, nLogLevel, nLogType;
    CString strDefPrefix;
    CString strLogFile;

    // modem group
    Get (bDirty, cConfig.m_strModemType, m_uiConfig.comboModemType);
    Get (bDirty, cConfig.m_strProtocol, m_uiConfig.comboProtocolType);
    Get (bDirty, cConfig.m_strExtra, m_uiConfig.editExtra);

    // host group
    Get (bDirty, cConfig.m_strModemHost, m_uiConfig.editModemHost);
    Get (bDirty, cConfig.m_nPort, m_uiConfig.spinModemPort,
            m_uiConfig.checkModemPort, -1);

    nEthType = cConfig.m_eEthType;
    if (Get (bDirty, nEthType, m_uiConfig.comboEthType))
    {
        cConfig.m_eEthType = (CProtocol::EEthType)nEthType;
    }

    // login group
    Get (bDirty, cConfig.m_strUser, m_uiConfig.editModemUser);
    Get (bDirty, cConfig.m_strPass, m_uiConfig.editModemPass);

    // file group
    Get (bDirty, cConfig.m_strPath, m_uiConfig.editFilePath,
            m_uiConfig.checkFilePath, NStrDef::Path);
    strDefPrefix.Format (NStrDef::FilePrefix, (const char*)cConfig.m_strModemHost);
    Get (bDirty, cConfig.m_strPrefix, m_uiConfig.editFilePrefix,
            m_uiConfig.checkFilePrefix, strDefPrefix);

    // log group
    nLogLevel = cConfig.m_eLogLevel;
    if (Get (bDirty, nLogLevel, m_uiConfig.comboLogLevel))
    {
        cConfig.m_eLogLevel = (CLog::ELevel)nLogLevel;
    }
    nLogType = LogType (cConfig.m_bLog, cConfig.m_strLogFile);
    if (Get (bDirty, nLogType, m_uiConfig.comboLog))
    {
        cConfig.m_bLog = (eLogNone != nLogType);
    }
    Get (bDirty, cConfig.m_strLogFile, m_uiConfig.editLogFile);

    // collect group
    Get (bDirty, cConfig.m_strCollectHost, m_uiConfig.editCollectHost,
            m_uiConfig.checkCollectHost, NStrDef::CollectHost);
    Get (bDirty, cConfig.m_strCollectSock, m_uiConfig.editCollectSock,
            m_uiConfig.checkCollectSock, NStrDef::CollectSock);

    // daemon group
    Get (bDirty, cConfig.m_nInterval, m_uiConfig.spinDaemonInt);
    Get (bDirty, cConfig.m_strPidFile, m_uiConfig.editDaemonPid,
            m_uiConfig.checkDaemonPid, NStrDef::DaemonPid);

    if (bDirty)
    {
        cConfigList.ReadData (cConfig);
    }

    return bDirty;
}

/*!
 * @fn          bool CGuiConfig::Get (bool &bDirty,
 *                      CString &strConfig,
 *                      const QLineEdit *pEdit,
 *                      const QCheckBox *pCheck,
 *                      const char *strDefault)
 * @brief       get data from edit box
 * @param[in,out] bDirty
 *              configuration changed flag
 * @param[in,out] strConfig
 *              coniguration data
 * @param[in]   pEdit
 *              edit box
 * @param[in]   pCheck
 *              optional check box
 * @param[in]   strDefault
 *              optional default value
 * @return      dirty flag
 * @retval      true
 *              configuration entry changed
 * @retval      false
 *              configuration entry not changed
 */
bool CGuiConfig::Get (bool &bDirty,
        CString &strConfig,
        const QLineEdit *pEdit,
        const QCheckBox *pCheck,
        const char *strDefault)
{
    bool bRet = false;

    if (pCheck)
    {
        if (pCheck->isChecked ())
        {
            if (0 != pEdit->text ().compare (QString (strConfig)))
            {
                strConfig = pEdit->text ().toUtf8 ();
                bRet = true;
            }
        }
        else
        {
            if (!strConfig.Compare (strDefault))
            {
                strConfig = strDefault;
                bRet = true;
            }
        }
    }
    else
    {
        if (0 != pEdit->text ().compare (QString (strConfig)))
        {
            strConfig = pEdit->text ().toUtf8 ();
            bRet = true;
        }
    }

    bDirty = bRet ? true : bDirty;
    return bRet;
}

/*!
 * @fn          bool CGuiConfig::Get (bool &bDirty,
 *                      CString &strConfig,
 *                      const QComboBox *pCombo,
 *                      const QCheckBox *pCheck,
 *                      const char *strDefault)
 * @brief       get data from combo box
 * @param[in,out] bDirty
 *              configuration changed flag
 * @param[in,out] strConfig
 *              coniguration data
 * @param[in]   pCombo
 *              combo box
 * @param[in]   pCheck
 *              optional check box
 * @param[in]   strDefault
 *              optional default value
 * @return      dirty flag
 * @retval      true
 *              configuration entry changed
 * @retval      false
 *              configuration entry not changed
 */
bool CGuiConfig::Get (bool &bDirty,
        CString &strConfig,
        const QComboBox *pCombo,
        const QCheckBox *pCheck,
        const char *strDefault)
{
    bool bRet = false;

    if (pCheck)
    {
        if (pCheck->isChecked ())
        {
            if (0 != pCombo->currentText ().compare (QString (strConfig)))
            {
                strConfig = pCombo->currentText ().toUtf8 ();
                bRet = true;
            }
        }
        else
        {
            if (!strConfig.Compare (strDefault))
            {
                strConfig = strDefault;
                bRet = true;
            }
        }
    }
    else
    {
        if (0 != pCombo->currentText ().compare (QString (strConfig)))
        {
            strConfig = pCombo->currentText ().toUtf8 ();
            bRet = true;
        }
    }

    bDirty = bRet ? true : bDirty;
    return bRet;
}

/*!
 * @fn          bool CGuiConfig::Get (bool &bDirty,
 *                      int &nConfig,
 *                      const QComboBox *pCombo,
 *                      const QCheckBox *pCheck,
 *                      int nDefault)
 * @brief       get data from combo box
 * @param[in,out] bDirty
 *              configuration changed flag
 * @param[in,out] nConfig
 *              coniguration data
 * @param[in]   pCombo
 *              combo box
 * @param[in]   pCheck
 *              optional check box
 * @param[in]   nDefault
 *              optional default value
 * @return      dirty flag
 * @retval      true
 *              configuration entry changed
 * @retval      false
 *              configuration entry not changed
 */
bool CGuiConfig::Get (bool &bDirty,
        int &nConfig,
        const QComboBox *pCombo,
        const QCheckBox *pCheck,
        int nDefault)
{
    bool bRet = false;

    if (pCheck)
    {
        if (pCheck->isChecked ())
        {
            if (nConfig != pCombo->currentIndex ())
            {
                nConfig = pCombo->currentIndex ();
                bRet = true;
            }
        }
        else
        {
            if (nConfig != nDefault)
            {
                nConfig = nDefault;
                bRet = true;
            }
        }
    }
    else
    {
        if (nConfig != pCombo->currentIndex ())
        {
            nConfig = pCombo->currentIndex ();
            bRet = true;
        }
    }

    bDirty = bRet ? true : bDirty;
    return bRet;
}

/*!
 * @fn          bool CGuiConfig::Get (bool &bDirty,
 *                      int &nConfig,
 *                      const QSpinBox *pSpin,
 *                      const QCheckBox *pCheck,
 *                      int nDefault)
 * @brief       get data from spin box
 * @param[in,out] bDirty
 *              configuration changed flag
 * @param[in,out] nConfig
 *              coniguration data
 * @param[in]   pSpin
 *              spin box
 * @param[in]   pCheck
 *              optional check box
 * @param[in]   nDefault
 *              optional default value
 * @return      dirty flag
 * @retval      true
 *              configuration entry changed
 * @retval      false
 *              configuration entry not changed
 */
bool CGuiConfig::Get (bool &bDirty,
        int &nConfig,
        const QSpinBox *pSpin,
        const QCheckBox *pCheck,
        int nDefault)
{
    bool bRet = false;

    if (pCheck)
    {
        if (pCheck->isChecked ())
        {
            if (nConfig != pSpin->value ())
            {
                nConfig = pSpin->value ();
                bRet = true;
            }
        }
        else
        {
            if (nConfig != nDefault)
            {
                nConfig = nDefault;
                bRet = true;
            }
        }
    }
    else
    {
        if (nConfig != pSpin->value ())
        {
            nConfig = pSpin->value ();
            bRet = true;
        }
    }

    bDirty = bRet ? true : bDirty;
    return bRet;
}

/*!
 * @fn          CGuiConfig::ELogType CGuiConfig::LogType (bool bLog,
 *                      const CString &strLogFile)
 * @brief       get log type
 * @param[in]   bLog
 *              log flag
 * @param[in]   strLogFile
 *              log file name (and special values)
 * @return      log type
 */
CGuiConfig::ELogType CGuiConfig::LogType (bool bLog, const CString &strLogFile)
{
    ELogType eLogType = eLogNone;

    if (bLog)
    {
        if (strLogFile.Compare ("-"))
        {
            eLogType = eLogStderr;
        }
        else if (strLogFile.Compare ("#"))
        {
            eLogType = eLogSyslog;
        }
        else if (strLogFile.Compare ("+"))
        {
            eLogType = eLogDefault;
        }
        else
        {
            eLogType = eLogFile;
        }
    }

    return eLogType;
}

/*!
 * @fn          void CGuiConfig::OnCheckExtra (int nState)
 * @brief       extra check box clicked
 * @details     enable/disable edit box and tool button
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckExtra (int nState)
{
    m_uiConfig.editExtra->setEnabled (nState);
    m_uiConfig.toolExtra->setEnabled (nState);

    if (!nState)
    {
        m_uiConfig.editExtra->setText ("");
    }
}

/*!
 * @fn          void CGuiConfig::OnCheckModemPort (int nState)
 * @brief       modem port check box clicked
 * @details     enable/disable spin box
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckModemPort (int nState)
{
    m_uiConfig.spinModemPort->setEnabled (nState);
    if (nState)
    {
        switch ((CProtocol::EProtocol)m_uiConfig.comboProtocolType->currentIndex ())
        {
        case CProtocol::eDemo:
            m_uiConfig.spinModemPort->setValue (0);
            break;
#ifdef WITH_TELNET
        case CProtocol::eTelnet:
#ifdef WITH_PCAP
        case CProtocol::ePcapTelnet:
#endif // WITH_PCAP
            m_uiConfig.spinModemPort->setValue (23);
            break;
#endif // WITH_TELNET
#ifdef WITH_SSH
        case CProtocol::eSSH:
#ifdef WITH_PCAP
        case CProtocol::ePcapSSH:
#endif // WITH_PCAP
            m_uiConfig.spinModemPort->setValue (22);
            break;
#endif // WITH_SSH
#ifdef WITH_HTTP
        case CProtocol::eHTTP:
#ifdef WITH_PCAP
        case CProtocol::ePcapHTTP:
#endif // WITH_PCAP
            m_uiConfig.spinModemPort->setValue (80);
            break;
#endif // WITH_HTTP
        }
    }
    else
    {
        m_uiConfig.spinModemPort->setValue (0);
    }
}

/*!
 * @fn          void CGuiConfig::OnCheckEthType (int nState)
 * @brief       ethertype check box clicked
 * @details     enable/disable combo box
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckEthType (int nState)
{
    m_uiConfig.comboEthType->setEnabled (nState);

    if (!nState)
    {
        m_uiConfig.comboEthType->setCurrentIndex (CProtocol::eAuto);
    }
}

/*!
 * @fn          void CGuiConfig::OnCheckFilePath (int nState)
 * @brief       file path check box clicked
 * @details     enable/disable edit box and tool button
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckFilePath (int nState)
{
    m_uiConfig.editFilePath->setEnabled (nState);
    m_uiConfig.toolFilePath->setEnabled (nState);

    if (!nState)
    {
        m_uiConfig.editFilePath->setText (NStrDef::Path);
    }
}

/*!
 * @fn          void CGuiConfig::OnCheckFilePrefix (int nState)
 * @brief       file prefix check box clicked
 * @details     enable/disable edit box and tool button
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckFilePrefix (int nState)
{
    m_uiConfig.editFilePrefix->setEnabled (nState);

    if (!nState)
    {
        m_uiConfig.editFilePrefix->setText (QString::asprintf (NStrDef::FilePrefix,
                (const char *)m_uiConfig.editModemHost->text ().toUtf8 ()));
    }
}

/*!
 * @fn          void CGuiConfig::OnCheckCollectHost (int nState)
 * @brief       collect host check box clicked
 * @details     enable/disable edit box
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckCollectHost (int nState)
{
    m_uiConfig.editCollectHost->setEnabled (nState);

    if (!nState)
    {
        m_uiConfig.editCollectHost->setText (QString (NStrDef::CollectHost));
    }
}

/*!
 * @fn          void CGuiConfig::OnCheckCollectSock (int nState)
 * @brief       collect sock check box clicked
 * @details     enable/disable edit box and tool button
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckCollectSock (int nState)
{
    m_uiConfig.editCollectSock->setEnabled (nState);
    m_uiConfig.toolCollectSock->setEnabled (nState);

    if (!nState)
    {
        m_uiConfig.editCollectSock->setText (QString (NStrDef::CollectSock));
    }
}

/*!
 * @fn          void CGuiConfig::OnCheckDaemonInt (int nState)
 * @brief       daemon interval check box clicked
 * @details     enable/disable spin box
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckDaemonInt (int nState)
{
    m_uiConfig.spinDaemonInt->setEnabled (nState);

    if (!nState)
    {
        m_uiConfig.spinDaemonInt->setValue (10);
    }
}

/*!
 * @fn          void CGuiConfig::OnCheckDaemonPid (int nState)
 * @brief       daemon pid file check box clicked
 * @details     enable/disable edit box and tool button
 * @param[in]   nState
 *              check box state
 */
void CGuiConfig::OnCheckDaemonPid (int nState)
{
    m_uiConfig.editDaemonPid->setEnabled (nState);
    m_uiConfig.toolDaemonPid->setEnabled (nState);

    if (!nState)
    {
        m_uiConfig.editDaemonPid->setText (QString (NStrDef::DaemonPid));
    }
}

/*!
 * @fn          void CGuiConfig::OnComboLog (int nIndex)
 * @brief       log type combo box changed
 * @details     set default values to log file
 * @param[in]   nIndex
 *              combo box state
 */
void CGuiConfig::OnComboLog (int nIndex)
{
    switch (nIndex)
    {
    case eLogNone:
        m_uiConfig.editLogFile->setEnabled (false);
        m_uiConfig.toolLogFile->setEnabled (false);
        m_uiConfig.editLogFile->setText ("");
        break;
    case eLogStderr:
        m_uiConfig.editLogFile->setEnabled (false);
        m_uiConfig.toolLogFile->setEnabled (false);
        m_uiConfig.editLogFile->setText ("-");
        break;
    case eLogSyslog:
        m_uiConfig.editLogFile->setEnabled (false);
        m_uiConfig.toolLogFile->setEnabled (false);
        m_uiConfig.editLogFile->setText ("#");
        break;
    case eLogDefault:
        m_uiConfig.editLogFile->setEnabled (false);
        m_uiConfig.toolLogFile->setEnabled (false);
        m_uiConfig.editLogFile->setText ("+");
        break;
    case eLogFile:
        m_uiConfig.editLogFile->setEnabled (true);
        m_uiConfig.toolLogFile->setEnabled (true);
        break;
    }
}

/*!
 * @fn          void CGuiConfig::OnToolExtra (void)
 * @brief       extra tool button clicked
 * @details     show open file dialog
 */
void CGuiConfig::OnToolExtra (void)
{
    QString strPath = QFileDialog::getOpenFileName (this,
            tr ("Open"),
            m_uiConfig.editExtra->text (),
            tr ("Capture files (*.pcap *.cap);; All files (*)"));

    if (!strPath.isEmpty ())
    {
        m_uiConfig.editExtra->setText (strPath);
    }
}

/*!
 * @fn          void CGuiConfig::OnToolFilePath (void)
 * @brief       file path tool button clicked
 * @details     show directory file dialog
 */
void CGuiConfig::OnToolFilePath (void)
{
    QString strPath = QFileDialog::getExistingDirectory (this,
            tr ("Path"),
            m_uiConfig.editFilePath->text ());

    if (!strPath.isEmpty ())
    {
        m_uiConfig.editFilePath->setText (strPath);
    }
}

/*!
 * @fn          void CGuiConfig::OnToolLogFile (void)
 * @brief       log file tool button clicked
 * @details     show save file dialog
 */
void CGuiConfig::OnToolLogFile (void)
{
    QString strPath = QFileDialog::getSaveFileName (this,
            tr ("Save"),
            m_uiConfig.editLogFile->text (),
            tr ("Log files (*.log);;All files (*)"));

    if (!strPath.isEmpty ())
    {
        m_uiConfig.editLogFile->setText (strPath);
    }
}

/*!
 * @fn          void CGuiConfig::OnToolCollectSock (void)
 * @brief       collect sock tool button clicked
 * @details     show save file dialog
 */
void CGuiConfig::OnToolCollectSock (void)
{
    QString strPath = QFileDialog::getSaveFileName (this,
            tr ("Save"),
            m_uiConfig.editCollectSock->text (),
            tr ("Socket (*.sock);; All files (*)"));

    if (!strPath.isEmpty ())
    {
        m_uiConfig.editCollectSock->setText (strPath);
    }
}

/*!
 * @fn          void CGuiConfig::OnToolDaemonPid (void)
 * @brief       daemon pid file tool button clicked
 * @details     show save file dialog
 */
void CGuiConfig::OnToolDaemonPid (void)
{
    QString strPath = QFileDialog::getSaveFileName (this,
            tr ("Save"),
            m_uiConfig.editDaemonPid->text (),
            tr ("Pid files (*.pid);;All files (*)"));

    if (!strPath.isEmpty ())
    {
        m_uiConfig.editDaemonPid->setText (strPath);
    }
}

//_oOo_
