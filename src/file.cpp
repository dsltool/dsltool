/*!
 * @file        file.cpp
 * @brief       file class implementation
 * @details     file access helper class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <sys/stat.h>

#include "log.h"
#include "txt.h"

/*!
 * @fn          CFile::CFile (FILE* pFile)
 * @brief       constructor
 * @details     copy file handle
 * @param[in]   pFile
 *              source file handle
 */
CFile::CFile (FILE* pFile)
: m_pFile (pFile)
{
}

/*!
 * @fn          CFile::~CFile (void)
 * @brief       destructor
 * @details     close file
 */
CFile::~CFile (void)
{
    Close ();
}

/*!
 * @fn          bool CFile::Open (FILE* pFile)
 * @brief       open from file handle
 * @param[in]   pFile
 *              source file handle
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CFile::Open (FILE* pFile)
{
    Close ();

    m_pFile = pFile;

    return true;
}

/*!
 * @fn          bool CFile::Open (const char *strFile, const char *strMode)
 * @brief       open named file
 * @details     open file with desired access
 * @param[in]   strFile
 *              file name
 * @param[in]   strMode
 *              access mode
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              open failed
 */
bool CFile::Open (const char *strFile, const char *strMode)
{
    bool bRet = false;
    FILE* pFile;

    if (strFile && strMode)
    {
        do
        {
            bool bWrite = !('r' == strMode[0]);

            if (!Exist (strFile, bWrite))
            {
                LogError (NStrErr::FileStat, strFile);
                break;;
            }

            pFile = fopen (strFile, strMode);

            if (pFile)
            {
                m_pFile = pFile;
                bRet = true;
            }
            else
            {
                LogError (NStrErr::FileOpen, strFile);
            }
        } while (false);
    }

    return bRet;
}

/*!
 * @fn          void CFile::Close (void)
 * @brief       close file
 */
void CFile::Close (void)
{
    if (m_pFile && (stdout != m_pFile) && (stderr != m_pFile))
    {
        fclose (m_pFile);
    }

    m_pFile = NULL;
}

//! @cond
#ifndef S_ISREG
#define S_ISREG(mode) (((mode) & S_IFMT) == S_IFREG)
#endif // S_ISREG
//! @endcond
/*!
 * @fn          bool CFile::Exist (const char *strFile, bool bCreate)
 * @brief       check if file can be accessed
 * @param[in]   strFile
 *              file name
 * @param[in]   bCreate
 *              only create access needed
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              open failed
 */
bool CFile::Exist (const char *strFile, bool bCreate)
{
    struct stat statbuf;
    bool bRet = false;

    if (stat (strFile, &statbuf) >= 0)
    {
        if (S_ISREG (statbuf.st_mode))
        {
            bRet = true;
        }
    }
    else if (bCreate)
    {
        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          void CFile::Delete (const char *strFile)
 * @brief       delete file
 * @param[in]   strFile
 *              file name
 */
void CFile::Delete (const char *strFile)
{
    remove (strFile);
}

/*!
 * @fn          bool CFile::Read (void *pBuffer,
 *                      size_t nBuffer,
 *                      size_t &nRead)
 * @brief       read data from file
 * @param[in]   pBuffer
 *              data buffer
 * @param[in]   nBuffer
 *              data buffer size
 * @param[out]  nRead
 *              bytes read
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              read failed or EOF reached
 */
bool CFile::Read (void *pBuffer, size_t nBuffer, size_t &nRead)
{
    bool bRet = false;
    nRead = 0;

    if (m_pFile)
    {
        nRead = fread (pBuffer, 1, nBuffer, m_pFile);
        if ((nRead > 0) && (0 == feof (m_pFile)))
        {
            bRet = true;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CFile::Write (void *pBuffer,
 *                      size_t nBuffer,
 *                      size_t &nWrite)
 * @brief       write data to file
 * @param[in]   pBuffer
 *              data buffer
 * @param[in]   nBuffer
 *              data buffer size
 * @param[out]  nWrite
 *              bytes written
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              write failed or EOF reached
 */
bool CFile::Write (const void *pBuffer, size_t nBuffer, size_t &nWrite)
{
    bool bRet = false;
    nWrite = 0;

    if (m_pFile)
    {
        nWrite = fwrite (pBuffer, 1, nBuffer, m_pFile);
        if (nWrite > 0)
        {
            bRet = true;
        }
    }

    return bRet;
}

/*!
 * @fn          void CFile::VPrintf (const char *strFormat, va_list vaArgs)
 * @brief       print formated
 * @details     format string as printf
 * @param[in]   strFormat
 *              format string
 * @param[in]   vaArgs
 *              arguments
 */
void CFile::VPrintf (const char *strFormat, va_list vaArgs)
{
    if (m_pFile)
    {
        vfprintf (m_pFile, strFormat, vaArgs);
    }
}

/*!
 * @fn          void CFile::Printf (const char *strFormat, ...)
 * @brief       print formated
 * @details     format string as printf
 * @param[in]   strFormat
 *              format string
 * @param[in]   ...
 *              arguments
 */
void CFile::Printf (const char *strFormat, ...)
{
    va_list vaArgs;

    va_start (vaArgs, strFormat);
    VPrintf (strFormat, vaArgs);
    va_end (vaArgs);
}

//_oOo_
