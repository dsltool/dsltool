/*!
 * @file        txt.cpp
 * @brief       text definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 *
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "txt.h"

namespace NStrDef
{
    STRING FilePrefix           = "dsltool-%s";
    STRING FilePrefixDaemon     = "dsltoold-%s";

    STRING ConfigSys            = "/etc/dsltool.conf";
    STRING ConfigUser           = "%s/.dsltool.conf";

    STRING LogPath              = "/var/log";
    STRING LogFile              = "%s/%s.log";
    STRING CaptureFile          = "%s/%s-%s-%s.pcap";
    STRING DatFile              = "%s/%s.dat";
    STRING PngBitsFile          = "%s/%s-bits.png";
    STRING PngSNRFile           = "%s/%s-snr.png";
    STRING PngCharFile          = "%s/%s-char.png";

    STRING Protocol             = "telnet";
    STRING Path                 = "/var/run";

    STRING CollectHost          = "localhost";
    STRING CollectSock          = "/var/run/collectd.sock";

    STRING DaemonPid            = "/var/run/dsltoold.pid";
} // namespace NStrDef

namespace NStrErr
{
    STRING ArgParseError        = "Wrong arguments in command line\n";
    STRING ArgMultiOption       = "Only one option of %s is allowed\n";
    STRING ArgWrongModemType    = "Modem type '%s' not supported\n";
    STRING ArgWrongProtocolType = "Protocol type '%s', not supported\n";

    STRING ArgInvalidOption     = "Invalid option : -%c\n";

    STRING ArgMissingOptionS    = "Missing option : -%c is required\n";
#ifdef WITH_LONGOPT
    STRING ArgMissingOptionL    = "Missing option : --%s is required\n";
    STRING ArgMissingOptionSL   = "Missing option : -%c or --%s is required\n";
#endif // WITH_LONGOPT
    STRING ArgMissingArgS       = "Missing argument: -%c<arg> requires argument\n";
#ifdef WITH_LONGOPT
    STRING ArgMissingArgL       = "Missing argument: --%s==<arg> requires argument\n";
    STRING ArgMissingArgSL      = "Missing argument: -%c<arg> or --%s=<arg> requires argument\n";
#endif // WITH_LONGOPT
    STRING ArgInvalidArgS       = "Wrong argument: -%c '%s', invalid argument value;\n";
#ifdef WITH_LONGOPT
    STRING ArgInvalidArgL       = "Invalid argument: --%s '%s', invalid argument value;\n";
    STRING ArgInvalidArgSL      = "Invald argument: -%c or --%s '%s' invalid argument value;\n";
#endif // WITH_LONGOPT

    STRING OutputType           = "Output type '%s', not supported\n";

    STRING Daemon               = "daemon fail: %s\n";

    STRING FileStat             = "stat: \"%s\" is not a file\n";
    STRING FileOpen             = "fopen failed: \"%s\"\n";

    STRING LogOpen              = "Log: error opening \"%s\"\n";

    STRING Version              = "Version mismatch: library %s is %d.%d.%d, required %d.%d.%d\n";

    STRING CollectOpen          = "lcc_connect failed: %s\n";
    STRING CollectPut           = "lcc_putval failed: %s\n";

    STRING ParserRegComp        = "regcomp: %s \"%s\"\n";
    STRING ParserRegExec        = "regexec: %s\n";
} // namespace NStrErr

namespace NStrInfo
{
    STRING LoadProtocol         = "protocol %s loaded\n";
    STRING LoadModem            = "modem %s loaded\n";
    STRING LoadOutput           = "output %s loaded\n";
}

namespace NStrHelp
{
    STRING Version              = "%s version %s\n";
    STRING Help                 = "\nUsage:\n%s [options]\n\nOPTIONS:\n";
} // namespace NStrHelp

//_oOo_
