/*!
 * @file        gui-info.cpp
 * @brief       info dialog class implementationn
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        13.03.2017
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gui-info.h"
#include "txt.h"

#include <QFileDialog>

/*!
 * @fn          CGuiInfo::CGuiInfo (CGuiMain *pParent)
 * @brief       constructor
 * @details     init UI
 * @param[in]   pParent
 *              parent window
 */
CGuiInfo::CGuiInfo (CGuiMain *pParent)
: QDialog (pParent)
, m_uiInfo ()
{
    m_uiInfo.setupUi (this);

    // set version string
    m_uiInfo.version->setText(QString ("Version %1.%2.%3").arg (
            QString::number (VERSION_MAJOR),
            QString::number (VERSION_MINOR),
            QString::number (VERSION_SUB)));

    {
        QPalette cPalette = m_uiInfo.dsltool->palette ();
        cPalette.setColor (QPalette::WindowText, Qt::blue);
        m_uiInfo.dsltool->setPalette (cPalette);
    }
}

//_oOo_
