/*!
 * @file        dsltool-gui.cpp
 * @brief       application class implementation
 * @details     gui execution classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "dsltool-gui.h"

/*!
 * @fn          int main (int argc, char **argv)
 * @brief       program entry function
 * @details     creates and executes application object
 * @param[in]   argc
 *              number of arguments
 * @param[in]   argv
 *              arguments
 * @return      exit code
 */
int main (int argc, char **argv)
{
    CDslToolGui cDslTool (argc, argv);
    return cDslTool.Main (argc, argv);
}

/*!
 * @fn          CDslToolGui::CDslToolGui (void)
 * @brief       constructor
 */
CDslToolGui::CDslToolGui (int &nArgc, char **astrArgv)
: m_cApp (nArgc, astrArgv)
, m_cGuiMain (*this)
, m_bConfigValid (false)
, m_bConfigDirty (true)
, m_bConfigSaved (false)
{
    m_cConfig.m_ulOutput = 0;
}

/*!
 * @fn          int CDslToolGui::Main (int nArgc, char **astrArgv)
 * @brief       application function
 * @details     check arguments and start @ref CRunOneShot
 * @param       nArgc
 *              number of arguments
 * @param       astrArgv
 * @return      exit code
 * @retval      EXIT_SUCCESS
 *              success
 * @retval      EXIT_FAILURE
 *              failure
 */
int CDslToolGui::Main (int nArgc, char **astrArgv)
{
    int nRet = EXIT_FAILURE;

    InitOutput (COutputLib::eApp, new CGuiOutput (m_cGuiMain), true);

    ReadConfigData (nArgc, astrArgv);
    Refresh ();

    m_cGuiMain.show ();

    nRet = m_cApp.exec ();

    CLog::g_cLog.Close ();

    return nRet;
}

/*!
 * @fn          void CDslToolGui::Refresh (CModem::ECommand eCommand)
 * @brief       refresh data
 * @details     check config, reload protocol and modem handler if necessary
 * @param[in]   eCommand
 *              modem command
 */
void CDslToolGui::Refresh (CModem::ECommand eCommand)
{
    CGuiMain::EStatus eStatus = CGuiMain::eError;

    m_bConfigValid = CheckConfigData (true);

    m_cConfig.m_eCommand = eCommand;
    if (m_bConfigValid)
    {
        if (m_bConfigDirty)
        {
            CModemFactory::g_cModemFactory.Unload ();
            CProtocolFactory::g_cProtocolFactory.Unload ();

            if (LoadProtocol ())
            {
                if (LoadModem ())
                {
                    m_bConfigDirty = false;
                }
            }
        }

        if (!m_bConfigDirty)
        {
            m_cGuiMain.SetStatus (CGuiMain::eRunning);

            if (Run ())
            {
                eStatus = CGuiMain::eReady;
            }
        }
    }
    else
    {
        eStatus = CGuiMain::eNotConfigured;

    }

    m_cGuiMain.SetStatus (eStatus);
}

/*!
 * @fn          void CDslToolGui::ConfigChanged (bool bSave)
 * @brief       called when configuration changed
 * @details     set config dirty and unsaved flags
 * @param[in]   bSave
 *              config needs to be saved
 */
void CDslToolGui::ConfigChanged (bool bSave)
{
    m_bConfigDirty = true;
    m_bConfigSaved = !bSave;

    m_cGuiMain.ResetFocus ();
    m_cGuiMain.EnableMenu ();

    Refresh ();
}


//_oOo_
