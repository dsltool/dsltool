/*!
 * @file        protocol.cpp
 * @brief       protocol base class implementation
 * @details     abstract protocol and helper classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>

#include "protocol.h"
#include "txt.h"
#include "log.h"

/*!
 * @var         CProtocolFactory CProtocolFactory::g_cProtocolFactory;
 * @brief       singleton protocol factory object
 */
CProtocolFactory CProtocolFactory::g_cProtocolFactory;

/*!
 * @fn          CProtocolFactory::CProtocolFactory (void)
 * @brief       constructor
 */
CProtocolFactory::CProtocolFactory (void)
: m_cProtocol ()
, m_pfnVersion (NULL)
, m_pfnCreate (NULL)
{
}

/*!
 * @fn          CProtocolFactory::~CProtocolFactory (void)
 * @brief       destructor
 * @details     unload protocol dynamic library
 */
CProtocolFactory::~CProtocolFactory (void)
{
    Unload ();
}

/*!
 * @fn          bool CProtocolFactory::Load (const char *strProtocol)
 * @brief       load protocol dynamic library
 * @param[in]   strProtocol
 *              protocol name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to load library
 */
bool CProtocolFactory::Load (const char *strProtocol)
{
    bool bRet = false;

    if (m_cProtocol.Open (strProtocol))
    {
        m_pfnVersion = (TpfnVersion) m_cProtocol.Symbol ("ProtocolVersion");
        m_pfnCreate = (TpfnCreate) m_cProtocol.Symbol ("ProtocolCreate");

        if (m_pfnVersion && m_pfnCreate)
        {
            LogInfo (NStrInfo::LoadProtocol, strProtocol);

            bRet = true;
        }
        else
        {
            Unload ();
        }
    }

    return bRet;
}

/*!
 * @fn          void CProtocolFactory::Unload (void)
 * @brief       unload protocol dynamic library
 */
void CProtocolFactory::Unload (void)
{
    m_cProtocol.Close ();
    m_pfnVersion = NULL;
    m_pfnCreate = NULL;
}

/*!
 * @fn          bool CProtocolFactory::TestVersion (const char *strProtocol)
 * @brief       test version number
 * @details     calls @ref ProtocolVersion function in dynamic library
 * @param[in]   strProtocol
 *              protocol name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              version missmatch
 */
bool CProtocolFactory::TestVersion (const char *strProtocol)
{
    bool bRet = false;
    int nMajor = 0;
    int nMinor = 0;
    int nSub = 0;

    if (m_pfnVersion)
    {
        m_pfnVersion (nMajor, nMinor, nSub);

        if ((VERSION_MAJOR == nMajor) &&
            (VERSION_MINOR == nMinor) &&
            (VERSION_SUB == nSub))
        {
            bRet = true;
        }
    }
    if (!bRet)
    {
        LogError (NStrErr::Version, strProtocol, nMajor, nMinor, nSub,
                  VERSION_MAJOR, VERSION_MINOR, VERSION_SUB);
    }
    return bRet;
}

/*!
 * @fn          CProtocol *CProtocolFactory::Create (const char *strHost,
 *                      int nPort,
 *                      CProtocol::EEthType eEthType,
 *                      CAuth *pAuth,
 *                      const char *strExtra)
 * @brief       create protocol instance
 * @details     calls @ref ProtocolCreate function in dynamic library
 * @param[in]   strHost
 *              host name
 * @param[in]   nPort
 *              port number
 * @param[in]   eEthType
 *              protocol type
 * @param[in]   pAuth
 *              authenticator pointer
 * @param[in]   strExtra
 *              extra parameter (or NULL)
 * @return      protocol pointer
 * @retval      NULL
 *              failed to create protocol
 */
CProtocol *CProtocolFactory::Create (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth,
            const char *strExtra)
{
    CProtocol *pProtocol = NULL;

    if (m_pfnCreate)
    {
        pProtocol = m_pfnCreate (strHost, nPort, eEthType, pAuth, strExtra);
    }

    return pProtocol;
}

/*!
 * @fn          CProtocol::CProtocol (const char *strHost,
 *                      int nPort,
 *                      CProtocol::EEthType eEthType,
 *                      CAuth *pAuth)
 * @brief       constructor
 * @param[in]   strHost
 *              host name or IP
 * @param[in]   nPort
 *              port number
 * @param[in]   eEthType
 *              protocol type
 * @param[in]   pAuth
 *              authentication pointer
 */
CProtocol::CProtocol (const char *strHost,
            int nPort,
            EEthType eEthType,
            CAuth *pAuth)
: m_strHost (strHost)
, m_nPort (nPort)
, m_eEthType (eEthType)
, m_pAuth (pAuth)
, m_pParser (NULL)
, m_bLoop (true)
#ifdef ENABLE_CAPTURE
, m_cCapture ()
#endif // ENABLE_CAPTURE
{
}

/*!
 * @fn          CProtocol::~CProtocol (void)
 * @brief       destructor
 */
CProtocol::~CProtocol (void)
{
    if (m_pParser)
    {
        delete m_pParser;
    }
}

/*!
 * @fn          const char *CProtocol::GetProtocol (EEthType eProtocol)
 * @brief       get protocol name
 * @param[in]   eProtocol
 *              protocol enum
 * @return      protocol name
 * @retval      NULL
 *              invalid protocol
 */
const char *CProtocol::GetProtocol (EEthType eProtocol)
{
    switch (eProtocol)
    {
    case eAuto:
        return "auto";
    case eIPv4:
        return "IPv4";
    case eIPv6:
        return "IPv6";
    }
    return NULL;
}

/*!
 * @fn          CProtocol::EEthType CProtocol::GetProtocol(const CString &strProtocol)
 * @brief       get protocol enum
 * @param[in]   strProtocol
 *              protocol name
 * @return      protocol enum
 */
CProtocol::EEthType CProtocol::GetProtocol (const CString &strProtocol)
{
    if (strProtocol.CaseCompare (GetProtocol (eAuto)))
    {
        return eAuto;
    }
    if (strProtocol.CaseCompare (GetProtocol (eIPv4)))
    {
        return eIPv4;
    }
    if (strProtocol.CaseCompare (GetProtocol (eIPv6)))
    {
        return eIPv6;
    }
    return eAuto;
}

/*!
 * @fn          bool CProtocol::ProtocolValid (const CString &strProtocol)
 * @brief       test for valid protocols
 * @param[in]   strProtocol
 *              protocol name
 * @return      valid
 * @retval      true
 *              valid protocol
 * @retval      true
 *              invalid protocol
 */
bool CProtocol::ProtocolValid (const CString &strProtocol)
{
    if (strProtocol.CaseCompare (GetProtocol (eAuto)))
    {
        return true;
    }
    if (strProtocol.CaseCompare (GetProtocol (eIPv4)))
    {
        return true;
    }
    if (strProtocol.CaseCompare (GetProtocol (eIPv6)))
    {
        return true;
    }
    return false;
}

/*!
 * @fn          const char *CProtocol::Host (void)
 * @brief       get host
 * @return      host
 */
const char *CProtocol::Host (void)
{
    return m_strHost;
}

/*!
 * @fn          int CProtocol::Port (void)
 * @brief       get port
 * @return      port
 */
int CProtocol::Port (void)
{
    return m_nPort;
}

/*!
 * @fn          CAuth *CProtocol::Auth (void)
 * @brief       get authenticator
 * @details     overload when derived protocol class manages the authentication
 * @return      authenticator
 */
CAuth *CProtocol::Auth (void)
{
    return m_pAuth;
}

/*!
 * @fn          bool CProtocol::Send (const char *strText, const char *strDump)
 * @brief       send string
 * @param[in]   strText
 *              text to send
 * @param[in]   strDump
 *              alternative text for dump
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              send failure
 */
bool CProtocol::Send (const char *strText, const char *strDump)
{

#ifdef ENABLE_CAPTURE
    if (strDump)
    {
        m_cCapture.Send(strDump);
    }
    else
    {
        m_cCapture.Send(strText);
    }
#else
    (void)strDump;
#endif // ENABLE_CAPTURE

    return SendBuf (strText, strlen (strText));
}

/*!
 * @fn          void CProtocol::ParserOpen (CParser::EContent eContent)
 * @brief       open parser
 * @param[in]   eContent
 *              content type
 */
void CProtocol::ParserOpen (CParser::EContent eContent)
{
    if (m_pParser)
    {
        m_pParser->Open (eContent);
    }
}

/*!
 * @fn          void CProtocol::ParserClose (void)
 * @brief       close parser
 */
void CProtocol::ParserClose (void)
{
    if (m_pParser)
    {
        Next (m_pParser->Close ());
    }
}

/*!
 * @fn          void CProtocol::Parse (const char *strData, size_t nSize)
 * @brief       parse data
 * @details     set next parser
 * @param[in]   strData
 *              received data
 * @param[in]   nSize
 *              data length
 */
void CProtocol::Parse (const char *strData, size_t nSize)
{
    if (m_pParser)
    {
#ifdef ENABLE_CAPTURE
        m_cCapture.Receive(strData, nSize);
#endif // ENABLE_CAPTURE
        Next (m_pParser->Parse (strData, nSize));
    }
}

/*!
 * @fn          void CProtocol::Next (CParser *pNext)
 * @brief       set next parser
 * @details     delete last parser
 * @param[in]   pNext
 *              next parser
 */
void CProtocol::Next (CParser *pNext)
{
    if (m_pParser != pNext)
    {
        if (m_pParser)
        {
            delete m_pParser;
        }
        m_pParser = pNext;
    }
}

//_oOo_
