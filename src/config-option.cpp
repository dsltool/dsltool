/*!
 * @file        config-option.cpp
 * @brief       application class implementation
 * @details     config option class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        29.04.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 *
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "config-option.h"

#include "txt.h"

//! @cond
namespace NStrConfig
{
    STRING NoArgS       = "   -%c\n";
#ifdef WITH_LONGOPT
    STRING NoArgL       = "   --%s\n";
    STRING NoArgSL      = "   -%c, --%s\n";
#endif // WITH_LONGOPT

    STRING ReqArgS       = "   -%c <%s>\n";
#ifdef WITH_LONGOPT
    STRING ReqArgL       = "   --%s=<%s>\n";
    STRING ReqArgSL      = "   -%c <%s>, --%s=<%s>\n";
#endif // WITH_LONGOPT

    STRING OptArgS       = "   -%c [<%s>]\n";
#ifdef WITH_LONGOPT
    STRING OptArgL       = "   --%s=[<%s>]\n";
    STRING OptArgSL      = "   -%c [<%s>], --%s[=<%s>]\n";
#endif // WITH_LONGOPT
} // namespace NStrConfig
//! @endcond

/*!
 * @fn          CConfigOption::CConfigOption (char cShort,
 *                      const char* strLong,
 *                      const char* strHelp,
 *                      const char* strArg,
 *                      unsigned long ulFlags,
 *                      EArg eArg,
 *                      EDataType eType)
 * @brief       destructor
 * @details     creates regular expression for config file parser
 * @param[in]   cShort
 *              short option name
 * @param[in]   strLong
 *              long option and config entry name
 * @param[in]   strHelp
 *              help text
 * @param[in]   strArg
 *              argument name
 * @param[in]   ulFlags
 *              entry flags
 * @param[in]   eArg
 *              argument option
 * @param[in]   eType
 *              variable type
 */
CConfigOption::CConfigOption (char cShort,
        const char* strLong,
        const char* strHelp,
        const char* strArg,
        unsigned long ulFlags,
        EArg eArg,
        EDataType eType)
: m_cShort (cShort)
, m_strLong (strLong)
, m_strHelp (strHelp)
, m_strArg (strArg)
, m_ulFlags (ulFlags)
, m_eArg (eArg)
, m_eType (eType)
, m_strRegEx ()
, m_tRegex ()
, m_nInc (0)
, m_bOption (false)
, m_strOption ()
, m_bConfig (false)
, m_strConfig ()
{
    if (0 == (m_ulFlags & eFlagsNoConfig))
    {
        m_strRegEx.Format ("(%s)([[:space:]]*)(=)([[:space:]]*)(.*)", strLong);

        if (regcomp (&m_tRegex, m_strRegEx, REG_EXTENDED | REG_ICASE))
        {
            m_strRegEx = "";
        }
    }
}

/*!
 * @fn          CConfigOption::~CConfigOption (void)
 * @brief       destructor
 * @details     cleanup regex parser
 */
CConfigOption::~CConfigOption (void)
{
    if (m_strRegEx.Length ())
    {
        regfree (&m_tRegex);
    }
}

/*!
 * @fn          bool CConfigOption::Disable (void)
 * @brief       disable entry
 * @details     disabled entries are not added to short and long options
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CConfigOption::Disable (void)
{
    m_ulFlags |= eFlagsDisabled;

    return true;
}

/*!
 * @fn          unsigned long CConfigOption::GetFlags (void)
 * @brief       get flags
 * @return      flags
 */
unsigned long CConfigOption::GetFlags (void)
{
    return m_ulFlags;
}

/*!
 * @fn          int CConfigOption::GetShort (void)
 * @brief       get short option character
 * @return      option character
 * @retval      -1
 *              disabled
 */
int CConfigOption::GetShort (void)
{
    int nRet = -1;

    if (0 == (m_ulFlags & eFlagsDisabled))
    {
        nRet = m_cShort;
    }

    return nRet;
}

/*!
 * @fn          const char *CConfigOption::GetLong (void)
 * @brief       get long option
 * @return      long option
 * @retval      NULL
 *              disabled
 */
const char *CConfigOption::GetLong (void)
{
    const char *strRet = NULL;

    if (0 == (m_ulFlags & eFlagsNoLong))
    {
        strRet = m_strLong;
    }

    return strRet;
}

/*!
 * @fn          CConfigOption::EArg CConfigOption::GetOptArg ()
 * @brief       get argument type
 * @return      argument type
 */
CConfigOption::EArg CConfigOption::GetOptArg ()
{
    return m_eArg;
}

/*!
 * @fn          bool CConfigOption::TestArg (const char* strValue)
 * @brief       test argument
 * @details     check if optional or required arguments are present
 *              and check value for enum types
 * @param[in]   strValue
 *              option argument
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigOption::TestArg (const char* strValue)
{
    CString strTemp (strValue);
    bool bRet = false;
    bool bArg = false;

    switch (m_eArg)
    {
        case eArgNone:
            if (!strValue)
            {
                bArg = true;
            }
            break;
        case eArgRequired:
            if (strValue)
            {
                bArg = true;
            }
            break;
        case eArgOptional:
            bArg = true;
            break;
    }

    if (bArg)
    {
        switch (m_eType)
        {
        case eTypeString:
            bRet = true;
            break;
        case eTypeBool:
            bRet = true;
            break;
        case eTypeInt:
            bRet = true;
            break;
        case eTypeUInt:
            bRet = true;
            break;
        case eTypeLogLevel:
            bRet = CLog::LevelValid(strTemp);
            break;
        case eTypeEthType:
            bRet = CProtocol::ProtocolValid(strTemp);
            break;
        case eTypeCommand:
            bRet = CModem::CommandValid(strTemp);
            break;
        }
    }
    return bRet;
}

/*!
 * @fn          void CConfigOption::Reset (void)
 * @brief       reset option and config flag
 */
void CConfigOption::Reset (void)
{
    m_bOption = false;
    m_bConfig = false;
}

/*!
 * @fn          bool CConfigOption::SetOption (const char* strValue)
 * @brief       set argument value
 * @details     tested with TestArg before set
 * @param[in]   strValue
 *              option argument
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigOption::SetOption (const char* strValue)
{
    bool bRet = TestArg (strValue);

    if (bRet)
    {
        m_bOption = true;
        m_strOption = strValue;
        if (m_ulFlags & eFlagsInc)
        {
            m_nInc++;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CConfigOption::SetConfig (const char* strValue)
 * @brief       set config value
 * @details     tested with TestArg before set
 * @param[in]   strValue
 *              option argument
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              error
 */
bool CConfigOption::SetConfig (const char* strValue)
{
    bool bRet = TestArg (strValue);

    if (bRet)
    {
        m_bConfig = true;
        m_strConfig = strValue;
        if (m_ulFlags & eFlagsInc)
        {
            m_nInc++;
        }
    }

    return bRet;
}

/*!
 * @fn          void CConfigOption::WriteConfig (CFile &cFile)
 * @brief       write option to config file
 * @param[out]  cFile
 *              file to be written
 */
void CConfigOption::WriteConfig (CFile &cFile)
{
    if (0 == (m_ulFlags & eFlagsNoConfig))
    {
        if (m_bConfig)
        {
            if (m_strConfig)
            {
                cFile.Printf ("%s=%s\n", (const char*)m_strLong, (const char*)m_strConfig);
            }
            else
            {
                cFile.Printf ("%s=\n", (const char*)m_strLong);
            }
        }
    }
}

/*!
 * @fn          bool CConfigOption::Parse (const char* strTest, CString& strValue)
 * @brief       parse config file line
 * @param[in]   strTest
 *              config file line
 * @param[out]  strValue
 *              config argument
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              not found
 */
bool CConfigOption::Parse (const char* strTest, CString& strValue)
{
    bool bRet = false;

    if (m_strRegEx.Length ())
    {
        regmatch_t atMatch [6];

        if (0 == regexec (&m_tRegex, strTest, 6, atMatch, 0))
        {
            strValue = CString (strTest + atMatch[5].rm_so,
                    (size_t)(atMatch[5].rm_eo - atMatch[5].rm_so));
            bRet = true;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CConfigOption::TestRequired (bool bSilent)
 * @brief       test if required argument was set
 * @param[in]   bSilent
 *              don't log message
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      true
 *              failure
 */
bool CConfigOption::TestRequired (bool bSilent)
{
    bool bRet = false;

    if (m_ulFlags & eFlagsDisabled)
    {
        // is disabled
        bRet = true;
    }
    else if (0 == (m_ulFlags & eFlagsRequired))
    {
        // is optional
        bRet = true;
    }
    else if (m_bConfig || m_bOption)
    {
        // is written
        bRet = true;
    }
    else if (!bSilent)
    {
#ifdef WITH_LONGOPT
        if (m_ulFlags & eFlagsNoLong)
        {
#endif // WITH_LONGOPT
            LogError (NStrErr::ArgMissingOptionS, m_cShort);
#ifdef WITH_LONGOPT
        }
        else if (m_ulFlags & eFlagsNoShort)
        {
            LogError (NStrErr::ArgMissingOptionL, (const char*)m_strLong);
        }
        else
        {
            LogError (NStrErr::ArgMissingOptionSL, m_cShort, (const char*)m_strLong);
        }
#endif // WITH_LONGOPT
    }

    return bRet;
}

/*!
 * @fn          void CConfigOption::GetValue (bool &bValue)
 * @brief       get option value
 * @param[out]  bValue
 *              value
 */
void CConfigOption::GetValue (bool &bValue)
{
    bValue = m_bOption | m_bConfig;
}

/*!
 * @fn          void CConfigOption::GetValue (CString& strValue)
 * @brief       get option value
 * @param[out]  strValue
 *              value
 */
void CConfigOption::GetValue (CString& strValue)
{
    if (m_bOption)
    {
        strValue = m_strOption;
    }
    else if (m_bConfig)
    {
        strValue = m_strConfig;
    }
}

/*!
 * @fn          void CConfigOption::GetValue (int &nValue)
 * @brief       get option value
 * @param[out]  nValue
 *              value
 */
void CConfigOption::GetValue (int &nValue)
{
    if (m_ulFlags & eFlagsInc)
    {
        nValue = m_nInc;
    }
    else
    {
        if (m_bOption)
        {
            nValue = (int)m_strOption.Int ();
        }
        else if (m_bConfig)
        {
            nValue = (int)m_strConfig.Int ();
        }
    }
}

/*!
 * @fn          void CConfigOption::GetValue (unsigned long &ulValue)
 * @brief       get option value
 * @param[out]  ulValue
 *              value
 */
void CConfigOption::GetValue (unsigned long &ulValue)
{
    if (m_bOption)
    {
        ulValue = m_strOption.UInt ();
    }
    else if (m_bConfig)
    {
        ulValue = m_strConfig.UInt ();
    }
}

/*!
 * @fn          void CConfigOption::GetValue (CLog::ELevel &eValue)
 * @brief       get option value
 * @param[out]  eValue
 *              value
 */
void CConfigOption::GetValue (CLog::ELevel &eValue)
{
    if (m_bOption || m_bConfig)
    {
        eValue = CLog::GetLevel (m_bOption ? m_strOption : m_strConfig);
    }
}

/*!
 * @fn          void CConfigOption::GetValue (CProtocol::EEthType &eValue)
 * @brief       get option value
 * @param[out]  eValue
 *              value
 */
void CConfigOption::GetValue (CProtocol::EEthType &eValue)
{
    if (m_bOption || m_bConfig)
    {
        eValue = CProtocol::GetProtocol (m_bOption ? m_strOption : m_strConfig);
    }
}

/*!
 * @fn          void CConfigOption::GetValue (CModem::ECommand &eValue)
 * @brief       get option value
 * @param[out]  eValue
 *              value
 */
void CConfigOption::GetValue (CModem::ECommand &eValue)
{
    if (m_bOption || m_bConfig)
    {
        eValue = CModem::GetCommand (m_bOption ? m_strOption : m_strConfig);
    }
}

/*!
 * @fn          void CConfigOption::PrintHelp (void)
 * @brief       print help
 */
void CConfigOption::PrintHelp (void)
{
    if ((0 == (m_ulFlags & eFlagsDisabled))
#ifndef WITH_LONGOPT
            && (0 == (m_ulFlags & eFlagsNoShort))
#endif // WITH_LONGOPT
       )
    {
        switch (m_eArg)
        {
        case eArgNone:
#ifdef WITH_LONGOPT
            if (m_ulFlags & eFlagsNoLong)
            {
#endif // WITH_LONGOPT
                LogError (NStrConfig::NoArgS, m_cShort);
#ifdef WITH_LONGOPT
            }
            else if (m_ulFlags & eFlagsNoShort)
            {
                LogError (NStrConfig::NoArgL, (const char*)m_strLong);
            }
            else
            {
                LogError (NStrConfig::NoArgSL, m_cShort, (const char*)m_strLong);
            }
#endif // WITH_LONGOPT
            break;
        case eArgRequired:
#ifdef WITH_LONGOPT
            if (m_ulFlags & eFlagsNoLong)
            {
#endif // WITH_LONGOPT
                LogError (NStrConfig::ReqArgS, m_cShort, m_strArg);
#ifdef WITH_LONGOPT
            }
            else if (m_ulFlags & eFlagsNoShort)
            {
                LogError (NStrConfig::ReqArgL, (const char*)m_strLong, m_strArg);
            }
            else
            {
                LogError (NStrConfig::ReqArgSL, m_cShort, m_strArg, (const char*)m_strLong, m_strArg);
            }
#endif // WITH_LONGOPT
            break;
        case eArgOptional:
#ifdef WITH_LONGOPT
            if (m_ulFlags & eFlagsNoLong)
            {
#endif // WITH_LONGOPT
                LogError (NStrConfig::OptArgS, m_cShort, m_strArg);
#ifdef WITH_LONGOPT
            }
            else if (m_ulFlags & eFlagsNoShort)
            {
                LogError (NStrConfig::OptArgL, (const char*)m_strLong, m_strArg);
            }
            else
            {
                LogError (NStrConfig::OptArgSL, m_cShort, m_strArg, (const char*)m_strLong, m_strArg);
            }
#endif // WITH_LONGOPT
            break;
        }
        LogError (m_strHelp, 0);
    }
}

//_oOo_
