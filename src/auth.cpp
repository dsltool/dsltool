/*!
 * @file        auth.cpp
 * @brief       authentication class implementation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        13.04.2019
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include "auth.h"

/*!
 * @fn          CAuth::CAuth (EAuth eAuth)
 * @brief       constructor
 */
CAuth::CAuth (void)
{
}

/*!
 * @fn          CAuthLogin::CAuthLogin (const char *strUser, const char *strPass)
 * @brief       constructor
 * @param[in]   strUser
 *              user name
 * @param[in]   strPass
 *              password
 */
CAuthLogin::CAuthLogin (const char *strUser, const char *strPass)
: CAuth ()
, m_strUser (strUser)
, m_strPass (strPass)
{
}

/*!
 * @fn          const char *CAuthLogin::User (void) const
 * @brief       get user name
 * @return      user name
 */
const char *CAuthLogin::User (void) const
{
    return m_strUser;
}

/*!
 * @fn          const char *CAuthLogin::Pass (void) const
 * @brief       get passwor
 * @return      passwor
 */
const char *CAuthLogin::Pass (void) const
{
    return m_strPass;
}

//_oOo_
