/*!
 * @file        dynlib-w32.cpp
 * @brief       dynamic library implementation
 * @details     dynamic library class
 *              windows implementation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        03.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <windows.h>

#include "dynlib.h"
#include "log.h"

/*!
 * @fn          CDynLib::CDynLib (void)
 * @brief       constructor
 */
CDynLib::CDynLib (void)
: m_pHandle (NULL)
{
}

/*!
 * @fn          CDynLib::CDynLib (void)
 * @brief       destructor
 * @details     close library
 */
CDynLib::~CDynLib (void)
{
    Close ();
}

/*!
 * @fn          bool CDynLib::Open (const char *strName)
 * @brief       open library
 * @param[in]   strName
 *              library name
 * @return      success
 * @retval      true
 *              o.k.
 * @return      false
 *              failure
 */
bool CDynLib::Open (const char *strName)
{
    char strLib[MAX_PATH];

    _snprintf_s (strLib, sizeof(strLib), "dsltool-%s.dll", strName);

    m_pHandle = LoadLibrary (strLib);

    if (!m_pHandle)
    {
        LogError("LoadLibrary (%s) failed: %x\n",
                strLib, GetLastError ());
    }

    return (NULL != m_pHandle);
}

/*!
 * @fn          void CDynLib::Close (void)
 * @brief       close library
 */
void CDynLib::Close (void)
{
    if (m_pHandle)
    {
        FreeLibrary (m_pHandle);
        m_pHandle = NULL;
    }
}

/*!
 * @fn          void *CDynLib::Symbol (const char *strSymbol)
 * @brief       find symbol
 * @param[in]   strSymbol
 *              symbol name
 * @return      symbol address
 * @return      NULL
 *              failure
 */
void *CDynLib::Symbol (const char *strSymbol)
{
    void *pSymbol = NULL;

    if (m_pHandle)
    {
        pSymbol = GetProcAddress (m_pHandle, strSymbol);

        if (!pSymbol)
        {
            LogError ("GetProcAddress (%s) failed: %x\n",
                    strSymbol, GetLastError ());
        }
    }

    return pSymbol;
}

//_oOo_
