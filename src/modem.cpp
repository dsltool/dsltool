/*!
 * @file        modem.cpp
 * @brief       modem base class implementation
 * @details     abstract modem and helper class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "modem.h"
#include "protocol.h"
#include "txt.h"
#include "log.h"

/*!
 * @var         CModemFactory CModemFactory::g_cModemFactory
 * @brief       singleton modem factory object
 */
CModemFactory CModemFactory::g_cModemFactory;

/*!
 * @fn          CModemFactory::CModemFactory (void)
 * @brief       constructor
 */
CModemFactory::CModemFactory (void)
: m_cModem ()
, m_pfnVersion (NULL)
, m_pfnCreate (NULL)
{
}

/*!
 * @fn          CModemFactory::~CModemFactory (void)
 * @brief       destructor
 * @details     unload modem dynamic library
 */
CModemFactory::~CModemFactory (void)
{
    Unload ();
}

/*!
 * @fn          bool CModemFactory::Load (const char *strModem)
 * @brief       load modem dynamic library
 * @param[in]   strModem
 *              modem name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to load library
 */
bool CModemFactory::Load (const char *strModem)
{
    bool bRet = false;

    if (m_cModem.Open (strModem))
    {
        m_pfnVersion = (TpfnVersion) m_cModem.Symbol ("ModemVersion");
        m_pfnCreate = (TpfnCreate) m_cModem.Symbol ("ModemCreate");

        if (m_pfnVersion && m_pfnCreate)
        {
            LogInfo (NStrInfo::LoadModem, strModem);

            bRet = true;
        }
        else
        {
            Unload ();
        }
    }

    return bRet;
}

/*!
 * @fn          void CModemFactory::Unload (void)
 * @brief       unload modem dynamic library
 */
void CModemFactory::Unload (void)
{
    m_cModem.Close ();
    m_pfnVersion = NULL;
    m_pfnCreate = NULL;
}

/*!
 * @fn          bool CModemFactory::TestVersion (const char *strModem)
 * @brief       test version number
 * @details     calls @ref ModemVersion function in dynamic library
 * @param[in]   strModem
 *              modem name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              version missmatch
 */
bool CModemFactory::TestVersion (const char *strModem)
{
    bool bRet = false;
    int nMajor = 0;
    int nMinor = 0;
    int nSub = 0;

    if (m_pfnVersion)
    {
        m_pfnVersion (nMajor, nMinor, nSub);

        if ((VERSION_MAJOR == nMajor) &&
            (VERSION_MINOR == nMinor) &&
            (VERSION_SUB == nSub))
        {
            bRet = true;
        }
    }
    if (!bRet)
    {
        LogError (NStrErr::Version, strModem, nMajor, nMinor, nSub,
                  VERSION_MAJOR, VERSION_MINOR, VERSION_SUB);
    }
    return bRet;
}

/*!
 * @fn          CModem *CModemFactory::Create (CDslData &cData,
 *                      CProtocol *pProtocol)
 * @brief       create modem instance
 * @details     calls @ref ModemCreate function in dynamic library
 * @param[in]   cData
 *              DSL data reference
 * @param[in]   pProtocol
 *              protocol pointer
 * @return      modem pointer
 * @retval      NULL
 *              failed to create modem
 */
CModem *CModemFactory::Create (CDslData &cData,
        CProtocol *pProtocol)
{
    CModem *pModem = NULL;

    if (m_pfnCreate)
    {
        pModem = m_pfnCreate (cData, pProtocol);
    }

    return pModem;
}

/*!
 * @fn          CModem::CModem (CDslData &cData,
 *                      CProtocol *pProtocol)
 * @brief       constructor
 * @param[in]   cData
 *              DSL data reference
 * @param[in]   pProtocol
 *              protocol pointer
 */
CModem::CModem (CDslData &cData,
        CProtocol *pProtocol)
: m_cData (cData)
, m_eCommand (eInfo)
, m_pProtocol (pProtocol)
, m_pLogin (NULL)
, m_strPrompt (">")
, m_strLF ("\n")
, m_nVersion (0)
{
    if (pProtocol)
    {
        if (pProtocol->Auth())
        {
            m_pLogin = dynamic_cast<CAuthLogin*>(pProtocol->Auth());
        }
    }
}

/*!
 * @fn          const char *CModem::GetCommand(CModem::ECommand eCommand)
 * @brief       get command name
 * @param[in]   eCommand
 *              modem command enum
 * @return      command name
 * @retval      NULL
 *              invalid command
 */
const char *CModem::GetCommand (CModem::ECommand eCommand)
{
    switch (eCommand)
    {
    case eInfo:
        return "info";
    case eStatus:
        return "status";
    case eResync:
        return "resync";
    case eReboot:
        return "reboot";
    }
    return NULL;
}

/*!
 * @fn          CModem::ECommand CModem::GetCommand (const CString &strCommand)
 * @brief       get command enum
 * @param[in]   strCommand
 *              modem command name
 * @return      command enum
 */
CModem::ECommand CModem::GetCommand (const CString &strCommand)
{
    if (strCommand.CaseCompare (GetCommand (eInfo)))
    {
        return eInfo;
    }
    if (strCommand.CaseCompare (GetCommand (eStatus)))
    {
        return eStatus;
    }
    if (strCommand.CaseCompare (GetCommand (eResync)))
    {
        return eResync;
    }
    if (strCommand.CaseCompare (GetCommand (eReboot)))
    {
        return eReboot;
    }
    return eInfo;
}

/*!
 * @fn          bool CModem::CommandValid (const CString &strCommand)
 * @brief       test for valid commands
 * @param[in]   strCommand
 *              modem command name
 * @return      valid
 * @retval      true
 *              valid command
 * @retval      true
 *              invalid command
 */
bool CModem::CommandValid (const CString &strCommand)
{
    if (strCommand.CaseCompare (GetCommand (eInfo)))
    {
        return true;
    }
    if (strCommand.CaseCompare (GetCommand (eStatus)))
    {
        return true;
    }
    if (strCommand.CaseCompare (GetCommand (eResync)))
    {
        return true;
    }
    if (strCommand.CaseCompare (GetCommand (eReboot)))
    {
        return true;
    }
    return false;
}

/*!
 * @fn          long CModem::BuildVersion (long nMajor, long nMinor,
 *                      long nSub, long nSubSub)
 * @brief       create version number
 * @param[in]   nMajor
 *              major version number
 * @param[in]   nMinor
 *              minor version number
 * @param[in]   nSub
 *              sub version number
 * @param[in]   nSubSub
 *              sub sub version number
 * @return      version number
 */
long CModem::BuildVersion (long nMajor, long nMinor,
        long nSub, long nSubSub)
{
    return ((nMajor & 0xff)  << 24) + ((nMinor & 0xff)  << 16) +
                ((nSub & 0xff)  << 8) + (nSubSub & 0xff);
}

/*!
 * @fn          void CModem::Init (ECommand eCommand)
 * @brief       initialization
 * @note        this function must be called when @ref Init is overloaded
 * @details     invalidate data storage
 * @param[in]   eCommand
 *              command
 */
void CModem::Init (ECommand eCommand)
{
    m_eCommand = eCommand;
    m_cData.Init ();
}

/*!
 * @fn          void CModem::Connect (void)
 * @brief       initialization
 * @note        this function can be called when @ref Connect is overloaded
 * @details     does nothing
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CModem::Connect (void)
{
    return true;
}

/*!
 * @fn          const char *CModem::User (void)
 * @brief       get user name
 * @return      user name
 */
const char *CModem::User (void)
{
    const char *strUser = NULL;

    if (m_pLogin)
    {
        strUser = m_pLogin->User ();
    }

    return strUser;
}

/*!
 * @fn          const char *CModem::Pass (void)
 * @brief       get password
 * @return      password
 */
const char *CModem::Pass (void)
{
    const char *strPass = NULL;

    if (m_pLogin)
    {
        strPass = m_pLogin->Pass ();
    }

    return strPass;
}

//_oOo_
