<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
   version="2.0" xmlns:src="src.xsl" xmlns:common="common.xsl">

   <xsl:output method="text" encoding="UTF-8"/>
   <xsl:strip-space elements="*"/>

   <xsl:include href="common.xsl"/>
   <xsl:variable name="CONT" select="concat ($CR, $ID, $ID)"/>
   <xsl:variable name="str-command" select="//strings/command"/>
   <xsl:variable name="str-namespace" select="//strings/namespace"/>
   <xsl:variable name="str-xpath" select="//strings/xpath"/>
   
   <xsl:template match="/">
      <xsl:call-template name="src-header"/>

      <xsl:apply-templates/>

      <xsl:call-template name="src-footer"/>
   </xsl:template>

   <xsl:template match="modem">
      <xsl:apply-templates select="strings"/>
      
      <xsl:call-template name="modem-header"/>

      <xsl:apply-templates select="versions"/>

      <xsl:call-template name="class-modem"/>

      <xsl:apply-templates select="login"/>

      <xsl:apply-templates select="prompt"/>

      <xsl:apply-templates select="class"/>
   </xsl:template>

   <xsl:template match="strings">
      <xsl:apply-templates select="command"/>
      <xsl:if test="namespace">
         <xsl:value-of select="$CR"/>
      </xsl:if>
      <xsl:apply-templates select="namespace"/>
      <xsl:if test="xpath">
         <xsl:value-of select="$CR"/>
      </xsl:if>
      <xsl:apply-templates select="xpath"/>

      <xsl:if test="user">
         <xsl:value-of select="concat($CR, $USER)"/>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="command">
      <xsl:value-of select="concat('static const char *s_strCmd', @name, ' = &quot;', @cmd, '&quot;;', $CR)"/>
   </xsl:template>

   <xsl:template match="namespace">
      <xsl:value-of select="concat('static const char *s_strNamespace', @name, ' = &quot;', @uri, '&quot;;', $CR)"/>
   </xsl:template>

   <xsl:template match="xpath">
      <xsl:value-of select="concat('static const char *s_strXPath', @name, ' = &quot;', @xpath, '&quot;;', $CR)"/>
   </xsl:template>
   
   <xsl:template match="versions">
      <xsl:value-of select="$CR"/>
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="version">
      <xsl:value-of select="concat('const long ', $modem-class, '::s_nVersion')"/>
      <xsl:value-of select="concat(@name, ' = CModem::BuildVersion (', @version, ');', $CR)"/>
   </xsl:template>

   <xsl:template match="login">
      <xsl:if test="@user">
         <xsl:call-template name="class-login-user"/>
      </xsl:if>
      <xsl:if test="@pass">
         <xsl:call-template name="class-login-pass"/>
      </xsl:if>
   </xsl:template>

   <xsl:template match="prompt">
      <xsl:choose>
         <xsl:when test="//settings/@parser='regex'">
            <xsl:variable name="init">
               <xsl:value-of select="'CRegExCmdParser&lt;T, CLineParser&gt; (pModem, strCmd)'"/>
               <xsl:value-of select="concat($CR, ', m_cPrompt ()')"/>
            </xsl:variable>
            <xsl:call-template name="src-class-ctor-begin">
               <xsl:with-param name="template" select="'class T'"/>
               <xsl:with-param name="template-use" select="'T'"/>
               <xsl:with-param name="class" select="'CPromptParser'"/>
               <xsl:with-param name="param" select="'CModem *pModem, const char *strCmd'"/>
               <xsl:with-param name="init" select="$init"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="//settings/@parser='sax'">
            <xsl:variable name="init">
               <xsl:value-of select="'CRegExCmdParser&lt;T, CSaxParser&gt; (pModem, strCmd)'"/>
               <xsl:value-of select="concat($CR, ', m_cPrompt ()')"/>
            </xsl:variable>
            <xsl:call-template name="src-class-ctor-begin">
               <xsl:with-param name="template" select="'class T'"/>
               <xsl:with-param name="template-use" select="'T'"/>
               <xsl:with-param name="class" select="'CPromptParser'"/>
               <xsl:with-param name="param" select="'CModem *pModem, const char *strCmd'"/>
               <xsl:with-param name="init" select="$init"/>
            </xsl:call-template>
         </xsl:when>
      </xsl:choose>

      <xsl:choose>
         <xsl:when test="//settings/@parser='dom'"/>
         <xsl:otherwise>
            <xsl:for-each select="*">
               <xsl:call-template name="add-regex">
                  <xsl:with-param name="var" select="'Prompt'"/>
                  <xsl:with-param name="idx" select="''"/>
                  <xsl:with-param name="id" select="$ID"/>
                  <xsl:with-param name="count" select="@count"/>
               </xsl:call-template>
            </xsl:for-each>
            <xsl:value-of select="concat($ID, 'm_cPrompt.Init ();', $CR)"/>
            <xsl:call-template name="src-func-end"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template match="class">
      <xsl:variable name="class" select="concat('C', @name)"/>
      <xsl:variable name="init">
         <xsl:choose>
            <xsl:when test="//settings/@parser='dom'">
               <xsl:value-of select="'CDomCmdParser (pModem, strCmd)'"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="'CPromptParser (pModem, strCmd)'"/>
               <xsl:for-each select="vars/var">
                  <xsl:value-of select="concat($CR, ', m_', @name, ' (')"/>
                  <xsl:choose>
                     <xsl:when test="@type='CString'"/>
                     <xsl:when test="@type='CTone'"/>
                     <xsl:when test="@type='bool'">
                        <xsl:value-of select="'false'"/>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="'0'"/>
                     </xsl:otherwise>
                  </xsl:choose>
                  <xsl:value-of select="')'"/>
               </xsl:for-each>
               <xsl:for-each select="parser/test">
                  <xsl:variable name="name" select="@name"/>
                  <xsl:choose>
                     <xsl:when test="regex/loop[1]/@sub">
                        <xsl:variable name="count" select="regex/loop[1]/@count" as="xs:integer"/>
                        <xsl:variable name="sub" select="regex/loop[1]/@sub" as="xs:integer"/>
                        <xsl:variable name="max" as="xs:integer" select="$count div $sub - 1"/>
                        <xsl:for-each select="0 to $max">
                           <xsl:variable name="pos" select="." as="xs:integer"/>
                           <xsl:variable name="idx" as="xs:integer" select="$count - $sub * $pos"/>
                           <xsl:value-of select="concat($CR, ', m_c', $name, '_', $idx, ' ()')"/>
                        </xsl:for-each>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="concat($CR, ', m_c', $name, ' ()')"/>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:for-each>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:call-template name="src-class-ctor-begin">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="'CModem *pModem, const char *strCmd'"/>
         <xsl:with-param name="init" select="$init"/>
      </xsl:call-template>
      <xsl:if test="vars/var[@type='CTone']">
         <xsl:for-each select="vars/var[@type='CTone']">
            <xsl:value-of select="concat($ID, 'm_', @name)"/>
            <xsl:value-of select="'.Size (m_pModem-&gt;Data ().Bandplan ()-&gt;Tones ());'"/>
            <xsl:value-of select="$CR"/>
         </xsl:for-each>
         <xsl:value-of select="$CR"/>
      </xsl:if>
      <xsl:for-each select="parser/test">
         <xsl:call-template name="test-var">
            <xsl:with-param name="class" select="$class"/>
         </xsl:call-template>
      </xsl:for-each>
      <xsl:for-each select="parser/test-prompt/set">
         <xsl:if test="condition">
            <xsl:variable name="cond" select="condition/@name"/>
            <xsl:for-each select="condition/cond">
               <xsl:value-of select="concat($ID, 'AddCallback (m_cPrompt, &amp;', $class, '::', $cond, 'Callback')"/>
               <xsl:if test="@state">
                  <xsl:value-of select="concat(', e', @state)"/>
               </xsl:if>
               <xsl:value-of select="concat(');', $CR)"/>
            </xsl:for-each>
         </xsl:if>
      </xsl:for-each>
      <xsl:choose>
         <xsl:when test="//settings/@parser='dom'"/>
         <xsl:otherwise>
            <xsl:value-of select="concat($ID, 'AddCallback (m_cPrompt, &amp;', $class, '::PromptCallback);', $CR)"/>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="concat($CR, $USER)"/>
      <xsl:call-template name="src-func-end"/>

      <xsl:apply-templates select="parser"/>
      <xsl:apply-templates select="domparser"/>
   </xsl:template>

   <xsl:template match="parser">
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="domparser">
      <xsl:call-template name="src-class-func-begin">
         <xsl:with-param name="class" select="concat('C', ../@name)"/>
         <xsl:with-param name="return" select="'CParser *'"/>
         <xsl:with-param name="func" select="'Finished'"/>
         <xsl:with-param name="param" select="'void'"/>
      </xsl:call-template>
      <xsl:apply-templates select="../vars"/>
      <xsl:call-template name="set-loop">
         <xsl:with-param name="set" select="*"/>
         <xsl:with-param name="id" select="$ID"/>
         <xsl:with-param name="mp" select="''"/>
      </xsl:call-template>
      <xsl:call-template name="src-func-end"/>
   </xsl:template>

   <xsl:template match="vars">
      <xsl:apply-templates select="var"/>
   </xsl:template>
   <xsl:template match="var">
      <xsl:value-of select="concat($ID, @type, ' ', @name)"/>
      <xsl:choose>
         <xsl:when test="@type='bool'">
            <xsl:value-of select="' = false'"/>
         </xsl:when>
         <xsl:when test="@type='long'">
            <xsl:value-of select="' = 0'"/>
         </xsl:when>
         <xsl:when test="@type='double'">
            <xsl:value-of select="' = 0.0'"/>
         </xsl:when>
      </xsl:choose>
      <xsl:value-of select="concat(';', $CR)"/>
   </xsl:template>
   
   <xsl:template match="test">
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="test-prompt">
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="test-next">
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="test/set">
      <xsl:call-template name="test-set">
         <xsl:with-param name="class" select="concat('C', ../../../@name)"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="test-prompt/set">
      <xsl:call-template name="test-prompt-set">
         <xsl:with-param name="class" select="concat('C', ../../../@name)"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="test-next/set">
      <xsl:call-template name="test-next-set">
         <xsl:with-param name="class" select="concat('C', ../../../@name)"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="class-modem">
      <xsl:call-template name="src-class-ctor-begin">
         <xsl:with-param name="class" select="$modem-class"/>
         <xsl:with-param name="param"
            select="concat('CDslData &amp;cData,', $CONT, 'CProtocol *pProtocol')"/>
         <xsl:with-param name="init" select="'CModem (cData, pProtocol)'"/>
         <xsl:with-param name="user" select="true()"/>
      </xsl:call-template>

      <xsl:value-of select="$USER"/>
      <xsl:call-template name="src-func-end"/>

      <!-- begin Init() -->
      <xsl:call-template name="src-class-func-begin">
         <xsl:with-param name="class" select="$modem-class"/>
         <xsl:with-param name="return" select="'void '"/>
         <xsl:with-param name="func" select="'Init'"/>
         <xsl:with-param name="param" select="'ECommand eCommand'"/>
      </xsl:call-template>
      <!--  parser -->
      <xsl:value-of
         select="concat($ID, 'CParser *pFirst = NULL;', $CR, $CR)"/>
      <xsl:value-of select="concat($ID, 'CModem::Init (eCommand);', $CR, $CR)"/>

      <!-- prompt -->
      <xsl:value-of
         select="concat($ID, 'm_strPrompt.Format (&quot;', prompt/@format , '&quot;')"/>
      <xsl:choose>
         <xsl:when test="prompt/@par0='user'">
            <xsl:value-of select="', User ()'"/>
         </xsl:when>
      </xsl:choose>
      <xsl:value-of
         select="concat(');', $CR)"/>
      
      <!-- LF -->
      <xsl:value-of select="concat($ID, 'm_strLF.Format (&quot;', settings/@LF, '&quot;);', $CR)"/>
      <xsl:value-of select="concat($CR, $USER)"/>

      <xsl:if test="login/@user or login/@pass">
         <xsl:value-of select="concat($CR, $ID, 'if (m_pLogin)', $CR, $ID, '{', $CR)"/>
         <xsl:choose>
            <xsl:when test="login/@user">
               <xsl:value-of select="concat($ID, $ID, 'pFirst = new ')"/>
               <xsl:value-of select="'CLoginUser (this, User ());'"/>
            </xsl:when>
            <xsl:when test="login/@pass">
               <xsl:value-of select="concat($ID, $ID, 'pFirst = new ')"/>
               <xsl:value-of select="'CLoginPass (this, Pass ());'"/>
            </xsl:when>
         </xsl:choose>
         <xsl:value-of select="concat($CR, $ID, '}', $CR)"/>
         <xsl:call-template name="return">
            <xsl:with-param name="set" select="login/return"/>
            <xsl:with-param name="id" select="$ID"/>
            <xsl:with-param name="next" select="true()"/>
            <xsl:with-param name="first" select="true()"/>
         </xsl:call-template>
      </xsl:if>
      
      <!-- call parser -->
      <xsl:value-of select="concat($CR, $ID, 'Protocol ()-&gt;Next (pFirst);', $CR)"/>
      <xsl:call-template name="src-func-end"/>
      <!-- end Init() -->

      <xsl:if test="(//settings/@parser='sax') or (//settings/@parser='dom')">
         <!-- begin Connect() -->
         <xsl:call-template name="src-class-func-begin">
            <xsl:with-param name="class" select="$modem-class"/>
            <xsl:with-param name="return" select="'bool '"/>
            <xsl:with-param name="func" select="'Connect'"/>
            <xsl:with-param name="param" select="'void'"/>
         </xsl:call-template>
         <xsl:value-of select="$USER"/>
         <xsl:call-template name="src-func-end"/>
         <!-- end Connect() -->
      </xsl:if>

      <xsl:if test="bandplan">
         <xsl:call-template name="class-bandplan"/>
      </xsl:if>
      
      <xsl:value-of select="concat($CR, $USER)"/>
   </xsl:template>

   <xsl:template name="class-bandplan">
      <xsl:variable name="class" select="'CBandplanData'"/>
      <xsl:value-of select="concat($CR, 'const ', $modem-class, '::', $class)"/>
      <xsl:value-of select="concat(' ', $modem-class, '::', $class, '::g_acList[] = {')"/>
      <xsl:value-of select="concat($CR, $USER, '};', $CR)"/>
      
      <xsl:variable name="param">
         <xsl:for-each select="bandplan/var">
            <xsl:choose>
               <xsl:when test="@type = 'string'">
                  <xsl:value-of select="concat('const char *', @name)"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="concat(@type, ' ', @name)"/>
               </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="position()!=last()">
               <xsl:value-of select="concat(',', $CR, $ID, $ID)"/>
            </xsl:if>
         </xsl:for-each>
      </xsl:variable>
      <xsl:variable name="init">
         <xsl:for-each select="bandplan/var">
            <xsl:value-of select="concat('m_', @name, ' (', @name, ')', $CR,', ')"/>
         </xsl:for-each>
         <xsl:value-of select="'m_eBandplan (eBandplan)'"/>
      </xsl:variable>
      <xsl:call-template name="src-class-ctor-begin">
         <xsl:with-param name="parent-class" select="$modem-class"/>
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat($param, ', CBandplan::EBandplan eBandplan')"/>
         <xsl:with-param name="init" select="$init"/>
      </xsl:call-template>
      <xsl:call-template name="src-func-end"/>
      
      <xsl:variable name="init">
         <xsl:for-each select="bandplan/var">
            <xsl:value-of select="concat('m_', @name, ' (cBandplanData.m_', @name, ')', $CR,', ')"/>
         </xsl:for-each>
         <xsl:value-of select="'m_eBandplan (cBandplanData.m_eBandplan)'"/>
      </xsl:variable>
      <xsl:call-template name="src-class-ctor-begin">
         <xsl:with-param name="parent-class" select="$modem-class"/>
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
         <xsl:with-param name="init" select="$init"/>
      </xsl:call-template>
      <xsl:call-template name="src-func-end"/>

      <xsl:call-template name="src-class-func-begin">
         <xsl:with-param name="parent-class" select="$modem-class"/>
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="func" select="'Get'"/>
         <xsl:with-param name="return" select="'CBandplan::EBandplan '"/>
         <xsl:with-param name="param" select="$param"/>
      </xsl:call-template>
      <xsl:value-of select="concat($ID, 'CBandplan::EBandplan eBandplan = CBandplan::eNone;', $CR, $CR)"/>
      <xsl:value-of select="concat($ID, 'for (size_t nIndex = 0; nIndex &lt; ARRAY_SIZE (g_acList); nIndex++)', $CR)"/>
      <xsl:value-of select="concat($ID, '{', $CR)"/>
      <xsl:value-of select="$USER"/>
      <xsl:value-of select="concat($ID, '}', $CR, $CR)"/>
      <xsl:value-of select="concat($ID, 'return eBandplan;', $CR)"/>
      <xsl:call-template name="src-func-end"/>

      <xsl:call-template name="src-class-func-begin">
         <xsl:with-param name="class" select="$modem-class"/>
         <xsl:with-param name="func" select="'Bandplan'"/>
         <xsl:with-param name="return" select="'CBandplan::EBandplan '"/>
         <xsl:with-param name="param" select="$param"/>
      </xsl:call-template>
      <xsl:value-of select="concat ($ID, 'return ', $class, '::Get(')"/>
      <xsl:for-each select="bandplan/var">
            <xsl:value-of select="@name"/>
         <xsl:if test="position()!=last()">
            <xsl:value-of select="', '"/>
         </xsl:if>
      </xsl:for-each>
      <xsl:value-of select="concat (');', $CR)"/>
      <xsl:call-template name="src-func-end"/>
   </xsl:template>
   
   <xsl:template name="class-login-user">
      <xsl:call-template name="class-login-begin">
         <xsl:with-param name="name" select="'User'"/>
      </xsl:call-template>

      <xsl:value-of select="concat($ID, 'if (Find (strRecv, nRecv, &quot;', @user, '&quot;))', $CR)"/>
      <xsl:value-of select="concat($ID, '{', $CR)"/>
      <xsl:value-of select="concat($ID, $ID, 'm_pModem-&gt;Protocol ()-&gt;Send (m_strUser, &quot;user&quot;);', $CR)"/>
      <xsl:value-of select="concat($ID, $ID, 'm_pModem-&gt;Protocol ()-&gt;Send (m_pModem-&gt;LF ());', $CR, $CR)"/>
      <xsl:value-of select="concat($ID, $ID, 'pNext = new CLoginPass (m_pModem, m_pModem-&gt;Pass ());', $CR)"/>
      <xsl:value-of select="concat($ID, '}', $CR)"/>

      <xsl:call-template name="class-login-end"/>
   </xsl:template>

   <xsl:template name="class-login-pass">
      <xsl:call-template name="class-login-begin">
         <xsl:with-param name="name" select="'Pass'"/>
      </xsl:call-template>

      <xsl:if test="@pass">
         <xsl:value-of select="concat($ID, 'if (Find (')"/>
         <xsl:choose>
            <xsl:when test="@line">
               <xsl:value-of select="'strLine, strlen (strLine)'"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="'strRecv, nRecv'"/>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:value-of select="concat(', &quot;', @pass, '&quot;))', $CR)"/>
         <xsl:value-of select="concat($ID, '{', $CR)"/>
         <xsl:value-of select="concat($ID, $ID, 'm_pModem-&gt;Protocol ()-&gt;Send (m_strPass, &quot;pass&quot;);', $CR)"/>
         <xsl:value-of select="concat($ID, $ID, 'm_pModem-&gt;Protocol ()-&gt;Send (m_pModem-&gt;LF ());', $CR)"/>
         <xsl:value-of select="concat($ID, '}', $CR)"/>
      </xsl:if>
      <xsl:value-of select="$ID"/>
      <xsl:if test="@pass">
         <xsl:value-of select="'else '"/>
      </xsl:if>
      <xsl:value-of select="'if (Find ('"/>
      <xsl:choose>
         <xsl:when test="@line">
            <xsl:value-of select="'strLine, strlen (strLine)'"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="'strRecv, nRecv'"/>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="concat(', m_pModem-&gt;Prompt ()))', $CR)"/>
      <xsl:value-of select="concat($ID, '{', $CR)"/>
      <xsl:call-template name="set-loop">
         <xsl:with-param name="set" select="*"/>
         <xsl:with-param name="next" select="true()"/>
         <xsl:with-param name="id" select="concat ($ID, $ID)"/>
         <xsl:with-param name="mp" select="'m_'"/>
      </xsl:call-template>
      <xsl:value-of select="concat($ID, '}', $CR)"/>

      <xsl:call-template name="class-login-end"/>
   </xsl:template>

   <xsl:template name="class-login-begin">
      <xsl:param name="name" as="xs:string" required="yes"/>
      <xsl:variable name="class" select="concat ('CLogin', $name)"/>

      <xsl:choose>
         <xsl:when test="@line">
            <xsl:call-template name="src-class-ctor-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="param" select="concat('CModem *pModem, const char *str', $name)"/>
               <xsl:with-param name="init"
                  select="concat('CLineParser (pModem)', $CR, ', m_str', $name, ' (str', $name, ')')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="src-class-ctor-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="param" select="concat('CModem *pModem, const char *str', $name)"/>
               <xsl:with-param name="init"
                  select="concat('CParser (pModem)', $CR, ', m_str', $name, ' (str', $name, ')')"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="src-func-end"/>

      <xsl:choose>
         <xsl:when test="@line">
            <xsl:call-template name="src-class-func-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="func" select="'ParseLine'"/>
               <xsl:with-param name="return" select="'CParser *'"/>
               <xsl:with-param name="param" select="'const char *strLine'"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="src-class-func-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="func" select="'Parse'"/>
               <xsl:with-param name="return" select="'CParser *'"/>
               <xsl:with-param name="param" select="'const char *strRecv, size_t nRecv'"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="concat($ID, 'CParser *pNext = this;', $CR, $CR)"/>
   </xsl:template>

   <xsl:template name="class-login-end">
      <xsl:value-of select="concat($CR, $ID, 'return pNext;', $CR)"/>
      <xsl:call-template name="src-func-end"/>
   </xsl:template>

   <xsl:template name="test-set">
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:variable name="name" select="../@name"/>
      <xsl:variable name="cond" select="condition/@name"/>
      <xsl:call-template name="src-class-func-begin">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="return" select="'CParser *'"/>
         <xsl:with-param name="func" select="concat($name, $cond, 'Callback')"/>
         <xsl:with-param name="param" select="'void'"/>
      </xsl:call-template>
      <xsl:call-template name="set-loop">
         <xsl:with-param name="set" select="*"/>
         <xsl:with-param name="id" select="$ID"/>
         <xsl:with-param name="mp" select="'m_'"/>
      </xsl:call-template>
      <xsl:call-template name="src-func-end"/>
   </xsl:template>

   <xsl:template name="test-prompt-set">
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:if test="condition">
         <xsl:variable name="cond" select="condition/@name"/>
         <xsl:for-each select="condition/cond">
            <xsl:call-template name="src-class-func-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="return" select="'CParser *'"/>
               <xsl:with-param name="func" select="concat($cond, 'Callback')"/>
               <xsl:with-param name="param" select="'void'"/>
            </xsl:call-template>
            <xsl:call-template name="set-loop">
               <xsl:with-param name="set" select="../../*"/>
               <xsl:with-param name="id" select="$ID"/>
               <xsl:with-param name="mp" select="'m_'"/>
            </xsl:call-template>
            <xsl:call-template name="src-func-end"/>
         </xsl:for-each>
      </xsl:if>
   </xsl:template>

   <xsl:template name="test-next-set">
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:call-template name="src-class-func-begin">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="return" select="'CParser *'"/>
         <xsl:with-param name="func" select="'PromptCallback'"/>
         <xsl:with-param name="param" select="'void'"/>
      </xsl:call-template>
      <xsl:call-template name="set-loop">
         <xsl:with-param name="set" select="*"/>
         <xsl:with-param name="id" select="$ID"/>
         <xsl:with-param name="mp" select="'m_'"/>
      </xsl:call-template>
      <xsl:call-template name="src-func-end"/>
   </xsl:template>

   <xsl:template name="set-loop">
      <xsl:param name="set" required="yes"/>
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:param name="next" required="no" as="xs:boolean" select="false()"/>
      
      <xsl:variable name="modem" as="xs:string" select="'m_pModem'"/>
      
      <xsl:if test="(count($set[name()='return']) &gt; 1) and not($next)">
         <xsl:value-of select="concat($id, 'CParser *pNext = this;', $CR)"/>
      </xsl:if>
      <xsl:for-each select="$set[name() != 'return']">
         <xsl:call-template name="callback-switch">
            <xsl:with-param name="id" select="$id"/>
            <xsl:with-param name="mp" select="$mp"/>
         </xsl:call-template>
      </xsl:for-each>

      <xsl:choose>
         <xsl:when test="count ($set[name()='return']) = 0">
            <xsl:if test="count($set) &gt; 0">
               <xsl:value-of select="$CR"/>
            </xsl:if>
            <xsl:value-of select="concat($id, 'return this;', $CR)"/>
         </xsl:when>
         <xsl:when test="(count ($set[name()='return']) = 1) and not($next)">
            <xsl:if test="count($set) &gt; 1">
               <xsl:value-of select="$CR"/>
            </xsl:if>
            <xsl:variable name="return" select="$set[name()='return']"/>
            <xsl:value-of select="concat($id, 'return ', src:return($return, $modem), ';', $CR)"/>
         </xsl:when>
         <xsl:when test="(count ($set[name()='return']) = 1) and $next">
            <xsl:if test="count($set) &gt; 1">
               <xsl:value-of select="$CR"/>
            </xsl:if>
            <xsl:variable name="return" select="$set[name()='return']"/>
            <xsl:value-of select="concat($id, 'pNext = ', src:return($return, $modem), ';', $CR)"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="return">
               <xsl:with-param name="set" select="$set[name()='return']"/>
               <xsl:with-param name="id" select="$id"/>
               <xsl:with-param name="next" select="$next"/>
            </xsl:call-template>
            <xsl:if test="not ($next)">
               <xsl:value-of select="concat($CR, $id, 'return pNext;', $CR)"/>
            </xsl:if>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="return">
      <xsl:param name="set" required="yes"/>
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="next" required="yes" as="xs:boolean"/>
      <xsl:param name="first" required="no" as="xs:boolean" select="false()"/>
      <xsl:for-each select="$set">
         <xsl:choose>
            <xsl:when test="(position() = 1) and not($next) and not($first)">
               <xsl:value-of select="concat($CR, $id)"/>
            </xsl:when>
            <xsl:when test="(position() = 1) and $next and not($first)">
               <xsl:value-of select="$id"/>
            </xsl:when>
            <xsl:when test="(position() = last()) and not(@exec or @version or @user) and not($first)">
               <xsl:value-of select="concat($id, 'else')"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="concat($id, 'else ')"/>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:choose>
            <xsl:when test="$first">
               <xsl:value-of select="concat('if (', common:exec(@exec))"/>
               <xsl:value-of select="concat(' == eCommand)', $CR)"/>
            </xsl:when>
            <xsl:when test="@exec and @version and @user">
               <xsl:value-of select="concat('if ((', common:exec(@exec))"/>
               <xsl:value-of select="concat(' == m_pModem-&gt;Command ()) &amp;&amp;', $CR)"/>
               <xsl:value-of select="concat($id, $ID, '(m_pModem-&gt;Version () &lt; ')"/>
               <xsl:value-of select="concat($modem-class, '::s_nVersion', @version, ') &amp;&amp;', $CR)"/>
               <xsl:value-of select="concat($id, $ID, '(', @user, ')', $CR)"/>
            </xsl:when>
            <xsl:when test="@exec and @version">
               <xsl:value-of select="concat('if ((', common:exec(@exec))"/>
               <xsl:value-of select="concat(' == m_pModem-&gt;Command ()) &amp;&amp;', $CR)"/>
               <xsl:value-of select="concat($id, $ID, '(m_pModem-&gt;Version () &lt; ')"/>
               <xsl:value-of select="concat($modem-class, '::s_nVersion', @version, '))', $CR)"/>
            </xsl:when>
            <xsl:when test="@exec and @user">
               <xsl:value-of select="concat('if ((', common:exec(@exec))"/>
               <xsl:value-of select="concat(' == m_pModem-&gt;Command ()) &amp;&amp;', $CR)"/>
               <xsl:value-of select="concat($id, $ID, '(', @user, '))', $CR)"/>
            </xsl:when>
            <xsl:when test="@version and @user">
               <xsl:value-of select="concat('if ', '((m_pModem-&gt;Version () &lt; ')"/>
               <xsl:value-of select="concat($modem-class, '::s_nVersion', @version, ') &amp;&amp;', $CR)"/>
               <xsl:value-of select="concat($id, $ID, '(', @user, '))', $CR)"/>
            </xsl:when>
            <xsl:when test="@exec">
               <xsl:value-of select="concat('if (', common:exec(@exec))"/>
               <xsl:value-of select="concat(' == m_pModem-&gt;Command ())', $CR)"/>
            </xsl:when>
            <xsl:when test="@version">
               <xsl:value-of select="concat('if ', '(m_pModem-&gt;Version () &lt; ')"/>
               <xsl:value-of select="concat($modem-class, '::s_nVersion', @version, ')', $CR)"/>
            </xsl:when>
            <xsl:when test="@user">
               <xsl:value-of select="concat('if (', @user, ')', $CR)"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="$CR"/>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:value-of select="concat($id, '{', $CR)"/>
         <xsl:choose>
            <xsl:when test="$first">
               <xsl:value-of select="concat($id, $ID, 'pFirst = ', src:return(., 'this'), ';', $CR)"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="concat($id, $ID, 'pNext = ', src:return(., 'm_pModem'), ';', $CR)"/>
            </xsl:otherwise>
         </xsl:choose>
         <xsl:value-of select="concat($id, '}', $CR)"/>
      </xsl:for-each>
   </xsl:template>
   
   <xsl:template name="callback-switch">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:choose>
         <xsl:when test="name()='user'">
            <xsl:value-of select="$USER"/>
         </xsl:when>
         <xsl:when test="name()='state'">
            <xsl:value-of select="concat($id, 'm_nState = e' , @name, ';', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='init'">
            <xsl:call-template name="callback-init">
               <xsl:with-param name="id" select="$id"/>
               <xsl:with-param name="mp" select="$mp"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="name()='flag'">
            <xsl:value-of select="concat($id , $mp, @var , ' = true;', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='var'">
            <xsl:call-template name="callback-var">
               <xsl:with-param name="id" select="$id"/>
               <xsl:with-param name="mp" select="$mp"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="name()='array'">
            <xsl:call-template name="callback-array">
               <xsl:with-param name="id" select="$id"/>
               <xsl:with-param name="mp" select="$mp"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="name()='version'">
            <xsl:call-template name="callback-version">
               <xsl:with-param name="id" select="$id"/>
               <xsl:with-param name="mp" select="$mp"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="name()='scale'">
            <xsl:call-template name="callback-scale">
               <xsl:with-param name="id" select="$id"/>
               <xsl:with-param name="mp" select="$mp"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="name()='tone'">
            <xsl:value-of select="concat ($id, common:dsl-data(@dsl), '.Set', @set)"/>
            <xsl:value-of select="concat (' (', $mp, @var)"/>
            <xsl:value-of select="concat(');', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='band-up'">
            <xsl:value-of select="concat ($id, common:dsl-data(@dsl), '.Set', @set)"/>
            <xsl:value-of select="concat (' (', $mp, @var, ',', $CR)"/>
            <xsl:value-of select="concat ($id, $ID, $ID, '(size_t)pBand-&gt;Band (nBand).MinUp (),', $CR)"/>
            <xsl:value-of select="concat ($id, $ID, $ID, '(size_t)pBand-&gt;Band (nBand).MaxUp ());', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='band-down'">
            <xsl:value-of select="concat ($id, common:dsl-data(@dsl), '.Set', @set)"/>
            <xsl:value-of select="concat (' (', $mp, @var, ',', $CR)"/>
            <xsl:value-of select="concat ($id, $ID, $ID, '(size_t)pBand-&gt;Band (nBand).MinDown (),', $CR)"/>
            <xsl:value-of select="concat ($id, $ID, $ID, '(size_t)pBand-&gt;Band (nBand).MaxDown ());', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='if'">
            <xsl:call-template name="callback-if">
               <xsl:with-param name="id" select="$id"/>
               <xsl:with-param name="mp" select="$mp"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="name()='trace'">
            <xsl:call-template name="callback-trace">
               <xsl:with-param name="id" select="$id"/>
               <xsl:with-param name="mp" select="$mp"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="name()='namespace'">
            <xsl:call-template name="callback-namespace">
               <xsl:with-param name="id" select="$id"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="name()='xpath'">
            <xsl:call-template name="callback-xpath">
               <xsl:with-param name="id" select="$id"/>
            </xsl:call-template>
         </xsl:when>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="callback-init">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:variable name="var" select="@var"/>
      <xsl:variable name="type" select="../../../../vars/var[@name=$var]/@type"/>
      <xsl:choose>
         <xsl:when test="$type='bool'">
            <xsl:value-of select="concat($id , $mp, @var , ' = false;', $CR)"/>
         </xsl:when>
         <xsl:when test="$type='CTone'">
            <xsl:value-of select="concat($id , $mp, @var , '.Init ();', $CR)"/>
         </xsl:when>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="callback-var">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:variable name="cast">
         <xsl:if test="@cast">
            <xsl:value-of select="concat('(', @cast, ')')"/>
         </xsl:if>
      </xsl:variable>
      <xsl:value-of select="concat ($id, common:dsl-data(@dsl), '.Set', @set)"/>
      <xsl:value-of select="concat (' (', $cast, $mp, @var0)"/>
      <xsl:if test="@var1">
         <xsl:value-of select="concat (', ', $cast, $mp, @var1)"/>
      </xsl:if>
      <xsl:if test="@var2">
         <xsl:value-of select="concat (', ', $cast, $mp, @var2)"/>
      </xsl:if>
      <xsl:if test="@var3">
         <xsl:value-of select="concat (', ', $cast, $mp, @var3)"/>
      </xsl:if>
      <xsl:value-of select="concat(');', $CR)"/>
   </xsl:template>

   <xsl:template name="callback-array">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:value-of select="concat ($id, common:dsl-data(@dsl), '.Set', @set)"/>
      <xsl:value-of select="concat (' (', @idx, ', ', $mp, @var0)"/>
      <xsl:if test="@var1">
         <xsl:value-of select="concat (', ', $mp, @var1)"/>
      </xsl:if>
      <xsl:if test="@var2">
         <xsl:value-of select="concat (', ', $mp, @var2)"/>
      </xsl:if>
      <xsl:if test="@var3">
         <xsl:value-of select="concat (', ', $mp, @var3)"/>
      </xsl:if>
      <xsl:value-of select="concat(');', $CR)"/>
   </xsl:template>

   <xsl:template name="callback-version">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:value-of select="concat ($id, 'm_pModem-&gt;Version (CModem::BuildVersion')"/>
      <xsl:value-of select="concat (' (', $mp, @var0)"/>
      <xsl:if test="@var1">
         <xsl:value-of select="concat (', ', $mp, @var1)"/>
      </xsl:if>
      <xsl:if test="@var2">
         <xsl:value-of select="concat (', ', $mp, @var2)"/>
      </xsl:if>
      <xsl:if test="@var3">
         <xsl:value-of select="concat (', ', $mp, @var3)"/>
      </xsl:if>
      <xsl:value-of select="concat('));', $CR)"/>
   </xsl:template>

   <xsl:template name="callback-scale">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:variable name="add">
         <xsl:choose>
            <xsl:when test="@add">
               <xsl:value-of select="@add"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="'0'"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="mul">
         <xsl:choose>
            <xsl:when test="@mul">
               <xsl:value-of select="@mul"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="'1'"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="div">
         <xsl:choose>
            <xsl:when test="@div">
               <xsl:value-of select="@div"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="'1'"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:value-of select="concat($id, $mp, @var, '.Scale (')"/>
      <xsl:value-of select="concat($add, ', ', $mul, ', ', $div)"/>
      <xsl:if test="@bit">
         <xsl:value-of select="concat(', ', @bit)"/>
      </xsl:if>
      <xsl:value-of select="concat(');', $CR)"/>
   </xsl:template>

   <xsl:template name="callback-if">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:value-of select="concat($id, 'if (', $mp, @var, ')', $CR, $id, '{', $CR)"/>
      <xsl:if test="band-up or band-down">
         <xsl:value-of select="concat($id, $ID, 'CBandplan *pBand = m_pModem-&gt;Data ().Bandplan ();', $CR, $CR)"/>
      </xsl:if>
      <xsl:for-each select="scale|tone">
         <xsl:call-template name="callback-switch">
            <xsl:with-param name="id" select="concat($ID, $id)"/>
            <xsl:with-param name="mp" select="$mp"/>
         </xsl:call-template>
      </xsl:for-each>

      <xsl:if test="band-up or band-down">
         <xsl:value-of select="concat($id, $ID, 'for (size_t nBand = 0; nBand &lt; pBand-&gt;Bands (); nBand++)', $CR)"/>
         <xsl:value-of select="concat($id, $ID, '{', $CR)"/>
         <xsl:for-each select="band-up|band-down">
            <xsl:call-template name="callback-switch">
               <xsl:with-param name="id" select="concat($ID, $ID, $id)"/>
               <xsl:with-param name="mp" select="$mp"/>
            </xsl:call-template>
         </xsl:for-each>
         <xsl:value-of select="concat($id, $ID, '}', $CR)"/>
      </xsl:if>
      <xsl:for-each select="trace">
         <xsl:call-template name="callback-switch">
            <xsl:with-param name="id" select="concat($ID, $id)"/>
            <xsl:with-param name="mp" select="$mp"/>
         </xsl:call-template>
      </xsl:for-each>
      <xsl:value-of select="concat($id, '}', $CR)"/>
   </xsl:template>

   <xsl:template name="callback-trace">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="mp" required="yes" as="xs:string"/>
      <xsl:value-of select="concat($id, 'LogTrace (&quot;', @trace, '\n&quot;')"/>
      <xsl:if test="@var0">
         <xsl:variable name="var" select="@var0"/>
         <xsl:value-of select="', '"/>
         <xsl:if test="substring ($var, 1, 3) = 'str'">
            <xsl:value-of select="'(const char*)'"/>
         </xsl:if>
         <xsl:value-of select="concat($mp, $var)"/>
      </xsl:if>
      <xsl:if test="@var1">
         <xsl:variable name="var" select="@var1"/>
         <xsl:value-of select="', '"/>
         <xsl:if test="substring ($var, 1, 3) = 'str'">
            <xsl:value-of select="'(const char*)'"/>
         </xsl:if>
         <xsl:value-of select="concat($mp, $var)"/>
      </xsl:if>
      <xsl:if test="@var2">
         <xsl:variable name="var" select="@var2"/>
         <xsl:value-of select="', '"/>
         <xsl:if test="substring ($var, 1, 3) = 'str'">
            <xsl:value-of select="'(const char*)'"/>
         </xsl:if>
         <xsl:value-of select="concat($mp, $var)"/>
      </xsl:if>
      <xsl:if test="@var3">
         <xsl:variable name="var" select="@var3"/>
         <xsl:value-of select="', '"/>
         <xsl:if test="substring ($var, 1, 3) = 'str'">
            <xsl:value-of select="'(const char*)'"/>
         </xsl:if>
         <xsl:value-of select="concat($mp, $var)"/>
      </xsl:if>
      <xsl:value-of select="concat(');', $CR)"/>
   </xsl:template>

   <xsl:template name="callback-namespace">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:variable name="name" select="@name"/>
      <xsl:variable name="namespace" select="$str-namespace[$name=@name]"/>
      <xsl:value-of select="concat($id, 'XPathNamespace (&quot;', $namespace/@prefix, '&quot;')"/>
      <xsl:value-of select="concat(', s_strNamespace', $namespace/@name,');', $CR)"/>
   </xsl:template>

   <xsl:template name="callback-xpath">
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:variable name="name" select="@name"/>
      <xsl:variable name="xpath" select="$str-xpath[$name=@name][1]"/>
      
      <xsl:choose>
         <xsl:when test="count(*)">
            <xsl:value-of select="concat($id, 'if (XPathQuery (', @var )"/>
            <xsl:value-of select="concat(', s_strXPath', $xpath/@name, '))', $CR)"/>
            <xsl:value-of select="concat($id, '{', $CR)"/>
            <xsl:for-each select="*">
               <xsl:call-template name="callback-switch">
                  <xsl:with-param name="id" select="concat($ID, $id)"/>
                  <xsl:with-param name="mp" select="''"/>
               </xsl:call-template>
            </xsl:for-each>
            
            <xsl:value-of select="concat($id, '}', $CR)"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="concat($id, 'XPathQuery (', @var )"/>
            <xsl:value-of select="concat(', s_strXPath', $xpath/@name, ');', $CR)"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   
   <xsl:template name="test-var">
      <xsl:param name="class" required="yes" as="xs:string"/>
      <xsl:variable name="test" select="."/>

      <xsl:choose>
         <xsl:when test="regex/loop[1]/@sub">
            <xsl:variable name="name" select="@name"/>
            <xsl:variable name="count" select="regex/loop[1]/@count" as="xs:integer"/>
            <xsl:variable name="sub" select="regex/loop[1]/@sub" as="xs:integer"/>
            <xsl:variable name="max" as="xs:integer" select="$count div $sub - 1"/>
            <xsl:for-each select="0 to $max">
               <xsl:variable name="pos" select="." as="xs:integer"/>
               <xsl:variable name="idx" as="xs:integer" select="$count - $sub * $pos"/>
               <xsl:call-template name="init-regex">
                  <xsl:with-param name="class" select="$class"/>
                  <xsl:with-param name="var" select="$name"/>
                  <xsl:with-param name="idx" select="concat('_', $idx)"/>
                  <xsl:with-param name="test" select="$test"/>
                  <xsl:with-param name="count" select="$idx"/>
               </xsl:call-template>
            </xsl:for-each>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="init-regex">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="var" select="@name"/>
               <xsl:with-param name="idx" select="''"/>
               <xsl:with-param name="test" select="$test"/>
               <xsl:with-param name="count" select="regex/loop[1]/@count"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="init-regex">
      <xsl:param name="class" required="yes" as="xs:string"/>
      <xsl:param name="var" required="yes" as="xs:string"/>
      <xsl:param name="idx" required="yes" as="xs:string"/>
      <xsl:param name="test" required="yes"/>
      <xsl:param name="count" required="yes"/>
      <xsl:for-each select="$test/regex/*">
         <xsl:call-template name="add-regex">
            <xsl:with-param name="var" select="$var"/>
            <xsl:with-param name="idx" select="$idx"/>
            <xsl:with-param name="id" select="$ID"/>
            <xsl:with-param name="count" select="$count"/>
         </xsl:call-template>
      </xsl:for-each>
      <xsl:value-of select="concat($ID, 'm_c', $var , $idx, '.Init ();', $CR)"/>
      <xsl:for-each select="$test/set">
         <xsl:choose>
            <xsl:when test="condition">
               <xsl:variable name="cond" select="condition/@name"/>
               <xsl:for-each select="condition/cond">
                  <xsl:value-of
                     select="concat($ID, 'AddCallback (m_c', $var, $idx, ', &amp;', $class, '::', $var, $cond, 'Callback')"/>
                  <xsl:if test="@state">
                     <xsl:value-of select="concat(', e', @state)"/>
                  </xsl:if>
                  <xsl:value-of select="concat(');', $CR)"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of
                  select="concat($ID, 'AddCallback (m_c', $var, $idx, ', &amp;', $class, '::', $var, 'Callback);', $CR)"
               />
            </xsl:otherwise>
         </xsl:choose>
      </xsl:for-each>
      <xsl:value-of select="$CR"/>
   </xsl:template>

   <xsl:template name="add-regex">
      <xsl:param name="var" required="yes" as="xs:string"/>
      <xsl:param name="idx" required="yes" as="xs:string"/>
      <xsl:param name="id" required="yes" as="xs:string"/>
      <xsl:param name="count" required="yes"/>
      <xsl:choose>
         <xsl:when test="name()='loop'">
            <xsl:choose>
               <xsl:when test="$count">
                  <xsl:value-of select="concat($id, 'for (int i = 0; i &lt; ', $count, '; i++)', $CR, $id, '{', $CR)"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="concat($id, 'for (int i = 0; i &lt; ', @count, '; i++)', $CR, $id, '{', $CR)"/>
               </xsl:otherwise>
            </xsl:choose>
            <xsl:for-each select="*">
               <xsl:call-template name="add-regex">
                  <xsl:with-param name="var" select="$var"/>
                  <xsl:with-param name="idx" select="$idx"/>
                  <xsl:with-param name="id" select="concat($id, $ID)"/>
                  <xsl:with-param name="count" select="$count"/>
               </xsl:call-template>
            </xsl:for-each>
            <xsl:value-of select="concat($id, '}', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='sep'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddSep ();', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='sepf'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddSep (true);', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='const'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddConst (&quot;', @regex, '&quot;);', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='term'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddTerm (&quot;', @regex, '&quot;);', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='username'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddConst (pModem-&gt;User ());', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='int'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddInt (m_', @var)"/>
            <xsl:if test="@base">
               <xsl:value-of select="concat(', ', @base)"/>
               <xsl:if test="@len">
                  <xsl:value-of select="concat(', ', @len)"/>
               </xsl:if>
            </xsl:if>
            <xsl:value-of select="concat(');', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='double'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddDouble (m_', @var, ');', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='string'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddString (m_', @var)"/>
            <xsl:if test="@regex">
               <xsl:value-of select="concat(', &quot;', @regex, '&quot;')"/>
            </xsl:if>
            <xsl:value-of select="concat(');', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='word'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddWord (m_', @var, ');', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='tone'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddTone (m_', @var)"/>
            <xsl:if test="@base">
               <xsl:value-of select="concat(', ', @base)"/>
               <xsl:if test="@len">
                  <xsl:value-of select="concat(', ', @len)"/>
               </xsl:if>
            </xsl:if>
            <xsl:value-of select="concat(');', $CR)"/>
         </xsl:when>
         <xsl:when test="name()='tone-idx'">
            <xsl:value-of select="concat($id, 'm_c', $var, $idx, '.AddToneIndex (m_', @var)"/>
            <xsl:if test="@base">
               <xsl:value-of select="concat(', ', @base)"/>
               <xsl:if test="@len">
                  <xsl:value-of select="concat(', ', @len)"/>
               </xsl:if>
            </xsl:if>
            <xsl:value-of select="concat(');', $CR)"/>
         </xsl:when>
      </xsl:choose>

   </xsl:template>

   <xsl:template name="modem-header">
      <xsl:call-template name="src-func-begin">
         <xsl:with-param name="return" select="'void LIBDSLTOOL_MODEM_EXPORT '"/>
         <xsl:with-param name="func" select="'ModemVersion'"/>
         <xsl:with-param name="param" select="'int &amp;nMajor, int &amp;nMinor, int &amp;nSub'"/>
      </xsl:call-template>
      <xsl:value-of select="concat($ID, 'nMajor = VERSION_MAJOR;', $CR)"/>
      <xsl:value-of select="concat($ID, 'nMinor = VERSION_MINOR;', $CR)"/>
      <xsl:value-of select="concat($ID, 'nSub   = VERSION_SUB;', $CR)"/>
      <xsl:call-template name="src-func-end"/>
      <xsl:call-template name="src-func-begin">
         <xsl:with-param name="return" select="'CModem LIBDSLTOOL_MODEM_EXPORT *'"/>
         <xsl:with-param name="func" select="'ModemCreate'"/>
         <xsl:with-param name="param" select="'CDslData &amp;cData, CProtocol *pProtocol'"/>
      </xsl:call-template>
      <xsl:value-of select="concat($ID, 'return new ', $modem-class, ' (cData, pProtocol);', $CR)"/>
      <xsl:call-template name="src-func-end"/>
   </xsl:template>

   <xsl:function name="src:return" as="xs:string">
      <xsl:param name="return"/>
      <xsl:param name="modem"/>
      <xsl:choose>
         <xsl:when test="$return/@cmd">
            <xsl:variable name="cmd" select="$return/@cmd"/>
            <xsl:variable name="class" select="$str-command[$cmd=@name]/@class"/>
            <xsl:if test="empty($class)">
               <xsl:message
                  select="concat ('class:', $return/../../../../@name, '//', $return/../../name(), ':', $return/../../@name, ': command ', $cmd, ' not found')"
               />
            </xsl:if>
            <xsl:variable name="param">
               <xsl:choose>
                  <xsl:when test="$return/@param">
                     <xsl:value-of select="$return/@param"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="concat('s_strCmd', $cmd)"/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="concat('new C', $class, ' (', $modem, ', ', $param, ')')"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="'NULL'"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:function>
</xsl:stylesheet>
