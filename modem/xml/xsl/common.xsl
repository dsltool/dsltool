<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   version="2.0" xmlns:common="common.xsl">

   <xsl:param name="modem" required="yes" as="xs:string"/>
   <xsl:variable name="modem-class" select="concat('C', common:class-name ($modem))"/>
   <xsl:variable name="modem-namespace" select="concat('N', common:class-name ($modem))"/>
   
   <xsl:variable name="apos" select='"&apos;"'/>
   <xsl:variable name="CR" select="'&#xa;'"/>
   <xsl:variable name="ID" select="'    '"/>
   <xsl:variable name="USER" select="concat('//begin user code',$CR, '//end user code',$CR)"/>
   
   <xsl:template name="inc-header">
      <xsl:call-template name="header"/>
      <xsl:value-of select="concat('#ifndef ', common:def-name ($modem), $CR)"/>
      <xsl:value-of select="concat('#define ', common:def-name ($modem), $CR)"/>
      <xsl:value-of select="$CR"/>
      <xsl:choose>
         <xsl:when test="//settings/@parser='regex'">
            <xsl:call-template name="include">
               <xsl:with-param name="file" select="'regex-parser.h'"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="include">
               <xsl:with-param name="file" select="'xml-parser.h'"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="$CR"/>
      <xsl:value-of select="concat('//! @cond', $CR)"/>
      <xsl:value-of select="concat('namespace ', $modem-namespace, $CR)"/>
      <xsl:value-of select="concat('{', $CR)"/>
   </xsl:template>

   <xsl:template name="inc-footer">
      <xsl:value-of select="$CR"/>
      <xsl:value-of select="concat('} // namespace ', $modem-namespace, $CR)"/>
      <xsl:value-of select="concat('//! @endcond', $CR)"/>
      <xsl:value-of select="concat('#endif // ', common:def-name ($modem), $CR)"/>
      <xsl:call-template name="footer"/>
   </xsl:template>
   
   <xsl:template name="src-header">
      <xsl:call-template name="header"/>
      <xsl:value-of select="$CR"/>
      <xsl:value-of select="concat('//! @cond', $CR)"/>
      <xsl:call-template name="include">
         <xsl:with-param name="file" select="concat('libdsltool-', $modem, '-export.h')"/>
      </xsl:call-template>
      <xsl:value-of select="'#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_'"/>
      <xsl:value-of select="translate(upper-case($modem),'-','_')"/>
      <xsl:value-of select="concat('_EXPORT', $CR)"/>
      <xsl:value-of select="concat('//! @endcond', $CR, $CR)"/>
      <xsl:call-template name="include">
         <xsl:with-param name="file" select="'config.h'"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="std-include">
         <xsl:with-param name="file" select="'string.h'"/>
      </xsl:call-template>
      <xsl:value-of select="concat($CR, $USER, $CR)"/>
      <xsl:call-template name="include">
         <xsl:with-param name="file" select="concat($modem, '.h')"/>
      </xsl:call-template>
      <xsl:call-template name="include">
         <xsl:with-param name="file" select="'log.h'"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:value-of select="concat('//! @cond', $CR)"/>
      <xsl:value-of select="concat('using namespace ', $modem-namespace, ';', $CR)"/>
      <xsl:value-of select="$CR"/>
   </xsl:template>
   
   <xsl:template name="src-footer">
      <xsl:value-of select="$CR"/>
      <xsl:value-of select="concat('//! @endcond', $CR)"/>
      <xsl:call-template name="footer"/>
   </xsl:template>
   
   <xsl:template name="header">
      <xsl:variable name="date" select="current-dateTime()"/>
      <xsl:value-of select="'// generated '"/>
      <xsl:value-of select="day-from-dateTime($date)"/>
      <xsl:value-of select="'.'"/>
      <xsl:value-of select="month-from-dateTime($date)"/>
      <xsl:value-of select="'.'"/>
      <xsl:value-of select="year-from-dateTime($date)"/>
      <xsl:value-of select="' '"/>
      <xsl:value-of select="hours-from-dateTime($date)"/>
      <xsl:value-of select="':'"/>
      <xsl:value-of select="minutes-from-dateTime($date)"/>
      <xsl:value-of select="':'"/>
      <xsl:value-of select="seconds-from-dateTime($date)"/>
      <xsl:value-of select="$CR"/>
   </xsl:template>

   <xsl:template name="footer">
      <xsl:value-of select="concat('//_oOo_', $CR)"/>
   </xsl:template>
   
   <xsl:template name="include">
      <xsl:param name="file" as="xs:string" required="yes"/>
      <xsl:value-of select="concat('#include &quot;', $file, '&quot;', $CR)"/>
   </xsl:template>

   <xsl:template name="std-include">
      <xsl:param name="file" as="xs:string" required="yes"/>
      <xsl:value-of select="concat('#include &lt;', $file, '&gt;', $CR)"/>
   </xsl:template>
   
   <xsl:template name="inc-class-begin">
      <xsl:param name="template" as="xs:string" required="no" select="''"/>
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:param name="base" as="xs:string" required="no" select="''"/>
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:choose>
      <xsl:when test="$id">
         <xsl:value-of select="$id"/>
      </xsl:when>
      <xsl:otherwise>
         <xsl:value-of select="$CR"/>
      </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="$template">
         <xsl:value-of select="concat('template &lt;', $template, '&gt; ')"/>
      </xsl:if>
      <xsl:value-of select="concat('class ', $class)"/>
      <xsl:if test="$base">
         <xsl:value-of select="concat(' : public ', $base)"/>
      </xsl:if>
      <xsl:value-of select="concat($CR, $id, '{', $CR)"/>
   </xsl:template>

   <xsl:template name="inc-class-end">
      <xsl:param name="template" as="xs:string" required="no" select="''"/>
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, '}; // class ', $class, $CR)"/>
   </xsl:template>
   
   <xsl:template name="inc-class-public">
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, 'public:', $CR)"/>
   </xsl:template>

   <xsl:template name="inc-class-protected">
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, 'protected:', $CR)"/>
   </xsl:template>

   <xsl:template name="inc-class-private">
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, 'private:', $CR)"/>
   </xsl:template>
   
   <xsl:template name="inc-class-ctor">
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:param name="param" as="xs:string" required="yes"/>
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, $ID, $class, ' (', $param, ');', $CR)"/>
   </xsl:template>

   <xsl:template name="inc-class-dtor">
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:param name="param" as="xs:string" required="no" select="''"/>
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, $ID, 'virtual ~', $class, ' (void);', $CR)"/>
   </xsl:template>

   <xsl:template name='inc-class-copy'>
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:param name="param" as="xs:string" required="yes"/>
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, $ID, $class, ' &amp;operator = (', $param, ');', $CR)"/>
   </xsl:template>
   <xsl:template name="inc-class-func">
      <xsl:param name="return" as="xs:string" required="yes"/>
      <xsl:param name="func" as="xs:string" required="yes"/>
      <xsl:param name="param" as="xs:string" required="yes"/>
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, $ID, $return , $func, ' (', $param,');', $CR)"/>
   </xsl:template>
   
   <xsl:template name="inc-class-var">
      <xsl:param name="type" as="xs:string" required="yes"/>
      <xsl:param name="var" as="xs:string" required="yes"/>
      <xsl:param name="id" as="xs:string" required="no" select="''"/>
      <xsl:value-of select="concat($id, $ID, $type, $var, ';', $CR)"/>
   </xsl:template>
   
   <xsl:template name="src-class-ctor-begin">
      <xsl:param name="template" as="xs:string" required="no" select="''"/>
      <xsl:param name="template-use" as="xs:string" required="no" select="''"/>
      <xsl:param name="parent-class" as="xs:string" required="no" select="''"/>
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:param name="param" as="xs:string" required="yes"/>
      <xsl:param name="init" as="xs:string" required="no" select="''"/>
      <xsl:param name="user" as="xs:boolean" required="no" select="false()"/>
      <xsl:value-of select="$CR"/>
      <xsl:if test="$template">
         <xsl:value-of select="concat('template &lt;', $template, '&gt; ')"/>
      </xsl:if>
      <xsl:if test="$parent-class">
         <xsl:value-of select="concat($parent-class, '::')"/>
      </xsl:if>
      <xsl:value-of select="$class"/>
      <xsl:if test="$template">
         <xsl:value-of select="concat('&lt;', $template-use, '&gt;')"/>
      </xsl:if>
      <xsl:value-of select="concat('::', $class, ' (', $param, ')', $CR)"/>
      <xsl:if test="$init">
         <xsl:value-of select="concat(': ', $init, $CR)"/>
      </xsl:if>
      <xsl:if test="$user">
         <xsl:value-of select="$USER"/>
      </xsl:if>
      <xsl:value-of select="concat('{', $CR)"/>
   </xsl:template>
   
   <xsl:template name="src-class-func-begin">
      <xsl:param name="template" as="xs:string" required="no" select="''"/>
      <xsl:param name="template-use" as="xs:string" required="no" select="''"/>
      <xsl:param name="parent-class" as="xs:string" required="no" select="''"/>
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:param name="return" as="xs:string" required="yes"/>
      <xsl:param name="func" as="xs:string" required="yes"/>
      <xsl:param name="param" as="xs:string" required="yes"/>
      <xsl:value-of select="concat ($CR, $return)"/>
      <xsl:if test="$template">
         <xsl:value-of select="concat('template &lt;', $template, '&gt; ')"/>
      </xsl:if>
      <xsl:if test="$parent-class">
         <xsl:value-of select="concat($parent-class, '::')"/>
      </xsl:if>
      <xsl:value-of select="$class"/>
      <xsl:if test="$template">
         <xsl:value-of select="concat('&lt;', $template-use, '&gt;')"/>
      </xsl:if>
      <xsl:value-of select="concat('::', $func, ' (', $param, ')', $CR)"/>
      <xsl:value-of select="concat('{', $CR)"/>
   </xsl:template>

   <xsl:template name="src-func-begin">
      <xsl:param name="return" as="xs:string" required="yes"/>
      <xsl:param name="func" as="xs:string" required="yes"/>
      <xsl:param name="param" as="xs:string" required="yes"/>
      <xsl:value-of select="concat($CR, $return, $func, ' (', $param, ')', $CR)"/>
      <xsl:value-of select="concat('{', $CR)"/>
   </xsl:template>
   
   <xsl:template name="src-func-end">
      <xsl:value-of select="concat('}', $CR)"/>
   </xsl:template>

   <xsl:function name="common:exec">
      <xsl:param name="exec" as="xs:string"/>
      <xsl:choose>
         <xsl:when test="$exec='info'">
            <xsl:value-of select="'CModem::eInfo'"/>
         </xsl:when>
         <xsl:when test="$exec='status'">
            <xsl:value-of select="'CModem::eStatus'"/>
         </xsl:when>
         <xsl:when test="$exec='resync'">
            <xsl:value-of select="'CModem::eResync'"/>
         </xsl:when>
         <xsl:when test="$exec='reboot'">
            <xsl:value-of select="'CModem::eReboot'"/>
         </xsl:when>
      </xsl:choose>
   </xsl:function>
   <xsl:function name="common:dsl-data" as="xs:string">
      <xsl:param name="data" as="xs:string"/>
      <xsl:variable name="base" select="'m_pModem-&gt;Data ().m_cData.'"/>
      <xsl:choose>
         <xsl:when test="$data='ModemState'">
            <xsl:value-of select="concat($base, 'm_cModemState')"/>
         </xsl:when>
         <xsl:when test="$data='ModemStateStr'">
            <xsl:value-of select="concat($base, 'm_cModemStateStr')"/>
         </xsl:when>
         <xsl:when test="$data='OperationMode'">
            <xsl:value-of select="concat($base, 'm_cOperationMode')"/>
         </xsl:when>
         <xsl:when test="$data='ChannelMode'">
            <xsl:value-of select="concat($base, 'm_cChannelMode')"/>
         </xsl:when>
         <xsl:when test="$data='NoiseMargin'">
            <xsl:value-of select="concat($base, 'm_cNoiseMargin')"/>
         </xsl:when>
         <xsl:when test="$data='Attenuation'">
            <xsl:value-of select="concat($base, 'm_cAttenuation')"/>
         </xsl:when>
         <xsl:when test="$data='TxPower'">
            <xsl:value-of select="concat($base, 'm_cTxPower')"/>
         </xsl:when>
         
         <xsl:when test="$data='BandwidthCells'">
            <xsl:value-of select="concat($base, 'm_cBandwidth.m_cCells')"/>
         </xsl:when>
         <xsl:when test="$data='BandwidthKbits'">
            <xsl:value-of select="concat($base, 'm_cBandwidth.m_cKbits')"/>
         </xsl:when>
         <xsl:when test="$data='BandwidthMaxKbits'">
            <xsl:value-of select="concat($base, 'm_cBandwidth.m_cMaxKbits')"/>
         </xsl:when>
         
         <xsl:when test="$data='ErrorsFEC'">
            <xsl:value-of select="concat($base, 'm_cStatistics.m_cError.m_cFEC')"/>
         </xsl:when>
         <xsl:when test="$data='ErrorsCRC'">
            <xsl:value-of select="concat($base, 'm_cStatistics.m_cError.m_cCRC')"/>
         </xsl:when>
         <xsl:when test="$data='ErrorsHEC'">
            <xsl:value-of select="concat($base, 'm_cStatistics.m_cError.m_cHEC')"/>
         </xsl:when>
         
         <xsl:when test="$data='FailErrorSecs15min'">
            <xsl:value-of select="concat($base, 'm_cStatistics.m_cFailure.m_cErrorSecs15min')"/>
         </xsl:when>
         <xsl:when test="$data='FailErrorSecsDay'">
            <xsl:value-of select="concat($base, 'm_cStatistics.m_cFailure.m_cErrorSecsDay')"/>
         </xsl:when>
         
         <xsl:when test="$data='AtmVpiVci'">
            <xsl:value-of select="concat($base, 'm_cAtm.m_cVpiVci')"/>
         </xsl:when>
         <xsl:when test="$data='AtmATU_C'">
            <xsl:value-of select="concat($base, 'm_cAtm.m_cATU_C')"/>
         </xsl:when>
         <xsl:when test="$data='AtmATU_R'">
            <xsl:value-of select="concat($base, 'm_cAtm.m_cATU_R')"/>
         </xsl:when>
         
         <xsl:when test="$data='ToneBitallocUp'">
            <xsl:value-of select="concat($base, 'm_cTones.m_cBitallocUp')"/>
         </xsl:when>
         <xsl:when test="$data='ToneBitallocDown'">
            <xsl:value-of select="concat($base, 'm_cTones.m_cBitallocDown')"/>
         </xsl:when>
         <xsl:when test="$data='ToneSNR'">
            <xsl:value-of select="concat($base, 'm_cTones.m_cSNR')"/>
         </xsl:when>
         <xsl:when test="$data='ToneGainQ2'">
            <xsl:value-of select="concat($base, 'm_cTones.m_cGainQ2')"/>
         </xsl:when>
         <xsl:when test="$data='ToneNoiseMargin'">
            <xsl:value-of select="concat($base, 'm_cTones.m_cNoiseMargin')"/>
         </xsl:when>
         <xsl:when test="$data='ToneChanCharLog'">
            <xsl:value-of select="concat($base, 'm_cTones.m_cChanCharLog')"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="''"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:function>
   
   <xsl:function name="common:class-name">
      <xsl:param name="name" as="xs:string"/>
      <xsl:value-of>
         <xsl:for-each select="tokenize($name, '-')">
            <xsl:value-of select="concat(upper-case(substring(.,1,1)), substring(., 2))"/>
         </xsl:for-each>
      </xsl:value-of>
   </xsl:function>
   <xsl:function name="common:var-name">
      <xsl:param name="name" as="xs:string"/>
      <xsl:value-of>
         <xsl:value-of select="concat(lower-case(substring($name,1,1)), substring($name, 2))"/>
      </xsl:value-of>
   </xsl:function>
   <xsl:function name="common:def-name">
      <xsl:param name="name" as="xs:string"/>
      <xsl:value-of select="concat('__DSLTOOL_', translate(upper-case($name),'-','_'), '_H__')"/>
   </xsl:function>
</xsl:stylesheet>
