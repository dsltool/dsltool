<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
   version="2.0" xmlns:common="common.xsl">

   <xsl:output method="text" encoding="UTF-8"/>
   <xsl:strip-space elements="*"/>

   <xsl:include href="common.xsl"/>
   <xsl:variable name="CONT" select="concat($CR, $ID, $ID, $ID)"/>

   <xsl:template match="/">
      <xsl:call-template name="inc-header"/>

      <xsl:apply-templates/>

      <xsl:call-template name="inc-footer"/>
   </xsl:template>

   <xsl:template match="modem">
      <xsl:call-template name="class-modem"/>
      <xsl:if test="login/@user">
         <xsl:call-template name="class-login">
            <xsl:with-param name="name" select="'User'"/>
         </xsl:call-template>
      </xsl:if>
      <xsl:if test="login/@pass">
         <xsl:call-template name="class-login">
            <xsl:with-param name="name" select="'Pass'"/>
         </xsl:call-template>
      </xsl:if>
      <xsl:choose>
         <xsl:when test="//settings/@parser='dom'"/> 
         <xsl:otherwise>
            <xsl:call-template name="class-prompt">
               <xsl:with-param name="class" select="'CPromptParser'"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="class"/>
   </xsl:template>

   <xsl:template match="class">
      <xsl:variable name="class">
         <xsl:value-of select="concat('C', @name)"/>
      </xsl:variable>
      <xsl:choose>
         <xsl:when test="//settings/@parser='dom'">
            <xsl:call-template name="inc-class-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="base" select="'CDomCmdParser'"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="inc-class-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="base" select="concat('CPromptParser&lt;C', @name, '&gt;')"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
      <!-- ctor -->
      <xsl:call-template name="inc-class-public"/>
      <xsl:call-template name="inc-class-ctor">
               <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="'CModem *pModem, const char *strCmd'"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private"/>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-copy">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private"/>
      <!-- callbacks -->
      <xsl:apply-templates select="parser"/>
      <xsl:apply-templates select="domparser"/>
      
      <!-- states and vars -->
      <xsl:choose>
         <xsl:when test="//settings/@parser='dom'"/>
         <xsl:otherwise>
            <xsl:apply-templates select="states"/>
            <xsl:apply-templates select="vars"/>
         </xsl:otherwise>
      </xsl:choose>

      <xsl:if test="parser/test">
         <xsl:value-of select="$CR"/>
         <xsl:call-template name="inc-class-private"/>
         <xsl:for-each select="parser/test">
            <xsl:call-template name="test-var"/>
         </xsl:for-each>
      </xsl:if>
      <xsl:call-template name="inc-class-end">
         <xsl:with-param name="class" select="$class"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="parser">
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="domparser">
      <xsl:call-template name="inc-class-func">
         <xsl:with-param name="func" select="'Finished'"/>
         <xsl:with-param name="return" select="'virtual CParser *'"/>
         <xsl:with-param name="param" select="'void'"/>
      </xsl:call-template>
   </xsl:template>
   
   <xsl:template match="test">
      <xsl:apply-templates select="set"/>
   </xsl:template>

   <xsl:template match="test-prompt">
      <xsl:apply-templates select="set"/>
   </xsl:template>

   <xsl:template match="test-next">
      <xsl:apply-templates select="set"/>
   </xsl:template>

   <xsl:template match="test/set">
      <xsl:call-template name="test-callback"/>
   </xsl:template>

   <xsl:template match="test-prompt/set">
      <xsl:call-template name="test-prompt-callback"/>
   </xsl:template>

   <xsl:template match="test-next/set">
      <xsl:call-template name="test-next-callback"/>
   </xsl:template>

   <xsl:template match="states">
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private"/>
      <xsl:value-of select="concat($ID, 'enum EState', $CR, $ID, '{', $CR)"/>
      <xsl:apply-templates/>
      <xsl:value-of select="concat($ID, '};', $CR)"/>
   </xsl:template>

   <xsl:template match="state">
      <xsl:value-of select="concat($ID, $ID, 'e', @name)"/>
      <xsl:if test="position()=1">
         <xsl:value-of select="' = 0'"/>
      </xsl:if>
      <xsl:if test="position()!=last()">
         <xsl:value-of select="','"/>
      </xsl:if>
      <xsl:value-of select="$CR"/>
   </xsl:template>

   <xsl:template match="vars">
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private"/>
      <xsl:apply-templates select="var"/>
   </xsl:template>
   <xsl:template match="var">
      <xsl:variable name="type">
         <xsl:choose>
            <xsl:when test="@type = 'string'">
               <xsl:value-of select="'const char *'"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="concat(@type, ' ')"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:call-template name="inc-class-var">
         <xsl:with-param name="var" select="concat('m_', @name)"/>
         <xsl:with-param name="type" select="$type"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="class-modem">
      <xsl:call-template name="inc-class-begin">
         <xsl:with-param name="class" select="$modem-class"/>
         <xsl:with-param name="base" select="'CModem'"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-public"/>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$modem-class"/>
         <xsl:with-param name="param"
            select="concat('CDslData &amp;cData,', $CONT, 'CProtocol *pProtocol')"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private"/>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$modem-class"/>
         <xsl:with-param name="param" select="concat('const ', $modem-class, ' &amp;', common:var-name($modem-class))"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-copy">
         <xsl:with-param name="class" select="$modem-class"/>
         <xsl:with-param name="param" select="concat('const ', $modem-class, ' &amp;', common:var-name($modem-class))"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-public"/>
      <xsl:call-template name="inc-class-func">
         <xsl:with-param name="return" select="'virtual void '"/>
         <xsl:with-param name="func" select="'Init'"/>
         <xsl:with-param name="param" select="'ECommand eCommand'"/>
      </xsl:call-template>
      <xsl:if test="(//settings/@parser='sax') or (//settings/@parser='dom')">
         <xsl:call-template name="inc-class-func">
            <xsl:with-param name="return" select="'virtual bool '"/>
            <xsl:with-param name="func" select="'Connect'"/>
            <xsl:with-param name="param" select="'void'"/>
         </xsl:call-template>
      </xsl:if>
      <xsl:if test="bandplan">
         <xsl:value-of select="$CR"/>
         <xsl:call-template name="inc-class-private"/>
         <xsl:call-template name="class-bandplan"/>
         <xsl:value-of select="$CR"/>
         <xsl:call-template name="inc-class-public"/>
         <xsl:variable name="band-param">
            <xsl:for-each select="bandplan/var">
               <xsl:choose>
                  <xsl:when test="@type = 'string'">
                     <xsl:value-of select="concat('const char *', @name)"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="concat(@type, ' ', @name)" />
                  </xsl:otherwise>
                  </xsl:choose>
               <xsl:if test="position()!=last()">
                  <xsl:value-of select="concat(',', $CR, $ID, $ID, $ID)" />
               </xsl:if>
            </xsl:for-each>
         </xsl:variable>
         <xsl:call-template name="inc-class-func">
               <xsl:with-param name="return" select="'static CBandplan::EBandplan '"/>
               <xsl:with-param name="func" select="'Bandplan'"/>
               <xsl:with-param name="param" select="$band-param"/>
         </xsl:call-template>
      </xsl:if>
      <xsl:value-of select="concat($CR, $USER)"/>
      <xsl:apply-templates select="versions"/>
      <xsl:call-template name="inc-class-end">
         <xsl:with-param name="class" select="$modem-class"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="versions">
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-public"/>
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="version">
      <xsl:value-of select="concat($ID, 'static const long s_nVersion', @name, ';', $CR)"/>
   </xsl:template>

   <xsl:template name="class-bandplan">
      <xsl:variable name="class" select="'CBandplanData'"/>
      <xsl:call-template name="inc-class-begin">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-private">
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:variable name="param">
         <xsl:for-each select="bandplan/var">
            <xsl:choose>
               <xsl:when test="@type = 'string'">
                  <xsl:value-of select="concat('const char *', @name)"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="concat(@type, ' ', @name)"/>
               </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="position()!=last()">
               <xsl:value-of select="concat(',', $CR, $ID, $ID, $ID, $ID)"/>
            </xsl:if>
         </xsl:for-each>
      </xsl:variable>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat($param, ',',$CR, $ID, $ID, $ID, $ID,'CBandplan::EBandplan eBandplan')"/>
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-copy">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-public">
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-func">
         <xsl:with-param name="return" select="'static CBandplan::EBandplan '"/>
         <xsl:with-param name="func" select="'Get'"/>
         <xsl:with-param name="param" select="$param"/>
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private">
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:for-each select="bandplan/var">
         <xsl:variable name="type">
            <xsl:choose>
               <xsl:when test="@type = 'string'">
                  <xsl:value-of select="'const char *'"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="concat(@type, ' ')"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:variable>
         <xsl:call-template name="inc-class-var">
            <xsl:with-param name="type" select="$type"/>
            <xsl:with-param name="var" select="concat('m_', @name)"/>
            <xsl:with-param name="id" select="$ID"/>
         </xsl:call-template>
      </xsl:for-each>
      <xsl:call-template name="inc-class-var">
         <xsl:with-param name="type" select="'CBandplan::EBandplan '"/>
         <xsl:with-param name="var" select="'m_eBandplan'"/>
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private">
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-var">
         <xsl:with-param name="type" select="concat ('static const ', $class , ' ')"/>
         <xsl:with-param name="var" select="'g_acList[]'"/>
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-end">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="id" select="$ID"/>
      </xsl:call-template>
   </xsl:template>
   <xsl:template name="class-login">
      <xsl:param name="name" as="xs:string" required="yes"/>
      <xsl:variable name="class" select="concat('CLogin', $name)"/>
      <xsl:choose>
         <xsl:when test="login/@line">
            <xsl:call-template name="inc-class-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="base" select="'CLineParser'"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="inc-class-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="base" select="'CParser'"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="inc-class-public"/>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('CModem *pModem, const char *str', $name)"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private"/>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-copy">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-public"/>
      <xsl:choose>
         <xsl:when test="login/@line">
            <xsl:call-template name="inc-class-func">
               <xsl:with-param name="func" select="'ParseLine'"/>
               <xsl:with-param name="return" select="'virtual CParser *'"/>
               <xsl:with-param name="param" select="'const char *strLine'"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="inc-class-func">
               <xsl:with-param name="func" select="'Parse'"/>
               <xsl:with-param name="return" select="'virtual CParser *'"/>
               <xsl:with-param name="param" select="'const char *strRecv, size_t nRecv'"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private"/>
      <xsl:call-template name="inc-class-var">
         <xsl:with-param name="var" select="concat('m_str', $name)"/>
         <xsl:with-param name="type" select="'const char *'"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-end">
         <xsl:with-param name="class" select="$class"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="class-prompt">
      <xsl:param name="class" as="xs:string" required="yes"/>
      <xsl:choose>
         <xsl:when test="//settings/@parser='regex'">
            <xsl:call-template name="inc-class-begin">
               <xsl:with-param name="template" select="'class T'"/>
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="base" select="'CRegExCmdParser&lt;T, CLineParser&gt;'"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="//settings/@parser='sax'">
            <xsl:call-template name="inc-class-begin">
               <xsl:with-param name="template" select="'class T'"/>
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="base" select="'CRegExCmdParser&lt;T, CSaxParser&gt;'"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="//settings/@parser='dom'">
            <xsl:call-template name="inc-class-begin">
               <xsl:with-param name="class" select="$class"/>
               <xsl:with-param name="base" select="'CDomParser'"/>
            </xsl:call-template>
         </xsl:when>
      </xsl:choose>
      <xsl:call-template name="inc-class-protected"/>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="'CModem *pModem, const char *strCmd'"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-private"/>
      <xsl:call-template name="inc-class-ctor">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-copy">
         <xsl:with-param name="class" select="$class"/>
         <xsl:with-param name="param" select="concat('const ', $class, ' &amp;', common:var-name($class))"/>
      </xsl:call-template>
      <xsl:value-of select="$CR"/>
      <xsl:call-template name="inc-class-protected"/>
      <xsl:call-template name="inc-class-var">
         <xsl:with-param name="var" select="'m_cPrompt'"/>
         <xsl:with-param name="type" select="'CRegExList '"/>
      </xsl:call-template>
      <xsl:call-template name="inc-class-end">
         <xsl:with-param name="class" select="$class"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="test-callback">
      <xsl:variable name="name" select="../@name"/>
      <xsl:variable name="cond" select="condition/@name"/>
      <xsl:call-template name="inc-class-func">
         <xsl:with-param name="func" select="concat($name, $cond, 'Callback')"/>
         <xsl:with-param name="return" select="'CParser *'"/>
         <xsl:with-param name="param" select="'void'"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="test-prompt-callback">
      <xsl:variable name="cond" select="condition/@name"/>
      <xsl:call-template name="inc-class-func">
         <xsl:with-param name="func" select="concat($cond, 'Callback')"/>
         <xsl:with-param name="return" select="'CParser *'"/>
         <xsl:with-param name="param" select="'void'"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="test-next-callback">
      <xsl:call-template name="inc-class-func">
         <xsl:with-param name="func" select="concat(@name, 'PromptCallback')"/>
         <xsl:with-param name="return" select="'CParser *'"/>
         <xsl:with-param name="param" select="'void'"/>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="test-var">
      <xsl:variable name="name" select="@name"/>
      <xsl:choose>
         <xsl:when test="regex/loop[1]/@sub">
            <xsl:variable name="count" select="regex/loop[1]/@count" as="xs:integer"/>
            <xsl:variable name="sub" select="regex/loop[1]/@sub" as="xs:integer"/>
            <xsl:variable name="max" as="xs:integer" select="$count div $sub - 1"/>
            <xsl:for-each select="0 to $max">
               <xsl:variable name="pos" select="." as="xs:integer"/>
               <xsl:variable name="idx" as="xs:integer" select="$count - $sub * $pos"/>
               <xsl:call-template name="inc-class-var">
                  <xsl:with-param name="var" select="concat('m_c', $name, '_', $idx)"/>
                  <xsl:with-param name="type" select="'CRegExList '"/>
               </xsl:call-template>
            </xsl:for-each>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="inc-class-var">
               <xsl:with-param name="var" select="concat('m_c', $name)"/>
               <xsl:with-param name="type" select="'CRegExList '"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

</xsl:stylesheet>
