/*!
 * @file        avm-tr064.cpp
 * @brief       fritzbox modem implementation
 * @details     parser for AVM Fritz!Box, Fritz!OS >= 5.50 tested with:
 * @n           Fritz!Box 3272
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.12.2015
 *
 * @note        This file is partly generated from ar7.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-avm-tr064-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_AVM_TR064_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
#include <iconv.h>
#include <openssl/md5.h>
//end user code

#include "avm-tr064.h"
#include "log.h"

//! @cond
using namespace NAvmTr064;

static const char *s_strCmdAuth = "/upnp/control/deviceinfo";
static const char *s_strCmdIgdWan = "/igdupnp/control/WANCommonIFC1";
static const char *s_strCmdAuth1 = "/upnp/control/deviceinfo";
static const char *s_strCmdIgdDsl = "/igdupnp/control/WANDSLLinkC1";
static const char *s_strCmdAuth2 = "/upnp/control/deviceinfo";
static const char *s_strCmdDslInfo = "/upnp/control/wandslifconfig1";
static const char *s_strCmdDslStat = "/upnp/control/wandslifconfig1";
static const char *s_strCmdDslLink = "/upnp/control/wandsllinkconfig1";

static const char *s_strNamespaceEnv = "http://schemas.xmlsoap.org/soap/envelope/";
static const char *s_strNamespaceAuth = "http://soap-authentication.org/digest/2001/10/";
static const char *s_strNamespaceIgdWan = "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1";
static const char *s_strNamespaceIgdDsl = "urn:schemas-upnp-org:service:WANDSLLinkConfig:1";
static const char *s_strNamespaceDslInterface = "urn:dslforum-org:service:WANDSLInterfaceConfig:1";
static const char *s_strNamespaceDslLink = "urn:dslforum-org:service:WANDSLLinkConfig:1";

static const char *s_strXPathNonce1 = "/s:Envelope/s:Header/h:Challenge/Nonce";
static const char *s_strXPathRealm1 = "/s:Envelope/s:Header/h:Challenge/Realm";
static const char *s_strXPathNonce = "/s:Envelope/s:Header/h:NextChallenge/Nonce";
static const char *s_strXPathRealm = "/s:Envelope/s:Header/h:NextChallenge/Realm";
static const char *s_strXPathWanType = "/s:Envelope/s:Body/u:GetCommonLinkPropertiesResponse/NewWANAccessType";
static const char *s_strXPathModemStatus = "/s:Envelope/s:Body/u:GetCommonLinkPropertiesResponse/NewPhysicalLinkStatus";
static const char *s_strXPathOperMode = "/s:Envelope/s:Body/u:GetModulationTypeResponse/NewModulationType";
static const char *s_strXPathRateCurrUp = "/s:Envelope/s:Body/u:GetInfoResponse/NewUpstreamCurrRate";
static const char *s_strXPathRateCurrDown = "/s:Envelope/s:Body/u:GetInfoResponse/NewDownstreamCurrRate";
static const char *s_strXPathRateMaxUp = "/s:Envelope/s:Body/u:GetInfoResponse/NewUpstreamMaxRate";
static const char *s_strXPathRateMaxDown = "/s:Envelope/s:Body/u:GetInfoResponse/NewDownstreamMaxRate";
static const char *s_strXPathNoisemarginUp = "/s:Envelope/s:Body/u:GetInfoResponse/NewUpstreamNoiseMargin";
static const char *s_strXPathNoisemarginDown = "/s:Envelope/s:Body/u:GetInfoResponse/NewDownstreamNoiseMargin";
static const char *s_strXPathAttenuationUp = "/s:Envelope/s:Body/u:GetInfoResponse/NewUpstreamAttenuation";
static const char *s_strXPathAttenuationDown = "/s:Envelope/s:Body/u:GetInfoResponse/NewDownstreamAttenuation";
static const char *s_strXPathAtuRVendor = "/s:Envelope/s:Body/u:GetInfoResponse/NewATURVendor";
static const char *s_strXPathAtuCVendor = "/s:Envelope/s:Body/u:GetInfoResponse/NewATUCVendor";
static const char *s_strXPathTxPowerUp = "/s:Envelope/s:Body/u:GetInfoResponse/NewUpstreamPower";
static const char *s_strXPathTxPowerDown = "/s:Envelope/s:Body/u:GetInfoResponse/NewDownstreamPower";
static const char *s_strXPathRxFEC = "/s:Envelope/s:Body/u:GetStatisticsTotalResponse/NewFECErrors";
static const char *s_strXPathTxFEC = "/s:Envelope/s:Body/u:GetStatisticsTotalResponse/NewATUCFECErrors";
static const char *s_strXPathRxHEC = "/s:Envelope/s:Body/u:GetStatisticsTotalResponse/NewHECErrors";
static const char *s_strXPathTxHEC = "/s:Envelope/s:Body/u:GetStatisticsTotalResponse/NewATUCHECErrors";
static const char *s_strXPathRxCRC = "/s:Envelope/s:Body/u:GetStatisticsTotalResponse/NewHECErrors";
static const char *s_strXPathTxCRC = "/s:Envelope/s:Body/u:GetStatisticsTotalResponse/NewATUCCRCErrors";
static const char *s_strXPathVpi = "substring-before(substring(/s:Envelope/s:Body/u:GetInfoResponse/NewDestinationAddress, 6), '/')";
static const char *s_strXPathVci = "substring-after(substring(/s:Envelope/s:Body/u:GetInfoResponse/NewDestinationAddress, 6), '/')";

//begin user code
static const char *s_strServiceIgdWan = "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1";
static const char *s_strServiceIgdDsl = "urn:schemas-upnp-org:service:WANDSLLinkConfig:1";

static const char *s_strServiceDeviceInfo = "urn:dslforum-org:service:DeviceInfo:1";
static const char *s_strServiceDslInterface = "urn:dslforum-org:service:WANDSLInterfaceConfig:1";
static const char *s_strServiceDslLink = "urn:dslforum-org:service:WANDSLLinkConfig:1";

static const char *s_strActionGetCommonLinkProperties = "GetCommonLinkProperties";
static const char *s_strActionGetModulationType = "GetModulationType";

static const char *s_strActionGetInfo = "GetInfo";
static const char *s_strActionGetStatisticsTotal = "GetStatisticsTotal";
//end user code

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CAvmTr064 (cData, pProtocol);
}

CAvmTr064::CAvmTr064 (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
, m_bAuth (false)
, m_strNonce ()
, m_strAuth ()
, m_strRealm ()
//end user code
{
//begin user code
//end user code
}

void CAvmTr064::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("/X:");
    m_strLF.Format ("");

//begin user code
    m_bAuth = false;
//end user code

    Protocol ()->Next (pFirst);
}

bool CAvmTr064::Connect (void)
{
//begin user code
    CString strSoap;
    CreateSoapRequest (strSoap,
            s_strCmdAuth, s_strServiceDeviceInfo, s_strActionGetInfo);

    Protocol ()->Next (new CSoapAuth (this, strSoap));
    return true;
//end user code
}

//begin user code
void CAvmTr064::Authenticate (const char *strNonce,
        const char *strRealm)
{
    if (strNonce && strRealm)
    {
        unsigned char anMD5[16];
        CString strMD5, strSecret;

        strSecret.Format ("%s:%s:%s", User (), strRealm, Pass ());
        MD5 ((const unsigned char*)(const char*)strSecret, strSecret.Length(), anMD5);
        strSecret.Format("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                anMD5[0], anMD5[1], anMD5[2], anMD5[3],
                anMD5[4], anMD5[5], anMD5[6], anMD5[7],
                anMD5[8], anMD5[9], anMD5[10], anMD5[11],
                anMD5[12], anMD5[13], anMD5[14], anMD5[15]);

        m_strAuth.Format ("%s:%s", (const char*)strSecret, strNonce);
        MD5 ((const unsigned char*)(const char*)m_strAuth, m_strAuth.Length(), anMD5);
        m_strAuth.Format("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                anMD5[0], anMD5[1], anMD5[2], anMD5[3],
                anMD5[4], anMD5[5], anMD5[6], anMD5[7],
                anMD5[8], anMD5[9], anMD5[10], anMD5[11],
                anMD5[12], anMD5[13], anMD5[14], anMD5[15]);

        m_strNonce = strNonce;
        m_strRealm = strRealm;
        m_bAuth = true;
    }
    else
    {
        m_bAuth = false;
    }
}

void CAvmTr064::CreateSoapRequest (CString &strRequest,
        const char *strUrl,
        const char *strService,
        const char *strAction)
{
    CString strHeader, strSoap;

    if (m_bAuth)
    {
        strHeader.Format ("<s:Header>"
                "<h:ClientAuth"
                " xmlns:h=\"http://soap-authentication.org/digest/2001/10/\""
                " s:mustUnderstand=\"1\">"
                "<Nonce>%s</Nonce>"
                "<Auth>%s</Auth>"
                "<UserID>%s</UserID>"
                "<Realm>%s</Realm>"
                "</h:ClientAuth>"
                "</s:Header>",
                (const char*)m_strNonce, (const char*)m_strAuth,
                User (), (const char*)m_strRealm);
    }
    else
    {
        strHeader.Format ("<s:Header>"
                "<h:InitChallenge"
                " xmlns:h=\"http://soap-authentication.org/digest/2001/10/\""
                " s:mustUnderstand=\"1\">"
                "<UserID>%s</UserID>"
                "</h:InitChallenge>"
                "</s:Header>",
                User ());
    }

    strSoap.Format ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            "<s:Envelope"
            " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\""
            " xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">"
            "%s"
            "<s:Body>"
            "<u:%s xmlns:u=\"%s\"/>"
            "</s:Body>"
            "</s:Envelope>",
            (const char*)strHeader, strAction, strService);

    strRequest.Format ("POST %s HTTP/1.1\r\n"
            "Host: %s:%d\r\n"
            "User-Agent: dsltool/%s\r\n"
            "Content-type: text/xml; charset=\"UTF-8\"\r\n"
            "Content-length: %d\r\n"
            "SoapAction: \"%s#%s\"\r\n"
            "\r\n%s",
            strUrl,
            m_pProtocol->Host (), m_pProtocol->Port (),
            VERSION,
            strSoap.Length (),
            strService, strAction, (const char*)strSoap);
}
//end user code

CSoapAuth::CSoapAuth (CModem *pModem, const char *strCmd)
: CDomCmdParser (pModem, strCmd)
{

//begin user code
//end user code
}

CParser *CSoapAuth::Finished (void)
{
    CString strNonce;
    CString strRealm;
//begin user code
    CAvmTr064 *pModem = (CAvmTr064*)m_pModem;
    CString strSoap;
//end user code
    XPathNamespace ("s", s_strNamespaceEnv);
    XPathNamespace ("h", s_strNamespaceAuth);
    XPathQuery (strNonce, s_strXPathNonce1);
    XPathQuery (strRealm, s_strXPathRealm1);
//begin user code
    pModem->Authenticate (strNonce, strRealm);
    pModem->CreateSoapRequest (strSoap,
            s_strCmdIgdWan, s_strServiceIgdWan, s_strActionGetCommonLinkProperties);
//end user code

    return new CIgdWan (m_pModem, strSoap);
}

CIgdWan::CIgdWan (CModem *pModem, const char *strCmd)
: CDomCmdParser (pModem, strCmd)
{

//begin user code
//end user code
}

CParser *CIgdWan::Finished (void)
{
    CString strTemp;
//begin user code
    CAvmTr064 *pModem = (CAvmTr064*)m_pModem;
    CString strSoap;
//end user code
    XPathNamespace ("s", s_strNamespaceEnv);
    XPathNamespace ("u", s_strNamespaceIgdWan);
    if (XPathQuery (strTemp, s_strXPathWanType))
    {
        LogTrace ("WanType %s\n", (const char*)strTemp);
    }
    if (XPathQuery (strTemp, s_strXPathModemStatus))
    {
//begin user code
        int nModemState = 0;
        if (strTemp.Compare ("up"))
        {
            nModemState = 1;
        }
        m_pModem->Data ().m_cData.m_cModemState.Set (nModemState);
//end user code
        m_pModem->Data ().m_cData.m_cModemStateStr.Set (strTemp);
        LogTrace ("ModemState %s\n", (const char*)strTemp);
    }
//begin user code
    pModem->Authenticate (NULL, NULL);
    pModem->CreateSoapRequest (strSoap,
            s_strCmdAuth1, s_strServiceDeviceInfo, s_strActionGetInfo);
//end user code

    return new CAuth1 (m_pModem, strSoap);
}

CAuth1::CAuth1 (CModem *pModem, const char *strCmd)
: CDomCmdParser (pModem, strCmd)
{

//begin user code
//end user code
}

CParser *CAuth1::Finished (void)
{
    CString strNonce;
    CString strRealm;
//begin user code
    CAvmTr064 *pModem = (CAvmTr064*)m_pModem;
    CString strSoap;
//end user code
    XPathNamespace ("s", s_strNamespaceEnv);
    XPathNamespace ("h", s_strNamespaceAuth);
    XPathQuery (strNonce, s_strXPathNonce1);
    XPathQuery (strRealm, s_strXPathRealm1);
//begin user code
    pModem->Authenticate (strNonce, strRealm);
    pModem->CreateSoapRequest (strSoap,
            s_strCmdIgdDsl, s_strServiceIgdDsl, s_strActionGetModulationType);
//end user code

    return new CIgdDsl (m_pModem, strSoap);
}

CIgdDsl::CIgdDsl (CModem *pModem, const char *strCmd)
: CDomCmdParser (pModem, strCmd)
{

//begin user code
//end user code
}

CParser *CIgdDsl::Finished (void)
{
    CString strTemp;
//begin user code
    CAvmTr064 *pModem = (CAvmTr064*)m_pModem;
    CString strSoap;
//end user code
    XPathNamespace ("s", s_strNamespaceEnv);
    XPathNamespace ("u", s_strNamespaceIgdDsl);
    if (XPathQuery (strTemp, s_strXPathOperMode))
    {
//begin user code
        if (0 == strncasecmp (strTemp, "ADSL G.lite", 11))
        {
            CString strOperationMode;
            m_pModem->Data ().Bandplan (CBandplan::eADSL2p_B);
            m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
            m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);
        }
        else if (0 == strncasecmp (strTemp, "VDSL", 11))
        {
            CString strOperationMode, strProfile;
            m_pModem->Data ().Bandplan (CBandplan::eVDSL2_B8_12_17a);
            m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
            m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);
            m_pModem->Data ().Bandplan ()->Profile (strProfile);
            m_pModem->Data ().m_cData.m_cProfile.Set (strProfile);
        }
//end user code
        LogTrace ("OperMode %s\n", (const char*)strTemp);
    }
//begin user code
    pModem->Authenticate (NULL, NULL);
    pModem->CreateSoapRequest (strSoap,
            s_strCmdAuth2, s_strServiceDeviceInfo, s_strActionGetInfo);
//end user code

    return new CAuth2 (m_pModem, strSoap);
}

CAuth2::CAuth2 (CModem *pModem, const char *strCmd)
: CDomCmdParser (pModem, strCmd)
{

//begin user code
//end user code
}

CParser *CAuth2::Finished (void)
{
    CString strNonce;
    CString strRealm;
//begin user code
    CAvmTr064 *pModem = (CAvmTr064*)m_pModem;
    CString strSoap;
//end user code
    XPathNamespace ("s", s_strNamespaceEnv);
    XPathNamespace ("h", s_strNamespaceAuth);
    XPathQuery (strNonce, s_strXPathNonce1);
    XPathQuery (strRealm, s_strXPathRealm1);
//begin user code
    pModem->Authenticate (strNonce, strRealm);
    pModem->CreateSoapRequest (strSoap,
            s_strCmdDslInfo, s_strServiceDslInterface, s_strActionGetInfo);
//end user code

    return new CDslInfo (m_pModem, strSoap);
}

CDslInfo::CDslInfo (CModem *pModem, const char *strCmd)
: CDomCmdParser (pModem, strCmd)
{

//begin user code
//end user code
}

CParser *CDslInfo::Finished (void)
{
    CString strNonce;
    CString strRealm;
    CString strTemp;
    long nTemp = 0;
    double fTemp = 0.0;
//begin user code
    CAvmTr064 *pModem = (CAvmTr064*)m_pModem;
    CString strSoap;
//end user code
    XPathNamespace ("s", s_strNamespaceEnv);
    XPathNamespace ("h", s_strNamespaceAuth);
    XPathNamespace ("u", s_strNamespaceDslInterface);
    XPathQuery (strNonce, s_strXPathNonce);
    XPathQuery (strRealm, s_strXPathRealm);
    if (XPathQuery (nTemp, s_strXPathRateCurrUp))
    {
        m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetUp (nTemp);
        LogTrace ("Bandwidth Up: %ld Kbits\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathRateCurrDown))
    {
        m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetDown (nTemp);
        LogTrace ("Bandwidth Down: %ld Kbits\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathRateMaxUp))
    {
        m_pModem->Data ().m_cData.m_cBandwidth.m_cMaxKbits.SetUp (nTemp);
        LogTrace ("Bandwidth Max Up: %ld Kbits\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathRateMaxDown))
    {
        m_pModem->Data ().m_cData.m_cBandwidth.m_cMaxKbits.SetDown (nTemp);
        LogTrace ("Bandwidth Max Down: %ld Kbits\n", nTemp);
    }
    if (XPathQuery (fTemp, s_strXPathNoisemarginUp))
    {
//begin user code
        fTemp /= 10.0;
//end user code
        m_pModem->Data ().m_cData.m_cNoiseMargin.SetUp (0, fTemp);
        LogTrace ("NoiseMargin Up: %lf\n", fTemp);
    }
    if (XPathQuery (fTemp, s_strXPathNoisemarginDown))
    {
//begin user code
        fTemp /= 10.0;
//end user code
        m_pModem->Data ().m_cData.m_cNoiseMargin.SetDown (0, fTemp);
        LogTrace ("NoiseMargin Down: %lf\n", fTemp);
    }
    if (XPathQuery (fTemp, s_strXPathAttenuationUp))
    {
//begin user code
        fTemp /= 10.0;
//end user code
        m_pModem->Data ().m_cData.m_cAttenuation.SetUp (0, fTemp);
        LogTrace ("Attenuation Up: %lf\n", fTemp);
    }
    if (XPathQuery (fTemp, s_strXPathAttenuationDown))
    {
//begin user code
        fTemp /= 10.0;
//end user code
        m_pModem->Data ().m_cData.m_cAttenuation.SetDown (0, fTemp);
        LogTrace ("Attenuation Down: %lf\n", fTemp);
    }
    if (XPathQuery (strTemp, s_strXPathAtuRVendor))
    {
//begin user code
        char strVendor[4];
        for (int i=0; i<4; i++)
        {
            CString strChar (&strTemp[2*i], 2);
            strVendor[i] = (char)strChar.Int (16);
        }
        pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetVendor (strVendor);
        LogTrace ("ATU-R Vendor: %.4s\n", strVendor);
//end user code
    }
    if (XPathQuery (strTemp, s_strXPathAtuCVendor))
    {
//begin user code
        char strVendor[4];
        for (int i=0; i<4; i++)
        {
            CString strChar (&strTemp[2*i], 2);
            strVendor[i] = (char)strChar.Int (16);
        }
        pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetVendor (strVendor);
        LogTrace ("ATU-C Vendor: %.4s\n", strVendor);
//end user code
    }
    if (XPathQuery (fTemp, s_strXPathTxPowerUp))
    {
//begin user code
        fTemp /= 10.0;
//end user code
        m_pModem->Data ().m_cData.m_cTxPower.SetUp (fTemp);
        LogTrace ("TxPower Up: %lf\n", fTemp);
    }
    if (XPathQuery (fTemp, s_strXPathTxPowerDown))
    {
//begin user code
        fTemp /= 10.0;
//end user code
        m_pModem->Data ().m_cData.m_cTxPower.SetDown (fTemp);
        LogTrace ("TxPower Down: %lf\n", fTemp);
    }
//begin user code
    pModem->Authenticate (strNonce, strRealm);
    pModem->CreateSoapRequest (strSoap,
            s_strCmdDslStat, s_strServiceDslInterface, s_strActionGetStatisticsTotal);
//end user code

    return new CDslStat (m_pModem, strSoap);
}

CDslStat::CDslStat (CModem *pModem, const char *strCmd)
: CDomCmdParser (pModem, strCmd)
{

//begin user code
//end user code
}

CParser *CDslStat::Finished (void)
{
    CString strNonce;
    CString strRealm;
    long nTemp = 0;
//begin user code
    CAvmTr064 *pModem = (CAvmTr064*)m_pModem;
    CString strSoap;
//end user code
    XPathNamespace ("s", s_strNamespaceEnv);
    XPathNamespace ("h", s_strNamespaceAuth);
    XPathNamespace ("u", s_strNamespaceDslInterface);
    XPathQuery (strNonce, s_strXPathNonce);
    XPathQuery (strRealm, s_strXPathRealm);
    if (XPathQuery (nTemp, s_strXPathRxFEC))
    {
        m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetDown (nTemp);
        LogTrace ("RxFEC: %ld\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathTxFEC))
    {
        m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetUp (nTemp);
        LogTrace ("TxFEC: %ld\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathRxHEC))
    {
        m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetDown (nTemp);
        LogTrace ("RxHEC: %ld\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathTxHEC))
    {
        m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetUp (nTemp);
        LogTrace ("TxHEC: %ld\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathRxCRC))
    {
        m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (nTemp);
        LogTrace ("RxCRC: %ld\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathTxCRC))
    {
        m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetUp (nTemp);
        LogTrace ("TxCRC: %ld\n", nTemp);
    }
//begin user code
    pModem->Authenticate (strNonce, strRealm);
    pModem->CreateSoapRequest (strSoap,
            s_strCmdDslLink, s_strServiceDslLink, s_strActionGetInfo);
//end user code

    return new CDslLink (m_pModem, strSoap);
}

CDslLink::CDslLink (CModem *pModem, const char *strCmd)
: CDomCmdParser (pModem, strCmd)
{

//begin user code
//end user code
}

CParser *CDslLink::Finished (void)
{
    CString strNonce;
    CString strRealm;
    long nTemp = 0;
    XPathNamespace ("s", s_strNamespaceEnv);
    XPathNamespace ("h", s_strNamespaceAuth);
    XPathNamespace ("u", s_strNamespaceDslLink);
    XPathQuery (strNonce, s_strXPathNonce);
    XPathQuery (strRealm, s_strXPathRealm);
    if (XPathQuery (nTemp, s_strXPathVpi))
    {
        m_pModem->Data ().m_cData.m_cAtm.m_cVpiVci.SetUp (nTemp);
        LogTrace ("VPI: %ld\n", nTemp);
    }
    if (XPathQuery (nTemp, s_strXPathVci))
    {
        m_pModem->Data ().m_cData.m_cAtm.m_cVpiVci.SetDown (nTemp);
        LogTrace ("VCI: %ld\n", nTemp);
    }

    return NULL;
}

//! @endcond
//_oOo_
