/*!
 * @file        trendchip.cpp
 * @brief       trendchip modem implementation
 * @details     parser for TrendChip based modems, tested with:
 * @n           D-Link DSL-321B HW Ver. Zx (not Dx!)
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        17.11.2015
 *
 * @note        This file is partly generated from bc63.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-trendchip-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_TRENDCHIP_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
//end user code

#include "trendchip.h"
#include "log.h"

//! @cond
using namespace NTrendchip;

static const char *s_strCmdStatus = "wan adsl status";
static const char *s_strCmdOpMode = "wan adsl opmode";
static const char *s_strCmdAnnex = "wan adsl annex";
static const char *s_strCmdChan = "wan adsl chandata";
static const char *s_strCmdPerf = "wan adsl perfdata";
static const char *s_strCmdAtm = "wan node display";
static const char *s_strCmdAtuR = "wan adsl nearituid";
static const char *s_strCmdAtuC = "wan adsl farituid";
static const char *s_strCmdLineUp = "wan adsl linedata near";
static const char *s_strCmdLineDown = "wan adsl linedata far";
static const char *s_strCmdResync = "wan adsl reset";
static const char *s_strCmdReboot = "sys reboot";
static const char *s_strCmdExit = "exit";

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CTrendchip (cData, pProtocol);
}

CTrendchip::CTrendchip (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
, m_strOpMode ()
, m_strAnnex ()
//end user code
{
//begin user code
//end user code
}

void CTrendchip::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("tc> ");
    m_strLF.Format ("\r\n");

//begin user code
//end user code

    if (m_pLogin)
    {
        pFirst = new CLoginPass (this, Pass ());
    }
    else if (CModem::eInfo == eCommand)
    {
        pFirst = new CStatus (this, s_strCmdStatus);
    }
    else if (CModem::eStatus == eCommand)
    {
        pFirst = new CStatus (this, s_strCmdStatus);
    }
    else if (CModem::eResync == eCommand)
    {
        pFirst = new CResync (this, s_strCmdResync);
    }
    else if (CModem::eReboot == eCommand)
    {
        pFirst = new CReboot (this, s_strCmdReboot);
    }

    Protocol ()->Next (pFirst);
}

const CTrendchip::CBandplanData CTrendchip::CBandplanData::g_acList[] = {
//begin user code
        CBandplanData ("ITU G.992.1(G.DMT)", "A", CBandplan::eADSL_A),
        CBandplanData ("ITU G.992.1(G.DMT)", "B", CBandplan::eADSL_B),
        CBandplanData ("ITU G.992.3(ADSL2)", "A", CBandplan::eADSL2_A),
        CBandplanData ("ITU G.992.3(ADSL2)", "B", CBandplan::eADSL2_B),
        CBandplanData ("ITU G.992.5(ADSL2PLUS)", "A", CBandplan::eADSL2p_A),
        CBandplanData ("ITU G.992.5(ADSL2PLUS)", "B", CBandplan::eADSL2p_B),
        CBandplanData ("ITU G.992.5(ADSL2PLUS)", "J", CBandplan::eADSL2p_J)
//end user code
};

CTrendchip::CBandplanData::CBandplanData (const char *strVersion,
        const char *strAnnex, CBandplan::EBandplan eBandplan)
: m_strVersion (strVersion)
, m_strAnnex (strAnnex)
, m_eBandplan (eBandplan)
{
}

CTrendchip::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
: m_strVersion (cBandplanData.m_strVersion)
, m_strAnnex (cBandplanData.m_strAnnex)
, m_eBandplan (cBandplanData.m_eBandplan)
{
}

CBandplan::EBandplan CTrendchip::CBandplanData::Get (const char *strVersion,
        const char *strAnnex)
{
    CBandplan::EBandplan eBandplan = CBandplan::eNone;

    for (size_t nIndex = 0; nIndex < ARRAY_SIZE (g_acList); nIndex++)
    {
//begin user code
        if ((NULL != strVersion) && (NULL != strAnnex))
        {
            if ((0 == strcmp (strVersion, g_acList[nIndex].m_strVersion)) &&
                    (0 == strcmp (strAnnex, g_acList[nIndex].m_strAnnex)))
            {
                eBandplan = g_acList[nIndex].m_eBandplan;
                break;
            }
        }
//end user code
    }

    return eBandplan;
}

CBandplan::EBandplan CTrendchip::Bandplan (const char *strVersion,
        const char *strAnnex)
{
    return CBandplanData::Get(strVersion, strAnnex);
}

//begin user code
//end user code

CLoginPass::CLoginPass (CModem *pModem, const char *strPass)
: CParser (pModem)
, m_strPass (strPass)
{
}

CParser *CLoginPass::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Password: "))
    {
        m_pModem->Protocol ()->Send (m_strPass, "pass");
        m_pModem->Protocol ()->Send (m_pModem->LF ());
    }
    else if (Find (strRecv, nRecv, m_pModem->Prompt ()))
    {
        if (CModem::eInfo == m_pModem->Command ())
        {
            pNext = new CStatus (m_pModem, s_strCmdStatus);
        }
        else if (CModem::eStatus == m_pModem->Command ())
        {
            pNext = new CStatus (m_pModem, s_strCmdStatus);
        }
        else if (CModem::eResync == m_pModem->Command ())
        {
            pNext = new CResync (m_pModem, s_strCmdResync);
        }
        else if (CModem::eReboot == m_pModem->Command ())
        {
            pNext = new CReboot (m_pModem, s_strCmdReboot);
        }
    }

    return pNext;
}

template <class T> CPromptParser<T>::CPromptParser (CModem *pModem, const char *strCmd)
: CRegExCmdParser<T, CLineParser> (pModem, strCmd)
, m_cPrompt ()
{
    m_cPrompt.AddConst ("tc> ");
    m_cPrompt.Init ();
}

CStatus::CStatus (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_strTemp0 ()
, m_cStatus ()
{
    m_cStatus.AddConst ("current modem status");
    m_cStatus.AddSep ();
    m_cStatus.AddConst (":");
    m_cStatus.AddSep ();
    m_cStatus.AddString (m_strTemp0);
    m_cStatus.Init ();
    AddCallback (m_cStatus, &CStatus::StatusCallback);

    AddCallback (m_cPrompt, &CStatus::PromptCallback);

//begin user code
//end user code
}

CParser *CStatus::StatusCallback (void)
{
//begin user code
    int nModemState = 0;
    if (0 == strncasecmp (m_strTemp0, "up", 2))
    {
        nModemState = 1;
    }
    m_pModem->Data ().m_cData.m_cModemState.Set (nModemState);
//end user code
    m_pModem->Data ().m_cData.m_cModemStateStr.Set (m_strTemp0);
    LogTrace ("ModemState: %s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CStatus::PromptCallback (void)
{
    return new COpMode (m_pModem, s_strCmdOpMode);
}

COpMode::COpMode (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_strTemp0 ()
, m_cMode ()
{
    m_cMode.AddConst ("operational mode");
    m_cMode.AddSep ();
    m_cMode.AddConst (":");
    m_cMode.AddSep ();
    m_cMode.AddString (m_strTemp0);
    m_cMode.Init ();
    AddCallback (m_cMode, &COpMode::ModeCallback);

    AddCallback (m_cPrompt, &COpMode::PromptCallback);

//begin user code
//end user code
}

CParser *COpMode::ModeCallback (void)
{
//begin user code
    CTrendchip *pModem = (CTrendchip*)m_pModem;
    pModem->m_strOpMode = m_strTemp0;
    LogTrace ("OperationMode: %s\n", (const char*)m_strTemp0);
//end user code

    return this;
}

CParser *COpMode::PromptCallback (void)
{
//begin user code
//end user code

    return new CAnnex (m_pModem, s_strCmdAnnex);
}

CAnnex::CAnnex (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_strTemp0 ()
, m_cAnnex ()
{
    m_cAnnex.AddConst ("ADSL ANNEX[[]HW[]]");
    m_cAnnex.AddSep ();
    m_cAnnex.AddConst (":");
    m_cAnnex.AddSep ();
    m_cAnnex.AddString (m_strTemp0);
    m_cAnnex.Init ();
    AddCallback (m_cAnnex, &CAnnex::AnnexCallback);

    AddCallback (m_cPrompt, &CAnnex::PromptCallback);

//begin user code
//end user code
}

CParser *CAnnex::AnnexCallback (void)
{
//begin user code
    CTrendchip *pModem = (CTrendchip*)m_pModem;
    pModem->m_strAnnex = m_strTemp0;
    LogTrace ("Annex: %s\n", (const char*)m_strTemp0);
//end user code

    return this;
}

CParser *CAnnex::PromptCallback (void)
{
//begin user code
    CTrendchip *pModem = (CTrendchip*)m_pModem;
    CString strOperationMode;

    m_pModem->Data ().Bandplan (CTrendchip::Bandplan (pModem->m_strOpMode,
            pModem->m_strAnnex));
    m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
    m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);

    LogTrace ("OperationMode: %s\n", (const char*)strOperationMode);
//end user code

    return new CChan (m_pModem, s_strCmdChan);
}

CChan::CChan (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_strTemp0 ()
, m_cDown ()
, m_cUp ()
{
    m_cDown.AddConst ("near-end");
    m_cDown.AddSep ();
    m_cDown.AddString (m_strTemp0);
    m_cDown.AddSep ();
    m_cDown.AddConst ("channel bit rate");
    m_cDown.AddSep ();
    m_cDown.AddConst (":");
    m_cDown.AddSep ();
    m_cDown.AddInt (m_nTemp0);
    m_cDown.AddSep ();
    m_cDown.AddConst ("kbps");
    m_cDown.Init ();
    AddCallback (m_cDown, &CChan::DownCallback);

    m_cUp.AddConst ("far-end");
    m_cUp.AddSep ();
    m_cUp.AddString (m_strTemp0);
    m_cUp.AddSep ();
    m_cUp.AddConst ("channel bit rate");
    m_cUp.AddSep ();
    m_cUp.AddConst (":");
    m_cUp.AddSep ();
    m_cUp.AddInt (m_nTemp0);
    m_cUp.AddSep ();
    m_cUp.AddConst ("kbps");
    m_cUp.Init ();
    AddCallback (m_cUp, &CChan::UpCallback);

    AddCallback (m_cPrompt, &CChan::PromptCallback);

//begin user code
//end user code
}

CParser *CChan::DownCallback (void)
{
//begin user code
    if (m_nTemp0 > 0)
    {
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cChannelMode.Set (m_strTemp0);
    LogTrace ("BandwidthKbits Down: %ld\n", m_nTemp0);
    LogTrace ("ChannelMode: %s\n", (const char*)m_strTemp0);
//begin user code
    }
//end user code

    return this;
}

CParser *CChan::UpCallback (void)
{
//begin user code
    if (m_nTemp0 > 0)
    {
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetUp (m_nTemp0);
    LogTrace ("BandwidthKbits Up: %ld\n", m_nTemp0);
//begin user code
    }
//end user code

    return this;
}

CParser *CChan::PromptCallback (void)
{
    return new CPerf (m_pModem, s_strCmdPerf);
}

CPerf::CPerf (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_strTemp0 ()
, m_cDownFEC ()
, m_cDownCRC ()
, m_cDownHEC ()
, m_cUpFEC ()
, m_cUpCRC ()
, m_cUpHEC ()
, m_cErrSec15min ()
, m_cErrSecDay ()
{
    m_cDownFEC.AddConst ("near-end FEC error");
    m_cDownFEC.AddSep ();
    m_cDownFEC.AddString (m_strTemp0);
    m_cDownFEC.AddConst (":");
    m_cDownFEC.AddSep ();
    m_cDownFEC.AddInt (m_nTemp0);
    m_cDownFEC.Init ();
    AddCallback (m_cDownFEC, &CPerf::DownFECCallback);

    m_cDownCRC.AddConst ("near-end CRC error");
    m_cDownCRC.AddSep ();
    m_cDownCRC.AddString (m_strTemp0);
    m_cDownCRC.AddConst (":");
    m_cDownCRC.AddSep ();
    m_cDownCRC.AddInt (m_nTemp0);
    m_cDownCRC.Init ();
    AddCallback (m_cDownCRC, &CPerf::DownCRCCallback);

    m_cDownHEC.AddConst ("near-end HEC error");
    m_cDownHEC.AddSep ();
    m_cDownHEC.AddString (m_strTemp0);
    m_cDownHEC.AddConst (":");
    m_cDownHEC.AddSep ();
    m_cDownHEC.AddInt (m_nTemp0);
    m_cDownHEC.Init ();
    AddCallback (m_cDownHEC, &CPerf::DownHECCallback);

    m_cUpFEC.AddConst ("far-end FEC error");
    m_cUpFEC.AddSep ();
    m_cUpFEC.AddString (m_strTemp0);
    m_cUpFEC.AddConst (":");
    m_cUpFEC.AddSep ();
    m_cUpFEC.AddInt (m_nTemp0);
    m_cUpFEC.Init ();
    AddCallback (m_cUpFEC, &CPerf::UpFECCallback);

    m_cUpCRC.AddConst ("far-end CRC error");
    m_cUpCRC.AddSep ();
    m_cUpCRC.AddString (m_strTemp0);
    m_cUpCRC.AddConst (":");
    m_cUpCRC.AddSep ();
    m_cUpCRC.AddInt (m_nTemp0);
    m_cUpCRC.Init ();
    AddCallback (m_cUpCRC, &CPerf::UpCRCCallback);

    m_cUpHEC.AddConst ("far-end HEC error");
    m_cUpHEC.AddSep ();
    m_cUpHEC.AddString (m_strTemp0);
    m_cUpHEC.AddConst (":");
    m_cUpHEC.AddSep ();
    m_cUpHEC.AddInt (m_nTemp0);
    m_cUpHEC.Init ();
    AddCallback (m_cUpHEC, &CPerf::UpHECCallback);

    m_cErrSec15min.AddConst ("Error second in 15min");
    m_cErrSec15min.AddSep ();
    m_cErrSec15min.AddConst ("..:");
    m_cErrSec15min.AddSep ();
    m_cErrSec15min.AddInt (m_nTemp0);
    m_cErrSec15min.Init ();
    AddCallback (m_cErrSec15min, &CPerf::ErrSec15minCallback);

    m_cErrSecDay.AddConst ("Error second in 24hr");
    m_cErrSecDay.AddSep ();
    m_cErrSecDay.AddConst ("..:");
    m_cErrSecDay.AddSep ();
    m_cErrSecDay.AddInt (m_nTemp0);
    m_cErrSecDay.Init ();
    AddCallback (m_cErrSecDay, &CPerf::ErrSecDayCallback);

    AddCallback (m_cPrompt, &CPerf::PromptCallback);

//begin user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.Set(0, 0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.Set(0, 0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.Set(0, 0);
//end user code
}

CParser *CPerf::DownFECCallback (void)
{
//begin user code
    if (m_nTemp0 > 0)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetDown (m_nTemp0);
    LogTrace ("ErrorsFEC Down: %ld\n", m_nTemp0);
//begin user code
    }
//end user code

    return this;
}

CParser *CPerf::DownCRCCallback (void)
{
//begin user code
    if (m_nTemp0 > 0)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (m_nTemp0);
    LogTrace ("ErrorsCRC Down: %ld\n", m_nTemp0);
//begin user code
    }
//end user code

    return this;
}

CParser *CPerf::DownHECCallback (void)
{
//begin user code
    if (m_nTemp0 > 0)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetDown (m_nTemp0);
    LogTrace ("ErrorsHEC Down: %ld\n", m_nTemp0);
//begin user code
    }
//end user code

    return this;
}

CParser *CPerf::UpFECCallback (void)
{
//begin user code
    if (m_nTemp0 > 0)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetUp (m_nTemp0);
    LogTrace ("ErrorsFEC Up: %ld\n", m_nTemp0);
//begin user code
    }
//end user code

    return this;
}

CParser *CPerf::UpCRCCallback (void)
{
//begin user code
    if (m_nTemp0 > 0)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetUp (m_nTemp0);
    LogTrace ("ErrorsCRC Up: %ld\n", m_nTemp0);
//begin user code
    }
//end user code

    return this;
}

CParser *CPerf::UpHECCallback (void)
{
//begin user code
    if (m_nTemp0 > 0)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetUp (m_nTemp0);
    LogTrace ("ErrorsHEC Up: %ld\n", m_nTemp0);
//begin user code
    }
//end user code

    return this;
}

CParser *CPerf::ErrSec15minCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min.Set (m_nTemp0);
    LogTrace ("ErrorSecs 15min: %ld\n", m_nTemp0);

    return this;
}

CParser *CPerf::ErrSecDayCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay.Set (m_nTemp0);
    LogTrace ("ErrorSecs day: %ld\n", m_nTemp0);

    return this;
}

CParser *CPerf::PromptCallback (void)
{
    CParser *pNext = this;

    if (CModem::eInfo == m_pModem->Command ())
    {
        pNext = new CAtm (m_pModem, s_strCmdAtm);
    }
    else
    {
        pNext = new CExit (m_pModem, s_strCmdExit);
    }

    return pNext;
}

CAtm::CAtm (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_cVpiVci ()
{
    m_cVpiVci.AddConst ("VPI/VCI");
    m_cVpiVci.AddSep ();
    m_cVpiVci.AddConst ("=");
    m_cVpiVci.AddSep ();
    m_cVpiVci.AddInt (m_nTemp0);
    m_cVpiVci.AddSep ();
    m_cVpiVci.AddConst ("/");
    m_cVpiVci.AddSep ();
    m_cVpiVci.AddInt (m_nTemp1);
    m_cVpiVci.Init ();
    AddCallback (m_cVpiVci, &CAtm::VpiVciCallback);

    AddCallback (m_cPrompt, &CAtm::PromptCallback);

//begin user code
//end user code
}

CParser *CAtm::VpiVciCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cVpiVci.Set (m_nTemp0, m_nTemp1);
    LogTrace ("VPI: %ld VCI: %ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CAtm::PromptCallback (void)
{
    return new CAtuR (m_pModem, s_strCmdAtuR);
}

CAtuR::CAtuR (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_nTemp3 (0)
, m_nTemp4 (0)
, m_nTemp5 (0)
, m_nTemp6 (0)
, m_nTemp7 (0)
, m_cID ()
{
    m_cID.AddConst ("near end itu identification");
    m_cID.AddSep ();
    m_cID.AddConst (":");
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp0, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp1, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp2, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp3, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp4, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp5, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp6, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp7, 16);
    m_cID.Init ();
    AddCallback (m_cID, &CAtuR::IDCallback);

    AddCallback (m_cPrompt, &CAtuR::PromptCallback);

//begin user code
//end user code
}

CParser *CAtuR::IDCallback (void)
{
//begin user code
    CString strVendor;
    strVendor.Format ("%c%c%c%c", (char)m_nTemp2, (char)m_nTemp3, (char)m_nTemp4, (char)m_nTemp5);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetVendor (strVendor);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetSpec ((int)m_nTemp6, (int)m_nTemp7);

    LogTrace ("ATU-R Vendor: %s %ld %ld\n", (const char*)strVendor, m_nTemp6, m_nTemp7);
//end user code

    return this;
}

CParser *CAtuR::PromptCallback (void)
{
    return new CAtuC (m_pModem, s_strCmdAtuC);
}

CAtuC::CAtuC (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_nTemp3 (0)
, m_nTemp4 (0)
, m_nTemp5 (0)
, m_nTemp6 (0)
, m_nTemp7 (0)
, m_cID ()
{
    m_cID.AddConst ("far end itu identification");
    m_cID.AddSep ();
    m_cID.AddConst (":");
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp0, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp1, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp2, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp3, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp4, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp5, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp6, 16);
    m_cID.AddSep ();
    m_cID.AddInt (m_nTemp7, 16);
    m_cID.Init ();
    AddCallback (m_cID, &CAtuC::IDCallback);

    AddCallback (m_cPrompt, &CAtuC::PromptCallback);

//begin user code
//end user code
}

CParser *CAtuC::IDCallback (void)
{
//begin user code
    CString strVendor;
    strVendor.Format ("%c%c%c%c", (char)m_nTemp2, (char)m_nTemp3, (char)m_nTemp4, (char)m_nTemp5);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetVendor (strVendor);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetSpec ((int)m_nTemp6, (int)m_nTemp7);

    LogTrace ("ATU-C Vendor: %s %ld %ld\n", (const char*)strVendor, m_nTemp6, m_nTemp7);
//end user code

    return this;
}

CParser *CAtuC::PromptCallback (void)
{
    return new CLineUp (m_pModem, s_strCmdLineUp);
}

CLineUp::CLineUp (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_fTemp0 (0)
, m_cNoiseMargin ()
, m_cTxPower ()
, m_cAttenuation ()
{
    m_cNoiseMargin.AddConst ("noise margin downstream");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddConst (":");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddDouble (m_fTemp0);
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddConst ("db");
    m_cNoiseMargin.Init ();
    AddCallback (m_cNoiseMargin, &CLineUp::NoiseMarginCallback);

    m_cTxPower.AddConst ("output power upstream");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst (":");
    m_cTxPower.AddSep ();
    m_cTxPower.AddDouble (m_fTemp0);
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst ("dbm");
    m_cTxPower.Init ();
    AddCallback (m_cTxPower, &CLineUp::TxPowerCallback);

    m_cAttenuation.AddConst ("attenuation downstream");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst (":");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp0);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst ("db");
    m_cAttenuation.Init ();
    AddCallback (m_cAttenuation, &CLineUp::AttenuationCallback);

    AddCallback (m_cPrompt, &CLineUp::PromptCallback);

//begin user code
//end user code
}

CParser *CLineUp::NoiseMarginCallback (void)
{
    m_pModem->Data ().m_cData.m_cNoiseMargin.SetDown (0, m_fTemp0);
    LogTrace ("NoiseMargin Down: %lf\n", m_fTemp0);

    return this;
}

CParser *CLineUp::TxPowerCallback (void)
{
    m_pModem->Data ().m_cData.m_cTxPower.SetUp (m_fTemp0);
    LogTrace ("TxPower Up: %lf\n", m_fTemp0);

    return this;
}

CParser *CLineUp::AttenuationCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.SetDown (0, m_fTemp0);
    LogTrace ("Attenuation Down: %lf\n", m_fTemp0);

    return this;
}

CParser *CLineUp::PromptCallback (void)
{
    return new CLineDown (m_pModem, s_strCmdLineDown);
}

CLineDown::CLineDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_fTemp0 (0)
, m_cTone ()
, m_bToneValid (false)
, m_cNoiseMargin ()
, m_cTxPower ()
, m_cAttenuation ()
, m_cBitAlloc ()
, m_cTones ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cNoiseMargin.AddConst ("noise margin upstream");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddConst (":");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddDouble (m_fTemp0);
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddConst ("db");
    m_cNoiseMargin.Init ();
    AddCallback (m_cNoiseMargin, &CLineDown::NoiseMarginCallback);

    m_cTxPower.AddConst ("output power downstream");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst (":");
    m_cTxPower.AddSep ();
    m_cTxPower.AddDouble (m_fTemp0);
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst ("dbm");
    m_cTxPower.Init ();
    AddCallback (m_cTxPower, &CLineDown::TxPowerCallback);

    m_cAttenuation.AddConst ("attenuation upstream");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst (":");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp0);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst ("db");
    m_cAttenuation.Init ();
    AddCallback (m_cAttenuation, &CLineDown::AttenuationCallback);

    m_cBitAlloc.AddConst ("carrier load: number of bits per symbol[(]tone[)]");
    m_cBitAlloc.Init ();
    AddCallback (m_cBitAlloc, &CLineDown::BitAllocCallback);

    m_cTones.AddConst ("tone");
    m_cTones.AddSep ();
    m_cTones.AddToneIndex (m_cTone);
    m_cTones.AddSep ();
    m_cTones.AddConst ("-");
    m_cTones.AddSep ();
    m_cTones.AddInt (m_nTemp0);
    m_cTones.AddSep ();
    m_cTones.AddConst (":");
    for (int i = 0; i < 16; i++)
    {
        m_cTones.AddSep ();
        m_cTones.AddTone (m_cTone, 16, 1);
        m_cTones.AddTone (m_cTone, 16, 1);
    }
    m_cTones.Init ();
    AddCallback (m_cTones, &CLineDown::TonesCallback, eBitAlloc);

    AddCallback (m_cPrompt, &CLineDown::PromptCallback);

//begin user code
//end user code
}

CParser *CLineDown::NoiseMarginCallback (void)
{
    m_pModem->Data ().m_cData.m_cNoiseMargin.SetUp (0, m_fTemp0);
    LogTrace ("NoiseMargin Up: %lf\n", m_fTemp0);

    return this;
}

CParser *CLineDown::TxPowerCallback (void)
{
    m_pModem->Data ().m_cData.m_cTxPower.SetDown (m_fTemp0);
    LogTrace ("TxPower Down: %lf\n", m_fTemp0);

    return this;
}

CParser *CLineDown::AttenuationCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.SetUp (0, m_fTemp0);
    LogTrace ("Attenuation Up: %lf\n", m_fTemp0);

    return this;
}

CParser *CLineDown::BitAllocCallback (void)
{
    m_nState = eBitAlloc;
    LogTrace ("State BitAlloc\n");

    return this;
}

CParser *CLineDown::TonesCallback (void)
{
    m_bToneValid = true;

    return this;
}

CParser *CLineDown::PromptCallback (void)
{
//begin user code
    if (m_bToneValid)
    {
        CBandplan *pBand = m_pModem->Data ().Bandplan ();

        for (size_t nBand = 0; nBand < pBand->Bands (); nBand++)
        {
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocUp.Set (m_cTone,
                    (size_t)pBand->Band (nBand).MinUp (),
                    (size_t)pBand->Band (nBand).MaxUp ());
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocDown.Set (m_cTone,
                    (size_t)pBand->Band (nBand).MinDown (),
                    (size_t)pBand->Band (nBand).MaxDown ());
        }
    }
//end user code

    return new CExit (m_pModem, s_strCmdExit);
}

CResync::CResync (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CResync::PromptCallback);

//begin user code
//end user code
}

CParser *CResync::PromptCallback (void)
{
    return new CExit (m_pModem, s_strCmdExit);
}

CReboot::CReboot (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CReboot::PromptCallback);

//begin user code
//end user code
}

CParser *CReboot::PromptCallback (void)
{
    return new CExit (m_pModem, s_strCmdExit);
}

CExit::CExit (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CExit::PromptCallback);

//begin user code
//end user code
}

CParser *CExit::PromptCallback (void)
{
    return NULL;
}

//! @endcond
//_oOo_
