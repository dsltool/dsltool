/*!
 * @file        speedtouch.cpp
 * @brief       speedtouch modem implementation
 * @details     parser for ALCATEL/Thomson Speedtouch modems, tested with:
 * @n           Speedtouch 516i V6 FW 5.4.0.14
 * @n           Speedtouch 585i V6 FW 6.1.0.5
 * @n           Speedtouch 536i V6 FW 6.2.15.5
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from speedtouch.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-speedtouch-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_SPEEDTOUCH_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
//end user code

#include "speedtouch.h"
#include "log.h"

//! @cond
using namespace NSpeedtouch;

static const char *s_strCmdEnvList = "env list";
static const char *s_strCmdAdslInfoV5 = "adsl info";
static const char *s_strCmdAdslInfoV6 = "adsl info expand=1";
static const char *s_strCmdBitLoadV5 = "debug exec cmd='tdsl getData all'";
static const char *s_strCmdBitLoadV6 = "adsl debug bitloadinginfo";
static const char *s_strCmdAdslDown = "adsl config status down";
static const char *s_strCmdAdslUp = "adsl config status up";
static const char *s_strCmdSysReboot = "system reboot";
static const char *s_strCmdExit = "exit";

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CSpeedtouch (cData, pProtocol);
}

const long CSpeedtouch::s_nVersionV6 = CModem::BuildVersion (6, 2, 0, 0);

CSpeedtouch::CSpeedtouch (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
//end user code
{
//begin user code
//end user code
}

void CSpeedtouch::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("{%s}=>", User ());
    m_strLF.Format ("\r\n");

//begin user code
//end user code

    if (m_pLogin)
    {
        pFirst = new CLoginUser (this, User ());
    }
    else if (CModem::eInfo == eCommand)
    {
        pFirst = new CEnvList (this, s_strCmdEnvList);
    }
    else if (CModem::eStatus == eCommand)
    {
        pFirst = new CEnvList (this, s_strCmdEnvList);
    }
    else if (CModem::eResync == eCommand)
    {
        pFirst = new CAdslDown (this, s_strCmdAdslDown);
    }
    else if (CModem::eReboot == eCommand)
    {
        pFirst = new CSysReboot (this, s_strCmdSysReboot);
    }

    Protocol ()->Next (pFirst);
}

const CSpeedtouch::CBandplanData CSpeedtouch::CBandplanData::g_acList[] = {
//begin user code
        CBandplanData ("G.992.1", "A", CBandplan::eADSL_A),
        CBandplanData ("G.992.1", "B", CBandplan::eADSL_B),
        CBandplanData ("G.992.3", "A", CBandplan::eADSL2_A),
        CBandplanData ("G.992.3", "B", CBandplan::eADSL2_B),
        CBandplanData ("G.992.5", "A", CBandplan::eADSL2p_A),
        CBandplanData ("G.992.5", "B", CBandplan::eADSL2p_B)
//end user code
};

CSpeedtouch::CBandplanData::CBandplanData (const char *strVersion,
        const char *strAnnex, CBandplan::EBandplan eBandplan)
: m_strVersion (strVersion)
, m_strAnnex (strAnnex)
, m_eBandplan (eBandplan)
{
}

CSpeedtouch::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
: m_strVersion (cBandplanData.m_strVersion)
, m_strAnnex (cBandplanData.m_strAnnex)
, m_eBandplan (cBandplanData.m_eBandplan)
{
}

CBandplan::EBandplan CSpeedtouch::CBandplanData::Get (const char *strVersion,
        const char *strAnnex)
{
    CBandplan::EBandplan eBandplan = CBandplan::eNone;

    for (size_t nIndex = 0; nIndex < ARRAY_SIZE (g_acList); nIndex++)
    {
//begin user code
        if ((NULL != strVersion) && (NULL != strAnnex))
        {
            if ((0 == strcmp (strVersion, g_acList[nIndex].m_strVersion)) &&
                    (0 == strcmp (strAnnex, g_acList[nIndex].m_strAnnex)))
            {
                eBandplan = g_acList[nIndex].m_eBandplan;
                break;
            }
        }
//end user code
    }

    return eBandplan;
}

CBandplan::EBandplan CSpeedtouch::Bandplan (const char *strVersion,
        const char *strAnnex)
{
    return CBandplanData::Get(strVersion, strAnnex);
}

//begin user code
//end user code

CLoginUser::CLoginUser (CModem *pModem, const char *strUser)
: CParser (pModem)
, m_strUser (strUser)
{
}

CParser *CLoginUser::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Username : "))
    {
        m_pModem->Protocol ()->Send (m_strUser, "user");
        m_pModem->Protocol ()->Send (m_pModem->LF ());

        pNext = new CLoginPass (m_pModem, m_pModem->Pass ());
    }

    return pNext;
}

CLoginPass::CLoginPass (CModem *pModem, const char *strPass)
: CParser (pModem)
, m_strPass (strPass)
{
}

CParser *CLoginPass::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Password : "))
    {
        m_pModem->Protocol ()->Send (m_strPass, "pass");
        m_pModem->Protocol ()->Send (m_pModem->LF ());
    }
    else if (Find (strRecv, nRecv, m_pModem->Prompt ()))
    {
        if (CModem::eInfo == m_pModem->Command ())
        {
            pNext = new CEnvList (m_pModem, s_strCmdEnvList);
        }
        else if (CModem::eStatus == m_pModem->Command ())
        {
            pNext = new CEnvList (m_pModem, s_strCmdEnvList);
        }
        else if (CModem::eResync == m_pModem->Command ())
        {
            pNext = new CAdslDown (m_pModem, s_strCmdAdslDown);
        }
        else if (CModem::eReboot == m_pModem->Command ())
        {
            pNext = new CSysReboot (m_pModem, s_strCmdSysReboot);
        }
    }

    return pNext;
}

template <class T> CPromptParser<T>::CPromptParser (CModem *pModem, const char *strCmd)
: CRegExCmdParser<T, CLineParser> (pModem, strCmd)
, m_cPrompt ()
{
    m_cPrompt.AddConst ("[{]");
    m_cPrompt.AddConst (pModem->User ());
    m_cPrompt.AddConst ("[}]=>");
    m_cPrompt.Init ();
}

CEnvList::CEnvList (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_nTemp3 (0)
, m_cBuild ()
, m_cATMaddr ()
, m_cIntf1PVC ()
{
    m_cBuild.AddConst ("_BUILD=");
    m_cBuild.AddInt (m_nTemp0);
    m_cBuild.AddConst ("\\.");
    m_cBuild.AddInt (m_nTemp1);
    m_cBuild.AddConst ("\\.");
    m_cBuild.AddInt (m_nTemp2);
    m_cBuild.AddConst ("\\.");
    m_cBuild.AddInt (m_nTemp3);
    m_cBuild.Init ();
    AddCallback (m_cBuild, &CEnvList::BuildCallback);

    m_cATMaddr.AddConst ("ATM_addr=");
    m_cATMaddr.AddInt (m_nTemp0);
    m_cATMaddr.AddConst ("\\.");
    m_cATMaddr.AddInt (m_nTemp1);
    m_cATMaddr.Init ();
    AddCallback (m_cATMaddr, &CEnvList::ATMaddrCallback);

    m_cIntf1PVC.AddConst ("Intf1_PVC=");
    m_cIntf1PVC.AddInt (m_nTemp0);
    m_cIntf1PVC.AddConst ("\\.");
    m_cIntf1PVC.AddInt (m_nTemp1);
    m_cIntf1PVC.Init ();
    AddCallback (m_cIntf1PVC, &CEnvList::Intf1PVCCallback);

    AddCallback (m_cPrompt, &CEnvList::PromptCallback);

//begin user code
//end user code
}

CParser *CEnvList::BuildCallback (void)
{
    m_pModem->Version (CModem::BuildVersion (m_nTemp0, m_nTemp1, m_nTemp2, m_nTemp3));
    LogTrace ("Version: %ld.%ld.%ld.%ld\n", m_nTemp0, m_nTemp1, m_nTemp2, m_nTemp3);

    return this;
}

CParser *CEnvList::ATMaddrCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cVpiVci.Set (m_nTemp0, m_nTemp1);
    LogTrace ("VPI/VCI: %ld/%ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CEnvList::Intf1PVCCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cVpiVci.Set (m_nTemp0, m_nTemp1);
    LogTrace ("VPI/VCI: %ld/%ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CEnvList::PromptCallback (void)
{
    CParser *pNext = this;

    if (m_pModem->Version () < CSpeedtouch::s_nVersionV6)
    {
        pNext = new CAdslInfo (m_pModem, s_strCmdAdslInfoV5);
    }
    else
    {
        pNext = new CAdslInfo (m_pModem, s_strCmdAdslInfoV6);
    }

    return pNext;
}

CAdslInfo::CAdslInfo (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_nTemp3 (0)
, m_fTemp0 (0)
, m_fTemp1 (0)
, m_strTemp0 ()
, m_strTemp1 ()
, m_bOpVersion (false)
, m_bOpAnnex (false)
, m_strOpVersion ()
, m_strOpAnnex ()
, m_cModemState ()
, m_cOperationMode ()
, m_cXDslStandard ()
, m_cXDslAnnex ()
, m_cChannelMode ()
, m_cVendorStateV5 ()
, m_cVendorStateV6Chip ()
, m_cVendorStateV6Sys ()
, m_cVendor ()
, m_cVendorSpec ()
, m_cStdRevision ()
, m_cNoiseMargin ()
, m_cAttenuation ()
, m_cTxPower ()
, m_cBandwidthStart ()
, m_cBandwidthEnd ()
, m_cBandwidthUp ()
, m_cBandwidthDown ()
, m_cStatisticsState ()
, m_cErrorsState ()
, m_cErrorsTxFEC ()
, m_cErrorsRxFEC ()
, m_cErrorsTxCRC ()
, m_cErrorsRxCRC ()
, m_cErrorsTxHEC ()
, m_cErrorsRxHEC ()
, m_cFail15minState ()
, m_cFailDayState ()
, m_cErrorSec ()
{
    m_cModemState.AddConst ("Modemstate");
    m_cModemState.AddSep ();
    m_cModemState.AddConst (":");
    m_cModemState.AddSep ();
    m_cModemState.AddString (m_strTemp0);
    m_cModemState.Init ();
    AddCallback (m_cModemState, &CAdslInfo::ModemStateCallback);

    m_cOperationMode.AddConst ("Operation Mode");
    m_cOperationMode.AddSep ();
    m_cOperationMode.AddConst (":");
    m_cOperationMode.AddSep ();
    m_cOperationMode.AddWord (m_strOpVersion);
    m_cOperationMode.AddSep ();
    m_cOperationMode.AddConst ("Annex");
    m_cOperationMode.AddSep ();
    m_cOperationMode.AddWord (m_strOpAnnex);
    m_cOperationMode.Init ();
    AddCallback (m_cOperationMode, &CAdslInfo::OperationModeCallback);

    m_cXDslStandard.AddConst ("xDSL Standard");
    m_cXDslStandard.AddSep ();
    m_cXDslStandard.AddConst (":");
    m_cXDslStandard.AddSep ();
    m_cXDslStandard.AddConst ("ITU-T");
    m_cXDslStandard.AddSep ();
    m_cXDslStandard.AddWord (m_strOpVersion);
    m_cXDslStandard.Init ();
    AddCallback (m_cXDslStandard, &CAdslInfo::XDslStandardCallback);

    m_cXDslAnnex.AddConst ("xDSL Annex");
    m_cXDslAnnex.AddSep ();
    m_cXDslAnnex.AddConst (":");
    m_cXDslAnnex.AddSep ();
    m_cXDslAnnex.AddConst ("annex");
    m_cXDslAnnex.AddSep ();
    m_cXDslAnnex.AddWord (m_strOpAnnex);
    m_cXDslAnnex.Init ();
    AddCallback (m_cXDslAnnex, &CAdslInfo::XDslAnnexCallback);

    m_cChannelMode.AddConst ("Channel Mode");
    m_cChannelMode.AddSep ();
    m_cChannelMode.AddConst (":");
    m_cChannelMode.AddSep ();
    m_cChannelMode.AddWord (m_strTemp0);
    m_cChannelMode.Init ();
    AddCallback (m_cChannelMode, &CAdslInfo::ChannelModeCallback);

    m_cVendorStateV5.AddConst ("Vendor");
    m_cVendorStateV5.AddSep ();
    m_cVendorStateV5.AddConst ("Local");
    m_cVendorStateV5.AddSep ();
    m_cVendorStateV5.AddConst ("Remote");
    m_cVendorStateV5.Init ();
    AddCallback (m_cVendorStateV5, &CAdslInfo::VendorStateV5Callback);

    m_cVendorStateV6Chip.AddConst ("Vendor Chipset");
    m_cVendorStateV6Chip.AddSep ();
    m_cVendorStateV6Chip.AddConst ("Local");
    m_cVendorStateV6Chip.AddSep ();
    m_cVendorStateV6Chip.AddConst ("Remote");
    m_cVendorStateV6Chip.Init ();
    AddCallback (m_cVendorStateV6Chip, &CAdslInfo::VendorStateV6ChipCallback);

    m_cVendorStateV6Sys.AddConst ("Vendor System");
    m_cVendorStateV6Sys.Init ();
    AddCallback (m_cVendorStateV6Sys, &CAdslInfo::VendorStateV6SysCallback);

    m_cVendor.AddConst ("Vendor");
    m_cVendor.AddSep ();
    m_cVendor.AddConst (":");
    m_cVendor.AddSep ();
    m_cVendor.AddWord (m_strTemp0);
    m_cVendor.AddSep ();
    m_cVendor.AddWord (m_strTemp1);
    m_cVendor.Init ();
    AddCallback (m_cVendor, &CAdslInfo::VendorV5Callback, eVendorV5);
    AddCallback (m_cVendor, &CAdslInfo::VendorV6ChipCallback, eVendorV6Chip);
    AddCallback (m_cVendor, &CAdslInfo::VendorV6SysCallback, eVendorV6Sys);

    m_cVendorSpec.AddConst ("VendorSpecific");
    m_cVendorSpec.AddSep ();
    m_cVendorSpec.AddConst (":");
    m_cVendorSpec.AddSep ();
    m_cVendorSpec.AddInt (m_nTemp0, 16, 2);
    m_cVendorSpec.AddInt (m_nTemp1, 16, 2);
    m_cVendorSpec.AddSep ();
    m_cVendorSpec.AddInt (m_nTemp2, 16, 2);
    m_cVendorSpec.AddInt (m_nTemp3, 16, 2);
    m_cVendorSpec.Init ();
    AddCallback (m_cVendorSpec, &CAdslInfo::VendorSpecV5Callback, eVendorV5);
    AddCallback (m_cVendorSpec, &CAdslInfo::VendorSpecV6ChipCallback, eVendorV6Chip);
    AddCallback (m_cVendorSpec, &CAdslInfo::VendorSpecV6SysCallback, eVendorV6Sys);

    m_cStdRevision.AddConst ("StandardRevisionNr");
    m_cStdRevision.AddSep ();
    m_cStdRevision.AddConst (":");
    m_cStdRevision.AddSep ();
    m_cStdRevision.AddInt (m_nTemp0, 16);
    m_cStdRevision.AddSep ();
    m_cStdRevision.AddInt (m_nTemp1, 16);
    m_cStdRevision.Init ();
    AddCallback (m_cStdRevision, &CAdslInfo::StdRevisionV5Callback, eVendorV5);
    AddCallback (m_cStdRevision, &CAdslInfo::StdRevisionV6ChipCallback, eVendorV6Chip);
    AddCallback (m_cStdRevision, &CAdslInfo::StdRevisionV6SysCallback, eVendorV6Sys);

    m_cNoiseMargin.AddConst ("Margin");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddConst ("[[(]dB[])]");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddConst (":");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddDouble (m_fTemp0);
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddDouble (m_fTemp1);
    m_cNoiseMargin.Init ();
    AddCallback (m_cNoiseMargin, &CAdslInfo::NoiseMarginCallback);

    m_cAttenuation.AddConst ("Attenuation");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst ("[[(]dB[])]");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst (":");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp0);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp1);
    m_cAttenuation.Init ();
    AddCallback (m_cAttenuation, &CAdslInfo::AttenuationCallback);

    m_cTxPower.AddConst ("OutputPower");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst ("[[(]dBm[])]");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst (":");
    m_cTxPower.AddSep ();
    m_cTxPower.AddDouble (m_fTemp0);
    m_cTxPower.AddSep ();
    m_cTxPower.AddDouble (m_fTemp1);
    m_cTxPower.Init ();
    AddCallback (m_cTxPower, &CAdslInfo::TxPowerCallback);

    m_cBandwidthStart.AddConst ("Available Bandwidth|Total Available Bandwidth");
    m_cBandwidthStart.AddSep ();
    m_cBandwidthStart.AddConst ("Cells/s");
    m_cBandwidthStart.AddSep ();
    m_cBandwidthStart.AddConst ("Kbit/s");
    m_cBandwidthStart.Init ();
    AddCallback (m_cBandwidthStart, &CAdslInfo::BandwidthStartCallback);

    m_cBandwidthEnd.AddConst ("Intrinsic/Actual and Maximum Bandwidth");
    m_cBandwidthEnd.AddSep ();
    m_cBandwidthEnd.AddConst ("%");
    m_cBandwidthEnd.AddSep ();
    m_cBandwidthEnd.AddConst ("Kbit/s");
    m_cBandwidthEnd.Init ();
    AddCallback (m_cBandwidthEnd, &CAdslInfo::BandwidthEndCallback);

    m_cBandwidthUp.AddConst ("Upstream");
    m_cBandwidthUp.AddSep ();
    m_cBandwidthUp.AddConst (":");
    m_cBandwidthUp.AddSep ();
    m_cBandwidthUp.AddInt (m_nTemp0);
    m_cBandwidthUp.AddSep ();
    m_cBandwidthUp.AddInt (m_nTemp1);
    m_cBandwidthUp.Init ();
    AddCallback (m_cBandwidthUp, &CAdslInfo::BandwidthUpCallback, eBandwidth);

    m_cBandwidthDown.AddConst ("Downstream");
    m_cBandwidthDown.AddSep ();
    m_cBandwidthDown.AddConst (":");
    m_cBandwidthDown.AddSep ();
    m_cBandwidthDown.AddInt (m_nTemp0);
    m_cBandwidthDown.AddSep ();
    m_cBandwidthDown.AddInt (m_nTemp1);
    m_cBandwidthDown.Init ();
    AddCallback (m_cBandwidthDown, &CAdslInfo::BandwidthDownCallback, eBandwidth);

    m_cStatisticsState.AddConst ("Transfer statistics");
    m_cStatisticsState.Init ();
    AddCallback (m_cStatisticsState, &CAdslInfo::StatisticsStateCallback);

    m_cErrorsState.AddConst ("Errors");
    m_cErrorsState.Init ();
    AddCallback (m_cErrorsState, &CAdslInfo::ErrorsStateCallback, eStatistics);

    m_cErrorsTxFEC.AddConst ("Transmitted FEC");
    m_cErrorsTxFEC.AddSep ();
    m_cErrorsTxFEC.AddConst (":");
    m_cErrorsTxFEC.AddSep ();
    m_cErrorsTxFEC.AddInt (m_nTemp0);
    m_cErrorsTxFEC.Init ();
    AddCallback (m_cErrorsTxFEC, &CAdslInfo::ErrorsTxFECCallback, eErrors);

    m_cErrorsRxFEC.AddConst ("Received FEC");
    m_cErrorsRxFEC.AddSep ();
    m_cErrorsRxFEC.AddConst (":");
    m_cErrorsRxFEC.AddSep ();
    m_cErrorsRxFEC.AddInt (m_nTemp0);
    m_cErrorsRxFEC.Init ();
    AddCallback (m_cErrorsRxFEC, &CAdslInfo::ErrorsRxFECCallback, eErrors);

    m_cErrorsTxCRC.AddConst ("Transmitted CRC");
    m_cErrorsTxCRC.AddSep ();
    m_cErrorsTxCRC.AddConst (":");
    m_cErrorsTxCRC.AddSep ();
    m_cErrorsTxCRC.AddInt (m_nTemp0);
    m_cErrorsTxCRC.Init ();
    AddCallback (m_cErrorsTxCRC, &CAdslInfo::ErrorsTxCRCCallback, eErrors);

    m_cErrorsRxCRC.AddConst ("Received CRC");
    m_cErrorsRxCRC.AddSep ();
    m_cErrorsRxCRC.AddConst (":");
    m_cErrorsRxCRC.AddSep ();
    m_cErrorsRxCRC.AddInt (m_nTemp0);
    m_cErrorsRxCRC.Init ();
    AddCallback (m_cErrorsRxCRC, &CAdslInfo::ErrorsRxCRCCallback, eErrors);

    m_cErrorsTxHEC.AddConst ("Transmitted HEC|Tranmsitted HEC");
    m_cErrorsTxHEC.AddSep ();
    m_cErrorsTxHEC.AddConst (":");
    m_cErrorsTxHEC.AddSep ();
    m_cErrorsTxHEC.AddInt (m_nTemp0);
    m_cErrorsTxHEC.Init ();
    AddCallback (m_cErrorsTxHEC, &CAdslInfo::ErrorsTxHECCallback, eErrors);

    m_cErrorsRxHEC.AddConst ("Received HEC");
    m_cErrorsRxHEC.AddSep ();
    m_cErrorsRxHEC.AddConst (":");
    m_cErrorsRxHEC.AddSep ();
    m_cErrorsRxHEC.AddInt (m_nTemp0);
    m_cErrorsRxHEC.Init ();
    AddCallback (m_cErrorsRxHEC, &CAdslInfo::ErrorsRxHECCallback, eErrors);

    m_cFail15minState.AddConst ("Near end failures last 15 minutes");
    m_cFail15minState.Init ();
    AddCallback (m_cFail15minState, &CAdslInfo::Fail15minStateCallback);

    m_cFailDayState.AddConst ("Near end failures current day");
    m_cFailDayState.Init ();
    AddCallback (m_cFailDayState, &CAdslInfo::FailDayStateCallback);

    m_cErrorSec.AddConst ("Errored seconds");
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst (":");
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddInt (m_nTemp0);
    m_cErrorSec.Init ();
    AddCallback (m_cErrorSec, &CAdslInfo::ErrorSec15minCallback, eFail15min);
    AddCallback (m_cErrorSec, &CAdslInfo::ErrorSecDayCallback, eFailDay);

    AddCallback (m_cPrompt, &CAdslInfo::PromptCallback);

//begin user code
//end user code
}

CParser *CAdslInfo::ModemStateCallback (void)
{
//begin user code
    int nModemState = 0;
    if (0 == strncasecmp (m_strTemp0, "up", 2))
    {
        nModemState = 1;
    }
    m_pModem->Data ().m_cData.m_cModemState.Set (nModemState);
//end user code
    m_pModem->Data ().m_cData.m_cModemStateStr.Set (m_strTemp0);
    LogTrace ("ModemState: %s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CAdslInfo::OperationModeCallback (void)
{
    m_bOpVersion = true;
    m_bOpAnnex = true;

    return this;
}

CParser *CAdslInfo::XDslStandardCallback (void)
{
    m_bOpVersion = true;

    return this;
}

CParser *CAdslInfo::XDslAnnexCallback (void)
{
    m_bOpAnnex = true;

    return this;
}

CParser *CAdslInfo::ChannelModeCallback (void)
{
    m_pModem->Data ().m_cData.m_cChannelMode.Set (m_strTemp0);
    LogTrace ("ChannelMode: %s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CAdslInfo::VendorStateV5Callback (void)
{
    m_nState = eVendorV5;
    LogTrace ("State VendorV5\n");

    return this;
}

CParser *CAdslInfo::VendorStateV6ChipCallback (void)
{
    m_nState = eVendorV6Chip;
    LogTrace ("State VendorV6Chip\n");

    return this;
}

CParser *CAdslInfo::VendorStateV6SysCallback (void)
{
    m_nState = eVendorV6Sys;
    LogTrace ("State VendorV6Sys\n");

    return this;
}

CParser *CAdslInfo::VendorV5Callback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetVendor (m_strTemp0);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetVendor (m_strTemp1);
    LogTrace ("Vendor R:%s C:%s\n", (const char*)m_strTemp0, (const char*)m_strTemp1);

    return this;
}

CParser *CAdslInfo::VendorV6ChipCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetVendor (m_strTemp1);
    LogTrace ("Vendor C:%s\n", (const char*)m_strTemp1);

    return this;
}

CParser *CAdslInfo::VendorV6SysCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetVendor (m_strTemp0);
    LogTrace ("Vendor R:%s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CAdslInfo::VendorSpecV5Callback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetSpec ((int)m_nTemp0, (int)m_nTemp1);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetSpec ((int)m_nTemp2, (int)m_nTemp3);
    LogTrace ("VendorSpec R:%ld.%ld C:%ld.%ld\n", m_nTemp0, m_nTemp1, m_nTemp2, m_nTemp3);

    return this;
}

CParser *CAdslInfo::VendorSpecV6ChipCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetSpec ((int)m_nTemp2, (int)m_nTemp3);
    LogTrace ("VendorSpec C:%ld.%ld\n", m_nTemp2, m_nTemp3);

    return this;
}

CParser *CAdslInfo::VendorSpecV6SysCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetSpec ((int)m_nTemp0, (int)m_nTemp1);
    LogTrace ("VendorSpec R:%ld.%ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CAdslInfo::StdRevisionV5Callback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetRevision ((int)m_nTemp0);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetRevision ((int)m_nTemp1);
    LogTrace ("StdRevision R:%ld C:%ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CAdslInfo::StdRevisionV6ChipCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetRevision ((int)m_nTemp1);
    LogTrace ("StdRevision C:%ld\n", m_nTemp1);

    return this;
}

CParser *CAdslInfo::StdRevisionV6SysCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetRevision ((int)m_nTemp0);
    LogTrace ("StdRevision R:%ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::NoiseMarginCallback (void)
{
    m_pModem->Data ().m_cData.m_cNoiseMargin.Set (0, m_fTemp1, m_fTemp0);
    LogTrace ("NoiseMargin Up: %f Down: %f\n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CAdslInfo::AttenuationCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.Set (0, m_fTemp1, m_fTemp0);
    LogTrace ("Attenuation Up: %f Down: %f\n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CAdslInfo::TxPowerCallback (void)
{
    m_pModem->Data ().m_cData.m_cTxPower.Set (m_fTemp1, m_fTemp0);
    LogTrace ("TxPower Up: %f Down: %f\n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CAdslInfo::BandwidthStartCallback (void)
{
    m_nState = eBandwidth;
    LogTrace ("State Bandwidth\n");

    return this;
}

CParser *CAdslInfo::BandwidthEndCallback (void)
{
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CAdslInfo::BandwidthUpCallback (void)
{
    m_pModem->Data ().m_cData.m_cBandwidth.m_cCells.SetUp (m_nTemp0);
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetUp (m_nTemp1);
    LogTrace ("Bandwidth Up:%ld Cells/s %ld Kbit/s\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CAdslInfo::BandwidthDownCallback (void)
{
    m_pModem->Data ().m_cData.m_cBandwidth.m_cCells.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetDown (m_nTemp1);
    LogTrace ("Bandwidth Down:%ld Cells/s %ld Kbit/s\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CAdslInfo::StatisticsStateCallback (void)
{
    m_nState = eStatistics;
    LogTrace ("State Statistics\n");

    return this;
}

CParser *CAdslInfo::ErrorsStateCallback (void)
{
    m_nState = eErrors;
    LogTrace ("State Errors\n");

    return this;
}

CParser *CAdslInfo::ErrorsTxFECCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetUp (m_nTemp0);
    LogTrace ("TxFEC: %ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::ErrorsRxFECCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetDown (m_nTemp0);
    LogTrace ("RxFEC: %ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::ErrorsTxCRCCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetUp (m_nTemp0);
    LogTrace ("TxCRC: %ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::ErrorsRxCRCCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (m_nTemp0);
    LogTrace ("RxCRC: %ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::ErrorsTxHECCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetUp (m_nTemp0);
    LogTrace ("TxHEC: %ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::ErrorsRxHECCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetDown (m_nTemp0);
    LogTrace ("TxHEC: %ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::Fail15minStateCallback (void)
{
    m_nState = eFail15min;
    LogTrace ("State Fail15min\n");

    return this;
}

CParser *CAdslInfo::FailDayStateCallback (void)
{
    m_nState = eFailDay;
    LogTrace ("State FailDay\n");

    return this;
}

CParser *CAdslInfo::ErrorSec15minCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min.Set (m_nTemp0);
    LogTrace ("Error secs/15min:%ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::ErrorSecDayCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay.Set (m_nTemp0);
    LogTrace ("Error secs/Day:%ld\n", m_nTemp0);

    return this;
}

CParser *CAdslInfo::PromptCallback (void)
{
    CParser *pNext = this;
//begin user code
    if (m_bOpVersion && m_bOpAnnex)
    {
        CString strOperationMode;

        m_pModem->Data ().Bandplan (CSpeedtouch::Bandplan (m_strOpVersion, m_strOpAnnex));
        m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
        m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);

        LogTrace ("OperationMode: %s\n", (const char*)strOperationMode);
    }
//end user code

    if ((CModem::eInfo == m_pModem->Command ()) &&
        (m_pModem->Version () < CSpeedtouch::s_nVersionV6))
    {
        pNext = new CBitLoad (m_pModem, s_strCmdBitLoadV5);
    }
    else if (CModem::eInfo == m_pModem->Command ())
    {
        pNext = new CBitLoad (m_pModem, s_strCmdBitLoadV6);
    }
    else
    {
        pNext = new CExit (m_pModem, s_strCmdExit);
    }

    return pNext;
}

CBitLoad::CBitLoad (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_bToneValid (false)
, m_cTone ()
, m_cToneV5_10 ()
, m_cToneV5_8 ()
, m_cToneV5_6 ()
, m_cToneV5_4 ()
, m_cToneV5_2 ()
, m_cToneV6_10 ()
, m_cToneV6_8 ()
, m_cToneV6_6 ()
, m_cToneV6_4 ()
, m_cToneV6_2 ()
, m_cStartBitAllocV5 ()
, m_cEndBitAllocV5 ()
, m_cStartBitAllocV6 ()
, m_cStartSNR ()
, m_cEndSNR ()
, m_cStartGainQ2 ()
, m_cEndGainQ2 ()
, m_cStartNoiseMargin ()
, m_cEndNoiseMargin ()
, m_cStartChanCharLog ()
, m_cEndChanCharLog ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cToneV5_10.AddConst ("tone");
    m_cToneV5_10.AddSep ();
    m_cToneV5_10.AddToneIndex (m_cTone);
    m_cToneV5_10.AddConst (":");
    for (int i = 0; i < 10; i++)
    {
        m_cToneV5_10.AddSep (true);
        m_cToneV5_10.AddTone (m_cTone);
    }
    m_cToneV5_10.Init ();
    AddCallback (m_cToneV5_10, &CBitLoad::ToneV5Callback, eBitAllocV5);
    AddCallback (m_cToneV5_10, &CBitLoad::ToneV5Callback, eSNR);
    AddCallback (m_cToneV5_10, &CBitLoad::ToneV5Callback, eGainQ2);
    AddCallback (m_cToneV5_10, &CBitLoad::ToneV5Callback, eNoiseMargin);
    AddCallback (m_cToneV5_10, &CBitLoad::ToneV5Callback, eChanCharLog);

    m_cToneV5_8.AddConst ("tone");
    m_cToneV5_8.AddSep ();
    m_cToneV5_8.AddToneIndex (m_cTone);
    m_cToneV5_8.AddConst (":");
    for (int i = 0; i < 8; i++)
    {
        m_cToneV5_8.AddSep (true);
        m_cToneV5_8.AddTone (m_cTone);
    }
    m_cToneV5_8.Init ();
    AddCallback (m_cToneV5_8, &CBitLoad::ToneV5Callback, eBitAllocV5);
    AddCallback (m_cToneV5_8, &CBitLoad::ToneV5Callback, eSNR);
    AddCallback (m_cToneV5_8, &CBitLoad::ToneV5Callback, eGainQ2);
    AddCallback (m_cToneV5_8, &CBitLoad::ToneV5Callback, eNoiseMargin);
    AddCallback (m_cToneV5_8, &CBitLoad::ToneV5Callback, eChanCharLog);

    m_cToneV5_6.AddConst ("tone");
    m_cToneV5_6.AddSep ();
    m_cToneV5_6.AddToneIndex (m_cTone);
    m_cToneV5_6.AddConst (":");
    for (int i = 0; i < 6; i++)
    {
        m_cToneV5_6.AddSep (true);
        m_cToneV5_6.AddTone (m_cTone);
    }
    m_cToneV5_6.Init ();
    AddCallback (m_cToneV5_6, &CBitLoad::ToneV5Callback, eBitAllocV5);
    AddCallback (m_cToneV5_6, &CBitLoad::ToneV5Callback, eSNR);
    AddCallback (m_cToneV5_6, &CBitLoad::ToneV5Callback, eGainQ2);
    AddCallback (m_cToneV5_6, &CBitLoad::ToneV5Callback, eNoiseMargin);
    AddCallback (m_cToneV5_6, &CBitLoad::ToneV5Callback, eChanCharLog);

    m_cToneV5_4.AddConst ("tone");
    m_cToneV5_4.AddSep ();
    m_cToneV5_4.AddToneIndex (m_cTone);
    m_cToneV5_4.AddConst (":");
    for (int i = 0; i < 4; i++)
    {
        m_cToneV5_4.AddSep (true);
        m_cToneV5_4.AddTone (m_cTone);
    }
    m_cToneV5_4.Init ();
    AddCallback (m_cToneV5_4, &CBitLoad::ToneV5Callback, eBitAllocV5);
    AddCallback (m_cToneV5_4, &CBitLoad::ToneV5Callback, eSNR);
    AddCallback (m_cToneV5_4, &CBitLoad::ToneV5Callback, eGainQ2);
    AddCallback (m_cToneV5_4, &CBitLoad::ToneV5Callback, eNoiseMargin);
    AddCallback (m_cToneV5_4, &CBitLoad::ToneV5Callback, eChanCharLog);

    m_cToneV5_2.AddConst ("tone");
    m_cToneV5_2.AddSep ();
    m_cToneV5_2.AddToneIndex (m_cTone);
    m_cToneV5_2.AddConst (":");
    for (int i = 0; i < 2; i++)
    {
        m_cToneV5_2.AddSep (true);
        m_cToneV5_2.AddTone (m_cTone);
    }
    m_cToneV5_2.Init ();
    AddCallback (m_cToneV5_2, &CBitLoad::ToneV5Callback, eBitAllocV5);
    AddCallback (m_cToneV5_2, &CBitLoad::ToneV5Callback, eSNR);
    AddCallback (m_cToneV5_2, &CBitLoad::ToneV5Callback, eGainQ2);
    AddCallback (m_cToneV5_2, &CBitLoad::ToneV5Callback, eNoiseMargin);
    AddCallback (m_cToneV5_2, &CBitLoad::ToneV5Callback, eChanCharLog);

    m_cToneV6_10.AddToneIndex (m_cTone);
    m_cToneV6_10.AddSep ();
    m_cToneV6_10.AddConst (":");
    for (int i = 0; i < 10; i++)
    {
        m_cToneV6_10.AddSep (true);
        m_cToneV6_10.AddTone (m_cTone);
    }
    m_cToneV6_10.Init ();
    AddCallback (m_cToneV6_10, &CBitLoad::ToneV6Callback, eBitAllocV6);

    m_cToneV6_8.AddToneIndex (m_cTone);
    m_cToneV6_8.AddSep ();
    m_cToneV6_8.AddConst (":");
    for (int i = 0; i < 8; i++)
    {
        m_cToneV6_8.AddSep (true);
        m_cToneV6_8.AddTone (m_cTone);
    }
    m_cToneV6_8.Init ();
    AddCallback (m_cToneV6_8, &CBitLoad::ToneV6Callback, eBitAllocV6);

    m_cToneV6_6.AddToneIndex (m_cTone);
    m_cToneV6_6.AddSep ();
    m_cToneV6_6.AddConst (":");
    for (int i = 0; i < 6; i++)
    {
        m_cToneV6_6.AddSep (true);
        m_cToneV6_6.AddTone (m_cTone);
    }
    m_cToneV6_6.Init ();
    AddCallback (m_cToneV6_6, &CBitLoad::ToneV6Callback, eBitAllocV6);

    m_cToneV6_4.AddToneIndex (m_cTone);
    m_cToneV6_4.AddSep ();
    m_cToneV6_4.AddConst (":");
    for (int i = 0; i < 4; i++)
    {
        m_cToneV6_4.AddSep (true);
        m_cToneV6_4.AddTone (m_cTone);
    }
    m_cToneV6_4.Init ();
    AddCallback (m_cToneV6_4, &CBitLoad::ToneV6Callback, eBitAllocV6);

    m_cToneV6_2.AddToneIndex (m_cTone);
    m_cToneV6_2.AddSep ();
    m_cToneV6_2.AddConst (":");
    for (int i = 0; i < 2; i++)
    {
        m_cToneV6_2.AddSep (true);
        m_cToneV6_2.AddTone (m_cTone);
    }
    m_cToneV6_2.Init ();
    AddCallback (m_cToneV6_2, &CBitLoad::ToneV6Callback, eBitAllocV6);

    m_cStartBitAllocV5.AddConst ("bitalloc start");
    m_cStartBitAllocV5.Init ();
    AddCallback (m_cStartBitAllocV5, &CBitLoad::StartBitAllocV5Callback);

    m_cEndBitAllocV5.AddConst ("bitalloc end");
    m_cEndBitAllocV5.Init ();
    AddCallback (m_cEndBitAllocV5, &CBitLoad::EndBitAllocV5Callback);

    m_cStartBitAllocV6.AddConst ("Tone");
    m_cStartBitAllocV6.AddSep ();
    m_cStartBitAllocV6.AddConst (":");
    m_cStartBitAllocV6.AddSep ();
    m_cStartBitAllocV6.AddConst ("# Bits per tone");
    m_cStartBitAllocV6.Init ();
    AddCallback (m_cStartBitAllocV6, &CBitLoad::StartBitAllocV6Callback);

    m_cStartSNR.AddConst ("snr start");
    m_cStartSNR.Init ();
    AddCallback (m_cStartSNR, &CBitLoad::StartSNRCallback);

    m_cEndSNR.AddConst ("snr end");
    m_cEndSNR.Init ();
    AddCallback (m_cEndSNR, &CBitLoad::EndSNRCallback);

    m_cStartGainQ2.AddConst ("gainQ2 start");
    m_cStartGainQ2.Init ();
    AddCallback (m_cStartGainQ2, &CBitLoad::StartGainQ2Callback);

    m_cEndGainQ2.AddConst ("gainQ2 end");
    m_cEndGainQ2.Init ();
    AddCallback (m_cEndGainQ2, &CBitLoad::EndGainQ2Callback);

    m_cStartNoiseMargin.AddConst ("noisemargin start");
    m_cStartNoiseMargin.Init ();
    AddCallback (m_cStartNoiseMargin, &CBitLoad::StartNoiseMarginCallback);

    m_cEndNoiseMargin.AddConst ("noisemargin end");
    m_cEndNoiseMargin.Init ();
    AddCallback (m_cEndNoiseMargin, &CBitLoad::EndNoiseMarginCallback);

    m_cStartChanCharLog.AddConst ("chanCharLog start");
    m_cStartChanCharLog.Init ();
    AddCallback (m_cStartChanCharLog, &CBitLoad::StartChanCharLogCallback);

    m_cEndChanCharLog.AddConst ("chanCharLog end");
    m_cEndChanCharLog.Init ();
    AddCallback (m_cEndChanCharLog, &CBitLoad::EndChanCharLogCallback);

    AddCallback (m_cPrompt, &CBitLoad::EndBitAllocV6Callback, eBitAllocV6);
    AddCallback (m_cPrompt, &CBitLoad::PromptCallback);

//begin user code
//end user code
}

CParser *CBitLoad::ToneV5Callback (void)
{
    m_bToneValid = true;

    return this;
}

CParser *CBitLoad::ToneV6Callback (void)
{
    m_bToneValid = true;

    return this;
}

CParser *CBitLoad::StartBitAllocV5Callback (void)
{
    m_bToneValid = false;
    m_cTone.Init ();
    m_nState = eBitAllocV5;
    LogTrace ("State BitAllocV5\n");

    return this;
}

CParser *CBitLoad::EndBitAllocV5Callback (void)
{
    if (m_bToneValid)
    {
        CBandplan *pBand = m_pModem->Data ().Bandplan ();

        for (size_t nBand = 0; nBand < pBand->Bands (); nBand++)
        {
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocUp.Set (m_cTone,
                    (size_t)pBand->Band (nBand).MinUp (),
                    (size_t)pBand->Band (nBand).MaxUp ());
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocDown.Set (m_cTone,
                    (size_t)pBand->Band (nBand).MinDown (),
                    (size_t)pBand->Band (nBand).MaxDown ());
        }
    }
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitLoad::StartBitAllocV6Callback (void)
{
    m_bToneValid = false;
    m_cTone.Init ();
    m_nState = eBitAllocV6;
    LogTrace ("State BitAllocV6\n");

    return this;
}

CParser *CBitLoad::EndBitAllocV6Callback (void)
{
    if (m_bToneValid)
    {
        CBandplan *pBand = m_pModem->Data ().Bandplan ();

        for (size_t nBand = 0; nBand < pBand->Bands (); nBand++)
        {
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocUp.Set (m_cTone,
                    (size_t)pBand->Band (nBand).MinUp (),
                    (size_t)pBand->Band (nBand).MaxUp ());
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocDown.Set (m_cTone,
                    (size_t)pBand->Band (nBand).MinDown (),
                    (size_t)pBand->Band (nBand).MaxDown ());
        }
    }
    LogTrace ("End BitAllocV6\n");

    return new CExit (m_pModem, s_strCmdExit);
}

CParser *CBitLoad::StartSNRCallback (void)
{
    m_bToneValid = false;
    m_cTone.Init ();
    m_nState = eSNR;
    LogTrace ("State SNR\n");

    return this;
}

CParser *CBitLoad::EndSNRCallback (void)
{
    if (m_bToneValid)
    {
        m_pModem->Data ().m_cData.m_cTones.m_cSNR.Set (m_cTone);
    }
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitLoad::StartGainQ2Callback (void)
{
    m_bToneValid = false;
    m_cTone.Init ();
    m_nState = eGainQ2;
    LogTrace ("State GainQ2\n");

    return this;
}

CParser *CBitLoad::EndGainQ2Callback (void)
{
    if (m_bToneValid)
    {
        m_pModem->Data ().m_cData.m_cTones.m_cGainQ2.Set (m_cTone);
    }
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitLoad::StartNoiseMarginCallback (void)
{
    m_bToneValid = false;
    m_cTone.Init ();
    m_nState = eNoiseMargin;
    LogTrace ("State NoiseMargin\n");

    return this;
}

CParser *CBitLoad::EndNoiseMarginCallback (void)
{
    if (m_bToneValid)
    {
        m_pModem->Data ().m_cData.m_cTones.m_cNoiseMargin.Set (m_cTone);
    }
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitLoad::StartChanCharLogCallback (void)
{
    m_bToneValid = false;
    m_cTone.Init ();
    m_nState = eChanCharLog;
    LogTrace ("State ChanCharLog\n");

    return this;
}

CParser *CBitLoad::EndChanCharLogCallback (void)
{
    if (m_bToneValid)
    {
        m_cTone.Scale (0, 1, 1);
        m_pModem->Data ().m_cData.m_cTones.m_cChanCharLog.Set (m_cTone);
    }
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitLoad::PromptCallback (void)
{
    return new CExit (m_pModem, s_strCmdExit);
}

CAdslDown::CAdslDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CAdslDown::PromptCallback);

//begin user code
//end user code
}

CParser *CAdslDown::PromptCallback (void)
{
    return new CAdslUp (m_pModem, s_strCmdAdslUp);
}

CAdslUp::CAdslUp (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CAdslUp::PromptCallback);

//begin user code
//end user code
}

CParser *CAdslUp::PromptCallback (void)
{
    return new CExit (m_pModem, s_strCmdExit);
}

CSysReboot::CSysReboot (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CSysReboot::PromptCallback);

//begin user code
//end user code
}

CParser *CSysReboot::PromptCallback (void)
{
    return NULL;
}

CExit::CExit (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CExit::PromptCallback);

//begin user code
//end user code
}

CParser *CExit::PromptCallback (void)
{
    return NULL;
}

//! @endcond
//_oOo_
