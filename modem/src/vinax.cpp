/*!
 * @file        vinax.cpp
 * @brief       vinax modem implementation
 * @details     parser for vinax based modems, tested with:
 * @n           Sphairon Speedlink 1113
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from vinax.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-vinax-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_VINAX_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
#include <math.h>
//end user code

#include "vinax.h"
#include "log.h"

//! @cond
using namespace NVinax;

static const char *s_strCmdInfo = "dsl-info";
static const char *s_strCmdBitsUp = "dsl_pipe g997bansg 0";
static const char *s_strCmdBitsDown = "dsl_pipe g997bansg 1";
static const char *s_strCmdSNRDown = "dsl_pipe g997sansg 1";
static const char *s_strCmdResync = "dsl_cpe_control -i 00_00_00_00_00_00_00_07 -f /firmware/vcpe_hw.bin -F /firmware/acpe_hw.bin -A /firmware/vdsl-annex-b.scr";
static const char *s_strCmdReboot = "reboot";
static const char *s_strCmdExit = "exit";

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CVinax (cData, pProtocol);
}

CVinax::CVinax (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
//end user code
{
//begin user code
//end user code
}

void CVinax::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("# ");
    m_strLF.Format ("\r\n");

//begin user code
//end user code

    if (m_pLogin)
    {
        pFirst = new CLoginUser (this, User ());
    }
    else if (CModem::eInfo == eCommand)
    {
        pFirst = new CInfo (this, s_strCmdInfo);
    }
    else if (CModem::eStatus == eCommand)
    {
        pFirst = new CInfo (this, s_strCmdInfo);
    }
    else if (CModem::eResync == eCommand)
    {
        pFirst = new CResync (this, s_strCmdResync);
    }
    else if (CModem::eReboot == eCommand)
    {
        pFirst = new CReboot (this, s_strCmdReboot);
    }

    Protocol ()->Next (pFirst);
}

//begin user code
//end user code

CLoginUser::CLoginUser (CModem *pModem, const char *strUser)
: CParser (pModem)
, m_strUser (strUser)
{
}

CParser *CLoginUser::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "sphaironbox login: "))
    {
        m_pModem->Protocol ()->Send (m_strUser, "user");
        m_pModem->Protocol ()->Send (m_pModem->LF ());

        pNext = new CLoginPass (m_pModem, m_pModem->Pass ());
    }

    return pNext;
}

CLoginPass::CLoginPass (CModem *pModem, const char *strPass)
: CParser (pModem)
, m_strPass (strPass)
{
}

CParser *CLoginPass::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Password: "))
    {
        m_pModem->Protocol ()->Send (m_strPass, "pass");
        m_pModem->Protocol ()->Send (m_pModem->LF ());
    }
    else if (Find (strRecv, nRecv, m_pModem->Prompt ()))
    {
        if (CModem::eInfo == m_pModem->Command ())
        {
            pNext = new CInfo (m_pModem, s_strCmdInfo);
        }
        else if (CModem::eStatus == m_pModem->Command ())
        {
            pNext = new CInfo (m_pModem, s_strCmdInfo);
        }
        else if (CModem::eResync == m_pModem->Command ())
        {
            pNext = new CResync (m_pModem, s_strCmdResync);
        }
        else if (CModem::eReboot == m_pModem->Command ())
        {
            pNext = new CReboot (m_pModem, s_strCmdReboot);
        }
    }

    return pNext;
}

template <class T> CPromptParser<T>::CPromptParser (CModem *pModem, const char *strCmd)
: CRegExCmdParser<T, CLineParser> (pModem, strCmd)
, m_cPrompt ()
{
    m_cPrompt.AddConst ("# ");
    m_cPrompt.Init ();
}

CInfo::CInfo (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_fTemp0 (0)
, m_fTemp1 (0)
, m_strTemp0 ()
, m_cModemState ()
, m_cBandwidth ()
, m_cMaxBandwidth ()
, m_cAttenuation ()
, m_cNoiseMargin ()
, m_cTxPower ()
{
    m_cModemState.AddConst ("Modem status");
    m_cModemState.AddSep ();
    m_cModemState.AddConst (":");
    m_cModemState.AddSep ();
    m_cModemState.AddString (m_strTemp0);
    m_cModemState.Init ();
    AddCallback (m_cModemState, &CInfo::ModemStateCallback);

    m_cBandwidth.AddConst ("Current Rate [(]kbps[)]");
    m_cBandwidth.AddSep ();
    m_cBandwidth.AddConst (":");
    m_cBandwidth.AddSep ();
    m_cBandwidth.AddDouble (m_fTemp0);
    m_cBandwidth.AddSep ();
    m_cBandwidth.AddDouble (m_fTemp1);
    m_cBandwidth.Init ();
    AddCallback (m_cBandwidth, &CInfo::BandwidthCallback);

    m_cMaxBandwidth.AddConst ("Attainable Rate [(]kbps[)]");
    m_cMaxBandwidth.AddSep ();
    m_cMaxBandwidth.AddConst (":");
    m_cMaxBandwidth.AddSep ();
    m_cMaxBandwidth.AddDouble (m_fTemp0);
    m_cMaxBandwidth.AddSep ();
    m_cMaxBandwidth.AddDouble (m_fTemp1);
    m_cMaxBandwidth.Init ();
    AddCallback (m_cMaxBandwidth, &CInfo::MaxBandwidthCallback);

    m_cAttenuation.AddConst ("LATN[[]");
    m_cAttenuation.AddInt (m_nTemp0);
    m_cAttenuation.AddConst ("[]] [(]dB[)]");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst (":");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp0);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp1);
    m_cAttenuation.Init ();
    AddCallback (m_cAttenuation, &CInfo::AttenuationCallback);

    m_cNoiseMargin.AddConst ("SNR Margin[[]");
    m_cNoiseMargin.AddInt (m_nTemp0);
    m_cNoiseMargin.AddConst ("[]] [(]dB[)]");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddConst (":");
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddDouble (m_fTemp0);
    m_cNoiseMargin.AddSep ();
    m_cNoiseMargin.AddDouble (m_fTemp1);
    m_cNoiseMargin.Init ();
    AddCallback (m_cNoiseMargin, &CInfo::NoiseMarginCallback);

    m_cTxPower.AddConst ("ACATP [(]dB[)]");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst (":");
    m_cTxPower.AddSep ();
    m_cTxPower.AddDouble (m_fTemp0);
    m_cTxPower.AddSep ();
    m_cTxPower.AddDouble (m_fTemp1);
    m_cTxPower.Init ();
    AddCallback (m_cTxPower, &CInfo::TxPowerCallback);

    AddCallback (m_cPrompt, &CInfo::PromptCallback);

//begin user code
//end user code
}

CParser *CInfo::ModemStateCallback (void)
{
//begin user code
    CString strOperationMode;
    int nModemState = 0;
    if (0 == strncasecmp (m_strTemp0, "Showtime", 8))
    {
        nModemState = 1;
    }
    m_pModem->Data ().m_cData.m_cModemState.Set (nModemState);
    m_pModem->Data ().Bandplan (CBandplan::eVDSL2_B8_12_17a);
    m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
    m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);
//end user code
    m_pModem->Data ().m_cData.m_cModemStateStr.Set (m_strTemp0);
    LogTrace ("ModemState: %s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CInfo::BandwidthCallback (void)
{
//begin user code
    m_fTemp0 = floor (m_fTemp0 + 0.5);
    m_fTemp1 = floor (m_fTemp1 + 0.5);
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.Set ((long)m_fTemp1, (long)m_fTemp0);
    LogTrace ("Bandwidth Up: %.0lf Kbit/s Down: %.0lf Kbit/s \n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CInfo::MaxBandwidthCallback (void)
{
//begin user code
    m_fTemp0 = floor (m_fTemp0 + 0.5);
    m_fTemp1 = floor (m_fTemp1 + 0.5);
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cMaxKbits.Set ((long)m_fTemp1, (long)m_fTemp0);
    LogTrace ("MaxBandwidth Up: %.0lf Kbit/s Down: %.0lf Kbit/s \n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CInfo::AttenuationCallback (void)
{
//begin user code
    size_t nBand = (size_t)m_nTemp0;
//end user code
    m_pModem->Data ().m_cData.m_cAttenuation.Set (nBand, m_fTemp1, m_fTemp0);
    LogTrace ("Attenuation [%ld] Up: %lf dB Down: %lf dB\n", m_nTemp0, m_fTemp1, m_fTemp0);

    return this;
}

CParser *CInfo::NoiseMarginCallback (void)
{
//begin user code
    size_t nBand = (size_t)m_nTemp0;
//end user code
    m_pModem->Data ().m_cData.m_cNoiseMargin.Set (nBand, m_fTemp1, m_fTemp0);
    LogTrace ("NoiseMargin [%ld] Up: %lf dB Down: %lf dB\n", m_nTemp0, m_fTemp1, m_fTemp0);

    return this;
}

CParser *CInfo::TxPowerCallback (void)
{
    m_pModem->Data ().m_cData.m_cTxPower.Set (m_fTemp1, m_fTemp0);
    LogTrace ("TxPower Up: %lf dB Down: %lf dB\n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CInfo::PromptCallback (void)
{
    CParser *pNext = this;

    if (CModem::eInfo == m_pModem->Command ())
    {
        pNext = new CBitsUp (m_pModem, s_strCmdBitsUp);
    }
    else
    {
        pNext = new CExit (m_pModem, s_strCmdExit);
    }

    return pNext;
}

CBitsUp::CBitsUp (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_strTemp0 ()
, m_nTones (0)
, m_cNum ()
, m_cFormat ()
, m_cBits ()
{
    m_cNum.AddConst ("nReturn=");
    m_cNum.AddInt (m_nTemp0);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nDirection=");
    m_cNum.AddInt (m_nTemp1);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nNumData=");
    m_cNum.AddInt (m_nTones);
    m_cNum.Init ();
    AddCallback (m_cNum, &CBitsUp::NumCallback);

    m_cFormat.AddConst ("nFormat=bits[(]hex[)] nData=[\"]");
    m_cFormat.Init ();
    AddCallback (m_cFormat, &CBitsUp::FormatCallback);

    m_cBits.AddString (m_strTemp0, "(([0-9a-f]{2} )*)");
    m_cBits.Init ();
    AddCallback (m_cBits, &CBitsUp::BitsCallback, eFormat);

    AddCallback (m_cPrompt, &CBitsUp::PromptCallback);

//begin user code
//end user code
}

CParser *CBitsUp::NumCallback (void)
{
    LogTrace ("Tones: %ld\n", m_nTones);

    return this;
}

CParser *CBitsUp::FormatCallback (void)
{
    m_nState = eFormat;
    LogTrace ("State Format\n");

    return this;
}

CParser *CBitsUp::BitsCallback (void)
{
//begin user code
    for (size_t nTone = 0; nTone < (size_t)m_nTones; nTone++)
    {
        if ( (nTone * 3) < (size_t)strlen (m_strTemp0))
        {
            CString strTemp (&m_strTemp0[(long)(nTone * 3)], 2);
            int nBits = (int)strTemp.Int (16);
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocUp.Set (nTone, nBits);
            LogExtra ("%4d:%02x\n", nTone, nBits);
        }
        else
        {
            break;
        }
    }
    LogTrace ("Bits\n");
//end user code
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitsUp::PromptCallback (void)
{
    return new CBitsDown (m_pModem, s_strCmdBitsDown);
}

CBitsDown::CBitsDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_strTemp0 ()
, m_nTones (0)
, m_cNum ()
, m_cFormat ()
, m_cBits ()
{
    m_cNum.AddConst ("nReturn=");
    m_cNum.AddInt (m_nTemp0);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nDirection=");
    m_cNum.AddInt (m_nTemp1);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nNumData=");
    m_cNum.AddInt (m_nTones);
    m_cNum.Init ();
    AddCallback (m_cNum, &CBitsDown::NumCallback);

    m_cFormat.AddConst ("nFormat=bits[(]hex[)] nData=[\"]");
    m_cFormat.Init ();
    AddCallback (m_cFormat, &CBitsDown::FormatCallback);

    m_cBits.AddString (m_strTemp0, "(([0-9a-f]{2} )*)");
    m_cBits.Init ();
    AddCallback (m_cBits, &CBitsDown::BitsCallback, eFormat);

    AddCallback (m_cPrompt, &CBitsDown::PromptCallback);

//begin user code
//end user code
}

CParser *CBitsDown::NumCallback (void)
{
    LogTrace ("Tones: %ld\n", m_nTones);

    return this;
}

CParser *CBitsDown::FormatCallback (void)
{
    m_nState = eFormat;
    LogTrace ("State Format\n");

    return this;
}

CParser *CBitsDown::BitsCallback (void)
{
//begin user code
    for (size_t nTone = 0; nTone < (size_t)m_nTones; nTone++)
    {
        if ( (nTone * 3) < (size_t)strlen (m_strTemp0))
        {
            CString strTemp (&m_strTemp0[(long)(nTone * 3)], 2);
            int nBits = (int)strTemp.Int (16);
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocDown.Set (nTone, nBits);
            LogExtra ("%4d:%02x\n", nTone, nBits);
        }
        else
        {
            break;
        }
    }
    LogTrace ("Bits\n");
//end user code
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitsDown::PromptCallback (void)
{
    return new CSNRDown (m_pModem, s_strCmdSNRDown);
}

CSNRDown::CSNRDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_strTemp0 ()
, m_nTones (0)
, m_cNum ()
, m_cFormat ()
{
    m_cNum.AddConst ("nReturn=");
    m_cNum.AddInt (m_nTemp0);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nDirection=");
    m_cNum.AddInt (m_nTemp1);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nNumData=");
    m_cNum.AddInt (m_nTones);
    m_cNum.Init ();
    AddCallback (m_cNum, &CSNRDown::NumCallback);

    m_cFormat.AddConst ("nFormat=snr[(]hex[)] nData=[\"]");
    m_cFormat.AddString (m_strTemp0, "(([0-9a-f]{2} )*)");
    m_cFormat.Init ();
    AddCallback (m_cFormat, &CSNRDown::FormatCallback);

    AddCallback (m_cPrompt, &CSNRDown::PromptCallback);

//begin user code
//end user code
}

CParser *CSNRDown::NumCallback (void)
{
    LogTrace ("Tones: %ld\n", m_nTones);

    return this;
}

CParser *CSNRDown::FormatCallback (void)
{
//begin user code
    for (size_t nTone = 0; nTone < (size_t)m_nTones; nTone++)
    {
        if ((nTone * 3) < (size_t)strlen (m_strTemp0))
        {
            CString strTemp (&m_strTemp0[(long)(nTone * 3)], 2);
            int nSNR = (int)strTemp.Int (16);
            if (nSNR >= 255)
            {
                nSNR = 0;
            }
            else
            {
                nSNR = (nSNR / 2) - 32;
            }
            m_pModem->Data ().m_cData.m_cTones.m_cSNR.Set (nTone, nSNR);
            LogTrace ("%4d:%02x\n", nTone, nSNR);
        }
        else
        {
            break;
        }
    }
    LogTrace ("SNR\n");
//end user code

    return this;
}

CParser *CSNRDown::PromptCallback (void)
{
    return new CExit (m_pModem, s_strCmdExit);
}

CResync::CResync (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CResync::PromptCallback);

//begin user code
//end user code
}

CParser *CResync::PromptCallback (void)
{
    return new CExit (m_pModem, s_strCmdExit);
}

CReboot::CReboot (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CReboot::PromptCallback);

//begin user code
//end user code
}

CParser *CReboot::PromptCallback (void)
{
    return NULL;
}

CExit::CExit (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CExit::PromptCallback);

//begin user code
//end user code
}

CParser *CExit::PromptCallback (void)
{
    return NULL;
}

//! @endcond
//_oOo_
