/*!
 * @file        amazon.cpp
 * @brief       amazon modem implementation
 * @details     parser for Infineon AMAZON SE based modems, tested with:
 * @n           Allnet ALL 0333 CJ
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        22.11.2015
 *
 * @note        This file is partly generated from ar7.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-amazon-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_AMAZON_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
//end user code

#include "amazon.h"
#include "log.h"

//! @cond
using namespace NAmazon;

static const char *s_strCmdStatus = "dsl_cpe_pipe.sh lsg";
static const char *s_strCmdOpMode = "dsl_cpe_pipe.sh g997xtusesg";
static const char *s_strCmdRateUp = "dsl_cpe_pipe.sh g997csg 0 0";
static const char *s_strCmdRateDown = "dsl_cpe_pipe.sh g997csg 0 1";
static const char *s_strCmdLineUp = "dsl_cpe_pipe.sh g997lsg 0 1";
static const char *s_strCmdLineDown = "dsl_cpe_pipe.sh g997lsg 1 1";
static const char *s_strCmdFECNear = "dsl_cpe_pipe.sh pmcctg 0 0";
static const char *s_strCmdFECFar = "dsl_cpe_pipe.sh pmcctg 0 1";
static const char *s_strCmdHECNear = "dsl_cpe_pipe.sh pmdpctg 0 0";
static const char *s_strCmdHECFar = "dsl_cpe_pipe.sh pmdpctg 0 1";
static const char *s_strCmdErrorSec15minNear = "dsl_cpe_pipe.sh pmlsc15mg 0 0";
static const char *s_strCmdErrorSecDayNear = "dsl_cpe_pipe.sh pmlsc1dg 0 0";
static const char *s_strCmdInfoNear = "dsl_cpe_pipe.sh g997lig 0";
static const char *s_strCmdInfoFar = "dsl_cpe_pipe.sh g997lig 1";
static const char *s_strCmdBitsUp = "dsl_cpe_pipe.sh g997bansg 0";
static const char *s_strCmdBitsDown = "dsl_cpe_pipe.sh g997bansg 1";
static const char *s_strCmdSNRDown = "dsl_cpe_pipe.sh g997sansg 1";
static const char *s_strCmdReboot = "reboot";
static const char *s_strCmdExit = "exit";

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CAmazon (cData, pProtocol);
}

CAmazon::CAmazon (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
//end user code
{
//begin user code
//end user code
}

void CAmazon::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("# ");
    m_strLF.Format ("\r\n");

//begin user code
//end user code

    if (m_pLogin)
    {
        pFirst = new CLoginUser (this, User ());
    }
    else if (CModem::eInfo == eCommand)
    {
        pFirst = new CStatus (this, s_strCmdStatus);
    }
    else if (CModem::eStatus == eCommand)
    {
        pFirst = new CStatus (this, s_strCmdStatus);
    }
    else if (CModem::eResync == eCommand)
    {
        pFirst = new CExit (this, s_strCmdExit);
    }
    else if (CModem::eReboot == eCommand)
    {
        pFirst = new CReboot (this, s_strCmdReboot);
    }

    Protocol ()->Next (pFirst);
}

const CAmazon::CBandplanData CAmazon::CBandplanData::g_acList[] = {
//begin user code
        // see ITU-T G.997.1 7.3.1.1.1
        // Oktet 1
        CBandplanData (0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::eADSL_A),
        // CBandplanData (0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.1 Annex A overlapped
        CBandplanData (0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::eADSL_B),
        // CBandplanData (0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.1 Annex B overlapped
        // CBandplanData (0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.1 Annex C
        // CBandplanData (0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.1 Annex C overlapped

        // Oktet 2
        // CBandplanData (0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.2 Annex A
        // CBandplanData (0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.2 Annex B overlapped
        // CBandplanData (0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.2 Annex C
        // CBandplanData (0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.2 Annex C overlapped

        // Oktet 3
         CBandplanData (0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::eADSL2_A),
        // CBandplanData (0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.3 Annex A overlapped
         CBandplanData (0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::eADSL2_B),
        // CBandplanData (0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.3 Annex B overlapped
        // CBandplanData (0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.3 Annex C
        // CBandplanData (0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, CBandplan::), G.992.3 Annex C overlapped

        // Oktet 6
         CBandplanData (0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, CBandplan::eADSL2p_A),
        // CBandplanData (0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, CBandplan::), G.992.5 Annex A overlapped
         CBandplanData (0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, CBandplan::eADSL2p_B),
        // CBandplanData (0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, CBandplan::), G.992.5 Annex B overlapped

        // Oktet 7
         CBandplanData (0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, CBandplan::eADSL2p_J),
        // CBandplanData (0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, CBandplan::), G.992.5 Annex J overlapped
         CBandplanData (0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, CBandplan::eADSL2p_M),
        // CBandplanData (0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, CBandplan::), G.992.5 Annex M overlapped
//end user code
};

CAmazon::CBandplanData::CBandplanData (int nXTS1,
        int nXTS2,
        int nXTS3,
        int nXTS4,
        int nXTS5,
        int nXTS6,
        int nXTS7,
        int nXTS8, CBandplan::EBandplan eBandplan)
: m_nXTS1 (nXTS1)
, m_nXTS2 (nXTS2)
, m_nXTS3 (nXTS3)
, m_nXTS4 (nXTS4)
, m_nXTS5 (nXTS5)
, m_nXTS6 (nXTS6)
, m_nXTS7 (nXTS7)
, m_nXTS8 (nXTS8)
, m_eBandplan (eBandplan)
{
}

CAmazon::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
: m_nXTS1 (cBandplanData.m_nXTS1)
, m_nXTS2 (cBandplanData.m_nXTS2)
, m_nXTS3 (cBandplanData.m_nXTS3)
, m_nXTS4 (cBandplanData.m_nXTS4)
, m_nXTS5 (cBandplanData.m_nXTS5)
, m_nXTS6 (cBandplanData.m_nXTS6)
, m_nXTS7 (cBandplanData.m_nXTS7)
, m_nXTS8 (cBandplanData.m_nXTS8)
, m_eBandplan (cBandplanData.m_eBandplan)
{
}

CBandplan::EBandplan CAmazon::CBandplanData::Get (int nXTS1,
        int nXTS2,
        int nXTS3,
        int nXTS4,
        int nXTS5,
        int nXTS6,
        int nXTS7,
        int nXTS8)
{
    CBandplan::EBandplan eBandplan = CBandplan::eNone;

    for (size_t nIndex = 0; nIndex < ARRAY_SIZE (g_acList); nIndex++)
    {
//begin user code
        if ((g_acList[nIndex].m_nXTS1 == nXTS1) &&
            (g_acList[nIndex].m_nXTS2 == nXTS2) &&
            (g_acList[nIndex].m_nXTS3 == nXTS3) &&
            (g_acList[nIndex].m_nXTS4 == nXTS4) &&
            (g_acList[nIndex].m_nXTS5 == nXTS5) &&
            (g_acList[nIndex].m_nXTS6 == nXTS6) &&
            (g_acList[nIndex].m_nXTS7 == nXTS7) &&
            (g_acList[nIndex].m_nXTS8 == nXTS8))
        {
            eBandplan = g_acList[nIndex].m_eBandplan;
            break;
        }
//end user code
    }

    return eBandplan;
}

CBandplan::EBandplan CAmazon::Bandplan (int nXTS1,
        int nXTS2,
        int nXTS3,
        int nXTS4,
        int nXTS5,
        int nXTS6,
        int nXTS7,
        int nXTS8)
{
    return CBandplanData::Get(nXTS1, nXTS2, nXTS3, nXTS4, nXTS5, nXTS6, nXTS7, nXTS8);
}

//begin user code
const CAmazon::CModemState CAmazon::CModemState::g_acList[] = {

        CModemState (0x00000000, "Not initialized"),
        CModemState (0x00000001, "Exception"),
        CModemState (0x00000010, "Not updated"),
        CModemState (0x000000ff, "Idle request"),
        CModemState (0x00000100, "Idle"),
        CModemState (0x000001ff, "Silent request"),
        CModemState (0x00000200, "Silent"),
        CModemState (0x00000300, "Handshake"),
        CModemState (0x00000380, "Full init"),
        CModemState (0x00000400, "Discovery"),
        CModemState (0x00000500, "Training"),
        CModemState (0x00000600, "Analysis"),
        CModemState (0x00000700, "Exchange"),
        CModemState (0x00000800, "Showtime no sync"),
        CModemState (0x00000801, "Showtime tc sync"),
        CModemState (0x00000900, "Fast retrain"),
        CModemState (0x00000a00, "Low power L2"),
        CModemState (0x00000b00, "Loop diagnostic active"),
        CModemState (0x00000b10, "Loop diagnostic data exchange"),
        CModemState (0x00000b20, "Loop diagnostic data request"),
        CModemState (0x00000c00, "Loop diagnostic complete"),
        CModemState (0x00000d00, "resync")
};

CAmazon::CModemState::CModemState (long nState, const char *strState)
: m_nState (nState)
, m_strState (strState)
{
}

CAmazon::CModemState::CModemState (const CModemState &cModemState)
: m_nState (cModemState.m_nState)
, m_strState (cModemState.m_strState)
{
}

const char* CAmazon::CModemState::Get (long nState)
{
    const char *strModemState = "?";

    for (size_t nIndex = 0; nIndex < ARRAY_SIZE (g_acList); nIndex++)
    {
        if (g_acList[nIndex].m_nState == nState)
        {
            strModemState = g_acList[nIndex].m_strState;
            break;
        }
    }

    return strModemState;
}
//end user code

CLoginUser::CLoginUser (CModem *pModem, const char *strUser)
: CParser (pModem)
, m_strUser (strUser)
{
}

CParser *CLoginUser::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Amazon_SE login: "))
    {
        m_pModem->Protocol ()->Send (m_strUser, "user");
        m_pModem->Protocol ()->Send (m_pModem->LF ());

        pNext = new CLoginPass (m_pModem, m_pModem->Pass ());
    }

    return pNext;
}

CLoginPass::CLoginPass (CModem *pModem, const char *strPass)
: CParser (pModem)
, m_strPass (strPass)
{
}

CParser *CLoginPass::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Password: "))
    {
        m_pModem->Protocol ()->Send (m_strPass, "pass");
        m_pModem->Protocol ()->Send (m_pModem->LF ());
    }
    else if (Find (strRecv, nRecv, m_pModem->Prompt ()))
    {
        if (CModem::eInfo == m_pModem->Command ())
        {
            pNext = new CStatus (m_pModem, s_strCmdStatus);
        }
        else if (CModem::eStatus == m_pModem->Command ())
        {
            pNext = new CStatus (m_pModem, s_strCmdStatus);
        }
        else if (CModem::eResync == m_pModem->Command ())
        {
            pNext = new CExit (m_pModem, s_strCmdExit);
        }
        else if (CModem::eReboot == m_pModem->Command ())
        {
            pNext = new CReboot (m_pModem, s_strCmdReboot);
        }
    }

    return pNext;
}

template <class T> CPromptParser<T>::CPromptParser (CModem *pModem, const char *strCmd)
: CRegExCmdParser<T, CLineParser> (pModem, strCmd)
, m_cPrompt ()
{
    m_cPrompt.AddConst ("# ");
    m_cPrompt.Init ();
}

CStatus::CStatus (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nLineState (0)
, m_cStatus ()
{
    m_cStatus.AddConst ("nReturn=");
    m_cStatus.AddInt (m_nReturn);
    m_cStatus.AddSep ();
    m_cStatus.AddConst ("nLineState=0x");
    m_cStatus.AddInt (m_nLineState, 16);
    m_cStatus.Init ();
    AddCallback (m_cStatus, &CStatus::StatusCallback);

    AddCallback (m_cPrompt, &CStatus::PromptCallback);

//begin user code
//end user code
}

CParser *CStatus::StatusCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        int nModemState = 0;
        CString strModemState (CAmazon::GetModemState (m_nLineState));
        switch (m_nLineState)
        {
            case 0x00000800:
            case 0x00000801:
                nModemState = 1;
                break;
        }
        m_pModem->Data ().m_cData.m_cModemState.Set (nModemState);
        m_pModem->Data ().m_cData.m_cModemStateStr.Set (strModemState);
        LogTrace ("ModemState: %x\n", m_nLineState);
    }
//end user code

    return this;
}

CParser *CStatus::PromptCallback (void)
{
    return new COpMode (m_pModem, s_strCmdOpMode);
}

COpMode::COpMode (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nXTSE1 (0)
, m_nXTSE2 (0)
, m_nXTSE3 (0)
, m_nXTSE4 (0)
, m_nXTSE5 (0)
, m_nXTSE6 (0)
, m_nXTSE7 (0)
, m_nXTSE8 (0)
, m_cOpMode ()
{
    m_cOpMode.AddConst ("nReturn=");
    m_cOpMode.AddInt (m_nReturn);
    m_cOpMode.AddSep ();
    m_cOpMode.AddConst ("XTSE1=0x");
    m_cOpMode.AddInt (m_nXTSE1, 16);
    m_cOpMode.AddSep ();
    m_cOpMode.AddConst ("XTSE2=0x");
    m_cOpMode.AddInt (m_nXTSE2, 16);
    m_cOpMode.AddSep ();
    m_cOpMode.AddConst ("XTSE3=0x");
    m_cOpMode.AddInt (m_nXTSE3, 16);
    m_cOpMode.AddSep ();
    m_cOpMode.AddConst ("XTSE4=0x");
    m_cOpMode.AddInt (m_nXTSE4, 16);
    m_cOpMode.AddSep ();
    m_cOpMode.AddConst ("XTSE5=0x");
    m_cOpMode.AddInt (m_nXTSE5, 16);
    m_cOpMode.AddSep ();
    m_cOpMode.AddConst ("XTSE6=0x");
    m_cOpMode.AddInt (m_nXTSE6, 16);
    m_cOpMode.AddSep ();
    m_cOpMode.AddConst ("XTSE7=0x");
    m_cOpMode.AddInt (m_nXTSE7, 16);
    m_cOpMode.AddSep ();
    m_cOpMode.AddConst ("XTSE8=0x");
    m_cOpMode.AddInt (m_nXTSE8, 16);
    m_cOpMode.Init ();
    AddCallback (m_cOpMode, &COpMode::OpModeCallback);

    AddCallback (m_cPrompt, &COpMode::PromptCallback);

//begin user code
//end user code
}

CParser *COpMode::OpModeCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        CString strOperationMode;
        m_pModem->Data ().Bandplan (CAmazon::Bandplan(
                (int)m_nXTSE1, (int)m_nXTSE2, (int)m_nXTSE3, (int)m_nXTSE4,
                (int)m_nXTSE5, (int)m_nXTSE6, (int)m_nXTSE7, (int)m_nXTSE8));
        m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
        m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);

        LogTrace ("OpMode: %02x %02x %02x %02x %02x %02x %02x %02x\n",
                m_nXTSE1, m_nXTSE2, m_nXTSE3, m_nXTSE4, m_nXTSE5, m_nXTSE6,
                m_nXTSE7, m_nXTSE8);
    }
//end user code

    return this;
}

CParser *COpMode::PromptCallback (void)
{
    return new CRateUp (m_pModem, s_strCmdRateUp);
}

CRateUp::CRateUp (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nChannel (0)
, m_nDirection (0)
, m_nActualDataRate (0)
, m_nPreviousDataRate (0)
, m_nActualInterleaveDelay (0)
, m_ActualImpulseNoiseProtection (0)
, m_cRate ()
{
    m_cRate.AddConst ("nReturn=");
    m_cRate.AddInt (m_nReturn);
    m_cRate.AddSep ();
    m_cRate.AddConst ("nChannel=");
    m_cRate.AddInt (m_nChannel);
    m_cRate.AddSep ();
    m_cRate.AddConst ("nDirection=");
    m_cRate.AddInt (m_nDirection);
    m_cRate.AddSep ();
    m_cRate.AddConst ("ActualDataRate=");
    m_cRate.AddInt (m_nActualDataRate);
    m_cRate.AddSep ();
    m_cRate.AddConst ("PreviousDataRate=");
    m_cRate.AddInt (m_nPreviousDataRate);
    m_cRate.AddSep ();
    m_cRate.AddConst ("ActualInterleaveDelay=");
    m_cRate.AddInt (m_nActualInterleaveDelay);
    m_cRate.AddSep ();
    m_cRate.AddConst ("ActualImpulseNoiseProtection=");
    m_cRate.AddInt (m_ActualImpulseNoiseProtection);
    m_cRate.Init ();
    AddCallback (m_cRate, &CRateUp::RateCallback);

    AddCallback (m_cPrompt, &CRateUp::PromptCallback);

//begin user code
//end user code
}

CParser *CRateUp::RateCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        m_nActualDataRate = (m_nActualDataRate + 500) / 1000;
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetUp (m_nActualDataRate);
    LogTrace ("BandwidthKbits Up:\n", m_nActualDataRate);
//begin user code
    }
//end user code

    return this;
}

CParser *CRateUp::PromptCallback (void)
{
    return new CRateDown (m_pModem, s_strCmdRateDown);
}

CRateDown::CRateDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nChannel (0)
, m_nDirection (0)
, m_nActualDataRate (0)
, m_nPreviousDataRate (0)
, m_nActualInterleaveDelay (0)
, m_ActualImpulseNoiseProtection (0)
, m_cRate ()
{
    m_cRate.AddConst ("nReturn=");
    m_cRate.AddInt (m_nReturn);
    m_cRate.AddSep ();
    m_cRate.AddConst ("nChannel=");
    m_cRate.AddInt (m_nChannel);
    m_cRate.AddSep ();
    m_cRate.AddConst ("nDirection=");
    m_cRate.AddInt (m_nDirection);
    m_cRate.AddSep ();
    m_cRate.AddConst ("ActualDataRate=");
    m_cRate.AddInt (m_nActualDataRate);
    m_cRate.AddSep ();
    m_cRate.AddConst ("PreviousDataRate=");
    m_cRate.AddInt (m_nPreviousDataRate);
    m_cRate.AddSep ();
    m_cRate.AddConst ("ActualInterleaveDelay=");
    m_cRate.AddInt (m_nActualInterleaveDelay);
    m_cRate.AddSep ();
    m_cRate.AddConst ("ActualImpulseNoiseProtection=");
    m_cRate.AddInt (m_ActualImpulseNoiseProtection);
    m_cRate.Init ();
    AddCallback (m_cRate, &CRateDown::RateCallback);

    AddCallback (m_cPrompt, &CRateDown::PromptCallback);

//begin user code
//end user code
}

CParser *CRateDown::RateCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        m_nActualDataRate = (m_nActualDataRate + 500) / 1000;
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetDown (m_nActualDataRate);
    LogTrace ("BandwidthKbits Down:\n", m_nActualDataRate);
//begin user code
    }
//end user code

    return this;
}

CParser *CRateDown::PromptCallback (void)
{
    return new CLineUp (m_pModem, s_strCmdLineUp);
}

CLineUp::CLineUp (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nDeltDataType (0)
, m_nLATN (0)
, m_nSATN (0)
, m_nSNR (0)
, m_nATTNDR (0)
, m_nACTPS (0)
, m_nACTATP (0)
, m_cLine ()
{
    m_cLine.AddConst ("nReturn=");
    m_cLine.AddInt (m_nReturn);
    m_cLine.AddSep ();
    m_cLine.AddConst ("nDirection=");
    m_cLine.AddInt (m_nDirection);
    m_cLine.AddSep ();
    m_cLine.AddConst ("nDeltDataType=");
    m_cLine.AddInt (m_nDeltDataType);
    m_cLine.AddSep ();
    m_cLine.AddConst ("LATN=");
    m_cLine.AddInt (m_nLATN);
    m_cLine.AddSep ();
    m_cLine.AddConst ("SATN=");
    m_cLine.AddInt (m_nSATN);
    m_cLine.AddSep ();
    m_cLine.AddConst ("SNR=");
    m_cLine.AddInt (m_nSNR);
    m_cLine.AddSep ();
    m_cLine.AddConst ("ATTNDR=");
    m_cLine.AddInt (m_nATTNDR);
    m_cLine.AddSep ();
    m_cLine.AddConst ("ACTPS=");
    m_cLine.AddInt (m_nACTPS);
    m_cLine.AddSep ();
    m_cLine.AddConst ("ACTATP=");
    m_cLine.AddInt (m_nACTATP);
    m_cLine.Init ();
    AddCallback (m_cLine, &CLineUp::LineCallback);

    AddCallback (m_cPrompt, &CLineUp::PromptCallback);

//begin user code
//end user code
}

CParser *CLineUp::LineCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        double fAttenuation = ((double)m_nSATN) / 10.0;
        double fNoisemargin = ((double)m_nSNR) / 10.0;
        m_nATTNDR = (m_nATTNDR + 500) / 1000;
        double fTxPower = ((double)-m_nACTPS) / 10.0;

        m_pModem->Data ().m_cData.m_cAttenuation.SetUp (0, fAttenuation);
        LogTrace ("Attenuation Up: %lf\n", fAttenuation);
        m_pModem->Data ().m_cData.m_cNoiseMargin.SetUp (0, fNoisemargin);
        LogTrace ("Noisemargin Up: %lf\n", fNoisemargin);
        m_pModem->Data ().m_cData.m_cTxPower.SetUp (fTxPower);
        LogTrace ("fTxPower Up: %lf\n", fTxPower);
        m_pModem->Data ().m_cData.m_cBandwidth.m_cMaxKbits.SetUp (m_nATTNDR);
        LogTrace ("MaxBandwidthKbits Up: %ld\n", m_nATTNDR);
    }
//end user code

    return this;
}

CParser *CLineUp::PromptCallback (void)
{
    return new CLineDown (m_pModem, s_strCmdLineDown);
}

CLineDown::CLineDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nDeltDataType (0)
, m_nLATN (0)
, m_nSATN (0)
, m_nSNR (0)
, m_nATTNDR (0)
, m_nACTPS (0)
, m_nACTATP (0)
, m_cLine ()
{
    m_cLine.AddConst ("nReturn=");
    m_cLine.AddInt (m_nReturn);
    m_cLine.AddSep ();
    m_cLine.AddConst ("nDirection=");
    m_cLine.AddInt (m_nDirection);
    m_cLine.AddSep ();
    m_cLine.AddConst ("nDeltDataType=");
    m_cLine.AddInt (m_nDeltDataType);
    m_cLine.AddSep ();
    m_cLine.AddConst ("LATN=");
    m_cLine.AddInt (m_nLATN);
    m_cLine.AddSep ();
    m_cLine.AddConst ("SATN=");
    m_cLine.AddInt (m_nSATN);
    m_cLine.AddSep ();
    m_cLine.AddConst ("SNR=");
    m_cLine.AddInt (m_nSNR);
    m_cLine.AddSep ();
    m_cLine.AddConst ("ATTNDR=");
    m_cLine.AddInt (m_nATTNDR);
    m_cLine.AddSep ();
    m_cLine.AddConst ("ACTPS=");
    m_cLine.AddInt (m_nACTPS);
    m_cLine.AddSep ();
    m_cLine.AddConst ("ACTATP=");
    m_cLine.AddInt (m_nACTATP);
    m_cLine.Init ();
    AddCallback (m_cLine, &CLineDown::LineCallback);

    AddCallback (m_cPrompt, &CLineDown::PromptCallback);

//begin user code
//end user code
}

CParser *CLineDown::LineCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        double fAttenuation = ((double)m_nSATN) / 10.0;
        double fNoisemargin = ((double)m_nSNR) / 10.0;
        m_nATTNDR = (m_nATTNDR + 500) / 1000;
        double fTxPower = ((double)-m_nACTPS) / 10.0;

        m_pModem->Data ().m_cData.m_cAttenuation.SetDown (0, fAttenuation);
        LogTrace ("Attenuation Down: %lf\n", fAttenuation);
        m_pModem->Data ().m_cData.m_cNoiseMargin.SetDown (0, fNoisemargin);
        LogTrace ("Noisemargin Down: %lf\n", fNoisemargin);
        m_pModem->Data ().m_cData.m_cTxPower.SetDown (fTxPower);
        LogTrace ("fTxPower Down: %lf\n", fTxPower);
        m_pModem->Data ().m_cData.m_cBandwidth.m_cMaxKbits.SetDown (m_nATTNDR);
        LogTrace ("MaxBandwidthKbits Down: %ld\n", m_nATTNDR);
    }
//end user code

    return this;
}

CParser *CLineDown::PromptCallback (void)
{
    return new CFECNear (m_pModem, s_strCmdFECNear);
}

CFECNear::CFECNear (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nChannel (0)
, m_nDirection (0)
, m_nElapsedTime (0)
, m_nCodeViolations (0)
, m_nFEC (0)
, m_cFEC ()
{
    m_cFEC.AddConst ("nReturn=");
    m_cFEC.AddInt (m_nReturn);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nChannel=");
    m_cFEC.AddInt (m_nChannel);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nDirection=");
    m_cFEC.AddInt (m_nDirection);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nElapsedTime=");
    m_cFEC.AddInt (m_nElapsedTime);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nCodeViolations=");
    m_cFEC.AddInt (m_nCodeViolations);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nFEC=");
    m_cFEC.AddInt (m_nFEC);
    m_cFEC.Init ();
    AddCallback (m_cFEC, &CFECNear::FECCallback);

    AddCallback (m_cPrompt, &CFECNear::PromptCallback);

//begin user code
//end user code
}

CParser *CFECNear::FECCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetUp (m_nFEC);
    LogTrace ("ErrorsFEC Up: %ld\n", m_nFEC);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetUp (m_nCodeViolations);
    LogTrace ("ErrorsCRC Up: %ld\n", m_nCodeViolations);
//begin user code
    }
//end user code

    return this;
}

CParser *CFECNear::PromptCallback (void)
{
    return new CFECFar (m_pModem, s_strCmdFECFar);
}

CFECFar::CFECFar (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nChannel (0)
, m_nDirection (0)
, m_nElapsedTime (0)
, m_nCodeViolations (0)
, m_nFEC (0)
, m_cFEC ()
{
    m_cFEC.AddConst ("nReturn=");
    m_cFEC.AddInt (m_nReturn);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nChannel=");
    m_cFEC.AddInt (m_nChannel);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nDirection=");
    m_cFEC.AddInt (m_nDirection);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nElapsedTime=");
    m_cFEC.AddInt (m_nElapsedTime);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nCodeViolations=");
    m_cFEC.AddInt (m_nCodeViolations);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("nFEC=");
    m_cFEC.AddInt (m_nFEC);
    m_cFEC.Init ();
    AddCallback (m_cFEC, &CFECFar::FECCallback);

    AddCallback (m_cPrompt, &CFECFar::PromptCallback);

//begin user code
//end user code
}

CParser *CFECFar::FECCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetDown (m_nFEC);
    LogTrace ("ErrorsFEC Down: %ld\n", m_nFEC);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (m_nCodeViolations);
    LogTrace ("ErrorsCRC Down: %ld\n", m_nCodeViolations);
//begin user code
    }
//end user code

    return this;
}

CParser *CFECFar::PromptCallback (void)
{
    return new CHECNear (m_pModem, s_strCmdHECNear);
}

CHECNear::CHECNear (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nChannel (0)
, m_nDirection (0)
, m_nElapsedTime (0)
, m_nHEC (0)
, m_nTotalCells (0)
, m_nUserTotalCells (0)
, m_nIBE (0)
, m_cHEC ()
{
    m_cHEC.AddConst ("nReturn=");
    m_cHEC.AddInt (m_nReturn);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nChannel=");
    m_cHEC.AddInt (m_nChannel);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nDirection=");
    m_cHEC.AddInt (m_nDirection);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nElapsedTime=");
    m_cHEC.AddInt (m_nElapsedTime);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nHEC=");
    m_cHEC.AddInt (m_nHEC);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nTotalCells=");
    m_cHEC.AddInt (m_nTotalCells);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nUserTotalCells=");
    m_cHEC.AddInt (m_nUserTotalCells);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nIBE=");
    m_cHEC.AddInt (m_nIBE);
    m_cHEC.Init ();
    AddCallback (m_cHEC, &CHECNear::HECCallback);

    AddCallback (m_cPrompt, &CHECNear::PromptCallback);

//begin user code
//end user code
}

CParser *CHECNear::HECCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetUp (m_nHEC);
    LogTrace ("ErrorsHEC Up: %ld\n", m_nHEC);
//begin user code
    }
//end user code

    return this;
}

CParser *CHECNear::PromptCallback (void)
{
    return new CHECFar (m_pModem, s_strCmdHECFar);
}

CHECFar::CHECFar (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nChannel (0)
, m_nDirection (0)
, m_nElapsedTime (0)
, m_nHEC (0)
, m_nTotalCells (0)
, m_nUserTotalCells (0)
, m_nIBE (0)
, m_cHEC ()
{
    m_cHEC.AddConst ("nReturn=");
    m_cHEC.AddInt (m_nReturn);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nChannel=");
    m_cHEC.AddInt (m_nChannel);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nDirection=");
    m_cHEC.AddInt (m_nDirection);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nElapsedTime=");
    m_cHEC.AddInt (m_nElapsedTime);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nHEC=");
    m_cHEC.AddInt (m_nHEC);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nTotalCells=");
    m_cHEC.AddInt (m_nTotalCells);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nUserTotalCells=");
    m_cHEC.AddInt (m_nUserTotalCells);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("nIBE=");
    m_cHEC.AddInt (m_nIBE);
    m_cHEC.Init ();
    AddCallback (m_cHEC, &CHECFar::HECCallback);

    AddCallback (m_cPrompt, &CHECFar::PromptCallback);

//begin user code
//end user code
}

CParser *CHECFar::HECCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetDown (m_nHEC);
    LogTrace ("ErrorsHEC Down: %ld\n", m_nHEC);
//begin user code
    }
//end user code

    return this;
}

CParser *CHECFar::PromptCallback (void)
{
    return new CErrorSec15minNear (m_pModem, s_strCmdErrorSec15minNear);
}

CErrorSec15minNear::CErrorSec15minNear (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nHistoryInterval (0)
, m_nElapsedTime (0)
, m_bValid (0)
, m_nES (0)
, m_nSES (0)
, m_nLOSS (0)
, m_nUAS (0)
, m_nLOFS (0)
, m_cErrorSec ()
{
    m_cErrorSec.AddConst ("nReturn=");
    m_cErrorSec.AddInt (m_nReturn);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nDirection=");
    m_cErrorSec.AddInt (m_nDirection);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nHistoryInterval=");
    m_cErrorSec.AddInt (m_nHistoryInterval);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nElapsedTime=");
    m_cErrorSec.AddInt (m_nElapsedTime);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("bValid=");
    m_cErrorSec.AddInt (m_bValid);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nES=");
    m_cErrorSec.AddInt (m_nES);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nSES=");
    m_cErrorSec.AddInt (m_nSES);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nLOSS=");
    m_cErrorSec.AddInt (m_nLOSS);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nUAS=");
    m_cErrorSec.AddInt (m_nUAS);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nLOFS=");
    m_cErrorSec.AddInt (m_nLOFS);
    m_cErrorSec.Init ();
    AddCallback (m_cErrorSec, &CErrorSec15minNear::ErrorSecCallback);

    AddCallback (m_cPrompt, &CErrorSec15minNear::PromptCallback);

//begin user code
//end user code
}

CParser *CErrorSec15minNear::ErrorSecCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min.Set (m_nES);
    LogTrace ("FailErrorSecs15min: %ld\n", m_nES);
//begin user code
    }
//end user code

    return this;
}

CParser *CErrorSec15minNear::PromptCallback (void)
{
    return new CErrorSecDayNear (m_pModem, s_strCmdErrorSecDayNear);
}

CErrorSecDayNear::CErrorSecDayNear (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nHistoryInterval (0)
, m_nElapsedTime (0)
, m_bValid (0)
, m_nES (0)
, m_nSES (0)
, m_nLOSS (0)
, m_nUAS (0)
, m_nLOFS (0)
, m_cErrorSec ()
{
    m_cErrorSec.AddConst ("nReturn=");
    m_cErrorSec.AddInt (m_nReturn);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nDirection=");
    m_cErrorSec.AddInt (m_nDirection);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nHistoryInterval=");
    m_cErrorSec.AddInt (m_nHistoryInterval);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nElapsedTime=");
    m_cErrorSec.AddInt (m_nElapsedTime);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("bValid=");
    m_cErrorSec.AddInt (m_bValid);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nES=");
    m_cErrorSec.AddInt (m_nES);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nSES=");
    m_cErrorSec.AddInt (m_nSES);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nLOSS=");
    m_cErrorSec.AddInt (m_nLOSS);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nUAS=");
    m_cErrorSec.AddInt (m_nUAS);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst ("nLOFS=");
    m_cErrorSec.AddInt (m_nLOFS);
    m_cErrorSec.Init ();
    AddCallback (m_cErrorSec, &CErrorSecDayNear::ErrorSecCallback);

    AddCallback (m_cPrompt, &CErrorSecDayNear::PromptCallback);

//begin user code
//end user code
}

CParser *CErrorSecDayNear::ErrorSecCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
//end user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay.Set (m_nES);
    LogTrace ("FailErrorSecsDay: %ld\n", m_nES);
//begin user code
    }
//end user code

    return this;
}

CParser *CErrorSecDayNear::PromptCallback (void)
{
    CParser *pNext = this;

    if (CModem::eInfo == m_pModem->Command ())
    {
        pNext = new CInfoNear (m_pModem, s_strCmdInfoNear);
    }
    else
    {
        pNext = new CExit (m_pModem, s_strCmdExit);
    }

    return pNext;
}

CInfoNear::CInfoNear (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nCountry0 (0)
, m_nCountry1 (0)
, m_nVendorID0 (0)
, m_nVendorID1 (0)
, m_nVendorID2 (0)
, m_nVendorID3 (0)
, m_nVendorSpec0 (0)
, m_nVendorSpec1 (0)
, m_nSelfTestResult (0)
, m_strTemp ()
, m_cInfo ()
{
    m_cInfo.AddConst ("nReturn=");
    m_cInfo.AddInt (m_nReturn);
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("nDirection=");
    m_cInfo.AddInt (m_nDirection);
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("G994VendorID=[(]");
    m_cInfo.AddInt (m_nCountry0, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nCountry1, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorID0, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorID1, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorID2, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorID3, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorSpec0, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorSpec1, 16);
    m_cInfo.AddConst ("[)]");
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("SystemVendorID=[(]");
    m_cInfo.AddString (m_strTemp);
    m_cInfo.AddConst ("[)]");
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("VersionNumber=[(]");
    m_cInfo.AddString (m_strTemp);
    m_cInfo.AddConst ("[)]");
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("SerialNumber=[(]");
    m_cInfo.AddString (m_strTemp);
    m_cInfo.AddConst ("[)]");
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("SelfTestResult=");
    m_cInfo.AddInt (m_nSelfTestResult);
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("XTSECapabilities=[(]");
    m_cInfo.AddString (m_strTemp);
    m_cInfo.AddConst ("[)]");
    m_cInfo.Init ();
    AddCallback (m_cInfo, &CInfoNear::InfoCallback);

    AddCallback (m_cPrompt, &CInfoNear::PromptCallback);

//begin user code
//end user code
}

CParser *CInfoNear::InfoCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        char strVendor[5];
        strVendor[0] = (char)m_nVendorID0;
        strVendor[1] = (char)m_nVendorID1;
        strVendor[2] = (char)m_nVendorID2;
        strVendor[3] = (char)m_nVendorID3;
        strVendor[4] = '\0';
        m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetVendor (strVendor);
        m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetSpec ((int)m_nVendorSpec0, (int)m_nVendorSpec1);
        LogTrace ("ATU-R Vendor: %s\n", strVendor);
        LogTrace ("ATU-R Spec: %ld.%ld\n", m_nVendorSpec0, m_nVendorSpec1);
    }
//end user code

    return this;
}

CParser *CInfoNear::PromptCallback (void)
{
    return new CInfoFar (m_pModem, s_strCmdInfoFar);
}

CInfoFar::CInfoFar (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nCountry0 (0)
, m_nCountry1 (0)
, m_nVendorID0 (0)
, m_nVendorID1 (0)
, m_nVendorID2 (0)
, m_nVendorID3 (0)
, m_nVendorSpec0 (0)
, m_nVendorSpec1 (0)
, m_nSelfTestResult (0)
, m_strTemp ()
, m_cInfo ()
{
    m_cInfo.AddConst ("nReturn=");
    m_cInfo.AddInt (m_nReturn);
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("nDirection=");
    m_cInfo.AddInt (m_nDirection);
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("G994VendorID=[(]");
    m_cInfo.AddInt (m_nCountry0, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nCountry1, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorID0, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorID1, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorID2, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorID3, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorSpec0, 16);
    m_cInfo.AddConst (",");
    m_cInfo.AddInt (m_nVendorSpec1, 16);
    m_cInfo.AddConst ("[)]");
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("SystemVendorID=[(]");
    m_cInfo.AddString (m_strTemp);
    m_cInfo.AddConst ("[)]");
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("VersionNumber=[(]");
    m_cInfo.AddString (m_strTemp);
    m_cInfo.AddConst ("[)]");
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("SerialNumber=[(]");
    m_cInfo.AddString (m_strTemp);
    m_cInfo.AddConst ("[)]");
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("SelfTestResult=");
    m_cInfo.AddInt (m_nSelfTestResult);
    m_cInfo.AddSep ();
    m_cInfo.AddConst ("XTSECapabilities=[(]");
    m_cInfo.AddString (m_strTemp);
    m_cInfo.AddConst ("[)]");
    m_cInfo.Init ();
    AddCallback (m_cInfo, &CInfoFar::InfoCallback);

    AddCallback (m_cPrompt, &CInfoFar::PromptCallback);

//begin user code
//end user code
}

CParser *CInfoFar::InfoCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        char strVendor[5];
        strVendor[0] = (char)m_nVendorID0;
        strVendor[1] = (char)m_nVendorID1;
        strVendor[2] = (char)m_nVendorID2;
        strVendor[3] = (char)m_nVendorID3;
        strVendor[4] = '\0';
        m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetVendor (strVendor);
        m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetSpec ((int)m_nVendorSpec0, (int)m_nVendorSpec1);
        LogTrace ("ATU-C Vendor: %s\n", strVendor);
        LogTrace ("ATU-C Spec: %ld.%ld\n", m_nVendorSpec0, m_nVendorSpec1);
    }
//end user code

    return this;
}

CParser *CInfoFar::PromptCallback (void)
{
    return new CBitsUp (m_pModem, s_strCmdBitsUp);
}

CBitsUp::CBitsUp (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nNumData (0)
, m_strData ()
, m_cTone ()
, m_cNum ()
, m_cFormat ()
, m_cBits ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cNum.AddConst ("nReturn=");
    m_cNum.AddInt (m_nReturn);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nDirection=");
    m_cNum.AddInt (m_nDirection);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nNumData=");
    m_cNum.AddInt (m_nNumData);
    m_cNum.Init ();
    AddCallback (m_cNum, &CBitsUp::NumCallback);

    m_cFormat.AddConst ("nFormat=bits[(]hex[)]");
    m_cFormat.AddSep ();
    m_cFormat.AddConst ("nData=[\"]");
    m_cFormat.Init ();
    AddCallback (m_cFormat, &CBitsUp::FormatCallback);

    m_cBits.AddString (m_strData, "(([0-9a-f]{2} )*)");
    m_cBits.AddConst ("[\"]");
    m_cBits.Init ();
    AddCallback (m_cBits, &CBitsUp::BitsCallback, eTones);

    AddCallback (m_cPrompt, &CBitsUp::PromptCallback);

//begin user code
//end user code
}

CParser *CBitsUp::NumCallback (void)
{
    LogTrace ("NumData: %ld\n", m_nNumData);

    return this;
}

CParser *CBitsUp::FormatCallback (void)
{
    m_nState = eTones;
    LogTrace ("State Tones\n");

    return this;
}

CParser *CBitsUp::BitsCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        for (long nTone = 0; nTone < m_nNumData; nTone++)
        {
            if ( (nTone * 3) < (long)strlen (m_strData))
            {
                CString strTemp (&m_strData[nTone * 3], 2);
                int nBits = (int)strTemp.Int (16);
                m_pModem->Data ().m_cData.m_cTones.m_cBitallocUp.Set ((size_t)nTone, nBits);
                LogExtra ("%4d:%02x\n", nTone, nBits);
            }
            else
            {
                break;
            }
        }
        LogTrace ("BitsUp\n");
    }
//end user code
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitsUp::PromptCallback (void)
{
    return new CBitsDown (m_pModem, s_strCmdBitsDown);
}

CBitsDown::CBitsDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nNumData (0)
, m_strData ()
, m_cTone ()
, m_cNum ()
, m_cFormat ()
, m_cBits ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cNum.AddConst ("nReturn=");
    m_cNum.AddInt (m_nReturn);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nDirection=");
    m_cNum.AddInt (m_nDirection);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nNumData=");
    m_cNum.AddInt (m_nNumData);
    m_cNum.Init ();
    AddCallback (m_cNum, &CBitsDown::NumCallback);

    m_cFormat.AddConst ("nFormat=bits[(]hex[)]");
    m_cFormat.AddSep ();
    m_cFormat.AddConst ("nData=[\"]");
    m_cFormat.Init ();
    AddCallback (m_cFormat, &CBitsDown::FormatCallback);

    m_cBits.AddString (m_strData, "(([0-9a-f]{2} )*)");
    m_cBits.AddConst ("[\"]");
    m_cBits.Init ();
    AddCallback (m_cBits, &CBitsDown::BitsCallback, eTones);

    AddCallback (m_cPrompt, &CBitsDown::PromptCallback);

//begin user code
//end user code
}

CParser *CBitsDown::NumCallback (void)
{
    LogTrace ("NumData: %ld\n", m_nNumData);

    return this;
}

CParser *CBitsDown::FormatCallback (void)
{
    m_nState = eTones;
    LogTrace ("State Tones\n");

    return this;
}

CParser *CBitsDown::BitsCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        for (long nTone = 0; nTone < m_nNumData; nTone++)
        {
            if ( (nTone * 3) < (long)strlen (m_strData))
            {
                CString strTemp (&m_strData[nTone * 3], 2);
                int nBits = (int)strTemp.Int (16);
                m_pModem->Data ().m_cData.m_cTones.m_cBitallocDown.Set ((size_t)nTone, nBits);
                LogExtra ("%4d:%02x\n", nTone, nBits);
            }
            else
            {
                break;
            }
        }
        LogTrace ("BitsDown\n");
    }
//end user code
    m_nState = eUnknown;
    LogTrace ("State Unknown\n");

    return this;
}

CParser *CBitsDown::PromptCallback (void)
{
    return new CSNRDown (m_pModem, s_strCmdSNRDown);
}

CSNRDown::CSNRDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nReturn (0)
, m_nDirection (0)
, m_nNumData (0)
, m_strData ()
, m_cTone ()
, m_cNum ()
, m_cSnr ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cNum.AddConst ("nReturn=");
    m_cNum.AddInt (m_nReturn);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nDirection=");
    m_cNum.AddInt (m_nDirection);
    m_cNum.AddSep ();
    m_cNum.AddConst ("nNumData=");
    m_cNum.AddInt (m_nNumData);
    m_cNum.Init ();
    AddCallback (m_cNum, &CSNRDown::NumCallback);

    m_cSnr.AddConst ("nFormat=snr[(]hex[)]");
    m_cSnr.AddSep ();
    m_cSnr.AddConst ("nData=[\"]");
    m_cSnr.AddString (m_strData, "(([0-9a-f]{2} )*)");
    m_cSnr.AddConst ("[\"]");
    m_cSnr.Init ();
    AddCallback (m_cSnr, &CSNRDown::SnrCallback);

    AddCallback (m_cPrompt, &CSNRDown::PromptCallback);

//begin user code
//end user code
}

CParser *CSNRDown::NumCallback (void)
{
    LogTrace ("NumData: %ld\n", m_nNumData);

    return this;
}

CParser *CSNRDown::SnrCallback (void)
{
//begin user code
    if (0 <= m_nReturn)
    {
        for (long nTone = 0; nTone < m_nNumData; nTone++)
        {
            if ( (nTone * 3) < (long)strlen (m_strData))
            {
                CString strTemp (&m_strData[nTone * 3], 2);
                int nSNR = (int)strTemp.Int (16);
                if (nSNR >= 255)
                {
                    nSNR = 0;
                }
                else
                {
                    nSNR = (nSNR / 2) - 32;

                    if (nSNR < 0)
                    {
                        nSNR = 0;
                    }
                }
                m_pModem->Data ().m_cData.m_cTones.m_cSNR.Set ((size_t)nTone, nSNR);
                LogExtra ("%4d:%02x\n", nTone, nSNR);
            }
            else
            {
                break;
            }
        }
        LogTrace ("BitsUp\n");
    }
//end user code

    return this;
}

CParser *CSNRDown::PromptCallback (void)
{
    return new CExit (m_pModem, s_strCmdExit);
}

CReboot::CReboot (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CReboot::PromptCallback);

//begin user code
//end user code
}

CParser *CReboot::PromptCallback (void)
{
    return NULL;
}

CExit::CExit (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CExit::PromptCallback);

//begin user code
//end user code
}

CParser *CExit::PromptCallback (void)
{
    return NULL;
}

//! @endcond
//_oOo_
