/*!
 * @file        ar7.cpp
 * @brief       ar7 modem implementation
 * @details     parser for AR7 based modems, tested with:
 * @n           Funkwerk M22
 * @n           Sphairon AR860
 * @n           D-Link DSL-T380
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from ar7.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-ar7-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_AR7_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
//end user code

#include "ar7.h"
#include "log.h"

//! @cond
using namespace NAr7;

static const char *s_strCmdVersion = "cat /proc/avalanche/avsar_ver";
static const char *s_strCmdStatus = "cat /proc/avalanche/avsar_modem_stats";
static const char *s_strCmdBitAlloc = "cat /proc/avalanche/avsar_bit_allocation_table";
static const char *s_strCmdRxSNR = "cat /proc/avalanche/avsar_rxsnr0";
static const char *s_strCmdReboot = "exit";
static const char *s_strCmdExit = "exit";

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CAr7 (cData, pProtocol);
}

CAr7::CAr7 (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
//end user code
{
//begin user code
//end user code
}

void CAr7::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("# ");
    m_strLF.Format ("\r\n");

//begin user code
//end user code

    if (m_pLogin)
    {
        pFirst = new CLoginUser (this, User ());
    }
    else if (CModem::eInfo == eCommand)
    {
        pFirst = new CVersion (this, s_strCmdVersion);
    }
    else if (CModem::eStatus == eCommand)
    {
        pFirst = new CVersion (this, s_strCmdVersion);
    }
    else if (CModem::eResync == eCommand)
    {
        pFirst = new CExit (this, s_strCmdExit);
    }
    else if (CModem::eReboot == eCommand)
    {
        pFirst = new CReboot (this, s_strCmdReboot);
    }

    Protocol ()->Next (pFirst);
}

const CAr7::CBandplanData CAr7::CBandplanData::g_acList[] = {
//begin user code
        CBandplanData (2, "AnxA", CBandplan::eADSL_A),
        CBandplanData (2, "AnxB", CBandplan::eADSL_B),
        //! @todo CBandplanData (4, "G.lite / G.992.2"),
        CBandplanData (8, "AnxA", CBandplan::eADSL2_A),
        CBandplanData (8, "AnxB", CBandplan::eADSL2_B),
        //! @todo CBandplanData (9, "RE-ADSL2 / G.dmt.bis DELT / G.992.3 Annex L"),
        CBandplanData (16, "AnxA", CBandplan::eADSL2p_A),
        CBandplanData (16, "AnxB", CBandplan::eADSL2p_B)
        //! @todo CBandplanData (17, "ADSL2+ DELT / G.992.5 Annex-L"),
        //! @todo CBandplanData (32, "RE-ADSL"),
        //! @todo CBandplanData (33, "RE-ADSL DELT"),
        //! @todo CBandplanData (128, "T1.413 (ADSL ANSI T1.413)")
//end user code
};

CAr7::CBandplanData::CBandplanData (int nMode,
        const char *strAnnex, CBandplan::EBandplan eBandplan)
: m_nMode (nMode)
, m_strAnnex (strAnnex)
, m_eBandplan (eBandplan)
{
}

CAr7::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
: m_nMode (cBandplanData.m_nMode)
, m_strAnnex (cBandplanData.m_strAnnex)
, m_eBandplan (cBandplanData.m_eBandplan)
{
}

CBandplan::EBandplan CAr7::CBandplanData::Get (int nMode,
        const char *strAnnex)
{
    CBandplan::EBandplan eBandplan = CBandplan::eNone;

    for (size_t nIndex = 0; nIndex < ARRAY_SIZE (g_acList); nIndex++)
    {
//begin user code
        if (NULL != strAnnex)
        {
            if ((g_acList[nIndex].m_nMode == nMode) &&
                    (0 == strcmp (strAnnex, g_acList[nIndex].m_strAnnex)))
            {
                eBandplan = g_acList[nIndex].m_eBandplan;
                break;
            }
        }
//end user code
    }

    return eBandplan;
}

CBandplan::EBandplan CAr7::Bandplan (int nMode,
        const char *strAnnex)
{
    return CBandplanData::Get(nMode, strAnnex);
}

//begin user code
//end user code

CLoginUser::CLoginUser (CModem *pModem, const char *strUser)
: CParser (pModem)
, m_strUser (strUser)
{
}

CParser *CLoginUser::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "login: "))
    {
        m_pModem->Protocol ()->Send (m_strUser, "user");
        m_pModem->Protocol ()->Send (m_pModem->LF ());

        pNext = new CLoginPass (m_pModem, m_pModem->Pass ());
    }

    return pNext;
}

CLoginPass::CLoginPass (CModem *pModem, const char *strPass)
: CParser (pModem)
, m_strPass (strPass)
{
}

CParser *CLoginPass::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Password: "))
    {
        m_pModem->Protocol ()->Send (m_strPass, "pass");
        m_pModem->Protocol ()->Send (m_pModem->LF ());
    }
    else if (Find (strRecv, nRecv, m_pModem->Prompt ()))
    {
        if (CModem::eInfo == m_pModem->Command ())
        {
            pNext = new CVersion (m_pModem, s_strCmdVersion);
        }
        else if (CModem::eStatus == m_pModem->Command ())
        {
            pNext = new CVersion (m_pModem, s_strCmdVersion);
        }
        else if (CModem::eResync == m_pModem->Command ())
        {
            pNext = new CExit (m_pModem, s_strCmdExit);
        }
        else if (CModem::eReboot == m_pModem->Command ())
        {
            pNext = new CReboot (m_pModem, s_strCmdReboot);
        }
    }

    return pNext;
}

template <class T> CPromptParser<T>::CPromptParser (CModem *pModem, const char *strCmd)
: CRegExCmdParser<T, CLineParser> (pModem, strCmd)
, m_cPrompt ()
{
    m_cPrompt.AddConst ("# ");
    m_cPrompt.Init ();
}

CVersion::CVersion (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CVersion::PromptCallback);

//begin user code
//end user code
}

CParser *CVersion::PromptCallback (void)
{
    return new CStatus (m_pModem, s_strCmdStatus);
}

CStatus::CStatus (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_nTemp3 (0)
, m_nTemp4 (0)
, m_nTemp5 (0)
, m_nTemp6 (0)
, m_nTemp7 (0)
, m_fTemp0 (0)
, m_fTemp1 (0)
, m_bModeValid (false)
, m_bAnnexValid (false)
, m_nMode (0)
, m_strAnnex ()
, m_cConnRate ()
, m_cDownStream ()
, m_cUpStream ()
, m_cTxPower ()
, m_cPath ()
, m_cMode ()
, m_cAnnex ()
, m_cATUC ()
, m_cATUR ()
, m_cTxInterleavePath ()
, m_cRxInterleavePath ()
, m_cTxFastPath ()
, m_cRxFastPath ()
, m_cCRC_FEC ()
, m_cHEC ()
{
    m_cConnRate.AddConst ("US Connection Rate");
    m_cConnRate.AddSep ();
    m_cConnRate.AddConst (":");
    m_cConnRate.AddSep ();
    m_cConnRate.AddInt (m_nTemp0);
    m_cConnRate.AddSep ();
    m_cConnRate.AddConst ("DS Connection Rate");
    m_cConnRate.AddSep ();
    m_cConnRate.AddConst (":");
    m_cConnRate.AddSep ();
    m_cConnRate.AddInt (m_nTemp1);
    m_cConnRate.Init ();
    AddCallback (m_cConnRate, &CStatus::ConnRateCallback);

    m_cDownStream.AddConst ("DS Line Attenuation");
    m_cDownStream.AddSep ();
    m_cDownStream.AddConst (":");
    m_cDownStream.AddSep ();
    m_cDownStream.AddDouble (m_fTemp0);
    m_cDownStream.AddSep ();
    m_cDownStream.AddConst ("DS Margin");
    m_cDownStream.AddSep ();
    m_cDownStream.AddConst (":");
    m_cDownStream.AddSep ();
    m_cDownStream.AddDouble (m_fTemp1);
    m_cDownStream.Init ();
    AddCallback (m_cDownStream, &CStatus::DownStreamCallback);

    m_cUpStream.AddConst ("US Line Attenuation:");
    m_cUpStream.AddSep ();
    m_cUpStream.AddDouble (m_fTemp0);
    m_cUpStream.AddSep ();
    m_cUpStream.AddConst ("US Margin:");
    m_cUpStream.AddSep ();
    m_cUpStream.AddDouble (m_fTemp1);
    m_cUpStream.Init ();
    AddCallback (m_cUpStream, &CStatus::UpStreamCallback);

    m_cTxPower.AddConst ("US Transmit Power");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst (":");
    m_cTxPower.AddSep ();
    m_cTxPower.AddDouble (m_fTemp0);
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst ("DS Transmit Power");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst (":");
    m_cTxPower.AddSep ();
    m_cTxPower.AddDouble (m_fTemp1);
    m_cTxPower.Init ();
    AddCallback (m_cTxPower, &CStatus::TxPowerCallback);

    m_cPath.AddConst ("Trained Path");
    m_cPath.AddSep ();
    m_cPath.AddConst (":");
    m_cPath.AddSep ();
    m_cPath.AddInt (m_nTemp0);
    m_cPath.AddSep ();
    m_cPath.AddConst ("US Peak Cell Rate");
    m_cPath.AddSep ();
    m_cPath.AddConst (":");
    m_cPath.AddSep ();
    m_cPath.AddInt (m_nTemp1);
    m_cPath.Init ();
    AddCallback (m_cPath, &CStatus::PathCallback);

    m_cMode.AddConst ("Trained Mode");
    m_cMode.AddSep ();
    m_cMode.AddConst (":");
    m_cMode.AddSep ();
    m_cMode.AddInt (m_nMode);
    m_cMode.AddSep ();
    m_cMode.AddConst ("Selected Mode");
    m_cMode.AddSep ();
    m_cMode.AddConst (":");
    m_cMode.AddSep ();
    m_cMode.AddInt (m_nTemp0);
    m_cMode.Init ();
    AddCallback (m_cMode, &CStatus::ModeCallback);

    m_cAnnex.AddConst ("Annex");
    m_cAnnex.AddSep ();
    m_cAnnex.AddConst (":");
    m_cAnnex.AddSep ();
    m_cAnnex.AddString (m_strAnnex);
    m_cAnnex.AddSep ();
    m_cAnnex.AddConst ("psd_mask_qualifier");
    m_cAnnex.AddSep ();
    m_cAnnex.AddConst (":");
    m_cAnnex.AddSep ();
    m_cAnnex.AddInt (m_nTemp0);
    m_cAnnex.Init ();
    AddCallback (m_cAnnex, &CStatus::AnnexCallback);

    m_cATUC.AddConst ("ATUC ghsVid");
    m_cATUC.AddSep ();
    m_cATUC.AddConst (":");
    m_cATUC.AddSep ();
    m_cATUC.AddInt (m_nTemp0, 16, 2);
    m_cATUC.AddSep ();
    m_cATUC.AddInt (m_nTemp1, 16, 2);
    m_cATUC.AddSep ();
    m_cATUC.AddInt (m_nTemp2, 16, 2);
    m_cATUC.AddSep ();
    m_cATUC.AddInt (m_nTemp3, 16, 2);
    m_cATUC.AddSep ();
    m_cATUC.AddInt (m_nTemp4, 16, 2);
    m_cATUC.AddSep ();
    m_cATUC.AddInt (m_nTemp5, 16, 2);
    m_cATUC.AddSep ();
    m_cATUC.AddInt (m_nTemp6, 16, 2);
    m_cATUC.AddSep ();
    m_cATUC.AddInt (m_nTemp7, 16, 2);
    m_cATUC.Init ();
    AddCallback (m_cATUC, &CStatus::ATUCCallback);

    m_cATUR.AddConst ("ATUR ghsVid");
    m_cATUR.AddSep ();
    m_cATUR.AddConst (":");
    m_cATUR.AddSep ();
    m_cATUR.AddInt (m_nTemp0, 16, 2);
    m_cATUR.AddSep ();
    m_cATUR.AddInt (m_nTemp1, 16, 2);
    m_cATUR.AddSep ();
    m_cATUR.AddInt (m_nTemp2, 16, 2);
    m_cATUR.AddSep ();
    m_cATUR.AddInt (m_nTemp3, 16, 2);
    m_cATUR.AddSep ();
    m_cATUR.AddInt (m_nTemp4, 16, 2);
    m_cATUR.AddSep ();
    m_cATUR.AddInt (m_nTemp5, 16, 2);
    m_cATUR.AddSep ();
    m_cATUR.AddInt (m_nTemp6, 16, 2);
    m_cATUR.AddSep ();
    m_cATUR.AddInt (m_nTemp7, 16, 2);
    m_cATUR.Init ();
    AddCallback (m_cATUR, &CStatus::ATURCallback);

    m_cTxInterleavePath.AddConst ("[[]Upstream [(]TX[)] Interleave path[]]");
    m_cTxInterleavePath.Init ();
    AddCallback (m_cTxInterleavePath, &CStatus::TxInterleavePath_ICallback, eInterleavePath);
    AddCallback (m_cTxInterleavePath, &CStatus::TxInterleavePath_ICallback, eRxInterleavePath);
    AddCallback (m_cTxInterleavePath, &CStatus::TxInterleavePath_FCallback, eTxFastPath);
    AddCallback (m_cTxInterleavePath, &CStatus::TxInterleavePath_FCallback, eRxFastPath);

    m_cRxInterleavePath.AddConst ("[[]Downstream [(]RX[)] Interleave path[]]");
    m_cRxInterleavePath.Init ();
    AddCallback (m_cRxInterleavePath, &CStatus::RxInterleavePath_ICallback, eInterleavePath);
    AddCallback (m_cRxInterleavePath, &CStatus::RxInterleavePath_ICallback, eTxInterleavePath);
    AddCallback (m_cRxInterleavePath, &CStatus::RxInterleavePath_FCallback, eTxFastPath);
    AddCallback (m_cRxInterleavePath, &CStatus::RxInterleavePath_FCallback, eRxFastPath);

    m_cTxFastPath.AddConst ("[[]Upstream [(]TX[)] Fast path[]]");
    m_cTxFastPath.Init ();
    AddCallback (m_cTxFastPath, &CStatus::TxFastPath_FCallback, eFastPath);
    AddCallback (m_cTxFastPath, &CStatus::TxFastPath_FCallback, eRxFastPath);
    AddCallback (m_cTxFastPath, &CStatus::TxFastPath_ICallback, eRxInterleavePath);
    AddCallback (m_cTxFastPath, &CStatus::TxFastPath_ICallback, eTxInterleavePath);

    m_cRxFastPath.AddConst ("[[]Downstream [(]RX[)] Fast path[]]");
    m_cRxFastPath.Init ();
    AddCallback (m_cRxFastPath, &CStatus::RxFastPath_FCallback, eFastPath);
    AddCallback (m_cRxFastPath, &CStatus::RxFastPath_FCallback, eTxFastPath);
    AddCallback (m_cRxFastPath, &CStatus::RxFastPath_ICallback, eRxInterleavePath);
    AddCallback (m_cRxFastPath, &CStatus::RxFastPath_ICallback, eTxInterleavePath);

    m_cCRC_FEC.AddConst ("CRC");
    m_cCRC_FEC.AddSep ();
    m_cCRC_FEC.AddConst (":");
    m_cCRC_FEC.AddSep ();
    m_cCRC_FEC.AddInt (m_nTemp0);
    m_cCRC_FEC.AddSep ();
    m_cCRC_FEC.AddConst ("FEC");
    m_cCRC_FEC.AddSep ();
    m_cCRC_FEC.AddConst (":");
    m_cCRC_FEC.AddSep ();
    m_cCRC_FEC.AddInt (m_nTemp1);
    m_cCRC_FEC.AddSep ();
    m_cCRC_FEC.AddConst ("NCD");
    m_cCRC_FEC.AddSep ();
    m_cCRC_FEC.AddConst (":");
    m_cCRC_FEC.AddSep ();
    m_cCRC_FEC.AddInt (m_nTemp2);
    m_cCRC_FEC.Init ();
    AddCallback (m_cCRC_FEC, &CStatus::CRC_FECTxICallback, eTxInterleavePath);
    AddCallback (m_cCRC_FEC, &CStatus::CRC_FECRxICallback, eRxInterleavePath);
    AddCallback (m_cCRC_FEC, &CStatus::CRC_FECTxFCallback, eTxFastPath);
    AddCallback (m_cCRC_FEC, &CStatus::CRC_FECRxFCallback, eRxFastPath);

    m_cHEC.AddConst ("LCD");
    m_cHEC.AddSep ();
    m_cHEC.AddConst (":");
    m_cHEC.AddSep ();
    m_cHEC.AddInt (m_nTemp0);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("HEC");
    m_cHEC.AddSep ();
    m_cHEC.AddConst (":");
    m_cHEC.AddSep ();
    m_cHEC.AddInt (m_nTemp1);
    m_cHEC.Init ();
    AddCallback (m_cHEC, &CStatus::HECTxICallback, eTxInterleavePath);
    AddCallback (m_cHEC, &CStatus::HECRxICallback, eRxInterleavePath);
    AddCallback (m_cHEC, &CStatus::HECTxFCallback, eTxFastPath);
    AddCallback (m_cHEC, &CStatus::HECRxFCallback, eRxFastPath);

    AddCallback (m_cPrompt, &CStatus::PromptCallback);

//begin user code
//end user code
}

CParser *CStatus::ConnRateCallback (void)
{
//begin user code
    if (m_nTemp0 && m_nTemp1)
    {
        m_pModem->Data ().m_cData.m_cModemState.Set(1);
        m_pModem->Data ().m_cData.m_cModemStateStr.Set("up");
    }
    else
    {
        m_pModem->Data ().m_cData.m_cModemState.Set(0);
        m_pModem->Data ().m_cData.m_cModemStateStr.Set("down");
    }
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.Set (m_nTemp0, m_nTemp1);
    LogTrace ("Bandwidth Up:%ld Down:%ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CStatus::DownStreamCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.SetDown (0, m_fTemp0);
    m_pModem->Data ().m_cData.m_cNoiseMargin.SetDown (0, m_fTemp1);
    LogTrace ("Attenuation Down:%f\n", m_nTemp0);
    LogTrace ("NoiseMargin Down:%f\n", m_fTemp1);

    return this;
}

CParser *CStatus::UpStreamCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.SetUp (0, m_fTemp0);
    m_pModem->Data ().m_cData.m_cNoiseMargin.SetUp (0, m_fTemp1);
    LogTrace ("Attenuation Up:%f\n", m_nTemp0);
    LogTrace ("NoiseMargin Up:%f\n", m_fTemp1);

    return this;
}

CParser *CStatus::TxPowerCallback (void)
{
    m_pModem->Data ().m_cData.m_cTxPower.Set (m_fTemp0, m_fTemp1);
    LogTrace ("TxPower Up:%f Down:%f\n", m_fTemp0, m_fTemp1);

    return this;
}

CParser *CStatus::PathCallback (void)
{
//begin user code
    if (0 == m_nTemp0)
    {
        m_pModem->Data ().m_cData.m_cChannelMode.Set( "Fast Path");
        m_nState = eFastPath;
    }
    else
    {
        m_pModem->Data ().m_cData.m_cChannelMode.Set( "Interleave Path");
        m_nState = eInterleavePath;
    }
//end user code
    LogTrace ("Path: %ld\n", m_nTemp0);

    return this;
}

CParser *CStatus::ModeCallback (void)
{
    m_bModeValid = true;

    return this;
}

CParser *CStatus::AnnexCallback (void)
{
    m_bAnnexValid = true;

    return this;
}

CParser *CStatus::ATUCCallback (void)
{
//begin user code
    char strVendor[5];
    strVendor[0] = (char)m_nTemp2;
    strVendor[1] = (char)m_nTemp3;
    strVendor[2] = (char)m_nTemp4;
    strVendor[3] = (char)m_nTemp5;
    strVendor[4] = '\0';
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetVendor (strVendor);
//end user code
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetSpec ((int)m_nTemp6, (int)m_nTemp7);
//begin user code
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetRevision (0);
//end user code
    LogTrace ("ATU-C Vendor: %c%c%c%c\n", m_nTemp2, m_nTemp3, m_nTemp4, m_nTemp5);
    LogTrace ("ATU-C Spec: %ld.%ld\n", m_nTemp6, m_nTemp7);

    return this;
}

CParser *CStatus::ATURCallback (void)
{
//begin user code
    char strVendor[5];
    strVendor[0] = (char)m_nTemp2;
    strVendor[1] = (char)m_nTemp3;
    strVendor[2] = (char)m_nTemp4;
    strVendor[3] = (char)m_nTemp5;
    strVendor[4] = '\0';
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetVendor (strVendor);
//end user code
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetSpec ((int)m_nTemp6, (int)m_nTemp7);
//begin user code
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetRevision (0);
//end user code
    LogTrace ("ATU-R Vendor: %c%c%c%c\n", m_nTemp2, m_nTemp3, m_nTemp4, m_nTemp5);
    LogTrace ("ATU-R Spec: %ld.%ld\n", m_nTemp6, m_nTemp7);

    return this;
}

CParser *CStatus::TxInterleavePath_ICallback (void)
{
    m_nState = eTxInterleavePath;
    LogTrace ("State TxInterleavePath\n");

    return this;
}

CParser *CStatus::TxInterleavePath_FCallback (void)
{
    m_nState = eFastPath;
    LogTrace ("State FastPath\n");

    return this;
}

CParser *CStatus::RxInterleavePath_ICallback (void)
{
    m_nState = eRxInterleavePath;
    LogTrace ("State RxInterleavePath\n");

    return this;
}

CParser *CStatus::RxInterleavePath_FCallback (void)
{
    m_nState = eFastPath;
    LogTrace ("State FastPath\n");

    return this;
}

CParser *CStatus::TxFastPath_FCallback (void)
{
    m_nState = eTxFastPath;
    LogTrace ("State TxFastPath\n");

    return this;
}

CParser *CStatus::TxFastPath_ICallback (void)
{
    m_nState = eInterleavePath;
    LogTrace ("State InterleavePath\n");

    return this;
}

CParser *CStatus::RxFastPath_FCallback (void)
{
    m_nState = eRxFastPath;
    LogTrace ("State RxFastPath\n");

    return this;
}

CParser *CStatus::RxFastPath_ICallback (void)
{
    m_nState = eInterleavePath;
    LogTrace ("State InterleavePath\n");

    return this;
}

CParser *CStatus::CRC_FECTxICallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetUp (m_nTemp0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetUp (m_nTemp1);
    LogTrace ("TxCRC: %ld TxFEC: %ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CStatus::CRC_FECRxICallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetDown (m_nTemp1);
    LogTrace ("RxCRC: %ld RxFEC: %ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CStatus::CRC_FECTxFCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetUp (m_nTemp0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetUp (m_nTemp1);
    LogTrace ("TxCRC: %ld TxFEC: %ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CStatus::CRC_FECRxFCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetDown (m_nTemp1);
    LogTrace ("RxCRC: %ld RxFEC: %ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CStatus::HECTxICallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetUp (m_nTemp1);
    LogTrace ("TxHEC: %ld\n", m_nTemp1);

    return this;
}

CParser *CStatus::HECRxICallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetDown (m_nTemp1);
    LogTrace ("RxHEC: %ld\n", m_nTemp1);

    return this;
}

CParser *CStatus::HECTxFCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetUp (m_nTemp1);
    LogTrace ("TxHEC: %ld\n", m_nTemp1);

    return this;
}

CParser *CStatus::HECRxFCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetDown (m_nTemp1);
    LogTrace ("RxHEC: %ld\n", m_nTemp1);

    return this;
}

CParser *CStatus::PromptCallback (void)
{
    CParser *pNext = this;
//begin user code
    if (m_bModeValid && m_bAnnexValid)
    {
        CString strOperationMode;
        m_pModem->Data ().Bandplan (CAr7::Bandplan((int)m_nMode, m_strAnnex));
        m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
        m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);
    }
//end user code

    if (CModem::eInfo == m_pModem->Command ())
    {
        pNext = new CBitAlloc (m_pModem, s_strCmdBitAlloc);
    }
    else
    {
        pNext = new CExit (m_pModem, s_strCmdExit);
    }

    return pNext;
}

CBitAlloc::CBitAlloc (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_bToneValid (false)
, m_cTone ()
, m_cBitAllocUp ()
, m_cBitAllocDown ()
, m_cTones ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cBitAllocUp.AddConst ("AR7 DSL Modem US Bit Allocation");
    m_cBitAllocUp.AddSep ();
    m_cBitAllocUp.AddConst (":");
    m_cBitAllocUp.Init ();
    AddCallback (m_cBitAllocUp, &CBitAlloc::BitAllocUpCallback);

    m_cBitAllocDown.AddConst ("AR7 DSL Modem DS Bit Allocation");
    m_cBitAllocDown.AddSep ();
    m_cBitAllocDown.AddConst (":");
    m_cBitAllocDown.Init ();
    AddCallback (m_cBitAllocDown, &CBitAlloc::BitAllocDownUpCallback, eBitAllocUp);
    AddCallback (m_cBitAllocDown, &CBitAlloc::BitAllocDownCallback);

    m_cTones.AddTone (m_cTone, 16, 2);
    for (int i = 0; i < 15; i++)
    {
        m_cTones.AddSep ();
        m_cTones.AddTone (m_cTone, 16, 2);
    }
    m_cTones.Init ();
    AddCallback (m_cTones, &CBitAlloc::TonesCallback, eBitAllocUp);
    AddCallback (m_cTones, &CBitAlloc::TonesCallback, eBitAllocDown);

    AddCallback (m_cPrompt, &CBitAlloc::PromptCallback);

//begin user code
//end user code
}

CParser *CBitAlloc::BitAllocUpCallback (void)
{
    m_cTone.Init ();
    m_bToneValid = false;
    m_nState = eBitAllocUp;
    LogTrace ("State BitAllocUp\n");

    return this;
}

CParser *CBitAlloc::BitAllocDownUpCallback (void)
{
    if (m_bToneValid)
    {
        m_pModem->Data ().m_cData.m_cTones.m_cBitallocUp.Set (m_cTone);
        LogTrace ("ToneBitallocUp\n");
    }
    m_cTone.Init ();
    m_bToneValid = false;
    m_nState = eBitAllocDown;
    LogTrace ("State BitAllocDown\n");

    return this;
}

CParser *CBitAlloc::BitAllocDownCallback (void)
{
    m_cTone.Init ();
    m_bToneValid = false;
    m_nState = eBitAllocDown;
    LogTrace ("State BitAllocDown\n");

    return this;
}

CParser *CBitAlloc::TonesCallback (void)
{
    m_bToneValid = true;
    LogTrace ("Tones\n");

    return this;
}

CParser *CBitAlloc::PromptCallback (void)
{
    if (m_bToneValid)
    {
        m_pModem->Data ().m_cData.m_cTones.m_cBitallocDown.Set (m_cTone);
        LogTrace ("ToneBitallocDown\n");
    }

    return new CRxSNR (m_pModem, s_strCmdRxSNR);
}

CRxSNR::CRxSNR (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_bToneValid (false)
, m_cTone ()
, m_cRxSNR ()
, m_cTones ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cRxSNR.AddConst ("AR7 DSL Modem Rx SNR Per Bin for Bin0");
    m_cRxSNR.AddSep ();
    m_cRxSNR.AddConst (":");
    m_cRxSNR.Init ();
    AddCallback (m_cRxSNR, &CRxSNR::RxSNRCallback);

    m_cTones.AddTone (m_cTone, 16, 4);
    for (int i = 0; i < 15; i++)
    {
        m_cTones.AddSep ();
        m_cTones.AddTone (m_cTone, 16, 4);
    }
    m_cTones.Init ();
    AddCallback (m_cTones, &CRxSNR::TonesCallback, eRxSNR);

    AddCallback (m_cPrompt, &CRxSNR::PromptCallback);

//begin user code
//end user code
}

CParser *CRxSNR::RxSNRCallback (void)
{
    m_cTone.Init ();
    m_bToneValid = false;
    m_nState = eRxSNR;
    LogTrace ("State RxSNR\n");

    return this;
}

CParser *CRxSNR::TonesCallback (void)
{
    m_bToneValid = true;
    LogTrace ("Tones\n");

    return this;
}

CParser *CRxSNR::PromptCallback (void)
{
    if (m_bToneValid)
    {
        m_cTone.Scale (0, 1, 100, 16);
        m_pModem->Data ().m_cData.m_cTones.m_cSNR.Set (m_cTone);
        LogTrace ("ToneSNR\n");
    }

    return new CExit (m_pModem, s_strCmdExit);
}

CReboot::CReboot (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CReboot::PromptCallback);

//begin user code
//end user code
}

CParser *CReboot::PromptCallback (void)
{
    return NULL;
}

CExit::CExit (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CExit::PromptCallback);

//begin user code
//end user code
}

CParser *CExit::PromptCallback (void)
{
    return NULL;
}

//! @endcond
//_oOo_
