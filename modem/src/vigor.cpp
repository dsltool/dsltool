/*!
 * @file        vigor.cpp
 * @brief       vigor modem implementation
 * @details     parser for vigor modems, tested with:
 * @n           Vigor 130
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from vigor.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-vigor-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_VIGOR_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
//end user code

#include "vigor.h"
#include "log.h"

//! @cond
using namespace NVigor;

static const char *s_strCmdStatus = "adsl status";
static const char *s_strCmdMore = "adsl status more";
static const char *s_strCmdAtm = "adsl ppp";
static const char *s_strCmdVersion = "sys version";
static const char *s_strCmdBitAllocDown = "adsl showbins";
static const char *s_strCmdBitAllocUp = "adsl showbins up";
static const char *s_strCmdResync = "adsl reboot";
static const char *s_strCmdReboot = "sys reboot";
static const char *s_strCmdExit = "quit";

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CVigor (cData, pProtocol);
}

const long CVigor::s_nVersionV3781 = CModem::BuildVersion (3, 7, 8, 1);

CVigor::CVigor (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
//end user code
{
//begin user code
//end user code
}

void CVigor::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("> ");
    m_strLF.Format ("\r\n");

//begin user code
//end user code

    if (m_pLogin)
    {
        pFirst = new CLoginUser (this, User ());
    }
    else if (CModem::eInfo == eCommand)
    {
        pFirst = new CStatus (this, s_strCmdStatus);
    }
    else if (CModem::eStatus == eCommand)
    {
        pFirst = new CStatus (this, s_strCmdStatus);
    }
    else if (CModem::eResync == eCommand)
    {
        pFirst = new CResync (this, s_strCmdResync);
    }
    else if (CModem::eReboot == eCommand)
    {
        pFirst = new CReboot (this, s_strCmdReboot);
    }

    Protocol ()->Next (pFirst);
}

const CVigor::CBandplanData CVigor::CBandplanData::g_acList[] = {
//begin user code
        CBandplanData ("ADSL2+ Annex A", CBandplan::eADSL2p_A),
        CBandplanData ("ADSL2+ Annex B", CBandplan::eADSL2p_B),
        CBandplanData ("ADSL2+ Annex J", CBandplan::eADSL2p_J),
        CBandplanData ("8B", CBandplan::eVDSL2_B8_6_8b),
        CBandplanData ("17A", CBandplan::eVDSL2_B8_12_17a),
        CBandplanData ("35B", CBandplan::eVDSL2_B8_21_35b)
//end user code
};

CVigor::CBandplanData::CBandplanData (const char *strMode, CBandplan::EBandplan eBandplan)
: m_strMode (strMode)
, m_eBandplan (eBandplan)
{
}

CVigor::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
: m_strMode (cBandplanData.m_strMode)
, m_eBandplan (cBandplanData.m_eBandplan)
{
}

CBandplan::EBandplan CVigor::CBandplanData::Get (const char *strMode)
{
    CBandplan::EBandplan eBandplan = CBandplan::eNone;

    for (size_t nIndex = 0; nIndex < ARRAY_SIZE (g_acList); nIndex++)
    {
//begin user code
        if (NULL != strMode)
        {
            if (0 == strcmp (strMode, g_acList[nIndex].m_strMode))
            {
                eBandplan = g_acList[nIndex].m_eBandplan;
                break;
            }
        }
//end user code
    }

    return eBandplan;
}

CBandplan::EBandplan CVigor::Bandplan (const char *strMode)
{
    return CBandplanData::Get(strMode);
}

//begin user code
//end user code

CLoginUser::CLoginUser (CModem *pModem, const char *strUser)
: CParser (pModem)
, m_strUser (strUser)
{
}

CParser *CLoginUser::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Account:"))
    {
        m_pModem->Protocol ()->Send (m_strUser, "user");
        m_pModem->Protocol ()->Send (m_pModem->LF ());

        pNext = new CLoginPass (m_pModem, m_pModem->Pass ());
    }

    return pNext;
}

CLoginPass::CLoginPass (CModem *pModem, const char *strPass)
: CParser (pModem)
, m_strPass (strPass)
{
}

CParser *CLoginPass::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Password: "))
    {
        m_pModem->Protocol ()->Send (m_strPass, "pass");
        m_pModem->Protocol ()->Send (m_pModem->LF ());
    }
    else if (Find (strRecv, nRecv, m_pModem->Prompt ()))
    {
        if (CModem::eInfo == m_pModem->Command ())
        {
            pNext = new CStatus (m_pModem, s_strCmdStatus);
        }
        else if (CModem::eStatus == m_pModem->Command ())
        {
            pNext = new CStatus (m_pModem, s_strCmdStatus);
        }
        else if (CModem::eResync == m_pModem->Command ())
        {
            pNext = new CResync (m_pModem, s_strCmdResync);
        }
        else if (CModem::eReboot == m_pModem->Command ())
        {
            pNext = new CReboot (m_pModem, s_strCmdReboot);
        }
    }

    return pNext;
}

template <class T> CPromptParser<T>::CPromptParser (CModem *pModem, const char *strCmd)
: CRegExCmdParser<T, CLineParser> (pModem, strCmd)
, m_cPrompt ()
{
    m_cPrompt.AddConst ("Vigor> |> ");
    m_cPrompt.Init ();
}

CStatus::CStatus (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_nTemp3 (0)
, m_nCountry0 (0)
, m_nCountry1 (0)
, m_nVendorID0 (0)
, m_nVendorID1 (0)
, m_nVendorID2 (0)
, m_nVendorID3 (0)
, m_nVendorSpec0 (0)
, m_nVendorSpec1 (0)
, m_fTemp0 (0)
, m_fTemp1 (0)
, m_strTemp0 ()
, m_strTemp1 ()
, m_cATU_R ()
, m_cATU_C ()
, m_cMode ()
, m_cActRate ()
, m_cMaxRate ()
, m_cPath ()
, m_cAttenuation ()
, m_cTxPower ()
, m_cVersion_R ()
, m_cVersion_C ()
{
    m_cATU_R.AddConst ("[-]*");
    m_cATU_R.AddSep ();
    m_cATU_R.AddConst ("ATU-R Info");
    m_cATU_R.AddSep ();
    m_cATU_R.AddConst ("[(]hw: annex");
    m_cATU_R.AddSep ();
    m_cATU_R.AddString (m_strTemp0);
    m_cATU_R.AddSep ();
    m_cATU_R.AddConst (", f/w: annex");
    m_cATU_R.AddSep ();
    m_cATU_R.AddString (m_strTemp1);
    m_cATU_R.AddSep ();
    m_cATU_R.AddConst ("[)] [-]*");
    m_cATU_R.Init ();
    AddCallback (m_cATU_R, &CStatus::ATU_RCallback);

    m_cATU_C.AddConst ("[-]*");
    m_cATU_C.AddSep ();
    m_cATU_C.AddConst ("ATU-C Info");
    m_cATU_C.AddSep ();
    m_cATU_C.AddConst ("[-]*");
    m_cATU_C.Init ();
    AddCallback (m_cATU_C, &CStatus::ATU_CCallback);

    m_cMode.AddConst ("Running Mode");
    m_cMode.AddSep ();
    m_cMode.AddConst (":");
    m_cMode.AddSep ();
    m_cMode.AddString (m_strTemp0);
    m_cMode.AddSep ();
    m_cMode.AddConst ("State");
    m_cMode.AddSep ();
    m_cMode.AddConst (":");
    m_cMode.AddSep ();
    m_cMode.AddString (m_strTemp1);
    m_cMode.Init ();
    AddCallback (m_cMode, &CStatus::ModeCallback, eATU_R);

    m_cActRate.AddConst ("DS Actual Rate");
    m_cActRate.AddSep ();
    m_cActRate.AddConst (":");
    m_cActRate.AddSep ();
    m_cActRate.AddInt (m_nTemp0);
    m_cActRate.AddSep ();
    m_cActRate.AddConst ("bps");
    m_cActRate.AddSep ();
    m_cActRate.AddConst ("US Actual Rate");
    m_cActRate.AddSep ();
    m_cActRate.AddConst (":");
    m_cActRate.AddSep ();
    m_cActRate.AddInt (m_nTemp1);
    m_cActRate.AddSep ();
    m_cActRate.AddConst ("bps");
    m_cActRate.Init ();
    AddCallback (m_cActRate, &CStatus::ActRateCallback, eATU_R);

    m_cMaxRate.AddConst ("DS Attainable Rate");
    m_cMaxRate.AddSep ();
    m_cMaxRate.AddConst (":");
    m_cMaxRate.AddSep ();
    m_cMaxRate.AddInt (m_nTemp0);
    m_cMaxRate.AddSep ();
    m_cMaxRate.AddConst ("bps");
    m_cMaxRate.AddSep ();
    m_cMaxRate.AddConst ("US Attainable Rate");
    m_cMaxRate.AddSep ();
    m_cMaxRate.AddConst (":");
    m_cMaxRate.AddSep ();
    m_cMaxRate.AddInt (m_nTemp1);
    m_cMaxRate.AddSep ();
    m_cMaxRate.AddConst ("bps");
    m_cMaxRate.Init ();
    AddCallback (m_cMaxRate, &CStatus::MaxRateCallback, eATU_R);

    m_cPath.AddConst ("DS Path Mode");
    m_cPath.AddSep ();
    m_cPath.AddConst (":");
    m_cPath.AddSep ();
    m_cPath.AddString (m_strTemp0);
    m_cPath.AddSep ();
    m_cPath.AddConst ("US Path Mode");
    m_cPath.AddSep ();
    m_cPath.AddConst (":");
    m_cPath.AddSep ();
    m_cPath.AddString (m_strTemp1);
    m_cPath.Init ();
    AddCallback (m_cPath, &CStatus::PathCallback, eATU_R);

    m_cAttenuation.AddString (m_strTemp0);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst ("Current Attenuation");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst (":");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp0);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst ("dB");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddString (m_strTemp1);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst ("SNR Margin");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst (":");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp1);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst ("dB");
    m_cAttenuation.Init ();
    AddCallback (m_cAttenuation, &CStatus::AttenuationUpCallback, eATU_C);
    AddCallback (m_cAttenuation, &CStatus::AttenuationDownCallback, eATU_R);

    m_cTxPower.AddConst ("DS actual PSD");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst (":");
    m_cTxPower.AddSep ();
    m_cTxPower.AddInt (m_nTemp0);
    m_cTxPower.AddConst ("\\.");
    m_cTxPower.AddSep ();
    m_cTxPower.AddInt (m_nTemp1);
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst ("dB");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst ("US actual PSD");
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst (":");
    m_cTxPower.AddSep ();
    m_cTxPower.AddInt (m_nTemp2);
    m_cTxPower.AddConst ("\\.");
    m_cTxPower.AddSep ();
    m_cTxPower.AddInt (m_nTemp3);
    m_cTxPower.AddSep ();
    m_cTxPower.AddConst ("dB");
    m_cTxPower.Init ();
    AddCallback (m_cTxPower, &CStatus::TxPowerUpCallback, eATU_R);

    m_cVersion_R.AddConst ("ITU Version[[]0[]]");
    m_cVersion_R.AddSep ();
    m_cVersion_R.AddConst (":");
    m_cVersion_R.AddSep ();
    m_cVersion_R.AddInt (m_nCountry0, 16, 2);
    m_cVersion_R.AddInt (m_nCountry1, 16, 2);
    m_cVersion_R.AddInt (m_nVendorID0, 16, 2);
    m_cVersion_R.AddInt (m_nVendorID1, 16, 2);
    m_cVersion_R.AddSep ();
    m_cVersion_R.AddConst ("ITU Version[[]1[]]");
    m_cVersion_R.AddSep ();
    m_cVersion_R.AddConst (":");
    m_cVersion_R.AddSep ();
    m_cVersion_R.AddInt (m_nVendorID2, 16, 2);
    m_cVersion_R.AddInt (m_nVendorID3, 16, 2);
    m_cVersion_R.AddInt (m_nVendorSpec0, 16, 2);
    m_cVersion_R.AddInt (m_nVendorSpec1, 16, 2);
    m_cVersion_R.Init ();
    AddCallback (m_cVersion_R, &CStatus::Version_RCallback, eATU_R);

    m_cVersion_C.AddConst ("CO ITU Version[[]0[]]");
    m_cVersion_C.AddSep ();
    m_cVersion_C.AddConst (":");
    m_cVersion_C.AddSep ();
    m_cVersion_C.AddInt (m_nCountry0, 16, 2);
    m_cVersion_C.AddInt (m_nCountry1, 16, 2);
    m_cVersion_C.AddInt (m_nVendorID0, 16, 2);
    m_cVersion_C.AddInt (m_nVendorID1, 16, 2);
    m_cVersion_C.AddSep ();
    m_cVersion_C.AddConst ("CO ITU Version[[]1[]]");
    m_cVersion_C.AddSep ();
    m_cVersion_C.AddConst (":");
    m_cVersion_C.AddSep ();
    m_cVersion_C.AddInt (m_nVendorID2, 16, 2);
    m_cVersion_C.AddInt (m_nVendorID3, 16, 2);
    m_cVersion_C.AddInt (m_nVendorSpec0, 16, 2);
    m_cVersion_C.AddInt (m_nVendorSpec1, 16, 2);
    m_cVersion_C.Init ();
    AddCallback (m_cVersion_C, &CStatus::Version_CCallback, eATU_C);

    AddCallback (m_cPrompt, &CStatus::PromptCallback);

//begin user code
//end user code
}

CParser *CStatus::ATU_RCallback (void)
{
    m_nState = eATU_R;
    LogTrace ("State ATU_R\n");

    return this;
}

CParser *CStatus::ATU_CCallback (void)
{
    m_nState = eATU_C;
    LogTrace ("State ATU_C\n");

    return this;
}

CParser *CStatus::ModeCallback (void)
{
//begin user code
    CString strOperationMode;
    int nModemState = 0;

    if (0 == strncasecmp (m_strTemp1, "SHOWTIME", 8))
    {
        nModemState = 1;
    }
    m_pModem->Data ().m_cData.m_cModemState.Set (nModemState);

    m_pModem->Data ().Bandplan (CVigor::Bandplan (m_strTemp0));
    m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
    m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);
    if (0 == strncasecmp (m_pModem->Data ().Bandplan()->Type(), "VDSL" , 4))
    {
        CString strProfile;
        m_pModem->Data ().Bandplan ()->Profile (strProfile);
        m_pModem->Data ().m_cData.m_cProfile.Set (strProfile);
    }

//end user code
    m_pModem->Data ().m_cData.m_cModemStateStr.Set (m_strTemp1);
    LogTrace ("Mode %s\nState %s\n", (const char*)m_strTemp0, (const char*)m_strTemp1);

    return this;
}

CParser *CStatus::ActRateCallback (void)
{
//begin user code
    m_nTemp0 /= 1000;
    m_nTemp1 /= 1000;
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetUp (m_nTemp1);
    LogTrace ("ActRate Down: %ld kbit/s Up: %ld kbit/s\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CStatus::MaxRateCallback (void)
{
//begin user code
    m_nTemp0 /= 1000;
    m_nTemp1 /= 1000;
//end user code
    m_pModem->Data ().m_cData.m_cBandwidth.m_cMaxKbits.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cBandwidth.m_cMaxKbits.SetUp (m_nTemp1);
    LogTrace ("MaxRate Down: %ld kbit/s Up: %ld kbit/s\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CStatus::PathCallback (void)
{
    m_pModem->Data ().m_cData.m_cChannelMode.Set (m_strTemp0);
    LogTrace ("ChannelMode: %s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CStatus::AttenuationUpCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.SetUp (0, m_fTemp0);
    m_pModem->Data ().m_cData.m_cNoiseMargin.SetUp (0, m_fTemp1);
    LogTrace ("Up Attenuation: %lf dB SNR Margin: %lf dB\n", m_fTemp0, m_fTemp1);

    return this;
}

CParser *CStatus::AttenuationDownCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.SetDown (0, m_fTemp0);
    m_pModem->Data ().m_cData.m_cNoiseMargin.SetDown (0, m_fTemp1);
    LogTrace ("Down Attenuation: %lf dB SNR Margin: %lf dB\n", m_fTemp0, m_fTemp1);

    return this;
}

CParser *CStatus::TxPowerUpCallback (void)
{
//begin user code
    m_fTemp0 = (double)m_nTemp0 + (((double)m_nTemp1)/10.0);
    m_fTemp1 = (double)m_nTemp2 + (((double)m_nTemp3)/10.0);
//end user code
    m_pModem->Data ().m_cData.m_cTxPower.Set (m_fTemp1, m_fTemp0);
    LogTrace ("TxPower Up: %lf dB Down: %lf dB\n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CStatus::Version_RCallback (void)
{
//begin user code
    char strVendor[5];

    strVendor[0] = (char)m_nVendorID0;
    strVendor[1] = (char)m_nVendorID1;
    strVendor[2] = (char)m_nVendorID2;
    strVendor[3] = (char)m_nVendorID3;
    strVendor[4] = '\0';

    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetVendor (strVendor);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_R.SetSpec ((int)m_nVendorSpec0, (int)m_nVendorSpec1);
    LogTrace ("ATU-R Vendor: %s\n", strVendor);
    LogTrace ("ATU-R Spec: %ld.%ld\n", m_nVendorSpec0, m_nVendorSpec1);
//end user code

    return this;
}

CParser *CStatus::Version_CCallback (void)
{
//begin user code
    char strVendor[5];

    strVendor[0] = (char)m_nVendorID0;
    strVendor[1] = (char)m_nVendorID1;
    strVendor[2] = (char)m_nVendorID2;
    strVendor[3] = (char)m_nVendorID3;
    strVendor[4] = '\0';

    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetVendor (strVendor);
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetSpec ((int)m_nVendorSpec0, (int)m_nVendorSpec1);
    LogTrace ("ATU-C Vendor: %s\n", strVendor);
    LogTrace ("ATU-C Spec: %ld.%ld\n", m_nVendorSpec0, m_nVendorSpec1);
//end user code

    return this;
}

CParser *CStatus::PromptCallback (void)
{
    return new CMore (m_pModem, s_strCmdMore);
}

CMore::CMore (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_strTemp0 ()
, m_strTemp1 ()
, m_cATU_R ()
, m_cHEC ()
, m_cCRC ()
, m_cFEC ()
{
    m_cATU_R.AddConst ("[-]*");
    m_cATU_R.AddSep ();
    m_cATU_R.AddConst ("ATU-R Info");
    m_cATU_R.AddSep ();
    m_cATU_R.AddConst ("[(]hw: annex");
    m_cATU_R.AddSep ();
    m_cATU_R.AddString (m_strTemp0);
    m_cATU_R.AddSep ();
    m_cATU_R.AddConst (", f/w: annex");
    m_cATU_R.AddSep ();
    m_cATU_R.AddString (m_strTemp1);
    m_cATU_R.AddSep ();
    m_cATU_R.AddConst ("[)] [-]*");
    m_cATU_R.Init ();
    AddCallback (m_cATU_R, &CMore::ATU_RCallback);

    m_cHEC.AddString (m_strTemp0);
    m_cHEC.AddSep ();
    m_cHEC.AddConst ("HECError");
    m_cHEC.AddSep ();
    m_cHEC.AddConst (":");
    m_cHEC.AddSep ();
    m_cHEC.AddInt (m_nTemp0);
    m_cHEC.AddSep ();
    m_cHEC.AddInt (m_nTemp1);
    m_cHEC.AddSep ();
    m_cHEC.Init ();
    AddCallback (m_cHEC, &CMore::HECCallback);

    m_cCRC.AddString (m_strTemp0);
    m_cCRC.AddSep ();
    m_cCRC.AddConst ("CRC");
    m_cCRC.AddSep ();
    m_cCRC.AddConst (":");
    m_cCRC.AddSep ();
    m_cCRC.AddInt (m_nTemp0);
    m_cCRC.AddSep ();
    m_cCRC.AddInt (m_nTemp1);
    m_cCRC.AddSep ();
    m_cCRC.Init ();
    AddCallback (m_cCRC, &CMore::CRCCallback);

    m_cFEC.AddString (m_strTemp0);
    m_cFEC.AddSep ();
    m_cFEC.AddConst ("NFEC");
    m_cFEC.AddSep ();
    m_cFEC.AddConst (":");
    m_cFEC.AddSep ();
    m_cFEC.AddInt (m_nTemp0);
    m_cFEC.AddSep ();
    m_cFEC.AddInt (m_nTemp1);
    m_cFEC.AddSep ();
    m_cFEC.Init ();
    AddCallback (m_cFEC, &CMore::FECCallback);

    AddCallback (m_cPrompt, &CMore::PromptCallback);

//begin user code
//end user code
}

CParser *CMore::ATU_RCallback (void)
{
    m_nState = eATU_R;
    LogTrace ("State ATU_R\n");

    return this;
}

CParser *CMore::HECCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetUp (m_nTemp1);
    LogTrace ("ErrorsHEC: %d  %d\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CMore::CRCCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetUp (m_nTemp1);
    LogTrace ("ErrorsCRC: %d  %d\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CMore::FECCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetDown (m_nTemp0);
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetUp (m_nTemp1);
    LogTrace ("ErrorsFEC: %d  %d\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CMore::PromptCallback (void)
{
    CParser *pNext = this;

    if (CModem::eInfo == m_pModem->Command ())
    {
        pNext = new CAtm (m_pModem, s_strCmdAtm);
    }
    else
    {
        pNext = new CExit (m_pModem, s_strCmdExit);
    }

    return pNext;
}

CAtm::CAtm (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_cVCI ()
, m_cVPI ()
{
    m_cVCI.AddConst ("vci=");
    m_cVCI.AddSep ();
    m_cVCI.AddInt (m_nTemp0);
    m_cVCI.Init ();
    AddCallback (m_cVCI, &CAtm::VCICallback);

    m_cVPI.AddConst ("vpi=");
    m_cVPI.AddSep ();
    m_cVPI.AddInt (m_nTemp0);
    m_cVPI.Init ();
    AddCallback (m_cVPI, &CAtm::VPICallback);

    AddCallback (m_cPrompt, &CAtm::PromptCallback);

//begin user code
//end user code
}

CParser *CAtm::VCICallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cVpiVci.SetDown (m_nTemp0);
    LogTrace ("VCI: %ld\n", m_nTemp0);

    return this;
}

CParser *CAtm::VPICallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cVpiVci.SetUp (m_nTemp0);
    LogTrace ("VPI: %ld\n", m_nTemp0);

    return this;
}

CParser *CAtm::PromptCallback (void)
{
    return new CVersion (m_pModem, s_strCmdVersion);
}

CVersion::CVersion (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_nTemp3 (0)
, m_strTemp0 ()
, m_strTemp1 ()
, m_cVersion4 ()
, m_cVersion3 ()
, m_cVersion2 ()
{
    m_cVersion4.AddConst ("Router Model");
    m_cVersion4.AddSep ();
    m_cVersion4.AddConst (":");
    m_cVersion4.AddSep ();
    m_cVersion4.AddString (m_strTemp0);
    m_cVersion4.AddSep ();
    m_cVersion4.AddConst ("Version");
    m_cVersion4.AddSep ();
    m_cVersion4.AddConst (":");
    m_cVersion4.AddSep ();
    m_cVersion4.AddInt (m_nTemp0);
    m_cVersion4.AddConst ("\\.");
    m_cVersion4.AddInt (m_nTemp1);
    m_cVersion4.AddConst ("\\.");
    m_cVersion4.AddInt (m_nTemp2);
    m_cVersion4.AddConst ("\\.");
    m_cVersion4.AddInt (m_nTemp3);
    m_cVersion4.AddConst ("_");
    m_cVersion4.AddString (m_strTemp1);
    m_cVersion4.Init ();
    AddCallback (m_cVersion4, &CVersion::Version4Callback);

    m_cVersion3.AddConst ("Router Model");
    m_cVersion3.AddSep ();
    m_cVersion3.AddConst (":");
    m_cVersion3.AddSep ();
    m_cVersion3.AddString (m_strTemp0);
    m_cVersion3.AddSep ();
    m_cVersion3.AddConst ("Version");
    m_cVersion3.AddSep ();
    m_cVersion3.AddConst (":");
    m_cVersion3.AddSep ();
    m_cVersion3.AddInt (m_nTemp0);
    m_cVersion3.AddConst ("\\.");
    m_cVersion3.AddInt (m_nTemp1);
    m_cVersion3.AddConst ("\\.");
    m_cVersion3.AddInt (m_nTemp2);
    m_cVersion3.AddConst ("_");
    m_cVersion3.AddString (m_strTemp1);
    m_cVersion3.Init ();
    AddCallback (m_cVersion3, &CVersion::Version3Callback);

    m_cVersion2.AddConst ("Router Model");
    m_cVersion2.AddSep ();
    m_cVersion2.AddConst (":");
    m_cVersion2.AddSep ();
    m_cVersion2.AddString (m_strTemp0);
    m_cVersion2.AddSep ();
    m_cVersion2.AddConst ("Version");
    m_cVersion2.AddSep ();
    m_cVersion2.AddConst (":");
    m_cVersion2.AddSep ();
    m_cVersion2.AddInt (m_nTemp0);
    m_cVersion2.AddConst ("\\.");
    m_cVersion2.AddInt (m_nTemp1);
    m_cVersion2.AddConst ("_");
    m_cVersion2.AddString (m_strTemp1);
    m_cVersion2.Init ();
    AddCallback (m_cVersion2, &CVersion::Version2Callback);

    AddCallback (m_cPrompt, &CVersion::PromptCallback);

//begin user code
//end user code
}

CParser *CVersion::Version4Callback (void)
{
    m_pModem->Version (CModem::BuildVersion (m_nTemp0, m_nTemp1, m_nTemp2, m_nTemp3));
    LogTrace ("Version: %ld.%ld.%ld.%ld\n", m_nTemp0, m_nTemp1, m_nTemp2, m_nTemp3);

    return this;
}

CParser *CVersion::Version3Callback (void)
{
    m_pModem->Version (CModem::BuildVersion (m_nTemp0, m_nTemp1, m_nTemp2));
    LogTrace ("Version: %ld.%ld.%ld\n", m_nTemp0, m_nTemp1, m_nTemp2);

    return this;
}

CParser *CVersion::Version2Callback (void)
{
    m_pModem->Version (CModem::BuildVersion (m_nTemp0, m_nTemp1));
    LogTrace ("Version: %ld.%ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CVersion::PromptCallback (void)
{
    CParser *pNext = this;

    if (m_pModem->Version () < CVigor::s_nVersionV3781)
    {
        pNext = new CExit (m_pModem, s_strCmdExit);
    }
    else
    {
        pNext = new CBitAllocDown (m_pModem, s_strCmdBitAllocDown);
    }

    return pNext;
}

CBitAllocDown::CBitAllocDown (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_bWorkaround (false)
, m_bToneValid (false)
, m_nTone0 (0)
, m_nTone1 (0)
, m_nTone2 (0)
, m_nTone3 (0)
, m_nSNR0 (0)
, m_nSNR1 (0)
, m_nSNR2 (0)
, m_nSNR3 (0)
, m_nGain0 (0)
, m_nGain1 (0)
, m_nGain2 (0)
, m_nGain3 (0)
, m_nBits0 (0)
, m_nBits1 (0)
, m_nBits2 (0)
, m_nBits3 (0)
, m_cToneBits ()
, m_cToneSNR ()
, m_cToneGain ()
, m_cDown ()
, m_cTone ()
{
    m_cToneBits.Size (m_pModem->Data ().Bandplan ()->Tones ());
    m_cToneSNR.Size (m_pModem->Data ().Bandplan ()->Tones ());
    m_cToneGain.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cDown.AddConst ("DOWNSTREAM");
    m_cDown.AddSep ();
    m_cDown.AddConst (":");
    m_cDown.Init ();
    AddCallback (m_cDown, &CBitAllocDown::DownCallback);

    m_cTone.AddInt (m_nTone0);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nSNR0);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nGain0);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nBits0);
    m_cTone.AddSep ();
    m_cTone.AddConst ("\\*");
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nTone1);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nSNR1);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nGain1);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nBits1);
    m_cTone.AddSep ();
    m_cTone.AddConst ("\\*");
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nTone2);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nSNR2);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nGain2);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nBits2);
    m_cTone.AddSep ();
    m_cTone.AddConst ("\\*");
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nTone3);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nSNR3);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nGain3);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nBits3);
    m_cTone.Init ();
    AddCallback (m_cTone, &CBitAllocDown::ToneCallback);

    AddCallback (m_cPrompt, &CBitAllocDown::PromptCallback);

//begin user code
    m_bWorkaround = m_pModem->Data ().Bandplan ()->Tones () > 512;
//end user code
}

CParser *CBitAllocDown::DownCallback (void)
{
    LogTrace ("Downstream\n");

    return this;
}

CParser *CBitAllocDown::ToneCallback (void)
{
    m_bToneValid = true;
//begin user code
    m_nSNR0 = (m_nSNR0 < 0) ? 0 : m_nSNR0;
    m_cToneBits.Set ((size_t)m_nTone0, (int)m_nBits0);
    m_nGain0 = (m_nGain0 < 0) ? 0 : m_nGain0/10;
    m_cToneGain.Set ((size_t)m_nTone0, (int)m_nGain0);
    if (m_bWorkaround)
    {
        for (size_t i = 0; i < 8; i++)
        {
            m_cToneSNR.Set ((size_t)(m_nTone0 * 8) + i, (int)m_nSNR0);
        }
    }
    else
    {
        m_cToneSNR.Set ((size_t)m_nTone0, (int)m_nSNR0);
    }

    m_nSNR1 = (m_nSNR1 < 0) ? 0 : m_nSNR1;
    m_cToneBits.Set ((size_t)m_nTone1, (int)m_nBits1);
    m_nGain1 = (m_nGain1 < 0) ? 0 : m_nGain1/10;
    m_cToneGain.Set ((size_t)m_nTone1, (int)m_nGain1);
    if (m_bWorkaround)
    {
        for (size_t i = 0; i < 8; i++)
        {
            m_cToneSNR.Set ((size_t)(m_nTone1 * 8) + i, (int)m_nSNR1);
        }
    }
    else
    {
        m_cToneSNR.Set ((size_t)m_nTone1, (int)m_nSNR1);
    }

    m_nSNR2 = (m_nSNR2 < 0) ? 0 : m_nSNR2;
    m_cToneBits.Set ((size_t)m_nTone2, (int)m_nBits2);
    m_nGain2 = (m_nGain2 < 0) ? 0 : m_nGain2/10;
    m_cToneGain.Set ((size_t)m_nTone2, (int)m_nGain2);
    if (m_bWorkaround)
    {
        for (size_t i = 0; i < 8; i++)
        {
            m_cToneSNR.Set ((size_t)(m_nTone2 * 8) + i, (int)m_nSNR2);
        }
    }
    else
    {
        m_cToneSNR.Set ((size_t)m_nTone2, (int)m_nSNR2);
    }

    m_nSNR3 = (m_nSNR3 < 0) ? 0 : m_nSNR3;
    m_cToneBits.Set ((size_t)m_nTone3, (int)m_nBits3);
    m_nGain3 = (m_nGain3 < 0) ? 0 : m_nGain3/10;
    m_cToneGain.Set ((size_t)m_nTone3, (int)m_nGain3);
    if (m_bWorkaround)
    {
        for (size_t i = 0; i < 8; i++)
        {
            m_cToneSNR.Set ((size_t)(m_nTone3 * 8) + i,(int) m_nSNR3);
        }
    }
    else
    {
        m_cToneSNR.Set ((size_t)m_nTone3, (int)m_nSNR3);
    }
//end user code
    LogTrace ("%ld Bits: %ld SNR: %ld\n", m_nTone0, m_nBits0, m_nSNR0);
    LogTrace ("%ld Bits: %ld SNR: %ld\n", m_nTone1, m_nBits1, m_nSNR1);
    LogTrace ("%ld Bits: %ld SNR: %ld\n", m_nTone2, m_nBits2, m_nSNR2);
    LogTrace ("%ld Bits: %ld SNR: %ld\n", m_nTone3, m_nBits3, m_nSNR3);

    return this;
}

CParser *CBitAllocDown::PromptCallback (void)
{
//begin user code
    if (m_bToneValid)
    {
        m_pModem->Data ().m_cData.m_cTones.m_cBitallocDown.Set (m_cToneBits);
        m_pModem->Data ().m_cData.m_cTones.m_cSNR.Set (m_cToneSNR);
        m_pModem->Data ().m_cData.m_cTones.m_cGainQ2.Set (m_cToneGain);
    }
//end user code

    return new CBitAllocUp (m_pModem, s_strCmdBitAllocUp);
}

CBitAllocUp::CBitAllocUp (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_bWorkaround (false)
, m_bToneValid (false)
, m_nTone0 (0)
, m_nTone1 (0)
, m_nTone2 (0)
, m_nTone3 (0)
, m_nSNR0 (0)
, m_nSNR1 (0)
, m_nSNR2 (0)
, m_nSNR3 (0)
, m_nGain0 (0)
, m_nGain1 (0)
, m_nGain2 (0)
, m_nGain3 (0)
, m_nBits0 (0)
, m_nBits1 (0)
, m_nBits2 (0)
, m_nBits3 (0)
, m_cToneBits ()
, m_cToneSNR ()
, m_cToneGain ()
, m_cUp ()
, m_cTone ()
{
    m_cToneBits.Size (m_pModem->Data ().Bandplan ()->Tones ());
    m_cToneSNR.Size (m_pModem->Data ().Bandplan ()->Tones ());
    m_cToneGain.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cUp.AddConst ("UPSTREAM");
    m_cUp.AddSep ();
    m_cUp.AddConst (":");
    m_cUp.Init ();
    AddCallback (m_cUp, &CBitAllocUp::UpCallback);

    m_cTone.AddInt (m_nTone0);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nSNR0);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nGain0);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nBits0);
    m_cTone.AddSep ();
    m_cTone.AddConst ("\\*");
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nTone1);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nSNR1);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nGain1);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nBits1);
    m_cTone.AddSep ();
    m_cTone.AddConst ("\\*");
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nTone2);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nSNR2);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nGain2);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nBits2);
    m_cTone.AddSep ();
    m_cTone.AddConst ("\\*");
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nTone3);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nSNR3);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nGain3);
    m_cTone.AddSep ();
    m_cTone.AddInt (m_nBits3);
    m_cTone.Init ();
    AddCallback (m_cTone, &CBitAllocUp::ToneCallback);

    AddCallback (m_cPrompt, &CBitAllocUp::PromptCallback);

//begin user code
    m_bWorkaround = m_pModem->Data ().Bandplan ()->Tones () > 512;
//end user code
}

CParser *CBitAllocUp::UpCallback (void)
{
    LogTrace ("Upstream\n");

    return this;
}

CParser *CBitAllocUp::ToneCallback (void)
{
    m_bToneValid = true;
//begin user code
    m_nSNR0 = (m_nSNR0 < 0) ? 0 : m_nSNR0;
    m_cToneBits.Set ((size_t)m_nTone0, (int)m_nBits0);
    m_nGain0 = (m_nGain0 < 0) ? 0 : m_nGain0/10;
    m_cToneGain.Set ((size_t)m_nTone0, (int)m_nGain0);
    if (m_bWorkaround)
    {
        for (size_t i = 0; i < 8; i++)
        {
            m_cToneSNR.Set ((size_t)(m_nTone0 * 8) + i, (int)m_nSNR0);
        }
    }
    else
    {
        m_cToneSNR.Set ((size_t)m_nTone0, (int)m_nSNR0);
    }

    m_nSNR1 = (m_nSNR1 < 0) ? 0 : m_nSNR1;
    m_cToneBits.Set ((size_t)m_nTone1, (int)m_nBits1);
    m_nGain1 = (m_nGain1 < 0) ? 0 : m_nGain1/10;
    m_cToneGain.Set ((size_t)m_nTone1, (int)m_nGain1);
    if (m_bWorkaround)
    {
        for (size_t i = 0; i < 8; i++)
        {
            m_cToneSNR.Set ((size_t)(m_nTone1 * 8) + i, (int)m_nSNR1);
        }
    }
    else
    {
        m_cToneSNR.Set ((size_t)m_nTone1, (int)m_nSNR1);
    }

    m_nSNR2 = (m_nSNR2 < 0) ? 0 : m_nSNR2;
    m_cToneBits.Set ((size_t)m_nTone2, (int)m_nBits2);
    m_nGain2 = (m_nGain2 < 0) ? 0 : m_nGain2/10;
    m_cToneGain.Set ((size_t)m_nTone2, (int)m_nGain2);
    if (m_bWorkaround)
    {
        for (size_t i = 0; i < 8; i++)
        {
            m_cToneSNR.Set ((size_t)(m_nTone2 * 8) + i, (int)m_nSNR2);
        }
    }
    else
    {
        m_cToneSNR.Set ((size_t)m_nTone2, (int)m_nSNR2);
    }

    m_nSNR3 = (m_nSNR3 < 0) ? 0 : m_nSNR3;
    m_cToneBits.Set ((size_t)m_nTone3, (int)m_nBits3);
    m_nGain3 = (m_nGain3 < 0) ? 0 : m_nGain3/10;
    m_cToneGain.Set ((size_t)m_nTone3, (int)m_nGain3);
    if (m_bWorkaround)
    {
        for (size_t i = 0; i < 8; i++)
        {
            m_cToneSNR.Set ((size_t)(m_nTone3 * 8) + i, (int)m_nSNR3);
        }
    }
    else
    {
        m_cToneSNR.Set((size_t)m_nTone3, (int)m_nSNR3);
    }
//end user code
    LogTrace ("%ld Bits: %ld SNR: %ld\n", m_nTone0, m_nBits0, m_nSNR0);
    LogTrace ("%ld Bits: %ld SNR: %ld\n", m_nTone1, m_nBits1, m_nSNR1);
    LogTrace ("%ld Bits: %ld SNR: %ld\n", m_nTone2, m_nBits2, m_nSNR2);
    LogTrace ("%ld Bits: %ld SNR: %ld\n", m_nTone3, m_nBits3, m_nSNR3);

    return this;
}

CParser *CBitAllocUp::PromptCallback (void)
{
//begin user code
    if (m_bToneValid)
    {
        m_pModem->Data ().m_cData.m_cTones.m_cBitallocUp.Set (m_cToneBits);
        m_pModem->Data ().m_cData.m_cTones.m_cSNR.Merge (m_cToneSNR);
    }
//end user code

    return new CExit (m_pModem, s_strCmdExit);
}

CResync::CResync (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CResync::PromptCallback);

//begin user code
//end user code
}

CParser *CResync::PromptCallback (void)
{
    return new CExit (m_pModem, s_strCmdExit);
}

CReboot::CReboot (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CReboot::PromptCallback);

//begin user code
//end user code
}

CParser *CReboot::PromptCallback (void)
{
    return NULL;
}

CExit::CExit (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CExit::PromptCallback);

//begin user code
//end user code
}

CParser *CExit::PromptCallback (void)
{
    return NULL;
}

//! @endcond
//_oOo_
