/*!
 * @file        conexant.cpp
 * @brief       conexant modem implementation
 * @details     parser for conexant based modems, tested with:
 * @n           Sphairon AR800
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        22.10.2015
 *
 * @note        This file is partly generated from ar7.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-conexant-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_CONEXANT_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
#include <math.h>
//end user code

#include "conexant.h"
#include "log.h"

//! @cond
using namespace NConexant;

static const char *s_strCmdMain2 = "m";
static const char *s_strCmdMain3 = "m";
static const char *s_strCmdStatus = "3";
static const char *s_strCmdGeneral = "1";
static const char *s_strCmdGeneralAdsl = "1";
static const char *s_strCmdStatus2 = "3";
static const char *s_strCmdAdsl = "2";
static const char *s_strCmdQuit = "q";
static const char *s_strCmdQuitYes = "y";

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CConexant (cData, pProtocol);
}

CConexant::CConexant (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
//end user code
{
//begin user code
//end user code
}

void CConexant::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("\x1b[24;04H");
    m_strLF.Format ("\r\n");

//begin user code
//end user code

    if (m_pLogin)
    {
        pFirst = new CLoginPass (this, Pass ());
    }
    else if (CModem::eInfo == eCommand)
    {
        pFirst = new CStatus (this, s_strCmdStatus);
    }
    else if (CModem::eStatus == eCommand)
    {
        pFirst = new CStatus (this, s_strCmdStatus);
    }
    else if (CModem::eResync == eCommand)
    {
        pFirst = new CQuit (this, s_strCmdQuit);
    }
    else if (CModem::eReboot == eCommand)
    {
        pFirst = new CQuit (this, s_strCmdQuit);
    }

    Protocol ()->Next (pFirst);
}

const CConexant::CBandplanData CConexant::CBandplanData::g_acList[] = {
//begin user code
        CBandplanData ("G.dmt", "ANNEX_A", CBandplan::eADSL_A),
        CBandplanData ("G.dmt", "ANNEX_B", CBandplan::eADSL_B)
//end user code
};

CConexant::CBandplanData::CBandplanData (const char *strVersion,
        const char *strAnnex, CBandplan::EBandplan eBandplan)
: m_strVersion (strVersion)
, m_strAnnex (strAnnex)
, m_eBandplan (eBandplan)
{
}

CConexant::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
: m_strVersion (cBandplanData.m_strVersion)
, m_strAnnex (cBandplanData.m_strAnnex)
, m_eBandplan (cBandplanData.m_eBandplan)
{
}

CBandplan::EBandplan CConexant::CBandplanData::Get (const char *strVersion,
        const char *strAnnex)
{
    CBandplan::EBandplan eBandplan = CBandplan::eNone;

    for (size_t nIndex = 0; nIndex < ARRAY_SIZE (g_acList); nIndex++)
    {
//begin user code
        if ((NULL != strVersion) && (NULL != strAnnex))
        {
            if ((0 == strcmp (strVersion, g_acList[nIndex].m_strVersion)) &&
                    (0 == strcmp (strAnnex, g_acList[nIndex].m_strAnnex)))
            {
                eBandplan = g_acList[nIndex].m_eBandplan;
                break;
            }
        }
//end user code
    }

    return eBandplan;
}

CBandplan::EBandplan CConexant::Bandplan (const char *strVersion,
        const char *strAnnex)
{
    return CBandplanData::Get(strVersion, strAnnex);
}

//begin user code
//end user code

CLoginPass::CLoginPass (CModem *pModem, const char *strPass)
: CLineParser (pModem)
, m_strPass (strPass)
{
}

CParser *CLoginPass::ParseLine (const char *strLine)
{
    CParser *pNext = this;

    if (Find (strLine, strlen (strLine), "\x1b[24;01HLOGON PASSWORD>"))
    {
        m_pModem->Protocol ()->Send (m_strPass, "pass");
        m_pModem->Protocol ()->Send (m_pModem->LF ());
    }
    else if (Find (strLine, strlen (strLine), m_pModem->Prompt ()))
    {
        if (CModem::eInfo == m_pModem->Command ())
        {
            pNext = new CStatus (m_pModem, s_strCmdStatus);
        }
        else if (CModem::eStatus == m_pModem->Command ())
        {
            pNext = new CStatus (m_pModem, s_strCmdStatus);
        }
        else if (CModem::eResync == m_pModem->Command ())
        {
            pNext = new CQuit (m_pModem, s_strCmdQuit);
        }
        else if (CModem::eReboot == m_pModem->Command ())
        {
            pNext = new CQuit (m_pModem, s_strCmdQuit);
        }
    }

    return pNext;
}

template <class T> CPromptParser<T>::CPromptParser (CModem *pModem, const char *strCmd)
: CRegExCmdParser<T, CLineParser> (pModem, strCmd)
, m_cPrompt ()
{
    m_cPrompt.AddTerm ("24;04H");
    m_cPrompt.Init ();
}

CStatus::CStatus (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CStatus::PromptCallback);

//begin user code
//end user code
}

CParser *CStatus::PromptCallback (void)
{
    return new CGeneral (m_pModem, s_strCmdGeneral);
}

CGeneral::CGeneral (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CGeneral::PromptCallback);

//begin user code
//end user code
}

CParser *CGeneral::PromptCallback (void)
{
    return new CGeneralAdsl (m_pModem, s_strCmdGeneralAdsl);
}

CGeneralAdsl::CGeneralAdsl (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_fTemp0 (0)
, m_cUpNoisemargin ()
, m_cUpNoisemarginDat ()
, m_cDownNoisemargin ()
, m_cDownNoisemarginDat ()
, m_cUpAttenuation ()
, m_cUpAttenuationDat ()
, m_cDownAttenuation ()
, m_cDownAttenuationDat ()
, m_cUpTxPower ()
, m_cUpTxPowerDat ()
, m_cUpCRC ()
, m_cUpCRCDat ()
, m_cDownCRC ()
, m_cDownCRCDat ()
, m_cUpFEC ()
, m_cUpFECDat ()
, m_cDownFEC ()
, m_cDownFECDat ()
, m_cUpHEC ()
, m_cUpHECDat ()
, m_cDownHEC ()
, m_cDownHECDat ()
{
    m_cUpNoisemargin.AddTerm ("07;45H");
    m_cUpNoisemargin.Init ();
    AddCallback (m_cUpNoisemargin, &CGeneralAdsl::UpNoisemarginCallback);

    m_cUpNoisemarginDat.AddTerm ("0K");
    m_cUpNoisemarginDat.AddDouble (m_fTemp0);
    m_cUpNoisemarginDat.AddSep ();
    m_cUpNoisemarginDat.AddConst ("dB");
    m_cUpNoisemarginDat.Init ();
    AddCallback (m_cUpNoisemarginDat, &CGeneralAdsl::UpNoisemarginDatCallback, eUpNoisemargin);

    m_cDownNoisemargin.AddTerm ("08;45H");
    m_cDownNoisemargin.Init ();
    AddCallback (m_cDownNoisemargin, &CGeneralAdsl::DownNoisemarginCallback);

    m_cDownNoisemarginDat.AddTerm ("0K");
    m_cDownNoisemarginDat.AddDouble (m_fTemp0);
    m_cDownNoisemarginDat.AddSep ();
    m_cDownNoisemarginDat.AddConst ("dB");
    m_cDownNoisemarginDat.Init ();
    AddCallback (m_cDownNoisemarginDat, &CGeneralAdsl::DownNoisemarginDatCallback, eDownNoisemargin);

    m_cUpAttenuation.AddTerm ("09;45H");
    m_cUpAttenuation.Init ();
    AddCallback (m_cUpAttenuation, &CGeneralAdsl::UpAttenuationCallback);

    m_cUpAttenuationDat.AddTerm ("0K");
    m_cUpAttenuationDat.AddDouble (m_fTemp0);
    m_cUpAttenuationDat.AddSep ();
    m_cUpAttenuationDat.AddConst ("dB");
    m_cUpAttenuationDat.Init ();
    AddCallback (m_cUpAttenuationDat, &CGeneralAdsl::UpAttenuationDatCallback, eUpAttenuation);

    m_cDownAttenuation.AddTerm ("10;45H");
    m_cDownAttenuation.Init ();
    AddCallback (m_cDownAttenuation, &CGeneralAdsl::DownAttenuationCallback);

    m_cDownAttenuationDat.AddTerm ("0K");
    m_cDownAttenuationDat.AddDouble (m_fTemp0);
    m_cDownAttenuationDat.AddSep ();
    m_cDownAttenuationDat.AddConst ("dB");
    m_cDownAttenuationDat.Init ();
    AddCallback (m_cDownAttenuationDat, &CGeneralAdsl::DownAttenuationDatCallback, eDownAttenuation);

    m_cUpTxPower.AddTerm ("11;45H");
    m_cUpTxPower.Init ();
    AddCallback (m_cUpTxPower, &CGeneralAdsl::UpTxPowerCallback);

    m_cUpTxPowerDat.AddTerm ("0K");
    m_cUpTxPowerDat.AddDouble (m_fTemp0);
    m_cUpTxPowerDat.AddSep ();
    m_cUpTxPowerDat.AddConst ("dbm/Hz");
    m_cUpTxPowerDat.Init ();
    AddCallback (m_cUpTxPowerDat, &CGeneralAdsl::UpTxPowerDatCallback, eUpTxPower);

    m_cUpCRC.AddTerm ("15;45H");
    m_cUpCRC.Init ();
    AddCallback (m_cUpCRC, &CGeneralAdsl::UpCRCCallback);

    m_cUpCRCDat.AddTerm ("0K");
    m_cUpCRCDat.AddInt (m_nTemp0);
    m_cUpCRCDat.Init ();
    AddCallback (m_cUpCRCDat, &CGeneralAdsl::UpCRCDatCallback, eUpCRC);

    m_cDownCRC.AddTerm ("16;45H");
    m_cDownCRC.Init ();
    AddCallback (m_cDownCRC, &CGeneralAdsl::DownCRCCallback);

    m_cDownCRCDat.AddTerm ("0K");
    m_cDownCRCDat.AddInt (m_nTemp0);
    m_cDownCRCDat.Init ();
    AddCallback (m_cDownCRCDat, &CGeneralAdsl::DownCRCDatCallback, eDownCRC);

    m_cUpFEC.AddTerm ("17;45H");
    m_cUpFEC.Init ();
    AddCallback (m_cUpFEC, &CGeneralAdsl::UpFECCallback);

    m_cUpFECDat.AddTerm ("0K");
    m_cUpFECDat.AddInt (m_nTemp0);
    m_cUpFECDat.Init ();
    AddCallback (m_cUpFECDat, &CGeneralAdsl::UpFECDatCallback, eUpFEC);

    m_cDownFEC.AddTerm ("18;45H");
    m_cDownFEC.Init ();
    AddCallback (m_cDownFEC, &CGeneralAdsl::DownFECCallback);

    m_cDownFECDat.AddTerm ("0K");
    m_cDownFECDat.AddInt (m_nTemp0);
    m_cDownFECDat.Init ();
    AddCallback (m_cDownFECDat, &CGeneralAdsl::DownFECDatCallback, eDownFEC);

    m_cUpHEC.AddTerm ("19;45H");
    m_cUpHEC.Init ();
    AddCallback (m_cUpHEC, &CGeneralAdsl::UpHECCallback);

    m_cUpHECDat.AddTerm ("0K");
    m_cUpHECDat.AddInt (m_nTemp0);
    m_cUpHECDat.Init ();
    AddCallback (m_cUpHECDat, &CGeneralAdsl::UpHECDatCallback, eUpHEC);

    m_cDownHEC.AddTerm ("20;45H");
    m_cDownHEC.Init ();
    AddCallback (m_cDownHEC, &CGeneralAdsl::DownHECCallback);

    m_cDownHECDat.AddTerm ("0K");
    m_cDownHECDat.AddInt (m_nTemp0);
    m_cDownHECDat.Init ();
    AddCallback (m_cDownHECDat, &CGeneralAdsl::DownHECDatCallback, eDownHEC);

    AddCallback (m_cPrompt, &CGeneralAdsl::PromptCallback);

//begin user code
//end user code
}

CParser *CGeneralAdsl::UpNoisemarginCallback (void)
{
    m_nState = eUpNoisemargin;
    LogTrace ("State: UpNoisemargin\n");

    return this;
}

CParser *CGeneralAdsl::UpNoisemarginDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cNoiseMargin.SetUp (0, m_fTemp0);
    LogTrace ("Noisemargin Up: %lf\n", m_fTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::DownNoisemarginCallback (void)
{
    m_nState = eDownNoisemargin;
    LogTrace ("State: DownNoisemargin\n");

    return this;
}

CParser *CGeneralAdsl::DownNoisemarginDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cNoiseMargin.SetDown (0, m_fTemp0);
    LogTrace ("Noisemargin Down: %lf\n", m_fTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::UpAttenuationCallback (void)
{
    m_nState = eUpAttenuation;
    LogTrace ("State: UpAttenuation\n");

    return this;
}

CParser *CGeneralAdsl::UpAttenuationDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.SetUp (0, m_fTemp0);
    LogTrace ("Attenuation Up: %lf\n", m_fTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::DownAttenuationCallback (void)
{
    m_nState = eDownAttenuation;
    LogTrace ("State: DownAttenuation\n");

    return this;
}

CParser *CGeneralAdsl::DownAttenuationDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.SetDown (0, m_fTemp0);
    LogTrace ("Attenuation Down: %lf\n", m_fTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::UpTxPowerCallback (void)
{
    m_nState = eUpTxPower;
    LogTrace ("State: UpTxPower\n");

    return this;
}

CParser *CGeneralAdsl::UpTxPowerDatCallback (void)
{
//begin user code
    m_fTemp0 = fabs (m_fTemp0);
    m_pModem->Data ().m_cData.m_cTxPower.SetDown (0.0);
//end user code
    m_pModem->Data ().m_cData.m_cTxPower.SetUp (m_fTemp0);
    LogTrace ("TxPower Up: %lf\n", m_fTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::UpCRCCallback (void)
{
    m_nState = eUpCRC;
    LogTrace ("State: UpCRC\n");

    return this;
}

CParser *CGeneralAdsl::UpCRCDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetUp (m_nTemp0);
    LogTrace ("CRC Up: %ld\n", m_nTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::DownCRCCallback (void)
{
    m_nState = eDownCRC;
    LogTrace ("State: DownCRC\n");

    return this;
}

CParser *CGeneralAdsl::DownCRCDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (m_nTemp0);
    LogTrace ("CRC Down: %ld\n", m_nTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::UpFECCallback (void)
{
    m_nState = eUpFEC;
    LogTrace ("State: UpFEC\n");

    return this;
}

CParser *CGeneralAdsl::UpFECDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetUp (m_nTemp0);
    LogTrace ("FEC Up: %ld\n", m_nTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::DownFECCallback (void)
{
    m_nState = eDownFEC;
    LogTrace ("State: DownFEC\n");

    return this;
}

CParser *CGeneralAdsl::DownFECDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.SetDown (m_nTemp0);
    LogTrace ("FEC Down: %ld\n", m_nTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::UpHECCallback (void)
{
    m_nState = eUpHEC;
    LogTrace ("State: UpHEC\n");

    return this;
}

CParser *CGeneralAdsl::UpHECDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetUp (m_nTemp0);
    LogTrace ("HEC Up: %ld\n", m_nTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::DownHECCallback (void)
{
    m_nState = eDownHEC;
    LogTrace ("State: DownHEC\n");

    return this;
}

CParser *CGeneralAdsl::DownHECDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.SetDown (m_nTemp0);
    LogTrace ("HEC Down: %ld\n", m_nTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CGeneralAdsl::PromptCallback (void)
{
    return new CMain2 (m_pModem, s_strCmdMain2);
}

CMain2::CMain2 (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CMain2::PromptCallback);

//begin user code
//end user code
}

CParser *CMain2::PromptCallback (void)
{
    return new CStatus2 (m_pModem, s_strCmdStatus2);
}

CStatus2::CStatus2 (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CStatus2::PromptCallback);

//begin user code
//end user code
}

CParser *CStatus2::PromptCallback (void)
{
    return new CAdsl (m_pModem, s_strCmdAdsl);
}

CAdsl::CAdsl (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_strTemp0 ()
, m_strOpVersion ()
, m_strOpAnnex ()
, m_bOpVersion (false)
, m_bOpAnnex (false)
, m_cOpVersion ()
, m_cOpVersionDat ()
, m_cOpAnnex ()
, m_cOpAnnexDat ()
, m_cModemState ()
, m_cModemStateDat ()
, m_cUpRate ()
, m_cUpRateDat ()
, m_cDownRate ()
, m_cDownRateDat ()
{
    m_cOpVersion.AddTerm ("05;50H");
    m_cOpVersion.Init ();
    AddCallback (m_cOpVersion, &CAdsl::OpVersionCallback);

    m_cOpVersionDat.AddTerm ("0K");
    m_cOpVersionDat.AddString (m_strOpVersion);
    m_cOpVersionDat.Init ();
    AddCallback (m_cOpVersionDat, &CAdsl::OpVersionDatCallback, eOpVersion);

    m_cOpAnnex.AddTerm ("06;50H");
    m_cOpAnnex.Init ();
    AddCallback (m_cOpAnnex, &CAdsl::OpAnnexCallback);

    m_cOpAnnexDat.AddTerm ("0K");
    m_cOpAnnexDat.AddString (m_strOpAnnex);
    m_cOpAnnexDat.Init ();
    AddCallback (m_cOpAnnexDat, &CAdsl::OpAnnexDatCallback, eOpAnnex);

    m_cModemState.AddTerm ("07;50H");
    m_cModemState.Init ();
    AddCallback (m_cModemState, &CAdsl::ModemStateCallback);

    m_cModemStateDat.AddTerm ("0K");
    m_cModemStateDat.AddString (m_strTemp0);
    m_cModemStateDat.Init ();
    AddCallback (m_cModemStateDat, &CAdsl::ModemStateDatCallback, eModemState);

    m_cUpRate.AddTerm ("17;50H");
    m_cUpRate.Init ();
    AddCallback (m_cUpRate, &CAdsl::UpRateCallback);

    m_cUpRateDat.AddTerm ("0K");
    m_cUpRateDat.AddInt (m_nTemp0);
    m_cUpRateDat.Init ();
    AddCallback (m_cUpRateDat, &CAdsl::UpRateDatCallback, eUpRate);

    m_cDownRate.AddTerm ("18;50H");
    m_cDownRate.Init ();
    AddCallback (m_cDownRate, &CAdsl::DownRateCallback);

    m_cDownRateDat.AddTerm ("0K");
    m_cDownRateDat.AddInt (m_nTemp0);
    m_cDownRateDat.Init ();
    AddCallback (m_cDownRateDat, &CAdsl::DownRateDatCallback, eDownRate);

    AddCallback (m_cPrompt, &CAdsl::PromptCallback);

//begin user code
//end user code
}

CParser *CAdsl::OpVersionCallback (void)
{
    m_nState = eOpVersion;
    LogTrace ("State: OpVersion\n");

    return this;
}

CParser *CAdsl::OpVersionDatCallback (void)
{
    m_bOpVersion = true;
    LogTrace ("OpVersion: %s\n", (const char*)m_strOpVersion);
    m_nState = eUnknown;

    return this;
}

CParser *CAdsl::OpAnnexCallback (void)
{
    m_nState = eOpAnnex;
    LogTrace ("State: OpAnnex\n");

    return this;
}

CParser *CAdsl::OpAnnexDatCallback (void)
{
    m_bOpAnnex = true;
    LogTrace ("OpAnnex: %s\n", (const char*)m_strOpAnnex);
    m_nState = eUnknown;

    return this;
}

CParser *CAdsl::ModemStateCallback (void)
{
    m_nState = eModemState;
    LogTrace ("State: ModemState\n");

    return this;
}

CParser *CAdsl::ModemStateDatCallback (void)
{
//begin user code
    int nModemState = 0;
    if (0 == strncasecmp (m_strTemp0, "SHOWTIME", 8))
    {
        nModemState = 1;
    }
    m_pModem->Data ().m_cData.m_cModemState.Set (nModemState);
//end user code
    m_pModem->Data ().m_cData.m_cModemStateStr.Set (m_strTemp0);
    LogTrace ("ModemState: %s\n", (const char*)m_strTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CAdsl::UpRateCallback (void)
{
    m_nState = eUpRate;
    LogTrace ("State: UpRate\n");

    return this;
}

CParser *CAdsl::UpRateDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetUp (m_nTemp0);
    LogTrace ("Bandwidth Up: %ld kbit/s\n", m_nTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CAdsl::DownRateCallback (void)
{
    m_nState = eDownRate;
    LogTrace ("State: DownRate\n");

    return this;
}

CParser *CAdsl::DownRateDatCallback (void)
{
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.SetDown (m_nTemp0);
    LogTrace ("Bandwidth Down: %ld kbit/s\n", m_nTemp0);
    m_nState = eUnknown;

    return this;
}

CParser *CAdsl::PromptCallback (void)
{
//begin user code
    if (m_bOpVersion && m_bOpAnnex)
    {
        CString strOperationMode;

        m_pModem->Data ().Bandplan (CConexant::Bandplan (m_strOpVersion, m_strOpAnnex));
        m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
        m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);

        LogTrace ("OperationMode: %s\n", (const char*)strOperationMode);
    }
//end user code

    return new CMain3 (m_pModem, s_strCmdMain3);
}

CMain3::CMain3 (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CMain3::PromptCallback);

//begin user code
//end user code
}

CParser *CMain3::PromptCallback (void)
{
    return new CQuit (m_pModem, s_strCmdQuit);
}

CQuit::CQuit (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CQuit::PromptCallback);

//begin user code
//end user code
}

CParser *CQuit::PromptCallback (void)
{
    return new CQuitYes (m_pModem, s_strCmdQuitYes);
}

CQuitYes::CQuitYes (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CQuitYes::PromptCallback);

//begin user code
//end user code
}

CParser *CQuitYes::PromptCallback (void)
{
    return NULL;
}

//! @endcond
//_oOo_
