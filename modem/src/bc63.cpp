/*!
 * @file        bc63.cpp
 * @brief       bc63 modem implementation
 * @details     parser for broadcom bc63xx based modems, tested with:
 * @n           D-Link DSL-321B HW Ver. Dx (not Zx!)
 * @n           Zyxel VMG1312-B30A and VMG1312-B10A
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from bc63.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-bc63-export.h"
#define LIBDSLTOOL_MODEM_EXPORT LIBDSLTOOL_BC63_EXPORT
//! @endcond

#include "config.h"

#include <string.h>

//begin user code
#include <math.h>
//end user code

#include "bc63.h"
#include "log.h"

//! @cond
using namespace NBc63;

static const char *s_strCmdInfoStats = "adsl info --stats";
static const char *s_strCmdInfoVendor = "adsl info --vendor";
static const char *s_strCmdAtmVcc = "atm vcc --show";
static const char *s_strCmdInfoBits = "adsl info --Bits";
static const char *s_strCmdInfoSNR = "adsl info --SNR";
static const char *s_strCmdReboot = "reboot";
static const char *s_strCmdExit = "logout";

void LIBDSLTOOL_MODEM_EXPORT ModemVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

CModem LIBDSLTOOL_MODEM_EXPORT *ModemCreate (CDslData &cData, CProtocol *pProtocol)
{
    return new CBc63 (cData, pProtocol);
}

CBc63::CBc63 (CDslData &cData,
        CProtocol *pProtocol)
: CModem (cData, pProtocol)
//begin user code
//end user code
{
//begin user code
//end user code
}

void CBc63::Init (ECommand eCommand)
{
    CParser *pFirst = NULL;

    CModem::Init (eCommand);

    m_strPrompt.Format ("> ");
    m_strLF.Format ("\r");

//begin user code
//end user code

    if (m_pLogin)
    {
        pFirst = new CLoginUser (this, User ());
    }
    else if (CModem::eInfo == eCommand)
    {
        pFirst = new CInfoStats (this, s_strCmdInfoStats);
    }
    else if (CModem::eStatus == eCommand)
    {
        pFirst = new CInfoStats (this, s_strCmdInfoStats);
    }
    else if (CModem::eResync == eCommand)
    {
        pFirst = new CReboot (this, s_strCmdReboot);
    }
    else if (CModem::eReboot == eCommand)
    {
        pFirst = new CReboot (this, s_strCmdReboot);
    }

    Protocol ()->Next (pFirst);
}

const CBc63::CBandplanData CBc63::CBandplanData::g_acList[] = {
//begin user code
        CBandplanData ("ADSL2", "A", NULL, CBandplan::eADSL2_A),
        CBandplanData ("ADSL2", "B", NULL, CBandplan::eADSL2_B),
        CBandplanData ("ADSL2", "J", NULL, CBandplan::eADSL2_J),
        CBandplanData ("ADSL2", "M", NULL, CBandplan::eADSL2_M),
        CBandplanData ("ADSL2+", "A", NULL, CBandplan::eADSL2p_A),
        CBandplanData ("ADSL2+", "B", NULL, CBandplan::eADSL2p_B),
        CBandplanData ("ADSL2+", "J", NULL, CBandplan::eADSL2p_J),
        CBandplanData ("ADSL2+", "M", NULL, CBandplan::eADSL2p_M),
        CBandplanData ("VDSL2", "B", "8b", CBandplan::eVDSL2_B8_6_8b),
        CBandplanData ("VDSL2", "B", "17a", CBandplan::eVDSL2_B8_12_17a),
        CBandplanData ("VDSL2", "B", "35b", CBandplan::eVDSL2_B8_21_35b)
//end user code
};

CBc63::CBandplanData::CBandplanData (const char *strMode,
        const char *strAnnex,
        const char *strProfile, CBandplan::EBandplan eBandplan)
: m_strMode (strMode)
, m_strAnnex (strAnnex)
, m_strProfile (strProfile)
, m_eBandplan (eBandplan)
{
}

CBc63::CBandplanData::CBandplanData (const CBandplanData &cBandplanData)
: m_strMode (cBandplanData.m_strMode)
, m_strAnnex (cBandplanData.m_strAnnex)
, m_strProfile (cBandplanData.m_strProfile)
, m_eBandplan (cBandplanData.m_eBandplan)
{
}

CBandplan::EBandplan CBc63::CBandplanData::Get (const char *strMode,
        const char *strAnnex,
        const char *strProfile)
{
    CBandplan::EBandplan eBandplan = CBandplan::eNone;

    for (size_t nIndex = 0; nIndex < ARRAY_SIZE (g_acList); nIndex++)
    {
//begin user code
        if ((NULL != strMode) && (NULL != strAnnex))
        {
            if (NULL == strProfile)
            {
                if ((0 == strcmp (strMode, g_acList[nIndex].m_strMode)) &&
                    (0 == strcmp (strAnnex, g_acList[nIndex].m_strAnnex)) &&
                    (NULL == g_acList[nIndex].m_strProfile))
                {
                    eBandplan = g_acList[nIndex].m_eBandplan;
                    break;
                }
            }
            else
            {
                if ((0 == strcmp (strMode, g_acList[nIndex].m_strMode)) &&
                    (0 == strcmp (strAnnex, g_acList[nIndex].m_strAnnex)) &&
                    (0 == strcmp (strProfile, g_acList[nIndex].m_strProfile)))
                {
                    eBandplan = g_acList[nIndex].m_eBandplan;
                    break;
                }
            }
        }
//end user code
    }

    return eBandplan;
}

CBandplan::EBandplan CBc63::Bandplan (const char *strMode,
        const char *strAnnex,
        const char *strProfile)
{
    return CBandplanData::Get(strMode, strAnnex, strProfile);
}

//begin user code
//end user code

CLoginUser::CLoginUser (CModem *pModem, const char *strUser)
: CParser (pModem)
, m_strUser (strUser)
{
}

CParser *CLoginUser::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Login: "))
    {
        m_pModem->Protocol ()->Send (m_strUser, "user");
        m_pModem->Protocol ()->Send (m_pModem->LF ());

        pNext = new CLoginPass (m_pModem, m_pModem->Pass ());
    }

    return pNext;
}

CLoginPass::CLoginPass (CModem *pModem, const char *strPass)
: CParser (pModem)
, m_strPass (strPass)
{
}

CParser *CLoginPass::Parse (const char *strRecv, size_t nRecv)
{
    CParser *pNext = this;

    if (Find (strRecv, nRecv, "Password: "))
    {
        m_pModem->Protocol ()->Send (m_strPass, "pass");
        m_pModem->Protocol ()->Send (m_pModem->LF ());
    }
    else if (Find (strRecv, nRecv, m_pModem->Prompt ()))
    {
        if (CModem::eInfo == m_pModem->Command ())
        {
            pNext = new CInfoStats (m_pModem, s_strCmdInfoStats);
        }
        else if (CModem::eStatus == m_pModem->Command ())
        {
            pNext = new CInfoStats (m_pModem, s_strCmdInfoStats);
        }
        else if (CModem::eResync == m_pModem->Command ())
        {
            pNext = new CReboot (m_pModem, s_strCmdReboot);
        }
        else if (CModem::eReboot == m_pModem->Command ())
        {
            pNext = new CReboot (m_pModem, s_strCmdReboot);
        }
    }

    return pNext;
}

template <class T> CPromptParser<T>::CPromptParser (CModem *pModem, const char *strCmd)
: CRegExCmdParser<T, CLineParser> (pModem, strCmd)
, m_cPrompt ()
{
    m_cPrompt.AddConst ("> ");
    m_cPrompt.Init ();
}

CInfoStats::CInfoStats (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_nTemp3 (0)
, m_fTemp0 (0)
, m_fTemp1 (0)
, m_strTemp0 ()
, m_strTemp1 ()
, m_strTemp2 ()
, m_bModeValid (false)
, m_bAnnexValid (false)
, m_bProfileValid (false)
, m_strMode ()
, m_strAnnex ()
, m_strProfile ()
, m_cModemState ()
, m_cAdslRate ()
, m_cVdslRate ()
, m_cVdslMax ()
, m_cOperationModeAnnex ()
, m_cOperationMode ()
, m_cAdslChannelMode ()
, m_cVdslProfile ()
, m_cNoisemargin ()
, m_cAttenuation ()
, m_cPower ()
, m_cTotal ()
, m_cLastDay ()
, m_cLast15min ()
, m_cPrevDay ()
, m_cPrev15min ()
, m_cLinkUp ()
, m_cErrorsHEC ()
, m_cErrorsFEC ()
, m_cErrorsCRC ()
, m_cErrorsCRCDown ()
, m_cDummySES ()
, m_cDummySESDown ()
, m_cErrorSec ()
, m_cErrorSecDown ()
{
    m_cModemState.AddConst ("Status");
    m_cModemState.AddSep ();
    m_cModemState.AddConst (":");
    m_cModemState.AddSep ();
    m_cModemState.AddString (m_strTemp0);
    m_cModemState.Init ();
    AddCallback (m_cModemState, &CInfoStats::ModemStateCallback);

    m_cAdslRate.AddConst ("Channel");
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddConst (":");
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddString (m_strTemp0);
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddConst ("Upstream rate");
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddConst ("=");
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddInt (m_nTemp0);
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddConst ("Kbps");
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddConst (",");
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddConst ("Downstream rate");
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddConst ("=");
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddInt (m_nTemp1);
    m_cAdslRate.AddSep ();
    m_cAdslRate.AddConst ("Kbps");
    m_cAdslRate.Init ();
    AddCallback (m_cAdslRate, &CInfoStats::AdslRateCallback);

    m_cVdslRate.AddConst ("Bearer");
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddConst (":");
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddString (m_strTemp0);
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddConst ("Upstream rate");
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddConst ("=");
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddInt (m_nTemp0);
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddConst ("Kbps");
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddConst (",");
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddConst ("Downstream rate");
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddConst ("=");
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddInt (m_nTemp1);
    m_cVdslRate.AddSep ();
    m_cVdslRate.AddConst ("Kbps");
    m_cVdslRate.Init ();
    AddCallback (m_cVdslRate, &CInfoStats::VdslRateCallback);

    m_cVdslMax.AddConst ("Max");
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddConst (":");
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddConst ("Upstream rate");
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddConst ("=");
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddInt (m_nTemp0);
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddConst ("Kbps");
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddConst (",");
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddConst ("Downstream rate");
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddConst ("=");
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddInt (m_nTemp1);
    m_cVdslMax.AddSep ();
    m_cVdslMax.AddConst ("Kbps");
    m_cVdslMax.Init ();
    AddCallback (m_cVdslMax, &CInfoStats::VdslMaxCallback);

    m_cOperationModeAnnex.AddConst ("Mode");
    m_cOperationModeAnnex.AddSep ();
    m_cOperationModeAnnex.AddConst (":");
    m_cOperationModeAnnex.AddSep ();
    m_cOperationModeAnnex.AddWord (m_strTemp0);
    m_cOperationModeAnnex.AddSep ();
    m_cOperationModeAnnex.AddConst ("Annex");
    m_cOperationModeAnnex.AddSep ();
    m_cOperationModeAnnex.AddWord (m_strTemp1);
    m_cOperationModeAnnex.AddSep ();
    m_cOperationModeAnnex.AddString (m_strTemp2);
    m_cOperationModeAnnex.Init ();
    AddCallback (m_cOperationModeAnnex, &CInfoStats::OperationModeAnnexCallback);

    m_cOperationMode.AddConst ("Mode");
    m_cOperationMode.AddSep ();
    m_cOperationMode.AddConst (":");
    m_cOperationMode.AddSep ();
    m_cOperationMode.AddWord (m_strTemp0);
    m_cOperationMode.Init ();
    AddCallback (m_cOperationMode, &CInfoStats::OperationModeCallback);

    m_cAdslChannelMode.AddConst ("Channel");
    m_cAdslChannelMode.AddSep ();
    m_cAdslChannelMode.AddConst (":");
    m_cAdslChannelMode.AddSep ();
    m_cAdslChannelMode.AddString (m_strTemp0);
    m_cAdslChannelMode.Init ();
    AddCallback (m_cAdslChannelMode, &CInfoStats::AdslChannelModeCallback);

    m_cVdslProfile.AddConst ("VDSL2 Profile");
    m_cVdslProfile.AddSep ();
    m_cVdslProfile.AddConst (":");
    m_cVdslProfile.AddSep ();
    m_cVdslProfile.AddConst ("Profile");
    m_cVdslProfile.AddSep ();
    m_cVdslProfile.AddString (m_strTemp0);
    m_cVdslProfile.Init ();
    AddCallback (m_cVdslProfile, &CInfoStats::VdslProfileCallback);

    m_cNoisemargin.AddConst ("SNR [(]dB[)]");
    m_cNoisemargin.AddSep ();
    m_cNoisemargin.AddConst (":");
    m_cNoisemargin.AddSep ();
    m_cNoisemargin.AddDouble (m_fTemp0);
    m_cNoisemargin.AddSep ();
    m_cNoisemargin.AddDouble (m_fTemp1);
    m_cNoisemargin.Init ();
    AddCallback (m_cNoisemargin, &CInfoStats::NoisemarginCallback);

    m_cAttenuation.AddConst ("Attn[(]dB[)]");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddConst (":");
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp0);
    m_cAttenuation.AddSep ();
    m_cAttenuation.AddDouble (m_fTemp1);
    m_cAttenuation.Init ();
    AddCallback (m_cAttenuation, &CInfoStats::AttenuationCallback);

    m_cPower.AddConst ("Pwr[(]dBm[)]");
    m_cPower.AddSep ();
    m_cPower.AddConst (":");
    m_cPower.AddSep ();
    m_cPower.AddDouble (m_fTemp0);
    m_cPower.AddSep ();
    m_cPower.AddDouble (m_fTemp1);
    m_cPower.Init ();
    AddCallback (m_cPower, &CInfoStats::PowerCallback);

    m_cTotal.AddConst ("Total time =");
    m_cTotal.AddSep ();
    m_cTotal.AddString (m_strTemp0);
    m_cTotal.Init ();
    AddCallback (m_cTotal, &CInfoStats::TotalCallback);

    m_cLastDay.AddConst ("Latest 1 day time =");
    m_cLastDay.AddSep ();
    m_cLastDay.AddString (m_strTemp0);
    m_cLastDay.Init ();
    AddCallback (m_cLastDay, &CInfoStats::LastDayCallback);

    m_cLast15min.AddConst ("Latest 15 minutes time =");
    m_cLast15min.AddSep ();
    m_cLast15min.AddString (m_strTemp0);
    m_cLast15min.Init ();
    AddCallback (m_cLast15min, &CInfoStats::Last15minCallback);

    m_cPrevDay.AddConst ("Previous 1 day time =");
    m_cPrevDay.AddSep ();
    m_cPrevDay.AddString (m_strTemp0);
    m_cPrevDay.Init ();
    AddCallback (m_cPrevDay, &CInfoStats::PrevDayCallback);

    m_cPrev15min.AddConst ("Previous 15 minutes time =");
    m_cPrev15min.AddSep ();
    m_cPrev15min.AddString (m_strTemp0);
    m_cPrev15min.Init ();
    AddCallback (m_cPrev15min, &CInfoStats::Prev15minCallback);

    m_cLinkUp.AddConst ("Since link time =");
    m_cLinkUp.AddSep ();
    m_cLinkUp.AddString (m_strTemp0);
    m_cLinkUp.Init ();
    AddCallback (m_cLinkUp, &CInfoStats::LinkUpCallback);

    m_cErrorsHEC.AddConst ("HEC");
    m_cErrorsHEC.AddSep ();
    m_cErrorsHEC.AddConst (":");
    m_cErrorsHEC.AddSep ();
    m_cErrorsHEC.AddInt (m_nTemp0);
    m_cErrorsHEC.AddSep ();
    m_cErrorsHEC.AddInt (m_nTemp1);
    m_cErrorsHEC.Init ();
    AddCallback (m_cErrorsHEC, &CInfoStats::ErrorsHECCallback);

    m_cErrorsFEC.AddConst ("FEC");
    m_cErrorsFEC.AddSep ();
    m_cErrorsFEC.AddConst (":");
    m_cErrorsFEC.AddSep ();
    m_cErrorsFEC.AddInt (m_nTemp0);
    m_cErrorsFEC.AddSep ();
    m_cErrorsFEC.AddInt (m_nTemp1);
    m_cErrorsFEC.Init ();
    AddCallback (m_cErrorsFEC, &CInfoStats::ErrorsFECCallback, eTotal);

    m_cErrorsCRC.AddConst ("CRC");
    m_cErrorsCRC.AddSep ();
    m_cErrorsCRC.AddConst (":");
    m_cErrorsCRC.AddSep ();
    m_cErrorsCRC.AddInt (m_nTemp0);
    m_cErrorsCRC.AddSep ();
    m_cErrorsCRC.AddInt (m_nTemp1);
    m_cErrorsCRC.Init ();
    AddCallback (m_cErrorsCRC, &CInfoStats::ErrorsCRCCallback, eTotal);

    m_cErrorsCRCDown.AddConst ("CRC");
    m_cErrorsCRCDown.AddSep ();
    m_cErrorsCRCDown.AddConst ("=");
    m_cErrorsCRCDown.AddSep ();
    m_cErrorsCRCDown.AddInt (m_nTemp0);
    m_cErrorsCRCDown.Init ();
    AddCallback (m_cErrorsCRCDown, &CInfoStats::ErrorsCRCDownCallback, eTotal);

    m_cDummySES.AddConst ("SES");
    m_cDummySES.AddSep ();
    m_cDummySES.AddConst (":");
    m_cDummySES.AddSep ();
    m_cDummySES.AddInt (m_nTemp0);
    m_cDummySES.AddSep ();
    m_cDummySES.AddInt (m_nTemp1);
    m_cDummySES.Init ();
    AddCallback (m_cDummySES, &CInfoStats::DummySESCallback);

    m_cDummySESDown.AddConst ("SES");
    m_cDummySESDown.AddSep ();
    m_cDummySESDown.AddConst ("=");
    m_cDummySESDown.AddSep ();
    m_cDummySESDown.AddInt (m_nTemp0);
    m_cDummySESDown.Init ();
    AddCallback (m_cDummySESDown, &CInfoStats::DummySESDownCallback);

    m_cErrorSec.AddConst ("ES");
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddConst (":");
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddInt (m_nTemp0);
    m_cErrorSec.AddSep ();
    m_cErrorSec.AddInt (m_nTemp1);
    m_cErrorSec.Init ();
    AddCallback (m_cErrorSec, &CInfoStats::ErrorSecLastDayCallback, eLastDay);
    AddCallback (m_cErrorSec, &CInfoStats::ErrorSecLast15minCallback, eLast15min);

    m_cErrorSecDown.AddConst ("ES");
    m_cErrorSecDown.AddSep ();
    m_cErrorSecDown.AddConst ("=");
    m_cErrorSecDown.AddSep ();
    m_cErrorSecDown.AddInt (m_nTemp0);
    m_cErrorSecDown.Init ();
    AddCallback (m_cErrorSecDown, &CInfoStats::ErrorSecDownLastDayCallback, eLastDay);
    AddCallback (m_cErrorSecDown, &CInfoStats::ErrorSecDownLast15minCallback, eLast15min);

    AddCallback (m_cPrompt, &CInfoStats::PromptCallback);

//begin user code
//end user code
}

CParser *CInfoStats::ModemStateCallback (void)
{
//begin user code
    int nModemState = 0;

    if (0 == strncasecmp (m_strTemp0, "Showtime", 8))
    {
        nModemState = 1;
    }
    m_pModem->Data ().m_cData.m_cModemState.Set (nModemState);
//end user code
    m_pModem->Data ().m_cData.m_cModemStateStr.Set (m_strTemp0);
    LogTrace ("ModemState: %s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CInfoStats::AdslRateCallback (void)
{
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.Set (m_nTemp0, m_nTemp1);
    LogTrace ("ADSL Rate Up: %ld Down: %ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CInfoStats::VdslRateCallback (void)
{
    m_pModem->Data ().m_cData.m_cBandwidth.m_cKbits.Set (m_nTemp0, m_nTemp1);
    LogTrace ("VDSL Rate Up: %ld Down: %ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CInfoStats::VdslMaxCallback (void)
{
    m_pModem->Data ().m_cData.m_cBandwidth.m_cMaxKbits.Set (m_nTemp0, m_nTemp1);
    LogTrace ("VDSL Max Up: %ld Down: %ld\n", m_nTemp0, m_nTemp1);

    return this;
}

CParser *CInfoStats::OperationModeAnnexCallback (void)
{
//begin user code
    m_strMode = m_strTemp0;
    m_strAnnex = m_strTemp1;
    m_bModeValid = true;
    m_bAnnexValid = true;
//end user code
    LogTrace ("OperationMode: %s Annex %s\n", (const char*)m_strTemp0, (const char*)m_strTemp1);

    return this;
}

CParser *CInfoStats::OperationModeCallback (void)
{
//begin user code
    m_strMode = m_strTemp0;
    m_bModeValid = true;
//end user code
    LogTrace ("OperationMode: %s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CInfoStats::AdslChannelModeCallback (void)
{
    m_pModem->Data ().m_cData.m_cChannelMode.Set (m_strTemp0);
    LogTrace ("ChannelMode\n", (const char*)m_strTemp0);

    return this;
}

CParser *CInfoStats::VdslProfileCallback (void)
{
//begin user code
    m_strProfile = m_strTemp0;
    m_bProfileValid = true;
//end user code
    LogTrace ("VDSL Profile %s\n", (const char*)m_strTemp0);

    return this;
}

CParser *CInfoStats::NoisemarginCallback (void)
{
    m_pModem->Data ().m_cData.m_cNoiseMargin.Set (0, m_fTemp1, m_fTemp0);
    LogTrace ("Noisemargin Up: %f Down: %f\n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CInfoStats::AttenuationCallback (void)
{
    m_pModem->Data ().m_cData.m_cAttenuation.Set (0, m_fTemp1, m_fTemp0);
    LogTrace ("Attenuation Up: %f Down: %f\n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CInfoStats::PowerCallback (void)
{
    m_pModem->Data ().m_cData.m_cTxPower.Set (m_fTemp1, m_fTemp0);
    LogTrace ("TxPower Up: %f Down: %f\n", m_fTemp1, m_fTemp0);

    return this;
}

CParser *CInfoStats::TotalCallback (void)
{
    m_nState = eTotal;
    LogTrace ("State Total\n");

    return this;
}

CParser *CInfoStats::LastDayCallback (void)
{
    m_nState = eLastDay;
    LogTrace ("State LastDay\n");

    return this;
}

CParser *CInfoStats::Last15minCallback (void)
{
    m_nState = eLast15min;
    LogTrace ("State Last15min\n");

    return this;
}

CParser *CInfoStats::PrevDayCallback (void)
{
    m_nState = ePrevDay;
    LogTrace ("State PrevDay\n");

    return this;
}

CParser *CInfoStats::Prev15minCallback (void)
{
    m_nState = ePrev15min;
    LogTrace ("State Prev15min\n");

    return this;
}

CParser *CInfoStats::LinkUpCallback (void)
{
    m_nState = eLinkUp;
    LogTrace ("State LinkUp\n");

    return this;
}

CParser *CInfoStats::ErrorsHECCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cHEC.Set (m_nTemp1, m_nTemp0);
    LogTrace ("ErrorsHEC Up: %ld Down: %ld\n", m_nTemp1, m_nTemp0);

    return this;
}

CParser *CInfoStats::ErrorsFECCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cFEC.Set (m_nTemp1, m_nTemp0);
    LogTrace ("ErrorsFEC Up: %ld Down: %ld\n", m_nTemp1, m_nTemp0);

    return this;
}

CParser *CInfoStats::ErrorsCRCCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.Set (m_nTemp1, m_nTemp0);
    LogTrace ("ErrorsCRC Up: %ld Down: %ld\n", m_nTemp1, m_nTemp0);

    return this;
}

CParser *CInfoStats::ErrorsCRCDownCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cError.m_cCRC.SetDown (m_nTemp0);
    LogTrace ("ErrorsCRC Down: %ld\n", m_nTemp0);

    return this;
}

CParser *CInfoStats::DummySESCallback (void)
{
//begin user code
//end user code

    return this;
}

CParser *CInfoStats::DummySESDownCallback (void)
{
//begin user code
//end user code

    return this;
}

CParser *CInfoStats::ErrorSecLastDayCallback (void)
{
//begin user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay.Set (m_nTemp0);
//end user code
    LogTrace ("FailErrorSecsDay: %ld\n", m_nTemp0);

    return this;
}

CParser *CInfoStats::ErrorSecLast15minCallback (void)
{
//begin user code
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min.Set (m_nTemp0);
//end user code
    LogTrace ("FailErrorSecs15min: %ld\n", m_nTemp0);

    return this;
}

CParser *CInfoStats::ErrorSecDownLastDayCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay.Set (m_nTemp0);
    LogTrace ("FailErrorSecsDay: %ld\n", m_nTemp0);

    return this;
}

CParser *CInfoStats::ErrorSecDownLast15minCallback (void)
{
    m_pModem->Data ().m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min.Set (m_nTemp0);
    LogTrace ("FailErrorSecs15min: %ld\n", m_nTemp0);

    return this;
}

CParser *CInfoStats::PromptCallback (void)
{
    CParser *pNext = this;
//begin user code
    if (m_bModeValid && m_bAnnexValid && m_bProfileValid)
    {
        CString strOperationMode, strProfile;
        m_pModem->Data ().Bandplan (CBc63::Bandplan (m_strMode, m_strAnnex, m_strProfile));
        m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
        m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);
        m_pModem->Data ().Bandplan ()->Profile (strProfile);
        m_pModem->Data ().m_cData.m_cProfile.Set (strProfile);
    }
    else if (m_bModeValid && m_bAnnexValid)
    {
        CString strOperationMode;
        m_pModem->Data ().Bandplan (CBc63::Bandplan (m_strMode, m_strAnnex, NULL));
        m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
        m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);
    }
    else if (m_bModeValid)
    {
        CString strOperationMode;
        m_pModem->Data ().Bandplan (CBc63::Bandplan (m_strMode, "B", NULL));
        m_pModem->Data ().Bandplan ()->OperationMode (strOperationMode);
        m_pModem->Data ().m_cData.m_cOperationMode.Set (strOperationMode);
    }
//end user code

    if (CModem::eInfo == m_pModem->Command ())
    {
        pNext = new CInfoVendor (m_pModem, s_strCmdInfoVendor);
    }
    else
    {
        pNext = new CExit (m_pModem, s_strCmdExit);
    }

    return pNext;
}

CInfoVendor::CInfoVendor (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_strTemp0 ()
, m_cVendor ()
{
    m_cVendor.AddConst ("ChipSet Vendor Id");
    m_cVendor.AddSep ();
    m_cVendor.AddConst (":");
    m_cVendor.AddSep ();
    m_cVendor.AddString (m_strTemp0);
    m_cVendor.AddSep ();
    m_cVendor.AddConst (":0x");
    m_cVendor.AddSep ();
    m_cVendor.AddInt (m_nTemp0, 16);
    m_cVendor.Init ();
    AddCallback (m_cVendor, &CInfoVendor::VendorCallback);

    AddCallback (m_cPrompt, &CInfoVendor::PromptCallback);

//begin user code
//end user code
}

CParser *CInfoVendor::VendorCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cATU_C.SetVendor (m_strTemp0);
    LogTrace ("Vendor: %s 0x%04x\n", (const char*)m_strTemp0, m_nTemp0);

    return this;
}

CParser *CInfoVendor::PromptCallback (void)
{
    CParser *pNext = this;
//begin user code
    bool bAdsl = (0 == strncmp (m_pModem->Data ().Bandplan ()->Type(), "ADSL", 4));
//end user code

    if (bAdsl)
    {
        pNext = new CAtmVcc (m_pModem, s_strCmdAtmVcc);
    }
    else
    {
        pNext = new CInfoBits (m_pModem, s_strCmdInfoBits);
    }

    return pNext;
}

CAtmVcc::CAtmVcc (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_nTemp0 (0)
, m_nTemp1 (0)
, m_nTemp2 (0)
, m_strTemp0 ()
, m_cVcc ()
, m_cVpiVci ()
{
    m_cVcc.AddConst ("vcc");
    m_cVcc.AddSep ();
    m_cVcc.AddConst ("status");
    m_cVcc.AddSep ();
    m_cVcc.AddConst ("type");
    m_cVcc.AddSep ();
    m_cVcc.AddConst ("tdte_index");
    m_cVcc.AddSep ();
    m_cVcc.AddConst ("q_size");
    m_cVcc.AddSep ();
    m_cVcc.AddConst ("q_priority");
    m_cVcc.AddSep ();
    m_cVcc.AddConst ("encapsulation");
    m_cVcc.Init ();
    AddCallback (m_cVcc, &CAtmVcc::VccCallback);

    m_cVpiVci.AddInt (m_nTemp0);
    m_cVpiVci.AddConst ("\\.");
    m_cVpiVci.AddInt (m_nTemp1);
    m_cVpiVci.AddConst ("\\.");
    m_cVpiVci.AddInt (m_nTemp2);
    m_cVpiVci.AddSep ();
    m_cVpiVci.AddString (m_strTemp0);
    m_cVpiVci.Init ();
    AddCallback (m_cVpiVci, &CAtmVcc::VpiVciCallback, eVcc);

    AddCallback (m_cPrompt, &CAtmVcc::PromptCallback);

//begin user code
//end user code
}

CParser *CAtmVcc::VccCallback (void)
{
    m_nState = eVcc;
    LogTrace ("State Vcc\n");

    return this;
}

CParser *CAtmVcc::VpiVciCallback (void)
{
    m_pModem->Data ().m_cData.m_cAtm.m_cVpiVci.Set (m_nTemp1, m_nTemp2);
    LogTrace ("VPI: %ld VCI: %ld\n", m_nTemp1, m_nTemp2);

    return this;
}

CParser *CAtmVcc::PromptCallback (void)
{
    return new CInfoBits (m_pModem, s_strCmdInfoBits);
}

CInfoBits::CInfoBits (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_bToneValid (false)
, m_cTone ()
, m_cBitAlloc ()
, m_cTones ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cBitAlloc.AddConst ("Tone number");
    m_cBitAlloc.AddSep ();
    m_cBitAlloc.AddConst ("Bit Allocation");
    m_cBitAlloc.Init ();
    AddCallback (m_cBitAlloc, &CInfoBits::BitAllocCallback);

    m_cTones.AddToneIndex (m_cTone);
    m_cTones.AddSep ();
    m_cTones.AddTone (m_cTone);
    m_cTones.Init ();
    AddCallback (m_cTones, &CInfoBits::TonesCallback, eBitAlloc);

    AddCallback (m_cPrompt, &CInfoBits::PromptCallback);

//begin user code
//end user code
}

CParser *CInfoBits::BitAllocCallback (void)
{
    m_bToneValid = false;
    m_cTone.Init ();
    m_nState = eBitAlloc;
    LogTrace ("State BitAlloc\n");

    return this;
}

CParser *CInfoBits::TonesCallback (void)
{
    m_bToneValid = true;

    return this;
}

CParser *CInfoBits::PromptCallback (void)
{
    if (m_bToneValid)
    {
        CBandplan *pBand = m_pModem->Data ().Bandplan ();

        for (size_t nBand = 0; nBand < pBand->Bands (); nBand++)
        {
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocUp.Set (m_cTone,
                    (size_t)pBand->Band (nBand).MinUp (),
                    (size_t)pBand->Band (nBand).MaxUp ());
            m_pModem->Data ().m_cData.m_cTones.m_cBitallocDown.Set (m_cTone,
                    (size_t)pBand->Band (nBand).MinDown (),
                    (size_t)pBand->Band (nBand).MaxDown ());
        }
    }

    return new CInfoSNR (m_pModem, s_strCmdInfoSNR);
}

CInfoSNR::CInfoSNR (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
, m_fTemp0 (0)
, m_bToneValid (false)
, m_cTone ()
, m_cSNR ()
, m_cTones ()
{
    m_cTone.Size (m_pModem->Data ().Bandplan ()->Tones ());

    m_cSNR.AddConst ("Tone number");
    m_cSNR.AddSep ();
    m_cSNR.AddConst ("SNR");
    m_cSNR.Init ();
    AddCallback (m_cSNR, &CInfoSNR::SNRCallback);

    m_cTones.AddToneIndex (m_cTone);
    m_cTones.AddSep ();
    m_cTones.AddDouble (m_fTemp0);
    m_cTones.Init ();
    AddCallback (m_cTones, &CInfoSNR::TonesCallback, eSNR);

    AddCallback (m_cPrompt, &CInfoSNR::PromptCallback);

//begin user code
//end user code
}

CParser *CInfoSNR::SNRCallback (void)
{
    m_nState = eSNR;
    LogTrace ("State SNR\n");

    return this;
}

CParser *CInfoSNR::TonesCallback (void)
{
    m_bToneValid = true;
//begin user code
    m_cTone.SetInc ((int)floor (m_fTemp0 + 0.5));
//end user code

    return this;
}

CParser *CInfoSNR::PromptCallback (void)
{
    if (m_bToneValid)
    {
        m_pModem->Data ().m_cData.m_cTones.m_cSNR.Set (m_cTone);
    }

    return new CExit (m_pModem, s_strCmdExit);
}

CReboot::CReboot (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CReboot::PromptCallback);

//begin user code
//end user code
}

CParser *CReboot::PromptCallback (void)
{
    return NULL;
}

CExit::CExit (CModem *pModem, const char *strCmd)
: CPromptParser (pModem, strCmd)
{
    AddCallback (m_cPrompt, &CExit::PromptCallback);

//begin user code
//end user code
}

CParser *CExit::PromptCallback (void)
{
    return NULL;
}

//! @endcond
//_oOo_
