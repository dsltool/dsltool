/*!
 * @file        speedtouch.h
 * @brief       speedtouch modem definitions
 * @details     parser for ALCATEL/Thomson Speedtouch modems, tested with:
 * @n           Speedtouch 516i V6 FW 5.4.0.14
 * @n           Speedtouch 585i V6 FW 6.1.0.5
 * @n           Speedtouch 536i V6 FW 6.2.15.5
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from speedtouch.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_SPEEDTOUCH_H__
#define __DSLTOOL_SPEEDTOUCH_H__

#include "regex-parser.h"

//! @cond
namespace NSpeedtouch
{

class CSpeedtouch : public CModem
{
public:
    CSpeedtouch (CDslData &cData,
            CProtocol *pProtocol);

private:
    CSpeedtouch (const CSpeedtouch &cSpeedtouch);
    CSpeedtouch &operator = (const CSpeedtouch &cSpeedtouch);

public:
    virtual void Init (ECommand eCommand);

private:
    class CBandplanData
    {
    private:
        CBandplanData (const char *strVersion,
                const char *strAnnex,
                CBandplan::EBandplan eBandplan);
        CBandplanData (const CBandplanData &cBandplanData);
        CBandplanData &operator = (const CBandplanData &cBandplanData);

    public:
        static CBandplan::EBandplan Get (const char *strVersion,
                const char *strAnnex);

    private:
        const char *m_strVersion;
        const char *m_strAnnex;
        CBandplan::EBandplan m_eBandplan;

    private:
        static const CBandplanData g_acList[];
    }; // class CBandplanData

public:
    static CBandplan::EBandplan Bandplan (const char *strVersion,
            const char *strAnnex);

//begin user code
//end user code

public:
    static const long s_nVersionV6;
}; // class CSpeedtouch

class CLoginUser : public CParser
{
public:
    CLoginUser (CModem *pModem, const char *strUser);

private:
    CLoginUser (const CLoginUser &cLoginUser);
    CLoginUser &operator = (const CLoginUser &cLoginUser);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strUser;
}; // class CLoginUser

class CLoginPass : public CParser
{
public:
    CLoginPass (CModem *pModem, const char *strPass);

private:
    CLoginPass (const CLoginPass &cLoginPass);
    CLoginPass &operator = (const CLoginPass &cLoginPass);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strPass;
}; // class CLoginPass

template <class T> class CPromptParser : public CRegExCmdParser<T, CLineParser>
{
protected:
    CPromptParser (CModem *pModem, const char *strCmd);

private:
    CPromptParser (const CPromptParser &cPromptParser);
    CPromptParser &operator = (const CPromptParser &cPromptParser);

protected:
    CRegExList m_cPrompt;
}; // class CPromptParser

class CEnvList : public CPromptParser<CEnvList>
{
public:
    CEnvList (CModem *pModem, const char *strCmd);

private:
    CEnvList (const CEnvList &cEnvList);
    CEnvList &operator = (const CEnvList &cEnvList);

private:
    CParser *BuildCallback (void);
    CParser *ATMaddrCallback (void);
    CParser *Intf1PVCCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    long m_nTemp1;
    long m_nTemp2;
    long m_nTemp3;

private:
    CRegExList m_cBuild;
    CRegExList m_cATMaddr;
    CRegExList m_cIntf1PVC;
}; // class CEnvList

class CAdslInfo : public CPromptParser<CAdslInfo>
{
public:
    CAdslInfo (CModem *pModem, const char *strCmd);

private:
    CAdslInfo (const CAdslInfo &cAdslInfo);
    CAdslInfo &operator = (const CAdslInfo &cAdslInfo);

private:
    CParser *ModemStateCallback (void);
    CParser *OperationModeCallback (void);
    CParser *XDslStandardCallback (void);
    CParser *XDslAnnexCallback (void);
    CParser *ChannelModeCallback (void);
    CParser *VendorStateV5Callback (void);
    CParser *VendorStateV6ChipCallback (void);
    CParser *VendorStateV6SysCallback (void);
    CParser *VendorV5Callback (void);
    CParser *VendorV6ChipCallback (void);
    CParser *VendorV6SysCallback (void);
    CParser *VendorSpecV5Callback (void);
    CParser *VendorSpecV6ChipCallback (void);
    CParser *VendorSpecV6SysCallback (void);
    CParser *StdRevisionV5Callback (void);
    CParser *StdRevisionV6ChipCallback (void);
    CParser *StdRevisionV6SysCallback (void);
    CParser *NoiseMarginCallback (void);
    CParser *AttenuationCallback (void);
    CParser *TxPowerCallback (void);
    CParser *BandwidthStartCallback (void);
    CParser *BandwidthEndCallback (void);
    CParser *BandwidthUpCallback (void);
    CParser *BandwidthDownCallback (void);
    CParser *StatisticsStateCallback (void);
    CParser *ErrorsStateCallback (void);
    CParser *ErrorsTxFECCallback (void);
    CParser *ErrorsRxFECCallback (void);
    CParser *ErrorsTxCRCCallback (void);
    CParser *ErrorsRxCRCCallback (void);
    CParser *ErrorsTxHECCallback (void);
    CParser *ErrorsRxHECCallback (void);
    CParser *Fail15minStateCallback (void);
    CParser *FailDayStateCallback (void);
    CParser *ErrorSec15minCallback (void);
    CParser *ErrorSecDayCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eVendorV5,
        eVendorV6Chip,
        eVendorV6Sys,
        eBandwidth,
        eStatistics,
        eErrors,
        eFail15min,
        eFailDay
    };

private:
    long m_nTemp0;
    long m_nTemp1;
    long m_nTemp2;
    long m_nTemp3;
    double m_fTemp0;
    double m_fTemp1;
    CString m_strTemp0;
    CString m_strTemp1;
    bool m_bOpVersion;
    bool m_bOpAnnex;
    CString m_strOpVersion;
    CString m_strOpAnnex;

private:
    CRegExList m_cModemState;
    CRegExList m_cOperationMode;
    CRegExList m_cXDslStandard;
    CRegExList m_cXDslAnnex;
    CRegExList m_cChannelMode;
    CRegExList m_cVendorStateV5;
    CRegExList m_cVendorStateV6Chip;
    CRegExList m_cVendorStateV6Sys;
    CRegExList m_cVendor;
    CRegExList m_cVendorSpec;
    CRegExList m_cStdRevision;
    CRegExList m_cNoiseMargin;
    CRegExList m_cAttenuation;
    CRegExList m_cTxPower;
    CRegExList m_cBandwidthStart;
    CRegExList m_cBandwidthEnd;
    CRegExList m_cBandwidthUp;
    CRegExList m_cBandwidthDown;
    CRegExList m_cStatisticsState;
    CRegExList m_cErrorsState;
    CRegExList m_cErrorsTxFEC;
    CRegExList m_cErrorsRxFEC;
    CRegExList m_cErrorsTxCRC;
    CRegExList m_cErrorsRxCRC;
    CRegExList m_cErrorsTxHEC;
    CRegExList m_cErrorsRxHEC;
    CRegExList m_cFail15minState;
    CRegExList m_cFailDayState;
    CRegExList m_cErrorSec;
}; // class CAdslInfo

class CBitLoad : public CPromptParser<CBitLoad>
{
public:
    CBitLoad (CModem *pModem, const char *strCmd);

private:
    CBitLoad (const CBitLoad &cBitLoad);
    CBitLoad &operator = (const CBitLoad &cBitLoad);

private:
    CParser *ToneV5Callback (void);
    CParser *ToneV6Callback (void);
    CParser *StartBitAllocV5Callback (void);
    CParser *EndBitAllocV5Callback (void);
    CParser *StartBitAllocV6Callback (void);
    CParser *EndBitAllocV6Callback (void);
    CParser *StartSNRCallback (void);
    CParser *EndSNRCallback (void);
    CParser *StartGainQ2Callback (void);
    CParser *EndGainQ2Callback (void);
    CParser *StartNoiseMarginCallback (void);
    CParser *EndNoiseMarginCallback (void);
    CParser *StartChanCharLogCallback (void);
    CParser *EndChanCharLogCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eBitAllocV5,
        eBitAllocV6,
        eSNR,
        eGainQ2,
        eNoiseMargin,
        eChanCharLog
    };

private:
    bool m_bToneValid;
    CTone m_cTone;

private:
    CRegExList m_cToneV5_10;
    CRegExList m_cToneV5_8;
    CRegExList m_cToneV5_6;
    CRegExList m_cToneV5_4;
    CRegExList m_cToneV5_2;
    CRegExList m_cToneV6_10;
    CRegExList m_cToneV6_8;
    CRegExList m_cToneV6_6;
    CRegExList m_cToneV6_4;
    CRegExList m_cToneV6_2;
    CRegExList m_cStartBitAllocV5;
    CRegExList m_cEndBitAllocV5;
    CRegExList m_cStartBitAllocV6;
    CRegExList m_cStartSNR;
    CRegExList m_cEndSNR;
    CRegExList m_cStartGainQ2;
    CRegExList m_cEndGainQ2;
    CRegExList m_cStartNoiseMargin;
    CRegExList m_cEndNoiseMargin;
    CRegExList m_cStartChanCharLog;
    CRegExList m_cEndChanCharLog;
}; // class CBitLoad

class CAdslDown : public CPromptParser<CAdslDown>
{
public:
    CAdslDown (CModem *pModem, const char *strCmd);

private:
    CAdslDown (const CAdslDown &cAdslDown);
    CAdslDown &operator = (const CAdslDown &cAdslDown);

private:
    CParser *PromptCallback (void);
}; // class CAdslDown

class CAdslUp : public CPromptParser<CAdslUp>
{
public:
    CAdslUp (CModem *pModem, const char *strCmd);

private:
    CAdslUp (const CAdslUp &cAdslUp);
    CAdslUp &operator = (const CAdslUp &cAdslUp);

private:
    CParser *PromptCallback (void);
}; // class CAdslUp

class CSysReboot : public CPromptParser<CSysReboot>
{
public:
    CSysReboot (CModem *pModem, const char *strCmd);

private:
    CSysReboot (const CSysReboot &cSysReboot);
    CSysReboot &operator = (const CSysReboot &cSysReboot);

private:
    CParser *PromptCallback (void);
}; // class CSysReboot

class CExit : public CPromptParser<CExit>
{
public:
    CExit (CModem *pModem, const char *strCmd);

private:
    CExit (const CExit &cExit);
    CExit &operator = (const CExit &cExit);

private:
    CParser *PromptCallback (void);
}; // class CExit

} // namespace NSpeedtouch
//! @endcond
#endif // __DSLTOOL_SPEEDTOUCH_H__
//_oOo_
