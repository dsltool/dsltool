/*!
 * @file        amazon.h
 * @brief       amazon modem definitions
 * @details     parser for Infineon AMAZON SE based modems, tested with:
 * @n           Allnet ALL 0333 CJ
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        22.11.2015
 *
 * @note        This file is partly generated from ar7.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_AMAZON_H__
#define __DSLTOOL_AMAZON_H__

#include "regex-parser.h"

//! @cond
namespace NAmazon
{

class CAmazon : public CModem
{
public:
    CAmazon (CDslData &cData,
            CProtocol *pProtocol);

private:
    CAmazon (const CAmazon &cAmazon);
    CAmazon &operator = (const CAmazon &cAmazon);

public:
    virtual void Init (ECommand eCommand);

private:
    class CBandplanData
    {
    private:
        CBandplanData (int nXTS1,
                int nXTS2,
                int nXTS3,
                int nXTS4,
                int nXTS5,
                int nXTS6,
                int nXTS7,
                int nXTS8,
                CBandplan::EBandplan eBandplan);
        CBandplanData (const CBandplanData &cBandplanData);
        CBandplanData &operator = (const CBandplanData &cBandplanData);

    public:
        static CBandplan::EBandplan Get (int nXTS1,
                int nXTS2,
                int nXTS3,
                int nXTS4,
                int nXTS5,
                int nXTS6,
                int nXTS7,
                int nXTS8);

    private:
        int m_nXTS1;
        int m_nXTS2;
        int m_nXTS3;
        int m_nXTS4;
        int m_nXTS5;
        int m_nXTS6;
        int m_nXTS7;
        int m_nXTS8;
        CBandplan::EBandplan m_eBandplan;

    private:
        static const CBandplanData g_acList[];
    }; // class CBandplanData

public:
    static CBandplan::EBandplan Bandplan (int nXTS1,
            int nXTS2,
            int nXTS3,
            int nXTS4,
            int nXTS5,
            int nXTS6,
            int nXTS7,
            int nXTS8);

//begin user code
private:
    class CModemState
    {
    private:
        CModemState (long nState, const char *strState);
        CModemState (const CModemState &cModemState);
        CModemState &operator = (const CModemState &cModemState);

    public:
        static const char* Get (long nState);

    private:
        long m_nState;
        const char *m_strState;
        static const CModemState g_acList[];
    }; // class CModemState

public:
    static const char* GetModemState (long nState){return CModemState::Get (nState);}
//end user code
}; // class CAmazon

class CLoginUser : public CParser
{
public:
    CLoginUser (CModem *pModem, const char *strUser);

private:
    CLoginUser (const CLoginUser &cLoginUser);
    CLoginUser &operator = (const CLoginUser &cLoginUser);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strUser;
}; // class CLoginUser

class CLoginPass : public CParser
{
public:
    CLoginPass (CModem *pModem, const char *strPass);

private:
    CLoginPass (const CLoginPass &cLoginPass);
    CLoginPass &operator = (const CLoginPass &cLoginPass);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strPass;
}; // class CLoginPass

template <class T> class CPromptParser : public CRegExCmdParser<T, CLineParser>
{
protected:
    CPromptParser (CModem *pModem, const char *strCmd);

private:
    CPromptParser (const CPromptParser &cPromptParser);
    CPromptParser &operator = (const CPromptParser &cPromptParser);

protected:
    CRegExList m_cPrompt;
}; // class CPromptParser

class CStatus : public CPromptParser<CStatus>
{
public:
    CStatus (CModem *pModem, const char *strCmd);

private:
    CStatus (const CStatus &cStatus);
    CStatus &operator = (const CStatus &cStatus);

private:
    CParser *StatusCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nLineState;

private:
    CRegExList m_cStatus;
}; // class CStatus

class COpMode : public CPromptParser<COpMode>
{
public:
    COpMode (CModem *pModem, const char *strCmd);

private:
    COpMode (const COpMode &cOpMode);
    COpMode &operator = (const COpMode &cOpMode);

private:
    CParser *OpModeCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nXTSE1;
    long m_nXTSE2;
    long m_nXTSE3;
    long m_nXTSE4;
    long m_nXTSE5;
    long m_nXTSE6;
    long m_nXTSE7;
    long m_nXTSE8;

private:
    CRegExList m_cOpMode;
}; // class COpMode

class CRateUp : public CPromptParser<CRateUp>
{
public:
    CRateUp (CModem *pModem, const char *strCmd);

private:
    CRateUp (const CRateUp &cRateUp);
    CRateUp &operator = (const CRateUp &cRateUp);

private:
    CParser *RateCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nChannel;
    long m_nDirection;
    long m_nActualDataRate;
    long m_nPreviousDataRate;
    long m_nActualInterleaveDelay;
    long m_ActualImpulseNoiseProtection;

private:
    CRegExList m_cRate;
}; // class CRateUp

class CRateDown : public CPromptParser<CRateDown>
{
public:
    CRateDown (CModem *pModem, const char *strCmd);

private:
    CRateDown (const CRateDown &cRateDown);
    CRateDown &operator = (const CRateDown &cRateDown);

private:
    CParser *RateCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nChannel;
    long m_nDirection;
    long m_nActualDataRate;
    long m_nPreviousDataRate;
    long m_nActualInterleaveDelay;
    long m_ActualImpulseNoiseProtection;

private:
    CRegExList m_cRate;
}; // class CRateDown

class CLineUp : public CPromptParser<CLineUp>
{
public:
    CLineUp (CModem *pModem, const char *strCmd);

private:
    CLineUp (const CLineUp &cLineUp);
    CLineUp &operator = (const CLineUp &cLineUp);

private:
    CParser *LineCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nDirection;
    long m_nDeltDataType;
    long m_nLATN;
    long m_nSATN;
    long m_nSNR;
    long m_nATTNDR;
    long m_nACTPS;
    long m_nACTATP;

private:
    CRegExList m_cLine;
}; // class CLineUp

class CLineDown : public CPromptParser<CLineDown>
{
public:
    CLineDown (CModem *pModem, const char *strCmd);

private:
    CLineDown (const CLineDown &cLineDown);
    CLineDown &operator = (const CLineDown &cLineDown);

private:
    CParser *LineCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nDirection;
    long m_nDeltDataType;
    long m_nLATN;
    long m_nSATN;
    long m_nSNR;
    long m_nATTNDR;
    long m_nACTPS;
    long m_nACTATP;

private:
    CRegExList m_cLine;
}; // class CLineDown

class CFECNear : public CPromptParser<CFECNear>
{
public:
    CFECNear (CModem *pModem, const char *strCmd);

private:
    CFECNear (const CFECNear &cFECNear);
    CFECNear &operator = (const CFECNear &cFECNear);

private:
    CParser *FECCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nChannel;
    long m_nDirection;
    long m_nElapsedTime;
    long m_nCodeViolations;
    long m_nFEC;

private:
    CRegExList m_cFEC;
}; // class CFECNear

class CFECFar : public CPromptParser<CFECFar>
{
public:
    CFECFar (CModem *pModem, const char *strCmd);

private:
    CFECFar (const CFECFar &cFECFar);
    CFECFar &operator = (const CFECFar &cFECFar);

private:
    CParser *FECCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nChannel;
    long m_nDirection;
    long m_nElapsedTime;
    long m_nCodeViolations;
    long m_nFEC;

private:
    CRegExList m_cFEC;
}; // class CFECFar

class CHECNear : public CPromptParser<CHECNear>
{
public:
    CHECNear (CModem *pModem, const char *strCmd);

private:
    CHECNear (const CHECNear &cHECNear);
    CHECNear &operator = (const CHECNear &cHECNear);

private:
    CParser *HECCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nChannel;
    long m_nDirection;
    long m_nElapsedTime;
    long m_nHEC;
    long m_nTotalCells;
    long m_nUserTotalCells;
    long m_nIBE;

private:
    CRegExList m_cHEC;
}; // class CHECNear

class CHECFar : public CPromptParser<CHECFar>
{
public:
    CHECFar (CModem *pModem, const char *strCmd);

private:
    CHECFar (const CHECFar &cHECFar);
    CHECFar &operator = (const CHECFar &cHECFar);

private:
    CParser *HECCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nChannel;
    long m_nDirection;
    long m_nElapsedTime;
    long m_nHEC;
    long m_nTotalCells;
    long m_nUserTotalCells;
    long m_nIBE;

private:
    CRegExList m_cHEC;
}; // class CHECFar

class CErrorSec15minNear : public CPromptParser<CErrorSec15minNear>
{
public:
    CErrorSec15minNear (CModem *pModem, const char *strCmd);

private:
    CErrorSec15minNear (const CErrorSec15minNear &cErrorSec15minNear);
    CErrorSec15minNear &operator = (const CErrorSec15minNear &cErrorSec15minNear);

private:
    CParser *ErrorSecCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nDirection;
    long m_nHistoryInterval;
    long m_nElapsedTime;
    long m_bValid;
    long m_nES;
    long m_nSES;
    long m_nLOSS;
    long m_nUAS;
    long m_nLOFS;

private:
    CRegExList m_cErrorSec;
}; // class CErrorSec15minNear

class CErrorSecDayNear : public CPromptParser<CErrorSecDayNear>
{
public:
    CErrorSecDayNear (CModem *pModem, const char *strCmd);

private:
    CErrorSecDayNear (const CErrorSecDayNear &cErrorSecDayNear);
    CErrorSecDayNear &operator = (const CErrorSecDayNear &cErrorSecDayNear);

private:
    CParser *ErrorSecCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nDirection;
    long m_nHistoryInterval;
    long m_nElapsedTime;
    long m_bValid;
    long m_nES;
    long m_nSES;
    long m_nLOSS;
    long m_nUAS;
    long m_nLOFS;

private:
    CRegExList m_cErrorSec;
}; // class CErrorSecDayNear

class CInfoNear : public CPromptParser<CInfoNear>
{
public:
    CInfoNear (CModem *pModem, const char *strCmd);

private:
    CInfoNear (const CInfoNear &cInfoNear);
    CInfoNear &operator = (const CInfoNear &cInfoNear);

private:
    CParser *InfoCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nDirection;
    long m_nCountry0;
    long m_nCountry1;
    long m_nVendorID0;
    long m_nVendorID1;
    long m_nVendorID2;
    long m_nVendorID3;
    long m_nVendorSpec0;
    long m_nVendorSpec1;
    long m_nSelfTestResult;
    CString m_strTemp;

private:
    CRegExList m_cInfo;
}; // class CInfoNear

class CInfoFar : public CPromptParser<CInfoFar>
{
public:
    CInfoFar (CModem *pModem, const char *strCmd);

private:
    CInfoFar (const CInfoFar &cInfoFar);
    CInfoFar &operator = (const CInfoFar &cInfoFar);

private:
    CParser *InfoCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nDirection;
    long m_nCountry0;
    long m_nCountry1;
    long m_nVendorID0;
    long m_nVendorID1;
    long m_nVendorID2;
    long m_nVendorID3;
    long m_nVendorSpec0;
    long m_nVendorSpec1;
    long m_nSelfTestResult;
    CString m_strTemp;

private:
    CRegExList m_cInfo;
}; // class CInfoFar

class CBitsUp : public CPromptParser<CBitsUp>
{
public:
    CBitsUp (CModem *pModem, const char *strCmd);

private:
    CBitsUp (const CBitsUp &cBitsUp);
    CBitsUp &operator = (const CBitsUp &cBitsUp);

private:
    CParser *NumCallback (void);
    CParser *FormatCallback (void);
    CParser *BitsCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eTones
    };

private:
    long m_nReturn;
    long m_nDirection;
    long m_nNumData;
    CString m_strData;
    CTone m_cTone;

private:
    CRegExList m_cNum;
    CRegExList m_cFormat;
    CRegExList m_cBits;
}; // class CBitsUp

class CBitsDown : public CPromptParser<CBitsDown>
{
public:
    CBitsDown (CModem *pModem, const char *strCmd);

private:
    CBitsDown (const CBitsDown &cBitsDown);
    CBitsDown &operator = (const CBitsDown &cBitsDown);

private:
    CParser *NumCallback (void);
    CParser *FormatCallback (void);
    CParser *BitsCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eTones
    };

private:
    long m_nReturn;
    long m_nDirection;
    long m_nNumData;
    CString m_strData;
    CTone m_cTone;

private:
    CRegExList m_cNum;
    CRegExList m_cFormat;
    CRegExList m_cBits;
}; // class CBitsDown

class CSNRDown : public CPromptParser<CSNRDown>
{
public:
    CSNRDown (CModem *pModem, const char *strCmd);

private:
    CSNRDown (const CSNRDown &cSNRDown);
    CSNRDown &operator = (const CSNRDown &cSNRDown);

private:
    CParser *NumCallback (void);
    CParser *SnrCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nReturn;
    long m_nDirection;
    long m_nNumData;
    CString m_strData;
    CTone m_cTone;

private:
    CRegExList m_cNum;
    CRegExList m_cSnr;
}; // class CSNRDown

class CReboot : public CPromptParser<CReboot>
{
public:
    CReboot (CModem *pModem, const char *strCmd);

private:
    CReboot (const CReboot &cReboot);
    CReboot &operator = (const CReboot &cReboot);

private:
    CParser *PromptCallback (void);
}; // class CReboot

class CExit : public CPromptParser<CExit>
{
public:
    CExit (CModem *pModem, const char *strCmd);

private:
    CExit (const CExit &cExit);
    CExit &operator = (const CExit &cExit);

private:
    CParser *PromptCallback (void);
}; // class CExit

} // namespace NAmazon
//! @endcond
#endif // __DSLTOOL_AMAZON_H__
//_oOo_
