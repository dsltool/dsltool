/*!
 * @file        vigor.h
 * @brief       vigor modem definitions
 * @details     parser for vigor modems, tested with:
 * @n           Vigor 130
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from vigor.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_VIGOR_H__
#define __DSLTOOL_VIGOR_H__

#include "regex-parser.h"

//! @cond
namespace NVigor
{

class CVigor : public CModem
{
public:
    CVigor (CDslData &cData,
            CProtocol *pProtocol);

private:
    CVigor (const CVigor &cVigor);
    CVigor &operator = (const CVigor &cVigor);

public:
    virtual void Init (ECommand eCommand);

private:
    class CBandplanData
    {
    private:
        CBandplanData (const char *strMode,
                CBandplan::EBandplan eBandplan);
        CBandplanData (const CBandplanData &cBandplanData);
        CBandplanData &operator = (const CBandplanData &cBandplanData);

    public:
        static CBandplan::EBandplan Get (const char *strMode);

    private:
        const char *m_strMode;
        CBandplan::EBandplan m_eBandplan;

    private:
        static const CBandplanData g_acList[];
    }; // class CBandplanData

public:
    static CBandplan::EBandplan Bandplan (const char *strMode);

//begin user code
//end user code

public:
    static const long s_nVersionV3781;
}; // class CVigor

class CLoginUser : public CParser
{
public:
    CLoginUser (CModem *pModem, const char *strUser);

private:
    CLoginUser (const CLoginUser &cLoginUser);
    CLoginUser &operator = (const CLoginUser &cLoginUser);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strUser;
}; // class CLoginUser

class CLoginPass : public CParser
{
public:
    CLoginPass (CModem *pModem, const char *strPass);

private:
    CLoginPass (const CLoginPass &cLoginPass);
    CLoginPass &operator = (const CLoginPass &cLoginPass);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strPass;
}; // class CLoginPass

template <class T> class CPromptParser : public CRegExCmdParser<T, CLineParser>
{
protected:
    CPromptParser (CModem *pModem, const char *strCmd);

private:
    CPromptParser (const CPromptParser &cPromptParser);
    CPromptParser &operator = (const CPromptParser &cPromptParser);

protected:
    CRegExList m_cPrompt;
}; // class CPromptParser

class CStatus : public CPromptParser<CStatus>
{
public:
    CStatus (CModem *pModem, const char *strCmd);

private:
    CStatus (const CStatus &cStatus);
    CStatus &operator = (const CStatus &cStatus);

private:
    CParser *ATU_RCallback (void);
    CParser *ATU_CCallback (void);
    CParser *ModeCallback (void);
    CParser *ActRateCallback (void);
    CParser *MaxRateCallback (void);
    CParser *PathCallback (void);
    CParser *AttenuationUpCallback (void);
    CParser *AttenuationDownCallback (void);
    CParser *TxPowerUpCallback (void);
    CParser *Version_RCallback (void);
    CParser *Version_CCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eATU_R,
        eATU_C
    };

private:
    long m_nTemp0;
    long m_nTemp1;
    long m_nTemp2;
    long m_nTemp3;
    long m_nCountry0;
    long m_nCountry1;
    long m_nVendorID0;
    long m_nVendorID1;
    long m_nVendorID2;
    long m_nVendorID3;
    long m_nVendorSpec0;
    long m_nVendorSpec1;
    double m_fTemp0;
    double m_fTemp1;
    CString m_strTemp0;
    CString m_strTemp1;

private:
    CRegExList m_cATU_R;
    CRegExList m_cATU_C;
    CRegExList m_cMode;
    CRegExList m_cActRate;
    CRegExList m_cMaxRate;
    CRegExList m_cPath;
    CRegExList m_cAttenuation;
    CRegExList m_cTxPower;
    CRegExList m_cVersion_R;
    CRegExList m_cVersion_C;
}; // class CStatus

class CMore : public CPromptParser<CMore>
{
public:
    CMore (CModem *pModem, const char *strCmd);

private:
    CMore (const CMore &cMore);
    CMore &operator = (const CMore &cMore);

private:
    CParser *ATU_RCallback (void);
    CParser *HECCallback (void);
    CParser *CRCCallback (void);
    CParser *FECCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eATU_R
    };

private:
    long m_nTemp0;
    long m_nTemp1;
    CString m_strTemp0;
    CString m_strTemp1;

private:
    CRegExList m_cATU_R;
    CRegExList m_cHEC;
    CRegExList m_cCRC;
    CRegExList m_cFEC;
}; // class CMore

class CAtm : public CPromptParser<CAtm>
{
public:
    CAtm (CModem *pModem, const char *strCmd);

private:
    CAtm (const CAtm &cAtm);
    CAtm &operator = (const CAtm &cAtm);

private:
    CParser *VCICallback (void);
    CParser *VPICallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;

private:
    CRegExList m_cVCI;
    CRegExList m_cVPI;
}; // class CAtm

class CVersion : public CPromptParser<CVersion>
{
public:
    CVersion (CModem *pModem, const char *strCmd);

private:
    CVersion (const CVersion &cVersion);
    CVersion &operator = (const CVersion &cVersion);

private:
    CParser *Version4Callback (void);
    CParser *Version3Callback (void);
    CParser *Version2Callback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    long m_nTemp1;
    long m_nTemp2;
    long m_nTemp3;
    CString m_strTemp0;
    CString m_strTemp1;

private:
    CRegExList m_cVersion4;
    CRegExList m_cVersion3;
    CRegExList m_cVersion2;
}; // class CVersion

class CBitAllocDown : public CPromptParser<CBitAllocDown>
{
public:
    CBitAllocDown (CModem *pModem, const char *strCmd);

private:
    CBitAllocDown (const CBitAllocDown &cBitAllocDown);
    CBitAllocDown &operator = (const CBitAllocDown &cBitAllocDown);

private:
    CParser *DownCallback (void);
    CParser *ToneCallback (void);
    CParser *PromptCallback (void);

private:
    bool m_bWorkaround;
    bool m_bToneValid;
    long m_nTone0;
    long m_nTone1;
    long m_nTone2;
    long m_nTone3;
    long m_nSNR0;
    long m_nSNR1;
    long m_nSNR2;
    long m_nSNR3;
    long m_nGain0;
    long m_nGain1;
    long m_nGain2;
    long m_nGain3;
    long m_nBits0;
    long m_nBits1;
    long m_nBits2;
    long m_nBits3;
    CTone m_cToneBits;
    CTone m_cToneSNR;
    CTone m_cToneGain;

private:
    CRegExList m_cDown;
    CRegExList m_cTone;
}; // class CBitAllocDown

class CBitAllocUp : public CPromptParser<CBitAllocUp>
{
public:
    CBitAllocUp (CModem *pModem, const char *strCmd);

private:
    CBitAllocUp (const CBitAllocUp &cBitAllocUp);
    CBitAllocUp &operator = (const CBitAllocUp &cBitAllocUp);

private:
    CParser *UpCallback (void);
    CParser *ToneCallback (void);
    CParser *PromptCallback (void);

private:
    bool m_bWorkaround;
    bool m_bToneValid;
    long m_nTone0;
    long m_nTone1;
    long m_nTone2;
    long m_nTone3;
    long m_nSNR0;
    long m_nSNR1;
    long m_nSNR2;
    long m_nSNR3;
    long m_nGain0;
    long m_nGain1;
    long m_nGain2;
    long m_nGain3;
    long m_nBits0;
    long m_nBits1;
    long m_nBits2;
    long m_nBits3;
    CTone m_cToneBits;
    CTone m_cToneSNR;
    CTone m_cToneGain;

private:
    CRegExList m_cUp;
    CRegExList m_cTone;
}; // class CBitAllocUp

class CResync : public CPromptParser<CResync>
{
public:
    CResync (CModem *pModem, const char *strCmd);

private:
    CResync (const CResync &cResync);
    CResync &operator = (const CResync &cResync);

private:
    CParser *PromptCallback (void);
}; // class CResync

class CReboot : public CPromptParser<CReboot>
{
public:
    CReboot (CModem *pModem, const char *strCmd);

private:
    CReboot (const CReboot &cReboot);
    CReboot &operator = (const CReboot &cReboot);

private:
    CParser *PromptCallback (void);
}; // class CReboot

class CExit : public CPromptParser<CExit>
{
public:
    CExit (CModem *pModem, const char *strCmd);

private:
    CExit (const CExit &cExit);
    CExit &operator = (const CExit &cExit);

private:
    CParser *PromptCallback (void);
}; // class CExit

} // namespace NVigor
//! @endcond
#endif // __DSLTOOL_VIGOR_H__
//_oOo_
