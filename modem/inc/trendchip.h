/*!
 * @file        trendchip.h
 * @brief       trendchip modem definitions
 * @details     parser for TrendChip based modems, tested with:
 * @n           D-Link DSL-321B HW Ver. Zx (not Dx!)
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        17.11.2015
 *
 * @note        This file is partly generated from bc63.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_TRENDCHIP_H__
#define __DSLTOOL_TRENDCHIP_H__

#include "regex-parser.h"

//! @cond
namespace NTrendchip
{

class CTrendchip : public CModem
{
public:
    CTrendchip (CDslData &cData,
            CProtocol *pProtocol);

private:
    CTrendchip (const CTrendchip &cTrendchip);
    CTrendchip &operator = (const CTrendchip &cTrendchip);

public:
    virtual void Init (ECommand eCommand);

private:
    class CBandplanData
    {
    private:
        CBandplanData (const char *strVersion,
                const char *strAnnex,
                CBandplan::EBandplan eBandplan);
        CBandplanData (const CBandplanData &cBandplanData);
        CBandplanData &operator = (const CBandplanData &cBandplanData);

    public:
        static CBandplan::EBandplan Get (const char *strVersion,
                const char *strAnnex);

    private:
        const char *m_strVersion;
        const char *m_strAnnex;
        CBandplan::EBandplan m_eBandplan;

    private:
        static const CBandplanData g_acList[];
    }; // class CBandplanData

public:
    static CBandplan::EBandplan Bandplan (const char *strVersion,
            const char *strAnnex);

//begin user code
    CString m_strOpMode;
    CString m_strAnnex;
//end user code
}; // class CTrendchip

class CLoginPass : public CParser
{
public:
    CLoginPass (CModem *pModem, const char *strPass);

private:
    CLoginPass (const CLoginPass &cLoginPass);
    CLoginPass &operator = (const CLoginPass &cLoginPass);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strPass;
}; // class CLoginPass

template <class T> class CPromptParser : public CRegExCmdParser<T, CLineParser>
{
protected:
    CPromptParser (CModem *pModem, const char *strCmd);

private:
    CPromptParser (const CPromptParser &cPromptParser);
    CPromptParser &operator = (const CPromptParser &cPromptParser);

protected:
    CRegExList m_cPrompt;
}; // class CPromptParser

class CStatus : public CPromptParser<CStatus>
{
public:
    CStatus (CModem *pModem, const char *strCmd);

private:
    CStatus (const CStatus &cStatus);
    CStatus &operator = (const CStatus &cStatus);

private:
    CParser *StatusCallback (void);
    CParser *PromptCallback (void);

private:
    CString m_strTemp0;

private:
    CRegExList m_cStatus;
}; // class CStatus

class COpMode : public CPromptParser<COpMode>
{
public:
    COpMode (CModem *pModem, const char *strCmd);

private:
    COpMode (const COpMode &cOpMode);
    COpMode &operator = (const COpMode &cOpMode);

private:
    CParser *ModeCallback (void);
    CParser *PromptCallback (void);

private:
    CString m_strTemp0;

private:
    CRegExList m_cMode;
}; // class COpMode

class CAnnex : public CPromptParser<CAnnex>
{
public:
    CAnnex (CModem *pModem, const char *strCmd);

private:
    CAnnex (const CAnnex &cAnnex);
    CAnnex &operator = (const CAnnex &cAnnex);

private:
    CParser *AnnexCallback (void);
    CParser *PromptCallback (void);

private:
    CString m_strTemp0;

private:
    CRegExList m_cAnnex;
}; // class CAnnex

class CChan : public CPromptParser<CChan>
{
public:
    CChan (CModem *pModem, const char *strCmd);

private:
    CChan (const CChan &cChan);
    CChan &operator = (const CChan &cChan);

private:
    CParser *DownCallback (void);
    CParser *UpCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    CString m_strTemp0;

private:
    CRegExList m_cDown;
    CRegExList m_cUp;
}; // class CChan

class CPerf : public CPromptParser<CPerf>
{
public:
    CPerf (CModem *pModem, const char *strCmd);

private:
    CPerf (const CPerf &cPerf);
    CPerf &operator = (const CPerf &cPerf);

private:
    CParser *DownFECCallback (void);
    CParser *DownCRCCallback (void);
    CParser *DownHECCallback (void);
    CParser *UpFECCallback (void);
    CParser *UpCRCCallback (void);
    CParser *UpHECCallback (void);
    CParser *ErrSec15minCallback (void);
    CParser *ErrSecDayCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    CString m_strTemp0;

private:
    CRegExList m_cDownFEC;
    CRegExList m_cDownCRC;
    CRegExList m_cDownHEC;
    CRegExList m_cUpFEC;
    CRegExList m_cUpCRC;
    CRegExList m_cUpHEC;
    CRegExList m_cErrSec15min;
    CRegExList m_cErrSecDay;
}; // class CPerf

class CAtm : public CPromptParser<CAtm>
{
public:
    CAtm (CModem *pModem, const char *strCmd);

private:
    CAtm (const CAtm &cAtm);
    CAtm &operator = (const CAtm &cAtm);

private:
    CParser *VpiVciCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    long m_nTemp1;

private:
    CRegExList m_cVpiVci;
}; // class CAtm

class CAtuR : public CPromptParser<CAtuR>
{
public:
    CAtuR (CModem *pModem, const char *strCmd);

private:
    CAtuR (const CAtuR &cAtuR);
    CAtuR &operator = (const CAtuR &cAtuR);

private:
    CParser *IDCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    long m_nTemp1;
    long m_nTemp2;
    long m_nTemp3;
    long m_nTemp4;
    long m_nTemp5;
    long m_nTemp6;
    long m_nTemp7;

private:
    CRegExList m_cID;
}; // class CAtuR

class CAtuC : public CPromptParser<CAtuC>
{
public:
    CAtuC (CModem *pModem, const char *strCmd);

private:
    CAtuC (const CAtuC &cAtuC);
    CAtuC &operator = (const CAtuC &cAtuC);

private:
    CParser *IDCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    long m_nTemp1;
    long m_nTemp2;
    long m_nTemp3;
    long m_nTemp4;
    long m_nTemp5;
    long m_nTemp6;
    long m_nTemp7;

private:
    CRegExList m_cID;
}; // class CAtuC

class CLineUp : public CPromptParser<CLineUp>
{
public:
    CLineUp (CModem *pModem, const char *strCmd);

private:
    CLineUp (const CLineUp &cLineUp);
    CLineUp &operator = (const CLineUp &cLineUp);

private:
    CParser *NoiseMarginCallback (void);
    CParser *TxPowerCallback (void);
    CParser *AttenuationCallback (void);
    CParser *PromptCallback (void);

private:
    double m_fTemp0;

private:
    CRegExList m_cNoiseMargin;
    CRegExList m_cTxPower;
    CRegExList m_cAttenuation;
}; // class CLineUp

class CLineDown : public CPromptParser<CLineDown>
{
public:
    CLineDown (CModem *pModem, const char *strCmd);

private:
    CLineDown (const CLineDown &cLineDown);
    CLineDown &operator = (const CLineDown &cLineDown);

private:
    CParser *NoiseMarginCallback (void);
    CParser *TxPowerCallback (void);
    CParser *AttenuationCallback (void);
    CParser *BitAllocCallback (void);
    CParser *TonesCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eBitAlloc
    };

private:
    long m_nTemp0;
    double m_fTemp0;
    CTone m_cTone;
    bool m_bToneValid;

private:
    CRegExList m_cNoiseMargin;
    CRegExList m_cTxPower;
    CRegExList m_cAttenuation;
    CRegExList m_cBitAlloc;
    CRegExList m_cTones;
}; // class CLineDown

class CResync : public CPromptParser<CResync>
{
public:
    CResync (CModem *pModem, const char *strCmd);

private:
    CResync (const CResync &cResync);
    CResync &operator = (const CResync &cResync);

private:
    CParser *PromptCallback (void);
}; // class CResync

class CReboot : public CPromptParser<CReboot>
{
public:
    CReboot (CModem *pModem, const char *strCmd);

private:
    CReboot (const CReboot &cReboot);
    CReboot &operator = (const CReboot &cReboot);

private:
    CParser *PromptCallback (void);
}; // class CReboot

class CExit : public CPromptParser<CExit>
{
public:
    CExit (CModem *pModem, const char *strCmd);

private:
    CExit (const CExit &cExit);
    CExit &operator = (const CExit &cExit);

private:
    CParser *PromptCallback (void);
}; // class CExit

} // namespace NTrendchip
//! @endcond
#endif // __DSLTOOL_TRENDCHIP_H__
//_oOo_
