/*!
 * @file        avm-tr064.h
 * @brief       fritzbox modem definitions
 * @details     parser for AVM Fritz!Box, Fritz!OS >= 5.50 tested with:
 * @n           Fritz!Box 3272
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.12.2015
 *
 * @note        This file is partly generated from ar7.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_AVM_TR064_H__
#define __DSLTOOL_AVM_TR064_H__

#include "xml-parser.h"

//! @cond
namespace NAvmTr064
{

class CAvmTr064 : public CModem
{
public:
    CAvmTr064 (CDslData &cData,
            CProtocol *pProtocol);

private:
    CAvmTr064 (const CAvmTr064 &cAvmTr064);
    CAvmTr064 &operator = (const CAvmTr064 &cAvmTr064);

public:
    virtual void Init (ECommand eCommand);
    virtual bool Connect (void);

//begin user code
public:
    void Authenticate (const char *strNonce,
            const char *strAuth);
    void CreateSoapRequest (CString &strRequest,
            const char *strUrl,
            const char *strService,
            const char *strAction);
private:
    bool m_bAuth;
    CString m_strNonce;
    CString m_strAuth;
    CString m_strRealm;
//end user code
}; // class CAvmTr064

class CSoapAuth : public CDomCmdParser
{
public:
    CSoapAuth (CModem *pModem, const char *strCmd);

private:
    CSoapAuth (const CSoapAuth &cSoapAuth);
    CSoapAuth &operator = (const CSoapAuth &cSoapAuth);

private:
    virtual CParser *Finished (void);
}; // class CSoapAuth

class CIgdWan : public CDomCmdParser
{
public:
    CIgdWan (CModem *pModem, const char *strCmd);

private:
    CIgdWan (const CIgdWan &cIgdWan);
    CIgdWan &operator = (const CIgdWan &cIgdWan);

private:
    virtual CParser *Finished (void);
}; // class CIgdWan

class CAuth1 : public CDomCmdParser
{
public:
    CAuth1 (CModem *pModem, const char *strCmd);

private:
    CAuth1 (const CAuth1 &cAuth1);
    CAuth1 &operator = (const CAuth1 &cAuth1);

private:
    virtual CParser *Finished (void);
}; // class CAuth1

class CIgdDsl : public CDomCmdParser
{
public:
    CIgdDsl (CModem *pModem, const char *strCmd);

private:
    CIgdDsl (const CIgdDsl &cIgdDsl);
    CIgdDsl &operator = (const CIgdDsl &cIgdDsl);

private:
    virtual CParser *Finished (void);
}; // class CIgdDsl

class CAuth2 : public CDomCmdParser
{
public:
    CAuth2 (CModem *pModem, const char *strCmd);

private:
    CAuth2 (const CAuth2 &cAuth2);
    CAuth2 &operator = (const CAuth2 &cAuth2);

private:
    virtual CParser *Finished (void);
}; // class CAuth2

class CDslInfo : public CDomCmdParser
{
public:
    CDslInfo (CModem *pModem, const char *strCmd);

private:
    CDslInfo (const CDslInfo &cDslInfo);
    CDslInfo &operator = (const CDslInfo &cDslInfo);

private:
    virtual CParser *Finished (void);
}; // class CDslInfo

class CDslStat : public CDomCmdParser
{
public:
    CDslStat (CModem *pModem, const char *strCmd);

private:
    CDslStat (const CDslStat &cDslStat);
    CDslStat &operator = (const CDslStat &cDslStat);

private:
    virtual CParser *Finished (void);
}; // class CDslStat

class CDslLink : public CDomCmdParser
{
public:
    CDslLink (CModem *pModem, const char *strCmd);

private:
    CDslLink (const CDslLink &cDslLink);
    CDslLink &operator = (const CDslLink &cDslLink);

private:
    virtual CParser *Finished (void);
}; // class CDslLink

} // namespace NAvmTr064
//! @endcond
#endif // __DSLTOOL_AVM_TR064_H__
//_oOo_
