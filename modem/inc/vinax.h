/*!
 * @file        vinax.h
 * @brief       vinax modem definitions
 * @details     parser for vinax based modems, tested with:
 * @n           Sphairon Speedlink 1113
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from vinax.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_VINAX_H__
#define __DSLTOOL_VINAX_H__

#include "regex-parser.h"

//! @cond
namespace NVinax
{

class CVinax : public CModem
{
public:
    CVinax (CDslData &cData,
            CProtocol *pProtocol);

private:
    CVinax (const CVinax &cVinax);
    CVinax &operator = (const CVinax &cVinax);

public:
    virtual void Init (ECommand eCommand);

//begin user code
//end user code
}; // class CVinax

class CLoginUser : public CParser
{
public:
    CLoginUser (CModem *pModem, const char *strUser);

private:
    CLoginUser (const CLoginUser &cLoginUser);
    CLoginUser &operator = (const CLoginUser &cLoginUser);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strUser;
}; // class CLoginUser

class CLoginPass : public CParser
{
public:
    CLoginPass (CModem *pModem, const char *strPass);

private:
    CLoginPass (const CLoginPass &cLoginPass);
    CLoginPass &operator = (const CLoginPass &cLoginPass);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strPass;
}; // class CLoginPass

template <class T> class CPromptParser : public CRegExCmdParser<T, CLineParser>
{
protected:
    CPromptParser (CModem *pModem, const char *strCmd);

private:
    CPromptParser (const CPromptParser &cPromptParser);
    CPromptParser &operator = (const CPromptParser &cPromptParser);

protected:
    CRegExList m_cPrompt;
}; // class CPromptParser

class CInfo : public CPromptParser<CInfo>
{
public:
    CInfo (CModem *pModem, const char *strCmd);

private:
    CInfo (const CInfo &cInfo);
    CInfo &operator = (const CInfo &cInfo);

private:
    CParser *ModemStateCallback (void);
    CParser *BandwidthCallback (void);
    CParser *MaxBandwidthCallback (void);
    CParser *AttenuationCallback (void);
    CParser *NoiseMarginCallback (void);
    CParser *TxPowerCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    double m_fTemp0;
    double m_fTemp1;
    CString m_strTemp0;

private:
    CRegExList m_cModemState;
    CRegExList m_cBandwidth;
    CRegExList m_cMaxBandwidth;
    CRegExList m_cAttenuation;
    CRegExList m_cNoiseMargin;
    CRegExList m_cTxPower;
}; // class CInfo

class CBitsUp : public CPromptParser<CBitsUp>
{
public:
    CBitsUp (CModem *pModem, const char *strCmd);

private:
    CBitsUp (const CBitsUp &cBitsUp);
    CBitsUp &operator = (const CBitsUp &cBitsUp);

private:
    CParser *NumCallback (void);
    CParser *FormatCallback (void);
    CParser *BitsCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eFormat
    };

private:
    long m_nTemp0;
    long m_nTemp1;
    CString m_strTemp0;
    long m_nTones;

private:
    CRegExList m_cNum;
    CRegExList m_cFormat;
    CRegExList m_cBits;
}; // class CBitsUp

class CBitsDown : public CPromptParser<CBitsDown>
{
public:
    CBitsDown (CModem *pModem, const char *strCmd);

private:
    CBitsDown (const CBitsDown &cBitsDown);
    CBitsDown &operator = (const CBitsDown &cBitsDown);

private:
    CParser *NumCallback (void);
    CParser *FormatCallback (void);
    CParser *BitsCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eFormat
    };

private:
    long m_nTemp0;
    long m_nTemp1;
    CString m_strTemp0;
    long m_nTones;

private:
    CRegExList m_cNum;
    CRegExList m_cFormat;
    CRegExList m_cBits;
}; // class CBitsDown

class CSNRDown : public CPromptParser<CSNRDown>
{
public:
    CSNRDown (CModem *pModem, const char *strCmd);

private:
    CSNRDown (const CSNRDown &cSNRDown);
    CSNRDown &operator = (const CSNRDown &cSNRDown);

private:
    CParser *NumCallback (void);
    CParser *FormatCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    long m_nTemp1;
    CString m_strTemp0;
    long m_nTones;

private:
    CRegExList m_cNum;
    CRegExList m_cFormat;
}; // class CSNRDown

class CResync : public CPromptParser<CResync>
{
public:
    CResync (CModem *pModem, const char *strCmd);

private:
    CResync (const CResync &cResync);
    CResync &operator = (const CResync &cResync);

private:
    CParser *PromptCallback (void);
}; // class CResync

class CReboot : public CPromptParser<CReboot>
{
public:
    CReboot (CModem *pModem, const char *strCmd);

private:
    CReboot (const CReboot &cReboot);
    CReboot &operator = (const CReboot &cReboot);

private:
    CParser *PromptCallback (void);
}; // class CReboot

class CExit : public CPromptParser<CExit>
{
public:
    CExit (CModem *pModem, const char *strCmd);

private:
    CExit (const CExit &cExit);
    CExit &operator = (const CExit &cExit);

private:
    CParser *PromptCallback (void);
}; // class CExit

} // namespace NVinax
//! @endcond
#endif // __DSLTOOL_VINAX_H__
//_oOo_
