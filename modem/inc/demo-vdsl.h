/*!
 * @file        demo-vdsl.h
 * @brief       VDSL modem demo definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DEMO_VDSL_H__
#define __DSLTOOL_DEMO_VDSL_H__

#include "modem.h"

//! @cond

class CDemoVdsl: public CModem
{
public:
    CDemoVdsl (CDslData &cData,
            CProtocol *pProtocol);

private:
    CDemoVdsl (const CDemoVdsl &cDemoVdsl);
    CDemoVdsl &operator = (const CDemoVdsl &cDemoVdsl);

protected:
    virtual ~CDemoVdsl (void) {}

private:
    long Count (long nCount);
    double Count (double fCount);
    long Random (long nRand);
    double Random (double fRand);
    static int s_anBitallocUp[];
    static int s_anBitallocDown[];
    static int s_anSNR[];

public:
    virtual void Init (ECommand eCommand);
private:
    int         m_nCount;
}; // class CDemoVdsl

//! @endcond
#endif // __DSLTOOL_DEMO_VDSL_H__
//_oOo_
