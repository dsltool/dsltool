/*!
 * @file        ar7.h
 * @brief       ar7 modem definitions
 * @details     parser for AR7 based modems, tested with:
 * @n           Funkwerk M22
 * @n           Sphairon AR860
 * @n           D-Link DSL-T380
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @note        This file is partly generated from ar7.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_AR7_H__
#define __DSLTOOL_AR7_H__

#include "regex-parser.h"

//! @cond
namespace NAr7
{

class CAr7 : public CModem
{
public:
    CAr7 (CDslData &cData,
            CProtocol *pProtocol);

private:
    CAr7 (const CAr7 &cAr7);
    CAr7 &operator = (const CAr7 &cAr7);

public:
    virtual void Init (ECommand eCommand);

private:
    class CBandplanData
    {
    private:
        CBandplanData (int nMode,
                const char *strAnnex,
                CBandplan::EBandplan eBandplan);
        CBandplanData (const CBandplanData &cBandplanData);
        CBandplanData &operator = (const CBandplanData &cBandplanData);

    public:
        static CBandplan::EBandplan Get (int nMode,
                const char *strAnnex);

    private:
        int m_nMode;
        const char *m_strAnnex;
        CBandplan::EBandplan m_eBandplan;

    private:
        static const CBandplanData g_acList[];
    }; // class CBandplanData

public:
    static CBandplan::EBandplan Bandplan (int nMode,
            const char *strAnnex);

//begin user code
//end user code
}; // class CAr7

class CLoginUser : public CParser
{
public:
    CLoginUser (CModem *pModem, const char *strUser);

private:
    CLoginUser (const CLoginUser &cLoginUser);
    CLoginUser &operator = (const CLoginUser &cLoginUser);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strUser;
}; // class CLoginUser

class CLoginPass : public CParser
{
public:
    CLoginPass (CModem *pModem, const char *strPass);

private:
    CLoginPass (const CLoginPass &cLoginPass);
    CLoginPass &operator = (const CLoginPass &cLoginPass);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strPass;
}; // class CLoginPass

template <class T> class CPromptParser : public CRegExCmdParser<T, CLineParser>
{
protected:
    CPromptParser (CModem *pModem, const char *strCmd);

private:
    CPromptParser (const CPromptParser &cPromptParser);
    CPromptParser &operator = (const CPromptParser &cPromptParser);

protected:
    CRegExList m_cPrompt;
}; // class CPromptParser

class CVersion : public CPromptParser<CVersion>
{
public:
    CVersion (CModem *pModem, const char *strCmd);

private:
    CVersion (const CVersion &cVersion);
    CVersion &operator = (const CVersion &cVersion);

private:
    CParser *PromptCallback (void);
}; // class CVersion

class CStatus : public CPromptParser<CStatus>
{
public:
    CStatus (CModem *pModem, const char *strCmd);

private:
    CStatus (const CStatus &cStatus);
    CStatus &operator = (const CStatus &cStatus);

private:
    CParser *ConnRateCallback (void);
    CParser *DownStreamCallback (void);
    CParser *UpStreamCallback (void);
    CParser *TxPowerCallback (void);
    CParser *PathCallback (void);
    CParser *ModeCallback (void);
    CParser *AnnexCallback (void);
    CParser *ATUCCallback (void);
    CParser *ATURCallback (void);
    CParser *TxInterleavePath_ICallback (void);
    CParser *TxInterleavePath_FCallback (void);
    CParser *RxInterleavePath_ICallback (void);
    CParser *RxInterleavePath_FCallback (void);
    CParser *TxFastPath_FCallback (void);
    CParser *TxFastPath_ICallback (void);
    CParser *RxFastPath_FCallback (void);
    CParser *RxFastPath_ICallback (void);
    CParser *CRC_FECTxICallback (void);
    CParser *CRC_FECRxICallback (void);
    CParser *CRC_FECTxFCallback (void);
    CParser *CRC_FECRxFCallback (void);
    CParser *HECTxICallback (void);
    CParser *HECRxICallback (void);
    CParser *HECTxFCallback (void);
    CParser *HECRxFCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eInterleavePath,
        eFastPath,
        eTxInterleavePath,
        eRxInterleavePath,
        eTxFastPath,
        eRxFastPath
    };

private:
    long m_nTemp0;
    long m_nTemp1;
    long m_nTemp2;
    long m_nTemp3;
    long m_nTemp4;
    long m_nTemp5;
    long m_nTemp6;
    long m_nTemp7;
    double m_fTemp0;
    double m_fTemp1;
    bool m_bModeValid;
    bool m_bAnnexValid;
    long m_nMode;
    CString m_strAnnex;

private:
    CRegExList m_cConnRate;
    CRegExList m_cDownStream;
    CRegExList m_cUpStream;
    CRegExList m_cTxPower;
    CRegExList m_cPath;
    CRegExList m_cMode;
    CRegExList m_cAnnex;
    CRegExList m_cATUC;
    CRegExList m_cATUR;
    CRegExList m_cTxInterleavePath;
    CRegExList m_cRxInterleavePath;
    CRegExList m_cTxFastPath;
    CRegExList m_cRxFastPath;
    CRegExList m_cCRC_FEC;
    CRegExList m_cHEC;
}; // class CStatus

class CBitAlloc : public CPromptParser<CBitAlloc>
{
public:
    CBitAlloc (CModem *pModem, const char *strCmd);

private:
    CBitAlloc (const CBitAlloc &cBitAlloc);
    CBitAlloc &operator = (const CBitAlloc &cBitAlloc);

private:
    CParser *BitAllocUpCallback (void);
    CParser *BitAllocDownUpCallback (void);
    CParser *BitAllocDownCallback (void);
    CParser *TonesCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eBitAllocUp,
        eBitAllocDown
    };

private:
    bool m_bToneValid;
    CTone m_cTone;

private:
    CRegExList m_cBitAllocUp;
    CRegExList m_cBitAllocDown;
    CRegExList m_cTones;
}; // class CBitAlloc

class CRxSNR : public CPromptParser<CRxSNR>
{
public:
    CRxSNR (CModem *pModem, const char *strCmd);

private:
    CRxSNR (const CRxSNR &cRxSNR);
    CRxSNR &operator = (const CRxSNR &cRxSNR);

private:
    CParser *RxSNRCallback (void);
    CParser *TonesCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eRxSNR
    };

private:
    bool m_bToneValid;
    CTone m_cTone;

private:
    CRegExList m_cRxSNR;
    CRegExList m_cTones;
}; // class CRxSNR

class CReboot : public CPromptParser<CReboot>
{
public:
    CReboot (CModem *pModem, const char *strCmd);

private:
    CReboot (const CReboot &cReboot);
    CReboot &operator = (const CReboot &cReboot);

private:
    CParser *PromptCallback (void);
}; // class CReboot

class CExit : public CPromptParser<CExit>
{
public:
    CExit (CModem *pModem, const char *strCmd);

private:
    CExit (const CExit &cExit);
    CExit &operator = (const CExit &cExit);

private:
    CParser *PromptCallback (void);
}; // class CExit

} // namespace NAr7
//! @endcond
#endif // __DSLTOOL_AR7_H__
//_oOo_
