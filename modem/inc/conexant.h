/*!
 * @file        conexant.h
 * @brief       conexant modem definitions
 * @details     parser for conexant based modems, tested with:
 * @n           Sphairon AR800
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        22.10.2015
 *
 * @note        This file is partly generated from ar7.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_CONEXANT_H__
#define __DSLTOOL_CONEXANT_H__

#include "regex-parser.h"

//! @cond
namespace NConexant
{

class CConexant : public CModem
{
public:
    CConexant (CDslData &cData,
            CProtocol *pProtocol);

private:
    CConexant (const CConexant &cConexant);
    CConexant &operator = (const CConexant &cConexant);

public:
    virtual void Init (ECommand eCommand);

private:
    class CBandplanData
    {
    private:
        CBandplanData (const char *strVersion,
                const char *strAnnex,
                CBandplan::EBandplan eBandplan);
        CBandplanData (const CBandplanData &cBandplanData);
        CBandplanData &operator = (const CBandplanData &cBandplanData);

    public:
        static CBandplan::EBandplan Get (const char *strVersion,
                const char *strAnnex);

    private:
        const char *m_strVersion;
        const char *m_strAnnex;
        CBandplan::EBandplan m_eBandplan;

    private:
        static const CBandplanData g_acList[];
    }; // class CBandplanData

public:
    static CBandplan::EBandplan Bandplan (const char *strVersion,
            const char *strAnnex);

//begin user code
//end user code
}; // class CConexant

class CLoginPass : public CLineParser
{
public:
    CLoginPass (CModem *pModem, const char *strPass);

private:
    CLoginPass (const CLoginPass &cLoginPass);
    CLoginPass &operator = (const CLoginPass &cLoginPass);

public:
    virtual CParser *ParseLine (const char *strLine);

private:
    const char *m_strPass;
}; // class CLoginPass

template <class T> class CPromptParser : public CRegExCmdParser<T, CLineParser>
{
protected:
    CPromptParser (CModem *pModem, const char *strCmd);

private:
    CPromptParser (const CPromptParser &cPromptParser);
    CPromptParser &operator = (const CPromptParser &cPromptParser);

protected:
    CRegExList m_cPrompt;
}; // class CPromptParser

class CStatus : public CPromptParser<CStatus>
{
public:
    CStatus (CModem *pModem, const char *strCmd);

private:
    CStatus (const CStatus &cStatus);
    CStatus &operator = (const CStatus &cStatus);

private:
    CParser *PromptCallback (void);
}; // class CStatus

class CGeneral : public CPromptParser<CGeneral>
{
public:
    CGeneral (CModem *pModem, const char *strCmd);

private:
    CGeneral (const CGeneral &cGeneral);
    CGeneral &operator = (const CGeneral &cGeneral);

private:
    CParser *PromptCallback (void);
}; // class CGeneral

class CGeneralAdsl : public CPromptParser<CGeneralAdsl>
{
public:
    CGeneralAdsl (CModem *pModem, const char *strCmd);

private:
    CGeneralAdsl (const CGeneralAdsl &cGeneralAdsl);
    CGeneralAdsl &operator = (const CGeneralAdsl &cGeneralAdsl);

private:
    CParser *UpNoisemarginCallback (void);
    CParser *UpNoisemarginDatCallback (void);
    CParser *DownNoisemarginCallback (void);
    CParser *DownNoisemarginDatCallback (void);
    CParser *UpAttenuationCallback (void);
    CParser *UpAttenuationDatCallback (void);
    CParser *DownAttenuationCallback (void);
    CParser *DownAttenuationDatCallback (void);
    CParser *UpTxPowerCallback (void);
    CParser *UpTxPowerDatCallback (void);
    CParser *UpCRCCallback (void);
    CParser *UpCRCDatCallback (void);
    CParser *DownCRCCallback (void);
    CParser *DownCRCDatCallback (void);
    CParser *UpFECCallback (void);
    CParser *UpFECDatCallback (void);
    CParser *DownFECCallback (void);
    CParser *DownFECDatCallback (void);
    CParser *UpHECCallback (void);
    CParser *UpHECDatCallback (void);
    CParser *DownHECCallback (void);
    CParser *DownHECDatCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eUpNoisemargin,
        eDownNoisemargin,
        eUpAttenuation,
        eDownAttenuation,
        eUpTxPower,
        eUpCRC,
        eDownCRC,
        eUpFEC,
        eDownFEC,
        eUpHEC,
        eDownHEC
    };

private:
    long m_nTemp0;
    double m_fTemp0;

private:
    CRegExList m_cUpNoisemargin;
    CRegExList m_cUpNoisemarginDat;
    CRegExList m_cDownNoisemargin;
    CRegExList m_cDownNoisemarginDat;
    CRegExList m_cUpAttenuation;
    CRegExList m_cUpAttenuationDat;
    CRegExList m_cDownAttenuation;
    CRegExList m_cDownAttenuationDat;
    CRegExList m_cUpTxPower;
    CRegExList m_cUpTxPowerDat;
    CRegExList m_cUpCRC;
    CRegExList m_cUpCRCDat;
    CRegExList m_cDownCRC;
    CRegExList m_cDownCRCDat;
    CRegExList m_cUpFEC;
    CRegExList m_cUpFECDat;
    CRegExList m_cDownFEC;
    CRegExList m_cDownFECDat;
    CRegExList m_cUpHEC;
    CRegExList m_cUpHECDat;
    CRegExList m_cDownHEC;
    CRegExList m_cDownHECDat;
}; // class CGeneralAdsl

class CMain2 : public CPromptParser<CMain2>
{
public:
    CMain2 (CModem *pModem, const char *strCmd);

private:
    CMain2 (const CMain2 &cMain2);
    CMain2 &operator = (const CMain2 &cMain2);

private:
    CParser *PromptCallback (void);
}; // class CMain2

class CStatus2 : public CPromptParser<CStatus2>
{
public:
    CStatus2 (CModem *pModem, const char *strCmd);

private:
    CStatus2 (const CStatus2 &cStatus2);
    CStatus2 &operator = (const CStatus2 &cStatus2);

private:
    CParser *PromptCallback (void);
}; // class CStatus2

class CAdsl : public CPromptParser<CAdsl>
{
public:
    CAdsl (CModem *pModem, const char *strCmd);

private:
    CAdsl (const CAdsl &cAdsl);
    CAdsl &operator = (const CAdsl &cAdsl);

private:
    CParser *OpVersionCallback (void);
    CParser *OpVersionDatCallback (void);
    CParser *OpAnnexCallback (void);
    CParser *OpAnnexDatCallback (void);
    CParser *ModemStateCallback (void);
    CParser *ModemStateDatCallback (void);
    CParser *UpRateCallback (void);
    CParser *UpRateDatCallback (void);
    CParser *DownRateCallback (void);
    CParser *DownRateDatCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eOpVersion,
        eOpAnnex,
        eModemState,
        eChannelMode,
        eUpRate,
        eDownRate
    };

private:
    long m_nTemp0;
    CString m_strTemp0;
    CString m_strOpVersion;
    CString m_strOpAnnex;
    bool m_bOpVersion;
    bool m_bOpAnnex;

private:
    CRegExList m_cOpVersion;
    CRegExList m_cOpVersionDat;
    CRegExList m_cOpAnnex;
    CRegExList m_cOpAnnexDat;
    CRegExList m_cModemState;
    CRegExList m_cModemStateDat;
    CRegExList m_cUpRate;
    CRegExList m_cUpRateDat;
    CRegExList m_cDownRate;
    CRegExList m_cDownRateDat;
}; // class CAdsl

class CMain3 : public CPromptParser<CMain3>
{
public:
    CMain3 (CModem *pModem, const char *strCmd);

private:
    CMain3 (const CMain3 &cMain3);
    CMain3 &operator = (const CMain3 &cMain3);

private:
    CParser *PromptCallback (void);
}; // class CMain3

class CQuit : public CPromptParser<CQuit>
{
public:
    CQuit (CModem *pModem, const char *strCmd);

private:
    CQuit (const CQuit &cQuit);
    CQuit &operator = (const CQuit &cQuit);

private:
    CParser *PromptCallback (void);
}; // class CQuit

class CQuitYes : public CPromptParser<CQuitYes>
{
public:
    CQuitYes (CModem *pModem, const char *strCmd);

private:
    CQuitYes (const CQuitYes &cQuitYes);
    CQuitYes &operator = (const CQuitYes &cQuitYes);

private:
    CParser *PromptCallback (void);
}; // class CQuitYes

} // namespace NConexant
//! @endcond
#endif // __DSLTOOL_CONEXANT_H__
//_oOo_
