/*!
 * @file        openwrt.h
 * @brief       openwrt modem definitions
 * @details     parser for opnewrt based modems, tested with:
 * @n           Technicolor DGA 4132
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        21.08.2018
 *
 * @note        This file is partly generated from openwrt.xml
 * @n           Edit only the parts marked with begin/end user code
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_OPENWRT_H__
#define __DSLTOOL_OPENWRT_H__

#include "regex-parser.h"

//! @cond
namespace NOpenwrt
{

class COpenwrt : public CModem
{
public:
    COpenwrt (CDslData &cData,
            CProtocol *pProtocol);

private:
    COpenwrt (const COpenwrt &cOpenwrt);
    COpenwrt &operator = (const COpenwrt &cOpenwrt);

public:
    virtual void Init (ECommand eCommand);

private:
    class CBandplanData
    {
    private:
        CBandplanData (const char *strMode,
                const char *strAnnex,
                const char *strProfile,
                CBandplan::EBandplan eBandplan);
        CBandplanData (const CBandplanData &cBandplanData);
        CBandplanData &operator = (const CBandplanData &cBandplanData);

    public:
        static CBandplan::EBandplan Get (const char *strMode,
                const char *strAnnex,
                const char *strProfile);

    private:
        const char *m_strMode;
        const char *m_strAnnex;
        const char *m_strProfile;
        CBandplan::EBandplan m_eBandplan;

    private:
        static const CBandplanData g_acList[];
    }; // class CBandplanData

public:
    static CBandplan::EBandplan Bandplan (const char *strMode,
            const char *strAnnex,
            const char *strProfile);

//begin user code
//end user code
}; // class COpenwrt

class CLoginUser : public CParser
{
public:
    CLoginUser (CModem *pModem, const char *strUser);

private:
    CLoginUser (const CLoginUser &cLoginUser);
    CLoginUser &operator = (const CLoginUser &cLoginUser);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strUser;
}; // class CLoginUser

class CLoginPass : public CParser
{
public:
    CLoginPass (CModem *pModem, const char *strPass);

private:
    CLoginPass (const CLoginPass &cLoginPass);
    CLoginPass &operator = (const CLoginPass &cLoginPass);

public:
    virtual CParser *Parse (const char *strRecv, size_t nRecv);

private:
    const char *m_strPass;
}; // class CLoginPass

template <class T> class CPromptParser : public CRegExCmdParser<T, CLineParser>
{
protected:
    CPromptParser (CModem *pModem, const char *strCmd);

private:
    CPromptParser (const CPromptParser &cPromptParser);
    CPromptParser &operator = (const CPromptParser &cPromptParser);

protected:
    CRegExList m_cPrompt;
}; // class CPromptParser

class CInfoStats : public CPromptParser<CInfoStats>
{
public:
    CInfoStats (CModem *pModem, const char *strCmd);

private:
    CInfoStats (const CInfoStats &cInfoStats);
    CInfoStats &operator = (const CInfoStats &cInfoStats);

private:
    CParser *ModemStateCallback (void);
    CParser *AdslRateCallback (void);
    CParser *VdslRateCallback (void);
    CParser *VdslMaxCallback (void);
    CParser *OperationModeAnnexCallback (void);
    CParser *OperationModeCallback (void);
    CParser *AdslChannelModeCallback (void);
    CParser *VdslProfileCallback (void);
    CParser *NoisemarginCallback (void);
    CParser *AttenuationCallback (void);
    CParser *PowerCallback (void);
    CParser *TotalCallback (void);
    CParser *LastDayCallback (void);
    CParser *Last15minCallback (void);
    CParser *PrevDayCallback (void);
    CParser *Prev15minCallback (void);
    CParser *LinkUpCallback (void);
    CParser *ErrorsHECCallback (void);
    CParser *ErrorsFECCallback (void);
    CParser *ErrorsCRCCallback (void);
    CParser *ErrorsCRCDownCallback (void);
    CParser *DummySESCallback (void);
    CParser *DummySESDownCallback (void);
    CParser *ErrorSecLastDayCallback (void);
    CParser *ErrorSecLast15minCallback (void);
    CParser *ErrorSecDownLastDayCallback (void);
    CParser *ErrorSecDownLast15minCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eTotal,
        eLastDay,
        eLast15min,
        ePrevDay,
        ePrev15min,
        eLinkUp
    };

private:
    long m_nTemp0;
    long m_nTemp1;
    long m_nTemp2;
    long m_nTemp3;
    double m_fTemp0;
    double m_fTemp1;
    CString m_strTemp0;
    CString m_strTemp1;
    CString m_strTemp2;
    bool m_bModeValid;
    bool m_bAnnexValid;
    bool m_bProfileValid;
    CString m_strMode;
    CString m_strAnnex;
    CString m_strProfile;
    long m_nBandwidthUp;
    long m_nBandwidthDown;

private:
    CRegExList m_cModemState;
    CRegExList m_cAdslRate;
    CRegExList m_cVdslRate;
    CRegExList m_cVdslMax;
    CRegExList m_cOperationModeAnnex;
    CRegExList m_cOperationMode;
    CRegExList m_cAdslChannelMode;
    CRegExList m_cVdslProfile;
    CRegExList m_cNoisemargin;
    CRegExList m_cAttenuation;
    CRegExList m_cPower;
    CRegExList m_cTotal;
    CRegExList m_cLastDay;
    CRegExList m_cLast15min;
    CRegExList m_cPrevDay;
    CRegExList m_cPrev15min;
    CRegExList m_cLinkUp;
    CRegExList m_cErrorsHEC;
    CRegExList m_cErrorsFEC;
    CRegExList m_cErrorsCRC;
    CRegExList m_cErrorsCRCDown;
    CRegExList m_cDummySES;
    CRegExList m_cDummySESDown;
    CRegExList m_cErrorSec;
    CRegExList m_cErrorSecDown;
}; // class CInfoStats

class CInfoVendor : public CPromptParser<CInfoVendor>
{
public:
    CInfoVendor (CModem *pModem, const char *strCmd);

private:
    CInfoVendor (const CInfoVendor &cInfoVendor);
    CInfoVendor &operator = (const CInfoVendor &cInfoVendor);

private:
    CParser *VendorCallback (void);
    CParser *PromptCallback (void);

private:
    long m_nTemp0;
    CString m_strTemp0;

private:
    CRegExList m_cVendor;
}; // class CInfoVendor

class CInfoBits : public CPromptParser<CInfoBits>
{
public:
    CInfoBits (CModem *pModem, const char *strCmd);

private:
    CInfoBits (const CInfoBits &cInfoBits);
    CInfoBits &operator = (const CInfoBits &cInfoBits);

private:
    CParser *BitAllocCallback (void);
    CParser *TonesCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eBitAlloc
    };

private:
    bool m_bToneValid;
    CTone m_cTone;

private:
    CRegExList m_cBitAlloc;
    CRegExList m_cTones;
}; // class CInfoBits

class CInfoSNR : public CPromptParser<CInfoSNR>
{
public:
    CInfoSNR (CModem *pModem, const char *strCmd);

private:
    CInfoSNR (const CInfoSNR &cInfoSNR);
    CInfoSNR &operator = (const CInfoSNR &cInfoSNR);

private:
    CParser *SNRCallback (void);
    CParser *TonesCallback (void);
    CParser *PromptCallback (void);

private:
    enum EState
    {
        eUnknown = 0,
        eSNR
    };

private:
    double m_fTemp0;
    bool m_bToneValid;
    CTone m_cTone;

private:
    CRegExList m_cSNR;
    CRegExList m_cTones;
}; // class CInfoSNR

class CReboot : public CPromptParser<CReboot>
{
public:
    CReboot (CModem *pModem, const char *strCmd);

private:
    CReboot (const CReboot &cReboot);
    CReboot &operator = (const CReboot &cReboot);

private:
    CParser *PromptCallback (void);
}; // class CReboot

class CExit : public CPromptParser<CExit>
{
public:
    CExit (CModem *pModem, const char *strCmd);

private:
    CExit (const CExit &cExit);
    CExit &operator = (const CExit &cExit);

private:
    CParser *PromptCallback (void);
}; // class CExit

} // namespace NOpenwrt
//! @endcond
#endif // __DSLTOOL_OPENWRT_H__
//_oOo_
