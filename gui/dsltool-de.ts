<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>CGuiConfig</name>
    <message>
        <location filename="../src/gui-config.cpp" line="113"/>
        <source>auto</source>
        <translation>auto</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="114"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="115"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="118"/>
        <source>error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="119"/>
        <source>warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="120"/>
        <source>info</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="121"/>
        <source>debug</source>
        <translation type="unfinished">Debug</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="122"/>
        <source>trace</source>
        <translation type="unfinished">Trace</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="123"/>
        <source>extra</source>
        <translation type="unfinished">Extra</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="126"/>
        <source>none</source>
        <translation>Kein</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="127"/>
        <source>stderr</source>
        <translation type="unfinished">STDERR</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="128"/>
        <source>syslog</source>
        <translation>Sys-Log</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="129"/>
        <source>default.log</source>
        <translation>Default Datei</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="130"/>
        <source>file</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="857"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="875"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="892"/>
        <location filename="../src/gui-config.cpp" line="910"/>
        <location filename="../src/gui-config.cpp" line="928"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="859"/>
        <source>Capture files (*.pcap *.cap);; All files (*)</source>
        <translation>Capture Dateien (*.pcap *.cap);; Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="894"/>
        <source>Log files (*.log);;All files (*)</source>
        <translation>Log Dateien (*.log);;Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="912"/>
        <source>Socket (*.sock);; All files (*)</source>
        <translation>Socket (*.sock);; Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../src/gui-config.cpp" line="930"/>
        <source>Pid files (*.pid);;All files (*)</source>
        <translation>Pid Dateien (*.pid);;Alle Dateien (*)</translation>
    </message>
</context>
<context>
    <name>CGuiMain</name>
    <message>
        <location filename="../src/gui-main.cpp" line="107"/>
        <source>dsltool - close</source>
        <translation>dsltool - Schließen</translation>
    </message>
    <message>
        <location filename="../src/gui-main.cpp" line="108"/>
        <source>The configuration has been modified.
Do you want to save your changes?</source>
        <translation>Die Konfiguration wurde geändert.
Wollen Sie die Änderungen speichern?</translation>
    </message>
    <message>
        <location filename="../src/gui-main.cpp" line="166"/>
        <location filename="../src/gui-main.cpp" line="208"/>
        <source>Config files (*.conf);;All files (*)</source>
        <translation>Konfigurationsdateien (*.conf);;Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../src/gui-main.cpp" line="164"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../src/gui-main.cpp" line="206"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../src/gui-main.cpp" line="442"/>
        <source>not configured</source>
        <translation>Nicht konfiguriert</translation>
    </message>
    <message>
        <location filename="../src/gui-main.cpp" line="445"/>
        <source>ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="../src/gui-main.cpp" line="448"/>
        <source>running</source>
        <translation>Abfrage läuft</translation>
    </message>
    <message>
        <location filename="../src/gui-main.cpp" line="451"/>
        <source>error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>ConfigWindow</name>
    <message>
        <location filename="../../build/ui_config.h" line="316"/>
        <source>dsltool - Configuration</source>
        <translation>dsltool - Konfiguration</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="317"/>
        <source>Modem</source>
        <translation>Modem</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="318"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="320"/>
        <source>Modem type</source>
        <translation>Modemtyp</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="322"/>
        <source>Protocol</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="324"/>
        <source>Protocol type</source>
        <translation>Protokolltyp</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="327"/>
        <source>Extra parameter
(capture file for pcap-protcols)</source>
        <translation>Extra Parameter
(Capture Datei für pcap-Protcolle)</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="330"/>
        <location filename="../../build/ui_config.h" line="379"/>
        <location filename="../../build/ui_config.h" line="387"/>
        <location filename="../../build/ui_config.h" line="400"/>
        <location filename="../../build/ui_config.h" line="424"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="332"/>
        <source>Use extra parameter</source>
        <translation>Nutze Extra-Parameter</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="334"/>
        <source>Extra</source>
        <translation>Extra</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="335"/>
        <location filename="../../build/ui_config.h" line="336"/>
        <location filename="../../build/ui_config.h" line="404"/>
        <source>Host</source>
        <translation type="unfinished">Host</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="338"/>
        <source>Use non default port number</source>
        <translation>Nutze spezielle Portnummer</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="340"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="342"/>
        <source>Name or IP address</source>
        <translation>Name oder IP-Adresse</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="345"/>
        <source>IP port number</source>
        <translation>IP-Portnummer</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="348"/>
        <source>Ether type</source>
        <translation>Ether-Typ</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="351"/>
        <source>Use non default ether type</source>
        <translation>Nutze speziellen Ether-Typ</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="353"/>
        <source>Ethtype</source>
        <translation>Ether-Typ</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="354"/>
        <source>Login</source>
        <translation>Einloggen</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="355"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="356"/>
        <location filename="../../build/ui_config.h" line="361"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="358"/>
        <source>User name</source>
        <translation>Benutzer-Name</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="363"/>
        <source>&amp;Modem</source>
        <translation>&amp;Modem</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="364"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="366"/>
        <source>Output file path</source>
        <translation>Ausgabedateipfad</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="369"/>
        <source>Use non default path</source>
        <translation>Nutze speziellen Pfad</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="371"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="373"/>
        <source>Use non default prefix</source>
        <translation>Nutze speziellen Prefix</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="375"/>
        <source>Prefix</source>
        <translation>Prefix</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="377"/>
        <source>Output file name prefix</source>
        <translation>Ausgabedatein-Namensprefix</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="380"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="382"/>
        <source>Log file name</source>
        <translation>Logdatei-Name</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="385"/>
        <source>Log type</source>
        <translation>Log-Typ</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="389"/>
        <location filename="../../build/ui_config.h" line="391"/>
        <source>Log level</source>
        <translation>Log-Stufe</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="392"/>
        <source>&amp;File/Log</source>
        <translation>&amp;Datei/Log</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="393"/>
        <source>Collect</source>
        <translation type="unfinished">Collect</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="395"/>
        <source>Collect host name or IP address</source>
        <translation>Collect Server Name oder IP-Adresse</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="398"/>
        <source>Collect socket name</source>
        <translation>Collect Socket Name</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="402"/>
        <source>Use non default host</source>
        <translation>Nutze speziellen Server</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="406"/>
        <source>Use non default socket</source>
        <translation>Nutze speziellen Socket</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="408"/>
        <source>Socket</source>
        <translation>Socket</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="409"/>
        <source>Daemon</source>
        <translation>Dienst</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="411"/>
        <source>Use non default pid file</source>
        <translation>Nutze spezielle Pid-Datei</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="413"/>
        <source>Pid file</source>
        <translation>Pid-Datei</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="415"/>
        <source>Put daemon to background</source>
        <translation>Stelle Dienst in Hintergrund</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="417"/>
        <source>Background</source>
        <translation>Hintergrund</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="419"/>
        <source>Daemon poll interval</source>
        <translation>Dienst-Abfrage-Intervall</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="422"/>
        <source>Daemon pid file name</source>
        <translation>Dienst Pid-Dateiname</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="426"/>
        <source>Use non default poll interval</source>
        <translation>Nutze spezielles Abfrage-Intervall</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="428"/>
        <source>Interval</source>
        <translation>Intervall</translation>
    </message>
    <message>
        <location filename="../../build/ui_config.h" line="429"/>
        <source>&amp;Collect/Daemon</source>
        <translation>&amp;Collect/Dienst</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../build/ui_main.h" line="534"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="535"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="536"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="537"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="538"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="539"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="540"/>
        <source>Save &amp;as...</source>
        <translation>Speichern &amp;unter...</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="541"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="542"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="543"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Aktualisieren</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="544"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="545"/>
        <source>&amp;Configuration...</source>
        <translation>&amp;Konfiguration</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="546"/>
        <source>Re&amp;sync</source>
        <translation>&amp;Synchronisieren</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="547"/>
        <source>Re&amp;boot</source>
        <translation>&amp;Neustart</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="548"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="549"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="550"/>
        <source>&amp;About dsltool</source>
        <translation>&amp;Über dsltool</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="551"/>
        <source>DSL</source>
        <translation>DSL</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="552"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="553"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="554"/>
        <source>Op.mode</source>
        <translation>Op.Modus</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="555"/>
        <source>Ch.mode</source>
        <translation>Kanalmodus</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="558"/>
        <source>Bandwidth</source>
        <translation>Bandbreite</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="559"/>
        <source>kBit/s</source>
        <translation>kBit/s</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="560"/>
        <source>Cells/s</source>
        <translation>Zellen/s</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="561"/>
        <source>Max. kBit/s</source>
        <translation>Max. kBit/s</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="562"/>
        <location filename="../../build/ui_main.h" line="574"/>
        <source>Upstream</source>
        <translation>Upstream</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="563"/>
        <location filename="../../build/ui_main.h" line="575"/>
        <source>Downstream</source>
        <translation>Downstream</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="570"/>
        <source>Line</source>
        <translation>Leitung</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="571"/>
        <source>Attenuation</source>
        <translation>Dämpfung</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="572"/>
        <source>Noisemargin</source>
        <translation type="unfinished">Rauschabst.</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="573"/>
        <source>Tx Power</source>
        <translation>Tx Leistung</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="582"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="583"/>
        <source>CRC</source>
        <translation>CRC</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="584"/>
        <source>FEC</source>
        <translation>FEC</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="585"/>
        <source>HEC</source>
        <translation>HEC</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="586"/>
        <source>Tx</source>
        <translation>Tx</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="587"/>
        <source>Rx</source>
        <translation>Rx</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="594"/>
        <source>ATM</source>
        <translation>ATM</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="595"/>
        <source>VPI/VCI</source>
        <translation>VPI/VCI</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="598"/>
        <source>ATU</source>
        <translation>ATU</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="599"/>
        <source>Vendorspec</source>
        <translation>Herst.spez.</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="600"/>
        <source>Vendor</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="601"/>
        <source>Revision</source>
        <translation>Revision</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="602"/>
        <source>ATU-C</source>
        <translation>ATU-C</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="603"/>
        <source>ATU-R</source>
        <translation>ATU-R</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="610"/>
        <source>Failure</source>
        <translation>Ausfälle</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="611"/>
        <source>Error s / 15min</source>
        <translation>Fehler s / 15min</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="614"/>
        <source>Error s / day</source>
        <translation>Fehler s / Tag</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="615"/>
        <source>&amp;Info</source>
        <translation>&amp;Information</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="616"/>
        <source>&amp;Bitalloc</source>
        <translation>&amp;Bitbelegung</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="617"/>
        <source>S&amp;NR</source>
        <translation>S&amp;NR</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="618"/>
        <source>&amp;Char</source>
        <translation>&amp;Char</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="619"/>
        <source>S&amp;pectrum</source>
        <translation>S&amp;pektrum</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="620"/>
        <source>S&amp;tatistics</source>
        <translation>S&amp;tatistik</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="621"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="622"/>
        <source>&amp;Action</source>
        <translation>&amp;Aktion</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="623"/>
        <source>&amp;Modem</source>
        <translation>&amp;Modem</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="624"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="625"/>
        <source>&amp;Language</source>
        <translation>&amp;Sprache</translation>
    </message>
    <message>
        <location filename="../../build/ui_main.h" line="626"/>
        <source>&amp;?</source>
        <translation>&amp;?</translation>
    </message>
</context>
</TS>
