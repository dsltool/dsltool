/*!
 * @file        demo.cpp
 * @brief       demo protocol implementation
 * @details     demo protocol class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-demo-export.h"
#define LIBDSLTOOL_PROTOCOL_EXPORT LIBDSLTOOL_DEMO_EXPORT
//! @endcond

#include "config.h"

#include "demo.h"

/*!
 * @implements  ProtocolVersion
 */
void LIBDSLTOOL_PROTOCOL_EXPORT ProtocolVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

/*!
 * @implements  ProtocolCreate
 */
CProtocol LIBDSLTOOL_PROTOCOL_EXPORT *ProtocolCreate (const char *strHost,
        int nPort,
        CProtocol::EEthType eEthType,
        CAuth *pAuth,
        const char *strExtra)
{
    (void)strHost;  // unused param
    (void)nPort;    // unused param
    (void)eEthType; // unused param
    (void)pAuth; // unused param
    (void)strExtra; // unused param

    return new CDemo ();
}

/*!
 * @fn          CDemo::CDemo ()
 * @brief       constructor
 */
CDemo::CDemo ()
: CProtocol ("demo", 0, eAuto, NULL)
{
}

/*!
 * @fn          bool CDemo::Open (void)
 * @brief       open protocol
 * @details     does nothing
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CDemo::Open (void)
{
    return true;
}

/*!
 * @fn          void CDemo::Close (void)
 * @brief       close protocol
 * @details     does nothing
 */
void CDemo::Close (void)
{
}

/*!
 * @fn          bool CDemo::Process (void)
 * @brief       protocol process loop
 * @details     does nothing
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CDemo::Process (void)
{
    return true;
}

/*!
 * @fn          bool CDemo::Capture (const char *strFile)
 * @brief       does nothing
 * @param[in]   strFile
 *              unused
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              create capture failure
 */

bool CDemo::Capture (const char *strFile)
{
    (void)strFile;

    return true;
}

/*!
 * @fn          bool CDemo::SendBuf (const char *pBuffer, size_t nSize)
 * @brief       send data
 * @details     does nothing
 * @param[in]   pBuffer
 *              unused
 * @param[in]   nSize
 *              unused
 * @return      success
 * @retval      true
 *              o.k.
 */
bool CDemo::SendBuf (const char *pBuffer, size_t nSize)
{
    (void)pBuffer;
    (void)nSize;

    return true;
}

//_oOo_
