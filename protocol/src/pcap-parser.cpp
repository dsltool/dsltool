/*!
 * @file        pcap-parser.cpp
 * @brief       pcap parser class implementation
 * @details     process capture file
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        14.04.2019
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @fn          CPcapParser<B>::CPcapParser (const char *strFile, int nPort, CAuth *pAuth)
 * @brief       constructor
 * @param[in]   strFile
 *              capture file name
 * @param[in]   nPort
 *              port number
 * @param[in]   pAuth
 *              authenticator
 */
template <class B> CPcapParser<B>::CPcapParser (const char *strFile, int nPort, CAuth *pAuth)
: B ("pcap", nPort, CProtocol::eAuto, pAuth)
, m_strFile (strFile)
, m_pPcap (NULL)
, m_nLink (DLT_NULL)
, m_bFirst (true)
{
    memset (m_auMAC, 0, sizeof (m_auMAC));
}

/*!
 * @fn          CPcapParser<B>::~CPcapParser (void)
 * @brief       destructor
 */
template <class B> CPcapParser<B>::~CPcapParser (void)
{
}

/*!
 * @fn          bool CPcapParser<B>::Open (void)
 * @brief       open capture
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              capture open failed
 */
template <class B> bool CPcapParser<B>::Open (void)
{
    bool bRet = true;

    m_bFirst = true;
    memset (m_auMAC, 0, sizeof (m_auMAC));

    bRet = B::Open ();

    if (bRet)
    {
        m_pPcap = pcap_open_offline (m_strFile, m_strErr);

        if (!m_pPcap)
        {
            bRet = false;
        }
    }

    return bRet;
}

/*!
 * @fn          void CPcapParser<B>::Close (void)
 * @brief       close capture
 */
template <class B> void CPcapParser<B>::Close (void)
{
    if (NULL != m_pPcap)
    {
        pcap_close (m_pPcap);
        m_pPcap = NULL;
    }

    B::Close ();
}

/*!
 * @fn          bool CPcapParser<B>::Process (void)
 * @brief       process http protocol
 * @details     calls parser via @ref PcapHandler
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              process failed
 */
template <class B> bool CPcapParser<B>::Process (void)
{
    m_nLink = pcap_datalink (m_pPcap);
    pcap_loop (m_pPcap, -1, _PcapHandler, (u_char*)this);

    return true;
}

#ifdef ENABLE_CAPTURE
/*!
 * @fn          bool CPcapParser<B>::Capture (const char *strFile)
 * @brief       does nothing
 * @param[in]   strFile
 *              unused
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              create capture failure
 */

template <class B> bool CPcapParser<B>::Capture (const char *strFile)
{
    (void)strFile;

    return true;
}
#endif // ENABLE_CAPTURE

/*!
 * @fn          bool CPcapParser<B>::SendBuf (const char *pBuffer, size_t nSize)
 * @brief       send data
 * @details     output to trace log
 * @param[in]   pBuffer
 *              data to send
 * @param[in]   nSize
 *              data length
 * @return      success
 * @retval      true
 *              o.k.
 */
template <class B> bool CPcapParser<B>::SendBuf (const char *pBuffer, size_t nSize)
{
    CString strPrint (pBuffer, nSize);
    strPrint.Printable ();

    LogExtraEnter ("(\"%s\")\n", (const char*)strPrint);

    LogExtraLeave ("");
    return true;
}

/*!
 * @fn          void CPcapParser<B>::_PcapHandler (u_char *pUser,
 *                      const struct pcap_pkthdr *pHeader,
 *                      const u_char *pBytes)
 * @brief       capture packet handler
 * @details     static function
 * @param[in]   pUser
 *              cast to this pointer
 * @param[in]   pHeader
 *              packet header pointer
 * @param[in]   pBytes
 *              packet data
 */
template <class B> void CPcapParser<B>::_PcapHandler (u_char *pUser,
        const struct pcap_pkthdr *pHeader, const u_char *pBytes)
{
    CPcapParser<B> *pThis = (CPcapParser<B>*)pUser;
    pThis->PcapHandler (pHeader, pBytes);
}

/*!
 * @fn          void CPcapParser<B>::PcapHandler (const struct pcap_pkthdr *pHeader,
 *                      const u_char *pBytes)
 * @brief       capture packet handler
 * @details     decode either ethernet or linux socket link layer packets
 * @param[in]   pHeader
 *              packet header pointer
 * @param[in]   pBytes
 *              packet size
 */
template <class B> void CPcapParser<B>::PcapHandler (const struct pcap_pkthdr *pHeader,
        const u_char *pBytes)
{
    switch (m_nLink)
    {
        case DLT_EN10MB:
            DecodeEth (pBytes, pHeader->caplen);
            break;
        case DLT_LINUX_SLL:
            DecodeSLL (pBytes, pHeader->caplen);
            break;
    }
}

/*!
 * @fn          void CPcapParser<B>::DecodeEth (TPData pBytes, size_t nLength)
 * @brief       decode ethernet packet
 * @param[in]   pBytes
 *              ethernet packet data
 * @param[in]   nLength
 *              packet length
 */
template <class B> void CPcapParser<B>::DecodeEth (TPData pBytes, size_t nLength)
{
    struct ether_header *pEth = (struct ether_header *) pBytes;
    size_t nOffset = sizeof (struct ether_header);
    if (m_bFirst)
    {
        m_bFirst = false;
        memcpy (m_auMAC, pEth->ether_dhost, sizeof (m_auMAC));
    }

    if (0 == memcmp (m_auMAC, pEth->ether_shost, sizeof (m_auMAC)))
    {
        switch (ntohs (pEth->ether_type))
        {
            case ETHERTYPE_IP:
                DecodeIP ((TPData)(pBytes + nOffset), nLength - nOffset);
                break;
            case ETHERTYPE_IPV6:
                DecodeIPv6 ((TPData)(pBytes + nOffset), nLength - nOffset);
                break;
        }
    }
}

/*!
 * @fn          void CPcapParser<B>::DecodeSLL (TPData pBytes, size_t nLength)
 * @brief       decode linux socket link layer packet
 * @param[in]   pBytes
 *              socket link layer packet data
 * @param[in]   nLength
 *              packet length
 */
template <class B> void CPcapParser<B>::DecodeSLL (TPData pBytes, size_t nLength)
{
    struct sllhdr {
        u_int16_t type;
        u_int16_t arphrd;
        u_int16_t reserved;
        u_int8_t addr[8];
        u_int16_t ether_type;
    } *pSLL = (struct sllhdr *) pBytes;
    size_t nOffset = sizeof (struct sllhdr);

    if (0 == pSLL->type)
    {
        switch (ntohs (pSLL->ether_type))
        {
            case ETHERTYPE_IP:
                DecodeIP ((TPData)(pBytes + nOffset), nLength - nOffset);
                break;
            case ETHERTYPE_IPV6:
                DecodeIPv6 ((TPData)(pBytes + nOffset), nLength - nOffset);
                break;
        }
    }
}

/*!
 * @fn          void CPcapParser<B>::DecodeIP (TPData pBytes, size_t nLength)
 * @brief       decode IP packet
 * @param[in]   pBytes
 *              IP packet data
 * @param[in]   nLength
 *              packet length
 */
template <class B> void CPcapParser<B>::DecodeIP (TPData pBytes, size_t nLength)
{
    struct iphdr *pIP = (struct iphdr*)pBytes;
    size_t nOffset = (size_t)(4 * pIP->ihl);

    (void)nLength;

    switch (pIP->protocol)
    {
        case  IPPROTO_TCP:
            DecodeTCP ((TPData)(pBytes + nOffset), ntohs(pIP->tot_len) - nOffset);
            break;
        case  IPPROTO_UDP:
        default:
            break;
    }
}

/*!
 * @fn          vvoid CPcapParser<B>::DecodeIPv6 (TPData pBytes, size_t nLength)
 * @brief       decode IPv6 packet
 * @param[in]   pBytes
 *              IP packet data
 * @param[in]   nLength
 *              packet length
 */
template <class B> void CPcapParser<B>::DecodeIPv6 (TPData pBytes, size_t nLength)
{
    struct ip6_hdr *pIPv6 = (struct ip6_hdr*)pBytes;
    struct ip6_ext *pExt = NULL;
    size_t nOffset = sizeof (struct ip6_hdr);
    u_int8_t u8Protocol = pIPv6->ip6_ctlun.ip6_un1.ip6_un1_nxt;
    size_t nPayload = (size_t)ntohs((u_int16_t)pIPv6->ip6_ctlun.ip6_un1.ip6_un1_plen);
    bool bLoop = true;

    (void)nLength;

    while (bLoop)
    {
        switch (u8Protocol)
        {
            case IPPROTO_TCP:
                bLoop = false;
                DecodeTCP ((TPData)(pBytes + nOffset), nPayload);
                break;
            case IPPROTO_HOPOPTS:
            case IPPROTO_ROUTING:
            case IPPROTO_FRAGMENT:
            case IPPROTO_DSTOPTS:
            {
                // next extension
                pExt = (struct ip6_ext*)(pBytes + nOffset);
                u8Protocol = pExt->ip6e_nxt;
                nOffset += (size_t)(8 * (pExt->ip6e_len + 1));
                nPayload -= (size_t)(8 * (pExt->ip6e_len + 1));
            }
                break;
            case IPPROTO_ESP:
            case IPPROTO_MH:
            case IPPROTO_UDP:
            case IPPROTO_ICMPV6:
            case IPPROTO_NONE:
            default:
                bLoop = false;
                break;
        }
    }
}

/*!
 * @fn          void CPcapParser<B>::DecodeTCP (TPData pBytes, size_t nLength)
 * @brief       decode TCIP packet
 * @param[in]   pBytes
 *              TCP packet data
 * @param[in]   nLength
 *              packet length
 */
template <class B> void CPcapParser<B>::DecodeTCP (TPData pBytes, size_t nLength)
{
    struct tcphdr *pTCP = (struct tcphdr*)pBytes;
    size_t nOffset = (size_t)(4 * pTCP->th_off);

    if (nLength > nOffset)
    {
        B::Receive ((char*)(pBytes + nOffset), nLength - nOffset);
    }
}

//_oOo_
