/*!
 * @file        http-pcap.cpp
 * @brief       http pcap class implementation
 * @details     process http protocol via capture file
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-pcap-http-export.h"
#define LIBDSLTOOL_PROTOCOL_EXPORT LIBDSLTOOL_PCAP_HTTP_EXPORT
//! @endcond

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include "http-pcap.h"
#include "log.h"
#include "pcap-parser.cpp"

/*!
 * @implements  ProtocolVersion
 */
void LIBDSLTOOL_PROTOCOL_EXPORT ProtocolVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

/*!
 * @implements  ProtocolCreate
 */
CProtocol LIBDSLTOOL_PROTOCOL_EXPORT *ProtocolCreate (const char *strHost,
        int nPort,
        CProtocol::EEthType eEthType,
        CAuth *pAuth,
        const char *strExtra)
{
    (void)strHost;      // unused param
    (void)nPort;        // unused param
    (void)eEthType;    // unused param

    return new CHttpPcap (strExtra, pAuth);
}

/*!
 * @fn          CHttpPcap::CHttpPcap (const char *strFile, CAuth *pAuth)
 * @brief       constructor
 * @param[in]   strFile
 *              capture file name
 * @param[in]   pAuth
 *              authenticator
 */
CHttpPcap::CHttpPcap (const char *strFile, CAuth *pAuth)
: CPcapParser<CHttpBase> (strFile, 80, pAuth)
{
}

//_oOo_
