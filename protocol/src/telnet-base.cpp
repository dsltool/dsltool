/*!
 * @file        telnet-base.cpp
 * @brief       telnet base class implementation
 * @details     telnet protocol class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>

#include "telnet-base.h"
#include "parser.h"
#include "log.h"
#include "txt.h"

namespace NStrErr
{
    STRING Telnet = "Telnet: %s\n";             //!< error text
} // NStrErr

const struct telnet_telopt_t CTelnetBase::s_atTelopts[] = {
    { TELNET_TELOPT_ECHO,           TELNET_WONT, TELNET_DO   },
    { TELNET_TELOPT_TTYPE,          TELNET_WILL, TELNET_DONT },
    { TELNET_TELOPT_COMPRESS2,      TELNET_WONT, TELNET_DO   },
    { TELNET_TELOPT_MSSP,           TELNET_WONT, TELNET_DO   },
    { -1, 0, 0 }
};

/*!
 * @fn          CTelnetBase::CTelnetBase (const char *strHost,
 *                      int nPort,
 *                      CProtocol::EEthType eEthType)
 * @brief       constructor
 * @param[in]   strHost
 *              host name or IP
 * @param[in]   nPort
 *              port number
 * @param[in]   eEthType
 *              protocol type
 * @param[in]   pAuth
 *              authenticator
 */
CTelnetBase::CTelnetBase (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth)
: CProtocol (strHost, nPort, eEthType, pAuth)
, m_pTelnet (NULL)
{
}

/*!
 * @fn          CTelnetBase::~CTelnetBase (void)
 * @brief       destructor
 */
CTelnetBase::~CTelnetBase (void)
{
    Close ();
}

/*!
 * @fn          bool CTelnetBase::Open (void)
 * @brief       open telnet protcol
 * @details     this function must be called by overloaded functions
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              protocol open failed
 */
bool CTelnetBase::Open (void)
{
    bool bRet = true;

    m_pTelnet = telnet_init (s_atTelopts, _EventHandler, 0, this);

    if (!m_pTelnet)
    {
        bRet = false;
    }
    return bRet;
}

/*!
 * @fn          void CTelnetBase::Close (void)
 * @brief       close telnet protcol
 * @details     this function must be called by overloaded functions
 */
void CTelnetBase::Close (void)
{
    if (m_pTelnet)
    {
        telnet_free (m_pTelnet);
        m_pTelnet = NULL;
    }
}

/*!
 * @fn          void CTelnetBase::Receive (const char *pBuffer, size_t nSize)
 * @brief       receive data
 * @param[in]   pBuffer
 *              received data
 * @param[in]   nSize
 *              data length
 */
void CTelnetBase::Receive (const char *pBuffer, size_t nSize)
{
    telnet_recv (m_pTelnet, pBuffer, nSize);
}

/*!
 * @fn          void CTelnetBase::_EventHandler (telnet_t *pTelnet,
 *                      telnet_event_t *pEvent, void *pUser)
 * @brief       telnet protocol event handler
 * @details     static function
 * @param[in]   pTelnet
 *              telnet pointer
 * @param[in]   pEvent
 *              telnet event data
 * @param[in]   pUser
 *              cast to this pointer
 */
void CTelnetBase::_EventHandler (telnet_t *pTelnet,
        telnet_event_t *pEvent, void *pUser)
{
        CTelnetBase *pThis = (CTelnetBase*)pUser;
        pThis->EventHandler (pTelnet, pEvent);
}

/*!
 * @fn          void CTelnetBase::EventHandler (telnet_t *pTelnet,
 *                      telnet_event_t *pEvent)
 * @brief       telnet protocol event handler
 * @details     calls parser
 * @param[in]   pTelnet
 *              telnet pointer
 * @param[in]   pEvent
 *              telnet event data
 */
void CTelnetBase::EventHandler (telnet_t *pTelnet,
        telnet_event_t *pEvent)
{
    (void)pTelnet;

    LogExtraEnter ("");

    switch (pEvent->type)
    {
    case TELNET_EV_DATA: // data received
        LogExtra ("\"%.*s\"\n", (int)pEvent->data.size, pEvent->data.buffer);
        fflush (stdout);
        Parse (pEvent->data.buffer, pEvent->data.size);
        break;
    case TELNET_EV_SEND: // data to be sent
        SendBuf (pEvent->data.buffer, pEvent->data.size);
        break;
    case TELNET_EV_WILL: // request to enable remote feature
        break;
    case TELNET_EV_WONT: // notification of disabling remote feature
        break;
    case TELNET_EV_DO: // request to enable local feature
        break;
    case TELNET_EV_DONT: // demand to disable local feature
        break;
    case TELNET_EV_TTYPE: // respond with our terminal type, if requested
        if (pEvent->ttype.cmd == TELNET_TTYPE_SEND)
        {
            telnet_ttype_is (m_pTelnet, "ANSI");
        }
        break;
    case TELNET_EV_SUBNEGOTIATION: // respond to particular subnegotiations
        break;
    case TELNET_EV_ERROR: // error
        LogError (NStrErr::Telnet, pEvent->error.msg);
        exit (1);
    default:
        break;
    }

    LogExtraLeave ("");
}

//_oOo_
