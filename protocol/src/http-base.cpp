/*!
 * @file        http-base.cpp
 * @brief       http base class implementation
 * @details     http protocol class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>

#include "http-base.h"
#include "log.h"
#include "txt.h"

namespace NStrErr
{
    STRING http = "HTTP: %s\n";             //!< error text
} // NStrErr

/*!
 * @fn          CHttpParser::CHttpParser (CHttpBase *pProtocol)
 * @brief       constructor
 * @details     set callbacks
 * @param[in]   pProtocol
 *              HTTP protocol pointer
 */
CHttpParser::CHttpParser (CProtocol *pProtocol)
: m_pProtocol (pProtocol)
, m_tParser ()
, m_tSettings ()
, m_eHeader (eHeaderUnknown)
, m_eContent (CParser::eContentUnknown)
{
    http_parser_init (&m_tParser, HTTP_RESPONSE);
    http_parser_settings_init (&m_tSettings);

    m_tParser.data = this;

    m_tSettings.on_message_begin = _MessageBegin;
    m_tSettings.on_header_field = _HeaderField;
    m_tSettings.on_header_value = _HeaderValue;
    m_tSettings.on_headers_complete = _HeadersComplete;
    m_tSettings.on_body = _Body;
    m_tSettings.on_message_complete = _MessageComplete;
}

/*!
 * @fn          void CHttpParser::Receive (const char *pData, size_t nSize)
 * @brief       parse stream
 * @param[in]   pData
 *              stream
 * @param[in]   nSize
 *              size of stream
 */
void CHttpParser::Receive (const char *pData, size_t nSize)
{
    http_parser_execute (&m_tParser, &m_tSettings, pData, nSize);
}

/*!
 * @fn          int CHttpParser::_MessageBegin (http_parser *pParser)
 * @brief       static message begin callback
 * @param[in]   pParser
 *              parser pointer
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::_MessageBegin (http_parser *pParser)
{
    CHttpParser *pThis = (CHttpParser*)(pParser->data);
    return pThis->MessageBegin ();
}

/*!
 * @fn          int CHttpParser::_HeaderField (http_parser *pParser,
 *                      const char *pData,
 *                      size_t nSize)
 * @brief       static header field callback
 * @param[in]   pParser
 *              parser pointer
 * @param[in]   pData
 *              header field
 * @param[in]   nSize
 *              size of header field
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::_HeaderField (http_parser *pParser,
        const char *pData,
        size_t nSize)
{
    CHttpParser *pThis = (CHttpParser*)(pParser->data);
    return pThis->HeaderField (pData, nSize);
}

/*!
 * @fn          int CHttpParser::_HeaderValue (http_parser *pParser,
 *                      const char *pData,
 *                      size_t nSize)
 * @brief       static header value callback
 * @param[in]   pParser
 *              parser pointer
 * @param[in]   pData
 *              header value
 * @param[in]   nSize
 *              size of header value
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::_HeaderValue (http_parser *pParser,
        const char *pData,
        size_t nSize)
{
    CHttpParser *pThis = (CHttpParser*)(pParser->data);
    return pThis->HeaderValue (pData, nSize);
}

/*!
 * @fn          int CHttpParser::_HeadersComplete (http_parser *pParser)
 * @brief       static header complete callback
 * @param[in]   pParser
 *              parser pointer
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::_HeadersComplete (http_parser *pParser)
{
    CHttpParser *pThis = (CHttpParser*)(pParser->data);
    return pThis->HeadersComplete ();
}

/*!
 * @fn          int CHttpParser::_Body (http_parser *pParser,
 *                      const char *pData,
 *                      size_t nSize)
 * @brief       static body callback
 * @param[in]   pParser
 *              parser pointer
 * @param[in]   pData
 *              body
 * @param[in]   nSize
 *              size of body
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::_Body (http_parser *pParser,
        const char *pData,
        size_t nSize)
{
    CHttpParser *pThis = (CHttpParser*)(pParser->data);
    return pThis->Body (pData, nSize);
}

/*!
 * @fn          int CHttpParser::_MessageComplete (http_parser *pParser)
 * @brief       static message complete callback
 * @param[in]   pParser
 *              parser pointer
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::_MessageComplete (http_parser *pParser)
{
    CHttpParser *pThis = (CHttpParser*)(pParser->data);
    return pThis->MessageComplete ();
}

/*!
 *  @fn          int CHttpParser::MessageBegin (void)
 * @brief       message begin callback
 * @details     reset header
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::MessageBegin (void)
{
    LogExtraEnter ("");

    m_eHeader = eHeaderUnknown;
    m_eContent = CParser::eContentUnknown;

#ifdef ENABLE_CAPTURE
    m_pProtocol->m_cCapture.Receive("HTTP/1.1 200 OK\r\n");
#endif // ENABLE_CAPTURE

    LogExtraLeave ("");
    return 0;
}

/*!
 * @fn          int CHttpParser::HeaderField (const char *pData, size_t nSize)
 * @brief       header field callback
 * @details     check for known header fields
 * @param[in]   pData
 *              header field
 * @param[in]   nSize
 *              size of header field
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::HeaderField (const char *pData, size_t nSize)
{
    LogExtraEnter ("(%ld\"%.*s\")\n", nSize, nSize, pData);

    m_eHeader = eHeaderUnknown;
    if (0 == strncasecmp (pData, "connection" , nSize))
    {
        m_eHeader = eConnection;
    } else if (0 == strncasecmp (pData, "content-length" , nSize))
    {
        m_eHeader = eContentLength;
    } else if (0 == strncasecmp (pData, "content-type" , nSize))
    {
        m_eHeader = eContentType;
    }
    LogExtraLeave ("");
    return 0;
}

/*!
 * @fn          int CHttpParser::HeaderValue (const char *pData, size_t nSize)
 * @brief       header value callback
 * @details     check for known header values
 * @param[in]   pData
 *              header value
 * @param[in]   nSize
 *              size of header field
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::HeaderValue (const char *pData, size_t nSize)
{
    LogExtraEnter ("(%ld\"%.*s\")\n", nSize, nSize, pData);

    switch (m_eHeader)
    {
        case eConnection:
#ifdef ENABLE_CAPTURE
            m_pProtocol->m_cCapture.Receive("Connection: ");
            m_pProtocol->m_cCapture.Receive(pData, nSize);
            m_pProtocol->m_cCapture.Receive("\r\n");
#endif // ENABLE_CAPTURE
            break;
        case eContentLength:
#ifdef ENABLE_CAPTURE
            m_pProtocol->m_cCapture.Receive("Content-length: ");
            m_pProtocol->m_cCapture.Receive(pData, nSize);
            m_pProtocol->m_cCapture.Receive("\r\n");
#endif // ENABLE_CAPTURE
            break;
        case eContentType:
            m_eContent = CParser::eContentUnknown;
            if (0 == strncasecmp (pData, "text/xml" , MIN (8, nSize)))
            {
                LogExtra ("ContentType text/xml\n");
                m_eContent = CParser::eTextXml;
            }
            else if (0 == strncasecmp (pData, "text/html" , MIN (9, nSize)))
            {
                LogExtra ("ContentType text/html\n");
                m_eContent = CParser::eTextHtml;
            }
#ifdef ENABLE_CAPTURE
            m_pProtocol->m_cCapture.Receive("Content-type: ");
            m_pProtocol->m_cCapture.Receive(pData, nSize);
            m_pProtocol->m_cCapture.Receive("\r\n");
#endif // ENABLE_CAPTURE
            break;
        default:
            break;
    }
    LogExtraLeave ("");
    return 0;
}

/*!
 * @fn          int CHttpParser::HeadersComplete (void)
 * @brief       header complete callback
 * @details     open SAX parser
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::HeadersComplete (void)
{
    LogExtraEnter ("");

    m_pProtocol->ParserOpen (m_eContent);
    m_pProtocol->m_cCapture.Receive("\r\n");

    LogExtraLeave ("");
    return 0;
}

/*!
 * @fn          int CHttpParser::Body (const char *pData, size_t nSize)
 * @brief       body callback
 * @details     pass data to SAX parser
 * @param[in]   pData
 *              body
 * @param[in]   nSize
 *              size of body
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::Body (const char *pData, size_t nSize)
{
    LogExtraEnter ("(%ld\"%.*s\")\n", nSize, nSize, pData);

    m_pProtocol->Parse (pData, nSize);

    LogExtraLeave ("");
    return 0;
}

/*!
 * @fn          int CHttpParser::MessageComplete (void)
 * @brief       message complete callback
 * @details     close SAX parser
 * @return      error code
 * @retval      0
 *              o.k.
 */
int CHttpParser::MessageComplete (void)
{
    LogExtraEnter ("");

    m_pProtocol->ParserClose ();

    LogExtraLeave ("");
    return 0;
}

/*!
 * @fn          CHttpBase::CHttpBase (const char *strHost,
 *                      int nPort,
 *                      CProtocol::EEthType eEthType)
 * @brief       constructor
 * @param[in]   strHost
 *              host name or IP
 * @param[in]   nPort
 *              port number
 * @param[in]   eEthType
 *              protocol type
 * @param[in]   pAuth
 *              authenticator
 */
CHttpBase::CHttpBase (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth)
: CProtocol (strHost, nPort, eEthType, pAuth)
, m_cHttpParser (this)
{
}

/*!
 * @fn          CHttpBase::~CHttpBase (void)
 * @brief       destructor
 */
CHttpBase::~CHttpBase (void)
{
    Close ();
}

/*!
 * @fn          bool CHttpBase::Open (void)
 * @brief       open http protcol
 * @details     this function must be called by overloaded functions
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              protocol open failed
 */
bool CHttpBase::Open (void)
{
    bool bRet = true;

    return bRet;
}

/*!
 * @fn          void CHttpBase::Close (void)
 * @brief       close http protcol
 * @details     this function must be called by overloaded functions
 */
void CHttpBase::Close (void)
{
}

/*!
 * @fn          void CHttpBase::Receive (const char *pBuffer, size_t nSize)
 * @brief       receive data
 * @param[in]   pBuffer
 *              received data
 * @param[in]   nSize
 *              data length
 */
void CHttpBase::Receive (const char *pBuffer, size_t nSize)
{
    m_cHttpParser.Receive (pBuffer, nSize);
}

//_oOo_
