/*!
 * @file        telnet-socket.cpp
 * @brief       telnet socket class implementation
 * @details     process telnet protocol via socket
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-telnet-export.h"
#define LIBDSLTOOL_PROTOCOL_EXPORT LIBDSLTOOL_TELNET_EXPORT
//! @endcond

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#ifdef _WIN32
#include <stdint.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#define close closesocket
#undef errno
#define errno WSAGetLastError()
#define ssize_t int
#define socklen_t int
#else // _WIN32
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <poll.h>
#endif // _WIN32

#include "telnet-socket.h"
#include "log.h"
#include "txt.h"

namespace NStrErr
{
    STRING Socket = "telnet socket: %s\n"; //!< error text
    STRING SocketErrno = "telnet socket: %s (%d)%s\n"; //!< error text, errno, strerror
} // NStrErr

#ifndef TCP_USER_TIMEOUT
#define TCP_USER_TIMEOUT 18             //!< how long for loss retry before timeout
#endif // TCP_USER_TIMEOUT

#ifndef INVALID_SOCKET
#define INVALID_SOCKET (-1)             //!< invalid socket handle
#endif // INVALID_SOCKET

/*!
 * @implements  ProtocolVersion
 */
void LIBDSLTOOL_PROTOCOL_EXPORT ProtocolVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

/*!
 * @implements  ProtocolCreate
 */
CProtocol LIBDSLTOOL_PROTOCOL_EXPORT *ProtocolCreate (const char *strHost,
        int nPort,
        CProtocol::EEthType eEthType,
        CAuth *pAuth,
        const char *strExtra)
{
    (void)strExtra;   // unused param

    return new CTelnetSocket (strHost, nPort, eEthType, pAuth);
}

#ifdef _WIN32
class CWinSock
{
private:
    CWinSock (void);

public:
    ~CWinSock (void);

private:
    static CWinSock g_cWinSock;
    WSADATA m_tData;
};

CWinSock CWinSock::g_cWinSock;

CWinSock::CWinSock (void)
{
    WSAStartup (MAKEWORD (2, 0), &m_tData);
}

CWinSock::~CWinSock (void)
{
    WSACleanup ();
}
#endif // _WIN32

/*!
 * @fn          CTelnetSocket::CTelnetSocket (const char *strHost,
 *                      int nPort,
 *                      CProtocol::EEthType eEthType)
 * @brief       constructor
 * @param[in]   strHost
 *              host name or IP
 * @param[in]   nPort
 *              port number
 * @param[in]   eEthType
 *              protocol type
 * @param[in]   pAuth
 *              authenticator
 */
CTelnetSocket::CTelnetSocket (const char *strHost,
        int nPort,
        CProtocol::EEthType eEthType,
        CAuth *pAuth)
: CTelnetBase (strHost, nPort, eEthType, pAuth)
, m_hSocket (INVALID_SOCKET)
{
}

/*!
 * @fn          bool CTelnetSocket::Open (void)
 * @brief       open socket
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              socket open failed
 */
bool CTelnetSocket::Open (void)
{
    int nRet;
    struct addrinfo *ai;
    struct addrinfo hints;
    bool bRet =  CTelnetBase::Open ();

    if (bRet)
    {
        memset ((void*)&hints, 0, sizeof (hints));
        switch (m_eEthType)
        {
            case eIPv4:
                hints.ai_family = AF_INET;
                break;
            case eIPv6:
                hints.ai_family = AF_INET6;
                break;
            default:
                hints.ai_family = AF_UNSPEC;
                break;
        }
        hints.ai_socktype = SOCK_STREAM;

        nRet = getaddrinfo (m_strHost, "telnet", &hints, &ai);

        if (0 == nRet)
        {
            // create server socket
            m_hSocket = socket (ai->ai_family, SOCK_STREAM, 0);

            if (INVALID_SOCKET == m_hSocket)
            {
                LogDebug (NStrErr::SocketErrno, "socket() failed", errno, strerror(errno));
                bRet = false;
            }
            else
            {
                int nSynCnt = 1;
                int nTimeout = 2000;

#ifndef _WIN32
                setsockopt (m_hSocket, SOL_TCP, TCP_SYNCNT, (char*) &nSynCnt, sizeof (nSynCnt));
                setsockopt (m_hSocket, SOL_TCP, TCP_USER_TIMEOUT, (char*) &nTimeout, sizeof (nTimeout));
#endif // _WIN32

                // set socket port
                if (-1 != m_nPort)
                {
                    switch (ai->ai_family)
                    {
                        case AF_INET:
                        {
                            struct sockaddr_in *addr4 = (struct sockaddr_in *)ai->ai_addr;
                            addr4->sin_port = htons ((uint16_t)m_nPort);
                        }
                            break;
                        case AF_INET6:
                        {
                            struct sockaddr_in6 *addr6 = (struct sockaddr_in6 *)ai->ai_addr;
                            addr6->sin6_port = htons ((uint16_t)m_nPort);
                        }
                            break;
                    }
                }

                // connect
                nRet = connect (m_hSocket, ai->ai_addr, (socklen_t)ai->ai_addrlen);

                if (0 != nRet)
                {
                    LogDebug (NStrErr::SocketErrno, "connect() failed", errno, strerror(errno));
                    bRet = false;

                    close (m_hSocket);
                    m_hSocket = INVALID_SOCKET;
                }
            }
            freeaddrinfo (ai);
        }
        else
        {
            LogDebug (NStrErr::SocketErrno, "getaddrinfo() failed", errno, strerror(errno));
            bRet = false;
        }
    }

    return bRet;
}

/*!
 * @fn          void CTelnetSocket::Close (void)
 * @brief       close socket
 */
void CTelnetSocket::Close (void)
{
    if (INVALID_SOCKET != m_hSocket)
    {
        close (m_hSocket);
        m_hSocket = INVALID_SOCKET;
    }

    CTelnetBase::Close ();
}

/*!
 * @fn          bool CTelnetSocket::Process (void)
 * @brief       process telnet protocol
 * @details     calls parser via @ref Receive
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              process failed
 */
bool CTelnetSocket::Process (void)
{
    bool bRet = true;
    char pBuffer[512];

    LogExtraEnter ("");

    while (m_bLoop)
    {
        struct timeval tTimeout;
        fd_set tFD;
        int nSelect;
        ssize_t nRead = 0;

        tTimeout.tv_sec = 60; // must be initialized inside loop!
        tTimeout.tv_usec = 0;

        FD_ZERO (&tFD);
        FD_SET (m_hSocket, &tFD);

        nSelect = select ((int)m_hSocket + 1, &tFD, (fd_set*)NULL, (fd_set*)NULL, &tTimeout);

        if (0 == nSelect)
        {
            LogDebug (NStrErr::Socket, "timeout");
            break; /* timeout */
        }
        else if (nSelect < 0)
        {
            continue;
        }
        else
        {
            if (FD_ISSET(m_hSocket, &tFD))
            {
                nRead = recv (m_hSocket, pBuffer, sizeof (pBuffer), 0);

                if (nRead > 0)
                {
                    Receive (pBuffer, (size_t)nRead);
                }
                else if (0 == nRead)
                {
                    break;
                }
                else
                {
                    if (errno)
                    {
                        LogDebug (NStrErr::SocketErrno, "recv() failed", errno, strerror (errno));
                        bRet = false;
                    }
                    break;
                }
            }
        }
        if (NULL == m_pParser)
        {
            LogExtra ("telnet parser finished\n");
            break;
        }
    }

    LogExtraLeave ("return: %s\n", bRet ? "true" : "false");
    return bRet;
}

#ifdef ENABLE_CAPTURE
/*!
 * @fn          bool CTelnetSocket::Capture (const char *strFile)
 * @brief       create capture file
 * @param[in]   strFile
 *              capture file name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              create capture failure
 */

bool CTelnetSocket::Capture (const char *strFile)
{
    int nPort = m_nPort;
    if(-1 == nPort)
    {
        struct addrinfo *ai;
        struct addrinfo hints;
        memset ((void*)&hints, 0, sizeof (hints));
        switch (m_eEthType)
        {
            case eIPv4:
                hints.ai_family = AF_INET;
                break;
            case eIPv6:
                hints.ai_family = AF_INET6;
                break;
            default:
                hints.ai_family = AF_UNSPEC;
                break;
        }
        hints.ai_socktype = SOCK_STREAM;

        if (0 == getaddrinfo(NULL, "telnet", &hints, &ai))
        {
            switch (ai->ai_family)
            {
            case eIPv4:
            {
                struct sockaddr_in *addr4 = (struct sockaddr_in *) ai->ai_addr;
                nPort = addr4->sin_port;
            }
                break;
            case eIPv6:
            {
                struct sockaddr_in6 *addr6 = (struct sockaddr_in6 *) ai->ai_addr;
                nPort = addr6->sin6_port;
            }
                break;
            default:
                nPort = IPPORT_TELNET;
                break;
            }
            freeaddrinfo(ai);
        }
    }

    return m_cCapture.Open(strFile, (uint16_t)nPort);
}
#endif // ENABLE_CAPTURE

/*!
 * @fn          bool CTelnetSocket::SendBuf (const char *pBuffer, size_t nSize)
 * @brief       send data
 * @param[in]   pBuffer
 *              data to send
 * @param[in]   nSize
 *              data length
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              send failure
 */
bool CTelnetSocket::SendBuf (const char *pBuffer, size_t nSize)
{
    bool bRet = true;
    ssize_t nSent;

    LogExtraEnter ("(\"%.*s\")\n",  nSize, pBuffer);

    while (nSize > 0)
    {
        nSent = send (m_hSocket, pBuffer, nSize, 0);

        if (nSent < 0)
        {
            bRet = false;
            break;
        }
        else if (nSent == 0)
        {
            bRet = false;
            break;
        }

        // update pointer and size to see if we've got more to send
        pBuffer += nSent;
        nSize -= (size_t)nSent;
    }

    LogExtraLeave ("");
    return bRet;
}

//_oOo_
