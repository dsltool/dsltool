/*!
 * @file        telnet-base.h
 * @brief       telnet base class definitions
 * @details     telnet protocol class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_TELNET_BASE_H__
#define __DSLTOOL_TELNET_BASE_H__

#include <libtelnet.h>

#include "protocol.h"

/*!
 * @class       CTelnetBase
 * @brief       telnet protocol handler
 */
class CTelnetBase: public CProtocol
{
protected:
    CTelnetBase (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth);

private:
    //! @cond
    CTelnetBase (CTelnetBase const &cTelnetBase);
    CTelnetBase &operator = (CTelnetBase const &cTelnetBase);
    //! @endcond

public:
    virtual ~CTelnetBase (void);

public:
    virtual bool Open (void);
    virtual void Close (void);
    virtual void Receive (const char *pBuffer, size_t nSize);

private:
    static void _EventHandler (struct telnet_t *pTelnet, union telnet_event_t *pEvent,
            void *pUser);
    void EventHandler (struct telnet_t *pTelnet, union telnet_event_t *pEvent);

private:
    static const struct telnet_telopt_t s_atTelopts[]; //!< telnet options
    telnet_t *m_pTelnet;                        //!< telnet pointer
}; // class CTelnetBase

#endif // __DSLTOOL_TELNET_BASE_H__
//_oOo_
