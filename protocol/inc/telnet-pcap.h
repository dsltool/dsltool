/*!
 * @file        telnet-pcap.h
 * @brief       telnet pcap class definitions
 * @details     process telnet protocol via capture file
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_TELNET_PCAP_H__
#define __DSLTOOL_TELNET_PCAP_H__

#include "telnet-base.h"
#include "pcap-parser.h"

/*!
 * @class       CTelnetPcap
 * @brief       telnet pcap handler
 * @details     uses telnet protocol handler
 */
class CTelnetPcap: public CPcapParser<CTelnetBase>
{
public:
    CTelnetPcap (const char *strFile,
            CAuth *pAuth);

private:
    //! @cond
    CTelnetPcap (CTelnetPcap const &cTelnetPcap);
    CTelnetPcap operator = (CTelnetPcap const &cTelnetPcap);
    //! @endcond
}; // class CTelnetPcap

#endif // __DSLTOOL_TELNET_PCAP_H__
//_oOo_
