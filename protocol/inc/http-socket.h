/*!
 * @file        http-socket.h
 * @brief       http socket class definitions
 * @details     process http protocol via socket
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_HTTP_SOCKET_H__
#define __DSLTOOL_HTTP_SOCKET_H__

#include "http-base.h"

/*!
 * @class       CHttpSocket
 * @brief       http socket handler
 * @details     uses http protocol handler
 */
class CHttpSocket: public CHttpBase
{
public:
    CHttpSocket (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth);

private:
    //! @cond
    CHttpSocket (const CHttpSocket &cHttpSocket);
    CHttpSocket &operator = (const CHttpSocket &cHttpSocket);
    //! @endcond

public:
    virtual bool Open (void);
    virtual void Close (void);
    virtual bool Process (void);
#ifdef ENABLE_CAPTURE
    virtual bool Capture (const char *strFile);
#endif // ENABLE_CAPTURE

protected:
    bool SendBuf (const char *pBuffer, size_t nSize);

private:
#ifdef  _WIN32
    SOCKET m_hSocket;                   //!< socket handle
#else // _WIN32
    int m_hSocket;                      //!< socket handle
#endif // _WIN32
    }; // class CHttpSocket

#endif // __DSLTOOL_HTTP_SOCKET_H__
//_oOo_
