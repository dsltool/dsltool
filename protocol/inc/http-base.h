/*!
 * @file        http-base.h
 * @brief       http base class definitions
 * @details     HTTP/1.1 protocol class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_HTTP_BASE_H__
#define __DSLTOOL_HTTP_BASE_H__

#include "parser.h"
#include "protocol.h"
#include "http_parser.h"

/*!
 * @class       CHttpParser
 * @brief       HTTP parser
 */
class CHttpParser
{
public:
    CHttpParser (CProtocol *pProtocol);

public:
    void Receive (const char *pData, size_t nSize);

private:
    static int _MessageBegin (http_parser *pParser);
    static int _HeaderField (http_parser *pParser, const char *pData, size_t nSize);
    static int _HeaderValue (http_parser *pParser, const char *pData, size_t nSize);
    static int _HeadersComplete (http_parser *pParser);
    static int _Body (http_parser *pParser, const char *pData, size_t nSize);
    static int _MessageComplete (http_parser *pParser);

private:
    int MessageBegin (void);
    int HeaderField (const char *pData, size_t nSize);
    int HeaderValue (const char *pData, size_t nSize);
    int HeadersComplete (void);
    int Body (const char *pData, size_t nSize);
    int MessageComplete (void);

private:
/*!
* @enum        EHeader
* @brief       last found header field
*/
    enum EHeader {
        eHeaderUnknown,         //!< unknown header
        eConnection,            //!< connection
        eContentLength,         //!< content length
        eContentType            //!< content type
    };

private:
    CProtocol *m_pProtocol;                     //!< HTTP protocol handler
    http_parser m_tParser;                      //!< http parser
    http_parser_settings m_tSettings;           //!< http parser settings
    EHeader m_eHeader;                          //!< last found header
    CParser::EContent m_eContent;               //!< content type
}; // class CHttpParser

/*!
 * @class       CHttpBase
 * @brief       HTTP/1.1 protocol handler
 */
class CHttpBase: public CProtocol
{
protected:
    CHttpBase (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth);
public:
    virtual ~CHttpBase (void);

public:
    virtual bool Open (void);
    virtual void Close (void);
    virtual void Receive (const char *pBuffer, size_t nSize);

private:
    CHttpParser m_cHttpParser;          //!< HTTP parser
}; // class CHttpBase

#endif // __DSLTOOL_HTTP_BASE_H__
//_oOo_
