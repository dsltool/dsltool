/*!
 * @file        http-pcap.h
 * @brief       http pcap class definitions
 * @details     process HTTP/1.1 protocol via capture file
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        04.12.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_HTTP_PCAP_H__
#define __DSLTOOL_HTTP_PCAP_H__

#include "http-base.h"
#include "pcap-parser.h"

/*!
 * @class       CHttpPcap
 * @brief       HTTP/1.1 pcap handler
 * @details     uses HTTP/1.1 protocol handler
 */
class CHttpPcap: public CPcapParser<CHttpBase>
{
public:
    CHttpPcap (const char *strFile, CAuth *pAuth);

private:
    //! @cond
    CHttpPcap (CHttpPcap const &cHttpPcap);
    CHttpPcap &operator = (CHttpPcap const &cHttpPcap);
    //! @endcond
}; // class CHttpPcap

#endif // __DSLTOOL_HTTP_PCAP_H__
//_oOo_
