/*!
 * @file        demo.h
 * @brief       demo protocol definitions
 * @details     demo protocol class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DEMO_H__
#define __DSLTOOL_DEMO_H__

#include "protocol.h"

/*!
 * @class       CDemo
 * @brief       demo protocol class
 * @details     used for demo-adsl and demo-vdsl
 */
class CDemo: public CProtocol
{
public:
    CDemo (void);

private:
    //! @cond
    CDemo (const CDemo &cDemo);
    CDemo &operator = (const CDemo &cDemo);
    //! @endcond

public:
    virtual bool Open (void);
    virtual void Close (void);
    virtual bool Process (void);
    virtual bool Capture (const char *strFile);

protected:
    virtual bool SendBuf (const char *pBuffer, size_t nSize);
}; // class CDummy

#endif // __DSLTOOL_DEMO_H__
//_oOo_
