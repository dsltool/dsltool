/*!
 * @file        pcap-parser.h
 * @brief       pcap parser class template
 * @details     parse capture file
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        14.04.2019
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_PCAP_PARSER_H__
#define __DSLTOOL_PCAP_PARSER_H__

#include <pcap.h>

#include <netdb.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/ip6.h>
#include <netinet/ether.h>
#include <sys/socket.h>
#include <arpa/inet.h>

/*!
 * @class       CHttpPcap
 * @brief       HTTP/1.1 pcap handler
 * @details     uses HTTP/1.1 protocol handler
 */
template <class B> class CPcapParser: public B
{
public:
    CPcapParser (const char *strFile, int nPort, CAuth *pAuth);
    virtual ~CPcapParser ();

private:
    //! @cond
    CPcapParser (CPcapParser const &cPcapParser);
    CPcapParser &operator = (CPcapParser const &cPcapParser);
    //! @endcond

public:
    virtual bool Open (void);
    virtual void Close (void);
    virtual bool Process (void);
#ifdef ENABLE_CAPTURE
    virtual bool Capture (const char *strFile);
#endif // ENABLE_CAPTURE

protected:
    bool SendBuf (const char *pBuffer, size_t nSize);

private:
/*!
 * @typedef     TPData
 * @brief       byte pointer data type
 */
    typedef const u_char * TPData;
    static void _PcapHandler (u_char *pUser, const struct pcap_pkthdr *pHeader, const u_char *pBytes);
    void PcapHandler (const struct pcap_pkthdr *pHeader, const u_char *pBytes);
    void DecodeEth (TPData pBytes, size_t nLength);
    void DecodeSLL (TPData pBytes, size_t nLength);
    void DecodeIP (TPData pBytes, size_t nLength);
    void DecodeIPv6 (TPData pBytes, size_t nLength);
    void DecodeTCP (TPData pBytes, size_t nLength);

private:
    const char *m_strFile;              //!< capture file name
    pcap_t *m_pPcap;                    //!< pcap pointer
    int m_nLink;                        //!< data link type
    char m_strErr[PCAP_ERRBUF_SIZE];    //!< error message buffe
    u_int8_t m_auMAC[ETH_ALEN];         //!< modem MAC address
    bool m_bFirst;                      //!< first packet flag
}; // class CPcapParser

#endif // __DSLTOOL_PCAP_PARSER_H__
//_oOo_
