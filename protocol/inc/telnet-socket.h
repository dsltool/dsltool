/*!
 * @file        telnet-socket.h
 * @brief       telnet socket class definitions
 * @details     process telnet protocol via socket
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_TELNET_SOCKET_H__
#define __DSLTOOL_TELNET_SOCKET_H__

#include "telnet-base.h"

/*!
 * @class       CTelnetSocket
 * @brief       telnet socket handler
 * @details     uses telnet protocol handler
 */
class CTelnetSocket: public CTelnetBase
{
public:
    CTelnetSocket (const char *strHost,
            int nPort,
            CProtocol::EEthType eEthType,
            CAuth *pAuth);

private:
    //! @cond
    CTelnetSocket (const CTelnetSocket &cTelnetSocket);
    CTelnetSocket &operator = (const CTelnetSocket &cTelnetSocket);
    //! @endcond

public:
    virtual bool Open (void);
    virtual void Close (void);
    virtual bool Process (void);
#ifdef ENABLE_CAPTURE
    virtual bool Capture (const char *strFile);
#endif // ENABLE_CAPTURE

protected:
    bool SendBuf (const char *pBuffer, size_t nSize);

private:
#ifdef  _WIN32
    SOCKET m_hSocket;                   //!< socket handle
#else // _WIN32
    int m_hSocket;                      //!< socket handle
#endif // _WIN32
}; // class CTelnetSocket

#endif // __DSLTOOL_TELNET_SOCKET_H__
//_oOo_
