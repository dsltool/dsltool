dsltool(1)
==========
:doctype: manpage


NAME
----
dsltool - a DSL modem information tool

SYNOPSIS
--------
*dsltool* [-h] [-V] [-v] [-l ['<file>'] [-f '<path>'] [-F '<prefix>']
  [-m '<modem>'] [-C '<prot>'] [-e '<extra>']
  -H '<host>' [-N '<port>'] [-4] [-6] -U '<user>' -P '<pass>'
  [-r '<host>'] [-s '<sock>']
  -o '<output>' -x '<cmd>'

DESCRIPTION
-----------
dsltool reads out a DSL modem and displays the information in several formats.

OPTIONS
-------
*-h*::
  print help

*-V*::
  show version info

*-v*::
  increase verbosity (may be used more than once)

*-l* ['<file>']::
  log to '<file>' (default "/var/log/'<prefix>'.log") +
  log to stderr when -l is not given +
  log to syslog when '<file>' is "#" +
  log to "'<path>'/'<prefix>'.log" when '<file>' is "+"

*-f* '<path>'::
  write outputs to '<path>' (default "/var/run ")

*-F* '<prefix>'::
  output file name prefix (default "dsltool-'<host>' ")

*-m* '<modem>'::
  specify modem type (see documentation)

*-C* '<prot>'::
  use protocol '<prot>' to connect (default: telnet)

*-e* '<extra>'::
  extra arguments to protocol handler

  (e.g. read from capture file instead of modem when '<prot>'= "pcap-telnet")

*-H* '<host>'::
  connect to modem '<host>' (name or IP)

*-N* '<port>'::
  use alternative port number (default telnet:23, http:80)

*-4*::
  use IPv4

*-6*::
  use IPv6

*-U* '<user>'::
  modem user name

*-P* '<pass>'::
  modem password

*-r* '<host'>::
  send collectd data to '<host>' (name or IP) (default "localhost")

*-s* '<sockS>'::
 send collectd data to '<sock>' (default "/var/run/collectd.sock")

*-o* '<output>'::
  output flags, ored of: +
  1: .dat +
  2: stdout (short) +
  4: stdout (long) +
  8: graph +
  16: rrd

*-x* '<cmd>'::
  command, one of "info", "status", "resync", "reboot"
  (default "info")

AUTHOR
------
*dsltool* was originally written by Carsten Spieß <dsltool@carsten-spiess.de>.

COPYING
-------
*dsltool* is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
