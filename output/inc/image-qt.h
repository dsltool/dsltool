/*!
 * @file        image-qt.h
 * @brief       Qt image class definitions
 * @details     QPainter based image class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        11.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_IMAGE_QT_H__
#define __DSLTOOL_IMAGE_QT_H__

#include <QPicture>
#include <QPainter>
#include "image.h"

/*!
 * @class       CImageQt
 * @brief       QPainter image drawing class
 */
class CImageQt : public CImage
{
public:
    CImageQt (QPicture *pPicture);
    virtual ~CImageQt (void);

private:
    //! @cond
    CImageQt (const CImageQt &cImageQt);
    CImageQt &operator = (const CImageQt &cImageQt);
    //! @endcond

public:
    virtual bool Create (double fWidth, double fHeight);

public:
    virtual void Color (unsigned long ulColor);
    virtual void TextColor (unsigned long ulTextColor);

public:
    virtual void Line (double fFromX, double fFromY, double fToX, double fToY,
            double fWidth = 1.0, bool bDash = false);
    virtual void Rect (double fFromX, double fFromY, double fToX, double fToY);

    virtual void Block (double x, double y);
    virtual void Text (double x, double y, const char *strText,
            EAlign eHAlign = eCenter, EAlign eVAlign = eCenter,
            ERotate eRotate = eNone);

private:
    QPainter m_cPainter;        //!< painter object
    QColor m_cColor;            //!< line/fill color
    QColor m_cTextColor;        //!< text color
}; // class CImageQt

#endif // __DSLTOOL_IMAGE_QT_H__
//_oOo_
