/*!
 * @file        rrd.h
 * @brief       rrd output class definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_RRD_H__
#define __DSLTOOL_RRD_H__

#include "output.h"
#include "collect.h"

/*!
 * @class       CRrdOutput
 * @brief       dat file output
 */
class CRrdOutput: public COutput
{
public:
    CRrdOutput (void);

private:
    //! @cond
    CRrdOutput (const CRrdOutput &cRrdOutput);
    CRrdOutput &operator = (const CRrdOutput &cRrdOutput);
    //! @endcond

public:
    virtual void Write (const CConfigData &cConfig,
            const CDslData &cData,
            bool bTones);

private:
    void Write (const CDataInt &cData,
            const char* strType,
            const char* strInst);
    void Write (const CDataIntUpDown &cData,
            const char* strType,
            const char* strInstUp,
            const char* strInstDown);
    void Write (const CDataDouble &cData,
            const char* strType,
            const char* strInst);
    void Write (const CDataDoubleUpDown &cData,
            const char* strType,
            const char* strInstUp,
            const char* strInstDown);
    void Write (const CArrayDoubleUpDown &cData,
            const char* strType,
            const char* strInstUp,
            const char* strInstDown);

private:
    CCollect m_cCollect;        //!< collect client
}; // class CRrdOutput

#endif // __DSLTOOL_RRD_H__
//_oOo_
