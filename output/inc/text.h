/*!
 * @file        txt.h
 * @brief       text output class definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_TEXT_H__
#define __DSLTOOL_TEXT_H__

#include "output.h"

/*!
 * @class       CTextOutput
 * @brief       text output
 * @details     writes to stdout
 */
class CTextOutput: public COutput
{
public:
    CTextOutput (void);

private:
    //! @cond
    CTextOutput (const CTextOutput &cTextOutput);
    CTextOutput &operator = (const CTextOutput &cTextOutput);
    //! @endcond

public:
    virtual void Write (const CConfigData &cConfig,
            const CDslData &cData,
            bool bTones);

private:
    void Write (const CBandplan* pBandplan,
            const char* strFmtType,
            const char* strFmtTones,
            const char* strFmtBands,
            const char* strFmtBandUp,
            const char* strFmtBandDown);
    void Write (const CDataInt &cData,
            const char* strFmt);
    void Write (const CDataIntUpDown &cData,
            const char* strFmt);
    void Write (const CDataDouble &cData,
            const char* strFmt);
    void Write (const CDataDoubleUpDown &cData,
            const char* strFmt);
    void Write (const CArrayDoubleUpDown &cData,
            const char* strFmt);
    void Write (const CDataString &cData,
            const char* strFmt);
    void Write (const CDataATU &cData,
            const char* strFmtVendor,
            const char* strFmtSpec,
            const char* strFmtRevision);
    void Write (const CDataTone &cData,
            const char* strFmt,
            const char* strFmtTone);

private:
    CFile m_cFile;      //!< file object
}; // class CTextOutput

#endif // __DSLTOOL_TEXT_H__
//_oOo_
