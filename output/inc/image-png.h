/*!
 * @file        image-png.h
 * @brief       png image class definitions
 * @details     pango based image class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_IMAGE_PNG_H__
#define __DSLTOOL_IMAGE_PNG_H__

#include <pango/pangocairo.h>
#include "image.h"

/*!
 * @class       CImagePng
 * @brief       pango png image drawing class
 */
class CImagePng : public CImage
{
public:
    CImagePng (void);
    virtual ~CImagePng (void);

private:
    //! @cond
    CImagePng (const CImagePng &cImagePng);
    CImagePng &operator = (const CImagePng &cImagePng);
    //! @endcond

private:
/*!
 * @class       CColor
 * @brief       color helper
 */
    class CColor
    {
    public:
        CColor (void);
        CColor (unsigned long ulColor);
        CColor &operator = (unsigned long ulColor);
/*!
 * @fn          double Red (void)
 * @brief       get red channel value
 * @return      red channel value
 */
        double Red (void) {return m_fRed;}
/*!
 * @fn          double Green (void)
 * @brief       get green channel value
 * @return      green channel value
 */
        double Green (void) {return m_fGreen;}
/*!
 * @fn          double Blue (void)
 * @brief       get blue channel value
 * @return      blue channel value
 */
        double Blue (void) {return m_fBlue;}
    private:
        double m_fRed;                          //!< red channel (0.0-1.0)
        double m_fGreen;                        //!< green channel
        double m_fBlue;                         //!< blue channel
    }; // class CColor

public:
    virtual bool Create (double fWidth, double fHeight);

public:
    bool Save (const char *strFile);

private:
    void Fit (double &fX, double &fY, double fWidth);

public:
    virtual void Color (unsigned long ulColor);
    virtual void TextColor (unsigned long ulTextColor);

public:
    virtual void Line (double fFromX, double fFromY, double fToX, double fToY,
            double fWidth = 1.0, bool bDash = false);
    virtual void Rect (double fFromX, double fFromY, double fToX, double fToY);

    virtual void Block (double fX, double fY);
    virtual void Text (double fX, double fY, const char *strText,
            EAlign eHAlign = eCenter, EAlign eVAlign = eCenter,
            ERotate eRotate = eNone);

private:
    cairo_surface_t            *m_pSurface;     //!< cairo surface pointer
    cairo_t                    *m_pCairo;       //!< cairo pointer
    PangoFontDescription       *m_pFontDescr;   //!< pango font
    CColor                      m_cColor;       //!< line/fill color
    CColor                      m_cTextColor;   //!< text color
}; // class CImage

#endif // __DSLTOOL_IMAGE_PNG_H__
//_oOo_
