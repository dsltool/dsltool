/*!
 * @file        image.h
 * @brief       image bass class definitions
 * @details     abstract image class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_IMAGE_H__
#define __DSLTOOL_IMAGE_H__

/*!
 * @class       CImage
 * @brief       image drawing class
 */
class CImage
{
protected:
/*!
 * @fn          CImage (void)
 * @brief       constructor
 */
    CImage (void) {}

private:
    //! @cond
    CImage (const CImage &cImage);
    CImage &operator = (const CImage &cTemplate);
    //! @endcond

public:
/*!
 * @fn          CImage (void)
 * @brief       destructor
 */
    virtual ~CImage (void) {}

public:
/*!
 * @enum        EAlign
 * @brief       alignement flags
 */
    enum EAlign
    {
        eCenter = 0,                            //!< centered
        eLeft = -1,                             //!< left aligned
        eRight = 1,                             //!< right aligned
        eUp = -1,                               //!< top aligned
        eDown = 1                               //!< bottom aligned
    };
/*!
 * @enum        ERotate
 * @brief       rotation flags
 */
    enum ERotate
    {
        eNone,                                  //!< no rotation
        eLeft90 = -1,                           //!< rotate left 90°
        eRight90 = 1                            //!< rotate right 90°
    };

public:
/*!
 * @fn          virtual bool Create (double fWidth, double fHeight)
 * @brief       create image
 * @details     pure virtual function, see derived classes
 * @details     create pango/cairo resources
 * @param[in]   fWidth
 *              image width [pixel]
 * @param[in]   fHeight
 *              image height [pixel]
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to create image
 */
    virtual bool Create (double fWidth, double fHeight) = 0;

public:
/*!
 * @fn          virtual void Color (unsigned long ulColor)
 * @brief       set paint color
 * @details     pure virtual function, see derived classes
 * @param[in]   ulColor
 *              color value (0x00rrggbb)
 */
    virtual void Color (unsigned long ulColor) = 0;
/*!
 * @fn          virtual void TextColor (unsigned long ulTextColor)
 * @brief       set text color
 * @details     pure virtual function, see derived classes
 * @param[in]   ulTextColor
 *              color value (0x00rrggbb)
 */
    virtual void TextColor (unsigned long ulTextColor) = 0;

public:
/*!
 * @fn          virtual void Line (double fFromX, double fFromY,
 *                      double fToX, double fToY,
 *                      double fWidth = 1.0, bool bDash = false)
 * @brief       draw line
 * @details     pure virtual function, see derived classes
 * @param[in]   fFromX
 *              start point x coordinate
 * @param[in]   fFromY
 *              start point y coordinate
 * @param[in]   fToX
 *              end point x coordinate
 * @param[in]   fToY
 *              end point y coordinate
 * @param[in]   fWidth
 *              line width
 * @param[in]   bDash
 *              dash flag
 */
    virtual void Line (double fFromX, double fFromY, double fToX, double fToY,
            double fWidth = 1.0, bool bDash = false) = 0;
/*!
 * @fn          virtual void Rect (double fFromX, double fFromY,
 *                      double fToX, double fToY)
 * @brief       draw rectangle
 * @details     pure virtual function, see derived classes
 * @param[in]   fFromX
 *              start point x coordinate
 * @param[in]   fFromY
 *              start point y coordinate
 * @param[in]   fToX
 *              end point x coordinate
 * @param[in]   fToY
 *              end point y coordinate
 */
    virtual void Rect (double fFromX, double fFromY, double fToX, double fToY) = 0;
/*!
 * @fn          virtual void Block (double fX, double fY)
 * @brief       draw filled rectangle with border
 * @details     pure virtual function, see derived classes
 * @param[in]   fX
 *              start point x coordinate
 * @param[in]   fY
 *              start point y coordinate
 */
    virtual void Block (double fX, double fY) = 0;
/*!
 * @fn          virtual void Text (double fX, double fY, const char *strText,
 *                      EAlign eHAlign = eCenter, EAlign eVAlign = eCenter,
 *                      ERotate eRotate = eNone)
 * @brief       draw text
 * @details     pure virtual function, see derived classes
 * @param[in]   fX
 *              start point x coordinate
 * @param[in]   fY
 *              start point y coordinate
 * @param[in]   strText
 *              text to draw
 * @param[in]   eHAlign
 *              horzontal alignement
 * @param[in]   eVAlign
 *              vertical alignement
 * @param[in]   eRotate
 *              rotation
 */
    virtual void Text (double fX, double fY, const char *strText,
            EAlign eHAlign = eCenter, EAlign eVAlign = eCenter,
            ERotate eRotate = eNone) = 0;
}; // class CImage

#endif // __DSLTOOL_IMAGE_H__
//_oOo_
