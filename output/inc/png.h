/*!
 * @file        png.h
 * @brief       png output class definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_PNG_H__
#define __DSLTOOL_PNG_H__

#include "output.h"

/*!
 * @class       CPngOutput
 * @brief       png file output
 */
class CPngOutput: public COutput
{
public:
    CPngOutput (void);

private:
    //! @cond
    CPngOutput (const CPngOutput &cPngOutput);
    CPngOutput &operator = (const CPngOutput &cPngOutput);
    //! @endcond

public:
    virtual void Write (const CConfigData &cConfig,
            const CDslData &cData,
            bool bTones);

private:
    void WriteBits (const char *strFile, const CTonesGroup &cTones);
    void WriteSNR (const char *strFile, const CTonesGroup &cTones);
    void WriteChar (const char *strFile, const CTonesGroup &cTones);
}; // class CPngOutput

#endif // __DSLTOOL_PNG_H__
//_oOo_
