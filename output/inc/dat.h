/*!
 * @file        dat.h
 * @brief       dat output class definitions
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_DAT_H__
#define __DSLTOOL_DAT_H__

#include "output.h"
#include "file.h"

/*!
 * @class       CDatOutput
 * @brief       dat file output
 */
class CDatOutput: public COutput
{
public:
    CDatOutput (void);

private:
    //! @cond
    CDatOutput (const CDatOutput &cDatOutput);
    CDatOutput &operator = (const CDatOutput &cDatOutput);
    //! @endcond

public:
    virtual void Write (const CConfigData &cConfig,
            const CDslData &cData,
            bool bTones);

private:
    void Write (const CBandplan* pBandplan,
            const char* strFmtType,
            const char* strFmtBands);
    void Write (const CDataInt &cData,
            const char* strFmt);
    void Write (const CDataIntUpDown &cData,
            const char* strFmtUp,
            const char* strFmtDown);
    void Write (const CDataDouble &cData,
            const char* strFmt);
    void Write (const CDataDoubleUpDown &cData,
            const char* strFmtUp,
            const char* strFmtDown);
    void Write (const CArrayDoubleUpDown &cData,
            const char* strFmtUp,
            const char* strFmtDown);
    void Write (const CDataString &cData,
            const char* strFmt);
    void Write (const CDataATU &cData,
            const char* strFmtVendor,
            const char* strFmtSpec,
            const char* strFmtRevision);

private:
    CFile m_cFile;      //!< file object
}; // class CDatOutput

#endif // __DSLTOOL_DAT_H__
//_oOo_
