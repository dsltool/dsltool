/*!
 * @file        graph.h
 * @brief       graph class definitions
 * @details     graph output classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_GRAPH_H__
#define __DSLTOOL_GRAPH_H__

#include "image.h"
#include "dsldata.h"

/*!
 * @class       CGraph
 * @brief       graph image
 * @details     abstract base class
 */
class CGraph
{
protected:
    CGraph (CImage &cImage, size_t nTones);

private:
    //! @cond
    CGraph (const CGraph &cGraph);
    CGraph &operator = (const CGraph &cGraph);
    //! @endcond

public:
/*!
 * @fn          virtual ~CGraph (void)
 * @brief       destructor
 */
    virtual ~CGraph (void) {}

public:
/*!
 * @fn          virtual bool Draw (void)
 * @brief       draw graph
 * @details     pure virtual function, see derived classes
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
    virtual bool Draw (void) = 0;

protected:
    bool Init (void);
    void Finish (void);

protected:
    void HScale (double fSpacing, const char *strTone, const char *strCaption);
    void VScaleLeft (int nInc, int nMax, const char *strCaption,
            bool bWide = false);
    void VScaleRight (int nInc, int nMax, const char *strCaption,
            bool bWide = false);
    void Caption (int nIndex, const char *strCaption, unsigned long ulColor);
    void PlotBar (const CTone &cData, unsigned long ulColor, int nMult, int nDiv);
    void PlotDot (const CTone &cData, unsigned long ulColor, int nMult, int nDiv);
    void PlotMark (bool *pbData, unsigned long ulColor);

private:
    int ToneScale (void);
    int ToneWidth (void);

private:
    static const unsigned long s_ulColorBgnd = 0xf3f3f3;        //!< background color
    static const unsigned long s_ulColorBorderTL = 0xcfcfcf;    //!< border color
    static const unsigned long s_ulColorBorderBR = 0x9e9e9e;    //!< border color
    static const unsigned long s_ulColorGraph = 0xe0e0e0;       //!< graph background
    static const unsigned long s_ulColorGrid = 0xdfabab;        //!< grid color
    static const unsigned long s_ulColorCoord = 0x666666;       //!< coordsystem color
    static const unsigned long s_ulColorText = 0x000000;        //!< text color
protected:
    static const unsigned long s_ulColorUp = 0x00ff00;          //!< upstream color
    static const unsigned long s_ulColorDown = 0x0000ff;        //!< downstream color
    static const unsigned long s_ulColorGainQ2 = 0xff00ff;      //!< gain Q2 color
    static const unsigned long s_ulColorGap = 0xff0000;         //!< gap color
    static const unsigned long s_ulColorSNR = 0x999999;         //!< SNR color
    static const unsigned long s_ulColorNoiseMargin = 0xffff00; //!< noisemargin color
    static const unsigned long s_ulColorChar = 0x999999;        //!< channel color

private:
    CImage &m_cImage;           //!< image generation object
    const size_t m_nTones;      //!< number of tones
    const int m_nScale;         //!< scaling
    const int m_nX0;            //!< graph origin (x)
    const int m_nY0;            //!< graph origin (y)
    const int m_nHScale;        //!< horiz. scale width
    const int m_nWidth;         //!< graph width
    const int m_nHeight;        //!< graph height
    const int m_nVScale;        //!< vert. scale width
    const int m_nVScaleWide;    //!< vert. scale width (wide spacing)
    const int m_nMaxX;          //!< graph max postion X
    const int m_nMaxY;          //!< graph max postion Y
}; // class CGraph

/*!
 * @class       CGraphBitalloc
 * @brief       draw bitalloc graph
 */
class CGraphBitalloc : public CGraph
{
public:
    CGraphBitalloc (CImage &cImage, const CTonesGroup &cTones);
    virtual ~CGraphBitalloc (void);

private:
    //! @cond
    CGraphBitalloc (CGraphBitalloc const &cGraphBitalloc);
    CGraphBitalloc &operator = (CGraphBitalloc const &cGraphBitalloc);
    //! @endcond

public:
    virtual bool Draw (void);

private:
    static void FindGap (bool *pbGap,
            const CDataTone &cTone,
            int nBandMin, int nBandMax);
    void FindGap (const CBandplan* pBandplan,
            const CDataTone &cBitallocUp,
            const CDataTone &cBitallocDown);

private:
    const CTonesGroup &m_cTones;//!< tones group
    bool *m_pbGap;              //!< gap array
}; // class CGraphBitalloc

/*!
 * @class       CGraphSNR
 * @brief       draw SNR graph
 */
class CGraphSNR : public CGraph
{
public:
    CGraphSNR (CImage &cImage, const CTonesGroup &cTones);

private:
    //! @cond
    CGraphSNR (CGraphSNR const &cGraphSNR);
    CGraphSNR &operator = (CGraphSNR const &cGraphSNR);
    //! @endcond

public:
    virtual bool Draw (void);

private:
    const CTonesGroup &m_cTones;//!< tones group
}; // class CGraphSNR

/*!
 * @class       CGraphChar
 * @brief       draw char graph
 */
class CGraphChar : public CGraph
{
public:
    CGraphChar (CImage &cImage, const CTonesGroup &cTones);

private:
    //! @cond
    CGraphChar (CGraphChar const &cGraphChar);
    CGraphChar &operator = (CGraphChar const &cGraphChar);
    //! @endcond

public:
    virtual bool Draw (void);

private:
    const CTonesGroup &m_cTones;//!< tones group
}; // class CGraphSNR

#endif // __DSLTOOL_GRAPH_H__
//_oOo_
