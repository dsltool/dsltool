/*!
 * @file        collect.h
 * @brief       collectd interface definitions
 * @details     rrdtool/collectd access
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DSLTOOL_COLLECT_H__
#define __DSLTOOL_COLLECT_H__

#include <collectd/client.h>
#include <time.h>

/*!
 * @class       CCollect
 * @brief       collectd client
 */
class CCollect
{
public:
    CCollect (void);

private:
    //! @cond
    CCollect (const CCollect &cCollect);
    CCollect &operator = (const CCollect &cCollect);
    //! @endcond

public:
/*!
 * @fn          double Interval (void)
 * @brief       get update interval
 * @return      update interval [s]
 */
    double Interval (void) {return (double)m_nInterval;}

    bool Open (const char *strHost,
            const char *strSock,
            const char *strModem,
            int nInterval,
            time_t tTime);
    void Close (void);
    bool Put (const char *strType,
            const char *strInst,
            double fValue);

private:
    int                 m_nInterval;    //!< update interval [s]
    lcc_connection_t   *m_pConnection;  //!< collectd connection
    value_t             m_tValue;       //!< collectd value
    int                 m_nType;        //!< collectd type
    lcc_value_list_t    m_tValueList;   //!< collectd value list
}; // class CCollect

#endif // __DSLTOOL_COLLECT_H__
//_oOo_
