/*!
 * @file        image-png.cpp
 * @brief       pango png image class implementation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdlib.h>
#include <math.h>

#include "image-png.h"
#include "log.h"

/*!
 * @fn          CImagePng::CImagePng (void)
 * @brief       constructor
 */
CImagePng::CImagePng (void)
: m_pSurface (NULL)
, m_pCairo (NULL)
, m_pFontDescr (NULL)
, m_cColor ()
, m_cTextColor ()
{
}

/*!
 * @fn          CImagePng::~CImagePng (void)
 * @brief       destructor
 * @details     free pango/cairo resources
 */
CImagePng::~CImagePng (void)
{
    if (m_pFontDescr)
    {
        pango_font_description_free (m_pFontDescr);
        m_pFontDescr = NULL;
    }

    if (m_pCairo)
    {
        cairo_destroy (m_pCairo);
        m_pCairo = NULL;
    }

    if (m_pSurface)
    {
        cairo_surface_destroy (m_pSurface);
        m_pSurface = NULL;
    }
}

/*!
 * @fn          CImagePng::CColor::CColor (void)
 * @brief       constructor
 * @details     initialize to black
 */
CImagePng::CColor::CColor (void)
: m_fRed (0)
, m_fGreen (0)
, m_fBlue (0)
{
}

/*!
 * @fn          CImagePng::CColor::CColor (unsigned long ulColor)
 * @brief       constructor
 * @details     initialize to rgb value
 * @param[in]   ulColor
 *              rgb value as 0x00RRGGBB
 */
CImagePng::CColor::CColor (unsigned long ulColor)
: m_fRed ((double) ((ulColor >> 16) & 0x000000ff) / 255.0)
, m_fGreen ((double) ((ulColor >> 8) & 0x000000ff) / 255.0)
, m_fBlue ((double) (ulColor & 0x000000ff) / 255.0)
{
}

/*!
 * @fn          CImagePng::CColor &CImagePng::CColor::operator = (unsigned long ulColor)
 * @brief       asignement operator
 * @param[in]   ulColor
 *              rgb value as 0x00RRGGBB
 * @return      color object
 */
CImagePng::CColor &CImagePng::CColor::operator = (unsigned long ulColor)
{
    m_fRed =   (double) ((ulColor >> 16) & 0x000000ff) / 255.0;
    m_fGreen = (double) ((ulColor >> 8) & 0x000000ff) / 255.0;
    m_fBlue =  (double) (ulColor & 0x000000ff) / 255.0;

    return *this;
}


/*!
 * @fn          bool CImagePng::Create (double fWidth, double fHeight)
 * @brief       create image
 * @details     create pango/cairo resources
 * @param[in]   fWidth
 *              image width [pixel]
 * @param[in]   fHeight
 *              image height [pixel]
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to create image
 */
bool CImagePng::Create (double fWidth, double fHeight)
{
    bool bRet = true;

    m_pSurface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
            (int)fWidth, (int)fHeight);
    if (NULL == m_pSurface)
    {
        LogError ("failed to create cairo surface");
        bRet = false;
    }

    if (bRet)
    {
        m_pCairo = cairo_create (m_pSurface);
        if (NULL == m_pCairo)
        {
            LogError ("failed to create cairo");
            bRet = false;
        }
    }

    if (bRet)
    {
        m_pFontDescr = pango_font_description_new ();

        if (NULL == m_pFontDescr)
        {
            LogError ("failed to create pango font");
            bRet = false;
        }
    }

    if (bRet)
    {
        pango_font_description_set_family (m_pFontDescr, "Courier");
        pango_font_description_set_weight (m_pFontDescr, PANGO_WEIGHT_NORMAL);
        pango_font_description_set_size (m_pFontDescr, 8 * PANGO_SCALE);
    }

    if (!bRet)
    {
        if (m_pFontDescr)
        {
            pango_font_description_free (m_pFontDescr);
            m_pFontDescr = NULL;
        }

        if (m_pCairo)
        {
            cairo_destroy (m_pCairo);
            m_pCairo = NULL;
        }

        if (m_pSurface)
        {
            cairo_surface_destroy (m_pSurface);
            m_pSurface = NULL;
        }
    }

    return bRet;
}

/*!
 * @fn          bool CImagePng::Save (const char *strFile)
 * @brief       save image to .png file
 * @param[in]   strFile
 *              file name
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to save image
 */
bool CImagePng::Save (const char *strFile)
{
    bool bRet = false;

    if (m_pSurface)
    {
        cairo_surface_write_to_png (m_pSurface, strFile);
    }

    return bRet;
}

/*!
 * @fn          void CImagePng::Fit (double &fX, double &fY, double fWidth)
 * @brief       fit point to width
 * @param[in,out] fX
 *              x coordinate
 * @param[in,out] fY
 *              y coordinate
 * @param[in]   fWidth
 *              width
 */
void CImagePng::Fit (double &fX, double &fY, double fWidth)
{
    if (m_pCairo)
    {
        fWidth = fWidth / 2.0 - ceil (fWidth / 2.0);

        cairo_user_to_device (m_pCairo, &fX, &fY);
        fX = floor (fX - 0.5) - fWidth;
        fY = ceil (fY + 0.5) + fWidth;
        cairo_device_to_user (m_pCairo, &fX, &fY);
    }
}

/*!
 * @fn          void CImagePng::Color (unsigned long ulColor)
 * @brief       set paint color
 * @param[in]   ulColor
 *              color value (0x00rrggbb)
 */
void CImagePng::Color (unsigned long ulColor)
{
    m_cColor = ulColor;
}

/*!
 * @fn          void CImagePng::TextColor (unsigned long ulTextColor)
 * @brief       set text color
 * @param[in]   ulTextColor
 *              color value (0x00rrggbb)
 */
void CImagePng::TextColor (unsigned long ulTextColor)
{
    m_cTextColor = ulTextColor;
}

/*!
 * @fn          void CImagePng::Line (double fFromX, double fFromY,
 *                      double fToX, double fToY,
 *                      double fWidth, bool fWidth)
 * @brief       draw line
 * @details     use @ref Color to set line color before calling @ref Line
 * @param[in]   fFromX
 *              start point x coordinate
 * @param[in]   fFromY
 *              start point y coordinate
 * @param[in]   fToX
 *              end point x coordinate
 * @param[in]   fToY
 *              end point y coordinate
 * @param[in]   fWidth
 *              line width
 * @param[in]   fWidth
 *              dash flag
 */
void CImagePng::Line (double fFromX, double fFromY,
        double fToX, double fToY,
        double fWidth, bool bDash)
{
    double afDashes[2] = { 1, 1 };

    if (m_pCairo)
    {
        Fit (fFromX, fFromY, fWidth);
        Fit (fToX, fToY, fWidth);

        cairo_save (m_pCairo);
        cairo_new_path (m_pCairo);
        cairo_set_source_rgb (m_pCairo, m_cColor.Red (), m_cColor.Green (), m_cColor.Blue ());
        cairo_set_line_width (m_pCairo, fWidth);

        if (bDash)
        {
            cairo_set_dash (m_pCairo, afDashes, 2, 0.0);
        }
        cairo_move_to (m_pCairo, fFromX, fFromY);
        cairo_line_to (m_pCairo, fToX, fToY);

        cairo_stroke (m_pCairo);
        cairo_restore (m_pCairo);
    }
}

/*!
 * @fn          void CImagePng::Rect (double fFromX, double fFromY,
 *                      double fToX, double fToY)
 * @brief       draw filled rectangle
 * @details     use @ref Color to set fill color before calling @ref Rect
 * @param[in]   fFromX
 *              start point x coordinate
 * @param[in]   fFromY
 *              start point y coordinate
 * @param[in]   fToX
 *              end point x coordinate
 * @param[in]   fToY
 *              end point y coordinate
 */
void CImagePng::Rect (double fFromX, double fFromY,
        double fToX, double fToY)
{
    if (m_pCairo)
    {
        cairo_new_path (m_pCairo);
        cairo_set_source_rgb (m_pCairo, m_cColor.Red (), m_cColor.Green (), m_cColor.Blue ());

        cairo_rectangle (m_pCairo, fFromX, fFromY, fToX - fFromX, fToY - fFromY);

        cairo_fill (m_pCairo);
        cairo_close_path (m_pCairo);
    }
}

/*!
 * @fn          void CImagePng::Block (double fX, double fY)
 * @brief       draw filled rectangle with border
 * @details     use @ref Color to set fill color and
 *              @ref TextColor to set border color before calling @ref Block
 * @param[in]   fX
 *              start point x coordinate
 * @param[in]   fY
 *              start point y coordinate
 */
void CImagePng::Block (double fX, double fY)
{
    if (m_pCairo)
    {
        double fFromX = fX;
        double fFromY = fY;
        double fToX = fX + 8.0;
        double fToY = fY + 8.0;

        Fit (fFromX, fFromY, 1.0);
        Fit (fToX, fToY, 1.0);

        cairo_save (m_pCairo);
        cairo_new_path (m_pCairo);

        cairo_set_source_rgb (m_pCairo, m_cColor.Red (), m_cColor.Green (), m_cColor.Blue ());
        cairo_rectangle (m_pCairo, fFromX, fFromY, fToX - fFromX, fToY - fFromY);
        cairo_fill (m_pCairo);
        cairo_close_path (m_pCairo);
        cairo_stroke (m_pCairo);

        cairo_new_path (m_pCairo);
        cairo_set_source_rgb (m_pCairo, m_cTextColor.Red (), m_cTextColor.Green (), m_cTextColor.Blue ());
        cairo_set_line_width (m_pCairo, 1.0);

        cairo_move_to (m_pCairo, fFromX, fFromY);
        cairo_line_to (m_pCairo, fToX, fFromY);
        cairo_line_to (m_pCairo, fToX, fToY);
        cairo_line_to (m_pCairo, fFromX, fToY);
        cairo_line_to (m_pCairo, fFromX, fFromY);

        cairo_stroke (m_pCairo);
        cairo_restore (m_pCairo);
    }
}

/*!
 * @fn          void CImagePng::Text (double fX, double fY, const char *strText,
 *                      EAlign eHAlign, EAlign eVAlign, ERotate eRotate)
 * @brief       draw text
 * @details     use @ref TextColor to set text color before calling @ref Text
 * @param[in]   fX
 *              start point x coordinate
 * @param[in]   fY
 *              start point y coordinate
 * @param[in]   strText
 *              text to draw
 * @param[in]   eHAlign
 *              horzontal alignement
 * @param[in]   eVAlign
 *              vertical alignement
 * @param[in]   eRotate
 *              rotation
 */
void CImagePng::Text (double fX, double fY, const char *strText,
        EAlign eHAlign, EAlign eVAlign, ERotate eRotate)
{
    PangoLayout *pLayout = NULL;
    PangoRectangle sRect;

    if (m_pCairo && strText)
    {
        double fAlignX = 0;
        double fAlignY = 0;

        pLayout = pango_cairo_create_layout (m_pCairo);
        if (pLayout)
        {
            cairo_save (m_pCairo);
            cairo_translate (m_pCairo, fX, fY);
            cairo_set_source_rgb (m_pCairo, m_cTextColor.Red (), m_cTextColor.Green (), m_cTextColor.Blue ());

            pango_layout_set_font_description (pLayout, m_pFontDescr);
            pango_layout_set_text (pLayout, strText, -1);
            pango_layout_get_pixel_extents (pLayout, NULL, &sRect);
            pango_cairo_update_layout (m_pCairo, pLayout);

            if (eNone != eRotate)
            {
                cairo_rotate (m_pCairo, eRotate * G_PI / 2.0);
            }

            switch (eHAlign)
            {
            case eCenter:
                fAlignX -= sRect.width / 2;
                break;
            case eRight:
                fAlignX -= sRect.width;
                break;
            default:
                break;
            }
            switch (eVAlign)
            {
            case eCenter:
                fAlignY -= sRect.height / 2;
                break;
            case eDown:
                fAlignY -= sRect.height;
                break;
            default:
                break;
            }

            cairo_move_to (m_pCairo, fAlignX, fAlignY);
            pango_cairo_show_layout (m_pCairo, pLayout);
            cairo_restore (m_pCairo);

            g_object_unref (pLayout);
        }
    }
}

//_oOo_
