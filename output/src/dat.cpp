/*!
 * @file        dat.cpp
 * @brief       dat output classes implementation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-dat-export.h"
#define LIBDSLTOOL_OUTPUT_EXPORT LIBDSLTOOL_DAT_EXPORT
//! @endcond

#include "config.h"

#include "dat.h"
#include "txt.h"

/*!
 * @implements  OutputVersion
 */
void OutputVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

/*!
 * @implements  OutputCreate
 */
COutput *OutputCreate (void)
{
    return new CDatOutput ();
}

//! @cond
namespace NStrDat
{
    STRING BandType             = "dsltype=\"%s\"\n";
    STRING BandBands            = "dslband=\"%d\"\n";

    STRING DslModemState        = "modemstate=\"%ld\"\n";
    STRING DslModemStateStr     = "modemstate_str=\"%s\"\n";
    STRING DslOperationMode     = "operationmode=\"%s\"\n";
    STRING DslProfile           = "profile=\"%s\"\n";
    STRING DslChannelMode       = "channelmode=\"%s\"\n";

    STRING DslNoiseMarginUp     = "noisemargin%s_up=\"%.1f\"\n";
    STRING DslNoiseMarginDown   = "noisemargin%s_down=\"%.1f\"\n";
    STRING DslAttenuationUp     = "attenuation%s_up=\"%.1f\"\n";
    STRING DslAttenuationDown   = "attenuation%s_down=\"%.1f\"\n";
    STRING DslTxPowerUp         = "txpower_up=\"%.1f\"\n";
    STRING DslTxPowerDown       = "txpower_down=\"%.1f\"\n";

    STRING BandWidthCellsUp     = "bandwidth_cells_up=\"%ld\"\n";
    STRING BandWidthCellsDown   = "bandwidth_cells_down=\"%ld\"\n";
    STRING BandWidthKBitsUp     = "bandwidth_kbit_up=\"%ld\"\n";
    STRING BandWidthKBitsDown   = "bandwidth_kbit_down=\"%ld\"\n";
    STRING BandWidthMaxKBitsUp  = "maxbandwidth_kbit_up=\"%ld\"\n";
    STRING BandWidthMaxKBitsDown= "maxbandwidth_kbit_down=\"%ld\"\n";

    STRING AtmVpi               = "ATM_VPI=\"%ld\"\n";
    STRING AtmVci               = "ATM_VCI=\"%ld\"\n";
    STRING AtmATU_C_Vendor      = "ATU_C_vendor=\"%-4.4s\"\n";
    STRING AtmATU_C_VendSpec    = "ATU_C_vendspec=\"%d.%d\"\n";
    STRING AtmATU_C_Revision    = "ATU_C_revision=\"%d\"\n";
    STRING AtmATU_R_Vendor      = "ATU_R_vendor=\"%-4.4s\"\n";
    STRING AtmATU_R_VendSpec    = "ATU_R_vendspec=\"%d.%d\"\n";
    STRING AtmATU_R_Revision    = "ATU_R_revision=\"%d\"\n";

    STRING ErrorFECTx           = "error_tx_FEC=\"%ld\"\n";
    STRING ErrorFECRx           = "error_rx_FEC=\"%ld\"\n";
    STRING ErrorCRCTx           = "error_tx_CRC=\"%ld\"\n";
    STRING ErrorCRCRx           = "error_rx_CRC=\"%ld\"\n";
    STRING ErrorHECTx           = "error_tx_HEC=\"%ld\"\n";
    STRING ErrorHECRx           = "error_rx_HEC=\"%ld\"\n";

    STRING FailErrorSecs15min   = "error_secs_15min=\"%ld\"\n";
    STRING FailErrorSecsDay     = "error_secs_day=\"%ld\"\n";
} // namespace NStrDat
//! @endcond

/*!
 * @fn          CDatOutput::CDatOutput (void)
 * @brief       constructor
 */
CDatOutput::CDatOutput (void)
: m_cFile ()
{
}

/*!
 * @fn          CDatOutput::Write (const CConfigData &cConfig,
 *                      const CDslData &cData,
 *                      bool bTones) = 0;
 * @brief       write data
 * @details     open .dat file and write key value pairs
 * @param[in]   cConfig
 *              configuration data
 * @param[in]   cData
 *              dsl data
 * @param[in]   bTones
 *              unused
 */
void CDatOutput::Write (const CConfigData &cConfig,
        const CDslData &cData,
        bool bTones)
{
    CString strFile;

    (void) bTones;

    strFile.Format (NStrDef::DatFile, cConfig.Path (), cConfig.Prefix ());

    if (m_cFile.Open (strFile, "w"))
    {
        Write (cData.m_cData.m_cTones.Bandplan (),
                NStrDat::BandType, NStrDat::BandBands);

        Write (cData.m_cData.m_cModemState,
                NStrDat::DslModemState);
        Write (cData.m_cData.m_cModemStateStr,
                NStrDat::DslModemStateStr);
        Write (cData.m_cData.m_cOperationMode,
                NStrDat::DslOperationMode);
        Write (cData.m_cData.m_cProfile,
                NStrDat::DslProfile);
        Write (cData.m_cData.m_cChannelMode,
                NStrDat::DslChannelMode);

        Write (cData.m_cData.m_cNoiseMargin,
                NStrDat::DslNoiseMarginUp, NStrDat::DslNoiseMarginDown);
        Write (cData.m_cData.m_cAttenuation,
                NStrDat::DslAttenuationUp, NStrDat::DslAttenuationDown);
        Write (cData.m_cData.m_cTxPower,
                NStrDat::DslTxPowerUp, NStrDat::DslTxPowerDown);

        Write (cData.m_cData.m_cBandwidth.m_cCells,
                NStrDat::BandWidthCellsUp , NStrDat::BandWidthCellsDown);
        Write (cData.m_cData.m_cBandwidth.m_cKbits,
                NStrDat::BandWidthKBitsUp , NStrDat::BandWidthKBitsDown);
        Write (cData.m_cData.m_cBandwidth.m_cMaxKbits,
                NStrDat::BandWidthMaxKBitsUp, NStrDat::BandWidthMaxKBitsDown);

        Write (cData.m_cData.m_cAtm.m_cVpiVci,
                NStrDat::AtmVpi, NStrDat::AtmVci);
        Write (cData.m_cData.m_cAtm.m_cATU_C,
                NStrDat::AtmATU_C_Vendor, NStrDat::AtmATU_C_VendSpec,
                NStrDat::AtmATU_C_Revision);
        Write (cData.m_cData.m_cAtm.m_cATU_R,
                NStrDat::AtmATU_R_Vendor, NStrDat::AtmATU_R_VendSpec,
                NStrDat::AtmATU_R_Revision);

        Write (cData.m_cData.m_cStatistics.m_cError.m_cFEC,
                NStrDat::ErrorFECTx, NStrDat::ErrorFECRx);
        Write (cData.m_cData.m_cStatistics.m_cError.m_cCRC,
                NStrDat::ErrorCRCTx, NStrDat::ErrorCRCRx);
        Write (cData.m_cData.m_cStatistics.m_cError.m_cHEC,
                NStrDat::ErrorHECTx, NStrDat::ErrorHECRx);

        Write (cData.m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min,
                NStrDat::FailErrorSecs15min);
        Write (cData.m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay,
                NStrDat::FailErrorSecsDay);

        m_cFile.Close ();
    }
}

/*!
 * @fn          void CDatOutput::Write (const CBandplan* pBandplan,
 *                      const char* strFmtType,
 *                      const char* strFmtBands)
 * @brief       write bandplan data
 * @param[in]   pBandplan
 *              bandplan pointer
 * @param[in]   strFmtType
 *              type format string
 * @param[in]   strFmtBands
 *              bands format string
 */
void CDatOutput::Write (const CBandplan* pBandplan,
        const char* strFmtType,
        const char* strFmtBands)
{
    m_cFile.Printf (strFmtType, pBandplan->Type ());
    m_cFile.Printf (strFmtBands, pBandplan->Bands ());
}

/*!
 * @fn          void CDatOutput::Write (const CDataInt &cData,
 *                      const char* strFmt)
 * @brief       write integer data
 * @param[in]   cData
 *              integer data
 * @param[in]   strFmt
 *              data format string
 */
void CDatOutput::Write (const CDataInt &cData,
        const char* strFmt)
{
    long nData;

    if (cData.Get (nData) && strFmt)
    {
        m_cFile.Printf (strFmt, nData);
    }
}

/*!
 * @fn          void CDatOutput::Write (const CDataIntUpDown &cData,
 *                      const char* strFmtUp,
 *                      const char* strFmtDown)
 * @brief       write integer up/down data
 * @param[in]   cData
 *              integer up/down data
 * @param[in]   strFmtUp
 *              upstream data format string
 * @param[in]   strFmtDown
 *              downstream data format string
 */
void CDatOutput::Write (const CDataIntUpDown &cData,
        const char* strFmtUp,
        const char* strFmtDown)
{
    long nData;

    if (cData.GetUp (nData) && strFmtUp)
    {
        m_cFile.Printf (strFmtUp, nData);
    }
    if (cData.GetDown (nData) && strFmtDown)
    {
        m_cFile.Printf (strFmtDown, nData);
    }
}

/*!
 * @fn          void CDatOutput::Write (const CDataDouble &cData,
 *                      const char* strFmt)
 * @brief       write double data
 * @param[in]   cData
 *              double data
 * @param[in]   strFmt
 *              data format string
 */
void CDatOutput::Write (const CDataDouble &cData,
        const char* strFmt)
{
    double fData;

    if (cData.Get (fData) && strFmt)
    {
        m_cFile.Printf (strFmt, fData);
    }
}

/*!
 * @fn          void CDatOutput::Write (const CDataDoubleUpDown &cData,
 *                      const char* strFmtUp,
 *                      const char* strFmtDown)
 * @brief       write double up/down data
 * @param[in]   cData
 *              double up/down data
 * @param[in]   strFmtUp
 *              upstream data format string
 * @param[in]   strFmtDown
 *              downstream data format string
 */
void CDatOutput::Write (const CDataDoubleUpDown &cData,
        const char* strFmtUp,
        const char* strFmtDown)
{
    double fData;

    if (cData.GetUp (fData) && strFmtUp)
    {
        m_cFile.Printf (strFmtUp, fData);
    }
    if (cData.GetDown (fData) && strFmtDown)
    {
        m_cFile.Printf (strFmtDown, fData);
    }
}

/*!
 * @fn          void CDatOutput::Write (const CArrayDoubleUpDown &cData,
 *                      const char* strFmtUp,
 *                      const char* strFmtDown)
 * @brief       write double up/down array data
 * @param[in]   cData
 *              double up/down array data
 * @param[in]   strFmtUp
 *              upstream data format string
 * @param[in]   strFmtDown
 *              downstream data format string
 */
void CDatOutput::Write (const CArrayDoubleUpDown &cData,
        const char* strFmtUp,
        const char* strFmtDown)
{
    char strIndex[4];
    double fData;
    size_t nIndex;
    size_t nSize = cData.Size ();

    if (strFmtUp)
    {
        for (nIndex = 0; nIndex < nSize; nIndex++)
        {
            if (nIndex)
            {
                snprintf (strIndex, sizeof (strIndex), "%lu", nIndex);
            }
            else
            {
                strIndex[0] = '\0';
            }

            if (cData.GetUp (nIndex, fData))
            {
                m_cFile.Printf (strFmtUp, strIndex, fData);
            }
        }
    }
    if (strFmtDown)
    {
        for (nIndex = 0; nIndex < nSize; nIndex++)
        {
            if (nIndex)
            {
                snprintf (strIndex, sizeof (strIndex), "%lu", nIndex);
            }
            else
            {
                strIndex[0] = '\0';
            }

            if (cData.GetDown (nIndex, fData))
            {
                m_cFile.Printf (strFmtDown, strIndex, fData);
            }
        }
    }
}

/*!
 * @fn          void CDatOutput::Write (const CDataString &cData,
 *                      const char* strFmt)
 * @brief       write string data
 * @param[in]   cData
 *              string data
 * @param[in]   strFmt
 *              format string
 */
void CDatOutput::Write (const CDataString &cData,
        const char* strFmt)
{
    CString strData;

    if (cData.Get (strData) && strFmt)
    {
        m_cFile.Printf (strFmt, (const char*)strData);
    }
}

/*!
 * @fn          void CDatOutput::Write (const CDataATU &cData,
 *                      const char* strFmtVendor,
 *                      const char* strFmtSpec,
 *                      const char* strFmtRevision)
 * @brief       write ATU-X data
 * @param[in]   cData
 *              ATU-X data
 * @param[in]   strFmtVendor
 *              vendor format string
 * @param[in]   strFmtSpec
 *              spec format string
 * @param[in]   strFmtRevision
 *              revision format string
 */
void CDatOutput::Write (const CDataATU &cData,
        const char* strFmtVendor,
        const char* strFmtSpec,
        const char* strFmtRevision)
{
    CString strVendor;
    int nSpec0, nSpec1, nRevision;

    if (cData.GetVendor (strVendor) && strFmtVendor)
    {
        m_cFile.Printf (strFmtVendor, (const char*)strVendor);
    }
    if (cData.GetSpec (nSpec0, nSpec1) && strFmtSpec)
    {
        m_cFile.Printf (strFmtSpec, nSpec0, nSpec1);
    }
    if (cData.GetRevision (nRevision) && strFmtRevision)
    {
        m_cFile.Printf (strFmtRevision, nRevision);
    }
}

//_oOo_
