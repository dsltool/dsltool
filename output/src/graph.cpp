/*!
 * @file        graph.cpp
 * @brief       graph class implementation
 * @details     graph output classes
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>

#include "graph.h"
#include "txt.h"
#include "log.h"

/*!
 * @fn          CGraph::CGraph (CImage &cImage, size_t nTones)
 * @brief       construtor
 * @details     set graph dimensions
 * @param[in]   cImage
 *              image reference
 * @param[in]   nTones
 *              number of tones (graph width)
 */
CGraph::CGraph (CImage &cImage, size_t nTones)
: m_cImage (cImage)
, m_nTones (nTones)
, m_nScale (1)
, m_nX0 (50)                    //!< graph origin (x)
, m_nY0 (40)                    //!< graph origin (y)
, m_nHScale (32)                //!< horiz. scale width
, m_nWidth (ToneWidth ())       //!< graph width
, m_nHeight (150)               //!< graph height
, m_nVScale (10)                //!< vert. scale width
, m_nVScaleWide (15)            //!< vert. scale width
, m_nMaxX (m_nX0 + m_nWidth)    //!< graph max postion X
, m_nMaxY (m_nY0 + m_nHeight)   //!< graph max postion Y
{
}

/*!
 * @fn          int CGraph::ToneScale (void)
 * @brief       calculate tone (graph) scaling
 * @return      tone scaling
 */
int CGraph::ToneScale (void)
{
    if (m_nTones > 512)
    {
        return (int)m_nTones / (512 * m_nScale);
    }

    return 1;
}

/*!
 * @fn          int CGraph::ToneWidth (void)
 * @brief       calculate tone (graph) width
 * @return      tone width
 */
int CGraph::ToneWidth (void)
{
    if (m_nTones > 512)
    {
        return 512 * m_nScale;
    }

    return 512;
}

/*!
 * @fn          bool CGraph::Init (void)
 * @brief       initialize graph
 * @details     draw background and border
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to init image
 */
bool CGraph::Init (void)
{
    bool bRet = false;
    double fWidth= m_nWidth + 100;
    double fHeight = m_nHeight + 100;

    if (m_cImage.Create (fWidth, fHeight))
    {
        m_cImage.Color (s_ulColorBgnd);
        m_cImage.Rect (0.0, 0.0, fWidth, fHeight);

        m_cImage.Color (s_ulColorBorderTL);
        m_cImage.Line (0.5, 0.5, fWidth - 0.5, 0.5);
        m_cImage.Line (1.5, 1.5, fWidth - 1.5, 1.5);
        m_cImage.Line (0.5, 0.5, 0.5, fHeight - 0.5);
        m_cImage.Line (1.5, 1.5, 1.5, fHeight - 1.5);

        m_cImage.Color (s_ulColorBorderBR);
        m_cImage.Line (0.5, fHeight - 0.5, fWidth - 0.5, fHeight - 0.5);
        m_cImage.Line (1.5, fHeight - 1.5, fWidth - 1.5, fHeight - 1.5);
        m_cImage.Line (fWidth - 0.5, 0.5, fWidth - 0.5, fHeight - 0.5);
        m_cImage.Line (fWidth - 1.5, 1.5, fWidth - 1.5, fHeight - 1.5);

        m_cImage.Color (s_ulColorGraph);
        m_cImage.Rect (m_nX0, m_nY0, m_nMaxX, m_nMaxY);

        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          void CGraph::Finish (void)
 * @brief       finish graph
 * @details     draw coordinates
 */
void CGraph::Finish (void)
{
    m_cImage.Color (s_ulColorCoord);

    m_cImage.Line (m_nX0 - 5, m_nMaxY, m_nMaxX + 5, m_nMaxY, 1.0, true);
    m_cImage.Line (m_nX0, m_nY0 - 5, m_nX0, m_nMaxY + 5, 1.0, true);
}

/*!
 * @fn          void CGraph::HScale (double fSpacing,
 *                      const char *strTone,
 *                      const char *strCaption)
 * @brief       draw horizontal scale bar and grid
 * @param[in]   fSpacing
 *              tone spacing [kHz]
 * @param[in]   strTone
 *              tone text
 * @param[in]   strCaption
 *              caption text
 */
void CGraph::HScale (double fSpacing,
        const char *strTone,
        const char *strCaption)
{
    const int nScale = ToneScale ();
    const int nWidth = ToneWidth ();
    int nX, nY0, nY1;
    char strText[16];

    m_cImage.Color (s_ulColorGrid);
    m_cImage.TextColor (s_ulColorText);

    nY0 = m_nY0 - 3;
    nY1 = m_nMaxY + 3;
    for (int i = m_nHScale; i <= nWidth ; i+= m_nHScale)
    {
        nX = m_nX0 + i;
        m_cImage.Line (nX, nY0, nX, nY1, 1.0, true);
    }

    nY0 = m_nY0 - 4;
    nY1 = m_nMaxY + 6;
    for (int i = 0; i <= nWidth; i+= m_nHScale * 2)
    {
        CImage::EAlign eHAlign = CImage::eCenter;
        nX = m_nX0 + i;

        snprintf (strText, sizeof (strText), "%d", i * nScale);
        if (nWidth == i)
        {
            eHAlign = CImage::eRight;
        }
        m_cImage.Text (nX, nY0, strText, eHAlign, CImage::eDown);

        snprintf (strText, sizeof (strText), "%.0lf", (double)(i * nScale  * fSpacing));
        m_cImage.Text (nX, nY1, strText, eHAlign , CImage::eUp);
    }

    m_cImage.Text (m_nMaxX + 4, m_nY0 - 4, strTone, CImage::eLeft, CImage::eDown);
    m_cImage.Text (m_nMaxX + 4, m_nMaxY + 6, strCaption, CImage::eLeft, CImage::eUp);
}

/*!
 * @fn          void CGraph::VScaleLeft (int nInc, int nMax,
 *                      const char *strCaption, bool bWide)
 * @brief       draw left scale bar, grid and caption
 * @param[in]   nInc
 *              increment between grid lines
 * @param[in]   nMax
 *              max value
 * @param[in]   strCaption
 *              caption text
 * @param[in]   bWide
 *              wide spacing between grid lines
 */
void CGraph::VScaleLeft (int nInc, int nMax,
        const char *strCaption, bool bWide)
{
    int nVScale = bWide ? m_nVScaleWide : m_nVScale;
    int nYMax = (m_nHeight / nVScale) + 1;
    int nX0, nX1, nY;
    char strText [16];

    m_cImage.Color (s_ulColorGrid);
    m_cImage.TextColor (s_ulColorText);

    nX0 = m_nX0 - 3;
    nX1 = m_nMaxX + 3;
    for (int i = 1; i < nYMax; i++)
    {
        nY = m_nMaxY - (i * nVScale);
        m_cImage.Line (nX0, nY, nX1, nY, 1.0, true);
    }

    nX0 = m_nX0 - 6;
    for (int i = 0; i < nYMax; i += nInc)
    {
        nY = m_nMaxY - (i * nVScale);

        snprintf (strText, sizeof (strText), "%d", i * nMax);
        m_cImage.Text (nX0, nY, strText, CImage::eRight, CImage::eCenter);
    }

    m_cImage.Text (m_nX0 - 30, (m_nY0 + m_nMaxY) / 2, strCaption,
            CImage::eCenter, CImage::eCenter, CImage::eLeft90);
}

/*!
 * @fn          void CGraph::VScaleRight (int nInc, int nMax,
 *                      const char *strCaption, bool bWide)
 * @brief       draw right scale bar and caption
 * @param[in]   nInc
 *              increment between grid lines
 * @param[in]   nMax
 *              max value
 * @param[in]   strCaption
 *              caption text
 * @param[in]   bWide
 *              wide spacing between grid lines
 */
void CGraph::VScaleRight (int nInc, int nMax, const char *strCaption, bool bWide)
{
    int nVScale = bWide ? m_nVScaleWide : m_nVScale;
    int nYMax = (m_nHeight / nVScale) + 1;
    int nX, nY;
    char strText [16];

    m_cImage.Color (s_ulColorGrid);
    m_cImage.TextColor (s_ulColorText);

    nX = m_nMaxX + 6;
    for (int i = 0; i < nYMax; i += nInc)
    {
        nY = m_nMaxY - (i * nVScale);

        snprintf (strText, sizeof (strText), "%d", i * nMax);
        m_cImage.Text (nX, nY, strText, CImage::eLeft, CImage::eCenter);
    }

    m_cImage.Text (m_nMaxX + 30, (m_nY0 + m_nMaxY) / 2, strCaption,
            CImage::eCenter, CImage::eCenter, CImage::eRight90);
}

/*!
 * @fn          void CGraph::Caption (int nIndex,
 *                      const char *strCaption, unsigned long ulColor)
 * @brief       draw caption block and text
 * @param[in]   nIndex
 *              position index
 * @param[in]   strCaption
 *              caption text
 * @param[in]   ulColor
 *              block fill color
 */
void CGraph::Caption (int nIndex,
        const char *strCaption, unsigned long ulColor)
{
    int nXBlock = m_nX0 + (nIndex * 128);
    int nXText = nXBlock + 15;
    int nY = m_nMaxY + 24;

    m_cImage.Color (ulColor);
    m_cImage.TextColor (s_ulColorText);

    m_cImage.Block (nXBlock, nY);

    m_cImage.Text (nXText, nY, strCaption, CImage::eLeft, CImage::eUp);
}

/*!
 * @fn          void CGraph::PlotBar (CTone &cData, unsigned long ulColor,
 *                      int nMult, int nDiv)
 * @brief       plot bar (vertical lines)
 * @param[in]   cData
 *              tone data
 * @param[in]   ulColor
 *              line color
 *              line color
 * @param[in]   nMult
 *              multiplier scaling factor
 * @param[in]   nDiv
 *              divisor scaling factor
 */
void CGraph::PlotBar (const CTone &cData, unsigned long ulColor,
        int nMult, int nDiv)
{
    size_t nTone;
    int nScale = ToneScale ();
    int nX, nY;

    m_cImage.Color (ulColor);

    for (nTone = 0; nTone < m_nTones; nTone++)
    {
        if (cData[nTone])
        {
            nX = m_nX0 + ((int)nTone / nScale);
            nY = m_nMaxY - ((cData[nTone] * nMult) / nDiv);
            if (nY < m_nY0)
            {
                nY = m_nY0;
            }
            m_cImage.Line (nX, m_nMaxY, nX, nY);
        }
    }
}

/*!
 * @fn          void CGraph::PlotDot (CTone &cData, unsigned long ulColor,
 *                      int nMult, int nDiv)
 * @brief       plot dots
 * @param[in]   cData
 *              tone data
 * @param[in]   ulColor
 *              line color
 * @param[in]   nMult
 *              multiplier scaling factor
 * @param[in]   nDiv
 *              divisor scaling factor
 */
void CGraph::PlotDot (const CTone &cData, unsigned long ulColor, int nMult, int nDiv)
{
    size_t nTone;
    int nScale = ToneScale ();
    int nX, nY;

    m_cImage.Color (ulColor);

    for (nTone = 0; nTone < m_nTones; nTone++)
    {
        if (cData[nTone])
        {
            nX = m_nX0 + ((int)nTone / nScale);
            nY = m_nMaxY - ((cData[nTone] * nMult) / nDiv);
            if (nY < m_nY0)
            {
                nY = m_nY0;
            }
            m_cImage.Line (nX, nY -1 , nX, nY + 1);
        }
    }
}

/*!
 * @fn          void CGraph::PlotMark (bool *pbData, unsigned long ulColor)
 * @brief       plot marks
 * @param[in]   pbData
 *              marker array
 * @param[in]   ulColor
 *              marker color
 */
void CGraph::PlotMark (bool *pbData, unsigned long ulColor)
{
    size_t nTone;
    int nScale = ToneScale ();
    int nX;

    if (pbData)
    {
        m_cImage.Color (ulColor);
        for (nTone = 0; nTone < m_nTones; nTone++)
        {
            if (pbData[nTone])
            {
                nX = m_nX0 + ((int)nTone / nScale);
                m_cImage.Line (nX, m_nY0 , nX, m_nY0 + 4, 3.0);
            }
        }
    }
}

//! @cond
namespace NStrPng
{
    STRING HScaleTone           = "tone";
    STRING HScaleKHz            = "kHz";
    STRING VScaleBits           = "bits";
    STRING VScaleDB             = "dB";
    STRING CaptionUp            = "Up-Stream";
    STRING CaptionDown          = "Down-Stream";
    STRING CaptionGap           = "GAP";
    STRING CaptionGainQ2        = "Gain Q2";
    STRING CaptionSNR           = "SNR";
    STRING CaptionNoiseMargin   = "Noisemargin";
    STRING CaptionChanCharLog   = "Channel characteristics";
} // namespace NStrPng
//! @endcond

/*!
 * @fn          CGraphBitalloc::CGraphBitalloc (CImage &cImage,
 *                      const CTonesGroup &cTones)
 * @brief       constructor
 * @param[in]   cImage
 *              image reference
 * @param[in]   cTones
 *              dsl data tones reference
 */
CGraphBitalloc::CGraphBitalloc (CImage &cImage, const CTonesGroup &cTones)
: CGraph (cImage, cTones.Bandplan ()->Tones ())
, m_cTones (cTones)
, m_pbGap (NULL)
{
}


/*!
 * @fn          CGraphBitalloc::~CGraphBitalloc (void)
 * @brief       destructor
 * @details     cleanup gap array
 */
CGraphBitalloc::~CGraphBitalloc (void)
{
    if (m_pbGap)
    {
        delete[] m_pbGap;
    }
}

/*!
 * @fn          bool CGraphBitalloc::Draw (void)
 * @brief       draw bitalloc graph
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
bool CGraphBitalloc::Draw (void)
{
    bool bRet = false;

    if (Init ())
    {
        HScale (m_cTones.Bandplan ()->Spacing (),
                NStrPng::HScaleTone, NStrPng::HScaleKHz);
        VScaleLeft (3, 1, NStrPng::VScaleBits);
        Caption (0, NStrPng::CaptionUp, s_ulColorUp);
        Caption (1, NStrPng::CaptionDown, s_ulColorDown);
        Caption (2, NStrPng::CaptionGap, s_ulColorGap);
        if (m_cTones.m_cGainQ2.Valid ())
        {
            VScaleRight (2, 10, NStrPng::VScaleDB);
            Caption (3, NStrPng::CaptionGainQ2, s_ulColorGainQ2);
        }

        if (m_cTones.m_cBitallocUp.Valid ())
        {
            PlotBar (m_cTones.m_cBitallocUp.Data (), s_ulColorUp, 10, 1);
        }
        if (m_cTones.m_cBitallocDown.Valid ())
        {
            PlotBar (m_cTones.m_cBitallocDown.Data (), s_ulColorDown, 10, 1);
        }
        FindGap (m_cTones.Bandplan (), m_cTones.m_cBitallocUp, m_cTones.m_cBitallocDown);
        PlotMark (m_pbGap, s_ulColorGap);

        if (m_cTones.m_cGainQ2.Valid ())
        {
            PlotDot (m_cTones.m_cGainQ2.Data (), s_ulColorGainQ2, 1, 1);
        }
        Finish ();
        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          void CGraphBitalloc::FindGap (bool *pbGap,
 *                      const CDataTone &cTone,
 *                      int nBandMin, int nBandMax)
 * @brief       find gap's in tone data
 * @param[out]  pbGap
 *              pointer to gap array, must be at min tone array size
 * @param[in]   cTone
 *              tone data reference
 * @param[in]   nBandMin
 *              min array index
 * @param[in]   nBandMax
 *              max array index
 */
void CGraphBitalloc::FindGap (bool *pbGap,
        const CDataTone &cTone,
        int nBandMin, int nBandMax)
{
    int nTone;
    int nMin = 0;
    int nMax = 0;

    if (cTone.Valid ())
    {
        const CTone &cData = cTone.Data ();

        for (nTone = nBandMin; nTone < nBandMax; nTone++)
        {
            if (cData[(size_t)nTone])
            {
                if (0 == nMin)
                {
                    nMin = nTone;
                }
                nMax = nTone;
            }
        }

        for (nTone = nMin; nTone < nMax; nTone++)
        {
            if (0 == cData[(size_t)nTone])
            {
                pbGap [nTone] = true;
            }
        }
    }
}

/*!
 * @fn          void CGraphBitalloc::FindGap (const CBandplan* pBandplan,
 *                      const CDataTone &cBitallocUp,
 *                      const CDataTone &cBitallocDown)
 * @brief       find gap's in tone data
 * @details     find in bandplan data ranges
 * @param[in]   pBandplan
 *              bandplan pointer
 * @param[in]   cBitallocUp
 *              upstream bitalloc data
 * @param[in]   cBitallocDown
 *              downstream bitalloc data
 */
void CGraphBitalloc::FindGap (const CBandplan* pBandplan,
        const CDataTone &cBitallocUp,
        const CDataTone &cBitallocDown)
{
    size_t nTone , nBand;

    if (m_pbGap)
    {
        delete[] m_pbGap;
    }
    m_pbGap = new bool[pBandplan->Tones ()];

    if (m_pbGap)
    {
        for (nTone = 0; nTone < pBandplan->Tones (); nTone++)
        {
            m_pbGap [nTone] = false;
        }

        for (nBand = 0;nBand < pBandplan->Bands (); nBand++)
        {
            FindGap (m_pbGap, cBitallocUp,
                    pBandplan->Band (nBand).MinUp (),
                    pBandplan->Band (nBand).MaxUp ());
            FindGap (m_pbGap, cBitallocDown,
                    pBandplan->Band (nBand).MinDown (),
                    pBandplan->Band (nBand).MaxDown ());
        }
    }

}

/*!
 * @fn          CGraphSNR::CGraphSNR (CImage &cImage,
 *                      const CTonesGroup &cTones)
 * @brief       constructor
 * @param[in]   cImage
 *              image reference
 * @param[in]   cTones
 *              dsl data tones reference
 */
CGraphSNR::CGraphSNR (CImage &cImage, const CTonesGroup &cTones)
: CGraph (cImage, cTones.Bandplan ()->Tones ())
, m_cTones (cTones)
{
}

/*!
 * @fn          bool CGraphSNR::Draw (void)
 * @brief       draw SNR graph
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
bool CGraphSNR::Draw (void)
{
    bool bRet = false;

    if (Init ())
    {
        HScale (m_cTones.Bandplan ()->Spacing (),
                NStrPng::HScaleTone, NStrPng::HScaleKHz);
        VScaleLeft (2, 5, NStrPng::VScaleDB);
        Caption (0, NStrPng::CaptionSNR, s_ulColorSNR);

        if (m_cTones.m_cNoiseMargin.Valid ())
        {
            Caption (1, NStrPng::CaptionNoiseMargin, s_ulColorNoiseMargin);
        }

        if (m_cTones.m_cSNR.Valid ())
        {
            PlotBar (m_cTones.m_cSNR.Data (), s_ulColorSNR, 2, 1);
        }
        if (m_cTones.m_cNoiseMargin.Valid ())
        {
            PlotBar (m_cTones.m_cNoiseMargin.Data (), s_ulColorNoiseMargin, 2, 1);
        }
        Finish ();
        bRet = true;
    }

    return bRet;
}

/*!
 * @fn          CGraphChar::CGraphChar (CImage &cImage,
 *                      const CTonesGroup &cTones)
 * @brief       constructor
 * @param[in]   cImage
 *              image reference
 * @param[in]   cTones
 *              dsl data tones reference
 */
CGraphChar::CGraphChar (CImage &cImage, const CTonesGroup &cTones)
: CGraph (cImage, cTones.Bandplan ()->Tones ())
, m_cTones (cTones)
{
}

/*!
 * @fn          bool CGraphChar::Draw (void)
 * @brief       draw char graph
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failure
 */
bool CGraphChar::Draw (void)
{
    bool bRet = false;

    if (Init ())
    {
        HScale (m_cTones.Bandplan ()->Spacing (),
                NStrPng::HScaleTone, NStrPng::HScaleKHz);
        VScaleLeft (2, 10, NULL, true);
        Caption (0, NStrPng::CaptionChanCharLog, s_ulColorChar);

        PlotBar (m_cTones.m_cChanCharLog.Data (), s_ulColorChar, 3, 2);

        Finish ();
        bRet = true;
    }

    return bRet;
}

//_oOo_
