/*!
 * @file        collect.cpp
 * @brief       collectd interface definitions
 * @details     rrdtool/collectd access
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        15.10.2015
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>

#include "collect.h"
#include "log.h"
#include "txt.h"

/*!
 * @fn          CCollect::CCollect (int nInterval)
 * @brief       constructor
 * @details     initializes value list
 */
CCollect::CCollect (void)
: m_nInterval (10)
, m_pConnection (NULL)
, m_tValue ()
, m_nType ()
, m_tValueList ()
{
    m_tValueList.values = &m_tValue;
    m_tValueList.values_types = &m_nType;
    m_tValueList.values_len = 1;
}

/*!
 * @fn          bool CCollect::Open (const char *strHost,
 *                      const char *strSock,
 *                      const char *strModem,
 *                      int nInterval,
 *                      time_t tTime)
 * @brief       open connection
 * @details     create sockect connection
 * @param[in]   strHost
 *              host name
 * @param[in]   strSock
 *              socket name
 * @param[in]   strModem
 *              modem name
 * @param[in]   nInterval
 *              update interval [s]
 * @param[in]   tTime
 *              timestamp
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to open connection
 */
bool CCollect::Open (const char *strHost,
        const char *strSock,
        const char *strModem,
        int nInterval,
        time_t tTime)
{
    bool bRet = false;
    lcc_connection_t *pConnection = NULL;

    m_nInterval = nInterval;

    if (0 == lcc_connect (strSock, &pConnection))
    {
        m_pConnection = pConnection;

        strncpy (m_tValueList.identifier.host, strHost,
                sizeof (m_tValueList.identifier.host));
        strncpy (m_tValueList.identifier.plugin, "dsltool",
                sizeof (m_tValueList.identifier.plugin));
        strncpy (m_tValueList.identifier.plugin_instance, strModem,
                sizeof (m_tValueList.identifier.plugin_instance));

        m_tValueList.time = (double)tTime;
        m_tValueList.interval = Interval ();

        bRet = true;
    }
    else
    {
        LogError (NStrErr::CollectOpen, lcc_strerror (pConnection));
    }
    return bRet;
}

/*!
 * @fn          void CCollect::Close (void)
 * @brief       close connection
 */
void CCollect::Close (void)
{
    if (m_pConnection)
    {
        lcc_disconnect (m_pConnection);
        m_pConnection = NULL;
    }
}

/*!
 * @fn          bool CCollect::Put (const char *strType,
 *                      const char *strInst,
 *                      double fValue)
 * @brief       send variable to collectd
 * @param[in]   strType
 *              variable type
 * @param[in]   strInst
 *              variable instance
 * @param[in]   fValue
 *              variable value
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to put value
 */
bool CCollect::Put (const char *strType,
        const char *strInst,
        double fValue)
{
    bool bRet = false;

    if (m_pConnection)
    {
        m_tValue.gauge = fValue;
        m_nType = LCC_TYPE_GAUGE;

        strncpy (m_tValueList.identifier.type, strType,
                sizeof (m_tValueList.identifier.type));
        strncpy (m_tValueList.identifier.type_instance, strInst,
                sizeof (m_tValueList.identifier.type_instance));

        if (0 == lcc_putval (m_pConnection, &m_tValueList))
        {
            bRet = true;
        }
        else
        {
            LogError (NStrErr::CollectPut, lcc_strerror (m_pConnection));
        }
    }
    return bRet;
}

//_oOo_
