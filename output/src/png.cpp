/*!
 * @file        png.cpp
 * @brief       png output classes implementation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-png-export.h"
#define LIBDSLTOOL_OUTPUT_EXPORT LIBDSLTOOL_PNG_EXPORT
//! @endcond

#include "config.h"

#include "png.h"
#include "graph.h"
#include "image-png.h"
#include "txt.h"

/*!
 * @implements  OutputVersion
 */
void LIBDSLTOOL_OUTPUT_EXPORT OutputVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

/*!
 * @implements  OutputCreate
 */
COutput LIBDSLTOOL_OUTPUT_EXPORT *OutputCreate (void)
{
    return new CPngOutput ();
}

/*!
 * @fn          CPngOutput::CPngOutput (void)
 * @brief       constructor
 */
CPngOutput::CPngOutput (void)
{
}

/*!
 * @fn          CPngOutput::Write (const CConfigData &cConfig,
 *                      const CDslData &cData,
 *                      bool bTones) = 0;
 * @brief       write data
 * @details     create .png files
 * @param[in]   cConfig
 *              configuration data
 * @param[in]   cData
 *              dsl data
 * @param[in]   bTones
 *              unused
 */
void CPngOutput::Write (const CConfigData &cConfig,
        const CDslData &cData,
        bool bTones)
{
    CString strFile;

    (void)bTones;

    strFile.Format (NStrDef::PngBitsFile, cConfig.Path (), cConfig.Prefix ());
    WriteBits (strFile, cData.m_cData.m_cTones);

    strFile.Format (NStrDef::PngSNRFile, cConfig.Path (), cConfig.Prefix ());
    WriteSNR (strFile, cData.m_cData.m_cTones);

    strFile.Format (NStrDef::PngCharFile, cConfig.Path (), cConfig.Prefix ());
    WriteChar (strFile, cData.m_cData.m_cTones);
}

/*!
 * @fn          void CPngOutput::WriteBits (const char *strFile,
 *                      const CTonesGroup &cTones)
 * @brief       write bit alloc graph
 * @param[in]   strFile
 *              file name
 * @param[in]   cTones
 *              tones data group
 */
void CPngOutput::WriteBits (const char *strFile, const CTonesGroup &cTones)
{
    CFile::Delete (strFile);

    if (cTones.m_cBitallocUp.Valid () ||
        cTones.m_cBitallocDown.Valid () ||
        cTones.m_cGainQ2.Valid ())
    {
        CImagePng cImage;
        CGraphBitalloc cGraph (cImage, cTones);

        if (cGraph.Draw ())
        {
            cImage.Save (strFile);
        }
    }
}

/*!
 * @fn          void CPngOutput::WriteSNR (const char *strFile,
 *                      const CTonesGroup &cTones)
 * @brief       write SNR graph
 * @param[in]   strFile
 *              file name
 * @param[in]   cTones
 *              tones data group
 */
void CPngOutput::WriteSNR (const char *strFile, const CTonesGroup &cTones)
{
    CFile::Delete (strFile);

    if (cTones.m_cSNR.Valid () ||
            cTones.m_cNoiseMargin.Valid ())
    {
        CImagePng cImage;
        CGraphSNR cGraph (cImage, cTones);

        if (cGraph.Draw ())
        {
            cImage.Save (strFile);
        }
    }
}

/*!
 * @fn          void CPngOutput::WriteChar (const char *strFile,
 *                      const CTonesGroup &cTones)
 * @brief       write char graph
 * @param[in]   strFile
 *              file name
 * @param[in]   cTones
 *              tones data group
 */
void CPngOutput::WriteChar (const char *strFile, const CTonesGroup &cTones)
{
    CFile::Delete (strFile);

    if (cTones.m_cChanCharLog.Valid ())
    {
        CImagePng cImage;
        CGraphChar cGraph (cImage, cTones);

        if (cGraph.Draw ())
        {
            cImage.Save (strFile);
        }
    }
}

//_oOo_
