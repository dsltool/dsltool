/*!
 * @file        image-qt.cpp
 * @brief       Qt image class implementation
 * @details     QPainter based image class
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        11.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "image-qt.h"

/*!
 * @fn          CImageQt::CImageQt (QPicture *pPicture)
 * @brief       constructor
 */
CImageQt::CImageQt (QPicture *pPicture)
: m_cPainter (pPicture)
, m_cColor ()
, m_cTextColor ()
{
}

/*!
 * @fn          CImageQt::~CImageQt (void)
 * @brief       destructor
 * @details     free pango/cairo resources
 */
CImageQt::~CImageQt (void)
{
}


/*!
 * @fn          bool CImageQt::Create (double fWidth, double fHeight)
 * @brief       create image
 * @param[in]   fWidth
 *              unused
 * @param[in]   fHeight
 *              unused
 * @return      success
 * @retval      true
 *              o.k.
 * @retval      false
 *              failed to create image
 */
bool CImageQt::Create (double fWidth, double fHeight)
{
    (void) fWidth;
    (void) fHeight;

    m_cPainter.setFont (QFont ("Courier", 8, QFont::Normal));
    m_cPainter.setRenderHint(QPainter::Antialiasing, false);

    return true;
}

/*!
 * @fn          void CImageQt::Color (unsigned long ulColor)
 * @brief       set paint color
 * @param[in]   ulColor
 *              color value (0x00rrggbb)
 */
void CImageQt::Color (unsigned long ulColor)
{
    m_cColor = QColor ((QRgb)ulColor);
}

/*!
 * @fn          void CImageQt::TextColor (unsigned long ulTextColor)
 * @brief       set text color
 * @param[in]   ulTextColor
 *              color value (0x00rrggbb)
 */
void CImageQt::TextColor (unsigned long ulTextColor)
{
    m_cTextColor = QColor ((QRgb)ulTextColor);
}

/*!
 * @fn          void CImageQt::Line (double fFromX, double fFromY,
 *                      double fToX, double fToY,
 *                      double fWidth, bool fWidth)
 * @brief       draw line
 * @details     use @ref Color to set line color before calling @ref Line
 * @param[in]   fFromX
 *              start point x coordinate
 * @param[in]   fFromY
 *              start point y coordinate
 * @param[in]   fToX
 *              end point x coordinate
 * @param[in]   fToY
 *              end point y coordinate
 * @param[in]   fWidth
 *              line width
 * @param[in]   fWidth
 *              dash flag
 */
void CImageQt::Line (double fFromX, double fFromY,
        double fToX, double fToY,
        double fWidth, bool bDash)
{
    QPen cPen (QBrush (m_cColor), fWidth, Qt::SolidLine, Qt::FlatCap);
    if (bDash)
    {
        QVector<qreal> cDash;
        cDash << 0.5 << 0.5;
        cPen.setDashPattern (cDash);
    }
    m_cPainter.setPen (cPen);
    m_cPainter.drawLine (QPointF (fFromX, fFromY), QPointF (fToX, fToY));
}

/*!
 * @fn          void CImageQt::Rect (double fFromX, double fFromY,
 *                      double fToX, double fToY)
 * @brief       draw filled rectangle
 * @details     use @ref Color to set fill color before calling @ref Rect
 * @param[in]   fFromX
 *              start point x coordinate
 * @param[in]   fFromY
 *              start point y coordinate
 * @param[in]   fToX
 *              end point x coordinate
 * @param[in]   fToY
 *              end point y coordinate
 */
void CImageQt::Rect (double fFromX, double fFromY,
        double fToX, double fToY)
{
    m_cPainter.fillRect (QRectF (QPointF (fFromX, fFromY), QPointF (fToX, fToY)),
            m_cColor);
}

/*!
 * @fn          void CImageQt::Block (double fX, double fY)
 * @brief       draw filled rectangle with border
 * @details     use @ref Color to set fill color and
 *              @ref TextColor to set border color before calling @ref Block
 * @param[in]   fX
 *              start point x coordinate
 * @param[in]   fY
 *              start point y coordinate
 */
void CImageQt::Block (double fX, double fY)
{
    m_cPainter.fillRect (QRectF (QPointF (fX, fY), QSizeF (8.0, 8.0)),
            m_cColor);
    m_cPainter.setPen (QPen (QBrush (m_cTextColor), 1.0));
    m_cPainter.drawRect (QRectF (QPointF (fX, fY), QSizeF (8.0, 8.0)));
}

/*!
 * @fn          void CImageQt::Text (double fX, double fY, const char *strText,
 *                      EAlign eHAlign, EAlign eVAlign, ERotate eRotate)
 * @brief       draw text
 * @details     use @ref TextColor to set text color before calling @ref Text
 * @param[in]   fX
 *              start point x coordinate
 * @param[in]   fY
 *              start point y coordinate
 * @param[in]   strText
 *              text to draw
 * @param[in]   eHAlign
 *              horzontal alignement
 * @param[in]   eVAlign
 *              vertical alignement
 * @param[in]   eRotate
 *              rotation
 */
void CImageQt::Text (double fX, double fY, const char *strText,
        EAlign eHAlign, EAlign eVAlign, ERotate eRotate)
{
    QRectF cRect;
    int nFlags = 0;
    switch (eHAlign)
    {
    case eLeft:
        nFlags |= Qt::AlignLeft;
        break;
    case eCenter:
        nFlags |= Qt::AlignHCenter;
        break;
    case eRight:
        nFlags |= Qt::AlignRight;
        break;
    }
    switch (eVAlign)
    {
    case eUp:
        nFlags |= Qt::AlignTop;
        break;
    case eCenter:
        nFlags |= Qt::AlignVCenter;
        break;
    case eDown:
        nFlags |= Qt::AlignBottom;
        break;
    }


    m_cPainter.save (); // save before translation / rotation
    m_cPainter.translate (QPointF (fX, fY));
    switch (eRotate)
    {
    case eNone:
        break;
    case eLeft90:
        m_cPainter.rotate (-90.0);
        break;
    case eRight90:
        m_cPainter.rotate (90.0);
        break;
    }
    cRect = m_cPainter.boundingRect (QRectF (), nFlags, QString (strText));

    m_cPainter.setPen (QPen (m_cTextColor));
    m_cPainter.drawText (cRect, QString (strText));
    m_cPainter.restore ();
}

//_oOo_
