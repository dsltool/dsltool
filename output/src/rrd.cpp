/*!
 * @file        rrd.cpp
 * @brief       rrd output classes implementation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-rrd-export.h"
#define LIBDSLTOOL_OUTPUT_EXPORT LIBDSLTOOL_RRD_EXPORT
//! @endcond

#include "config.h"

#include "rrd.h"
#include "txt.h"

/*!
 * @implements  OutputVersion
 */
void LIBDSLTOOL_OUTPUT_EXPORT OutputVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

/*!
 * @implements  OutputCreate
 */
COutput LIBDSLTOOL_OUTPUT_EXPORT *OutputCreate (void)
{
    return new CRrdOutput ();
}

//! @cond
namespace NStrRrd
{
    STRING TypeModemState       = "modemstate";
    STRING TypeBandWidth        = "bandwidth";
    STRING TypeBandWidthMax     = "bandwidth-max";
    STRING TypeAttenuation      = "attenuation%s";
    STRING TypeNoiseMargin      = "noisemargin%s";
    STRING TypeTxPower          = "txpower";
    STRING TypeStatistics       = "statistics";

    STRING InstEmpty            = "";
    STRING InstUp               = "up";
    STRING InstDown             = "down";
    STRING InstUpKBit           = "kbit-up";
    STRING InstDownKBit         = "kbit-down";
    STRING InstTxFEC            = "error-FEC-Tx";
    STRING InstRxFEC            = "error-FEC-Rx";
    STRING InstTxCRC            = "error-CRC-Tx";
    STRING InstRxCRC            = "error-CRC-Rx";
    STRING InstTxHEC            = "error-HEC-Tx";
    STRING InstRxHEC            = "error-HEC-Rx";
    STRING InstErrorSecs15min   = "failure-errsec-15min";
    STRING InstErrorSecsDay     = "failure-errsec-day";
} // namespace NStrCollect
//! @endcond

/*!
 * @fn          CRrdOutput::CRrdOutput (void)
 * @brief       constructor
 */
CRrdOutput::CRrdOutput (void)
: m_cCollect ()
{
}

/*!
 * @fn          CRrdOutput::Write (const CConfigData &cConfig,
 *                      const CDslData &cData,
 *                      bool bTones) = 0;
 * @brief       write data
 * @details     open .dat file and write key value pairs
 * @param[in]   cConfig
 *              configuration data
 * @param[in]   cData
 *              dsl data
 * @param[in]   bTones
 *              unused
 */
void CRrdOutput::Write (const CConfigData &cConfig,
        const CDslData &cData,
        bool bTones)
{
    (void) bTones;

    if (m_cCollect.Open (cConfig.CollectHost (),
            cConfig.CollectSock (),
            cConfig.ModemHost (),
            cConfig.Interval (),
            cData.TimeStamp ()))
    {
        Write (cData.m_cData.m_cModemState,
                NStrRrd::TypeModemState,
                NStrRrd::InstEmpty);

        Write (cData.m_cData.m_cNoiseMargin,
                NStrRrd::TypeNoiseMargin,
                NStrRrd::InstUp, NStrRrd::InstDown);
        Write (cData.m_cData.m_cAttenuation,
                NStrRrd::TypeAttenuation,
                NStrRrd::InstUp, NStrRrd::InstDown);
        Write (cData.m_cData.m_cTxPower,
                NStrRrd::TypeTxPower,
                NStrRrd::InstUp, NStrRrd::InstDown);

        Write (cData.m_cData.m_cBandwidth.m_cKbits,
                NStrRrd::TypeBandWidth,
                NStrRrd::InstUpKBit, NStrRrd::InstDownKBit);
        Write (cData.m_cData.m_cBandwidth.m_cMaxKbits,
                NStrRrd::TypeBandWidthMax,
                NStrRrd::InstUpKBit, NStrRrd::InstDownKBit);

        Write (cData.m_cData.m_cStatistics.m_cError.m_cFEC,
                NStrRrd::TypeStatistics,
                NStrRrd::InstTxFEC, NStrRrd::InstRxFEC);
        Write (cData.m_cData.m_cStatistics.m_cError.m_cCRC,
                NStrRrd::TypeStatistics,
                NStrRrd::InstTxCRC, NStrRrd::InstRxCRC);
        Write (cData.m_cData.m_cStatistics.m_cError.m_cHEC,
                NStrRrd::TypeStatistics,
                NStrRrd::InstTxHEC, NStrRrd::InstRxHEC);

        Write (cData.m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min,
                NStrRrd::TypeStatistics,
                NStrRrd::InstErrorSecs15min);
        Write (cData.m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay,
                NStrRrd::TypeStatistics,
                NStrRrd::InstErrorSecsDay);

        m_cCollect.Close ();
    }
}

/*!
 * @fn          void CRrdOutput::Write (const CDataInt &cData,
 *                      const char* strType,
 *                      const char* strInst)
 * @brief       write integer data
 * @param[in]   cData
 *              integer data
 * @param[in]   strType
 *              collect type
 * @param[in]   strInst
 *              collect instance
 */
void CRrdOutput::Write (const CDataInt &cData,
        const char* strType,
        const char* strInst)
{
    long nData;

    if (strType && strInst)
    {
        if (cData.IsDiff ())
        {
            if (cData.Diff (nData))
            {
                m_cCollect.Put (strType, strInst,
                        (double)(nData) / m_cCollect.Interval ());
            }
        }
        else
        {
            if (cData.Get (nData))
            {
                m_cCollect.Put (strType, strInst,
                        (double)nData);
            }
        }
    }
}

/*!
 * @fn          void CRrdOutput::Write (const CDataIntUpDown &cData,
 *                      const char* strType,
 *                      const char* strInstUp,
 *                      const char* strInstDown)
 * @brief       write integer up/down data
 * @param[in]   cData
 *              integer up/down data
 * @param[in]   strType
 *              collect type
 * @param[in]   strInstUp
 *              upstream collect instance
 * @param[in]   strInstDown
 *              downstream collect instance
 */
void CRrdOutput::Write (const CDataIntUpDown &cData,
        const char* strType,
        const char* strInstUp,
        const char* strInstDown)
{
    long nData;

    if (strType)
    {
        if (strInstUp)
        {
            if (cData.IsDiff ())
            {
                if (cData.DiffUp (nData))
                {
                    m_cCollect.Put (strType, strInstUp,
                            (double)(nData) / m_cCollect.Interval ());
                }
            }
            else
            {
                if (cData.GetUp (nData))
                {
                    m_cCollect.Put (strType, strInstUp,
                            (double)nData);
                }
            }
        }
        if (strInstDown)
        {
            if (cData.IsDiff ())
            {
                if (cData.DiffDown (nData))
                {
                    m_cCollect.Put (strType, strInstDown,
                            (double)(nData) / m_cCollect.Interval ());
                }
            }
            else
            {
                if (cData.GetDown (nData))
                {
                    m_cCollect.Put (strType, strInstDown,
                            (double)nData);
                }
            }
        }
    }
}

/*!
 * @fn          void CRrdOutput::Write (const CDataDouble &cData,
 *                      const char* strType,
 *                      const char* strInst)
 * @brief       write double data
 * @param[in]   cData
 *              double data
 * @param[in]   strType
 *              collect type
 * @param[in]   strInst
 *              collect instance
 */
void CRrdOutput::Write (const CDataDouble &cData,
        const char* strType,
        const char* strInst)
{
    double fData;

    if (strType && strInst)
    {
        if (cData.IsDiff ())
        {
            if (cData.Diff (fData))
            {
                m_cCollect.Put (strType, strInst,
                        (double)(fData) / m_cCollect.Interval ());
            }
        }
        else
        {
            if (cData.Get (fData))
            {
                m_cCollect.Put (strType, strInst,
                        (double)fData);
            }
        }
    }
}

/*!
 * @fn          void CRrdOutput::Write (const CDataDoubleUpDown &cData,
 *                      const char* strType,
 *                      const char* strInstUp,
 *                      const char* strInstDown)
 * @brief       write double up/down data
 * @param[in]   cData
 *              double up/down data
 * @param[in]   strType
 *              collect type
 * @param[in]   strInstUp
 *              upstream collect instance
 * @param[in]   strInstDown
 *              downstream collect instance
 */
void CRrdOutput::Write (const CDataDoubleUpDown &cData,
        const char* strType,
        const char* strInstUp,
        const char* strInstDown)
{
    double fData;

    if (strType)
    {
        if (strInstUp)
        {
            if (cData.IsDiff ())
            {
                if (cData.DiffUp (fData))
                {
                    m_cCollect.Put (strType, strInstUp,
                            fData / m_cCollect.Interval ());
                }
            }
            else
            {
                if (cData.GetUp (fData))
                {
                    m_cCollect.Put (strType, strInstUp,
                            fData);
                }
            }
        }
        if (strInstDown)
        {
            if (cData.IsDiff ())
            {
                if (cData.DiffDown (fData))
                {
                    m_cCollect.Put (strType, strInstDown,
                            fData / m_cCollect.Interval ());
                }
            }
            else
            {
                if (cData.GetDown (fData))
                {
                    m_cCollect.Put (strType, strInstDown,
                            (double)fData);
                }
            }
        }
    }
}

/*!
 * @fn          void CRrdOutput::Write (const CArrayDoubleUpDown &cData,
 *                      const char* strType,
 *                      const char* strInstUp,
 *                      const char* strInstDown)
 * @brief       write double up/down array data
 * @param[in]   cData
 *              double up/down array data
 * @param[in]   strType
 *              collect type
 * @param[in]   strInstUp
 *              upstream collect instance
 * @param[in]   strInstDown
 *              downstream collect instance
 */
void CRrdOutput::Write (const CArrayDoubleUpDown &cData,
        const char* strType,
        const char* strInstUp,
        const char* strInstDown)
{
    char strIndex[4];
    char strTypeIdx[LCC_NAME_LEN];
    double fData;
    size_t nIndex;
    size_t nSize = cData.Size ();

    if (strType)
    {
        for (nIndex = 0; nIndex < nSize; nIndex++)
        {
            if (nIndex)
            {
                snprintf (strIndex, sizeof (strIndex), "%lu", nIndex);
            }
            else
            {
                strIndex[0] = '\0';
            }
            snprintf (strTypeIdx, sizeof (strTypeIdx), strType, strIndex);

            if (strInstUp)
            {
                if (cData.IsDiff ())
                {
                    if (cData.DiffUp (nIndex, fData))
                    {
                        m_cCollect.Put (strTypeIdx, strInstUp,
                                fData / m_cCollect.Interval ());
                    }
                }
                else
                {
                    if (cData.GetUp (nIndex, fData))
                    {
                        m_cCollect.Put (strTypeIdx, strInstUp,
                                fData);
                    }
                }
            }
            if (strInstDown)
            {
                if (cData.IsDiff ())
                {
                    if (cData.DiffDown (nIndex, fData))
                    {
                        m_cCollect.Put (strTypeIdx, strInstDown,
                                fData / m_cCollect.Interval ());
                    }
                }
                else
                {
                    if (cData.GetDown (nIndex, fData))
                    {
                        m_cCollect.Put (strTypeIdx, strInstDown,
                                fData);
                    }
                }
            }
        }
    }

}

//_oOo_

