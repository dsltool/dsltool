/*!
 * @file        text.cpp
 * @brief       text output classes implementation
 *
 * @author      Carsten Spie&szlig; dsltool@carsten-spiess.de
 * @date        01.05.2016
 *
 * @copyright
 *      This file is part of @b dsltool.
 * @n
 *      @b dsltool is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 * @n
 *      dsltool is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 * @n
 *      You should have received a copy of the GNU General Public License
 *      along with dsltool.  If not, see <http://www.gnu.org/licenses/>.
 */

//! @cond
#include "libdsltool-text-export.h"
#define LIBDSLTOOL_OUTPUT_EXPORT LIBDSLTOOL_TEXT_EXPORT
//! @endcond

#include "config.h"

#include <math.h>

#include "text.h"
#include "txt.h"

/*!
 * @implements  OutputVersion
 */
void LIBDSLTOOL_OUTPUT_EXPORT OutputVersion (int &nMajor, int &nMinor, int &nSub)
{
    nMajor = VERSION_MAJOR;
    nMinor = VERSION_MINOR;
    nSub   = VERSION_SUB;
}

/*!
 * @implements  OutputCreate
 */
COutput LIBDSLTOOL_OUTPUT_EXPORT *OutputCreate (void)
{
    return new CTextOutput ();
}

//! @cond
namespace NStrText
{
    STRING BandType             = "DSL Type                : %s\n";
    STRING BandTones            = "Tones :  %5d             Spacing : %.4f kHz\n";
    STRING BandBands            = "DSL Bands               : %d\n";
    STRING BandUp               = "  Up   %d : Tone %7d - %-7d    %9.4f - %.4f kHz\n";
    STRING BandDown             = "  Down %d : Tone %7d - %-7d    %9.4f - %.4f kHz\n";

    STRING DslModemState        = "Modemstate              : (%ld)\n";
    STRING DslModemStateStr     = "Modemstate              : \"%s\"\n";
    STRING DslOperationMode     = "Operation Mode          : \"%s\"\n";
    STRING DslProfile           = "Profile                 : \"%s\"\n";
    STRING DslChannelMode       = "Channel Mode            : \"%s\"\n";
    STRING DslNoiseMargin       = "Noise Margin(%d)    [dB] : Up   %6.1f  Down %6.1f\n";
    STRING DslAttenuation       = "Attenuation(%d)     [dB] : Up   %6.1f  Down %6.1f\n";
    STRING DslTxPower           = "Tx Power          [dBm] : Up   %6.1f  Down %6.1f\n";

    STRING BandWidthCells       = "Bandwidth     [Cells/s] : Up   %6d  Down %6d\n";
    STRING BandWidthKBits       = "Bandwidth      [kBit/s] : Up   %6d  Down %6d\n";
    STRING BandWidthMaxKBits    = "Max.Bandwidth  [kBit/s] : Up   %6d  Down %6d\n";

    STRING AtmVpiVci            = "VPI : %3d      VCI      : %3d\n";
    STRING AtmATU_C_Vendor      = "ATU-C Vendor: %4s\n";
    STRING AtmATU_C_VendSpec    = "ATU-C V-Spec: %3d.%-3d\n";
    STRING AtmATU_C_Revision    = "ATU-C Revision: %3d\n";
    STRING AtmATU_R_Vendor      = "ATU-R Vendor: %4s\n";
    STRING AtmATU_R_VendSpec    = "ATU-R V-Spec: %3d.%-3d\n";
    STRING AtmATU_R_Revision    = "ATU-R Revision: %3d\n";

    STRING ErrorFEC             = "Error FEC               : Tx  %12ld     Rx   %12ld\n";
    STRING ErrorCRC             = "Error CRC               : Tx  %12ld     Rx   %12ld\n";
    STRING ErrorHEC             = "Error HEC               : Tx  %12ld     Rx   %12ld\n";

    STRING FailErrorSecs15min   = "Failure   [error s]     : %6d/15min\n";
    STRING FailErrorSecsDay     = "Failure   [error s]     : %6d/day\n";

    STRING Tone                 = "\nTone %3d :";

    STRING TonesBitallocUp      = "\nBitalloc Up";
    STRING TonesBitallocDown    = "\nBitalloc Down";
    STRING TonesSNR             = "\nSNR";
    STRING TonesGainQ2          = "\nGain Q2";
    STRING TonesNoiseMargin     = "\nNoiseMargin";
    STRING TonesChanCharLog     = "\nChanCharLog";
} // namespace NStrText
//! @endcond

/*!
 * @fn          CTextOutput::CTextOutput (void)
 * @brief       constructor
 */
CTextOutput::CTextOutput (void)
: m_cFile ()
{
}

/*!
 * @fn          CTextOutput::Write (const CConfigData &cConfig,
 *                      const CDslData &cData,
 *                      bool bTones) = 0;
 * @brief       write data
 * @details     open stdout and write formated text
 * @param[in]   cConfig
 *              configuration data
 * @param[in]   cData
 *              dsl data
 * @param[in]   bTones
 *              unused
 */
void CTextOutput::Write (const CConfigData &cConfig,
        const CDslData &cData,
        bool bTones)
{
    CString strFile;

    (void) bTones;

    strFile.Format (NStrDef::DatFile, cConfig.Path (), cConfig.Prefix ());

    if (m_cFile.Open (stdout))
    {
        Write (cData.m_cData.m_cTones.Bandplan (),
                NStrText::BandType, NStrText::BandTones, NStrText::BandBands,
                NStrText::BandUp, NStrText::BandDown);

        Write (cData.m_cData.m_cModemState,
                NStrText::DslModemState);
        Write (cData.m_cData.m_cModemStateStr,
                NStrText::DslModemStateStr);
        Write (cData.m_cData.m_cOperationMode,
                NStrText::DslOperationMode);
        Write (cData.m_cData.m_cProfile,
                NStrText::DslProfile);
        Write (cData.m_cData.m_cChannelMode,
                NStrText::DslChannelMode);

        Write (cData.m_cData.m_cNoiseMargin,
                NStrText::DslNoiseMargin);
        Write (cData.m_cData.m_cAttenuation,
                NStrText::DslAttenuation);
        Write (cData.m_cData.m_cTxPower,
                NStrText::DslTxPower);

        Write (cData.m_cData.m_cBandwidth.m_cCells,
                NStrText::BandWidthCells);
        Write (cData.m_cData.m_cBandwidth.m_cKbits,
                NStrText::BandWidthKBits);
        Write (cData.m_cData.m_cBandwidth.m_cMaxKbits,
                NStrText::BandWidthMaxKBits);

        Write (cData.m_cData.m_cAtm.m_cVpiVci,
                NStrText::AtmVpiVci);
        Write (cData.m_cData.m_cAtm.m_cATU_C,
                NStrText::AtmATU_C_Vendor, NStrText::AtmATU_C_VendSpec,
                NStrText::AtmATU_C_Revision);
        Write (cData.m_cData.m_cAtm.m_cATU_R,
                NStrText::AtmATU_R_Vendor, NStrText::AtmATU_R_VendSpec,
                NStrText::AtmATU_R_Revision);

        Write (cData.m_cData.m_cStatistics.m_cError.m_cFEC,
                NStrText::ErrorFEC);
        Write (cData.m_cData.m_cStatistics.m_cError.m_cCRC,
                NStrText::ErrorCRC);
        Write (cData.m_cData.m_cStatistics.m_cError.m_cHEC,
                NStrText::ErrorHEC);

        Write (cData.m_cData.m_cStatistics.m_cFailure.m_cErrorSecs15min,
                NStrText::FailErrorSecs15min);
        Write (cData.m_cData.m_cStatistics.m_cFailure.m_cErrorSecsDay,
                NStrText::FailErrorSecsDay);

        if (bTones)
        {
            Write (cData.m_cData.m_cTones.m_cBitallocUp,
                    NStrText::TonesBitallocUp, NStrText::Tone);
            Write (cData.m_cData.m_cTones.m_cBitallocDown,
                    NStrText::TonesBitallocDown, NStrText::Tone);
            Write (cData.m_cData.m_cTones.m_cSNR,
                    NStrText::TonesSNR, NStrText::Tone);
            Write (cData.m_cData.m_cTones.m_cGainQ2,
                    NStrText::TonesGainQ2, NStrText::Tone);
            Write (cData.m_cData.m_cTones.m_cNoiseMargin,
                    NStrText::TonesNoiseMargin, NStrText::Tone);
            Write (cData.m_cData.m_cTones.m_cChanCharLog,
                    NStrText::TonesChanCharLog, NStrText::Tone);

        }
        m_cFile.Close ();
    }
}

/*!
 * @fn          void CTextOutput::Write (const CBandplan* pBandplan,
 *                      const char* strFmtType,
 *                      const char* strFmtTones,
 *                      const char* strFmtBands,
 *                      const char* strFmtBandUp,
 *                      const char* strFmtBandDown)
 * @brief       write bandplan data
 * @param[in]   pBandplan
 *              bandplan pointer
 * @param[in]   strFmtType
 *              type format string
 * @param[in]   strFmtTones
 *              tones format string
 * @param[in]   strFmtBands
 *              bands format string
 * @param[in]   strFmtBandUp
 *              upstream band format string
 * @param[in]   strFmtBandDown
 *              downstream band format string
 */
void CTextOutput::Write (const CBandplan* pBandplan,
        const char* strFmtType,
        const char* strFmtTones,
        const char* strFmtBands,
        const char* strFmtBandUp,
        const char* strFmtBandDown)
{
    size_t nBand;

    m_cFile.Printf (strFmtType, pBandplan->Type ());
    m_cFile.Printf (strFmtTones, pBandplan->Tones (), pBandplan->Spacing ());
    m_cFile.Printf (strFmtBands, pBandplan->Bands ());

    for (nBand = 0; nBand < pBandplan->Bands (); nBand++)
    {
        const CBand &cBand = pBandplan->Band (nBand);

        if (-1 != cBand.MinUp ())
        {
            m_cFile.Printf (strFmtBandUp, nBand,
                cBand.MinUp (), cBand.MaxUp (),
                pBandplan->Spacing () * cBand.MinUp (),
                pBandplan->Spacing () * cBand.MaxUp ());
        }
        if (-1 != cBand.MaxDown ())
        {
            m_cFile.Printf (strFmtBandDown, nBand,
                cBand.MinDown (), cBand.MaxDown (),
                pBandplan->Spacing () * cBand.MinDown (),
                pBandplan->Spacing () * cBand.MaxDown ());
        }
    }
}

/*!
 * @fn          void CTextOutput::Write (const CDataInt &cData,
 *                      const char* strFmt)
 * @brief       write integer data
 * @param[in]   cData
 *              integer data
 * @param[in]   strFmt
 *              data format string
 */
void CTextOutput::Write (const CDataInt &cData,
        const char* strFmt)
{
    long nData;

    if (cData.Get (nData) && strFmt)
    {
        m_cFile.Printf (strFmt, nData);
    }
}

/*!
 * @fn          void CTextOutput::Write (const CDataIntUpDown &cData,
 *                      const char* strFmt)
 * @brief       write integer up/down data
 * @param[in]   cData
 *              integer up/down data
 * @param[in]   strFmt
 *              data format string
 */
void CTextOutput::Write (const CDataIntUpDown &cData,
        const char* strFmt)
{
    long nDataUp, nDataDown;

    if (cData.GetUp (nDataUp) && cData.GetDown (nDataDown) && strFmt)
    {
        m_cFile.Printf (strFmt, nDataUp, nDataDown);
    }
}

/*!
 * @fn          void CTextOutput::Write (const CDataDouble &cData,
 *                      const char* strFmt)
 * @brief       write double data
 * @param[in]   cData
 *              double data
 * @param[in]   strFmt
 *              data format string
 */
void CTextOutput::Write (const CDataDouble &cData,
        const char* strFmt)
{
    double fData;

    if (cData.Get (fData) && strFmt)
    {
        m_cFile.Printf (strFmt, fData);
    }
}

/*!
 * @fn          void CTextOutput::Write (const CDataDoubleUpDown &cData,
 *                      const char* strFmt)
 * @brief       write double up/down data
 * @param[in]   cData
 *              double up/down data
 * @param[in]   strFmt
 *              data format string
 */
void CTextOutput::Write (const CDataDoubleUpDown &cData,
        const char* strFmt)
{
    double fDataUp, fDataDown;

    if (cData.GetUp (fDataUp) && cData.GetDown (fDataDown) && strFmt)
    {
        m_cFile.Printf (strFmt, fDataUp, fDataDown);
    }
}

/*!
 * @fn          void CTextOutput::Write (const CArrayDoubleUpDown &cData,
 *                      const char* strFmt)
 * @brief       write double up/down array data
 * @param[in]   cData
 *              double up/down array data
 * @param[in]   strFmt
 *              data format string
 */
void CTextOutput::Write (const CArrayDoubleUpDown &cData,
        const char* strFmt)
{
    bool bValidUp, bValidDown;
    double fDataUp, fDataDown;
    size_t nIndex;
    size_t nSize = cData.Size ();

    if (strFmt)
    {
        for (nIndex = 0; nIndex < nSize; nIndex++)
        {
            bValidUp = cData.GetUp (nIndex, fDataUp);
            bValidDown = cData.GetDown (nIndex, fDataDown);

            if (bValidUp && bValidDown)
            {
                m_cFile.Printf (strFmt, nIndex, fDataUp, fDataDown);
            }
            else if (bValidUp)
            {
                m_cFile.Printf (strFmt, nIndex, fDataUp, nan (""));
            }
            else if (bValidDown)
            {
                m_cFile.Printf (strFmt, nIndex, nan (""), fDataUp);
            }
        }
    }
}

/*!
 * @fn          void CTextOutput::Write (const CDataString &cData,
 *                      const char* strFmt)
 * @brief       write string data
 * @param[in]   cData
 *              string data
 * @param[in]   strFmt
 *              format string
 */
void CTextOutput::Write (const CDataString &cData,
        const char* strFmt)
{
    CString strData;

    if (cData.Get (strData) && strFmt)
    {
        m_cFile.Printf (strFmt, (const char*)strData);
    }
}

/*!
 * @fn          void CTextOutput::Write (const CDataATU &cData,
 *                      const char* strFmtVendor,
 *                      const char* strFmtSpec,
 *                      const char* strFmtRevision)
 * @brief       write ATU-X data
 * @param[in]   cData
 *              ATU-X data
 * @param[in]   strFmtVendor
 *              vendor format string
 * @param[in]   strFmtSpec
 *              spec format string
 * @param[in]   strFmtRevision
 *              revision format string
 */
void CTextOutput::Write (const CDataATU &cData,
        const char* strFmtVendor,
        const char* strFmtSpec,
        const char* strFmtRevision)
{
    CString strVendor;
    int nSpec0, nSpec1, nRevision;

    if (cData.GetVendor (strVendor) && strFmtVendor)
    {
        m_cFile.Printf (strFmtVendor, (const char*)strVendor);
    }
    if (cData.GetSpec (nSpec0, nSpec1) && strFmtSpec)
    {
        m_cFile.Printf (strFmtSpec, nSpec0, nSpec1);
    }
    if (cData.GetRevision (nRevision) && strFmtRevision)
    {
        m_cFile.Printf (strFmtRevision, nRevision);
    }
}

/*!
 * @fn          void CTextOutput::Write (const CDataTone &cData,
 *                      const char* strFmt,
 *                      const char* strFmtTone)
 * @brief       write tone data
 * @param[in]   cData
 *              tone data
 * @param[in]   strFmt
 *              line format string
 * @param[in]   strFmtTone
 *              tone format string
 */
void CTextOutput::Write (const CDataTone &cData,
        const char* strFmt,
        const char* strFmtTone)
{
    if (cData.Valid () && strFmt && strFmtTone)
    {
        const CTone &cTone = cData.Data ();
        m_cFile.Printf (strFmt);

        for (size_t i= 0; i < cTone.Size (); i++)
        {
            if (0 == (i % 16))
            {
                m_cFile.Printf (strFmtTone, i);
            }
            m_cFile.Printf ("%4d", cTone[i]);
        }

        m_cFile.Printf ("\n");
    }
}

//_oOo_
